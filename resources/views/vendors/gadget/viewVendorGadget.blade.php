@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('vendors/lab/byvendor') }}">Lab</a>
      </li>

      <li>
        <a href="{{ url('vendors/material/byvendor') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('vendors/instrument/byvendor') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('vendors/gadget/byvendor') }}" class="active">Gadget</a>
      </li>
      <li>
        <a href="{{ url('vendors/machine/byvendor') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('vendors/maintenance/byvendor') }}">Maintenance</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:50%" role="presentation" class="active"><a href="{{ url('vendors/gadget/byvendor') }}">By Vendor</a></li>
      <li style="width:49%" role="presentation"><a href="{{ url('vendor/gadget/bywork/workdashboard') }}">By Work</a></li>
    </ul>
  </div>

  <ul style="width:80%" class="nav nav-pills">
    <li style="width:33%" role="presentation" class="active"><a href="{{ url('vendor/gadget/byvendor/trackwork') }}">Track Work</a></li>
  </ul>

  <div>
    <a href="{{ url('master/vendors/gadget/add') }}" class="btn btn-success pull-right"><i class="fa fa-plus fa-fw"></i>New Gadget Vendor</a>  
  </div>                    

  <br><br>
  <div class="panel panel-default filterable">
   <table class="table">
    <thead>
      <tr>
        <th class="panel panel-default">Vendor Name</th>
        <th>
          {!! Form::text('gadget_vendor_name', $vendor_gadget_detail->name, array('class'=>'form-control','readonly')) !!}
        </th>
        <td>
         {!! Form::open(array('route'=>'viewVendorGadgetWork', 'class' => 'form')) !!}
         {!! Form::hidden('vendor_gadget_id', $vendor_gadget_detail->id) !!}
         {!! Form::button('<span class="glyphicon glyphicon-eye-open"></span>View Details',array('type' => 'submit','class'=>'btn btn-xs .btn-default')) !!}
         {!! Form::close() !!}
       </td>                        
     </tr>
     <tr class="filters">
     <th><input type="text" class="form-control" placeholder="PO Number" disabled></th>
        <th><input type="text" class="form-control" placeholder="Expected Delivery Date" disabled></th>
       <th><input type="text" class="form-control" placeholder="Challan Number" disabled></th>
        <th><input type="text" class="form-control" placeholder="Bill No" disabled></th>
       <th><input type="text" class="form-control" placeholder="Cost" disabled></th>


        <th><button class="btn btn-default btn-xs btn-filter">Filter</button></th> 
    </tr>
  </thead>
  <tbody>
    <tr>
     <td>{{$vendor_gadget_detail->purchase_order_id}}</td>
        <td>{{$vendor_gadget_detail->expected_date_of_delivery}}</td>
        <td></td>
        <td></td>
        <td>{{$vendor_gadget_detail->rate}}</td>   
   </tr>
   </tbody>
 </table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop