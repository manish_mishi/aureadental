@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('vendors/lab/by_vendor') }}" class="active">Lab</a>
      </li>

      <li>
        <a href="{{ url('vendors/material/byvendor') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('vendors/instrument/byvendor') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('vendors/gadget/byvendor') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('vendors/machine/byvendor') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('vendors/maintenance/byvendor') }}">Maintenance</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br><br>  <br>                 
  <div class="row">
    {!! Form::label('Lab Vendor Name:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('lab_vendor_name', $lab_workspec_detail->vendor_lab_name,array('class'=>'form-control','readonly')) !!}
    </div>

  </div></br></br>
  <div class="row">
    {!! Form::label('Lab Work Name:',null,array('class'=>'col-xs-3 control-label'))!!}
    <div class="col-xs-3">
      {!! Form::text('lab_work_name',$lab_workspec_detail->lab_work_name,array('class'=>'form-control','readonly'))!!}
    </div>
  </div></br></br>
  <div class="row">
    {!! Form::label('Cost:',null,array('class'=>'col-xs-3 control-label'))!!}
    <div class="col-xs-3">
      {!! Form::text('cost',$lab_workspec_detail->rate,array('class'=>'form-control','readonly'))!!}
    </div>
  </div>
  <br>
  <div class="col-xs-7">
    {!! HTML::link('vendors/lab/by_work/work/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
  </div>

 <!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop