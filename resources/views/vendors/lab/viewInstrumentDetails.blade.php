@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('vendors/lab/byvendor') }}" class="active">Lab</a>
      </li>

      <li>
        <a href="{{ url('vendors/material/byvendor') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('vendors/instrument/byvendor') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('vendors/gadget/byvendor') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('vendors/machine/byvendor') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('vendors/maintenance/byvendor') }}">Maintenance</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

    <div class="panel panel-info">
      <ul style="width:100%" class="nav nav-pills">
        <li style="width:50%" role="presentation" class="active"><a href="{{ url('vendor/lab/byvendor/instrumentdetails') }}">By Vendor</a></li>
        <li style="width:49%" role="presentation"><a href="{{ url('vendor/lab/bywork/workdashboard') }}">By Work</a></li>

      </ul>
    </div>
    <ul style="width:80%" class="nav nav-pills">
      <li style="width:23%" role="presentation" ><a href="{{ url('vendor/lab/byvendor/trackwork') }}">Track Work</a></li>
      <li style="width:33%" role="presentation" class="active" ><a href="{{ url('vendor/lab/byvendor/instrumentdetails') }}">Instruments Details</a></li>

    </ul>
    <div>
      <a href="{{ url('#') }}" class="btn btn-success pull-right col-xs-2">Add Lab Vendor</a>  
    </div>
    <br><br><br><br><br><br>
    <div class="panel panel-default filterable">
     <table class="table">
      <thead>
        <tr><th class="panel panel-default">Vendor Name</th>
          <th><input type="text" class="form-control" placeholder="Vendor Name" disabled></th>
          <th><a href="#">View Details</a></th>
        </tr>
        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="Instrument Id" disabled></th>
          <th><input type="text" class="form-control" placeholder="Instrument Name" disabled></th>
          <th><input type="text" class="form-control" placeholder="Job ID" disabled></th>
          <th><input type="text" class="form-control" placeholder="Re-work ID" disabled></th>
          <th><input type="text" class="form-control" placeholder="Date" disabled></th>
          <th><input type="text" class="form-control" placeholder="Contact Person" disabled></th>
          <th><input type="text" class="form-control" placeholder="Cell#" disabled></th>
          <th><input type="text" class="form-control" placeholder="Status" disabled></th>

          <th></th>
          <th></th>
          <th></th>
          <!-- <th><button class="btn btn-default btn-xs btn-filter">Filter</button></th> -->
        </tr>
      </thead>
      <tbody>


        <tr>

          <td>  </td>
          <td>  </td>
          <td> </td>
          <td>  </td>
          <td>  </td>
          <td></td>
          <td></td>
          <td></td>
          <td><a href="#">View Details </a></td>
          <td></td>
        </tr>
      </tbody>
    </table>

  </div>

 <!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop