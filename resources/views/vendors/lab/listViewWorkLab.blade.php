@extends('layouts.menuNav')

@section('title')
Vendors -> Lab -> By Work
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('vendors/lab/by_vendor') }}" class="active">Lab</a>
      </li>

      <li>
        <a href="{{ url('vendors/material/byvendor') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('vendors/instrument/byvendor') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('vendors/gadget/byvendor') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('vendors/machine/byvendor') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('vendors/maintenance/byvendor') }}">Maintenance</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:50%" role="presentation" ><a href="{{ url('vendors/lab/by_vendor') }}">By Vendor</a></li>
      <li style="width:49%" role="presentation" class="active"><a href="{{ url('vendors/lab/by_work/work/listview') }}">By Work</a></li>
    </ul>
  </div>
  <div>
    <a href="{{ url('master/vendors/lab/add') }}" class="btn btn-success pull-right"><i class="fa fa-plus fa-fw"></i>New Lab Vendor</a>  
  </div>                    

  <br><br>
  <div class="panel panel-default filterable">
   <table class="table">
    <thead>

      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="Lab Work Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Lab Vendor Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Cost" disabled></th>
        <th><button class="btn btn-info btn-xs btn-filter">Filter</button></th>
      </tr>
    </thead>
    <tbody>

     @foreach($vendor_lab_details as $vendor_lab)
     <tr>
      <td>{{ $vendor_lab->lab_work_name }}</td>
      <td>{{ $vendor_lab->name }}</td>
      <td>{{ $vendor_lab->rate }}</td> 
      <td>
        {!! Form::open(array('route' => 'viewVendorLabWork','class' => 'form')) !!}
        {!! Form::hidden('vendor_lab_id', $vendor_lab->id) !!}
        {!! Form::button(' <span class="glyphicon glyphicon-eye-open"></span></i>View Details',array('type' => 'submit','class'=>'btn btn-xs .btn-default')) !!}
        {!! Form::close() !!}
      </td> 
    </tr>
    @endforeach

  </tbody>
</table>
</div>

 <!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop