@extends('layouts.menuNav')

@section('title')
Vendors -> Lab -> By Vendor -> Track Work
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('vendors/lab/by_vendor') }}" class="active">Lab</a>
      </li>

      <li>
        <a href="{{ url('vendors/material/byvendor') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('vendors/instrument/byvendor') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('vendors/gadget/byvendor') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('vendors/maintenance/byvendor') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('vendors/maintenance/byvendor') }}">Maintenance</a>
      </li>


    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:50%" role="presentation" class="active"><a href="{{ url('vendors/lab/by_vendor') }}">By Vendor</a></li>
      <li style="width:49%" role="presentation"><a href="{{ url('vendors/lab/by_work/work/listview') }}">By Work</a></li>

    </ul>
  </div>
  <ul style="width:80%" class="nav nav-pills">
    <li style="width:33%" role="presentation" class="active"><a href="{{ url('vendors/lab/by_vendor/track_work',[$vendor_lab_id]) }}">Track Work</a></li>
    <li style="width:33%" role="presentation"  ><a href="{{ url('vendors/lab/byvendor/instrumentdetails') }}">Instruments Details</a></li>

  </ul>
  <div>
    <a href="{{ url('master/vendors/lab/add') }}" class="btn btn-success pull-right"><i class="fa fa-plus fa-fw"></i>New Lab Vendor</a>  
  </div>                    

  <br><br>
  <div class="panel panel-default filterable">
   <table class="table">
    <thead>
      <tr>
        <th class="panel panel-default">Lab Vendor Name</th>
        <th>
          {!! Form::text('lab_name', $vendor_lab_details->name, array('class'=>'form-control','readonly')) !!}
        </th>
        <td>
         <a href="{{ url('vendors/lab/by_vendor/track_work',[$vendor_lab_id]) }}">
         {!! Form::button(' <span class="glyphicon glyphicon-eye-open"></span>Get Details',array('type' => 'submit','class'=>'btn btn-xs .btn-default')) !!}
         </a>
       </td>             
     </tr>
     <tr class="filters">
      <th><input type="text" class="form-control" placeholder="Work Name" disabled></th>
      <th><input type="text" class="form-control" placeholder="Cost" disabled></th>
      <th><button class="btn btn-info btn-xs btn-filter">Filter</button></th>
    </tr>

    <tr>
     <td>{{$lab_workspec_detail->lab_work_type_name}}</td>
     <td>{{$lab_workspec_detail->rate}}</td>
   </tr>

 </thead>

</table>

</div>

 <!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
@stop