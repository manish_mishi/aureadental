@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('vendors/lab/byvendor') }}" >Lab</a>
      </li>

      <li>
        <a href="{{ url('vendors/material/byvendor') }}" class="active">Material</a>
      </li>

      <li>
        <a href="{{ url('vendors/instrument/byvendor') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('vendors/gadget/byvendor') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('vendors/machine/byvendor') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('vendors/maintenance/byvendor') }}">Maintenance</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:50%" role="presentation" ><a href="{{ url('vendors/material/byvendor') }}">By Vendor</a></li>
      <li style="width:49%" role="presentation" class="active"><a href="{{ url('vendor/material/bywork/workdashboard') }}">By Work</a></li>
    </ul>
  </div>
  
  <div>
    <a href="{{ url('master/vendors/material/add') }}" class="btn btn-success pull-right"><i class="fa fa-plus fa-fw"></i>New Material Vendor</a>  
  </div>

  <br><br>
  <div class="panel panel-default filterable">
   <table class="table">
    <thead>

      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="PO Number" disabled></th>
        <th><input type="text" class="form-control" placeholder=" Material Vendor Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Status" disabled></th>
        <th><input type="text" class="form-control" placeholder="Delivery Date" disabled></th>
        <th><input type="text" class="form-control" placeholder="Cost" disabled></th>
        <th><button class="btn btn-info btn-xs btn-filter">Filter</button></th>
      </tr>
    </thead>
    <tbody>

     @foreach($vendor_material_details as $vendor_material)
     <tr>
      <td>{{ $vendor_material->purchase_order_id }}</td>
      <td>{{ $vendor_material->vendor_name}}</td>
      <td></td>
      <td>{{ $vendor_material->expected_date_of_delivery}}</td>
      <td>{{ $vendor_material->rate }}</td>

      <td>
        {!! Form::open(array('route' => 'viewVendorMaterialWork','class' => 'form')) !!}
        {!! Form::hidden('vendor_material_id', $vendor_material->id) !!}
        {!! Form::button(' <span class="glyphicon glyphicon-eye-open"></span></i>View Details',array('type' => 'submit','class'=>'btn btn-xs .btn-default')) !!}
        {!! Form::close() !!}
      </td> 
      <td></td>
    </tr>
    @endforeach

  </tbody>
</table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop