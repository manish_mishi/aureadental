@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('vendors/lab/byvendor') }}" >Lab</a>
      </li>

      <li>
        <a href="{{ url('vendors/material/byvendor') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('vendors/instrument/byvendor') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('vendors/gadget/byvendor') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('vendors/machine/byvendor') }}" class="active">Machine</a>
      </li>

      <li>
        <a href="{{ url('vendors/maintenance/byvendor') }}">Maintenance</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:50%" role="presentation" class="active"><a href="{{ url('vendors/machine/byvendor') }}">By Vendor</a></li>
      <li style="width:49%" role="presentation"><a href="{{ url('vendor/machine/bywork/workdashboard') }}">By Work</a></li>
    </ul>
  </div>

  <ul style="width:80%" class="nav nav-pills">
    <li style="width:33%" role="presentation" class="active"><a href="{{ url('vendor/machine/byvendor/trackwork') }}">Track Work</a></li>
  </ul>

  <div>
    <a href="{{ url('master/vendors/machine/add') }}" class="btn btn-success pull-right"><i class="fa fa-plus fa-fw"></i>New Machine Vendor</a>  
  </div>

  <br><br>
  <div class="panel panel-default filterable">
   <table class="table">
    <thead>
      <tr><th class="panel panel-default">Vendor Name</th>
        <th>
          {!! Form::text('machine_vendor_name', $vendor_machine_details->name, array('class'=>'form-control','readonly')) !!}
        </th>
        <td>
         {!! Form::open(array('route'=>'viewVendorMachineWork', 'class' => 'form')) !!}
         {!! Form::hidden('vendor_machine_id', $vendor_machine_details->id) !!}
         {!! Form::button(' <span class="glyphicon glyphicon-eye-open"></span></i>View Details',array('type' => 'submit','class'=>'btn btn-xs .btn-default')) !!}
         {!! Form::close() !!}
       </td>                        
     </tr>
     <tr class="filters">
      <th><input type="text" class="form-control" placeholder="Machine Work Name" disabled></th>
      <th><input type="text" class="form-control" placeholder="Cost" disabled></th>
      <th><button class="btn btn-info btn-xs btn-filter">Filter</button></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>{{$machine_workspec_detail->machine_name}}</td>
      <td>{{$machine_workspec_detail->rate}}</td>
    </tr>
  </tbody>
</table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop