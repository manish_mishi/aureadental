@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('vendors/lab/byvendor') }}" >Lab</a>
      </li>

      <li>
        <a href="{{ url('vendors/material/byvendor') }}" >Material</a>
      </li>

      <li>
        <a href="{{ url('vendors/instrument/byvendor') }}"  >Instrument</a>
      </li>

      <li>
        <a href="{{ url('vendors/gadget/byvendor') }}" >Gadget</a>
      </li>
      <li>
        <a href="{{ url('vendors/machine/byvendor') }}" class="active">Machine</a>
      </li>

      <li>
        <a href="{{ url('vendors/maintenance/byvendor') }}">Maintenance</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="row">
    {!! Form::label('Machine Vendor Name:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('machine_vendor_name', $machine_workspec_detail->name,array('class'=>'form-control','readonly')) !!}
    </div>

  </div></br></br>
  <div class="row">
    {!! Form::label('Machine Work Name:',null,array('class'=>'col-xs-3 control-label'))!!}
    <div class="col-xs-3">
      {!! Form::text('machine_work_name',$machine_workspec_detail->machine_name,array('class'=>'form-control','readonly'))!!}
    </div>
  </div></br></br>
  <div class="row">
    {!! Form::label('Cost:',null,array('class'=>'col-xs-3 control-label'))!!}
    <div class="col-xs-3">
      {!! Form::text('cost',$machine_workspec_detail->rate,array('class'=>'form-control','readonly'))!!}
    </div>
  </div>

<br>
<div class="col-xs-7">
  {!! HTML::link('vendor/machine/bywork/workdashboard', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop