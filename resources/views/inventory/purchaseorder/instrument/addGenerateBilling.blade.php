@extends('layouts.menuNav')

@section('title')
Inventory->purachase Order-> Add Vendor Details
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
  <div id="page-wrapper">

   <br>

   <div class="">
    
  <div class="panel panel-warning col-xs-12">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/instrument/add',[$po_id]) }}">Details</a></li>
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/instrument/vendor_details',[$po_id]) }}">Vendor Detail</a></li> 
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/instrument/generate_delivery',[$po_id]) }}">Generate delivery</a></li>
    <li style="width:24%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/instrument/generate_billing',[$po_id]) }}">Generate Billing</a></li> 
  </ul>
</div>
</div>

  <div class="container">

  {!! Form::open(array('route'=>'addGenerateBillingInstrument')) !!}

    <div class="row">

                <label class="col-xs-3">Bill number:</label>
                <div class="col-xs-3">
                            <input type="text" class="form-control" name="bill_no" id="billNo">
                </div>

                <label class="col-xs-3">Billing Status:</label>
                <div class="col-xs-3">
                            <select class="form-control" name="bill_status" id="billStatus">
                            @foreach($bill_status as $status)
                                <option value="{{$status->id}}">{{$status->name}}</option>
                            @endforeach()
                            </select>
                </div>

              </div><br>

              <div class="row">
                <label class="col-xs-3">Date Of Billing:</label>
                <div class="col-xs-3"><input data-provide="datepicker" data-date-format="dd/mm/yy" id="dateofbilling" class="form-control" name="dateofbilling" value="">
                </div>
                <label class="col-xs-3">Amount:</label>
                <div class="col-xs-3"><input type="text" class="form-control" name="amt">
                </div>

              </div><br><br><br>

              <div class="row">
                <label class="col-xs-3">Date Of Payment:</label>
                <div class="col-xs-3"><input data-provide="datepicker" data-date-format="dd/mm/yy" id="dateofpayment" class="form-control" name="dateofpayment" value="">
                </div>
                <label class="col-xs-3">Advance:</label>
                <div class="col-xs-3"><input type="text" class="form-control" name="advance">
                </div>

              </div><br>

              <div class="row">
                
                <div class="col-xs-6"></div>
                <label class="col-xs-3">Discount:</label>
                <div class="col-xs-3"><input type="text" class="form-control" name="discount">
                </div>

              </div><br>
              <div class="row">

                <div class="col-xs-6">
                </div>
                <label class="col-xs-3">Final Cost:</label>
                <div class="col-xs-3"><input type="text" class="form-control" name="final_cost">
                </div>

              </div><br>

              <div class="row">

              <div class="col-xs-5">  </div>
              <div class="col-xs-3">
                    <input type="submit" value="&#x2714; Generate Billing" class="btn btn-warning">
              </div>
              
                    <input type="hidden" value="{{$po_id}}" name="po_id">

              <div class="col-xs-2">
              <a href="{{url('billing/inventory/material')}}" class="btn btn-info pull-right">&#x2714; Make Payment</a>
              </div>
              <div class="col-xs-2">
                {!! HTML::link('inventory/purchase_order/instrument/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
              </div>

              </div>

        {!! Form::close() !!}
    
  </div>
</div>
</div>

<script type="text/javascript">


</script>

@stop