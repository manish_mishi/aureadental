@extends('layouts.menuNav')

@section('title')
Inventory->purachase Order-> Add
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
  <div id="page-wrapper">

   <br>

   <div class="">
   
  <div class="panel panel-warning col-xs-12">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/instrument/view',[$po_id]) }}">Details</a></li>
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/instrument/vendor_details/view',[$po_id]) }}">Vendor Detail</a></li> 
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/instrument/generate_delivery/view',[$po_id]) }}">Generate delivery</a></li>
    <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchase_order/instrument/generate_billing/view',[$po_id]) }}">Generate Billing</a></li> 
  </ul>
</div>
</div>

<div class="container">

  <!-- show current values open-->

  @foreach($curr_instrument as $details)
  <div class="row">
    <div class="col-xs-3">
     <label>Instrument Type<label>
     </div>
     <div class="col-xs-3">
      {!! Form::text('null',$details->instrument_type,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
    </div>

    <div class="col-xs-3">
      <label>Instrument Sub-type<label>
      </div>
      <div class="col-xs-3">
        {!! Form::text('null',$details->instrument_subtype,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
      </div>
    </div><br>

    <div class="row">
      <div class="col-xs-3">
        <label>Instrument Name<label>
        </div>
        <div class="col-xs-3">
          {!! Form::text('null',$details->instrument_name,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
        </div>

        <div class="col-xs-3">
         <label>Quantity Required<label>
         </div>
         <div class="col-xs-3">
          {!! Form::text('null',$details->quantity,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
        </div>
        
      </div><br>

      @endforeach()
      <!-- current values close -->

    <div class="row">
      <div class="col-xs-3">
        <label>Date Of Order<label>
        </div>
        <div class="col-xs-3">
          <input data-provide="datepicker" data-date-format="dd/mm/yy" id="dateoforder" class="form-control" name="dateoforder" value="{{$curr_po['0']->date_of_order}}" readonly>
        </div>

        <div class="col-xs-3">
         <label>Expected Delivery date<label>
         </div>
         <div class="col-xs-3">
           <input data-provide="datepicker" data-date-format="dd/mm/yy" id="expecteddateoforder" class="form-control" name="expecteddateoforder" value="{{$curr_po['0']->expected_date_of_delivery}}" readonly>
         </div>
       </div><br>

       <div class="row">
        <div class="col-xs-3">
          <label>Order status<label>
        </div>
          <div class="col-xs-3">
            <input class=" form-control"type="text" value="{{$curr_po['0']->name}}" readonly>
            
          </div>    
        </div><br>

        <div class="col-lg-12">
          {!! HTML::link('inventory/purchase_order/instrument/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
        </div>
      </div>




    </div>
  </div>

  
   @stop