@extends('layouts.menuNav')

@section('title')
Inventory->purachase Order-> Add
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
  <div id="page-wrapper">

   <br>

   <div class="">
    <!-- <div class="panel panel-info col-xs-12">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/purchaseorderAdd',[$po_id]) }}">Material</a></li>
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchaseorder/instrument/detailsadd',[$po_id]) }}">Instrument</a></li> 
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/gadget/detailsadd',[$po_id]) }}">Gadget</a></li>
      <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchaseorder/machine/detailsadd',[$po_id]) }}">Machine</a></li> 
    </ul>
  </div> -->
  <div class="panel panel-warning col-xs-12">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/instrument/add',[$po_id]) }}">Details</a></li>
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/instrument/vendor_details',[$po_id]) }}">Vendor Detail</a></li> 
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/instrument/generate_delivery',[$po_id]) }}">Generate delivery</a></li>
    <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchase_order/instrument/generate_billing',[$po_id]) }}">Generate Billing</a></li> 
  </ul>
</div>
</div>

<div class="container">


{!! Form::open(array('route'=>'updatePurchaseOrderInstrument')) !!}

<div><!-- cimbined div for duplication start -->

  <div class="row">
    <div class="col-xs-2">
     <label>Instrument Type<label>
     </div>
     <div class="col-xs-3">
      
      <select class="form-control" name="instrument_type[]" id="instrType">
        <option value>select</option>

        @foreach($instrument_type as $instrument)
        <option value="{{$instrument->id}}">{{$instrument->name}}</option>
        @endforeach()
        
      </select>
    </div>

    <div class="col-xs-3">
      <label>Instrument Sub-type<label>
      </div>
      <div class="col-xs-3">
        
        <select class="form-control" name="instrument_subtype[]" id="instrSubType">
          <option value>select</option>
        </select>
      </div>
    </div><br>

    <div class="row">
      <div class="col-xs-2">
        <label> Instrument Name<label>
        </div>
        <div class="col-xs-3">
          
          <select class="form-control" name="instrument_name[]" id="instrName">
            <option value>select</option>
          </select>
        </div>

        <div class="col-xs-3">
         <label>Quantity Required<label>
         </div>
         <div class="col-xs-3">
          
          {!! Form::text('quantity[]',null,array('class'=>'form-control','id'=>'quntityRequired')) !!}
        </div>
        <div class="col-xs-1">
            {!! Form::button(null,array('class'=>'btn btn-md btn-info glyphicon glyphicon-plus','onClick'=>'duplicateinstrument()')) !!}
        </div>
      </div><br>

      <!-- duplication start -->

      <div id="duplicater" style="display:none;">

        <div class="row">   
          <div class="col-xs-2"><label>Instrument Type</label></div>
          <div class="col-xs-3">
            <input type="text" class="form-control instr_type_name" id="instrumentTypeName" class="form-control" Readonly/><br>
            <input type="hidden" class="form-control instr_type_id" id="instrumentTypeId" class="form-control"/>
          </div>
          <div class="col-xs-3"><label>Instrument Sub-Type</div>
          <div class="col-xs-3">
            <input type="text" class="form-control instr_subtype_name" id="instrumentSubTypeName" class="form-control" Readonly/><br>
            <input type="hidden" class="form-control instr_subtype_id" id="instrumentSubTypeId" class="form-control"/>
          </div>
        </div>

        <div class="row">   
          <div class="col-xs-2"><label>Instrument Name</label></div>
          <div class="col-xs-3">
            <input type="text" class="form-control instr_name" id="instrumentName" class="form-control" Readonly/><br>
            <input type="hidden" class="form-control instr_id" id="instrumentId" class="form-control"/>
          </div>
          <div class="col-xs-3"><label>Quantity Required</div>
          <div class="col-xs-3">
            <input type="text" class="form-control qty" id="Quantity" class="form-control" Readonly/><br>
          </div>
          <div class="col-xs-1">
            {!! Form::button(null,array('id' =>'delete_btn','class'=>'btn btn-md btn-danger glyphicon glyphicon-trash','onClick'=>'removeduplicate(this)')) !!}
          </div>
        </div>
                
      </div>
      <!-- duplication end-->  

</div><!-- combined div for duplication finish -->

  <!-- show current values open-->

  @foreach($curr_instrument as $details)
  <div class="row">
    <div class="col-xs-2">
     <label>Instrument Type<label>
     </div>
     <div class="col-xs-3">
      {!! Form::text('null',$details->instrument_type,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
    </div>

    <div class="col-xs-3">
      <label>Instrument Sub-type<label>
      </div>
      <div class="col-xs-3">
        {!! Form::text('null',$details->instrument_subtype,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
      </div>
    </div><br>

    <div class="row">
      <div class="col-xs-2">
        <label>Instrument Name<label>
        </div>
        <div class="col-xs-3">
          {!! Form::text('null',$details->instrument_name,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
        </div>

        <div class="col-xs-3">
         <label>Quantity Required<label>
         </div>
         <div class="col-xs-3">
          {!! Form::text('null',$details->quantity,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
        </div>
        <div class="col-xs-1">
          <a href="{{url('inventory/purchase_order/instrument/delete',[$details->instrument_name_id,$po_id])}}"><button type="button" class="btn btn-md btn-danger glyphicon glyphicon-trash"></button></a>
        </div>
      </div><br>

      @endforeach()
      <!-- current values close -->

    <div class="row">
      <div class="col-xs-2">
        <label>Date Of Order<label>
        </div>
        <div class="col-xs-3">
          <input data-provide="datepicker" data-date-format="dd/mm/yy" id="dateoforder" class="form-control" name="dateoforder" value="{{$curr_po['0']->date_of_order}}">
        </div>

        <div class="col-xs-3">
         <label>Expected Delivery date<label>
         </div>
         <div class="col-xs-3">
           <input data-provide="datepicker" data-date-format="dd/mm/yy" id="expecteddateoforder" class="form-control" name="expecteddateoforder" value="{{$curr_po['0']->expected_date_of_delivery}}">
         </div>
       </div><br>

       <div class="row">
        <div class="col-xs-2">
          <label>Order status<label>
        </div>
          <div class="col-xs-3">
            <select class="form-control" name="order_status_id">
              <option value="{{$curr_po['0']->purchase_order_status_id}}">{{$curr_po['0']->name}}</option>
              @foreach($order_status as $order)
            <option value="{{$order->id}}">{{$order->name}}</option>
            @endforeach
            </select>
          </div>    
        </div><br>

        <div class="col-lg-12">
          <div class="col-xs-8">
          </div>
        <div class="col-xs-2">
          <button type="submit" class="btn btn-warning pull-right">&#x2714; Save</button>
        </div>
          <div class="col-xs-2">
            {!! HTML::link('inventory/purchase_order/instrument/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
            
        </div>
          <input type="hidden" name= "po_id" value="{{$po_id}}">
        </div>
      </div>


{!! Form::close() !!}

    </div>
  </div>

  <script type="text/javascript">

  /*for duplication*/

  /*instrument type*/
  $('#instrType').change(function(){

    $('#instrumentTypeId').val($(this).val());
    $('#instrumentTypeName').val($('option:selected',this).text());
  });

  /*instrument subtype*/
  $('#instrSubType').change(function(){
    $('#instrumentSubTypeId').val($(this).val());
    $('#instrumentSubTypeName').val($('option:selected',this).text());
  });

  /*instrument names*/

  $('#instrName').change(function(){
    $('#instrumentId').val($(this).val());
    $('#instrumentName').val($('option:selected',this).text());
  });

  /*quantity*/
  $('#quntityRequired').change(function(){
    $('#Quantity').val($(this).val());
  });

  

  var i=0;

  function duplicateinstrument()
  {
    var instrumenttype_id=document.getElementById('instrumentTypeId').value;
    var instrumenttype_name=document.getElementById('instrumentTypeName').value;

    var instrumentsubtype_id=document.getElementById('instrumentSubTypeId').value;
    var instrumentsubtype_name=document.getElementById('instrumentSubTypeName').value;

    var instrument_id=document.getElementById('instrumentId').value;
    var instrument_name=document.getElementById('instrumentName').value;

    var qty=document.getElementById('Quantity').value;



    if(instrument_name!='' && instrumentsubtype_name!='' && instrumenttype_name!='')
    {
      var original=document.getElementById('duplicater');
      original.style.display = "block";

      document.getElementById('instrumentTypeId').value=instrumenttype_id;
      document.getElementById('instrumentTypeName').value=instrumenttype_name;

      document.getElementById('instrumentSubTypeId').value=instrumentsubtype_id;
      document.getElementById('instrumentSubTypeName').value=instrumentsubtype_name;

      document.getElementById('instrumentId').value=instrument_id;
      document.getElementById('instrumentName').value=instrument_name;


    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicater" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);
    original.style.display = "none";

    document.getElementById('instrumentTypeId').value="";
    document.getElementById('instrumentTypeName').value="";

    document.getElementById('instrumentSubTypeId').value="";
    document.getElementById('instrumentSubTypeName').value="";

    document.getElementById('instrumentId').value="";
    document.getElementById('instrumentName').value="";



    /*to make dropdown blank*/
    document.getElementById('instrType').value="select";
    document.getElementById('instrSubType').value="select";
    document.getElementById('instrName').value="select";
    document.getElementById('quntityRequired').value="";

    var nameElemntinstrtypeid = document.getElementById("duplicater"+i).getElementsByClassName('instr_type_id')[0];
    nameElemntinstrtypeid.setAttribute("name","instrument_type[]");

    var nameElemntinstrsubtypeid = document.getElementById("duplicater"+i).getElementsByClassName('instr_subtype_id')[0];
    nameElemntinstrsubtypeid.setAttribute("name","instrument_subtype[]");

    var nameElemntinstrid = document.getElementById("duplicater"+i).getElementsByClassName('instr_id')[0];
    nameElemntinstrid.setAttribute("name","instrument_name[]");

    var nameElemntqty = document.getElementById("duplicater"+i).getElementsByClassName('qty')[0];
    nameElemntqty.setAttribute("name","quantity[]");
  }

}

function removeduplicate(element)
{
    element=element.parentNode.parentNode.parentNode;//gets the id of the parent
    element.parentNode.removeChild(element);
  }


  
  /*ajax call*/


  $(document).ready(function()
{

   $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

   $('#instrType').change(function()
   {

    var instrument_id=$(this).val();
    var instrumentsubtypeselect=document.getElementById('instrSubType');



    if(instrument_id != " ")
    {

      $(instrument_id).val(0);
    $(instrumentsubtypeselect).empty().append("<option value>Select</option>");
    $('#instrName').empty().append("<option value>Select</option>");

     $.ajax({
      method: "POST",
      url: '{{url("inventory/purchaseorder/instrumentsubtype/")}}' + "/" + instrument_id,
      success: function(data){
        console.log("data"+data);
        for(var i in data)
        {
          $(instrumentsubtypeselect).append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
        }
      }
    }); 
   }
   else
   {
    $(instrument_id).val(0);
    $(instrumentsubtypeselect).empty().append("<option value>Select</option>");
    $('#instrName').empty().append("<option value>Select</option>");
   }


  });


   $('#instrSubType').change(function()
   {

    var instrument_subtypeid=$(this).val();
    var instrumentnameselect=document.getElementById('instrName');

    

    if(instrument_subtypeid != " ")
    {

      $(instrument_subtypeid).empty().append("<option value>Select</option>");
      $(instrumentnameselect).empty().append("<option value>Select</option>");

     $.ajax({
      method: "POST",
      url: '{{url("inventory/purchaseorder/instrumentname/")}}' + "/" + instrument_subtypeid,
      success: function(data){
        console.log("data"+data);
        for(var i in data)
        {
          $(instrumentnameselect).append('<option value=' + data[i]['id'] + '>' + data[i]['instrument_name'] + '</option>');
        }
      }
    }); 
   }
   else
   {
    $(instrument_subtypeid).empty().append("<option value>Select</option>");
      $(instrumentnameselect).empty().append("<option value>Select</option>");
   }


  });

});

   </script>

   @stop