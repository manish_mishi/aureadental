@extends('layouts.menuNav')

@section('title')
Inventory->purachase Order-> Add Vendor Details
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
  <div id="page-wrapper">

   <br>

   <div class="">
    
  <div class="panel panel-warning col-xs-12">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/instrument/add',[$po_id]) }}">Details</a></li>
    <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/instrument/vendor_details',[$po_id]) }}">Vendor Detail</a></li> 
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/instrument/generate_delivery',[$po_id]) }}">Generate delivery</a></li>
    <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchase_order/instrument/generate_billing',[$po_id]) }}">Generate Billing</a></li> 
  </ul>
</div>
</div>

<div class="container">

  <div class="col-xs-12">
    <div class="col-xs-6">
      <label>Vendor List </label>
      <br><br>
      <div class="panel panel-default filterable">
        <table class="table">
          <thead>

            <tr class="filters">
              <th><input type="text" class="form-control" placeholder="Instrument Name" disabled></th>
              <th><input type="text" class="form-control" placeholder="Vendor Name" disabled></th>
              <th><input type="text" class="form-control" placeholder="Rate" disabled></th>
            </tr>
          </thead>
          <tbody>

            @foreach($vendors as $vendor)
            <tr>
              <td>{{$vendor->instrument_name}}</td>
              <td>{{$vendor->name}}</td>
              <td>{{$vendor->rate}}</td>
            </tr>
            @endforeach()

          </tbody>
        </table>                                          
      </div>
    </div>
    <div class="col-xs-6">
      <div class="col-xs-4"><label>Vendor Name</label></div>
      <div class="col-xs-5">
        <select class="form-control" id="VendorId">
          <option value="{{$ven_instrument['0']->vendor_name_id}}">{{$ven_instrument['0']->name}}</option>
          @foreach($vendornames as $vendor)
          <option value="{{$vendor->id}}">{{$vendor->name}}</option>
          @endforeach
        </select>
      </div><br><br>  

      <div class="panel panel-default filterable">
        <table class="table">
          <thead>
            <tr class="filters">
              <th><input type="text" class="form-control" placeholder="Instrument Name" disabled></th>
              <th><input type="text" class="form-control" placeholder="Rate" disabled></th>
              <th><input type="text" class="form-control" placeholder="Qty" disabled></th>
              <th><input type="text" class="form-control" placeholder="Amount" disabled></th>
            </tr>
          </thead>
          <tbody id="InstrDetails">
            @foreach($ven_instrument as $vendor)
            <tr>
              <td>{{$vendor->instrument_name}}</td>
              <td>{{$vendor->rate}}</td>
              <td>{{$vendor->quantity}}</td>
              <td>{{$vendor->amount}}</td>
            </tr>

      @endforeach
          </tbody>

          <tr>
            <td colspan="3"></td>
            <td id='TotalAmount'>{{$ven_instrument['0']->total_amount}}</td>
          </tr>
        </table>                                          
      </div>
      
    </div>
  </div>

  

 <div class="col-xs-12">
  {!! Form::open(array('route'=>'updatevendordetailsinstrument')) !!}



  <div id="hiddenInstrDetails">
  </div>

<input type="hidden" value="{{$vendorlist['0']->total_amount}}" name="curr_totalamt" id="CurrTotalAmt" class="form-control" readonly>
  
  <span  style="display:block" id="Discountspan"> 
  <div class="form-group">
    <div class="col-md-6"></div>
    {!! Form::label('Discount :', null, array('class'=>'col-xs-2 control-label')) !!}
    <div class="col-md-4">
     {!! Form::text('discount',$vendorlist['0']->discount,array('class'=>'form-control','id'=>'discountAmount')) !!}<br>
   </div>
 </div>
</span>

<span  style="display:block" id="finalAmountspan"> 
  <div class="form-group">
    <div class="col-md-6"></div>
    {!! Form::label('Final Amount :', null, array('class'=>'col-xs-2 control-label')) !!}
    <div class="col-md-4">
     {!! Form::text('finalamount', $vendorlist['0']->final_amount,array('class'=>'form-control','id'=>'finalAmount','readonly')) !!}<br>
   </div>
 </div>
</span>

  <div class="col-lg-12">
          <div class="col-xs-8">
          </div>
        <div class="col-xs-2">
          <button type="submit" class="btn btn-warning pull-right">&#x2714; Place Order</button>
        </div>
          <div class="col-xs-2">
            {!! HTML::link('inventory/purchase_order/instrument/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
            
        </div>
          
</div>
  <input type="hidden" value="{{$po_id}}" name="po_id" id="PoId">

  {!! Form::close() !!}
</div>





</div>
</div>
</div>

<script type="text/javascript">

$(document).ready(function()
{

 $.ajaxSetup(
 {
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

 $('#VendorId').change(function()
 {
  var instrdetails=document.getElementById('InstrDetails');
  var hiddenInstrDetails = document.getElementById('hiddenInstrDetails');
  var totalAmount = document.getElementById('TotalAmount');

  $('#discountAmount').val("");
  $('#finalAmount').val(""); 

  var van_id=$(this).val();

  var po_id=$('#PoId').val();


  if(van_id !='')
  {

    $.ajax(
    {     

      method:'POST',
      url:'{{url("inventory/purchaseorder/instrument/vendordetails/getinstrument")}}' + "/" + van_id + "/" + po_id,
      success:function(data)
      {          
        console.log('data'+data);

        $(instrdetails).html('');
        $(hiddenInstrDetails).html('');
        $(totalAmount).html('');    

        for(var i in data)
        {

          $(instrdetails).append('<tr><td>'+data[i]['instrument_name']+'</td><td>'+data[i]['rate']+'</td><td>'+data[i]['quantity']+'</td><td>'+data[i]['amount']+'</td></tr>');

          $(hiddenInstrDetails).append('<input type="hidden" name="inst_name_id[]" value="'+data[i]['inst_name_id']+'">');

          $(hiddenInstrDetails).append('<input type="hidden" name="vendor_inst_id" value="'+data[i]['vendor_inst_id']+'">');

          $(hiddenInstrDetails).append('<input type="hidden" name="rate[]" value="'+data[i]['rate']+'">');

          $(hiddenInstrDetails).append('<input type="hidden" name="amount[]" value="'+data[i]['amount']+'"><br>');
        }


        var totalamt=0;

        for(var i in data)
        {
          totalamt=totalamt + data[i]['amount'];
        }

        $(totalAmount).append(totalamt);

        $("#CurrTotalAmt").val(totalamt);

        document.getElementById('Discountspan').style.display="block";
        document.getElementById('finalAmountspan').style.display="block";

      }


      

    });

      

}
else
{


  $(instrdetails).html('');
  $(hiddenInstrDetails).html('');
  $(totalAmount).html(''); 

}

});


$("#discountAmount").change(function () {

    $discountAmount = parseFloat($(this).val());
    $totalAmount = parseFloat($("#CurrTotalAmt").val());
    
    
    if(!$.isNumeric($discountAmount))
    {
      $totalDeduction = 0;
    }

    if(!$.isNumeric($totalAmount))
    {
      $totalAmount = 0;
    }


    $netAmount = ($totalAmount -  $discountAmount);

    $("#finalAmount").val($netAmount);

  });


});


</script>

@stop