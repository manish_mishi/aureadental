@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

   <br>

   <div class="">
    <!-- <div class="panel panel-info col-xs-12">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchaseorder/purchaseorderAdd',[$po_id]) }}">Material</a></li>
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/instrument/detailsadd',[$po_id]) }}">Instrument</a></li> 
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/gadget/detailsadd',[$po_id]) }}">Gadget</a></li>
      <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchaseorder/machine/detailsadd',[$po_id]) }}">Machine</a></li> 
    </ul>
  </div> -->
  <div class="panel panel-warning col-xs-12">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/material/add',[$po_id]) }}">Details</a></li>
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/material/vendor_details',[$po_id]) }}">Vendor Detail</a></li> 
    <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/material/generate_delivery',[$po_id]) }}">Generate delivery</a></li>
    <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchase_order/material/generate_billing',[$po_id]) }}">Generate Billing</a></li> 
  </ul>
</div>
</div>


{!! Form::open(array('route'=>'editGenarateDelivery')) !!}


<div class="row">
  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
    {!! Form::label('Challan Number', null, array()) !!}
  </div>
  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
    {!! Form::label('Date of Delivery', null, array()) !!}
  </div>
  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
    {!! Form::label('Delivery Location', null, array()) !!}
  </div>
  <a onClick="duplicatechallannumber()" class="btn btn-info btn-md col-md-1">
    <span class="glyphicon glyphicon-plus"></span> 
  </a>
</div>

<div class="row">
  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
    {!! Form::text('challan_no[]',null,array('class'=>'form-control','id'=>'challanNumberId')) !!}
  </div>
  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
    <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dateofDelivery" class="form-control" name="date_of_delivery[]" value="<?php echo date("d/m/Y");?>">
  </div>
  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
    {!! Form::text('delivery_location[]',null,array('class'=>'form-control','id'=>'deliveryLocation')) !!}
  </div>
</div><br>

<div class="row">
  <div id="duplicater" style="display:none;">

    <div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <input type="text" class="form-control challanNo" id="challan_no" class="form-control" Readonly/><br>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <input type="text" class="form-control dateofDelivery" id="date_of_delivery" class="form-control" Readonly/><br>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <input type="text" class="form-control deliveryLocation" id="delivery_location" class="form-control" Readonly/><br>
      </div>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::button(null,array('id' =>'delete_btn','class'=>'btn btn-md btn-danger glyphicon glyphicon-trash','onClick'=>'removeduplicate(this)')) !!}
    </div><br><br><br>
  </div>
</div>

<div class="row">
  
          <div class="col-xs-7">
          </div>
        <div class="col-xs-2">
          <button type="submit" class="btn btn-warning pull-right">&#x2714; Generate Delivery</button>
        </div>
          <div class="col-xs-2">
            {!! HTML::link('inventory/purchase_order/material/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
            
        </div>
          <input type="hidden" name= "po_id" value="{{$po_id}}">
  </div><br>

{!! Form::close() !!}



</div>
</div>
<script>

var i=0;

function duplicatechallannumber() 
{

  var challannumber = document.getElementById('challanNumberId').value;
  var dateofdelivery = document.getElementById('dateofDelivery').value;
  var deliverylocation = document.getElementById('deliveryLocation').value;
  
  if(challannumber!='' )
  {

    var original = document.getElementById('duplicater');
    original.style.display = "block";

    document.getElementById('challan_no').value =challannumber;
    document.getElementById('date_of_delivery').value =dateofdelivery;
    document.getElementById('delivery_location').value =deliverylocation;

    
    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicater" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);
    original.style.display = "none";

    document.getElementById('challanNumberId').value="";
    document.getElementById('dateofDelivery').value="";
    document.getElementById('deliveryLocation').value="";
    
    var nameElemnt = document.getElementById("duplicater"+i).getElementsByClassName('challanNo')[0];
    nameElemnt.setAttribute("name","challan_no[]");

    var nameElemnt = document.getElementById("duplicater"+i).getElementsByClassName('dateofDelivery')[0];
    nameElemnt.setAttribute("name","date_of_delivery[]");

    var nameElemnt = document.getElementById("duplicater"+i).getElementsByClassName('deliveryLocation')[0];
    nameElemnt.setAttribute("name","delivery_location[]");

  }

}

function removeduplicate(element)
{
    element=element.parentNode.parentNode;//gets the id of the parent
    element.parentNode.removeChild(element);
  }

  </script>
  @stop