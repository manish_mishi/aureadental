@extends('layouts.menuNav')

@section('title')
Vendor Details
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

   <br>

   <div class="">

    <div class="panel panel-info col-xs-12">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/material/add',[$po_id]) }}">Details</a></li>
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/material/vendor_details',[$po_id]) }}">Vendor Detail</a></li> 
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/material/generate_delivery',[$po_id]) }}">Generate delivery</a></li>
      <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchase_order/material/generate_billing',[$po_id]) }}">Generate Billing</a></li> 
    </ul>
  </div>
</div>

<div class="col-xs-12">
 <div class="col-xs-6">
  <label>Vendor List </label>
  <br><br>
  <div class="panel panel-default filterable">
    <table class="table">
     <thead>

       <tr class="filters">
        <th><input type="text" class="form-control" placeholder="Material Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Vendor Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Rate" disabled></th>
      </tr>
    </thead>
    <tbody>
      @foreach($vendors as $vendor)
      <tr>
        <td>{{$vendor->material_name}}</td>
        <td>{{$vendor->name}}</td>
        <td>{{$vendor->rate}}</td>
      </tr>
      @endforeach()

    </tbody>
  </table>         																	
</div>
</div>
<div class="col-xs-6">
  <div class="col-xs-4"><label>Vendor Name</label></div>
  <div class="col-xs-5">
    <select class="form-control" id="VendorId">
      <option value>select</option>
      @foreach($vendornames as $vendor)
      <option value="{{$vendor->id}}">{{$vendor->name}}</option>
      @endforeach
    </select>
  </div><br><br>  

  <div class="panel panel-default filterable">
    <table class="table">
     <thead>
       <tr class="filters">
        <th><input type="text" class="form-control" placeholder="Mat Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Rate" disabled></th>
        <th><input type="text" class="form-control" placeholder="Qty" disabled></th>
        <th><input type="text" class="form-control" placeholder="Amount" disabled></th>
      </tr>
    </thead>
    <tbody id="MatDetails">

    </tbody>

    <tr>
      <td colspan="3"></td>
      <td id='TotalAmount'></td>
    </tr>

  </table>         																	
</div>

</div>
</div>


<div class="col-xs-12">

  {!! Form::open(array('route'=>'addVendorDetailsMaterial')) !!}

  <div id="hiddenMatDetails">
  </div>

  <span  style="display:none" id="Discountspan"> 
    <div class="form-group">
      <div class="col-md-6"></div>
      {!! Form::label('Discount :', null, array('class'=>'col-xs-2 control-label')) !!}
      <div class="col-md-4">
       {!! Form::text('discount', null,array('class'=>'form-control','id'=>'discountAmount')) !!}<br>
     </div>
   </div>
 </span>

 <span  style="display:none" id="finalAmountspan"> 
  <div class="form-group">
    <div class="col-md-6"></div>
    {!! Form::label('Final Amount :', null, array('class'=>'col-xs-2 control-label')) !!}
    <div class="col-md-4">
     {!! Form::text('finalamount', null,array('class'=>'form-control','id'=>'finalAmount','readonly')) !!}<br>
   </div>
 </div>
</span>

<div class="col-lg-12">
  <div class="col-xs-8">
  </div>
  <div class="col-xs-2">
    <button type="submit" class="btn btn-warning pull-right">&#x2714; Place Order</button>
  </div>
  <div class="col-xs-2">
    {!! HTML::link('inventory/purchase_order/material/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}

  </div>

</div>


<input type="hidden" value="{{$po_id}}" name="po_id" id="PoId">

{!! Form::close() !!}
</div>

</div>

</div>
<script>

$(document).ready(function()
{

 $.ajaxSetup(
 {
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

 $('#VendorId').change(function()
 {
  var matdetails=document.getElementById('MatDetails');
  var hiddenMatdetails = document.getElementById('hiddenMatDetails');
  var totalAmount = document.getElementById('TotalAmount');

  $('#discountAmount').val("");
  $('#finalAmount').val(""); 


  var van_id=$(this).val();

  var po_id=$('#PoId').val();

  if(van_id !='')
  {

    $.ajax(
    {     

      method:'POST',
      url:'{{url("inventory/purchaseorder/material/vendordetails/getmaterial")}}' + "/" + van_id + "/" + po_id,
      success:function(data)
      {    

        console.log('data'+data);

        $(matdetails).html('');     
        $(hiddenMatdetails).html('');
        $(totalAmount).html('');

        for(var i in data)
        {
          $(matdetails).append('<tr><td>'+data[i]['material_name']+'</td><td>'+data[i]['rate']+'</td><td>'+data[i]['quantity']+'</td><td>'+data[i]['amount']+'</td></tr>');

          $(hiddenMatdetails).append('<input type="hidden" name="mat_name_id[]" value="'+data[i]['mat_name_id']+'">');

          $(hiddenMatdetails).append('<input type="hidden" name="vendor_mat_id" value="'+data[i]['vendor_mat_id']+'">');

          $(hiddenMatdetails).append('<input type="hidden" name="rate[]" value="'+data[i]['rate']+'">');

          $(hiddenMatdetails).append('<input type="hidden" name="amount[]" value="'+data[i]['amount']+'">');

        }

        var totalamt=0;

        for(var i in data)
        {
          totalamt=totalamt + data[i]['amount'];
        }

        $(totalAmount).append(totalamt);

        $(hiddenMatdetails).append('<input type="hidden" name="totalamt" id="totalAmount" value="'+totalamt+'"><br>');

        document.getElementById('Discountspan').style.display="block";
        document.getElementById('finalAmountspan').style.display="block";

      }
    });

}
else
{
  $(matdetails).html('');     
  $(hiddenMatdetails).html('');
  $(totalAmount).html(''); 
}

});


$("#discountAmount").change(function () {

  $discountAmount = parseFloat($(this).val());
  $totalAmount = parseFloat($("#totalAmount").val());


  if(!$.isNumeric($discountAmount))
  {
    $totalDeduction = 0;
  }

  if(!$.isNumeric($totalAmount))
  {
    $totalAmount = 0;
  }


  $netAmount = ($totalAmount -  $discountAmount);

  $("#finalAmount").val($netAmount);

});


});

</script>

@stop