@extends('layouts.menuNav')

@section('title')
Inventory->purachase Order-> Edit
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
  <div id="page-wrapper">

   <br>

   <div class="">
    <!-- <div class="panel panel-info col-xs-12">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchaseorder/purchaseorderAdd',[$po_id]) }}">Material</a></li>
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/instrument/detailsadd',[$po_id]) }}">Instrument</a></li> 
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/gadget/detailsadd',[$po_id]) }}">Gadget</a></li>
      <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchaseorder/machine/detailsadd',[$po_id]) }}">Machine</a></li> 
    </ul>
  </div> -->
  <div class="panel panel-warning col-xs-12">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/material/add',[$po_id]) }}">Details</a></li>
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/material/vendor_details',[$po_id]) }}">Vendor Detail</a></li> 
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/material/generate_delivery',[$po_id]) }}">Generate delivery</a></li>
    <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchase_order/material/generate_billing',[$po_id]) }}">Generate Billing</a></li> 
  </ul>
</div>
</div>

<div class="container">

  {!! Form::open(array('route'=>'editPurchaseOrder')) !!}

  <div><!-- combine div with duplicate start -->
    <div class="row">
      <div class="col-xs-2">
       <label>Material Type</label>
       </div>
       <div class="col-xs-3">
        
        <select class="form-control" name="material_type[]" id="matType">
          <option value>select</option>
          @foreach($material_type as $material)
          <option value="{{$material->id}}">{{$material->name}}</option>
          @endforeach 
        </select>
      </div>

      <div class="col-xs-3">
        <label>Material Sub-type<label>
        </div>
        <div class="col-xs-3">
          
          <select class="form-control" name="material_subtype[]" id="matSubType">
            <option value>select</option>
          </select>
        </div>
      </div><br>

      <div class="row">
        <div class="col-xs-2">
          <label>Material Name</label>
          </div>
          <div class="col-xs-3">
            
            <select class="form-control" name="mat_name[]" id="matName">
              <option value>select</option>
            </select>
          </div>
          <div class="col-xs-3">
           <label>Quantity Required<label>
           </div>
           <div class="col-xs-3">
            
            {!! Form::text('quantity[]',null,array('class'=>'form-control','id'=>'quntityRequired')) !!}
          </div>
          <div class="col-xs-1">
            {!! Form::button(null,array('class'=>'btn btn-md btn-info glyphicon glyphicon-plus','onClick'=>'duplicatematerial()')) !!}
        </div>
        </div><br>

      <!-- duplication start -->

      <div id="duplicater" style="display:none;" class="rbox">

        <div class="row">   
          <div class="col-xs-2"><label>Material Type</label></div>
          <div class="col-xs-3">
            <input type="text" class="form-control mat_type_name" id="materialTypeName" class="form-control" Readonly/><br>
            <input type="hidden" class="form-control mat_type_id" id="materialTypeId" class="form-control"/>
          </div>
          <div class="col-xs-3"><label>Material Sub-Type</div>
          <div class="col-xs-3">
            <input type="text" class="form-control mat_subtype_name" id="materialSubTypeName" class="form-control" Readonly/><br>
            <input type="hidden" class="form-control mat_subtype_id" id="materialSubTypeId" class="form-control"/>
          </div>
        </div>

        <div class="row">   
          <div class="col-xs-2"><label>Material Name</label></div>
          <div class="col-xs-3">
            <input type="text" class="form-control mat_name" id="materialName" class="form-control" Readonly/><br>
            <input type="hidden" class="form-control mat_id" id="materialId" class="form-control"/>
          </div>
          <div class="col-xs-3"><label>Quantity Required</div>
          <div class="col-xs-3">
            <input type="text" class="form-control qty" id="Quantity" class="form-control" Readonly/><br>
          </div>
          <div class="col-xs-1">
            {!! Form::button(null,array('id' =>'delete_btn','class'=>'btn btn-md btn-danger glyphicon glyphicon-trash','onClick'=>'removeduplicate(this)')) !!}
          </div>
        </div>
                
      </div>
      <!-- duplication end-->
    </div><!-- combine div with duplicate finish -->


  <!-- show current values open-->

  @foreach($curr_mat_details as $details)
  <div class="row">
    <div class="col-xs-2">
     <label>Material Type<label>
     </div>
     <div class="col-xs-3">
      {!! Form::text('null',$details->material_type,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
    </div>

    <div class="col-xs-3">
      <label>Material Sub-type<label>
      </div>
      <div class="col-xs-3">
        {!! Form::text('null',$details->material_subtype,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
      </div>
    </div><br>

    <div class="row">
      <div class="col-xs-2">
        <label> Material Name<label>
        </div>
        <div class="col-xs-3">
          {!! Form::text('null',$details->material_name,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
        </div>

        <div class="col-xs-3">
         <label>Quantity Required<label>
         </div>
         <div class="col-xs-3">
          {!! Form::text('null',$details->quantity,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
        </div>
        <div class="col-xs-1">
          <a href="{{url('inventory/purchase_order/material/delete',[$details->mat_id,$po_id])}}"><button type="button" class="btn btn-md btn-danger glyphicon glyphicon-trash"></button></a>
        </div>
      </div><br>

      @endforeach()
      <!-- current values close -->

    <div class="row">
      <div class="col-xs-2">
        <label>Date Of Order<label>
        </div>
        <div class="col-xs-3">
          <input data-provide="datepicker" data-date-format="dd/mm/yy" id="dateoforder" class="form-control" name="dateoforder" value="{{$curr_po_details['0']->date_of_order}}">
        </div>

        <div class="col-xs-3">
         <label>Expected Delivery date<label>
         </div>
         <div class="col-xs-3">
           <input data-provide="datepicker" data-date-format="dd/mm/yy" id="expecteddateoforder" class="form-control" name="expecteddateoforder" value="{{$curr_po_details['0']->expected_date_of_delivery}}">
         </div>
       </div><br>

       <div class="row">
        <div class="col-xs-2">
          <label>Order status<label>
          </div>
          <div class="col-xs-3">
            <select class="form-control" name="order_status_id">
              <option value="{{$curr_po_details['0']->purchase_order_status_id}}">{{$curr_po_details['0']->name}}</option>
              @foreach($order_status as $order)
              <option value="{{$order->id}}">{{$order->name}}</option>
              @endforeach
            </select>
          </div>    
        </div><br>

        <div class="col-lg-12">
          <div class="col-xs-8">
          </div>
        <div class="col-xs-2">
          <button type="submit" class="btn btn-warning pull-right">&#x2714; Save</button>
        </div>
          <div class="col-xs-2">
            {!! HTML::link('inventory/purchase_order/material/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
        </div>
          <input type="hidden" name= "po_id" value="{{$po_id}}">
        </div>

        {!! Form::close() !!}

        
      </div>

    </div>
  </div>

  <script type="text/javascript">

 /*for duplication*/

  /*material type*/
  $('#matType').change(function(){

    $('#materialTypeId').val($(this).val());
    $('#materialTypeName').val($('option:selected',this).text());
  });

  /*material subtype*/
  $('#matSubType').change(function(){
    $('#materialSubTypeId').val($(this).val());
    $('#materialSubTypeName').val($('option:selected',this).text());
  });

  /*material names*/

  $('#matName').change(function(){
    $('#materialId').val($(this).val());
    $('#materialName').val($('option:selected',this).text());
  });

  /*quantity*/
  $('#quntityRequired').change(function(){
    $('#Quantity').val($(this).val());
  });

  

  var i=0;

  function duplicatematerial()
  {
    var materialtype_id=document.getElementById('materialTypeId').value;
    var materialtype_name=document.getElementById('materialTypeName').value;

    var materialsubtype_id=document.getElementById('materialSubTypeId').value;
    var materialsubtype_name=document.getElementById('materialSubTypeName').value;

    var material_id=document.getElementById('materialId').value;
    var material_name=document.getElementById('materialName').value;

    var qty=document.getElementById('Quantity').value;



    if(materialtype_name!='' && materialsubtype_name!='' && material_name!='')
    {
      var original=document.getElementById('duplicater');
      original.style.display = "block";

      document.getElementById('materialTypeId').value=materialtype_id;
      document.getElementById('materialTypeName').value=materialtype_name;

      document.getElementById('materialSubTypeId').value=materialsubtype_id;
      document.getElementById('materialSubTypeName').value=materialsubtype_name;

      document.getElementById('materialId').value=material_id;
      document.getElementById('materialName').value=material_name;


    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicater" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);
    original.style.display = "none";

    document.getElementById('materialTypeId').value="";
    document.getElementById('materialTypeName').value="";

    document.getElementById('materialSubTypeId').value="";
    document.getElementById('materialSubTypeName').value="";

    document.getElementById('materialId').value="";
    document.getElementById('materialName').value="";



    /*to make dropdown blank*/
    document.getElementById('matType').value="select";
    document.getElementById('matSubType').value="select";
    document.getElementById('matName').value="select";
    document.getElementById('quntityRequired').value="";

    var nameElemntmattypeid = document.getElementById("duplicater"+i).getElementsByClassName('mat_type_id')[0];
    nameElemntmattypeid.setAttribute("name","material_type[]");

    var nameElemntmatsubtypeid = document.getElementById("duplicater"+i).getElementsByClassName('mat_subtype_id')[0];
    nameElemntmatsubtypeid.setAttribute("name","material_subtype[]");

    var nameElemntmatid = document.getElementById("duplicater"+i).getElementsByClassName('mat_id')[0];
    nameElemntmatid.setAttribute("name","mat_name[]");

    var nameElemntmatid = document.getElementById("duplicater"+i).getElementsByClassName('qty')[0];
    nameElemntmatid.setAttribute("name","quantity[]");
  }

}

function removeduplicate(element)
{
    element=element.parentNode.parentNode.parentNode;//gets the id of the parent
    element.parentNode.removeChild(element);
  }

  
  /*ajax call*/


  $(document).ready(function(){

   $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

   $('#matType').change(function(){

    var material_id=$(this).val();
    var matsubtypeselect=document.getElementById('matSubType');



    if(material_id != "")
    {
    $(material_id).val(0);
    $(matsubtypeselect).empty().append("<option value>Select</option>");
    $('#matName').empty().append("<option value>Select</option>");

     $.ajax({
      method: "POST",
      url: '{{url("inventory/purchaseorder/materialsubtype/")}}' + "/" + material_id,
      success: function(data){

        for(var i in data)
        {
          $(matsubtypeselect).append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
        }
      }
    }); 
   }
   else
   {
    $(material_id).val(0);
    $(matsubtypeselect).empty().append("<option value>Select</option>");
    $('#matName').empty().append("<option value>Select</option>");
  }


});

   $('#matSubType').change(function(){

    var materialsubtype_id=$(this).val();
    var matnameselect=document.getElementById('matName');


    /*$(matsubtypeselect).empty();*/

    if(materialsubtype_id != "")
    {

    $(materialsubtype_id).val(0);
    $(matnameselect).empty().append("<option value>Select</option>");

     $.ajax({
      method: "POST",
      url: '{{url("inventory/purchaseorder/materialname/")}}' + "/" + materialsubtype_id,
      success: function(data){
        console.log("data"+data);
        for(var i in data)
        {
          $(matnameselect).append('<option value=' + data[i]['id'] + '>' + data[i]['material_name'] + '</option>');
        }
      }
    }); 
   }
   else
   {
    $(materialsubtype_id).val(0);
    $(matnameselect).empty().append("<option value>Select</option>");
  }


});



 });

</script>

@stop