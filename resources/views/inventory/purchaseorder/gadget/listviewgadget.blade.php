@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
 <div id="page-wrapper">
   <br>
   <div class="">
    <div class="panel panel-info col-xs-12">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/material/listview') }}">Material</a></li>
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/instrument/listview') }}">Instrument</a></li> 
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/gadget/listview') }}">Gadget</a></li>
      <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchase_order/machine/listview') }}">Machine</a></li> 
    </ul>
  </div>
</div>

<div class="col-xs-12">
  <a href="{{url('inventory/purchase_order/gadget/add',[$po_id])}}" class="btn btn-success pull-right col-xs-2">Place Order</a>
</div>
<br><br><br><br><br>
<div class="panel panel-default filterable">
 <table class="table">
  <thead>
    
    <tr class="filters">
      <th><input type="text" class="form-control" placeholder="PO Number" disabled></th>
      <th><input type="text" class="form-control" placeholder="Vendor Name" disabled></th>
      <th><input type="text" class="form-control" placeholder="Expected Delivery date" disabled></th>
      <th><input type="text" class="form-control" placeholder="Chalan Number (Latest*)" disabled></th>
      <th><input type="text" class="form-control" placeholder="Bill No" disabled></th>
      <th><input type="text" class="form-control" placeholder="" disabled></th>
      <th><input type="text" class="form-control" placeholder="" disabled></th>
      <th><input type="text" class="form-control" placeholder="" disabled></th>
      <th><input type="text" class="form-control" placeholder="" disabled></th>
    </tr>
  </thead>
  <tbody>
    @foreach($po_gadget as $gadget)
    <tr>
      <td>{{$gadget->po_id}}</td>
      <td>{{$gadget->name}}</td>
      <td>{{$gadget->expected_date_of_delivery}}</td>
      <td></td>
      <td>{{$gadget->bill_number}}</td>

      @if($gadget->bill_number == null)
      <td>
        <a href="{{url('inventory/purchase_order/gadget/add',[$gadget->po_id])}}" class="btn btn-primary btn-xs">Edit</a>
      </td>
      <td>
        <a href="{{url('inventory/purchase_order/gadget/view',[$gadget->po_id])}}" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-eye-open">View</span></a>
      </td>
      @else
      <td>
        <a href="#" class="btn .btn-defualt btn-xs">Edit</a>
      </td>
      <td>
        <a href="{{url('inventory/purchase_order/gadget/view',[$gadget->po_id])}}" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-eye-open">View</span></a>
      </td>
      @endif

      <!-- <td><a href="{{url('inventory/purchase_order/gadget/detailsadd',[$gadget->po_id])}}">Edit</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="{{url('inventory/purchase_order/gadget/viewdetailsadd',[$gadget->po_id])}}">View</a></td> -->
      <td><a href="{{url('inventory/purchase_order/gadget/generate_delivery',[$gadget->po_id])}}">Generate Delivery</a></td>
      <td><a href="{{url('inventory/purchase_order/gadget/generate_billing/edit',[$gadget->po_id])}}">Generate Billing</a></td>
    </tr>
    @endforeach
  </tbody>
</table>

</div>


</div>
</div>

@stop