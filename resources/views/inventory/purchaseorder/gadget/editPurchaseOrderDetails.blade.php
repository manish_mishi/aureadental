@extends('layouts.menuNav')

@section('title')
Inventory->purachase Order-> Add Purchase Order
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
  <div id="page-wrapper">

   <br>

   <div class="">
    <!-- <div class="panel panel-info col-xs-12">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/purchaseorderAdd',[$po_id]) }}">Material</a></li>
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/instrument/detailsadd',[$po_id]) }}">Instrument</a></li> 
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchaseorder/gadget/detailsadd',[$po_id]) }}">Gadget</a></li>
      <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchaseorder/machine/detailsadd',[$po_id]) }}">Machine</a></li> 
    </ul>
  </div> -->
  <div class="panel panel-warning col-xs-12">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/gadget/add',[$po_id]) }}">Details</a></li>
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/gadget/vendor_details',[$po_id]) }}">Vendor Detail</a></li> 
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/gadget/generate_delivery',[$po_id]) }}">Generate delivery</a></li>
    <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchase_order/gadget/generate_billing',[$po_id]) }}">Generate Billing</a></li> 
  </ul>
</div>
</div>

  <div class="container">

{!! Form::open(array('route'=>'updatePurchaseOrderGadget')) !!}

<div><!-- combine div for duplication start -->
    <div class="row">
    <div class="col-xs-2">
     <label>Gadget Type<label>
     </div>
     <div class="col-xs-3">
      
       <select class="form-control" name="gadget_type[]" id="gadType">
        <option value>select</option>
        @foreach($gadget_type as $gadget)
        <option value="{{$gadget->id}}">{{$gadget->name}}</option>
        @endforeach()
      </select>
    </div>

    <div class="col-xs-3">
      <label>Gadget Sub-type<label>
      </div>
      <div class="col-xs-3">
        
       <select class="form-control" name="gadget_subtype[]" id="gadSubType">
        <option value>select</option>
      </select>
    </div>
  </div><br>

  <div class="row">
    <div class="col-xs-2">
      <label> Gadget Name<label>
      </div>
      <div class="col-xs-3">
        
        <select class="form-control" name="gadget_name[]" id="gadName">
          <option value>select</option>
        </select>
      </div>

      <div class="col-xs-3">
       <label>Quantity Required<label>
       </div>
       <div class="col-xs-3">
        
         {!! Form::text('quantity[]',null,array('class'=>'form-control','id'=>'quntityRequired')) !!}
       </div>
       <div class="col-xs-1">
            {!! Form::button(null,array('class'=>'btn btn-md btn-info glyphicon glyphicon-plus','onClick'=>'duplicategadget()')) !!}
        </div>
     </div><br>

     <!-- duplication start -->

      <div id="duplicater" style="display:none;">

        <div class="row">   
          <div class="col-xs-2"><label>Gadget Type</label></div>
          <div class="col-xs-3">
            <input type="text" class="form-control gad_type_name" id="gadgetTypeName" class="form-control" Readonly/><br>
            <input type="hidden" class="form-control gad_type_id" id="gadgetTypeId" class="form-control"/>
          </div>
          <div class="col-xs-3"><label>Gadget Sub-Type</div>
          <div class="col-xs-3">
            <input type="text" class="form-control gad_subtype_name" id="gadgetSubTypeName" class="form-control" Readonly/><br>
            <input type="hidden" class="form-control gad_subtype_id" id="gadgetSubTypeId" class="form-control"/>
          </div>
        </div>

        <div class="row">   
          <div class="col-xs-2"><label>Gadget Name</label></div>
          <div class="col-xs-3">
            <input type="text" class="form-control gad_name" id="gadgetName" class="form-control" Readonly/><br>
            <input type="hidden" class="form-control gad_id" id="gadgetId" class="form-control"/>
          </div>
          <div class="col-xs-3"><label>Quantity Required</div>
          <div class="col-xs-3">
            <input type="text" class="form-control qty" id="Quantity" class="form-control" Readonly/><br>
          </div>
          <div class="col-xs-1">
            {!! Form::button(null,array('id' =>'delete_btn','class'=>'btn btn-md btn-danger glyphicon glyphicon-trash','onClick'=>'removeduplicate(this)')) !!}
          </div>
        </div>
               
      </div>
      <!-- duplication end-->  


     
</div><!-- combine div for duplication end -->

<!-- show current values open-->

  @foreach($curr_gadget as $details)
  <div class="row">
    <div class="col-xs-2">
     <label>Gadget Type<label>
     </div>
     <div class="col-xs-3">
      {!! Form::text('null',$details->gadget_type,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
    </div>

    <div class="col-xs-3">
      <label>Gadget Sub-type<label>
      </div>
      <div class="col-xs-3">
        {!! Form::text('null',$details->gadget_subtype,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
      </div>
    </div><br>

    <div class="row">
      <div class="col-xs-2">
        <label> Gadget Name<label>
        </div>
        <div class="col-xs-3">
          {!! Form::text('null',$details->gadgetname,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
        </div>

        <div class="col-xs-3">
         <label>Quantity Required<label>
         </div>
         <div class="col-xs-3">
          {!! Form::text('null',$details->quantity,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
        </div>
        <div class="col-xs-1">
          <a href="{{url('inventory/purchase_order/gadget/delete',[$details->gadget_name_id,$po_id])}}"><button type="button" class="btn btn-md btn-danger glyphicon glyphicon-trash"></button></a>
        </div>
      </div><br>

      @endforeach()
      <!-- current values close -->


  <div class="row">
    <div class="col-xs-2">
      <label>Date Of Order<label>
      </div>
      <div class="col-xs-3">
        <input data-provide="datepicker" data-date-format="dd/mm/yy" id="dateoforder" class="form-control" name="dateoforder" value="{{$curr_po['0']->date_of_order}}">
      </div>

      <div class="col-xs-3">
       <label>Expected Delivery date<label>
       </div>
       <div class="col-xs-3">
         <input data-provide="datepicker" data-date-format="dd/mm/yy" id="expecteddateoforder" class="form-control" name="expecteddateoforder" value="{{$curr_po['0']->expected_date_of_delivery}}">
       </div>
     </div><br>

     <div class="row">
      <div class="col-xs-2">
        <label>Order status<label>
        </div>
        <div class="col-xs-3">
          <select class="form-control" name="order_status_id">
            <option value="{{$curr_po['0']->purchase_order_status_id}}">{{$curr_po['0']->name}}</option>
            @foreach($order_status as $order)
            <option value="{{$order->id}}">{{$order->name}}</option>
            @endforeach
          </select>
        </div>    
      </div><br>

      <div class="col-lg-12">
          <div class="col-xs-8">
          </div>
        <div class="col-xs-2">
          <button type="submit" class="btn btn-warning pull-right">&#x2714; Save</button>
        </div>
          <div class="col-xs-2">
            {!! HTML::link('inventory/purchase_order/gadget/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
            
        </div>
          <input type="hidden" name= "po_id" value="{{$po_id}}">
        </div>

{!! Form::close() !!}
    
  </div>
</div>
</div>

<script type="text/javascript">


  /*for duplication*/

  
  $('#gadType').change(function(){

    $('#gadgetTypeId').val($(this).val());
    $('#gadgetTypeName').val($('option:selected',this).text());
  });

  
  $('#gadSubType').change(function(){
    $('#gadgetSubTypeId').val($(this).val());
    $('#gadgetSubTypeName').val($('option:selected',this).text());
  });

  

  $('#gadName').change(function(){
    $('#gadgetId').val($(this).val());
    $('#gadgetName').val($('option:selected',this).text());
  });

  /*quantity*/
  $('#quntityRequired').change(function(){
    $('#Quantity').val($(this).val());
  });

  

  var i=0;

  function duplicategadget()
  {
    var gadgettype_id=document.getElementById('gadgetTypeId').value;
    var gadgettype_name=document.getElementById('gadgetTypeName').value;

    var gadgetsubtype_id=document.getElementById('gadgetSubTypeId').value;
    var gadgetsubtype_name=document.getElementById('gadgetSubTypeName').value;

    var gadget_id=document.getElementById('gadgetId').value;
    var gadget_name=document.getElementById('gadgetName').value;

    var qty=document.getElementById('Quantity').value;



    if(gadgettype_name!='' && gadgetsubtype_name!='' && gadget_name!='')
    {
      var original=document.getElementById('duplicater');
      original.style.display = "block";

      document.getElementById('gadgetTypeId').value=gadgettype_id;
      document.getElementById('gadgetTypeName').value=gadgettype_name;

      document.getElementById('gadgetSubTypeId').value=gadgetsubtype_id;
      document.getElementById('gadgetSubTypeName').value=gadgetsubtype_name;

      document.getElementById('gadgetId').value=gadget_id;
      document.getElementById('gadgetName').value=gadget_name;


    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicater" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);
    original.style.display = "none";

    document.getElementById('gadgetTypeId').value="";
    document.getElementById('gadgetTypeName').value="";

    document.getElementById('gadgetSubTypeId').value="";
    document.getElementById('gadgetSubTypeName').value="";

    document.getElementById('gadgetId').value="";
    document.getElementById('gadgetName').value="";



    /*to make dropdown blank*/
    document.getElementById('gadType').value="select";
    document.getElementById('gadSubType').value="select";
    document.getElementById('gadName').value="select";
    document.getElementById('quntityRequired').value="";

    var nameElemntgadtypeid = document.getElementById("duplicater"+i).getElementsByClassName('gad_type_id')[0];
    nameElemntgadtypeid.setAttribute("name","gadget_type[]");

    var nameElemntgadsubtypeid = document.getElementById("duplicater"+i).getElementsByClassName('gad_subtype_id')[0];
    nameElemntgadsubtypeid.setAttribute("name","gadget_subtype[]");

    var nameElemntgadid = document.getElementById("duplicater"+i).getElementsByClassName('gad_id')[0];
    nameElemntgadid.setAttribute("name","gadget_name[]");

    var nameElemntqty = document.getElementById("duplicater"+i).getElementsByClassName('qty')[0];
    nameElemntqty.setAttribute("name","quantity[]");
  }

}

function removeduplicate(element)
{
    element=element.parentNode.parentNode.parentNode;//gets the id of the parent
    element.parentNode.removeChild(element);
  }

 /*ajax call*/


  $(document).ready(function()
{

   $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

   $('#gadType').change(function()
   {

    var gadget_id=$(this).val();
    var gadgetsubtypeselect=document.getElementById('gadSubType');



    if(gadget_id != " ")
    {

      $(gadget_id).val(0);
    $(gadgetsubtypeselect).empty().append("<option value>Select</option>");
    $('#gadName').empty().append("<option value>Select</option>");

     $.ajax({
      method: "POST",
      url: '{{url("inventory/purchaseorder/gadgetsubtype/")}}' + "/" + gadget_id,
      success: function(data){
        console.log("data"+data);
        for(var i in data)
        {
          $(gadgetsubtypeselect).append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
        }
      }
    }); 
   }
   else
   {
    $(gadget_id).val(0);
    $(gadgetsubtypeselect).empty().append("<option value>Select</option>");
    $('#gadName').empty().append("<option value>Select</option>");
   }

  });

   $('#gadSubType').change(function()
   {

    var gadget_subtypeid=$(this).val();
    var gadgetnameselect=document.getElementById('gadName');

    

    if(gadget_subtypeid != " ")
    {

      $(gadget_subtypeid).val(0);
    $(gadgetnameselect).empty().append("<option value>Select</option>");

     $.ajax({
      method: "POST",
      url: '{{url("inventory/purchaseorder/gadgetname/")}}' + "/" + gadget_subtypeid,
      success: function(data){
        console.log("data"+data);
        for(var i in data)
        {
          $(gadgetnameselect).append('<option value=' + data[i]['id'] + '>' + data[i]['gadget_name'] + '</option>');
        }
      }
    }); 
   }
   else
   {
    $(gadget_subtypeid).val(0);
    $(gadgetnameselect).empty().append("<option value>Select</option>");
   }


  });
   

});


</script>

@stop