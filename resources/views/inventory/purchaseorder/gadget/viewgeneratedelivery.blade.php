@extends('layouts.menuNav')

@section('title')
Inventory->purachase Order-> Add
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
  <div id="page-wrapper">

   <br>

   <div class="">
   
  <div class="panel panel-warning col-xs-12">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation" ><a href="{{ url('inventory/purchase_order/gadget/view',[$po_id]) }}">Details</a></li>
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/gadget/vendor_details/view',[$po_id]) }}">Vendor Detail</a></li> 
    <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/gadget/generate_delivery/view',[$po_id]) }}">Generate delivery</a></li>
    <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchase_order/gadget/generate_billing/view',[$po_id]) }}">Generate Billing</a></li> 
  </ul>
</div>
</div>

<div class="container">

 @foreach($curr_delivery as $delivery)

 <div class="row">
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
      {!! Form::label('Challan Number', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
      {!! Form::label('Date of Delivery', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
      {!! Form::label('Delivery Location', null, array()) !!}
    </div>
    
  </div><br>

  <div class="row">
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
      {!! Form::text(null,$delivery->chalan_number,array('class'=>'form-control','id'=>'challanNumberId','readonly')) !!}
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dateofDelivery" class="form-control"  value="{{$delivery->delivery_date}}" Readonly>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
      {!! Form::text(null,$delivery->delivery_location,array('class'=>'form-control','id'=>'deliveryLocation','readonly')) !!}
    </div>
    
  </div><br>
  @endforeach

<div class="col-lg-12">
          {!! HTML::link('inventory/purchase_order/gadget/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
        </div>
</div>

  </div>
</div>


  
   @stop