@extends('layouts.menuNav')

@section('title')
Inventory->purachase Order->View
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
  <div id="page-wrapper">

   <br>

   <div class="">
    
  <div class="panel panel-warning col-xs-12">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/machine/view',[$po_id]) }}">Details</a></li>
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/machine/vendor_details/view',[$po_id]) }}">Vendor Detail</a></li> 
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/machine/generate_delivery/view',[$po_id]) }}">Generate delivery</a></li>
    <li style="width:24%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/machine/generate_billing/view',[$po_id]) }}">Generate Billing</a></li> 
  </ul>
</div>
</div>

<div class="container">

  @if($curr_bill == null)
  <div class="row">

    <label class="col-xs-3">Bill number:</label>
    <div class="col-xs-3">
        <input type="text" class="form-control" value="" name="bill_no" id="billNo" Readonly>
    </div>

    <label class="col-xs-3">Billing Status:</label>
    <div class="col-xs-3">
      <input type="text" class="form-control" value="" name="bill_no" id="billNo" Readonly>
        
    </div>

</div><br>

<div class="row">
    <label class="col-xs-3">Date Of Billing:</label>
    <div class="col-xs-3"><input data-provide="datepicker" data-date-format="dd/mm/yy" id="dateofbilling" class="form-control" name="dateofbilling" value="" Readonly>
    </div>
    <label class="col-xs-3">Amount:</label>
    <div class="col-xs-3"><input type="text" class="form-control" name="amt" value="" Readonly>
    </div>

</div><br><br><br>

<div class="row">
    <label class="col-xs-3">Date Of Payment:</label>
    <div class="col-xs-3"><input data-provide="datepicker" data-date-format="dd/mm/yy" id="dateofpayment" class="form-control" name="dateofpayment" value="" Readonly>
    </div>
    <label class="col-xs-3">Advance:</label>
    <div class="col-xs-3"><input type="text" class="form-control" name="advance" value="" Readonly>
    </div>

</div><br>

<div class="row">
    
    <div class="col-xs-6"></div>
    <label class="col-xs-3">Discount:</label>
    <div class="col-xs-3"><input type="text" class="form-control" name="discount" value="" Readonly>
    </div>

</div><br>
<div class="row">

    <div class="col-xs-6">
    </div>
    <label class="col-xs-3">Final Cost:</label>
    <div class="col-xs-3"><input type="text" class="form-control" name="final_cost" value="" Readonly>
    </div>

</div><br>

  @else
  <div class="row">

    <label class="col-xs-3">Bill number:</label>
    <div class="col-xs-3">
        <input type="text" class="form-control" value="{{$curr_bill['0']->bill_number}}" name="bill_no" id="billNo" Readonly>
    </div>

    <label class="col-xs-3">Billing Status:</label>
    <div class="col-xs-3">
      <input type="text" class="form-control" value="{{$curr_bill['0']->name}}" name="bill_no" id="billNo" Readonly>
        
    </div>

</div><br>

<div class="row">
    <label class="col-xs-3">Date Of Billing:</label>
    <div class="col-xs-3"><input data-provide="datepicker" data-date-format="dd/mm/yy" id="dateofbilling" class="form-control" name="dateofbilling" value="{{$curr_bill['0']->date_of_billing}}" Readonly>
    </div>
    <label class="col-xs-3">Amount:</label>
    <div class="col-xs-3"><input type="text" class="form-control" name="amt" value="{{$curr_bill['0']->amount}}" Readonly>
    </div>

</div><br><br><br>

<div class="row">
    <label class="col-xs-3">Date Of Payment:</label>
    <div class="col-xs-3"><input data-provide="datepicker" data-date-format="dd/mm/yy" id="dateofpayment" class="form-control" name="dateofpayment" value="" Readonly>
    </div>
    <label class="col-xs-3">Advance:</label>
    <div class="col-xs-3"><input type="text" class="form-control" name="advance" value="{{$curr_bill['0']->advance}}" Readonly>
    </div>

</div><br>

<div class="row">
    
    <div class="col-xs-6"></div>
    <label class="col-xs-3">Discount:</label>
    <div class="col-xs-3"><input type="text" class="form-control" name="discount" value="{{$curr_bill['0']->discount}}" Readonly>
    </div>

</div><br>
<div class="row">

    <div class="col-xs-6">
    </div>
    <label class="col-xs-3">Final Cost:</label>
    <div class="col-xs-3"><input type="text" class="form-control" name="final_cost" value="{{$curr_bill['0']->discount}}" Readonly>
    </div>

</div><br>
  @endif

        <div class="col-lg-12">
          {!! HTML::link('inventory/purchase_order/dashboard', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
        </div>

        

        
      </div>

    </div>
  </div>

  

@stop