@extends('layouts.menuNav')

@section('title')
Inventory->purachase Order-> Add
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
  <div id="page-wrapper">

   <br>

   <div class="">
   
  <div class="panel panel-warning col-xs-12">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/machine/view',[$po_id]) }}">Details</a></li>
    <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/machine/vendor_details/view',[$po_id]) }}">Vendor Detail</a></li> 
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/machine/generate_delivery/view',[$po_id]) }}">Generate delivery</a></li>
    <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchase_order/machine/generate_billing/view',[$po_id]) }}">Generate Billing</a></li> 
  </ul>
</div>
</div>

<div class="container">

<div class="row">
    
    <label class="col-xs-3">Vendor Name :</label>
    <div class="col-xs-3">
      <input type="text" class="form-control" value="{{$vendordetails['0']->name}}" name="bill_no" id="billNo" Readonly>
    </div>
    
      
    </div>
</div><br>

<div class="row">
    
    
    <div class="col-xs-6">
      <div class="panel panel-default filterable">
       <table class="table">
            <thead>
              
              <tr class="filters">
                <th><input type="text" class="form-control" placeholder="Machine Name" disabled></th>
                <th><input type="text" class="form-control" placeholder="Rate" disabled></th>
                <th><input type="text" class="form-control" placeholder="Quantity" disabled></th>
                <th><input type="text" class="form-control" placeholder="Amount" disabled></th>
                
                
              </tr>
            </thead>
            <tbody>
              @foreach($vendordetails as $vendor)
              <tr>
                <td>{{$vendor->machine_name}}</td>
                <td>{{$vendor->rate}}</td>
                <td>{{$vendor->quantity}}</td>
                <td>{{$vendor->amount}}</td>
              </tr>
              @endforeach()
            </tbody>
            <tr>
            </tr>
          </table>
    </div>
    </div>
    

    
</div><br>

<div class="row">
  <label class="col-xs-3">Discount :</label>
    <div class="col-xs-3">
      <input type="text" class="form-control" value="{{$vendordetails['0']->discount}}" name="bill_no" id="billNo" Readonly>

    </div>
</div><br>

<div class="row">
  <label class="col-xs-3">Final Amount :</label>
    <div class="col-xs-3">
      <input type="text" class="form-control" value="{{$vendordetails['0']->final_amount}}" name="bill_no" id="billNo" Readonly>

    </div>
</div><br>

<div class="col-lg-12">
          {!! HTML::link('inventory/purchase_order/machine/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
        </div>
      </div>





  


    </div>
  </div>
</div>

  
   @stop