@extends('layouts.menuNav')

@section('title')
Inventory->purachase Order-> Add Vendor Details
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
  <div id="page-wrapper">

   <br>

   <div class="">
    <div class="panel panel-info col-xs-12">
     <!-- <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/purchaseorderAdd',[$po_id]) }}">Material</a></li>
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/instrument/detailsadd',[$po_id]) }}">Instrument</a></li> 
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/gadget/detailsadd',[$po_id]) }}">Gadget</a></li>
      <li style="width:24%" role="presentation" class="active"><a href="{{ url('inventory/purchaseorder/machine/detailsadd',[$po_id]) }}">Machine</a></li> 
    </ul> -->
  </div>
  <div class="panel panel-warning col-xs-12">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/machine/add',[$po_id]) }}">Details</a></li>
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/machine/vendor_details',[$po_id]) }}">Vendor Detail</a></li> 
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/machine/generate_delivery',[$po_id]) }}">Generate delivery</a></li>
    <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchase_order/machine/generate_billing',[$po_id]) }}">Generate Billing</a></li> 
  </ul>
</div>
</div>

<div class="container">
  {!! Form::open(array('route'=>'updatePurchaseOrderMachine')) !!}

<div><!-- comdine div for duplication start -->
  <div class="row">
    <div class="col-xs-2">
     <label>Machine Type<label>
     </div>
     <div class="col-xs-3">
      
      <select class="form-control" name="machine_type[]" id="machType">
        <option value>select</option>
        @foreach($machine_type as $machine)
        <option value="{{$machine->id}}">{{$machine->name}}</option>
        @endforeach
      </select>
    </div>

    <div class="col-xs-3">
      <label>Machine Sub-type<label>
      </div>
      <div class="col-xs-3">
        
        <select class="form-control" name="machine_subtype[]" id="machSubType">
          <option value>select</option>
        </select>
      </div>
    </div><br>

    <div class="row">
      <div class="col-xs-2">
        <label> Machine Name<label>
        </div>
        <div class="col-xs-3">
          
          <select class="form-control" name="machine_name[]" id="machName">
            <option value>select</option>
          </select>
        </div>

        <div class="col-xs-3">
         <label>Quantity Required<label>
         </div>
         <div class="col-xs-3">
          <!-- quantity[] after cloning -->
          {!! Form::text('quantity[]',null,array('class'=>'form-control','required','id'=>'quntityRequired')) !!}
        </div>
        <div class="col-xs-1">
            {!! Form::button(null,array('class'=>'btn btn-md btn-info glyphicon glyphicon-plus','onClick'=>'duplicatemachine()')) !!}
        </div>
      </div><br>

      

    <!-- duplication start -->

      <div id="duplicater" style="display:none;">

        <div class="row">   
          <div class="col-xs-2"><label>Machine Type</label></div>
          <div class="col-xs-3">
            <input type="text" class="form-control mach_type_name" id="machineTypeName" class="form-control" Readonly/><br>
            <input type="hidden" class="form-control mach_type_id" id="machineTypeId" class="form-control"/>
          </div>
          <div class="col-xs-3"><label>Machine Sub-Type</div>
          <div class="col-xs-3">
            <input type="text" class="form-control mach_subtype_name" id="machineSubTypeName" class="form-control" Readonly/><br>
            <input type="hidden" class="form-control mach_subtype_id" id="machineSubTypeId" class="form-control"/>
          </div>
        </div>

        <div class="row">   
          <div class="col-xs-2"><label>Machine Name</label></div>
          <div class="col-xs-3">
            <input type="text" class="form-control mach_name" id="machineName" class="form-control" Readonly/><br>
            <input type="hidden" class="form-control mach_id" id="machineId" class="form-control"/>
          </div>
          <div class="col-xs-3"><label>Quantity Required</div>
          <div class="col-xs-3">
            <input type="text" class="form-control qty" id="Quantity" class="form-control" Readonly/><br>
          </div>
          <div class="col-xs-1">
            {!! Form::button(null,array('id' =>'delete_btn','class'=>'btn btn-md btn-danger glyphicon glyphicon-trash','onClick'=>'removeduplicate(this)')) !!}
          </div>
        </div>
               
      </div>
      <!-- duplication end-->  

</div><!-- combined div duplication end -->



    <!-- show current values open-->

  @foreach($curr_machine as $details)
  <div class="row">
    <div class="col-xs-2">
     <label>Machine Type<label>
     </div>
     <div class="col-xs-3">
      {!! Form::text('null',$details->machine_type,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
    </div>

    <div class="col-xs-3">
      <label>Machine Sub-type<label>
      </div>
      <div class="col-xs-3">
        {!! Form::text('null',$details->machine_subtype,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
      </div>
    </div><br>

    <div class="row">
      <div class="col-xs-2">
        <label>Machine Name<label>
        </div>
        <div class="col-xs-3">
          {!! Form::text('null',$details->machinename,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
        </div>

        <div class="col-xs-3">
         <label>Quantity Required<label>
         </div>
         <div class="col-xs-3">
          {!! Form::text('null',$details->quantity,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
        </div>
        <div class="col-xs-1">
          <a href="{{url('inventory/purchase_order/machine/delete',[$details->machine_name_id,$po_id])}}"><button type="button" class="btn btn-md btn-danger glyphicon glyphicon-trash"></button></a>
        </div>
      </div><br>

      @endforeach()
      <!-- current values close -->

    <div class="row">
      <div class="col-xs-2">
        <label>Date Of Order<label>
        </div>
        <div class="col-xs-3">
          <input data-provide="datepicker" data-date-format="dd/mm/yy" id="dateoforder" class="form-control" name="dateoforder" value="{{$curr_po['0']->date_of_order}}">
        </div>

        <div class="col-xs-3">
         <label>Expected Delivery date<label>
         </div>
         <div class="col-xs-3">
           <input data-provide="datepicker" data-date-format="dd/mm/yy" id="expecteddateoforder" class="form-control" name="expecteddateoforder" value="{{$curr_po['0']->expected_date_of_delivery}}">
         </div>
       </div><br>

       <div class="row">
        <div class="col-xs-2">
          <label>Order status<label>
        </div>
          <div class="col-xs-3">
            <select class="form-control" name="order_status_id">
              <option value="{{$curr_po['0']->purchase_order_status_id}}">{{$curr_po['0']->name}}</option>
              @foreach($order_status as $order)
            <option value="{{$order->id}}">{{$order->name}}</option>
            @endforeach
            </select>
          </div>    
        </div><br>

        <div class="col-lg-12">
          <div class="col-xs-8">
          </div>
        <div class="col-xs-2">
          <button type="submit" class="btn btn-warning pull-right">&#x2714; Save</button>
        </div>
          <div class="col-xs-2">
            {!! HTML::link('inventory/purchase_order/machine/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
            
        </div>
          <input type="hidden" name= "po_id" value="{{$po_id}}">
        </div>
{!! Form::close() !!}

      </div>
    </div>
  </div>

  <script type="text/javascript">

  /*for duplication*/

  
  $('#machType').change(function(){

    $('#machineTypeId').val($(this).val());
    $('#machineTypeName').val($('option:selected',this).text());
  });

  
  $('#machSubType').change(function(){
    $('#machineSubTypeId').val($(this).val());
    $('#machineSubTypeName').val($('option:selected',this).text());
  });

  

  $('#machName').change(function(){
    $('#machineId').val($(this).val());
    $('#machineName').val($('option:selected',this).text());
  });

  /*quantity*/
  $('#quntityRequired').change(function(){
    $('#Quantity').val($(this).val());
  });

  

  var i=0;

  function duplicatemachine()
  {
    var machinetype_id=document.getElementById('machineTypeId').value;
    var machinetype_name=document.getElementById('machineTypeName').value;

    var machinesubtype_id=document.getElementById('machineSubTypeId').value;
    var machinesubtype_name=document.getElementById('machineSubTypeName').value;

    var machine_id=document.getElementById('machineId').value;
    var machine_name=document.getElementById('machineName').value;

    var qty=document.getElementById('Quantity').value;



    if(machinetype_name!='' && machinesubtype_name!='' && machine_name!='')
    {
      var original=document.getElementById('duplicater');
      original.style.display = "block";

      document.getElementById('machineTypeId').value=machinetype_id;
      document.getElementById('machineTypeName').value=machinetype_name;

      document.getElementById('machineSubTypeId').value=machinesubtype_id;
      document.getElementById('machineSubTypeName').value=machinesubtype_name;

      document.getElementById('machineId').value=machine_id;
      document.getElementById('machineName').value=machine_name;


    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicater" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);
    original.style.display = "none";

    document.getElementById('machineTypeId').value="";
    document.getElementById('machineTypeName').value="";

    document.getElementById('machineSubTypeId').value="";
    document.getElementById('machineSubTypeName').value="";

    document.getElementById('machineId').value="";
    document.getElementById('machineName').value="";



    /*to make dropdown blank*/
    document.getElementById('machType').value="select";
    document.getElementById('machSubType').value="select";
    document.getElementById('machName').value="select";
    document.getElementById('quntityRequired').value="";

    var elementtypeid = document.getElementById("duplicater"+i).getElementsByClassName('mach_type_id')[0];
    elementtypeid.setAttribute("name","machine_type[]");


    var nameElemntmachsubtypeid = document.getElementById("duplicater"+i).getElementsByClassName('mach_subtype_id')[0];
    nameElemntmachsubtypeid.setAttribute("name","machine_subtype[]");

    var nameElemntmachid = document.getElementById("duplicater"+i).getElementsByClassName('mach_id')[0];
    nameElemntmachid.setAttribute("name","machine_name[]");

    var nameElemntqty = document.getElementById("duplicater"+i).getElementsByClassName('qty')[0];
    nameElemntqty.setAttribute("name","quantity[]");
  }

}

function removeduplicate(element)
{
    element=element.parentNode.parentNode.parentNode;//gets the id of the parent
    element.parentNode.removeChild(element);
  }

  /*ajax call*/


  $(document).ready(function(){

   $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

   $('#machType').change(function(){

    var machine_id=$(this).val();
    var machineSubtypeSelect=document.getElementById('machSubType');



    if(machine_id != "")
    {

      $(machine_id).val(0);
      $(machineSubtypeSelect).empty().append("<option value>Select</option>");
      $('#machName').empty().append("<option value>Select</option>");

     $.ajax({
      method: "POST",
      url: '{{url("inventory/purchaseorder/machinesubtype")}}' + "/" + machine_id,
      success: function(data){
        console.log("data"+data);
        for(var i in data)
        {
          $(machineSubtypeSelect).append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
        }
      }
    }); 
   }
   else
   {
    $(machine_id).val(0);
      $(machineSubtypeSelect).empty().append("<option value>Select</option>");
      $('#machName').empty().append("<option value>Select</option>");
  }


});

   $('#machSubType').change(function(){

    var machine_subtypeid=$(this).val();
    var machineNameSelect=document.getElementById('machName');



    if(machine_subtypeid != "")
    {

      $(machine_subtypeid).empty().append("<option value>Select</option>");
      $(machineNameSelect).empty().append("<option value>Select</option>");

     $.ajax({
      method: "POST",
      url: '{{url("inventory/purchaseorder/machinename")}}' + "/" + machine_subtypeid,
      success: function(data){
        console.log("data"+data);
        for(var i in data)
        {
          $(machineNameSelect).append('<option value=' + data[i]['id'] + '>' + data[i]['machine_name'] + '</option>');
        }
      }
    }); 
   }
   else
   {
      $(machine_subtypeid).empty().append("<option value>Select</option>");
      $(machineNameSelect).empty().append("<option value>Select</option>");
  }


});


 });


</script>
@stop