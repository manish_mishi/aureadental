@extends('layouts.menuNav')

@section('title')
Inventory->purachase Order-> Add Vendor Details
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" >Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}" class="active">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
  <div id="page-wrapper">

   <br>

   <div class="">
    <div class="panel panel-info col-xs-12">
     <!-- <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/purchaseorderAdd',[$po_id]) }}">Material</a></li>
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/instrument/detailsadd',[$po_id]) }}">Instrument</a></li> 
      <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchaseorder/gadget/detailsadd',[$po_id]) }}">Gadget</a></li>
      <li style="width:24%" role="presentation" class="active"><a href="{{ url('inventory/purchaseorder/machine/detailsadd',[$po_id]) }}">Machine</a></li> 
    </ul> -->
  </div>
  <div class="panel panel-warning col-xs-12">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation" class="active"><a href="{{ url('inventory/purchase_order/machine/view',[$po_id]) }}">Details</a></li>
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/machine/vendor_details/view',[$po_id]) }}">Vendor Detail</a></li> 
    <li style="width:25%" role="presentation"><a href="{{ url('inventory/purchase_order/machine/generate_delivery/generate_delivery/view',[$po_id]) }}">Generate delivery</a></li>
    <li style="width:24%" role="presentation"><a href="{{ url('inventory/purchase_order/machine/generate_billing/view',[$po_id]) }}">Generate Billing</a></li> 
  </ul>
</div>
</div>

<div class="container">
  

  @foreach($curr_machine as $details)
  <div class="row">
    <div class="col-xs-2">
     <label>Machine Type<label>
     </div>
     <div class="col-xs-3">
      {!! Form::text('null',$details->machine_type,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
    </div>

    <div class="col-xs-3">
      <label>Machine Sub-type<label>
      </div>
      <div class="col-xs-3">
        {!! Form::text('null',$details->machine_subtype,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
      </div>
    </div><br>

    <div class="row">
      <div class="col-xs-2">
        <label>Machine Name<label>
        </div>
        <div class="col-xs-3">
          {!! Form::text('null',$details->machinename,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
        </div>

        <div class="col-xs-3">
         <label>Quantity Required<label>
         </div>
         <div class="col-xs-3">
          {!! Form::text('null',$details->quantity,array('class'=>'form-control','required','id'=>'quntityRequired','readonly')) !!}
        </div>
        
      </div><br>

      @endforeach()
      <!-- current values close -->

    <div class="row">
      <div class="col-xs-2">
        <label>Date Of Order<label>
        </div>
        <div class="col-xs-3">
          <input data-provide="datepicker" data-date-format="dd/mm/yy" id="dateoforder" class="form-control" name="dateoforder" value="{{$curr_po['0']->date_of_order}}" readonly>
        </div>

        <div class="col-xs-3">
         <label>Expected Delivery date<label>
         </div>
         <div class="col-xs-3">
           <input data-provide="datepicker" data-date-format="dd/mm/yy" id="expecteddateoforder" class="form-control" name="expecteddateoforder" value="{{$curr_po['0']->expected_date_of_delivery}}" readonly>
         </div>
       </div><br>

       <div class="row">
        <div class="col-xs-2">
          <label>Order status<label>
        </div>
          <div class="col-xs-3">
            <input type="text" name= "po_id" class="form-control" value="{{$curr_po['0']->name}}" readonly>
            <!-- <select class="form-control" name="order_status_id">
              <option value="{{$curr_po['0']->purchase_order_status_id}}">{{$curr_po['0']->name}}</option>
              @foreach($order_status as $order)
            <option value="{{$order->id}}">{{$order->name}}</option>
            @endforeach
            </select> -->
          </div>    
        </div><br>

        <div class="col-lg-12">
          {!! HTML::link('inventory/purchase_order/machine/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
          <input type="hidden" name= "po_id" value="{{$po_id}}">
        </div>


      </div>
    </div>
  </div>

  
@stop