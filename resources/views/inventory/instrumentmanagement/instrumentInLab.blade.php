@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

	<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
          <!-- <li class="sidebar-search">
            <div class="input-group custom-search-form">
              <input type="text" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div> -->
            <!-- /input-group -->
            <!--  </li> -->
            <li>
              <a href="{{ url('inventory/materialmanagement') }}">Material Management</a>
            </li>

            <li>
              <a href="{{ url('inventory/instrumentmanagement/dashboard') }}" class="active">Instrument management</a>
            </li>

            <li>
              <a href="{{ url('inventory/machinemanagement/dashboard') }}">Machine/Gadget Management</a>
            </li>

            <li>
              <a href="{{ url('inventory/purchaseorder/dashboard') }}">Purchase Order</a>
            </li>
            <li>
              <a href="{{ url('inventory/alert/dashboard') }}">Alert</a>
            </li>

          </ul>
        </div>
        <!-- /.sidebar-collapse -->
      </div>


      <div id="wrapper">

       <div id="page-wrapper">
        <div class="row marketing">
          <div class="">
            <div class="panel panel-info col-xs-12">
              <ul style="width:100%" class="nav nav-pills">
                <li style="width:50%" role="presentation"><a href="{{ url('inventory/instrumentmanagement/dashboard') }}">Inventory Count</a></li>
                <li style="width:49%" role="presentation" class="active"><a href="{{ url('inventory/instrumentmanagement/instrumentinlab') }}">Instrument in Lab</a></li>
              </ul>
            </div>
          </div>
            <br><br><br>
            <div class="panel panel-default filterable">
           <table class="table">
            <thead>
              
              <tr class="filters">
                <th><input type="text" class="form-control" placeholder="Instrument ID" disabled></th>
                <th><input type="text" class="form-control" placeholder="Ins. Name" disabled></th>
                <th><input type="text" class="form-control" placeholder="Lab Name" disabled></th>
                <th><input type="text" class="form-control" placeholder="Contact Person" disabled></th>
                <th><input type="text" class="form-control" placeholder="Cell#" disabled></th>
                <th><input type="text" class="form-control" placeholder="Status" disabled></th>
                <th><input type="text" class="form-control" placeholder="" disabled></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>  </td>
                <td>  </td>
                <td> </td>
                <td>  </td>
                <td>  </td>
                <td> </td>
                <td><a href="#">Change Status</a></td>
              </tr>
              <tr>
                <td>  </td>
                <td>  </td>
                <td> </td>
                <td>  </td>
                <td>  </td>
                <td> </td>
                <td><a href="">Change Status</a></td>
              </tr>
            </tbody>
          </table>

        </div>

      </div>
    </div>
  </div>

@stop