@extends('layouts.menuNav')

@section('title')
listview
@stop

@section('content')

<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}" class="active">Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine  Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>




<div id="wrapper">

 <div id="page-wrapper">

  <div class="row marketing">
    <div class="">
      <div class="panel panel-info col-xs-12">
        <ul style="width:100%" class="nav nav-pills">
          <li style="width:50%" role="presentation" class="active"><a href="{{ url('inventory/instrumentmanagement/listview') }}">Inventory Count</a></li>
          <li style="width:49%" role="presentation"><a href="{{ url('inventory/instrumentmanagement/instrumentinlab') }}">Instrument in Lab</a></li>
        </ul>
      </div>
    </div>
    <br><br><br>
    <div class="panel panel-default filterable">
     <table class="table">
      <thead>

        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="Ins type" disabled></th>
          <th><input type="text" class="form-control" placeholder="Ins. Sub-Type" disabled></th>
          <th><input type="text" class="form-control" placeholder="Ins. Name" disabled></th>
          <th><input type="text" class="form-control" placeholder="Stock Qty" disabled></th>
          <th><input type="text" class="form-control" placeholder="Pending PO Qty" disabled></th>
          <th><input type="text" class="form-control" placeholder="Net Balance Qty" disabled></th>
          <th><input type="text" class="form-control" placeholder="Used in Treatment" disabled></th>
          <th>Action</th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th> 


        </tr>
      </thead>
      <tbody>

        @foreach($instrumentdetails as $instrument)
        <tr>

          <td>{{$instrument->instrument_type}}</td>
          <td>{{$instrument->instrument_subtype}} </td>
          <td>{{$instrument->instrument_name}}</td>
          <td>  </td>
          <td>  </td>
          <td> </td>
          <td> </td>
          <td><a href="{{url('inventory/instrumentmanagement/getdetails',[$instrument->instrument_id])}}"><button type="button" class="btn btn-xs .btn-default"><span class="glyphicon glyphicon-eye-open"></span>Get Details</button></a></td>
          <td> <a href="{{ url('#')}}" class="btn btn-xs btn-success"><i class="fa fa-edit fa-fw"></i>Reset</a></td>

        </tr>
      </tbody>
      @endforeach

    </table>

  </div>

</div>
<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>
</div>
</div>

@stop