@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
          <!-- <li class="sidebar-search">
            <div class="input-group custom-search-form">
              <input type="text" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div> -->
            <!-- /input-group -->
            <!--  </li> -->
            <li>
              <a href="{{ url('inventory/materialmanagement') }}">Material Management</a>
            </li>

            <li>
              <a href="{{ url('inventory/instrumentmanagement/dashboard') }}" class="active">Instrument management</a>
            </li>

            <li>
              <a href="{{ url('inventory/machinemanagement/dashboard') }}">Machine/Gadget Management</a>
            </li>

            <li>
              <a href="{{ url('inventory/purchaseorder/dashboard') }}">Purchase Order</a>
            </li>
            <li>
              <a href="{{ url('inventory/alert/dashboard') }}">Alert</a>
            </li>

          </ul>
        </div>
        <!-- /.sidebar-collapse -->
      </div>

      <div id="wrapper">
      	<div id="page-wrapper">
      		<div class="row marketing">
          		<div class="">
            		<div class="panel panel-info col-xs-12">
              		<ul style="width:100%" class="nav nav-pills">
                	<li style="width:50%" role="presentation" class="active"><a href="{{ url('inventory/instrumentmanagement/dashboard') }}">Inventory Count</a></li>
                	<li style="width:49%" role="presentation"><a href="{{ url('#') }}">Instrument in Lab</a></li>
              		</ul>
            		</div>
          		</div>
          		<div class="">
            		<div class="panel panel-warning col-xs-5">
              		<ul style="width:100%" class="nav nav-pills">
                	<li style="width:100%" role="presentation" class="active"><a href="{{ url('inventory/instrumentmanagement/getdetails') }}">Details</a></li>
              		</ul>
            		</div>
          		</div>
</div>

          	<div class="col-xs-12">
            <div class="col-xs-3">
              <label>Material Name</label>
            </div>
            <div class="col-xs-3">
          {!! Form::text('instrument_name',$vendor_instrument_details->instrument_name, array('class'=>'form-control','readonly')) !!}

            </div>
          </div>
          <br><br>

      			<div class="panel panel-default filterable">
           <table class="table">
            <thead>
              
              <tr class="filters">
                <th><input type="text" class="form-control" placeholder="PO Number" disabled></th>
                <th><input type="text" class="form-control" placeholder="Last Bill Date" disabled></th>
                <th><input type="text" class="form-control" placeholder="Delivery Date" disabled></th>
                <th><input type="text" class="form-control" placeholder="Last Chalan Number" disabled></th>
                <th><input type="text" class="form-control" placeholder="" disabled></th>
                <th><input type="text" class="form-control" placeholder="" disabled></th>
                <th><input type="text" class="form-control" placeholder="" disabled></th>
                <th><input type="text" class="form-control" placeholder="" disabled></th>
              </tr>
            </thead>
            <tbody>


              <tr>
                <td>{{$vendor_instrument_details->purchase_order_id}}</td>
                <td> {{$vendor_instrument_details->date_of_billing}} </td>
                <td>{{$vendor_instrument_details->expected_date_of_delivery}}</td>
                <td>  </td>
                <td>  </td>
                <td> </td>
                <td> </td>
                <td> <a href="#">Throw Away</a></td>
              </tr>
               <tr>
                <td>  </td>
                <td>  </td>
                <td> </td>
                <td>  </td>
                <td>  </td>
                <td> </td>
                <td> </td>
                <td> <a href="#">Throw Away</a></td>
              </tr>
            </tbody>
          </table>

        </div>

      		</div>
      </div>
  </div>

@stop