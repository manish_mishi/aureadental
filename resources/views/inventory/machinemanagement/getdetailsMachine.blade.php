@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}">Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}" class="active">Machine Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">
  <br>
        	<!-- <div class="panel panel-info col-xs-12">
              <ul style="width:100%" class="nav nav-pills">
                <li style="width:50%" role="presentation" class="active"><a href="{{ url('inventory/machinemanagement/dashboard') }}">Inventory Count</a></li>
                <li style="width:49%" role="presentation"><a href="{{ url('inventory/instrumentmanagement/instrumentinlab') }}">Instrument in Lab</a></li>
              </ul>
            </div> -->
            <div class="panel panel-warning">
              <ul style="width:100%" class="nav nav-pills">
                <li style="width:100%" role="presentation" class="active"><a href="{{ url('inventory/machine_management/get_details') }}">Details</a></li>
                
              </ul>
            </div>
            <br>

            <div class="col-xs-12">
              <div class="col-xs-3">
                <label>Machine Name</label>
              </div>
              <div class="col-xs-3">
                {!! Form::text('material_name',$machine_name, array('class'=>'form-control','readonly')) !!}

              </div>
            </div>
            <br><br>
            <div class="panel panel-default filterable">
             <table class="table">
              <thead>

                <tr class="filters">
                  <th><input type="text" class="form-control" placeholder="PO Number" disabled></th>
                  <th><input type="text" class="form-control" placeholder="Last Bill date" disabled></th>
                  <th><input type="text" class="form-control" placeholder="Delivery date" disabled></th>
                  <th><input type="text" class="form-control" placeholder="Challan No" disabled></th>
                  <th><input type="text" class="form-control" placeholder="" disabled></th>
                  <th><input type="text" class="form-control" placeholder="" disabled></th>
                  <th><input type="text" class="form-control" placeholder="" disabled></th>
                </tr>
              </thead>
              <tbody>



                @foreach($vendor_machine_details as $vendor_machine_details)
                <tr>
                  <td>{{$vendor_machine_details->po_id}}</td>
                  <td> {{$vendor_machine_details->date_of_billing}} </td>
                  <td>{{$vendor_machine_details->expected_date_of_delivery}}</td>
                  <td>  </td>
                  <td> </td>
                  <td> </td>
                  <td> <a href="#">Throw Away</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>

          </div>

        </div>
      </div>

      @stop