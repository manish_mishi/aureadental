@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('inventory/material_management/listview') }}" class="active">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}">Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>




<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="panel panel-warning">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:100%" role="presentation" class="active"><a href="{{url('inventory/material_management/get_details',[$po_id])}}">Details</a></li>
    </ul>
  </div>

  <br>
  <div class="col-xs-12">
    <div class="col-xs-3">
      <label>Material Name</label>
    </div>
    <div class="col-xs-3">
      {!! Form::text('material_name', $material_name, array('class'=>'form-control','readonly')) !!}

    </div>
  </div>
  <br><br>
  <div class="panel panel-default filterable">
   <table class="table">
    <thead>

      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="PO Number" disabled></th>
        <th><input type="text" class="form-control" placeholder="Expiry Date" disabled></th>
        <th><input type="text" class="form-control" placeholder="Last Bill Date" disabled></th>
        <th><input type="text" class="form-control" placeholder="Delivery Datey" disabled></th>
        <th><input type="text" class="form-control" placeholder="Pending PO Qty" disabled></th>
        <th><input type="text" class="form-control" placeholder="Last Chalan Number" disabled></th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>

      @foreach($vendor_material_details as $vendor_material_detail)
      <tr>
        <td>{{$vendor_material_detail->purchase_order_id}}</td>
        <td></td>
        <td>{{$vendor_material_detail->expected_date_of_delivery}}</td>
        <td>{{$vendor_material_detail->date_of_billing}} </td>
        <td>{{$vendor_material_detail->quantity}}</td>
        @foreach($po_delivery as $po_del)
        @if($po_del->purchase_order_id == $vendor_material_detail->purchase_order_id)
        <td>{{$po_del->chalan_number}}</td>
        <?php break; ?>
        @endif
        @endforeach
        <td> <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-trash fa-fw"></i>Throw Away</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>

</div>



</div>
</div>

@stop