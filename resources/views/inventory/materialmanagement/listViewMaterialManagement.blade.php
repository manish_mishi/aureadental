  @extends('layouts.menuNav')

  @section('title')
  Dashboard
  @stop

  @section('content')

  <div class="sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">

        <li>
          <a href="{{ url('inventory/material_management/listview') }}" class="active">Material Management</a>
        </li>

        <li>
          <a href="{{ url('inventory/instrument_management/listview') }}">Instrument management</a>
        </li>

        <li>
          <a href="{{ url('inventory/machine_management/listview') }}">Machine Management</a>
        </li>

        <li>
          <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
        </li>

        <li>
          <a href="{{ url('inventory/purchase_order/material/listview') }}">Purchase Order</a>
        </li>
        <li>
          <a href="{{ url('inventory/alert/listview') }}">Alert</a>
        </li>

      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>

  <div id="wrapper">
   <div id="page-wrapper">

    <br>
    <div class="panel panel-info col-xs-12">
      <ul style="width:100%" class="nav nav-pills">
        <li style="width:50%" role="presentation" class="active"><a href="{{ url('inventory/material_management/listview') }}">Inventory Count</a></li>
      </ul>
    </div>

    <br><br><br>
    <div class="panel panel-default filterable">
     <table class="table">
      <thead>
        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="Material type" disabled></th>
          <th><input type="text" class="form-control" placeholder="Mat. Sub-Type" disabled></th>
          <th><input type="text" class="form-control" placeholder="Mat. Name" disabled></th>
          <th><input type="text" class="form-control" placeholder="Stock Qty" disabled></th>
          <th><input type="text" class="form-control" placeholder="Pending PO Qty" disabled></th>
          <th><input type="text" class="form-control" placeholder="Net Balance Qty" disabled></th>
          <th><input type="text" class="form-control" placeholder="Used in Treatment" disabled></th>
          <th>Action</th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th> 

        </tr>
      </thead>
      <tbody>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

        @foreach($materialdetails as $material)
        <tr>
          <td>{{$material->material_type}}</td>
          <td>{{$material->material_subtype}}</td>
          <td>{{$material->material_name}}</td>
          <td>{{$material->safety_stock_value}}</td>
          <td>{{$material->quantity}}</td>
          <td>{{$material->safety_stock_value + $material->quantity}}</td>
          <td> </td>
          <td><a href="{{url('inventory/material_management/get_details',[$material->purchase_order_id])}}"><button type="button" class="btn btn-xs .btn-default"><span class="glyphicon glyphicon-eye-open"></span>Get Details</button></a></td>
          <td> <a href="{{ url('#')}}" class="btn btn-xs btn-reset"><i class="fa fa-pencil fa-fw"></i>Reset</a></td>
        </tr>
        @endforeach
      </tbody>
    </table>

  </div>
  <!-- back to top of the page -->
  <p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
  </p>
</div>
</div>

@stop