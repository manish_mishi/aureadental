@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">   
      <li>
        <a href="{{ url('inventory/material_management') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}">Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine/Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/listview') }}">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}" class="active">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">
  <div class="row marketing">
    <div class="panel panel-info col-xs-12">
      <ul style="width:100%" class="nav nav-pills in">
        <li style="width:50%" role="presentation" class="active"><a href="{{url('inventory/alert/listview')}}">Material Safety Stock Alert</a></li>
        <li style="width:49%" role="presentation"><a href="{{url('inventory/alert/instrument/listview')}}"> Instrument Safety Stock Alerts</a></li> 
      </ul>
    </div>

    <div class="col-xs-12">
      <label>Material</label>
    </div>

    <div class="col-xs-12">

      <div class="panel panel-default filterable">
       <table class="table">
        <thead>

          <tr class="filters">
            <th><input type="text" class="form-control" placeholder=" ID" disabled></th>
            <th><input type="text" class="form-control" placeholder=" Name" disabled></th>
            <th><input type="text" class="form-control" placeholder="Qty Left" disabled></th>
            <th><input type="text" class="form-control" placeholder="Threshold Status" disabled></th>
            <th><input type="text" class="form-control" placeholder="Used in treatment" disabled></th>
            <th><input type="text" class="form-control" placeholder="" disabled></th>
            <th><input type="text" class="form-control" placeholder="" disabled></th>
          </tr>
        </thead>
        <tbody>

         @foreach($material_details as $material)
         <tr>

          <td>{{$material->id}}</td>
          <td>{{$material->material_name}}</td>
          <td>{{$material->quantity}}</td>
          <td>{{$threshold_status->status}}</td>
          <td>  </td>
          <td> <a href="{{url('inventory/purchaseorder/purchaseorderAdd',[$po_id])}}">Place Order </a></td>
          <td><a href="#"></a></td>

        </tr>
        @endforeach
      </tbody>
    </table>

  </div>

</div>
<input type="hidden" name= "po_id" value="{{$po_id}}">

</div>
</div>
</div>
</div>

@stop