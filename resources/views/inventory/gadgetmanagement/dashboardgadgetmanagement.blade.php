@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('inventory/material_management/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/instrument_management/listview') }}">Instrument management</a>
      </li>

      <li>
        <a href="{{ url('inventory/machine_management/listview') }}">Machine Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/gadget_management/listview') }}" class="active">Gadget Management</a>
      </li>

      <li>
        <a href="{{ url('inventory/purchase_order/material/listview') }}">Purchase Order</a>
      </li>
      <li>
        <a href="{{ url('inventory/alert/listview') }}">Alert</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>
<div id="page-wrapper">
 <br>
 <div class="">
   <div class="panel panel-info col-xs-12">
    <ul style="width:100%" class="nav nav-pills">
     <li style="width:50%" role="presentation" class="active"><a href="{{ url('inventory/gadget_management/listview') }}">Inventory Count</a></li>
   </ul>
 </div>
</div>

<br><br><br>
<div class="panel panel-default filterable">
 <table class="table">
  <thead>
    
    <tr class="filters">
      <th><input type="text" class="form-control" placeholder="Gadget Type" disabled></th>
      <th><input type="text" class="form-control" placeholder="Gadget Subtype" disabled></th>
      <th><input type="text" class="form-control" placeholder="Gadget Name" disabled></th>
      <th><input type="text" class="form-control" placeholder="Stock Qty" disabled></th>
      <th><input type="text" class="form-control" placeholder="Pending PO Qty" disabled></th>
      <th><input type="text" class="form-control" placeholder="Net Balance Qty" disabled></th>
      <th><input type="text" class="form-control" placeholder="Used in Treatment" disabled></th>
    </tr>
  </thead>
  <tbody>

    @foreach($gadgetdetails as $gadget)
    <tr>
      <td>{{$gadget->gadget_type}}  </td>
      <td>{{$gadget->gadget_subtype }}  </td>
      <td>{{$gadget->gadget_name}} </td>
      <td>0</td>
      <td>{{$gadget->quantity}}</td>
      <td>{{$gadget->quantity}}</td><!-- add  when stock qty included -->
      <td> </td>
                <!-- <td> <a href="#">Reset Used in Treatment</a></td>
                <td><a href="{{ url('inventory/gadgetmanagement/getdetails') }}">Get Details </a></td> -->
                <td><a href="{{url('inventory/gadgetmanagement/getdetails',[$gadget->po_id])}}"><button type="button" class="btn btn-xs .btn-default"><span class="glyphicon glyphicon-eye-open"></span>Get Details</button></a></td>
                <td> <a href="{{ url('#')}}" class="btn btn-xs btn-success"><i class="fa fa-edit fa-fw"></i>Reset</a></td>
              </tr>

              @endforeach
            </tbody>
          </table>

        </div>

        
      </div>


      @stop