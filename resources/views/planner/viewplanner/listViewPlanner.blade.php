@extends('layouts.menuNav')

@section('title')
List View
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('planner/viewplanner') }}" class="active">View Planner</a>
      </li>

      <li>
        <a href="{{ url('planner/scheduling') }}">Scheduling</a>
      </li>

      <li>
        <a href="{{ url('planner/followup/appointments') }}">Follow Up</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="col-xs-6 col-md-6">
    <div class="col-xs-3 col-md-3">
   {!! Form::label('Sort', null, array('class'=>'control-label','style'=>'float:right')) !!}
 </div>
   <div class="col-xs-5 col-md-5">
    <select class="form-control">
      <option type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" name="lab1" value="cash">Doctor</option>
      <option type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" name="lab1" value="cheque">Consultant</option>
    </select>

  </div>
  <a href="#" class="btn btn-default col-xs-3 col-md-3">Search</a>
</div>

<div class="col-xs-6 col-md-6 pull-right">
  <div class="col-xs-3 col-md-3">
    {!! Form::label('Date', null, array('class'=>'control-label','style'=>'float:right')) !!}
  </div>
  <div class="col-xs-5 col-md-5" align="left">
    <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="date" class="form-control" name="date" value="<?php echo date("d/m/Y");?>">
  </div>
  <a href="#" class="btn btn-default col-xs-2 col-md-2">Go</a>
</div>

<br><br><br>

<?php 
$id="";
$name="";
$cell_no="";
?>

{!! Form::open(array('route' => 'newAppointment','class' => 'form')) !!}
{!! Form::hidden('patient_id', $id) !!}
{!! Form::hidden('patient_name', $name) !!}
{!! Form::hidden('patient_cellno', $cell_no) !!}
<button type="submit" class="btn btn-success col-xs-3 col-md-3 pull-right"><i class="fa fa-plus fa-fw"></i>New Appointment</button>
{!! Form::close() !!}

<br>

<div class="panel panel-info filterable" style="margin-top:3%">

            <!-- Table -->
            <table class="table table-hover"> 
              <thead class="panel-info">
                <tr class="filters">
                  <th><input type="text" class="form-control" placeholder="Time Slots" disabled></th>
                  <th><input type="text" class="form-control" placeholder="D1/C1" disabled></th>
                  <th><input type="text" class="form-control" placeholder="D2/C2" disabled></th>
                  <th><input type="text" class="form-control" placeholder="D3" disabled></th>
                  <th><input type="text" class="form-control" placeholder="C1" disabled></th>
                  <th><input type="text" class="form-control" placeholder="C2" disabled></th>
                  <th>Action</th>
                  <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
                </tr> 
              </thead>
              <tbody>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>  

                    {!! Form::open(array('class' => 'form')) !!}
                    {!! Form::button('<i class="fa fa-edit fa-fw"></i>Edit',array('type' => 'submit','class'=>'btn btn-xs btn-primary')) !!}
                    {!! Form::close() !!}

                  </td> 
                  <td>
                    {!! Form::open(array('class' => 'form')) !!}
                    {!! Form::button('<i class="fa fa-trash fa-fw"></i>Delete',array('type' => 'submit','class'=>'btn btn-xs btn-danger')) !!}
                    {!! Form::close() !!}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop