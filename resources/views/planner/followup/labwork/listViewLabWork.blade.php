@extends('layouts.menuNav')

@section('title')
List view -> Lab Work
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('planner/viewplanner') }}" >View Planner</a>
      </li>

      <li>
        <a href="{{ url('planner/scheduling') }}"  >Scheduling</a>
      </li>

      <li>
        <a href="{{ url('planner/followup/appointments') }}" class="active">Follow Up</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>
<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:24%" role="presentation" ><a href="{{ url('planner/followup/appointments') }}">Appointments</a></li>
      <li style="width:25%" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Inventory
          <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
            <li role="presentation"><a href="{{ url('planner/followup/inventory/material/newfollowup') }}">Material</a></li>
            <li role="presentation"><a href="{{ url('planner/followup/inventory/instrument/newfollowup') }}">Instrument</a></li>   
          </ul>
        </li>
        <li style="width:25%" class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Maintenance
            <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
              <li role="presentation"><a href="{{ url('planner/followup/maintenance/gadget/newfollowup') }}">Gadget</a></li>
              <li role="presentation"><a href="{{ url('planner/followup/maintenance/machine/newfollowup') }}">Machine</a></li>   
            </ul>
          </li>
      <li style="width:25%" role="presentation"  class="active"><a href="{{ url('planner/followup/inventory/labwork/newfollowup') }}">Lab Work</a></li>
    </ul>
  </div>

  <div class="row">
    <label class="col-xs-3" align="right">Sort By:</label>
    <div class="col-xs-3">
      <select class="form-control">
        <option type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" name="lab1" value="lab1">Instrument</option>
      </select>
    </div>
  </div>

  <br>
  <div class="panel panel-default filterable">
   <table class="table">
    <thead>

      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="" disabled> Job ID</th>
        <th><input type="text" class="form-control" placeholder="" disabled> Re-work ID</th>
        <th><input type="text" class="form-control" placeholder="" disabled>Date</th>
        <th><input type="text" class="form-control" placeholder="" disabled>Dentist/Consultan</th>
        <th><input type="text" class="form-control" placeholder="" disabled>Lab Id</th>
        <th><input type="text" class="form-control" placeholder="" disabled>Status</th>

        <th><input type="text" class="form-control" placeholder="" disabled>Instrument Id</th>
        <th><input type="text" class="form-control" placeholder="" disabled>Work</th>
        <th><input type="text" class="form-control" placeholder="" disabled></th>
      </tr>
    </thead>
    <tbody>


      <tr>

        <td>  </td>
        <td>  </td>
        <td> </td>
        <td> </td>
        <td> </td> 
        <td> </td> 
        <td> </td>
        <td> </td>
        <td>Get Details </td>
        <td><a href="#">generate reworkr</td>



      </tr>
    </tbody>
  </table>

</div>

</div>
</div>
@stop