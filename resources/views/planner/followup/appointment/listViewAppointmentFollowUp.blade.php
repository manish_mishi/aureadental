@extends('layouts.menuNav')

@section('title')
List view -> Appointment
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('planner/viewplanner') }}" >View Planner</a>
      </li>

      <li>
        <a href="{{ url('planner/scheduling') }}"  >Scheduling</a>
      </li>

      <li>
        <a href="{{ url('planner/followup/appointments') }}" class="active">Follow Up</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="panel panel-info">

    <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('planner/followup/appointments') }}">Appointments</a></li>
      <li style="width:25%" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Inventory
          <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
            <li role="presentation"><a href="{{ url('planner/followup/inventory/material/newfollowup') }}">Material</a></li>
            <li role="presentation"><a href="{{ url('planner/followup/inventory/instrument/newfollowup') }}">Instrument</a></li>   
          </ul>
        </li>
        <li style="width:25%" class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Maintenance
            <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
              <li role="presentation"><a href="{{ url('planner/followup/maintenance/gadget/newfollowup') }}">Gadget</a></li>
              <li role="presentation"><a href="{{ url('planner/followup/maintenance/machine/newfollowup') }}">Machine</a></li>   
            </ul>
          </li>
          <li style="width:24%" role="presentation"  ><a href="{{ url('planner/followup/inventory/labwork/newfollowup') }}">Lab Work</a></li>
        </ul>
      </div>

      <div class="row">
        {!! Form::open(array('route'=>'searchFollowUpDetails','class'=>'form')) !!}
        <div class="col-xs-2 col-md-2">
         {!! form::label('Date',null,array('class'=>'control-label','style'=>'float:right'))!!}
       </div>
       <div class="col-xs-3 col-md-3" align="left">
        <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="date" class="form-control" name="date">
      </div>

      <br><br>
      <div class="col-xs-2 col-md-2">
        {!! form::label('Status',null,array('class'=>'control-label','style'=>'float:right'))!!}
      </div>
      <div class="col-xs-3 col-md-3" align="left">
        <select class="form-control" name="follow_up_status_id" id ='status'>
         <option value>Select</option>
         @foreach($status as $stat)
         <option value="{{$stat->id}}">{{$stat->follow_up_status_type}}</option>
         @endForeach
       </select>
      </div>

      <div class=" col-xs-1 col-md-1">
        <button type="submit" class="btn btn-default">Search</button>
      </div>
      {!! Form::close()!!}
    </div>

    <br>
    <div>
      <a href="{{url('planner/followup/appointments/newfollowup')}}" class="col-xs-3 col-md-3 btn btn-success pull-right"><i class="fa fa-plus fa-fw"></i>New Follow-Up</a>
    </div>
    <br><br>

  <div class="panel panel-default filterable table-responsive">
   <table class="table table-hover">
    <thead class="panel-info">
      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="Patient Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Cell No" disabled></th>
        <th><input type="text" class="form-control" placeholder="Date" disabled></th>
        <th><input type="text" class="form-control" placeholder="Slot Timing" disabled></th>
        <th><input type="text" class="form-control" placeholder="Doctor" disabled></th>
        <th><input type="text" class="form-control" placeholder="Status" disabled></th>
        <th><input type="text" class="form-control" placeholder="Comment" disabled></th>
        <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
      </tr>
    </thead>
    <tbody>
      @foreach($followUpDetails as $followUp)
        <tr>
         <td>{{$followUp->name}}</td>
         <td>{{$followUp->cell_no}}</td>
         <td>{{$followUp->date}}</td>
         <td>{{$followUp->time}}</td>
         <td></td>
         <td>{{$followUp->follow_up_status_type}}</td>
         <td colspan="2">{{$followUp->notes}}</td>
       </tr>
       @endforeach     
  </tbody>
</table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop