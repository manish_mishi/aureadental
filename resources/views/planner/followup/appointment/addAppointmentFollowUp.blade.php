@extends('layouts.menuNav')

@section('title')
Follow up
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('planner/viewplanner') }}" >View Planner</a>
      </li>

      <li>
        <a href="{{ url('planner/scheduling') }}"  >Scheduling</a>
      </li>

      <li>
        <a href="{{ url('planner/followup/appointments') }}" class="active">Follow Up</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('planner/followup/appointments') }}">Appointments</a></li>
      <li style="width:25%" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Inventory
          <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
            <li role="presentation"><a href="{{ url('planner/followup/inventory/material/newfollowup') }}">Material</a></li>
            <li role="presentation"><a href="{{ url('planner/followup/inventory/instrument/newfollowup') }}">Instrument</a></li>   
          </ul>
        </li>
        <li style="width:25%" class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Maintenance
            <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
              <li role="presentation"><a href="{{ url('planner/followup/maintenance/gadget/newfollowup') }}">Gadget</a></li>
              <li role="presentation"><a href="{{ url('planner/followup/maintenance/machine/newfollowup') }}">Machine</a></li>   
            </ul>
          </li>
          <li style="width:24%" role="presentation"><a href="{{ url('planner/followup/inventory/labwork/newfollowup') }}">Lab Work</a></li>
        </ul>
      </div>

      {!! Form::open(array('route'=>'addnewfollowupdetails','class'=>'form')) !!}

      <div class="row">
        {!! form::label('Patient Name',null,array('class'=>'col-xs-3 control-label'))!!}
        <div class="col-xs-3 col-lg-3 col-md-3">
          <select class="form-control" name="patient_id" id ='patientId'>
           <option value>Select</option>
           @foreach($patientNames as $patientName)
           <option value="{{$patientName->id}}">{{$patientName->name}}</option>
           @endForeach
         </select>
       </div>
       {!! form::label('Status',null,array('class'=>'col-xs-3 control-label'))!!}
       <div class="col-xs-3 col-lg-3 col-md-3">
        <select class="form-control" name="status_id" id ='status'>
         <option value>Select</option>
         @foreach($status as $stat)
         <option value="{{$stat->id}}">{{$stat->follow_up_status_type}}</option>
         @endForeach
       </select>
     </div>
   </div><br>

   <div class="row">
    {!! form::label('Cell',null,array('class'=>'col-xs-3 control-label'))!!}
    <div class="col-xs-3 col-lg-3 col-md-3">
      {!! form::text('cell_no',null,array('class'=>'form-control','id'=>'cellNo','readonly'))!!}
    </div>
    <label class="col-xs-3 col-lg-3 col-md-3">Notes</label>
    <div class="col-xs-3 col-lg-3 col-md-3">
      {!! Form::textarea('notes',null,array('class'=>'form-control','rows' => 2, 'cols' => 40))!!}   
    </div>
  </div>

  <br>
  <div class="row">
    {!! form::label('Follow Up Date',null,array('class'=>'col-xs-3 control-label'))!!}
    <div class="col-xs-3 col-lg-3 col-md-3">
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="follow_up_date" class="form-control" name="follow_up_date" value="<?php echo date("d/m/Y");?>">
    </div>

    {!! form::label('Time',null,array('class'=>'col-xs-3 control-label'))!!}
    <div class="col-xs-3 col-lg-3 col-md-3">
      {!! Form::text('follow_up_time', null,array('class'=>'form-control time','id'=>'time')) !!}
    </div>
  </div>

  <br>
  <div class="row pull-right">
   {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning')) !!}
   {!! HTML::link('planner/followup/appointments', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}
 </div>

 {!! Form::close() !!}

<script>
$(document).ready(function(){

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(".time").timepicker({
  template: false,
  showInputs: false,
  minuteStep: 15
});

var cellNo = document.getElementById('cellNo');

$("#patientId").change(function(){

    var patientId = $(this).val();

    if(patientId != "")
    {
      $.ajax({
        method: "POST",
        url: '{{url("planner/followup/appointment")}}'+"/"+patientId,
        success : function(data){
          for (var i in data) {
            $(cellNo).val(data[i]['cell_no']);
          }

        }

      });
    }
    else
    {
      $(cellNo).val("");
    }

});

});

</script>


 <!-- back to top of the page -->
 <p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop
