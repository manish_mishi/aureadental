@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
            <li>
              <a href="{{ url('planner/viewplanner') }}" >View Planner</a>
            </li>

            <li>
              <a href="{{ url('planner/scheduling') }}"  >Scheduling</a>
            </li>

            <li>
              <a href="{{ url('planner/followup/appointments') }}" class="active">Follow Up</a>
            </li>

          </ul>
        </div>
        <!-- /.sidebar-collapse -->
      </div>




      <div id="wrapper">

       <div id="page-wrapper">
        <div class="row marketing">
           <div class="panel panel-info">
        <ul style="width:100%" class="nav nav-pills">
          <li style="width:23%" role="presentation" class="active"><a href="{{ url('planner/followup/appointments') }}">Appointments</a></li>
          <li style="width:23%" role="presentation"  ><a href="{{ url('maintenance/gadget/maintenance') }}">Inventory</a></li>
          <li style="width:23%" role="presentation"  ><a href="{{ url('maintenance/gadget/maintenance') }}">Maintenance</a></li>
          <li style="width:23%" role="presentation"  ><a href="{{ url('maintenance/gadget/maintenance') }}">Lab Work</a></li>

        </ul>
</div>
          <form>
         
      <br><br>   
     <div class="row">
                
                <label class="col-xs-2" align="left">Date:</label>
                <div class="col-xs-2" align="left"><input type="text" class="form-control" name="status">
  
                </div>
                  
                   <br> <br><br>
                <label class="col-xs-2" align="left">Status:</label>
                <div class="col-xs-2" align="left"><input type="text" class="form-control" name="status">
                  </div>
                  <a href="#" class="btn btn-success  col-xs-1">Search</a>
              </div>
              <br>
               <a href="{{url('planner/followup/appointments/newfollowup')}}" class="btn btn-success pull-right col-xs-2"style=><i class="fa fa-plus fa-fw"></i>New Follow-Up</a>
     <br><br>

          <div class="panel panel-default filterable">
           <table class="table">
            <thead>
              
              <tr class="filters">
                <th><input type="text" class="form-control" placeholder="" disabled>Patient Name</th>
                <th><input type="text" class="form-control" placeholder="" disabled>Cell#</th>
                <th><input type="text" class="form-control" placeholder="" disabled>Appointment Id</th>
                <th><input type="text" class="form-control" placeholder="" disabled>Date</th>
                <th><input type="text" class="form-control" placeholder="" disabled>Slot Timing</th>
                <th><input type="text" class="form-control" placeholder="" disabled>Doctor</th>
                <th><input type="text" class="form-control" placeholder="" disabled>Status</th>
                <th><input type="text" class="form-control" placeholder="" disabled>Comment</th>
                 <th></th>
                <th></th>
            
                

                <!-- <th><button class="btn btn-default btn-xs btn-filter">Filter</button></th> -->
              </tr>
            </thead>
            <tbody>


              <tr>

                <td>  </td>
                <td>  </td>
                <td> </td>
                <td>  </td>
                <td>  </td>
                <td></td>
                <td></td>
                <td></td>
               <td></td>
                <td><a href="{{url('#')}}">Make Payment</a></td>
         </tr>
            </tbody>
          </table>

        </div>
      </form>
      </div>
    </div>
  </div>

  @stop