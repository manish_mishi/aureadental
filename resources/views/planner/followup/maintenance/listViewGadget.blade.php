@extends('layouts.menuNav')

@section('title')
List view -> Gadget
@stop

@section('content')

<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('planner/viewplanner') }}">View Planner</a>
      </li>

      <li>
        <a href="{{ url('planner/scheduling') }}">Scheduling</a>
      </li>

      <li>
        <a href="{{ url('planner/followup/appointments') }}" class="active">Follow Up</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>
<div id="wrapper">
 <div id="page-wrapper">

   <br>
   <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation" ><a href="{{ url('planner/followup/appointments') }}">Appointments</a></li>
      <li style="width:25%" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Inventory
          <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
            <li role="presentation"><a href="{{ url('planner/followup/inventory/material/newfollowup') }}">Material</a></li>
            <li role="presentation"><a href="{{ url('planner/followup/inventory/instrument/newfollowup') }}">Instrument</a></li>   
          </ul>
        </li>
        <li style="width:25%" class="active dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Maintenance
            <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
              <li role="presentation"><a href="{{ url('planner/followup/maintenance/gadget/newfollowup') }}">Gadget</a></li>
              <li role="presentation"><a href="{{ url('planner/followup/maintenance/machine/newfollowup') }}">Machine</a></li>   
            </ul>
          </li>
          <li style="width:24%" role="presentation"  ><a href="{{ url('planner/followup/inventory/labwork/newfollowup') }}">Lab Work</a></li>
        </ul>
      </div>

      <div class="panel panel-default filterable">
       <table class="table table-hover">
        <thead class="panel-info">

          <tr class="filters">
            <th><input type="text" class="form-control" placeholder="Gadget Name" disabled></th>
            <th><input type="text" class="form-control" placeholder="Last Dt of Maintenance" disabled></th>
            <th><input type="text" class="form-control" placeholder="Due Date of Maintenance" disabled></th>
            <th>Action</th>
            <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
          </tr>
        </thead>
        <tbody>


          @foreach($gadgetdetails as $gadget)
          <tr>
            <td>{{$gadget->gadget_name}}</td>
            <td>{{$gadget->last_maintenance_date}}</td>
            <td>{{$gadget->due_date}}</td>
            <td colspan="2"><a href="{{url('gadget/maintenance/assignmaintenance/assignwork',[$gadget->id])}}">Assign Maintenance </a></td>
          </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>
  @stop