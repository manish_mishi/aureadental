    @extends('layouts.menuNav')

    @section('title')
    Planner -> New Appointment
    @stop

    @section('content')

    <div class="sidebar" role="navigation">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <li>
            <a href="{{ url('planner/viewplanner') }}">View Planner</a>
          </li>

          <li>
            <a href="{{ url('planner/scheduling') }}" class="active">Scheduling</a>
          </li>

          <li>
            <a href="{{ url('planner/followup/appointments') }}">Follow Up</a>
          </li>
        </ul>
      </div>
      <!-- /.sidebar-collapse -->
    </div>

    <div id="wrapper">
      <div id="page-wrapper">

        <br>
        {!! Form::open(array('route' => 'addnewappointment','class' => 'form')) !!}

        <!-- successfullye added msg -->
        <div class="flash-message">
          @foreach (['danger', 'warning', 'success', 'info'] as $msg)
          @if(Session::has('alert-' . $msg))

          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
          @endforeach
        </div> <!-- end .flash-message -->

        <div class="row">
          {!! Form::label('Name', null, array('class'=>'col-xs-2 col-md-2 control-label')) !!}

          <div class="col-xs-3 col-md-3">
            {!! Form::text('patient_name', $patientDetails->name,array('class'=>'form-control','id'=>'name')) !!}
          </div>

          {!! Form::label('Cell', null, array('class'=>'col-xs-2 col-md-2 control-label')) !!}

          <div class="col-xs-3 col-md-3">
            {!! Form::input('number','cell_no', $patientDetails->cell_no,array('class'=>'form-control','id'=>'cell_no')) !!}
          </div>
        </div>

        <br><br>
        <div class="row">
          {!! Form::label('Assign Date', null, array('class'=>'col-xs-2 col-md-2 control-label')) !!}

          <div class="col-xs-3 col-md-3">
            <input data-provide="datepicker" data-date-format="dd/mm/yy" id="day" class="form-control" name="assign_date" value="">
          </div>
          {!! Form::label('Dentist Name',null,array('class'=>'col-xs-2 col-md-2 control-label')) !!}

          <div class="col-xs-3 col-md-3">
            <select class="form-control" name="dentist_id" id ='dentist'>
              <option value>Select</option>
              @foreach($dentistName as $dentist)
              <option value="{{$dentist->id}}">{{$dentist->dentist_name}}</option>
              @endForeach
            </select>
          </div>
        </div>

        <br><br>
        <div class="row">
          {!! Form::label('Assign Slot', null, array('class'=>'col-xs-2 col-md-2 control-label')) !!}

          <div class="col-xs-3 col-md-3">
            <input type="text" class="form-control assign_slot" name="assign_slot" id="assign_slot" value="">
          </div>
          {!! Form::label('Chair',null,array('class'=>'col-xs-2 col-md-2 control-label')) !!}

          <div class="col-xs-3 col-md-3">
            <select class="form-control" name="chair_id" id ='chair'>
              <option value>Select</option>
              @foreach($chairName as $chair)
              <option value="{{$chair->id}}">{{$chair->chair_name}}</option>
              @endForeach
            </select>
          </div>
        </div>

        <br><br>
        <div class="row">
          {!! Form::label('Assistant', null, array('class'=>'col-xs-2 col-md-2 control-label')) !!}

          <div class="col-xs-3 col-md-3">
            {!! Form::text('assistant', null,array('class'=>'form-control','id'=>'assistant')) !!}
          </div>
          {!! Form::label('Consultant', null, array('class'=>'col-xs-2 col-md-2 control-label')) !!}

          <div class="col-xs-3 col-md-3">
            {!! Form::text('consultant', null,array('class'=>'form-control','id'=>'consultant')) !!}
          </div>
        </div>

        <br><br>
        <div class="row">
          {!! Form::label('Status', null, array('class'=>'col-xs-2 col-md-2 control-label')) !!}

          <div class="col-xs-3 col-md-3">
             <select class="form-control" name="appointment_status_id" id ='appointmentStatus'>
              <option value>Select</option>
              @foreach($appointmentStatus as $status)
              <option value="{{$status->id}}">{{$status->status}}</option>
              @endForeach
            </select>
          </div>
        </div>

        <br><br>
        <div class="pull-right">
          {!! Form::hidden('patient_id', $patientDetails->id) !!}
          {!! Form::button('&#x2714; Save',array('type' => 'submit','class'=>'btn btn-warning')) !!}
          {!! HTML::link('planner/scheduling', '&#10006; Cancel', array('id' => 'linkid','class'=>'btn btn-danger')) !!}
        </div>

        {!! Form::close() !!}

        <script type="text/javascript">

        $(".assign_slot").timepicker({
          template: false,
          showInputs: false,
          minuteStep: 15
        });

        </script>

      </div>
    </div>


    @stop