    @extends('layouts.menuNav')

    @section('title')
    Planner -> Reschedule Appointment
    @stop

    @section('content')

    <div class="navbar-default sidebar" role="navigation">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">

          <li>
            <a href="{{ url('planner/viewplanner') }}">View Planner</a>
          </li>

          <li>
            <a href="{{ url('planner/scheduling') }}" class="active">Scheduling</a>
          </li>

          <li>
            <a href="{{ url('planner/followup/appointments') }}">Follow Up</a>
          </li>

        </ul>
      </div>
      <!-- /.sidebar-collapse -->
    </div>

    <div id="wrapper">
      <div id="page-wrapper">

        <br>
        {!! Form::open(array('route' => 'updatereschedulePatientInformation','class' => 'form')) !!}


        {!! Form::label('Name', null, array('class'=>'col-xs-2 control-label')) !!}

        <div class="col-xs-3">
          {!! Form::text('patient_name',$appointment->name,array('class'=>'form-control','id'=>'name')) !!}
        </div>

        <div class="row">
          {!! Form::label('Cell', null, array('class'=>'col-xs-2 control-label')) !!}

          <div class="col-xs-3">
            {!! Form::input('number','cell_no',$appointment->cell_no,array('class'=>'form-control','id'=>'cell_no')) !!}
          </div>
        </div>

        <br><br>
        {!! Form::label('Assign Date', null, array('class'=>'col-xs-2 control-label')) !!}

        <div class="col-xs-3">
          <input data-provide="datepicker" data-date-format="dd/mm/yy" id="day" class="form-control" name="assign_date" value="{{$appointment->date}}">
        </div>
        <div class="row">
          {!! Form::label('Dentist Name',null,array('class'=>'col-xs-2 control-label')) !!}

          <div class="col-xs-3">
            <select class="form-control" name="dentist_id" id ='dentist'>
             <option value="{{$appointment->dentist_id}}">{{$appointment->dentist_name}}</option>
             @foreach($dentist_names as $dentistname)
             <option value="{{$dentistname->id}}">{{$dentistname->dentist_name}}</option>
             @endForeach
           </select>  



         </div></div><br><br>
         {!! Form::label('Assign Slot', null, array('class'=>'col-xs-2 control-label')) !!}

         <div class="col-xs-3">
          <input type="text" class="form-control assign_slot" name="assign_slot" id="assign_slot" value="{{$appointment->slot}}">
        </div>

        <div class="row">
          {!! Form::label('Chair',null,array('class'=>'col-xs-2 control-label')) !!}

          <div class="col-xs-3">
            <select class="form-control" name="chair_id" id ='chair'>
             <option value="{{$appointment->chair_id}}">{{$appointment->chair_name}}</option>
             @foreach($chair_names as $chairname)
             <option value="{{$chairname->id}}">{{$chairname->chair_name}}</option>
             @endForeach
           </select>

         </div></div><br><br>

         {!! Form::label('Assistant', null, array('class'=>'col-xs-2 control-label')) !!}

         <div class="col-xs-3">
          {!! Form::text('assistant', $appointment->assistant,array('class'=>'form-control','id'=>'assistant')) !!}
        </div>

        <div class="row">
          {!! Form::label('Consultant', null, array('class'=>'col-xs-2 control-label')) !!}

          <div class="col-xs-3">
            {!! Form::text('consultant', $appointment->consultant,array('class'=>'form-control','id'=>'consultant')) !!}
          </div></div><br><br>

          {!! Form::label('Status', null, array('class'=>'col-xs-2 control-label')) !!}

          <div class="col-xs-3">
            <select class="form-control" name="appointment_status_id" id ='appointmentStatus'>
              <option value="{{$appointment->appointment_status_id}}">{{$appointment->status}}</option>
              @foreach($appointmentStatus as $status)
              <option value="{{$status->id}}">{{$status->status}}</option>
              @endForeach
            </select>
          </div><br><br>


          <div class="pull-right">
            {!! Form::hidden('appointment_id', $appointment->id) !!}
            {!! Form::hidden('patient_id', $appointment->patient_id) !!}
            {!! Form::button('&#x2714; Save',array('type' => 'submit','class'=>'btn btn-warning')) !!}
            {!! HTML::link('planner/scheduling', '&#10006; Cancel', array('id' => 'linkid','class'=>'btn btn-danger')) !!}
          </div>

          {!! Form::close() !!}

          <script type="text/javascript">

          $(".assign_slot").timepicker({
            template: false,
            showInputs: false,
            minuteStep: 15
          });

          </script>
        </div>
      </div>


      @stop