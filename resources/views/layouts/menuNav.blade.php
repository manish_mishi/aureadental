<!doctype html>
<html lang="en">
<head>
	@section('head')

  <title>@yield('title')</title>

  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  {!! HTML::style('assets/css/sweet-alert.css') !!}
  {!! HTML::style('assets/css/jquery-ui.css') !!}
  {!! HTML::style('assets/css/style.css') !!}
  {!! HTML::style('assets/css/bootstrap.min.css') !!}

  <!-- Custom styles for this template -->
  {!! HTML::style('assets/css/main-theme.css') !!}
  {!! HTML::style('assets/css/datepicker3.css') !!}
  {!! HTML::style('assets/css/typeahead.css') !!}
  {!! HTML::style('assets/css/table.css') !!}
  {!! HTML::style('assets/css/datefield.css') !!}
  {!! HTML::style('assets/css/custom-button.css') !!}
  
  <!-- MetisMenu CSS -->
  {!! HTML::style('assets/bower_components/metisMenu/dist/metisMenu.min.css') !!}

  <!-- Timeline CSS -->
  {!! HTML::style('assets/dist/css/timeline.css') !!}

  <!-- Custom CSS -->
  {!! HTML::style('assets/dist/css/sb-admin-2.css') !!}

  <!-- Morris Charts CSS -->
  {!! HTML::style('assets/bower_components/morrisjs/morris.css') !!}

  <!-- Custom Fonts -->
  {!! HTML::style('assets/bower_components/font-awesome/css/font-awesome.min.css') !!}

  <!-- timepicker -->
  {!! HTML::style('assets/css/jquery.ptTimeSelect.css') !!}
  {!! HTML::style('assets/css/ui.theme.css') !!}
  {!! HTML::style('assets/css/bootstrap-timepicker.min.css') !!}
  
  <!-- toggle view-->
  {!! HTML::style('assets/css/responsive-nav.css') !!}

  <!-- scroll up-->
  {!! HTML::style('assets/css/scroll-up.css') !!}
  
  <!-- JS Files -->

  {!! HTML::script('assets/js/sweet-alert.min.js') !!}
  {!! HTML::script('assets/js/jquery-1.11.0.min.js') !!}
  {!! HTML::script('assets/js/bootstrap-datepicker.js') !!}
  {!! HTML::script('assets/js/formUtilities.js') !!}
  {!! HTML::script('assets/js/jquery-ui.js') !!}
  {!! HTML::script('assets/js/jquery-ui.min.js') !!}
  {!! HTML::script('assets/js/table.js') !!}
  {!! HTML::script('assets/js/bootstrap-checkbox.js') !!}

  {!! HTML::script('assets/js/typeahead.jquery.min.js') !!}

  <!-- AngularJS JavaScript -->
  {!! HTML::script('assets/js/angularJS/angular.min.js') !!}
  {!! HTML::script('assets/js/angularJS/angular-resource.min.js') !!}
  {!! HTML::script('assets/js/angularJS/angular-route.min.js') !!}
  {!! HTML::script('assets/js/angularJS/app.js') !!}
  {!! HTML::script('assets/js/angularJS/AppDirectives.js') !!}
  <!--  {!! HTML::script('public/app/controllers/router.js') !!} -->

  <!-- Bootstrap Core JavaScript -->
  {!! HTML::script('assets/js/bootstrap.min.js') !!}

  <!-- Metis Menu Plugin JavaScript -->
  {!! HTML::script('assets/bower_components/metisMenu/dist/metisMenu.min.js') !!}

  <!-- Morris Charts JavaScript -->
 <!--  {!! HTML::script('assets/bower_components/raphael/raphael-min.js') !!}
  {!! HTML::script('assets/bower_components/morrisjs/morris.min.js') !!}
  {!! HTML::script('assets/js/morris-data.js') !!} -->

  <!-- Custom Theme JavaScript -->
  {!! HTML::script('assets/dist/js/sb-admin-2.js') !!}

  <!-- timepicker -->
  {!! HTML::script('assets/js/jquery.ptTimeSelect.js') !!}
  {!! HTML::script('assets/js/bootstrap-timepicker.js') !!}
  {!! HTML::script('assets/js/toaster.js') !!}

  <!-- toggle view -->
  {!! HTML::script('assets/js/responsive-nav.js') !!}

  <!-- scroll up-->
  {!! HTML::script('assets/js/scroll-up.js') !!}  
  
  @show
</head>
<body ng-app="myApp" id="top">
  <div class="container">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
        <a class="navbar-brand" href="{{ url('/home') }}">Aurea Dental</a>
      </div>
      <!-- /.navbar-header -->

      <ul class="nav navbar-top-links navbar-right">
     <!--  @if (Auth::guest()!=null)
           <li>Login</li>
            
           @else -->
           <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-messages">
              <li>
                <a href="#">
                  <div>
                    <strong>John Smith</strong>
                    <span class="pull-right text-muted">
                      <em>Yesterday</em>
                    </span>
                  </div>
                  <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="#">
                  <div>
                    <strong>John Smith</strong>
                    <span class="pull-right text-muted">
                      <em>Yesterday</em>
                    </span>
                  </div>
                  <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="#">
                  <div>
                    <strong>John Smith</strong>
                    <span class="pull-right text-muted">
                      <em>Yesterday</em>
                    </span>
                  </div>
                  <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a class="text-center" href="#">
                  <strong>Read All Messages</strong>
                  <i class="fa fa-angle-right"></i>
                </a>
              </li>
            </ul>
            <!-- /.dropdown-messages -->
          </li>
          <!-- /.dropdown -->
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-tasks">
              <li>
                <a href="#">
                  <div>
                    <p>
                      <strong>Task 1</strong>
                      <span class="pull-right text-muted">40% Complete</span>
                    </p>
                    <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                        <span class="sr-only">40% Complete (success)</span>
                      </div>
                    </div>
                  </div>
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="#">
                  <div>
                    <p>
                      <strong>Task 2</strong>
                      <span class="pull-right text-muted">20% Complete</span>
                    </p>
                    <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                        <span class="sr-only">20% Complete</span>
                      </div>
                    </div>
                  </div>
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="#">
                  <div>
                    <p>
                      <strong>Task 3</strong>
                      <span class="pull-right text-muted">60% Complete</span>
                    </p>
                    <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                        <span class="sr-only">60% Complete (warning)</span>
                      </div>
                    </div>
                  </div>
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="#">
                  <div>
                    <p>
                      <strong>Task 4</strong>
                      <span class="pull-right text-muted">80% Complete</span>
                    </p>
                    <div class="progress progress-striped active">
                      <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                        <span class="sr-only">80% Complete (danger)</span>
                      </div>
                    </div>
                  </div>
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a class="text-center" href="#">
                  <strong>See All Tasks</strong>
                  <i class="fa fa-angle-right"></i>
                </a>
              </li>
            </ul>
            <!-- /.dropdown-tasks -->
          </li>
          <!-- /.dropdown -->
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-alerts">
              <li>
                <a href="#">
                  <div>
                    <i class="fa fa-comment fa-fw"></i> New Comment
                    <span class="pull-right text-muted small">4 minutes ago</span>
                  </div>
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="#">
                  <div>
                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                    <span class="pull-right text-muted small">12 minutes ago</span>
                  </div>
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="#">
                  <div>
                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                    <span class="pull-right text-muted small">4 minutes ago</span>
                  </div>
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="#">
                  <div>
                    <i class="fa fa-tasks fa-fw"></i> New Task
                    <span class="pull-right text-muted small">4 minutes ago</span>
                  </div>
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="#">
                  <div>
                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                    <span class="pull-right text-muted small">4 minutes ago</span>
                  </div>
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a class="text-center" href="#">
                  <strong>See All Alerts</strong>
                  <i class="fa fa-angle-right"></i>
                </a>
              </li>
            </ul>
            <!-- /.dropdown-alerts -->
          </li>
          <!-- /.dropdown -->
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
              <li><a href="#"><i class="fa fa-user fa-fw"></i>{{ Auth::user()->name }}</a>
              </li>
        <!--   <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
      </li> -->
      <li class="divider"></li>
      <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
      </li>
    </ul>
    <!-- /.dropdown-user -->
  </li>
  <!-- /.dropdown -->
  <!--       @endif -->
</ul>
<!-- /.navbar-top-links -->


<!-- /.navbar-static-side -->
</nav>


<nav class="navbar navbar-default" style="margin-bottom: 0.1%">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">

      <li class="dropdown {{ Request::is('settings','settings/inventory/materialmanagement/addmaterialtype','settings/inventory/materialmanagement/addmaterialsubtype','settings/inventory/materialmanagement/addmaterailunit','settings/inventory/instrumentmanagement/addinstrumenttype','settings/inventory/instrumentmanagement/addinstrumentsubtype','settings/inventory/instrumentmanagement/addinstrumentunit','settings/inventory/machinemanagement/addmachinetype','settings/inventory/machinemanagement/addmachinesubtype','settings/inventory/machinemanagement/addmachineunit','settings/inventory/gadgetmanagement/addgadgettype','settings/inventory/gadgetmanagement/addgadgetsubtype','settings/patient/dentist/adddentistname','settings/patient/chair/addchairdetails','settings/treatment/type','settings/treatment/status','settings/lab/addworktype','settings/lab/addworksubtype','settings/lab/addworkname','settings/billing/paymentmode/view','settings/billing/billingstatus/view','settings/planner/followup/status','settings/planner/appointment/status','settings/lab/work/status','settings/clinic/staff/add_staff_type') ? 'active' : '' }}">
        <a href="{{url('settings')}}" class="dropdown-toggle"><i class="fa fa-cog fa-fw"></i>Settings</a>
      </li>

      <li class="dropdown {{ Request::is('master','master/patient','master/patient/general','master/patient/medicalhistory','master/patient/attachments','master/inventory/materialmanagement/listview','master/inventory/materialmanagement/add','master/inventory/materialmanagement/alertsettings','master/inventory/materialmanagement/treatment','master/inventory/instrumentmanagement/listview','master/inventory/instrumentmanagement/add','master/inventory/instrumentmanagement/alertsettings','master/inventory/instrumentmanagement/treatment','master/inventory/machinemanagement/listview','master/inventory/machinemanegement/add','master/inventory/machinemanagement/treatment','master/inventory/gadgetmanagement/listview','master/inventory/gadgetmanagement/add','master/inventory/gadgetmanagement/treatment','master/vendors/lab/listview','master/vendors/lab/add','master/vendors/lab/work_specification','master/vendors/material/listview','master/vendors/material/add','master/vendors/material/work_specification','master/vendors/instrument/listview','master/vendors/instrument/add','master/vendors/instrument/work_specification','master/vendors/gadget/listview','master/vendors/gadget/add','master/vendors/gadget/work_specification','master/vendors/machine/listview','master/vendors/machine/add','master/vendors/machine/work_specification','master/vendors/maintenance/listview','master/vendors/maintenance/add','master/vendors/maintenance/work_specification','master/clinic/staff_register/listview','master/clinic/staff_register/add','master/clinic/staff_register/qualifications','master/clinic/staff_register/slot_time_details','master/clinic/staff_register/specialization','master/clinic/staff_register/primary_clinic_slot_time_details/*','master/clinic/staff_register/edit/*','master/clinic/staff_register/slot_time_details/edit/*','master/clinic/staff_register/visiting_clinic_slot_time_details/*','master/clinic/staff_register/view/*','master/clinic/staff_register/slot_time_details/view/*','master/clinic/staff_register/primary_clinic_slot_time_details/view/*','master/clinic/staff_register/visiting_clinic_slot_time_details/view/*','master/clinic/leave_management/listview','master/clinic/leave_management/add','master/clinic/leave_management/full_day/edit/*','master/clinic/leave_management/partial_leave/edit/*','master/clinic/clinic/listview','master/clinic/clinic/details','master/clinic/clinic/work_timings','master/clinic/clinic/details/edit/*','master/clinic/clinic/work_timings/edit/*','master/billing/bank/dashboard','master/billing/personal/dashboard','master/patient/general/edit','master/patient/medicalhistory/edit','master/treatment/name','master/inventory/materialmanagement/alert','master/inventory/instrumentmanagement/alert','master/inventory/materialmanagement/edit/*','master/inventory/materialmanagement/alert/edit/*','master/inventory/materialmanagement/treatment/edit/*','master/inventory/instrumentmanagement/edit/*','master/inventory/instrumentmanagement/alert/edit/*','master/inventory/instrumentmanagement/treatment/edit/*','master/inventory/machinemanagement/edit/*','master/inventory/machinemanagement/treatment/edit/*','master/inventory/gadgetmanagement/edit/*','master/inventory/gadgetmanagement/treatment/edit/*','master/vendors/lab/work_specification','master/vendors/lab/edit/*','master/vendors/lab/work_specification/edit/*','master/vendors/lab/view','master/vendors/lab/view/*','master/vendors/lab/work_specification/view/*','master/vendors/material/work_specification','master/vendors/material/edit/*','master/vendors/material/work_specification/edit/*','master/vendors/material/view/*','master/vendors/material/work_specification/view/*','master/vendors/instrument/work_specification','master/vendors/instrument/edit/*','master/vendors/instrument/work_specification/edit/*','master/vendors/instrument/view/*','master/vendors/instrument/work_specification/view/*','master/vendors/gadget/work_specification','master/vendors/gadget/edit/*','master/vendors/gadget/work_specification/edit/*','master/vendors/gadget/view/*','master/vendors/gadget/work_specification/view/*','master/vendors/gadget/work_specification','master/vendors/machine/work_specification','master/vendors/machine/edit/*','master/vendors/machine/work_specification/edit/*','master/vendors/machine/view/*','master/vendors/machine/work_specification/view/*','master/vendors/maintenance/work_specification/view/*','master/vendors/maintenance/edit/*','master/vendors/maintenance/work_specification/edit/*','master/vendors/maintenance/view/*','master/vendors/maintenance/work_specification','master/vendors/lab/add','master/vendors/material/add','master/vendors/instrument/add','master/vendors/gadget/add','master/vendors/machine/add','master/vendors/maintenance/add') ? 'active' : '' }}">
        <a href="{{ url('master') }}"><i class="fa fa-briefcase fa-fw"></i>Master</a> 
      </li>

      <li class="dropdown {{ Request::is('patient','patient/search','patient/appointment/details/*','patient/treatment/details/*','patient/patient/details/*','patient/referrals/details/*') ? 'active' : '' }}"> 
        <a class="dropdown-toggle" href="{{ url('patient') }}"><i class="fa fa-plus fa-fw"></i>Patient</a>
      </li>

      <li class="dropdown {{ Request::is('planner/viewplanner','planner/scheduling','planner/scheduling/search','planner/scheduling/appointmentdetails','planner/scheduling/treatmentinformation','planner/scheduling/patientinformation','planner/scheduling/referralsinformation','planner/scheduling/newappointment','planner/followup/appointments','planner/followup/appointments/newfollowup','planner/followup/inventory/material/newfollowup','planner/followup/inventory/instrument/newfollowup','planner/followup/inventory/labwork/newfollowup','planner/followup/maintenance/gadget/newfollowup','planner/followup/maintenance/machine/newfollowup','planner/scheduling/appointment/details/*','planner/scheduling/treatment/details/*','planner/scheduling/patient/details/*','planner/scheduling/referrals/details/*') ? 'active' : '' }}"> 
        <a href="{{url('planner/viewplanner')}}" class="dropdown-toggle" ><i class="fa fa-tachometer fa-fw"></i>Planner</a>
      </li>

      <li class="dropdown {{ Request::is('treatment','treatment/getpatientdetails/*','treatment/treatmentnotes/GetDetailsTreatmentInfo','treatment/treatmentnotes/dashboardTreatmentNotes','treatment/treatmentnotes/GetDetailsTreatmentSitting','treatment/labwork/*','treatment/treatmentnotes/general/*','treatment/treatmentnotes/showstopper/*','treatment/treatmentnotes/consultant/*','treatment/treatmentnotes/quatations/*','treatment/treatmentnotes/visitingclinic/*','treatment/treatmentnotes/findingnotes/*','treatment/sittings/*','treatment/sittings/general/*','treatment/sittings/labwork/*','treatment/sittings/labwork/add/*','treatment/sittings/attachment/*','treatment/sittings/diagonosis/*','treatment/search','treatment/getpatientdetails/*') ? 'active' : '' }}">
        <a href="{{url('treatment')}}" class="dropdown-toggle"><i class="fa fa-building-o fa-fw"></i>Treatment</a>
      </li>

      <li class="dropdown {{ Request::is('labs/labdetails','labs/trackworkdetails','labs/assign/labworkinfo','labs/assign/assignwork','labs/assign/labform','labs/assign/delivery','labs/assign/billing') ? 'active' : '' }}">
        <a href="{{ url('labs/labdetails') }}" class="dropdown-toggle"><i class="fa fa-exchange fa-fw"></i>Labs</a>
      </li> 

      <li class="dropdown {{ Request::is('inventory/material_management/listview','inventory/material_management/get_details/*','inventory/instrument_management/listview','inventory/instrument_management/get_details','inventory/instrument_management/instrument_in_lab','inventory/machine_management/listview','inventory/machine_management/get_details','inventory/gadget_management/listview','inventory/gadget_management/get_details','inventory/purchase_order/material/listview','inventory/purchase_order/material/add','inventory/purchase_order/material/vendor_details','inventory/purchase_order/material/generate_delivery','inventory/purchase_order/material/generate_billing','inventory/alert/dashboard','inventory/alert/instrument/Dashboard') ? 'active' : '' }}"> 
        <a href="{{  url('inventory/material_management/listview')}}" class="dropdown-toggle"><i class="fa fa-building fa-fw"></i>Inventory</a>
      </li>

      <li class="dropdown {{ Request::is('vendors/lab/by_vendor','vendor/lab/byvendor/trackwork','vendor/lab/byvendor/instrumentdetails','vendor/lab/bywork/workdashboard','vendors/material/byvendor','vendor/material/byvendor/trackwork','vendor/material/bywork/workdashboard','vendors/instrument/byvendor','vendor/instrument/byvendor/trackwork','vendor/instrument/bywork/workdashboard','vendors/gadget/byvendor','vendor/gadget/byvendor/trackwork','vendor/gadget/bywork/workdashboard','vendors/machine/byvendor','vendor/machine/byvendor/trackwork','vendor/machine/bywork/workdashboard','vendors/maintenance/byvendor','vendor/maintenance/byvendor/trackwork','vendor/maintenance/bywork/workdashboard') ? 'active' : '' }}">
        <a href="{{ url('vendors/lab/by_vendor') }}" class="dropdown-toggle"><i class="fa fa-send fa-fw"></i>Vendors</a>
      </li>

      <li class="dropdown {{ Request::is('maintenance/gadget/nextduemaintenance','maintenance/gadget/maintenance','maintenance/gadget/assignedmaintenance','maintenance/machine/nextduemaintenance','maintenance/machine/completedmaintenance','maintenance/machine/assignedmaintenance','maintenance/machine/nextduemaintenance','maintenance/machine/completedmaintenance','maintenance/machine/assignedmaintenance','gadget/maintenance/assignmaintenance/assignwork','gadget/maintenance/assignmaintenance/completedmaintenance','gadget/maintenance/assignmaintenance/extracharges') ? 'active' : '' }}">
        <a href="{{ url('maintenance/gadget/nextduemaintenance') }}" class="dropdown-toggle"><i class="fa fa-wrench fa-fw"></i>Maintenance</a>
      </li>

      <li class="dropdown {{ Request::is('billing/treatment','billing/treatment/prevoiusbilldetails','billing/treatment/generatebill','billing/inventory/material','billing/inventory/material/makepayment','billing/labwork','billing/labwork/getdetails','billing/labwork/material/makepayment','billing/maintenance/contractpayment','billing/maintenance/contractpayment/makepayment','billing/maintenance/serviceamount','billing/miscellaneous','billing/salary','billing/salary/viewdetails','billing/cashmanagement','billing/cashmanagement/transfercash','billing/inventory/instrument','billing/inventory/instrument/makepayment','billing/inventory/gadget','billing/inventory/gadget/makepayment','billing/inventory/machine','billing/inventory/machine/makepayment') ? 'active' : '' }}">
        <a href="{{ url('billing/treatment') }}" class="dropdown-toggle"><i class="fa fa-usd fa-fw"></i>Billing</a>
      </li>

      <li class="dropdown {{ Request::is() ? 'active' : '' }}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-clipboard fa-fw"></i>Report</a>
      </li>

    </ul>
  </div><!-- /.navbar-collapse -->

</nav>


@yield('content')

</div>
</body>
</html>
