@extends('layouts.menuNav')

@section('head')
    @parent
      @section('title')
        Settings
      @stop
@stop

@section('content')


<nav class="navbar navbar-inverse" role="navigation" style="margin-bottom: 0.5%">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-3">
    <ul style="width:100%" class="nav navbar-nav">

      <li style="margin-right:3%" class="dropdown {{ Request::is('settings/patient/dentist/adddentistname','settings/patient/dentist/adddentistname','deletedentistname','settings/patient/dentist/adddentistname','settings/patient/chair/addchairdetails','deletechairdetails') ? 'active' : '' }}"> 
        <a class="dropdown-toggle" href="{{ url('settings/patient/dentist/adddentistname') }}"></i>Patient</a>
      </li>

      <li style="margin-right:3%" class="dropdown {{ Request::is('settings/planner/followup/status','settings/planner/appointment/status') ? 'active' : '' }}"> 
        <a href="{{ url('settings/planner/followup/status')}}" class="dropdown-toggle">Planner</a>
      </li>

      <li style="margin-right:3%" class="dropdown {{ Request::is('settings/treatment/type','settings/treatment/status') ? 'active' : '' }}">
        <a href="{{ url('settings/treatment/type') }}" class="dropdown-toggle">Treatment</a>
      </li> 

      <li style="margin-right:3%" class="dropdown {{ Request::is('settings/lab/addworktype','settings/lab/addworksubtype','settings/lab/addworkname','settings/lab/work/status') ? 'active' : '' }}">
        <a href="{{ url('settings/lab/addworktype') }}" class="dropdown-toggle">Lab</a>
      </li>
      
      <li style="margin-right:3%" class="dropdown {{ Request::is('settings/inventory/materialmanagement/addmaterialtype','settings/inventory/materialmanagement/addmaterialsubtype','settings/inventory/materialmanagement/addmaterailunit','settings/inventory/instrumentmanagement/addinstrumenttype','settings/inventory/instrumentmanagement/addinstrumentsubtype','settings/inventory/instrumentmanagement/addinstrumentunit','settings/inventory/machinemanagement/addmachinetype','settings/inventory/machinemanagement/addmachinesubtype','settings/inventory/machinemanagement/addmachineunit','settings/inventory/gadgetmanagement/addgadgettype','settings/inventory/gadgetmanagement/addgadgetsubtype') ? 'active' : '' }}">
        <a href="{{ url('settings/inventory/materialmanagement/addmaterialtype') }}" class="dropdown-toggle">Inventory</a>
      </li> 

      <li style="margin-right:3%" class="dropdown {{ Request::is() ? 'active' : '' }}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Vendors</a>
      </li> 

      <li style="margin-right:3%" class="dropdown {{ Request::is() ? 'active' : '' }}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Maintenance</a>
      </li> 

      <li style="margin-right:3%" class="dropdown {{ Request::is('settings/clinic/staff/add_staff_type','master/clinic/staffregister/dashboard','master/clinic/staffregister/add','master/clinic/staffregister/qualifications','master/clinic/staffregister/slottimedetails','master/clinic/staffregister/specialization','master/clinic/leavemanagement/dashboard','master/clinic/leavemanagement/add','master/clinic/clinic/dashboard','master/clinic/clinic/details','master/clinic/clinic/worktimings') ? 'active' : '' }}">
        <a href="{{ url('settings/clinic/staff/add_staff_type') }}" class="dropdown-toggle">Clinic</a>
      </li>

      <li style="width:11%" class="dropdown {{ Request::is('settings/billing/paymentmode/view','settings/billing/billingstatus/view') ? 'active' : '' }}">
        <a href="{{ url('settings/billing/paymentmode/view') }}" class="dropdown-toggle">Billing</a>
      </li>


    </ul>
  </div><!-- /.navbar-collapse -->

  @yield('side_bar')

</nav>

@yield('main')

@stop 