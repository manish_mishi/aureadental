@extends('layouts.menuNav')

@section('head')
    @parent
      @section('title')
        Master
      @stop
@stop

@section('content')


<nav class="navbar navbar-inverse" role="navigation" style="margin-bottom: 0.5%">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-3">
    <ul style="width:100%" class="nav navbar-nav">

      <li style="margin-right:5%" class="dropdown {{ Request::is('master/patient','master/patient/general','master/patient/medicalhistory','master/patient/general/edit','master/patient/medicalhistory/edit') ? 'active' : '' }}"> 
        <a class="dropdown-toggle" href="{{ url('master/patient') }}"></i>Patient</a>
      </li>

      <li style="margin-right:5%" class="dropdown {{ Request::is() ? 'active' : '' }}"> 
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Planner</a>
      </li>

      <li style="margin-right:5%" class="dropdown {{ Request::is('master/treatment/name') ? 'active' : '' }}">
        <a href="{{ url('master/treatment/name')}}" class="dropdown-toggle">Treatment</a>
      </li> 
      
      <li style="margin-right:5%" class="dropdown {{ Request::is('master/inventory/materialmanagement/listview','master/inventory/materialmanagement/add','master/inventory/materialmanagement/alertsettings','master/inventory/materialmanagement/treatment','master/inventory/instrumentmanagement/listview','master/inventory/instrumentmanagement/add','master/inventory/instrumentmanagement/alertsettings','master/inventory/instrumentmanagement/treatment','master/inventory/machinemanagement/listview','master/inventory/machinemanegement/add','master/inventory/machinemanagement/treatment','master/inventory/gadgetmanagement/listview','master/inventory/gadgetmanagement/add','master/inventory/gadgetmanagement/treatment','master/inventory/materialmanagement/edit/*','master/inventory/materialmanagement/alert/edit/*','master/inventory/materialmanagement/treatment/edit/*','master/inventory/instrumentmanagement/alert','master/inventory/instrumentmanagement/edit/*','master/inventory/instrumentmanagement/alert/edit/*','master/inventory/instrumentmanagement/treatment/edit/*','master/inventory/machinemanagement/edit/*','master/inventory/machinemanagement/treatment/edit/*','master/inventory/gadgetmanagement/edit/*','master/inventory/gadgetmanagement/treatment/edit/*') ? 'active' : '' }}">
        <a href="{{ url('master/inventory/materialmanagement/listview') }}" class="dropdown-toggle">Inventory</a>
      </li> 

      <li style="margin-right:5%" class="dropdown {{ Request::is('master/vendors/lab/listview','master/vendors/lab/add','master/vendors/lab/work_specification','master/vendors/material/listview','master/vendors/material/add','master/vendors/material/work_specification','master/vendors/instrument/listview','master/vendors/instrument/add','master/vendors/instrument/work_specification','master/vendors/gadget/listview','master/vendors/gadget/add','master/vendors/gadget/work_specification','master/vendors/machine/listview','master/vendors/machine/add','master/vendors/machine/work_specification','master/vendors/maintenance/listview','master/vendors/maintenance/add','master/vendors/maintenance/work_specification','master/vendors/lab/work_specification','master/vendors/lab/edit/*','master/vendors/lab/work_specification/edit/*','master/vendors/lab/view/*','master/vendors/lab/view/*','master/vendors/lab/work_specification/view/*','master/vendors/material/work_specification','master/vendors/material/edit/*','master/vendors/material/work_specification/edit/*','master/vendors/material/view/*','master/vendors/material/work_specification/view/*','master/vendors/instrument/work_specification','master/vendors/instrument/edit/*','master/vendors/instrument/work_specification/edit/*','master/vendors/instrument/view/*','master/vendors/instrument/work_specification/view/*','master/vendors/gadget/work_specification','master/vendors/gadget/edit/*','master/vendors/gadget/work_specification/edit/*','master/vendors/gadget/view/*','master/vendors/gadget/work_specification/view/*','master/vendors/machine/work_specification/view/*','master/vendors/machine/edit/*','master/vendors/machine/work_specification/edit/*','master/vendors/machine/view/*','master/vendors/machine/work_specification','master/vendors/maintenance/work_specification','master/vendors/maintenance/edit/*','master/vendors/maintenance/work_specification/edit/*','master/vendors/maintenance/view/*','master/vendors/maintenance/work_specification/view/*') ? 'active' : '' }}">
        <a href="{{ url('master/vendors/lab/listview') }}" class="dropdown-toggle">Vendors</a>
      </li> 

      <li style="margin-right:5%" class="dropdown {{ Request::is() ? 'active' : '' }}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Maintenance</a>
      </li> 

      <li style="margin-right:5%" class="dropdown {{ Request::is('master/clinic/staff_register/listview','master/clinic/staff_register/add','master/clinic/staff_register/qualifications','master/clinic/staff_register/slot_time_details','master/clinic/staff_register/specialization','master/clinic/staff_register/view/*','master/clinic/staff_register/slot_time_details/view/*','master/clinic/staff_register/primary_clinic_slot_time_details/view/*','master/clinic/staff_register/visiting_clinic_slot_time_details/view/*','master/clinic/leave_management/listview','master/clinic/leave_management/add','master/clinic/leave_management/full_day/edit/*','master/clinic/leave_management/partial_leave/edit/*','master/clinic/clinic/listview','master/clinic/clinic/details','master/clinic/clinic/work_timings','master/clinic/clinic/details/edit/*','master/clinic/clinic/work_timings/edit/*','master/clinic/staff_register/primary_clinic_slot_time_details/*','master/clinic/staff_register/edit/*','master/clinic/staff_register/slot_time_details/edit/*','master/clinic/staff_register/visiting_clinic_slot_time_details/*') ? 'active' : '' }}">
        <a href="{{ url('master/clinic/staff_register/listview') }}" class="dropdown-toggle">Clinic</a>
      </li>

      <li style="margin-right:5%" class="dropdown {{ Request::is('master/billing/bank/dashboard','master/billing/personal/dashboard') ? 'active' : '' }}">
        <a href="{{ url('master/billing/bank/dashboard') }}" class="dropdown-toggle">Billing</a>
      </li>


    </ul>
  </div><!-- /.navbar-collapse -->

  @yield('side_bar')

</nav>

@yield('main')

@stop 