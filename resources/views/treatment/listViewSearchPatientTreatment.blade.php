@extends('layouts.menuNav')

@section('title')
List View -> Search
@stop

@section('content')

<br><br>
<div class="container">

  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->

  {!! Form::open(array('route' => 'patientBasicDetails','class' => 'form')) !!}

  <div class="row">
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">  
    {!! Form::label('Name', null, array('class'=>'control-label')) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      {!! Form::text('name', $name,array('class'=>'form-control','id'=>'name')) !!}
    </div>
  </div>
  
  <br>
  <div class="row">
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
    {!! Form::label('Cell No', null, array('class'=>'control-label')) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      {!! Form::text('cell_no', $cell_no,array('class'=>'form-control','id'=>'cell_no')) !!}
    </div>

    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
      <button type="submit" class="btn btn-default">Search</button>
    </div>
    
  </div>
  {!! Form::close() !!}

@if($patient == "")
  <br>
  <div class="panel panel-default table-responsive">
    <div class="panel-body"><p align="center">No entries found.</p></div>
  </div>
@else
  <br><br>
  <div class="panel panel-default filterable table-responsive">
   <table class="table table-hover">
    <thead class="panel-info">
      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Cell No" disabled></th>
        <th><input type="text" class="form-control" placeholder="Gender" disabled></th>
        <th><input type="text" class="form-control" placeholder="Email" disabled></th>
        <th><input type="text" class="form-control" placeholder="Address" disabled></th>
        <th><input type="text" class="form-control" placeholder="Action" disabled></th>
        <!-- <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th> -->
      </tr>
    </thead>
    <tbody>
      @foreach($patient as $pat)
        <tr>
         <td>{{$pat->name}}</td>
         <td>{{$pat->cell_no}}</td>
         <td>{{$pat->gender}}</td>
         <td>{{$pat->email}}</td>
         <td>{{$pat->address}}</td>
         <td colspan="2">
           <a href="{{url('treatment/getpatientdetails',[$pat->id])}}"><button type="button" class="btn btn-xs .btn-default"><span class="glyphicon glyphicon-eye-open"></span>Get Details</button></a>
         </td>
       </tr>
       @endforeach     
  </tbody>
</table>
</div>
@endif

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>

@stop