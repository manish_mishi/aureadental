@extends('layouts.menuNav')

@section('title')
List View
@stop

@section('content')

<br><br>
<div class="container">

  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->

  {!! Form::open(array('route' => 'patientBasicDetails','class' => 'form')) !!}

  <div class="row">
    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">  
    {!! Form::label('Name', null, array('class'=>'control-label')) !!}
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
      {!! Form::text('name', null,array('class'=>'form-control','id'=>'name')) !!}
    </div>
  </div>
  
  <br>
  <div class="row">
    <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
    {!! Form::label('Cell No', null, array('class'=>'control-label')) !!}
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
      {!! Form::text('cell_no', null,array('class'=>'form-control','id'=>'cell_no')) !!}
    </div>

    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
      <button type="submit" class="btn btn-default">Search</button>
    </div>
    
  </div>
  {!! Form::close() !!}


<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>

<script type="text/javascript">
  $('#name').focus();
</script>

@stop