@extends('layouts.menuNav')

@section('title')
List View
@stop

@section('content')

<div class="container">
  <br>
  <div id="patDetails">
   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Name', null, array('class'=>'control-label pull-right')) !!}
    </div>
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
      {!! Form::text('name', $patient[0]->name,array('class'=>'form-control','id'=>'name','readonly')) !!}
    </div>

    <br><br>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Cell', null, array('class'=>'control-label pull-right')) !!}
    </div>
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
      {!! Form::text('cell', $patient[0]->cell_no,array('class'=>'form-control','id'=>'cell','readonly')) !!}
    </div>
  </div>

  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
   <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 form-group">
    {!! Form::label('Medical History', null, array('class'=>'control-label pull-right')) !!}
  </div>
  <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 form-group">
    {!! Form::textarea('medical_history', $patientHistory[0]->medical_history, array('rows'=>'5', 'cols'=>'10','class'=>'form-control','id'=>'medical_history','readonly')) !!}
  </div>
</div>
</div><!-- Close div id="patDetails"-->

<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
  <br><br><br><br><br><br>
  <a href="{{url('treatment/treatmentnotes',[$patient[0]->id])}}"><button type="button" class="btn btn-success btn-md pull-right"><i class="fa fa-plus fa-fw"></i>New Treatment</button></a>
</div>

<br><br><br><br><br><br><br>
<caption><b>Treatment Info</b></caption>
<div class="panel panel-default filterable table-responsive">

 <table class="table table-hover">
  <thead class="panel-info">
    <tr class="filters">
      <th><input type="text" class="form-control" placeholder="Dentist" disabled></th>
      <th><input type="text" class="form-control" placeholder="Consultant" disabled></th>
      <th><input type="text" class="form-control" placeholder="Date" disabled></th>
      <th><input type="text" class="form-control" placeholder="Lab Status" disabled></th>
      <th><input type="text" class="form-control" placeholder="Treatment Status" disabled></th>
      <th>Action</th>
      <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
    </tr>
  </thead>
  <tbody>
    @foreach($patientTreatmentDetails as $patTreatDet)
    @if($patTreatDet->patient_treatment_id != null && $patTreatDet->status_id != null)
   <tr>
    <td>{{$patTreatDet->dentist_name}}</td>
    <td>{{$patTreatDet->consultant_name}}</td>
    <td></td>
    <td></td>
    <td>{{$patTreatDet->status}}</td>
    <td colspan='2'>
      @if($patTreatDet->status != 'Open')
        <a href="{{url('treatment/treatmentnotes/general/view',[$patTreatDet->patient_treatment_id])}}"><button class="btn btn-xs .btn-primary">View Details</button></a>
      @else
      <a href="{{url('treatment/treatmentnotes/general',[$patTreatDet->patient_treatment_id])}}"><button class="btn btn-xs btn-primary">Edit Details</button></a>
      @endif
    </td>
  </tr>
  @endif
  @endforeach
</tbody>
</table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div><!-- Close div container-->


@stop