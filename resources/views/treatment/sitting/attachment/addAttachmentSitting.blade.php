@extends('layouts.menuNav')

@section('title')
Treatment -> Sitting -> Attachment
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}" class="active">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/general',[$sittingsId]) }}">General</a></li>
      <li style="width:24%" role="presentation"><a href="{{ url('treatment/sittings/labwork',[$sittingsId]) }}">Lab Work</a></li> 
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('treatment/sittings/attachment',[$sittingsId]) }}">Attachments</a></li>
      <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/diagonosis',[$sittingsId]) }}">Diagnosis</a></li>

    </ul>
  </div>

  {!! Form::open(array('route'=>'storeAttachmentSitting','class'=>'form')) !!}
  <div class="row">
    
  </div><br>

  <div class="row">
    
  </div><br>
  <div class="row">
    
  </div><br>
  <div class="row">
    
  </div>


  <br><br>
  <div class="row">
    <div>
      {!! Form::hidden('settings_id', $sittingsId) !!}
      <button type="submit" class="btn btn-warning" style="margin-left:81%">&#x2714; Save</button>
      <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
    </div>
  </div><br>
  {!! Form::close() !!}


  <!-- back to top of the page -->
  <p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
  </p>

</div>
</div>


@stop