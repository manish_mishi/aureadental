@extends('layouts.menuNav')

@section('title')
Treatment -> Sitting -> General
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

@if($view == "Open")
      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}" class="active">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
      </li>
@else
     <li>
        <a href="{{ url('treatment/treatmentnotes/general/view',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings/view',[$patientTreatmentId]) }}" class="active">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork/view',[$patientTreatmentId]) }}" >Lab Work</a>
      </li> 
@endif
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('treatment/sittings/general/view',[$sittingsId]) }}">General</a></li>
      <li style="width:24%" role="presentation"><a href="{{ url('treatment/sittings/labwork/view',[$sittingsId]) }}">Lab Work</a></li> 
      <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/attachment/view',[$sittingsId]) }}">Attachments</a></li>
      <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/diagonosis/view',[$sittingsId]) }}">Diagnosis</a></li>

    </ul>
  </div>

   <div class="row">
    {!! Form::label('Appointment Id', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('appointment_id', $generalSittingsDetails->appointment_id,array('class'=>'form-control','id'=>'date','readonly')) !!}
   </div>
   {!! Form::label('Date', null, array('class'=>'col-xs-3 control-label')) !!}
   <div class="col-xs-3">
    {!! Form::text('date', $generalSittingsDetails->date,array('class'=>'form-control','id'=>'date','readonly')) !!}
  </div>
</div>

<br>
<div class="row">
  {!! Form::label('time', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    {!! Form::text('time', $generalSittingsDetails->slot,array('class'=>'form-control','id'=>'time','readonly')) !!}
  </div>
  {!! Form::label('Material List', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    {!! Form::text('material_id', $generalSittingsDetails->material_name,array('class'=>'form-control','id'=>'date','readonly')) !!}
  </div>
</div>

<br>
<div class="row">
  {!! Form::label('Instrument List', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    {!! Form::text('Instrument', $generalSittingsDetails->instrument_name, array('class'=>'form-control','id'=>'date','readonly')) !!}
  </div>
  {!! Form::label('Gadget List', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    {!! Form::text('gadget', $generalSittingsDetails->gadget_name, array('class'=>'form-control','id'=>'date','readonly')) !!}
  </div>
</div>


<br><br>
<div class="row">
  @if($view == "Open")
  <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}"><button type="submit" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
  @else
  <a href="{{ url('treatment/sittings/view',[$patientTreatmentId]) }}"><button type="submit" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
  @endif
</div><br>


<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>


@stop