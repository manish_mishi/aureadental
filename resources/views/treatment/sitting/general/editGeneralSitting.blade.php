@extends('layouts.menuNav')

@section('title')
Treatment -> Sitting -> General
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}" class="active">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('treatment/sittings/general',[$sittingsId]) }}">General</a></li>
      <li style="width:24%" role="presentation"><a href="{{ url('treatment/sittings/labwork',[$sittingsId]) }}">Lab Work</a></li> 
      <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/attachment',[$sittingsId]) }}">Attachments</a></li>
      <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/diagonosis',[$sittingsId]) }}">Diagnosis</a></li>

    </ul>
  </div>

  {!! Form::open(array('route'=>'updateGeneralSitting','class'=>'form')) !!}
   <div class="row">
    {!! Form::label('Appointment Id', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <select class="form-control" name="appointment_id" id ='appointmentId'>
       <option value="{{$generalSittingsDetails->appointment_id}}">{{$generalSittingsDetails->appointment_id}}</option>
       @foreach($appointmentDetails as $appointment)
       <option value="{{$appointment->appointmentId}}">{{$appointment->appointmentId}}</option>
       @endForeach
     </select>
   </div>
   {!! Form::label('Date', null, array('class'=>'col-xs-3 control-label')) !!}
   <div class="col-xs-3">
    {!! Form::text('date', $generalSittingsDetails->date,array('class'=>'form-control','id'=>'date','readonly')) !!}
  </div>
</div>

<br>
<div class="row">
  {!! Form::label('time', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    {!! Form::text('time', $generalSittingsDetails->slot,array('class'=>'form-control','id'=>'time','readonly')) !!}
  </div>
  {!! Form::label('Material List', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    <select class="form-control" name="material_id" id ='materialId'>
       <option value="{{$generalSittingsDetails->material_name_id}}">{{$generalSittingsDetails->material_name}}</option>
       @foreach($materialName as $material)
       <option value="{{$material->id}}">{{$material->material_name}}</option>
       @endForeach
     </select>
  </div>
</div>

<br>
<div class="row">
  {!! Form::label('Instrument List', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    <select class="form-control" name="instrument_id" id ='instrumentId'>
       <option value="{{$generalSittingsDetails->instrument_name_id}}">{{$generalSittingsDetails->instrument_name}}</option>
       @foreach($instrumentName as $instrument)
       <option value="{{$instrument->id}}">{{$instrument->instrument_name}}</option>
       @endForeach
     </select>
  </div>
  {!! Form::label('Gadget List', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    <select class="form-control" name="gadget_id" id ='gadgetId'>
       <option value="{{$generalSittingsDetails->gadget_id}}">{{$generalSittingsDetails->gadget_name}}</option>
       @foreach($gadgetName as $gadget)
       <option value="{{$gadget->id}}">{{$gadget->gadget_name}}</option>
       @endForeach
     </select>
  </div>
</div>


<br><br>
<div class="row">
  {!! Form::hidden('settings_id', $sittingsId) !!}
  {!! Form::hidden('settings_general_id', $generalSittingsDetails->id) !!}
  <div>
    <button type="submit" class="btn btn-warning" style="margin-left:81%">&#x2714; Save</button>
    <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
  </div>
</div><br>
{!! Form::close() !!}

<script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(document).ready(function(){

  var inputDate = document.getElementById('date');
  var inputTime = document.getElementById('time');

  $("#appointmentId").change(function(){

    var appointmentID=$(this).val();

    if(appointmentID != "")
    {
      $.ajax({
        method: "POST",
        url: '{{url("treatment/sittings/general/")}}'+"/"+appointmentID,
        success : function(data){
          /*$("#mat_subtype").val(data);*/
          for (var i in data) {
            $(inputDate).val(data[i]['date']);
            $(inputTime).val(data[i]['slot']);
          }

        }

      });
    }
    else
      {
      $(inputDate).val("");
      $(inputTime).val("");
    }

    
  });


});


</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>


@stop