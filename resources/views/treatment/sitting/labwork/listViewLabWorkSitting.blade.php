@extends('layouts.menuNav')

@section('title')
Treatment -> Sitting -> LabWork
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}" class="active">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/treatmentnotes/GetDetailsTreatmentLabWork') }}" >Lab Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="panel panel-info">
      <ul style="width:100%" class="nav nav-pills">
        <li style="width:24%" role="presentation" ><a href="{{ url('treatment/sittings/general',[$sittingsId]) }}">General</a></li>
        <li style="width:25%" role="presentation" class="active"><a href="{{ url('treatment/sittings/labwork',[$sittingsId]) }}">Lab Work</a></li> 
        <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/attachment',[$sittingsId]) }}">Attachments</a></li>
        <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/diagonosis',[$sittingsId]) }}">Diagnosis</a></li>

      </ul>
    </div>

    <br>
    <div>
      <a href="{{url('treatment/sittings/labwork/add',[$sittingsId])}}" class="btn btn-success pull-right" style=""><i class="fa fa-plus fa-fw"></i>New LabWork</a>
    </div>

    <br><br>
    <caption><b>Lab Work Info</b></caption>
    <div class="panel panel-default filterable table-responsive">

      <table class="table table-hover">
        <thead class="panel-info">
          <tr class="filters">
            <th><input type="text" class="form-control" placeholder="Lab ID" disabled></th>
            <th><input type="text" class="form-control" placeholder="Date" disabled></th>
            <th><input type="text" class="form-control" placeholder="Dentist/Consultant" disabled></th>
            <th><input type="text" class="form-control" placeholder="Lab Status" disabled></th>
            <th>Action</th>
            <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
          </tr>
        </thead>
        <tbody>
          @foreach($labWork as $work)
          <tr>
            <td>{{$work->id}}</td>
            <td>{{$work->job_date}}</td>
            <td>{{$work->observation}}</td>
            <td>{{$work->status}}</td>
            <td>
              @foreach($labWorkStatus as $workstatus)
              @if($workstatus->status == "Re-work")
              {!! Form::open(array('route'=>'makeStatusRework','class' => 'form')) !!}
              {!! Form::hidden('lab_work_id', $work->id) !!}
              {!! Form::hidden('lab_work_status_id', $workstatus->id) !!}
              {!! Form::hidden('settings_id', $work->sittings_id) !!}
              <button type="submit" class="btn btn-xs btn-rework"><i class="fa fa-pencil fa-fw"></i>Re-Work</button>
              {!! Form::close() !!}
              @endif
              @endforeach
            </td>
            <td>
              @foreach($labWorkStatus as $workstatus)
              @if($workstatus->status == "Completed")
              {!! Form::open(array('route'=>'makeStatusComplete','class' => 'form')) !!}
              {!! Form::hidden('lab_work_id', $work->id) !!}
              {!! Form::hidden('lab_work_status_id', $workstatus->id) !!}
              {!! Form::hidden('settings_id', $work->sittings_id) !!}
              <button type="submit" class="btn btn-xs btn-complete"><i class="fa fa-check fa-fw"></i>Complete</button>
              {!! Form::close() !!}
              @endif
              @endforeach
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    <!-- back to top of the page -->
    <p id="backTop" style="display: none;">
      <a href="#top"><span></span>Back to Top</a>
    </p>

  </div>
</div>


@stop