@extends('layouts.menuNav')

@section('title')
Treatment -> Sitting -> Add Lab Work
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

@if($view == "Open")
      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}" class="active">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
      </li>
@else
     <li>
        <a href="{{ url('treatment/treatmentnotes/general/view',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings/view',[$patientTreatmentId]) }}" class="active">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork/view',[$patientTreatmentId]) }}" >Lab Work</a>
      </li> 
@endif      
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="">
     <div class="panel panel-info">
       <ul style="width:100%" class="nav nav-pills">
        <li style="width:24%" role="presentation" ><a href="{{ url('treatment/sittings/general/view',[$sittingsId]) }}">General</a></li>
        <li style="width:25%" role="presentation" class="active"><a href="{{ url('treatment/sittings/labwork/view',[$sittingsId]) }}">Lab Work</a></li> 
        <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/attachment/view',[$sittingsId]) }}">Attachments</a></li>
        <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/diagonosis/view',[$sittingsId]) }}">Diagnosis</a></li>

      </ul>
    </div>
  </div>

  <div class="row">
    {!! Form::label('Job Date', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('job_date', $labWork->job_date, array('class'=>'form-control','readonly')) !!}
    </div>
    {!! Form::label('Status', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('status', $labWork->status, array('class'=>'form-control','readonly')) !!}
    </div>
  </div>

  <br>
  <div class="row">
    {!! Form::label('Delivery date', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('delivery_date', $labWork->delivery_date, array('class'=>'form-control','readonly')) !!}
    </div>
    {!! Form::label('Observation', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('observation', $labWork->observation,array('class'=>'form-control','readonly')) !!}
    </div>
  </div>

  <br>
  <div class="row">
    {!! Form::label('Work Type', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('work_type', $labWork->name,array('class'=>'form-control','readonly')) !!}
    </div>
    {!! Form::label('Remarks', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('remarks', $labWork->remarks,array('class'=>'form-control','readonly')) !!}
    </div>
  </div>

    <br><br>
    <div class="row">
    @if($view == "Open")
      <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
    @else
      <a href="{{ url('treatment/sittings/view',[$patientTreatmentId]) }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a> 
    @endif
      <a href="{{ url('treatment/sittings/labwork/view',[$sittingsId]) }}"><button type="button" class="btn btn-success pull-right" style="margin-right:1%"><i class="fa fa-arrow-left fa-fw"></i>Back</button></a>
    </div><br>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>


@stop