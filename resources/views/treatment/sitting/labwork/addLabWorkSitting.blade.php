@extends('layouts.menuNav')

@section('title')
Treatment -> Sitting -> Add Lab Work
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}" class="active">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="">
     <div class="panel panel-info">
       <ul style="width:100%" class="nav nav-pills">
        <li style="width:24%" role="presentation" ><a href="{{ url('treatment/sittings/general',[$sittingsId]) }}">General</a></li>
        <li style="width:25%" role="presentation" class="active"><a href="{{ url('treatment/sittings/labwork',[$sittingsId]) }}">Lab Work</a></li> 
        <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/attachment',[$sittingsId]) }}">Attachments</a></li>
        <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/diagonosis',[$sittingsId]) }}">Diagnosis</a></li>

      </ul>
    </div>
  </div>

  {!! Form::open(array('route'=>'addlabWorkSitting','class'=>'form')) !!}

  <div class="row">
    {!! Form::label('Job Date', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <input data-provide="datepicker" data-date-format="dd/mm/yy" class="form-control" name="job_date" value="<?php echo date('d/m/y');?>">
    </div>
    {!! Form::label('Status', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <select class="form-control" name="lab_work_status_id" id ='labWorkStatusId'>
       <option value>Select</option>
       @foreach($labWorkStatus as $status)
       <option value="{{$status->id}}">{{$status->status}}</option>
       @endForeach
     </select>
   </div>
 </div>

 <br>
 <div class="row">
  {!! Form::label('Delivery date', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    <input data-provide="datepicker" data-date-format="dd/mm/yy" class="form-control" name="delivery_date" value="<?php echo date('d/m/y');?>">
  </div>
  {!! Form::label('Observation', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    {!! Form::text('observation', null,array('class'=>'form-control')) !!}
  </div>
</div>

<br>
<div class="row">
  {!! Form::label('Work Type', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    <select class="form-control" name="work_type_id" id ='workTypeId' required>
     <option value>Select</option>
     @foreach($workType as $work)
     <option value="{{$work->id}}">{{$work->name}}</option>
     @endForeach
   </select>
 </div>
 {!! Form::label('Remarks', null, array('class'=>'col-xs-3 control-label')) !!}
 <div class="col-xs-3">
  {!! Form::text('remarks', null,array('class'=>'form-control')) !!}
</div>
</div>


<br><br>
<div class="row">
  {!! Form::hidden('settings_id', $sittingsId) !!}
  <div>
    <button type="submit" class="btn btn-warning" style="margin-left:81%">&#x2714; Save</button>
    <a href="{{ url('treatment/sittings/labwork',[$sittingsId]) }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
  </div>
</div><br>
{!! Form::close() !!}


<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>


@stop