@extends('layouts.menuNav')

@section('title')
Treatment -> Sitting -> LabWork
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

@if($view == "Open")
      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}" class="active">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
      </li>
@else
     <li>
        <a href="{{ url('treatment/treatmentnotes/general/view',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings/view',[$patientTreatmentId]) }}" class="active">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork/view',[$patientTreatmentId]) }}" >Lab Work</a>
      </li> 
@endif
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="panel panel-info">
      <ul style="width:100%" class="nav nav-pills">
        <li style="width:24%" role="presentation" ><a href="{{ url('treatment/sittings/general/view',[$sittingsId]) }}">General</a></li>
        <li style="width:25%" role="presentation" class="active"><a href="{{ url('treatment/sittings/labwork/view',[$sittingsId]) }}">Lab Work</a></li> 
        <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/attachment/view',[$sittingsId]) }}">Attachments</a></li>
        <li style="width:25%" role="presentation"><a href="{{ url('treatment/sittings/diagonosis/view',[$sittingsId]) }}">Diagnosis</a></li>

      </ul>
    </div>

    <br><br>
    <caption><b>Lab Work Info</b></caption>
    <div class="panel panel-default filterable table-responsive">

      <table class="table table-hover">
        <thead class="panel-info">
          <tr class="filters">
            <th><input type="text" class="form-control" placeholder="ReWork-ID" disabled></th>
            <th><input type="text" class="form-control" placeholder="Date" disabled></th>
            <th><input type="text" class="form-control" placeholder="Dentist/Consultant" disabled></th>
            <th><input type="text" class="form-control" placeholder="Lab Status" disabled></th>
            <th>Action</th>
            <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
          </tr>
        </thead>
        <tbody>
          @foreach($labWork as $work)
          <tr>
            <td>{{$work->id}}</td>
            <td>{{$work->job_date}}</td>
            <td>{{$work->observation}}</td>
            <td>{{$work->status}}</td>
            <td colspan="2">
              <a href="{{url('treatment/sittings/labwork/add/view',[$work->id])}}" class="btn btn-xs btn-default" style=""><span class="glyphicon glyphicon-eye-open"></span>View Details</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>


@stop