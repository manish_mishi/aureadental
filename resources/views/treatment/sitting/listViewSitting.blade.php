@extends('layouts.menuNav')

@section('title')
Treatment -> Sitting
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}" class="active">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

<br><br>
<div>
  <a href="{{url('treatment/sittings/create',[$patientTreatmentId])}}" class="btn btn-success pull-right" style=""><i class="fa fa-plus fa-fw"></i>New Sitting</a>
</div>
    
    <br>
    <div class="col-xs-12"><label>Sittings Info</label></div><br>
    <div class="panel panel-default filterable table-responsive">
     <table class="table table-hover">
      <thead class="panel-info">
        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="Dentist" disabled></th>
          <th><input type="text" class="form-control" placeholder="Consultant" disabled></th>
          <th><input type="text" class="form-control" placeholder="Date" disabled></th>
          <th><input type="text" class="form-control" placeholder="Lab Status" disabled></th>
          <th>Action</th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
        </tr>
      </thead>
      <tbody>
        @if($sittingsDetails != null)
        @foreach($sittingsDetails as $sittings)
       <tr>
        <td>{{$sittings->dentist_name}}</td>
        <td>{{$sittings->consultant}}</td>
        <td>{{$sittings->date}}</td>
        <td>
          <table class="table table-hover">
        @foreach($labWork as $lab)
        @if($lab->sittings_id == $sittings->sittings_id)
        <tr>
        <td>{{$lab->status}}</td>
        </tr>
        @endif
        @endforeach
        </table>
        </td>
        <td>
          <a href="{{url('treatment/sittings/general',[$sittings->sittings_id])}}" class="btn btn-xs btn-primary" style=""><i class="fa fa-edit fa-fw"></i>Edit Details</a>
        </td>
        <td>
          <a href="{{url('treatment/sittings/general/view',[$sittings->sittings_id])}}" class="btn btn-xs btn-default" style=""><span class="glyphicon glyphicon-eye-open"></span>View Details</a>
        </td>
      </tr>
      @endforeach
      @endif
    </tbody>
  </table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>


@stop