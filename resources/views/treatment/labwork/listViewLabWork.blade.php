@extends('layouts.menuNav')

@section('title')
Treatment -> LabWork
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" class="active">Lab Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="col-xs-12"><label>Lab Work Info</label></div><br>
    <div class="panel panel-default filterable table-responsive">

      <table class="table table-hover">
        <thead class="panel-info">
          <tr class="filters">
            <th><input type="text" class="form-control" placeholder="ReWork-ID" disabled></th>
            <th><input type="text" class="form-control" placeholder="Date" disabled></th>
            <th><input type="text" class="form-control" placeholder="Dentist/Consultant" disabled></th>
            <th><input type="text" class="form-control" placeholder="Lab Status" disabled></th>
            <th>Action</th>
            <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
          </tr>
        </thead>
        <tbody>
          @if($labWork != null)
          @foreach($labWork as $work)
          <tr>
            <td>{{$work->id}}</td>
            <td>{{$work->job_date}}</td>
            <td>{{$work->observation}}</td>
            <td>{{$work->status}}</td>
            <td colspan="2">
              <a href="{{url('treatment/labwork/add/view',[$work->id])}}" class="btn btn-xs btn-default" style=""><span class="glyphicon glyphicon-eye-open"></span>View Details</a>
            </td>
          </tr>
          @endforeach
          @endif
        </tbody>
      </table>
    </div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>


@stop