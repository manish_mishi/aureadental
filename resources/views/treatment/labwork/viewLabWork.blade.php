@extends('layouts.menuNav')

@section('title')
Treatment -> Lab Work -> View
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

@if($view == "Open")
      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" class="active">Lab Work</a>
      </li>
@else
      <li>
        <a href="{{ url('treatment/treatmentnotes/general/view',[$patientTreatmentId]) }}">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings/view',[$patientTreatmentId]) }}">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork/view',[$patientTreatmentId]) }}" class="active">Lab Work</a>
      </li> 
@endif
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

<br><br>
  <div class="row">
    {!! Form::label('Job Date', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('job_date', $labWork->job_date, array('class'=>'form-control','readonly')) !!}
    </div>
    {!! Form::label('Status', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('status', $labWork->status, array('class'=>'form-control','readonly')) !!}
    </div>
  </div>

  <br>
  <div class="row">
    {!! Form::label('Delivery date', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('delivery_date', $labWork->delivery_date, array('class'=>'form-control','readonly')) !!}
    </div>
    {!! Form::label('Observation', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('observation', $labWork->observation,array('class'=>'form-control','readonly')) !!}
    </div>
  </div>

  <br>
  <div class="row">
    {!! Form::label('Work Type', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('work_type', $labWork->name,array('class'=>'form-control','readonly')) !!}
    </div>
    {!! Form::label('Remarks', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('remarks', $labWork->remarks,array('class'=>'form-control','readonly')) !!}
    </div>
  </div>

<br><br>
<div class="row">
  @if($view == "Open")
  <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}"><button type="button" class="btn btn-success pull-right"><i class="fa fa-arrow-left fa-fw"></i>Back</button></a>
  @else
  <a href="{{ url('treatment/labwork/view',[$patientTreatmentId]) }}"><button type="button" class="btn btn-success pull-right"><i class="fa fa-arrow-left fa-fw"></i>Back</button></a>
  @endif
</div><br> 

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>


@stop