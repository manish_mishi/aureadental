@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
          
            <li>
              <a href="{{ url('treatment/treatmentnotes/GetDetailsTreatmentInfo') }}">Treatment Notes</a>
            </li>

            <li>
              <a href="{{ url('treatment/treatmentnotes/GetDetailsTreatmentSitting') }}">Sitting</a>
            </li>

            <li>
              <a href="{{ url('treatment/treatmentnotes/GetDetailsTreatmentLabWork') }}" class="active">Lab Work</a>
            </li>
          </ul>
        </div>
        <!-- /.sidebar-collapse -->
      </div>
      <a href="{{url('treatment/sittings/newsittings/general/dashboard')}}" class="btn btn-success pull-right col-xs-2" style="">New Sitting</a>

<br><br><br><br><br>
      <div id="wrapper">
		<div id="page-wrapper">
			<div class="row marketing"><br>
				<div class="col-xs-12"><label>Lab Work Info</label></div><br>
				<div class="panel panel-default filterable">
           		<table class="table">
            		<thead>
              		<tr class="filters">
                		<th><input type="text" class="form-control" placeholder="Sitting ID" disabled></th>
                		<th><input type="text" class="form-control" placeholder="Job ID" disabled></th>
                		<th><input type="text" class="form-control" placeholder="Re-work ID" disabled></th>
                		<th><input type="text" class="form-control" placeholder="Date" disabled></th>
                		<th><input type="text" class="form-control" placeholder="Dentist/Consultant" disabled></th>
                		<th><input type="text" class="form-control" placeholder="Lab Status" disabled></th>
                		<th><input type="text" class="form-control" placeholder="" disabled></th>
                		
              		</tr>
            		</thead>
            		<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>Open</td>
						<td><a href="#">View Details</a></td>
               		</tr>
               		<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>Completed</td>
						<td><a href="#">View Details</a></td>
               		</tr>
               		<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>Re-work</td>
						<td><a href="#">View Details</a></td>
               		</tr>
            		</tbody>
          		</table>
        	</div>

			</div>
		</div>
	</div>


@stop