@extends('layouts.menuNav')

@section('title')
Treatment -> General
@stop

@section('content')

<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>

<div class="navbar sidebar" id="menu">
  <div class="sidebar-nav navbar-collapse" role="navigation" id="bs-sidebar-navbar-collapse-1">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}" class="active">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:13%" role="presentation" class="active"><a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">General</a></li>
      <li style="width:20%" role="presentation"><a href="{{ url('treatment/treatmentnotes/showstopper',[$patientTreatmentId]) }}">Showstopper</a></li> 
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/consultant',[$patientTreatmentId]) }}">Consultant</a></li>
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/quatations',[$patientTreatmentId]) }}">Quotations</a></li>
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/visitingclinic',[$patientTreatmentId]) }}">Visiting Clinic</a></li>
      <li style="width:17%" role="presentation"><a href="{{ url('treatment/treatmentnotes/findingnotes',[$patientTreatmentId]) }}">Finding Notes</a></li> 
    </ul>
  </div>


  {!! Form::open(array('route'=>'storeGeneralTreamentNotes','class'=>'form')) !!}
  <div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Treatment Type', null, array('class'=>'control-label')) !!}
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
      <select class="form-control" name="treatment_type_id" id ='treatmentType'>
        <option value>Select</option>
        @foreach($treatmentType as $treatType)
        <option value="{{$treatType->id}}">{{$treatType->name}}</option>
        @endForeach
      </select>
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
     {!! Form::label('Status', null, array('class'=>'control-label')) !!}
   </div>
   <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
    <select class="form-control" name="status_id" id ='status'>
      <option value>Select</option>
      @foreach($status as $stat)
      <option value="{{$stat->id}}">{{$stat->status}}</option>
      @endForeach
    </select>
  </div>
</div>

<br>
<div class="row">
  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
    {!! Form::label('Treatment Name', null, array('class'=>'control-label')) !!}
  </div>
  <div class="col-xs-3">
    <select class="form-control" name="treatment_name_id" id ='treatmentName'>
      <option value>Select</option>
    </select>
  </div>
  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
   {!! Form::label('Clinic', null, array('class'=>'control-label')) !!}
 </div>
 <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
  <select class="form-control" name="clinic_id" id ='clinic'>
   @foreach($clinics as $clinic)
   <option value="{{$clinic->id}}">{{$clinic->name}}</option>
   @endForeach
 </select>
</div>
</div><br>

<div class="row">
  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
    {!! Form::label('Dentist Name', null, array('class'=>'control-label')) !!}
  </div>
  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
    <select class="form-control" name="dentist_name_id" id ='dentistName'>
      <option value>Select</option>
      @foreach($dentistName as $dentistNam)
      <option value="{{$dentistNam->id}}">{{$dentistNam->dentist_name}}</option>
      @endForeach
    </select>
  </div>
  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
   {!! Form::label('Location', null, array('class'=>'control-label')) !!}
 </div>
 <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
  <select class="form-control" name="location" id ='location'>      
  </select>
</div>
</div><br>

<br><br>
<div class="row">
  {!! Form::hidden('patient_treatment_id', $patientTreatmentId) !!}
  <button type="submit" class="btn btn-warning" style="margin-left:81%">&#x2714; Save</button>
  <a href="{{ url('treatment/getpatientdetails',[$patientId]) }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
</div>
<br>
{!! Form::close() !!}

<script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(document).ready(function(){

//Ajax for treatment-type

/*var treat_type_id=$('#treatmentType').val();

if(treat_type_id != null)
{
  var selectTreatmentName = document.getElementById('treatmentName');

  $(selectTreatmentName).empty();

  $.ajax({
    method: "POST",
    url: '{{url("treatment_type/")}}'+"/"+treat_type_id,
    success : function(data){console.log(data);
      for (var i in data) {
        $(selectTreatmentName).append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
      }

    }

  });
}*/

$("#treatmentType").change(function(){

  var treat_type_id=$(this).val();
  var select = document.getElementById('treatmentName');

  $(select).empty();

  $.ajax({
    method: "POST",
    url: '{{url("treatment_type/")}}'+"/"+treat_type_id,
    success : function(data){
      for (var i in data) {
        $(select).append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
      }

    }

  });


});
//End of Ajax for treatment-type


//Ajax for clinic name
var clinicId=$('#clinic').val();

if(clinicId != null)
{
  var selectLocation = document.getElementById('location');

  $(selectLocation).empty();

  $.ajax({
    method: "POST",
    url: '{{url("treatment/treatmentnotes/general/")}}'+"/"+clinicId,
    success : function(data){
      for (var i in data) {
        $(selectLocation).append('<option value=' + data[i]['id'] + '>' + data[i]['location'] + '</option>');
      }

    }

  });
}

$("#clinic").change(function(){
  var clinicId=$(this).val();
  var select = document.getElementById('location');

  $(select).empty();

  $.ajax({
    method: "POST",
    url: '{{url("treatment/treatmentnotes/general/")}}'+"/"+clinicId,
    success : function(data){
      for (var i in data) {
        $(select).append('<option value=' + data[i]['id'] + '>' + data[i]['location'] + '</option>');
      }

    }

  });


});
//End of Ajax for clinic name

});


</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>


@stop