@extends('layouts.menuNav')

@section('title')
Treatment -> ShowStopper
@stop

@section('content')

<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
</div>

<div class="navbar sidebar">
	<div class="sidebar-nav navbar-collapse" role="navigation" id="bs-sidebar-navbar-collapse-1">
		<ul class="nav" id="side-menu">
			<li>
				<a href="{{ url('treatment/treatmentnotes/general/view',[$patientTreatmentId]) }}" class="active">Treatment Notes</a>
			</li>

			<li>
				<a href="{{ url('treatment/sittings/view',[$patientTreatmentId]) }}">Sitting</a>
			</li>

			<li>
				<a href="{{ url('treatment/labwork/view',[$patientTreatmentId]) }}" >Lab Work</a>
			</li>
		</ul>
	</div>
	<!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
	<div id="page-wrapper">

		<br>
		<div class="panel panel-info">
			<ul style="width:100%" class="nav nav-pills">
				<li style="width:13%" role="presentation"><a href="{{ url('treatment/treatmentnotes/general/view',[$patientTreatmentId]) }}">General</a></li>
				<li style="width:20%" role="presentation"><a href="{{ url('treatment/treatmentnotes/showstopper/view',[$patientTreatmentId]) }}">ShowstopperChecklist</a></li> 
				<li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/consultant/view',[$patientTreatmentId]) }}">Consultant</a></li>
				<li style="width:16%" role="presentation" class="active"><a href="{{ url('treatment/treatmentnotes/quatations/view',[$patientTreatmentId]) }}">Quotations</a></li>
				<li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/visitingclinic/view',[$patientTreatmentId]) }}">Visiting Clinic</a></li>
				<li style="width:17%" role="presentation"><a href="{{ url('treatment/treatmentnotes/findingnotes/view',[$patientTreatmentId]) }}">Finding Notes</a></li> 
			</ul>
		</div>
		@if($treatmentQuotDetails != null)
		<div class="col-xs-6">
			<div class="col-xs-4">
				{!! Form::label('Final Quotation', null, array('class'=>'control-label')) !!}
			</div>
			<div class="col-xs-8">
				<div class="col-xs-6">
					{!! Form::text('desc', $treatmentQuotDetails['0']->description, array('class'=>'form-control','id'=>'options','placeholder'=>'Description','readonly')) !!}
				</div>

				<div class="col-xs-6">
					{!! Form::text('cost', $treatmentQuotDetails['0']->cost, array('class'=>'form-control','id'=>'options1','placeholder'=>'Cost','readonly')) !!}
				</div>
			</div>
		</div>
		<div class="col-xs-6 pull-right">
			<div class="col-xs-5">
				{!! Form::label('Referrals', null, array('class'=>'control-label')) !!}
			</div>

			<div class="panel panel-default filterable table-responsive col-xs-7">
				<table class="table table-hover">
					<thead class="panel-info">
						<tr class="filters">
							<th><input type="text" class="form-control" placeholder="Name" disabled></th>
							<th><input type="text" class="form-control" placeholder="Cell" disabled></th>
							<!-- <th><button class="btn btn-default btn-xs btn-filter">Filter</button></th> -->
						</tr>
					</thead>
					<tbody>
						@foreach($referrals as $ref)
						<tr>
							<td>{{$ref->name}}</td>
							<td>{{$ref->cell_no}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="row">
				<a href="{{ url('treatment/getpatientdetails',[$patientId]) }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
			</div>
		</div>
		
		@endif

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>
		
	</div>
</div>

@stop