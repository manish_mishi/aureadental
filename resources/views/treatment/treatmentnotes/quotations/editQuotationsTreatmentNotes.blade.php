@extends('layouts.menuNav')

@section('title')
Treatment -> Quotations
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}" class="active">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:13%" role="presentation" ><a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">General</a></li>
      <li style="width:20%" role="presentation"><a href="{{ url('treatment/treatmentnotes/showstopper',[$patientTreatmentId]) }}">Showstopper</a></li> 
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/consultant',[$patientTreatmentId]) }}">Consultant</a></li>
      <li style="width:16%" role="presentation" class="active"><a href="{{ url('treatment/treatmentnotes/quatations',[$patientTreatmentId]) }}">Quotations</a></li>
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/visitingclinic',[$patientTreatmentId]) }}">Visiting Clinic</a></li>
      <li style="width:17%" role="presentation"><a href="{{ url('treatment/treatmentnotes/findingnotes',[$patientTreatmentId]) }}">Finding Notes</a></li> 
    </ul>
  </div>

  {!! Form::open(array('route'=>'updateQuatationsTreamentNotes','class'=>'form')) !!}
  
  <div class="row" ng-controller="quotationCtrl">

    <div class="col-xs-6">
      <div class="col-xs-3">
        {!! Form::label('Quotation', null, array('class'=>'control-label')) !!}
      </div>
      <div class="col-xs-4">
        {!! Form::text('null', null, array('class'=>'form-control','id'=>'options', 'ng-model'=>'newEmployee.desc','placeholder'=>'Description')) !!}
      </div>

      <div class="col-xs-3">
        {!! Form::text('null', null, array('class'=>'form-control','id'=>'options1', 'ng-model'=>'newEmployee.cost','placeholder'=>'Cost')) !!}
      </div>

      <button type="button" class="btn btn-info btn-md" ng-click="saveRecord()">
        <span class="glyphicon glyphicon-plus"></span> 
      </button>

      <br>
      <br>
      <div id='duplicater' ng-repeat="employee in employees">
        <div class="col-xs-2"></div>
        <div class="col-xs-5">
          <input type='text' ng-model="employee.desc" value="@{{employee.desc}}" class='form-control' id="optionsDesc" readonly/>
        </div>
        <div class="col-xs-3">
          <input type='text' ng-model="employee.cost" value="@{{employee.cost}}" class='form-control' id="optionsCost" readonly>
        </div>
        <div class="col-xs-2">
          {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger remove_duplicate','id'=>'delete_btn','ng-click'=>'delete(employee.id)')) !!}
        </div>
        <br>
        <br>
      </div>


    </div>

    <div class="col-xs-6">
      <div class="col-xs-4">
        {!! Form::label('Final Quotation', null, array('class'=>'control-label')) !!}
      </div>
      <div class="col-xs-8">
        <div class="col-xs-6">
          {!! Form::text('desc', $treatmentQuotDetails['0']->description, array('class'=>'form-control','id'=>'options','placeholder'=>'Description')) !!}
        </div>

        <div class="col-xs-6">
          {!! Form::text('cost', $treatmentQuotDetails['0']->cost, array('class'=>'form-control','id'=>'options1','placeholder'=>'Cost')) !!}
        </div>
      </div>
    </div>

  </div>

  <br>
  <div class="row">
    <br>
    <div class="col-xs-6 pull-right">
      <div class="col-xs-5">
        {!! Form::label('Referrals', null, array('class'=>'control-label')) !!}
      </div>

      <div class="panel panel-default filterable table-responsive col-xs-7">
       <table class="table table-hover">
        <thead class="panel-info">
          <tr class="filters">
            <th><input type="text" class="form-control" placeholder="Name" disabled></th>
            <th><input type="text" class="form-control" placeholder="Cell" disabled></th>
            <!-- <th><button class="btn btn-default btn-xs btn-filter">Filter</button></th> -->
          </tr>
        </thead>
        <tbody>
          @foreach($referrals as $ref)
          <tr>
           <td>{{$ref->name}}</td>
           <td>{{$ref->cell_no}}</td>
         </tr>
         @endforeach
       </tbody>
     </table>
   </div>

 </div>

</div>

{!! Form::hidden('treatment_quot_id', $treatmentQuotDetails[0]->id) !!}

<br><br><br>
<div class="row">
  {!! Form::hidden('patient_treatment_id', $patientTreatmentId) !!}
  <button type="submit" class="btn btn-warning" style="margin-left:81%">&#x2714; Save</button>
  <a href="{{ url('treatment/getpatientdetails',[$patientId]) }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
</div><br>
{!! Form::close() !!}

<script type="text/javascript">
var i = 0;

function duplicateOption() {

  var options = document.getElementById('options').value;
  var options1 = document.getElementById('options1').value;

  if(options != '' && options1 != ''){

    var original = document.getElementById('duplicater');
    original.style.display = "block";

    document.getElementById('optionsDesc').value =options;
    document.getElementById('optionsCost').value =options1;

    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicater" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);

    original.style.display = "none";

    document.getElementById('options').value="";
    document.getElementById('options1').value="";

    var dateElement = document.getElementById("duplicater"+i).getElementsByClassName('optionsDup')[0];
    dateElement.setAttribute("name","option[]");

/*    var removeElement = document.getElementById("duplicater"+i).getElementsByClassName('remove_duplicate')[0];
removeElement.setAttribute("onClick","removeduplicate(this,'"+i+"')");*/

var clickElement = document.getElementById("duplicater"+i).getElementsByClassName('remove_duplicate')[0];
clickElement.setAttribute("ng-click","OnDelete("+i+")");

}

}

function removeduplicate(element)
{
    element=element.parentNode.parentNode;//gets the id of the parent
    element.parentNode.removeChild(element);
  }
  </script>


  <!-- back to top of the page -->
  <p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
  </p>

</div>
</div>


@stop