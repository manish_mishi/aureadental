@extends('layouts.menuNav')

@section('title')
Treatment -> Visiting Clinic
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}" class="active">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:13%" role="presentation" ><a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">General</a></li>
      <li style="width:20%" role="presentation"><a href="{{ url('treatment/treatmentnotes/showstopper',[$patientTreatmentId]) }}">Showstopper</a></li> 
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/consultant',[$patientTreatmentId]) }}">Consultant</a></li>
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/quatations',[$patientTreatmentId]) }}">Quotations</a></li>
      <li style="width:16%" role="presentation" class="active"><a href="{{ url('treatment/treatmentnotes/visitingclinic',[$patientTreatmentId]) }}">Visiting Clinic</a></li>
      <li style="width:17%" role="presentation"><a href="{{ url('treatment/treatmentnotes/findingnotes',[$patientTreatmentId]) }}">Finding Notes</a></li> 
    </ul>
  </div>

  {!! Form::open(array('route'=>'storeVisitingClinicTreamentNotes','class'=>'form')) !!}

  @if($treatmentVisitDetails == null)
  <div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Visiting clinic:', null, array('class'=>'control-label')) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      <input type="checkbox" id="consul" onclick="visitingClinicRequire(this)"/>
      <snap id="consulText" class="alert-info">&nbsp;Check If it is not requied</snap>
    </div>
    <!-- <input type="checkbox"  id="consul" checked data-off-active-cls="btn-warning" data-on-active-cls="btn-primary" onclick="consultantRequire(this)"> -->
  </div>

  <br>

  <div id="display" style="display:none">

    <div class="row">
      {!! Form::label('Clinic', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        <select class="form-control" name="clinic_id" id ='clinic'>
          <option value>Select Clinic</option>
          @foreach($clinics as $clinic)
          <option value="{{$clinic->id}}">{{$clinic->name}}</option>
          @endForeach
        </select>
      </div>
      {!! Form::label('Fees', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
       {!! Form::text('fees', null, array('class'=>'form-control')) !!}
     </div>
   </div><br>

   <div class="row">
    {!! Form::label('Location', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <select class="form-control" name="location" id ='location'>      
      </select>
    </div>
    {!! Form::label('Notes', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
     <textarea name="notes" class="form-control"></textarea>
   </div>
 </div><br>

</div>

@else

<div class="row">
  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
    {!! Form::label('Visiting clinic:', null, array('class'=>'control-label')) !!}
  </div>
  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
    <input type="checkbox" id="consul" onclick="visitingClinicRequire(this)" checked/>
    <snap id="consulText" class="alert-info">&nbsp;Uncheck If it is not requied</snap>
  </div>
  <!-- <input type="checkbox"  id="consul" checked data-off-active-cls="btn-warning" data-on-active-cls="btn-primary" onclick="consultantRequire(this)"> -->
</div>

<br>

<div id="display" style="display:block">

  <div class="row">
    {!! Form::label('Clinic', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <select class="form-control" name="clinic_id" id ='clinic'>
        <option value="{{$treatmentVisitDetails[0]->clinic_id}}">{{$treatmentVisitDetails[0]->name}}</option>
        @foreach($clinics as $clinic)
        <option value="{{$clinic->id}}">{{$clinic->name}}</option>
        @endForeach
      </select>
    </div>
    {!! Form::label('Fees', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
     {!! Form::text('fees', $treatmentVisitDetails[0]->fees, array('class'=>'form-control')) !!}
   </div>
 </div><br>

 <div class="row">
  {!! Form::label('Location', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    <select class="form-control" name="location" id ='location'> 
      <option value="{{$treatmentVisitDetails[0]->location}}">{{$treatmentVisitDetails[0]->location}}</option>     
    </select>
  </div>
  {!! Form::label('Notes', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
   <textarea name="notes" class="form-control">{{$treatmentVisitDetails[0]->notes}}</textarea>
 </div>
</div><br>

</div>

{!! Form::hidden('treatment_visit_id', $treatmentVisitDetails[0]->t_treatment_visit_clinic_id) !!}

@endif

<br><br>
<div class="row">
  {!! Form::hidden('patient_treatment_id', $patientTreatmentId) !!}
  <button type="submit" class="btn btn-warning" style="margin-left:81%">&#x2714; Save</button>
  <a href="{{ url('treatment/getpatientdetails',[$patientId]) }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>

</div><br>
{!! Form::close() !!}

<script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

/*$(':checkbox').checkboxpicker();*/

function visitingClinicRequire(cb) {

  if(cb.checked == true)
  {
    document.getElementById('display').style.display="block";
    document.getElementById('consulText').innerHTML = "Uncheck If it is not requied";
  }
  else
  {
   document.getElementById('display').style.display="none"; 
   document.getElementById('consulText').innerHTML = "Check If it is requied";
 }

}

//Ajax for clinic name

$("#clinic").change(function(){
  var clinicId=$(this).val();
  var select = document.getElementById('location');

  $(select).empty();

  $.ajax({
    method: "POST",
    url: '{{url("treatment/treatmentnotes/general/")}}'+"/"+clinicId,
    success : function(data){
      for (var i in data) {
        $(select).append('<option value=' + data[i]['id'] + '>' + data[i]['location'] + '</option>');
      }

    }

  });


});
//End of Ajax for clinic name
</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>


@stop