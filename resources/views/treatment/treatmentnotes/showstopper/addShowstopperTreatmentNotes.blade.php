 @extends('layouts.menuNav')

 @section('title')
 Treatment -> Showstopper
 @stop

 @section('content')

 <div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}" class="active">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:13%" role="presentation"><a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">General</a></li>
      <li style="width:20%" role="presentation" class="active"><a href="{{ url('treatment/treatmentnotes/showstopper',[$patientTreatmentId]) }}">Showstopper</a></li> 
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/consultant',[$patientTreatmentId]) }}">Consultant</a></li>
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/quatations',[$patientTreatmentId]) }}">Quotations</a></li>
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/visitingclinic',[$patientTreatmentId]) }}">Visiting Clinic</a></li>
      <li style="width:17%" role="presentation"><a href="{{ url('treatment/treatmentnotes/findingnotes',[$patientTreatmentId]) }}">Finding Notes</a></li> 
    </ul>
  </div>

  {!! Form::open(array('route'=>'storeShowstopperTreamentNotes','class'=>'form')) !!}
  <div class="row">
    <div class="col-xs-5">
      {!! Form::label('Does this patient require showstopper?', null, array('class'=>'control-label pull-right')) !!}
    </div>
    <div class="col-xs-6 pull-left">
      {!! Form::radio('showstop','yes',true,array( 'onclick' => 'yesShowStopper(this)')) !!}Yes
      {!! Form::radio('showstop','no',true,array( 'onclick' => 'noShowStopper(this)')) !!}No
    </div>
  </div>

  <br>
  <div class="row" style="display:none" id="noticeDiv">
    <div class="col-xs-5">
      {!! Form::label('Does this patient have any notice?', null, array('class'=>'control-label pull-right')) !!}
    </div>
    <div class="col-xs-6 pull-left">
      {!! Form::radio('notie','yes',true,array( 'onclick' => 'yesNotice(this)')) !!}Yes
      {!! Form::radio('notie','no',true,array( 'onclick' => 'noNotice(this)')) !!}No
    </div>
  </div>

  <br>
  <div class="row" style="display:none" id="fileDiv">
    <div class="col-xs-5"></div>
    <div class="col-xs-6">
      {!! Form::input('file','file', null,array('class'=>'form-control')) !!}
    </div>
  </div>

  <br>
  <div class="row">

  </div>

  <br>
  <div class="row">
    {!! Form::hidden('patient_treatment_id', $patientTreatmentId) !!}
    <button type="submit" class="btn btn-warning" style="margin-left:81%">&#x2714; Save</button>
  <a href="{{ url('treatment/getpatientdetails',[$patientId]) }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
  </div><br>

  {!! Form::close() !!}

  <script type="text/javascript">

  function yesShowStopper(cb) {

    if(cb.checked == true)
    {
     document.getElementById('noticeDiv').style.display="block";
   }

 }  

 function noShowStopper(cb) {

  if(cb.checked == true)
  {
   document.getElementById('noticeDiv').style.display="none"; 
   document.getElementById('fileDiv').style.display="none"; 
 }

}

function yesNotice(cb) {

  if(cb.checked == true)
  {
   document.getElementById('fileDiv').style.display="block";
 }

}  

function noNotice(cb) {

  if(cb.checked == true)
  {
   document.getElementById('fileDiv').style.display="none"; 
 }

}

</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>


@stop