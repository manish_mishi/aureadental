@extends('layouts.menuNav')

@section('title')
Treatment -> Finding Notes
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}" class="active">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:13%" role="presentation" ><a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}">General</a></li>
      <li style="width:20%" role="presentation"><a href="{{ url('treatment/treatmentnotes/showstopper',[$patientTreatmentId]) }}">Showstopper</a></li> 
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/consultant',[$patientTreatmentId]) }}">Consultant</a></li>
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/quatations',[$patientTreatmentId]) }}">Quotations</a></li>
      <li style="width:16%" role="presentation" ><a href="{{ url('treatment/treatmentnotes/visitingclinic',[$patientTreatmentId]) }}">Visiting Clinic</a></li>
      <li style="width:17%" role="presentation" class="active"><a href="{{ url('treatment/treatmentnotes/findingnotes',[$patientTreatmentId]) }}">Finding Notes</a></li> 
    </ul>
  </div>

  {!! Form::open(array('route'=>'updateFindingNotesTreamentNotes','class'=>'form')) !!}

 <div class="row">
  <label class="col-xs-3">Finding :</label>
  <div class="col-xs-3">
    <textarea name="findings" class="form-control">{{$treatmentFindingDetails[0]->findings}}</textarea>
  </div>
  <label class="col-xs-3">Notes:</label>
  <div class="col-xs-3">
   <textarea name="notes" class="form-control">{{$treatmentFindingDetails[0]->notes}}</textarea>
 </div>
</div><br>    

{!! Form::hidden('treatment_finding_id', $treatmentFindingDetails[0]->id) !!}

<br><br>
<div class="row">
  {!! Form::hidden('patient_treatment_id', $patientTreatmentId) !!}
  <button type="submit" class="btn btn-warning" style="margin-left:81%">&#x2714; Save</button>
  <a href="{{ url('treatment/getpatientdetails',[$patientId]) }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
</div><br>
{!! Form::close() !!}

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>


@stop