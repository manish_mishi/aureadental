@extends('layouts.menuNav')

@section('title')
Treatment -> Consultant
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
   <ul class="nav" id="side-menu">

    <li>
      <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}" class="active">Treatment Notes</a>
    </li>

    <li>
      <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}">Sitting</a>
    </li>

    <li>
      <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
    </li>
  </ul>
</div>
<!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:13%" role="presentation"><a href="{{ url('treatment/treatmentnotes/general,[$patientTreatmentId]') }}">General</a></li>
      <li style="width:20%" role="presentation"><a href="{{ url('treatment/treatmentnotes/showstopper',[$patientTreatmentId]) }}">Showstopper</a></li> 
      <li style="width:16%" role="presentation" class="active"><a href="{{ url('treatment/treatmentnotes/consultant',[$patientTreatmentId]) }}">Consultant</a></li>
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/quatations',[$patientTreatmentId]) }}">Quotations</a></li>
      <li style="width:16%" role="presentation"><a href="{{ url('treatment/treatmentnotes/visitingclinic',[$patientTreatmentId]) }}">Visiting Clinic</a></li>
      <li style="width:17%" role="presentation"><a href="{{ url('treatment/treatmentnotes/findingnotes',[$patientTreatmentId]) }}">Finding Notes</a></li> 
    </ul>
  </div>

  <div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Consultant Required:', null, array('class'=>'control-label')) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      <input type="checkbox" id="consul" onclick="consultantRequire(this)"/>
      <snap id="consulText" class="alert-info">&nbsp;Check If it is not requied</snap>
    </div>
    <!-- <input type="checkbox"  id="consul" checked data-off-active-cls="btn-warning" data-on-active-cls="btn-primary" onclick="consultantRequire(this)"> -->
  </div>

  <br>

  <div id="display" style="display:none">

    {!! Form::open(array('route'=>'storeConsultantTreamentNotes','class'=>'form')) !!}

    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          {!! Form::label('Consultant Name', null, array('class'=>'control-label')) !!}
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          {!! Form::text('consultant_name', null,array('class'=>'form-control')) !!}
        </div>
      </div>

      <br>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          {!! Form::label('Clinic', null, array('class'=>'control-label')) !!}
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          {!! Form::text('clinic', null,array('class'=>'form-control')) !!}
        </div>
      </div>

      <br>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          {!! Form::label('Location', null, array('class'=>'control-label')) !!}
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          {!! Form::text('location', null,array('class'=>'form-control')) !!}
        </div>
      </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
         {!! Form::label('Fees', null, array('class'=>'control-label')) !!}
       </div>
       <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
        {!! Form::textarea('fees', null, array('rows'=>2, 'cols'=>10,'class'=>'form-control')) !!}
      </div>
    </div>

    <br>
    <div class="row">
      <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        {!! Form::label('Notes', null, array('class'=>'control-label')) !!}
      </div>
      <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
        {!! Form::textarea('notes', null, array('rows'=>2, 'cols'=>10, 'class'=>'form-control')) !!}
      </div>
    </div>
  </div>

</div><!-- Close of div id="display" -->

<br><br><br><br><br><br><br><br>
<div class="row">
  {!! Form::hidden('patient_treatment_id', $patientTreatmentId) !!}
  <button type="submit" class="btn btn-warning" style="margin-left:81%">&#x2714; Save</button>
  <a href="{{ url('treatment/getpatientdetails',[$patientId]) }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
</div>

{!! Form::close() !!}

<script type="text/javascript">

/*$(':checkbox').checkboxpicker();*/

function consultantRequire(cb) {

  if(cb.checked == true)
  {
    document.getElementById('display').style.display="block";
    document.getElementById('consulText').innerHTML = "Uncheck If it is not requied";
  }
  else
  {
   document.getElementById('display').style.display="none"; 
   document.getElementById('consulText').innerHTML = "Check If it is requied";
 }

}
</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>



@stop