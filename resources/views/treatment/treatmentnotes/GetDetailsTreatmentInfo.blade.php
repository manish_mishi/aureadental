@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      
      <li>
        <a href="{{ url('treatment/treatmentnotes/general',[$patientTreatmentId]) }}" class="active">Treatment Notes</a>
      </li>

      <li>
        <a href="{{ url('treatment/sittings',[$patientTreatmentId]) }}">Sitting</a>
      </li>

      <li>
        <a href="{{ url('treatment/labwork',[$patientTreatmentId]) }}" >Lab Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
	<div id="page-wrapper">
   <div class="row marketing">
    <br><br>
    <form>
      <div class="col-lg-12">
        <div class="col-xs-8">
          
          <div class="form-group">
            <label class="col-xs-3" align="right">Patient ID :</label>
            <div class="col-xs-5"><input type="text" class="form-control" name="patient_id" id="patient_id"></div>
          </div><br><br>
          <div class="form-group">
            <label class="col-xs-3" align="right">Name :</label>
            <div class="col-xs-5"><input type="text" class="form-control" name="name" id="name"></div>
          </div><br><br>
          <div class="form-group">
            <label class="col-xs-3" align="right">Cell # :</label>
            <div class="col-xs-5"><input type="text" class="form-control" name="cell" id="cell"></div>
          </div><br><br>
          
        </div>
        <div class="col-xs-4">
          <div class="form-group">
            <label class="col-xs-8">Medical History:</label><br>
          </div>

          <div class="form-group">
            <div class="col-xs-12"><textarea class="form-control"></textarea></div>
          </div>
        </div>
      </div>
    </form>

    

  </div>
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills in">
      <li style="width:12%" role="presentation"><a href="{{url('')}}" class="active">General</a></li>
      <li style="width:20%" role="presentation"><a href="{{url('')}}">ShowstopperChecklist</a></li>
      <li style="width:16%" role="presentation"><a href="{{url('')}}">Consultant</a></li>
      <li style="width:16%" role="presentation"><a href="{{url('')}}">Quotations</a></li>
      <li style="width:16%" role="presentation"><a href="{{url('')}}">Visiting Clinic</a></li>
      <li style="width:16%" role="presentation"><a href="{{url('')}}">Finding Notes</a></li>
    </ul>
  </div>
</div>
</div>

@stop