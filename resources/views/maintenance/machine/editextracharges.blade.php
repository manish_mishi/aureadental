@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')


<div id="wrapper">
  <div id="page-wrapper">
    <br>
    <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:33%" role="presentation"><a href="{{ url('maintenance/machine/nextduemaintenance/assignmaintenance',[$machine_id]) }}">Assign Work</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('maintenance/machine/nextduemaintenance/assignmaintenance/complteMaintenance',[$machine_id]) }}">Completed Maintenance</a></li>
      <li style="width:33%" role="presentation" class="active"><a href="{{ url('maintenance/machine/nextduemaintenance/assignmaintenance/extracharges',[$machine_id]) }}">Extra Charges</a></li>

    </ul>
  </div>

  {!! Form::open(array('route' => 'updateextrachargesmachine','class'=>'form')) !!}

  <div class="row">
      {!! Form::label('Amount:', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        @if(Session::has('amount'))
        <?php $amount=Session::get('amount');?>
          {!! Form::text('amount',$amount,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
        @else
          {!! Form::text('amount',$current_machineDet['0']->amount,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
        @endif
      </div>
      <label class="col-xs-2" align="right">Remarks:</label>
      <div class="col-xs-4">
        @if(Session::has('remark_extracharge'))
        <?php $remark_extracharge=Session::get('remark_extracharge');?>
        <textarea  align="left" rows="4" cols="50" class="form-control" name="remark_extracharge">{{$remark_extracharge}}</textarea>
        @else
        <textarea  align="left" rows="4" cols="50" class="form-control" name="remark_extracharge">{{$current_machineDet['0']->extra_charge_remarks}}</textarea>
        @endif
      </div>
    </div><br>

    <div class="row">
      {!! Form::label('Advance:', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        @if(Session::has('advance'))
        <?php $advance=Session::get('advance');?>
        {!! Form::text('advance',$advance,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
        @else
        {!! Form::text('advance',$current_machineDet['0']->advance,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
        @endif
      </div>
    </div><br>

    <div class="row">
      {!! Form::label('Discount:', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        @if(Session::has('discount'))
        <?php $discount=Session::get('discount');?>
        {!! Form::text('discount',$discount,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
        @else
        {!! Form::text('discount',$current_machineDet['0']->discount,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
        @endif
      </div>

    </div><br>

    <div class="row">
      {!! Form::label('Total Amount:', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        @if(Session::has('total_amount'))
        <?php $total_amount=Session::get('total_amount');?>
        {!! Form::text('total_amount', $total_amount,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
        @else
        {!! Form::text('total_amount',$current_machineDet['0']->total_amount,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
        @endif
      </div>

    </div><br>
    <div class="row">
      {!! Form::label('Bill:', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        @if(Session::has('bill'))
        <?php $bill=Session::get('bill');?>
        {!! Form::text('bill',$bill,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
        @else
        {!! Form::text('bill',$current_machineDet['0']->bill_no,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
        @endif
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <button type="submit" class="btn btn-warning pull-right">Save</button>
        {!! Form::hidden('gadget_machine_id',$machine_id,array('class'=>'form-control')) !!}
      </div>
    </div><br>
    {!! Form::close() !!}


</div>
</div>


@stop