@extends('layouts.menuNav')

@section('title')
Machine Assign Work
@stop

@section('content')


<div id="wrapper">
  <div id="page-wrapper">
   <br>
    <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">
      
      <li style="width:33%" role="presentation" class="active"><a href="{{ url('maintenance/machine/nextduemaintenance/assignmaintenance',[$machine_id]) }}">Assign Work</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('maintenance/machine/nextduemaintenance/assignmaintenance/complteMaintenance',[$machine_id]) }}">Completed Maintenance</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('maintenance/machine/nextduemaintenance/assignmaintenance/extracharges',[$machine_id]) }}">Extra Charges</a></li>
    </ul>
  </div>

  {!! Form::open(array('route' => 'addmaintenanceassignworkmachine','class'=>'form')) !!}
    
   <div class="row">
    {!! Form::label('Gadget/Machine Name', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">

      {!! Form::text('gadget_machine_name',$machinedetails['0']->machine_name,array('class'=>'form-control','readonly')) !!}
      
    </div>
    {!! Form::label('Last Maintenance Date', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('last_maintenance_date'))
        <?php $last_maintenance_date = Session::get('last_maintenance_date'); ?>
        <input data-provide="datepicker" value="{{$last_maintenance_date}}" data-date-format="dd/mm/yyyy" id="lastMaintenanceDate" class="form-control" name="last_maintenance_date">
       @else
        <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="lastMaintenanceDate" class="form-control" name="last_maintenance_date">
      @endif
    </div>
  </div><br>

  <div class="row">
    {!! Form::label('Vendor', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('vendor_name'))
        <?php 
            $vendor_name = Session::get('vendor_name'); 
            $vendor_id = Session::get('vendor_id');
            $vendor_type_id=Session::get('vendor_type_id');
          ?>
          <select class="form-control" name="vendor">
            <option value="{{$vendor_id}},{{$vendor_name}},{{$vendor_type_id}}">{{$vendor_name}}</option>
            @foreach($vendordetails as $vendor)
              <option value="{{$vendor->id}},{{$vendor->name}},{{$vendor->vendor_type_id}}">{{$vendor->name}}</option>
            @endforeach
          </select>
        @else
        <select class="form-control" name="vendor">
        <option>select</option>
        @foreach($vendordetails as $vendor)
          <option value="{{$vendor->id}},{{$vendor->name}},{{$vendor->vendor_type_id}}">{{$vendor->name}}</option>
        @endforeach
        </select>

        
      @endif
    </div>
    {!! Form::label('Due Date', null, array('class'=>'col-xs-3 control-label')) !!}
    
    <div class="col-xs-3">
      @if(Session::has('duedate'))
        <?php $duedate = Session::get('duedate'); ?>
        <input data-provide="datepicker" value="{{$duedate}}"data-date-format="dd/mm/yyyy" id="dueDate" class="form-control" name="duedate">
      @else
        <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dueDate" class="form-control" name="duedate">
      @endif
    </div>
      
  </div><br>

  <div class="row">
    {!! Form::label('Status', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('status_name'))
      <?php 
          $status_id = Session::get('status_id');
          $status_name = Session::get('status_name'); 
        ?>
        <select class="form-control" name="status">
        <option value="{{$status_id}},{{$status_name}}">{{$status_name}}</option>
        @foreach($maintenance_status as $status)
          <option value="{{$status->id}},{{$status->name}}">{{$status->name}}</option>
        @endforeach
      </select>
      @else
      <select class="form-control" name="status">
        <option>select</option>
        @foreach($maintenance_status as $status)
          <option value="{{$status->id}},{{$status->name}}">{{$status->name}}</option>
        @endforeach
      </select>
      @endif
      <!-- {!! Form::text('status', null,array('class'=>'form-control')) !!} -->
    </div>
    
    {!! Form::label('Set Maintenance Date', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('set_maintenance_date'))
        <?php $set_maintenance_date = Session::get('set_maintenance_date'); ?>
        <input data-provide="datepicker" value="{{$set_maintenance_date}}" data-date-format="dd/mm/yyyy" id="setMaintenanceDate" class="form-control" name="set_maintenance_date">
      @else
        <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="setMaintenanceDate" class="form-control" name="set_maintenance_date">
      @endif
    </div>
  </div><br>

  <div class="row">
    {!! Form::label(null, null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
    </div>
    {!! Form::label('Time', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('time'))
        <?php $time = Session::get('time'); ?>
        {!! Form::text('time',$time,array('class'=>'form-control')) !!}
      @else
      {!! Form::text('time', null,array('class'=>'form-control')) !!}
      @endif
    </div>
  </div><br>
  <div class="row">
    <div class="col-xs-11">
    </div>
    <div class="col-xs-1">
      {!! Form::hidden('gadget_machine_id',$machine_id,array('class'=>'form-control')) !!}
      {!! Form::button('&#x2714; Next',array('type' => 'submit','class'=>'btn btn-default')) !!} 
    </div>
  </div><br>
{!! Form::close() !!}


</div>
</div>


@stop