@extends('layouts.menuNav')

@section('title')
Machine->Maintenance->Completed Maintenance
@stop

@section('content')


<div id="wrapper">
  <div id="page-wrapper">
   <br>
   <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">
      
      <li style="width:33%" role="presentation"><a href="{{ url('maintenance/machine/maintenance/completedmaintenance/getdetailsassignwork',[$machine_id]) }}">Assign Work</a></li>
      <li style="width:33%" role="presentation" class="active"><a href="{{ url('maintenance/machine/maintenance/completedmaintenance/getdetailscompletedmaintenance',[$machine_id]) }}">Completed Maintenance</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('maintenance/machine/maintenance/completedmaintenance/getdetailsextracharges',[$machine_id]) }}">Extra Charges</a></li>
    </ul>
  </div>

  
  @if($current_machineDet != null)
  <div class="row">
    {!! Form::label('Date of Maintenance:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('actual_date_of_maintenance'))
      <?php $actual_date_of_maintenance=session::get('actual_date_of_maintenance');?>
      <input data-provide="datepicker" value="{{$actual_date_of_maintenance}}" data-date-format="dd/mm/yyyy" id="dueDate" class="form-control" name="date_of_maintenance">
      @else
      <input data-provide="datepicker" value="{{$current_machineDet['0']->actual_date_of_maintenance}}" data-date-format="dd/mm/yyyy" id="dueDate" class="form-control" name="date_of_maintenance" Readonly>
      @endif
    </div>
    <label class="col-xs-2" align="right">Remarks:</label>
    <div class="col-xs-4">
      @if(Session::has('remark'))
      <?php $remark=session::get('remark');?>
      <textarea  align="left" rows="4" cols="50" class="form-control" name="remark">{{$remark}}</textarea>
      @else
      <textarea  align="left" rows="4" cols="50" class="form-control" name="remark" Readonly>{{$current_machineDet['0']->remarks}}</textarea>
      @endif
    </div>
  </div><br>

  <div class="row">
    {!! Form::label('Time:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('actual_time'))
      <?php $actual_time=session::get('actual_time');?>
      {!! Form::text('time_complete',$actual_time,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
      @else
      {!! Form::text('time_complete',$current_machineDet['0']->actual_time,array('id'=>'day','class'=>'form-control','placeholder'=>'','readonly')) !!}
      @endif
    </div>
  </div><br>

  <div class="row">
    {!! Form::label('Duration:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('actual_duration'))
      <?php $actual_duration=session::get('actual_duration');?>
      {!! Form::text('duration',$actual_duration,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
      @else
      {!! Form::text('duration',$current_machineDet['0']->actual_duration,array('id'=>'day','class'=>'form-control','placeholder'=>'','readonly')) !!}
      @endif
    </div>

  </div><br>

  <div class="row">
    {!! Form::label('Person Responsible:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('person_responsible'))
      <?php $person_responsible=session::get('person_responsible');?>
      {!! Form::text('person_responsible',$person_responsible,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
      @else
      {!! Form::text('person_responsible',$current_machineDet['0']->person_responsible,array('id'=>'day','class'=>'form-control','placeholder'=>'','readonly')) !!}
      @endif
    </div>

  </div><br>
  <div class="row">
    {!! Form::label('Cell:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('cell_no'))
      <?php $cell_no=session::get('cell_no');?>
      {!! Form::text('cell',$cell_no,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
      @else
      {!! Form::text('cell',$current_machineDet['0']->cell_no,array('id'=>'day','class'=>'form-control','placeholder'=>'','readonly')) !!}
      @endif
    </div>
  </div>
  @else
  <div class="row">
    {!! Form::label('Date of Maintenance:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('actual_date_of_maintenance'))
      <?php $actual_date_of_maintenance=session::get('actual_date_of_maintenance');?>
      <input data-provide="datepicker" value="{{$actual_date_of_maintenance}}" data-date-format="dd/mm/yyyy" id="dueDate" class="form-control" name="date_of_maintenance">
      @else
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dueDate" class="form-control" name="date_of_maintenance">
      @endif
    </div>
    <label class="col-xs-2" align="right">Remarks:</label>
    <div class="col-xs-4">
      @if(Session::has('remark'))
      <?php $remark=session::get('remark');?>
      <textarea  align="left" rows="4" cols="50" class="form-control" name="remark">{{$remark}}</textarea>
      @else
      <textarea  align="left" rows="4" cols="50" class="form-control" name="remark"></textarea>
      @endif
    </div>
  </div><br>

  <div class="row">
    {!! Form::label('Time:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('actual_time'))
      <?php $actual_time=session::get('actual_time');?>
      {!! Form::text('time_complete',$actual_time,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
      @else
      {!! Form::text('time_complete', null,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
      @endif
    </div>
  </div><br>

  <div class="row">
    {!! Form::label('Duration:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('actual_duration'))
      <?php $actual_duration=session::get('actual_duration');?>
      {!! Form::text('duration',$actual_duration,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
      @else
      {!! Form::text('duration', null,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
      @endif
    </div>

  </div><br>

  <div class="row">
    {!! Form::label('Person Responsible:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('person_responsible'))
      <?php $person_responsible=session::get('person_responsible');?>
      {!! Form::text('person_responsible',$person_responsible,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
      @else
      {!! Form::text('person_responsible', null,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
      @endif
    </div>

  </div><br>
  <div class="row">
    {!! Form::label('Cell:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      @if(Session::has('cell_no'))
      <?php $cell_no=session::get('cell_no');?>
      {!! Form::text('cell',$cell_no,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
      @else
      {!! Form::text('cell', null,array('id'=>'day','class'=>'form-control','placeholder'=>'')) !!}
      @endif
    </div>
  </div>
  @endif
  <div class="row"><br>
    <div class="col-xs-11">
    </div>
    <div class="col-xs-1">
      <a href="{{url('maintenance/machine/completedmaintenance')}}">{!! Form::button('&#10006; Cancel',array('type' => 'submit','class'=>'btn btn-danger')) !!}</a> 
      {!! Form::hidden('gadget_machine_id',$machine_id,array('class'=>'form-control')) !!}
    </div>
  </div>
  <br>
  

</div>
</div>
</div>


@stop