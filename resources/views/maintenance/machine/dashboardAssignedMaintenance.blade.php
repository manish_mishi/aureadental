@extends('layouts.menuNav')

@section('title')
Machine->Maintenance->Assigned Maintenance
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('maintenance/gadget/nextduemaintenance') }}">Gadget</a>
      </li>

      <li>
        <a href="{{ url('maintenance/machine/nextduemaintenance') }}" class="active">Machine</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">
    <br>
      <div class="panel panel-info">
        <ul style="width:100%" class="nav nav-pills">
          <li style="width:53%" role="presentation" ><a href="{{ url('maintenance/machine/nextduemaintenance') }}">Next Due Maintenance</a></li>
          <li style="width:43%" role="presentation" class="active" ><a href="{{ url('maintenance/machine/completedmaintenance') }}">Maintenance</a></li>

        </ul>
      </div>
      <ul style="width:80%" class="nav nav-pills">
        <li style="width:33%" role="presentation" ><a href="{{ url('maintenance/machine/completedmaintenance') }}">Completed Maintenance</a></li>
        <li style="width:33%" role="presentation"  class="active"><a href="{{ url('maintenance/machine/assignedmaintenance') }}">Assigned Maintenance</a></li>

      </ul>
      
      <div class="panel panel-default filterable">
       <table class="table">
        <thead>

          <tr class="filters">
            <th><input type="text" class="form-control" placeholder="" disabled>Machine Name</th>
            <th><input type="text" class="form-control" placeholder="" disabled>Vendor</th>
            <th><input type="text" class="form-control" placeholder="" disabled>Assigned Date</th>
            <th><input type="text" class="form-control" placeholder="" disabled>Due Date of Maintenance</th>
            <th><input type="text" class="form-control" placeholder="" disabled>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          
          @foreach($machinedetails as $machine)
          <tr>
            <td>{{$machine->machine_name}}</td>
            <td>{{$machine->vendor_name}}</td>
            <td>{{$machine->actual_date_of_maintenance}}</td>
            <td>{{$machine->due_date}}</td>
            <td>Open </td>
            <td>

              <a href="#"><button class="btn btn-xs btn-default" OnClick="changestatus()">Change Status</button></a>
              <input type="hidden" value="{{$machine->machine_id}}" name="machine_id" id="machineId">
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

    </div>

  
</div>
</div>

<script>
    $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });


function changestatus()
{
  $machineId = $('#machineId').val();

  
  swal({   
    title: "Change Gadget Status",   
    text: "Do you want to change status?",   
    type: "info",   
    showCancelButton: true,   
    closeOnConfirm: false,   
    showLoaderOnConfirm: true, 
  }, function(){   



    $.ajax({
        method:'POST',
        url: '{{url("maintenance/machine/assignedmaintenance/changestatus/")}}'+"/"+$machineId,
         success : function(data){
          console.log('arrived');
          window.location.replace('completedmaintenance');
         }
      });

});
}

</script>

@stop