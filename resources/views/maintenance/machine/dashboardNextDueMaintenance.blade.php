@extends('layouts.menuNav')

@section('title')
Machine->Next Due Maintenance
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('maintenance/gadget/nextduemaintenance') }}">Gadget</a>
      </li>

      <li>
        <a href="{{ url('maintenance/machine/nextduemaintenance') }}" class="active">Machine</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>



<div id="wrapper">
  <div id="page-wrapper">
    <br>
    <div class="panel panel-info">
      <ul style="width:100%" class="nav nav-pills">
        <li style="width:53%" role="presentation" class="active"><a href="{{ url('maintenance/machine/nextduemaintenance') }}">Next Due Maintenance</a></li>
        <li style="width:43%" role="presentation"  ><a href="{{ url('maintenance/machine/completedmaintenance') }}">Maintenance</a></li>
      </ul>
    </div>

    

    <div class="col-xs-12">

      <div class="col-xs-2" align="right"><label>Sort By</label></div>
      <div class="col-xs-2" align="left"><select><option>ALL</option></select></div>
    </div>

    <br>
    <div class="panel panel-default filterable">
     <table class="table">
      <thead>

        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="" disabled>Machine Name</th>
          <th><input type="text" class="form-control" placeholder="" disabled>Last Date of Maintenance</th>
          <th><input type="text" class="form-control" placeholder="" disabled>Due Date of Maintenance</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach($machinedetails as $machine)
        
        <tr>
          <td>{{$machine->machine_name}}</td>
          <td>{{$machine->last_maintenance_date}}</td>
          <td>{{$machine->due_date}}</td> 
          <td><a href="{{url('maintenance/machine/nextduemaintenance/assignmaintenance',[$machine->machine_id])}}">Assign Maintenance </a></td>
        </tr>
        @endforeach
      </tbody>
    </table>

  </div>


</div>
</div>

@stop