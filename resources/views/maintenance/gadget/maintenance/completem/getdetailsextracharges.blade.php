@extends('layouts.menuNav')

@section('title')
View Extra Charges
@stop

@section('content')


<div id="wrapper">
  <div id="page-wrapper">
    <br>
    <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">
      <li style="width:33%" role="presentation"><a href="{{ url('maintenance/gadget/maintenance/completemaintenance/getdetails',[$id]) }}">Assign Work</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('maintenance/gadget/maintenance/completemaintenance/getdetails/completedmaintenance',[$id]) }}">Completed Maintenance</a></li>
      <li style="width:33%" role="presentation" class="active"><a href="{{ url('maintenance/gadget/maintenance/completemaintenance/getdetails/extracharges',[$id]) }}">Extra Charges</a></li>

    </ul>
  </div>

  
    <div class="row">
      {!! Form::label('Amount:', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('amount',$gadgetdetails['0']->amount,array('id'=>'day','class'=>'form-control','placeholder'=>'','readonly')) !!}
      </div>

      <label class="col-xs-2" align="right">Remarks:</label>
      <div class="col-xs-4">
        <textarea  align="left" rows="4" cols="50" class="form-control" name="remark_extracharge" Readonly>{{$gadgetdetails['0']->extra_charge_remarks}}</textarea>
      </div>
    </div><br>

    <div class="row">
      {!! Form::label('Advance:', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('advance', $gadgetdetails['0']->advance,array('id'=>'day','class'=>'form-control','placeholder'=>'','readonly')) !!}
      </div>
    </div><br>

    <div class="row">
      {!! Form::label('Discount:', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('discount', $gadgetdetails['0']->discount,array('id'=>'day','class'=>'form-control','placeholder'=>'','readonly')) !!}
      </div>

    </div><br>

    <div class="row">
      {!! Form::label('Total Amount:', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('total_amount', $gadgetdetails['0']->total_amount,array('id'=>'day','class'=>'form-control','placeholder'=>'','readonly')) !!}
      </div>

    </div><br>
    <div class="row">
      {!! Form::label('Bill:', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('bill', $gadgetdetails['0']->bill_no,array('id'=>'day','class'=>'form-control','placeholder'=>'','readonly')) !!}
      </div>
    </div>

    <div class="row"><br>
    <div class="col-xs-11">
    </div>
    <div class="col-xs-1">
      <a href="{{ url('maintenance/gadget/maintenance') }}">{!! Form::button('&#x2714; Cancel',array('type' => 'submit','class'=>'btn btn-danger')) !!} </a>
      {!! Form::hidden('gadget_machine_id',$id,array('class'=>'form-control')) !!}
    </div>
    </div>
    


</div>
</div>


@stop