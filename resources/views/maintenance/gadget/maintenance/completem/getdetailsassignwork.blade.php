@extends('layouts.menuNav')

@section('title')
View ssign Work
@stop

@section('content')


<div id="wrapper">
  <div id="page-wrapper">
   <br>
   <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">

      <li style="width:33%" role="presentation" class="active"><a href="{{ url('maintenance/gadget/maintenance/completemaintenance/getdetails',[$id]) }}">Assign Work</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('maintenance/gadget/maintenance/completemaintenance/getdetails/completedmaintenance',[$id]) }}">Completed Maintenance</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('maintenance/gadget/maintenance/completemaintenance/getdetails/extracharges',[$id]) }}">Extra Charges</a></li>
    </ul>
  </div>

  
  <div class="row">
    {!! Form::label('Gadget/Machine Name', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('gadget_machine_name',$gadgetdetails['0']->gadget_name,array('class'=>'form-control','readonly')) !!}
      
    </div>
    {!! Form::label('Last Maintenance Date', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <input data-provide="datepicker" value="{{$gadgetdetails['0']->last_maintenance_date}}" data-date-format="dd/mm/yyyy" id="lastMaintenanceDate" class="form-control" name="last_maintenance_date" Readonly>
    </div>
  </div><br>

  <div class="row">
    {!! Form::label('Vendor', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('gadget_machine_name',$gadgetdetails['0']->vendorname,array('class'=>'form-control','readonly')) !!}
    </div>
    {!! Form::label('Due Date', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <input data-provide="datepicker" value="{{$gadgetdetails['0']->due_date}}" data-date-format="dd/mm/yyyy" id="dueDate" class="form-control" name="duedate" Readonly>
    </div>
  </div><br>

  <div class="row">
    {!! Form::label('Status', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('status', $gadgetdetails['0']->statusname,array('class'=>'form-control','readonly')) !!}
    </div>
    
    {!! Form::label('Set Maintenance Date', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <input data-provide="datepicker" value="{{$gadgetdetails['0']->set_maintenance_date}}" data-date-format="dd/mm/yyyy" id="setMaintenanceDate" class="form-control" name="set_maintenance_date" Readonly>
      
    </div>
  </div><br>

  <div class="row">
    {!! Form::label(null, null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
    </div>
    {!! Form::label('Time', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('time', $gadgetdetails['0']->time,array('class'=>'form-control','readonly')) !!}
    </div>
  </div><br>
  

  <div class="row">
    <div class="col-xs-11">
    </div>
    <div class="col-xs-1">
      {!! Form::hidden('gadget_machine_id',null,array('class'=>'form-control')) !!}
      <a href="{{ url('maintenance/gadget/maintenance') }}">{!! Form::button('&#x2714; Cancel',array('type' => 'submit','class'=>'btn btn-danger')) !!} </a>
    </div>
  </div><br>
  


</div>
</div>


@stop