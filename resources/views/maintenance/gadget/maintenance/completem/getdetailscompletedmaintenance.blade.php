@extends('layouts.menuNav')

@section('title')
Gadget->Maintenance-> View Completed Maintenance
@stop

@section('content')


<div id="wrapper">
  <div id="page-wrapper">
   <br>
    <div class="panel panel-info">
     <ul style="width:100%" class="nav nav-pills">

      <li style="width:33%" role="presentation"><a href="{{ url('maintenance/gadget/maintenance/completemaintenance/getdetails',[$id]) }}">Assign Work</a></li>
      <li style="width:33%" role="presentation" class="active"><a href="{{ url('maintenance/gadget/maintenance/completemaintenance/getdetails/completedmaintenance',[$id]) }}">Completed Maintenance</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('maintenance/gadget/maintenance/completemaintenance/getdetails/extracharges',[$id]) }}">Extra Charges</a></li>
    </ul>
  </div>

  
   <div class="row">
    {!! Form::label('Date of Maintenance:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <input data-provide="datepicker" value="{{$gadgetdetails['0']->actual_date_of_maintenance}}" data-date-format="dd/mm/yyyy" id="dueDate" class="form-control" name="date_of_maintenance" Readonly>
      
    </div>
    <label class="col-xs-2" align="right">Remarks:</label>
    <div class="col-xs-4">
      <textarea  align="left" rows="4" cols="50" class="form-control" name="remark" Readonly>{{$gadgetdetails['0']->remarks}}</textarea>
    </div>
  </div><br>

  <div class="row">
    {!! Form::label('Time:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('time_complete', $gadgetdetails['0']->actual_time,array('id'=>'day','class'=>'form-control','placeholder'=>'','readonly')) !!}
    </div>
  </div><br>

  <div class="row">
    {!! Form::label('Duration:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('duration', $gadgetdetails['0']->actual_duration,array('id'=>'day','class'=>'form-control','placeholder'=>'','readonly')) !!}
    </div>

  </div><br>

  <div class="row">
    {!! Form::label('Person Responsible:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('person_responsible', $gadgetdetails['0']->person_responsible,array('id'=>'day','class'=>'form-control','placeholder'=>'','readonly')) !!}
    </div>

  </div><br>
  <div class="row">
    {!! Form::label('Cell:', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('cell', $gadgetdetails['0']->cell_no,array('id'=>'day','class'=>'form-control','readonly')) !!}
    </div>
  </div>

  <div class="row"><br>
    <div class="col-xs-11">
    </div>
    <div class="col-xs-1">
      <a href="{{ url('maintenance/gadget/maintenance') }}">{!! Form::button('&#x2714; Cancel',array('type' => 'submit','class'=>'btn btn-danger')) !!} </a>
      {!! Form::hidden('gadget_machine_id',$id,array('class'=>'form-control')) !!}
    </div>
    </div>
  <br>
  

</div>
</div>
</div>


@stop