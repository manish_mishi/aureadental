@extends('layouts.menuNav')

@section('title')
Gadget->Next Due Maintenance
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('maintenance/gadget/nextduemaintenance') }}" class="active">Gadget</a>
      </li>

      <li>
        <a href="{{ url('maintenance/machine/nextduemaintenance') }}">Machine</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>



<div id="wrapper">
  <div id="page-wrapper">
    <br>
      <div class="panel panel-info">
        <ul style="width:100%" class="nav nav-pills">
          <li style="width:53%" role="presentation" class="active"><a href="{{ url('maintenance/gadget/nextduemaintenance') }}">Next Due Maintenance</a></li>
          <li style="width:43%" role="presentation"  ><a href="{{ url('maintenance/gadget/maintenance') }}">Maintenance</a></li>
        </ul>
      </div>
      <div class="col-xs-12">
        <div class="col-xs-2" align="right"><label>Sort By</label></div>
        <div class="col-xs-2" align="left"><select><option>ALL</option></select></div>
      </div>
      <br>
      <div class="panel panel-default filterable">
       <table class="table">
        <thead>
          <tr class="filters">
            <th><input type="text" class="form-control" placeholder="" disabled>Gadget Name</th>
            <th><input type="text" class="form-control" placeholder="" disabled>Last Date of Maintenance</th>
            <th><input type="text" class="form-control" placeholder="" disabled>Due Date of Maintenance</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($gadgetdetails as $gadget)
          <tr>
            <td>{{$gadget->gadget_name}}</td>
            <td>{{$gadget->last_maintenance_date}}</td>
            <td>{{$gadget->due_date}}</td>
            <td><a href="{{url('gadget/maintenance/assignmaintenance/assignwork',[$gadget->id])}}">Assign Maintenance </a></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
</div>
</div>

@stop