@extends('layouts.menuNav')

@section('title')
List View
@stop

@section('content')

<br>
<br>
<div class="container">
  {!! Form::open(array('route' => 'patientdetails','class' => 'form')) !!}
  <div class="row">
    {!! Form::label('Name', null, array('class'=>'col-xs-2 col-sm-2 col-md-1 control-label')) !!}
    <div class="col-xs-5 col-sm-5 col-md-4">
      {!! Form::text('name', null,array('class'=>'form-control','id'=>'name','required')) !!}
    </div>
  </div>

  <br>
  <div class="row">
    {!! Form::label('Cell No', null, array('class'=>'col-xs-2 col-sm-2 col-md-1 control-label')) !!}
    <div class="col-xs-5 col-sm-5 col-md-4">
      {!! Form::text('cell_no', null,array('class'=>'form-control','id'=>'cell_no','required')) !!}
    </div>
    <div class="col-xs-2">
      <button type="submit" class="btn btn-default">Search</button>
    </div>
  </div>
  {!! Form::close() !!}

<!--   <br>
<div class="row">
  <a href="{{ url('planner/newappointment') }}" class="btn btn-success btn-md col-md-2 pull-right"><i class="fa fa-plus fa-fw"></i>New Appointment</a>
</div> -->

<script type="text/javascript">
  $('#name').focus();
</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div> <!-- /#container -->

@stop