    @extends('layouts.menuNav')

    @section('title')
    Dashboard
    @stop

    @section('content')

    <div class="navbar-default sidebar" role="navigation">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">

            <li>
              <a href="{{ url('planner/viewplanner') }}">View Planner</a>
          </li>

          <li>
              <a href="{{ url('planner/scheduling/appointmentdetails') }}" class="active" >Scheduling</a>
          </li>

          <li>
              <a href="{{ url('planner/followup/appointments') }}">Follow Up</a>
          </li>

      </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>
<div id="wrapper">
  <div id="page-wrapper">
     <div class="row marketing">

        
           {!! Form::open(array('route' => 'addnewappointment','class' => 'form')) !!}

           
             {!! Form::label('Name', null, array('class'=>'col-xs-2 control-label')) !!}

             <div class="col-xs-3">
              {!! Form::text('patient_name', null,array('class'=>'form-control','id'=>'name')) !!}
          </div>

          <div class="row">
            {!! Form::label('Cell', null, array('class'=>'col-xs-2 control-label')) !!}

            <div class="col-xs-3">
              {!! Form::input('number','cell_no', null,array('class'=>'form-control','id'=>'cell_no')) !!}
          </div></div>
          <br><br>



      
         {!! Form::label('Assign Date', null, array('class'=>'col-xs-2 control-label')) !!}

         <div class="col-xs-3">
              {!! Form::input('date','assign_date', null, array('id'=>'day','class'=>'form-control','placeholder'=>'DOB')) !!}
      </div>

      <div class="row">
      {!! Form::label('Dentist Name',null,array('class'=>'col-xs-2 control-label')) !!}

        <div class="col-xs-3">
        {!! Form::select('dentist_name',$items, null, ['class' => 'form-control'] )!!}
      </div></div><br><br>
    {!! Form::label('Assign Slot', null, array('class'=>'col-xs-2 control-label')) !!}

         <div class="col-xs-3">
          {!! Form::input('time','assign_slot', null,array('class'=>'form-control','id'=>'assign_slot')) !!}
      </div>

      <div class="row">
      {!! Form::label('Chair',null,array('class'=>'col-xs-2 control-label')) !!}

        <div class="col-xs-3">
        {!! Form::select('chair',$chair_items, null, ['class' => 'form-control'] )!!}
      </div></div><br><br>
    
         {!! Form::label('Assistant', null, array('class'=>'col-xs-2 control-label')) !!}

         <div class="col-xs-3">
          {!! Form::text('assistant', null,array('class'=>'form-control','id'=>'assistant')) !!}
      </div>

      <div class="row">
        {!! Form::label('Consultant', null, array('class'=>'col-xs-2 control-label')) !!}

        <div class="col-xs-3">
          {!! Form::text('consultant', null,array('class'=>'form-control','id'=>'consultant')) !!}
      </div></div><br><br>
     
        {!! Form::label('Status', null, array('class'=>'col-xs-2 control-label')) !!}

        <div class="col-xs-3">
          {!! Form::text('status', null,array('class'=>'form-control','id'=>'status')) !!}
      </div><br><br>

      
       <div>
            {!! HTML::link('planner/viewplanner', '&#10006; Cancel', array('id' => 'linkid','class'=>'btn btn-danger pull-right')) !!}
          {!! Form::button('&#x2714; Save',array('type' => 'submit','class'=>'btn btn-warning  pull-right')) !!}

          </div>
    
    {!! Form::close() !!}
</div>
</div>
</div>
</div>


@stop