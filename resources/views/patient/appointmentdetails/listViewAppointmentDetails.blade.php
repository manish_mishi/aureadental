@extends('layouts.menuNav')

@section('title')
Patient -> Appontment Details
@stop

@section('content')
<br><br>
<div class="container">

  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->

  {!! Form::open(array('route' => 'patientdetails','class' => 'form')) !!}

  <div class="row">  
    {!! Form::label('Name', null, array('class'=>'col-xs-2 col-sm-2 col-md-1 control-label')) !!}
    <div class="col-xs-5 col-sm-5 col-md-4">
      {!! Form::text('name', $patientName,array('class'=>'form-control','id'=>'name','required')) !!}
    </div>
  </div>
  
  <br>
  <div class="row">
    <div class="col-xs-2 col-sm-2 col-md-1">
      {!! Form::label('Cell No', null, array('class'=>'control-label')) !!}
    </div>
    <div class="col-xs-5 col-sm-5 col-md-4">
      {!! Form::text('cell_no', $patientCell,array('class'=>'form-control','id'=>'cell_no','required')) !!}
    </div>

    <div class="col-xs-2">
      <button type="submit" class="btn btn-default">Search</button>
    </div>
    
  </div>
  {!! Form::close() !!}

  <br>
  <div class="row">
    {!! Form::open(array('route' => 'newAppointment','class' => 'form')) !!}
    {!! Form::hidden('patient_id', $patientId) !!}
    {!! Form::hidden('patient_name', $patientName) !!}
    {!! Form::hidden('patient_cellno', $patientCell) !!}
    <div class="col-xs-1 col-sm-2 col-md-2 pull-right">
      <button type="submit" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>New Appointment</button>
    </div>
    {!! Form::close() !!}
  </div>

  <!-- making ul list Responsive -->
  <style type="text/css">
  ul.topnav li.list1 {
    width: 25%;
  }

  ul.topnav li.list2 {
    width: 25%;
  }

  ul.topnav li.list3 {
    width: 24%;
  }

  ul.topnav li.list4 {
    width: 25%;
  }

  ul.topnav li.icon {display: none;}

  @media screen and (max-width:680px) {
    ul.topnav li:not(:first-child) {display: none;}
    ul.topnav li.icon {
      float: right;
      display: inline-block;
    }
  }

  @media screen and (max-width:680px) {
    ul.topnav li.list1 {
      width: 70%;
    }

    ul.topnav.responsive {position: relative;}
    ul.topnav.responsive li.icon {
      position: absolute;
      right: 0;
      top: 0;
      display: block;
    }
    ul.topnav.responsive li {
      float: none;
      display: inline;
    }
    ul.topnav.responsive li a {
      display: block;
      text-align: left;
    }

    ul.topnav.responsive li a.toggle-symbol {
      color: black;
    }

  }

  </style>

  <script>
  function myFunction() {
    document.getElementsByClassName("topnav")[0].classList.toggle("responsive");
  }
  </script>

  <br>
  <div class="panel panel-info">
    <ul class="nav nav-pills topnav">
      <li align="center" role="presentation" class="active list1"><a href="{{ url('patient/appointment/details',[$patientId]) }}">Appointment Details</a></li>
      <li align="center" role="presentation" class="list2"><a href="{{ url('patient/treatment/details',[$patientId]) }}">Treatment Info</a></li>
      <li align="center" role="presentation" class="list3"><a href="{{ url('patient/patient/details',[$patientId]) }}">Patient Info</a></li>
      <li align="center" role="presentation" class="list4"><a href="{{ url('patient/referrals/details',[$patientId]) }}">Referrals</a></li>
      <li class="icon">
        <a href="javascript:void(0);" style="font-size:15px;" class="toggle-symbol" onclick="myFunction()">☰</a>
      </li>
    </ul>
  </div>
  
  <div class="panel panel-info filterable table-responsive">
    <!-- Table -->
    <table class="table table-hover"> 
      <thead class="panel-info">
        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="Patient Name" disabled></th>
          <th><input type="text" class="form-control" placeholder="Cell No" disabled></th>
          <th><input type="text" class="form-control" placeholder="Assign Date" disabled></th>
          <th><input type="text" class="form-control" placeholder="Slot Timing" disabled></th>
          <th><input type="text" class="form-control" placeholder="Doctor" disabled></th>
          <th><input type="text" class="form-control" placeholder="Status" disabled></th>
          <th><input type="text" class="form-control" placeholder="Action" disabled></th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
        </tr> 
      </thead>
      <tbody>
        @foreach($appointmentDetails as $appointment)
        <tr>
         <td>{{$appointment->name}}</td>
         <td>{{$appointment->cell_no}}</td>
         <td>{{$appointment->date}}</td>
         <td>{{$appointment->slot}}</td>
         <td>{{$appointment->dentist_name}}</td>
         <td>{{$appointment->status}}</td>

         <td>
          {!! Form::open(array('route' => 'reschedulePatientInformation','class' => 'form')) !!}
          {!! Form::hidden('appointment_id', $appointment->id) !!}
          {!! Form::button('<i class="fa fa-edit fa-fw"></i>Reschedule',array('type' => 'submit','class'=>'btn btn-xs btn-primary')) !!}
          {!! Form::close() !!}
        </td>
        <td>
          {!! Form::open(array('url'=>'deleteRescheduleDetails','class' => 'form')) !!}
          {!! Form::hidden('appointment_id', $appointment->id) !!}
          {!! Form::button('<i class="fa fa-trash fa-fw"></i>Cancel',array('type' => 'submit','class'=>'btn btn-xs btn-danger')) !!}
          {!! Form::close() !!}
        </td>

      </tr>
      @endforeach 
    </tbody>
  </table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div> <!-- /#container -->

@stop