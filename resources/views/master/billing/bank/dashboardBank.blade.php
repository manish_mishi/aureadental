@extends('layouts.masterNav')

@section('title')
        Inventory->Materal Management
@stop

@section('side_bar')

<div class="navbar-default sidebar" role="navigation">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <!-- <li class="sidebar-search">
            <div class="input-group custom-search-form">
              <input type="text" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div> -->
            <!-- /input-group -->
         <!--  </li> -->
          <li>
            <a href="{{ url('master/billing/bank/dashboard') }}" class="active">Bank</a>
          </li>
          
          <li>
            <a href="{{ url('master/billing/personal/dashboard') }}">Personal</a>
          </li>  
        </ul>
      </div>
      <!-- /.sidebar-collapse -->
    </div>

@stop

@section('main')

<div id="wrapper">
  <div id="page-wrapper">

    <div class="panel panel-info">
            <ul style="width:100%" class="nav nav-pills">
              <li style="width:100%" role="presentation" class="active"><a href="{{ url('master/billing/bank/dashboard') }}">Add Bank</a></li>              
          </ul>
      </div>
 <div class="row">
         {!! Form::open(array('route' => 'addbillingbank','class' => 'form')) !!}

         @if (count($errors) > 0)
         <div class="alert alert-danger">
          There were some problems adding the category.<br />
          <ul>
            @foreach ($errors->all() as $error)
            <li></li>
            @endforeach
          </ul>
        </div>
        @endif
    <div class="row marketing">
      <div class="col-lg-12">
        <form class="form-inline" role="form">
          <div class="form-group col-xs-12">
            <div class="col-xs-4">
             {!! Form::label('Account Holder', null, array('class'=>'control-label')) !!}
            </div>
            <div class="col-xs-6">
               {!! Form::text('account_holder', null,array('class'=>'form-control')) !!}
            </div>
            <br><br><br>
          </div>

          <div class="form-group col-xs-12">
            <div class="col-xs-4">
             {!! Form::label('Bank Name', null, array('class'=>'control-label')) !!}
            </div>
            <div class="col-xs-6">
               {!! Form::text('bank_name', null,array('class'=>'form-control')) !!}
            </div>
            <br><br><br>
          </div>

          <div class="form-group col-xs-12">
            <div class="col-xs-4">
             {!! Form::label('Account Number', null, array('class'=>'control-label')) !!}
            </div>
            <div class="col-xs-6">
               {!! Form::text('account_number', null,array('class'=>'form-control')) !!}
            </div>
            <br><br><br>
          </div>

           <div class="form-group col-xs-12">
            <div class="col-xs-4">
             {!! Form::label('Branch', null, array('class'=>'control-label')) !!}
            </div>
            <div class="col-xs-6">
               {!! Form::text('branch', null,array('class'=>'form-control')) !!}
            </div>
            <br><br><br>
          </div>

          <div class="form-group col-xs-12">
            <div class="col-xs-4">
              
            </div>
            <div class="col-xs-4">
                {!! Form::submit('Save',array('class'=>'btn btn-warning')) !!}
            </div>
            
          </div>
       {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>

@stop