@extends('layouts.masterNav')

@section('title')
Treatment Name
@stop

@section('side_bar')


<div class="navbar-default sidebar" role="navigation">
 <div class="sidebar-nav navbar-collapse">
   <ul class="nav" id="side-menu">
    <li>
      <a href="{{ url('master/treatment/name') }}" class="active">Name</a>
    </li>
  </ul>
</div>
</div>
@stop

@section('main')

<div id="wrapper">
 <div id="page-wrapper">

  <div class="row">

    {!! Form::open(array('route'=>'addTreatmentName','class'=>'form')) !!}

    <!-- successfullye added msg -->
    <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
    </div> <!-- end .flash-message -->

    <div class="form-group">
      {!! Form::label('Treatment Name',null,array('class'=>' col-xs-3 control-label')) !!}
      <div class="col-xs-4">
        {!! Form::text('treat_name',null,array('class'=>'form-control'))!!}
      </div>
    </div>

    <br><br>
    <div class="form-group">
      {!! Form::label('Treatment type',null,array('class'=>'col-xs-3 control-label')) !!}

      <div class="col-xs-4">
        <select class="form-control" name="treatment_type_id" id ='treatmentType'>
        <option value>Select</option>
          @foreach($treatmentTypes as $treatmentType)
           <option value="{{$treatmentType->id}}">{{$treatmentType->name}}</option>
          @endForeach
        </select>

      </div>
      <div class="form-group">
        {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning')) !!}
      </div>
    </div>



    {!! Form::close() !!}

  </div>

  <div class="panel panel-info filterable">
    <!-- Table -->
    <table class="table table-hover"> 
      <thead class="panel-info">
        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="#" disabled></th>
          <th><input type="text" class="form-control" placeholder="Treatment Name" disabled></th>
          <th><input type="text" class="form-control" placeholder="Treatment Type" disabled></th>
          <th>Action</th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
        </tr> 
      </thead>
      <tbody>
        @foreach($treatname_details as $treatname)
        <tr>
         <td>{{$treatname->id}}</th>
           <td>{{$treatname->name}}</td>
           <td>{{$treatname->treatment_type_name}}</td>
           <td colspan="2">
            {!! Form::open(array('url'=>'deleteTreatmentName', 'class' => 'form')) !!}
            {!! Form::hidden('treat_name_id', $treatname->id) !!}
            {!! Form::button('<i class="fa fa-trash fa-fw"></i>Delete',array('type' => 'submit','class'=>'btn btn-xs btn-danger')) !!}
            {!! Form::close() !!}
          </td>
        </tr>
        @endforeach 
      </tbody>
    </table>
  </div>

</div>
</div>




@stop





