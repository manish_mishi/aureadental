@extends('layouts.masterNav')

@section('title')
Staff Register -> List View
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/clinic/staff_register/listview') }}" class="active">Staff Registration</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/leave_management/listview') }}">Leave Management</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/clinic/listview') }}">Clinic</a>
      </li>



    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">

 <div id="page-wrapper">

    <!-- successfullye added msg -->
    <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
    </div> <!-- end .flash-message -->    

    <br>
    <!-- Add new material button -->
    <a href="{{ url('master/clinic/staff_register/add') }}" class="btn btn-success pull-right col-xs-2"><i class="fa fa-plus fa-fw"></i>New Staff</a>


    <!-- <div class="form-group col-xs-3">
      <label class="control-label">Staff Type</label>
      <select class="form-control" name="category_id" id="material_category_select">
        <option value="0">ALL</option>
      </select>
    </div>

    <div class="form-group col-xs-3">
      <label class="control-label">Staff Name</label>
      <span id="material_type_select_div">
        Material type will get populated over here
        <select class="form-control">
          <option>ALL</option>
        </select>
      </span>
    </div>

    <div class="form-group col-xs-3">
      <label class="control-label"></label>
      <span id="material_subtype_select_div">
        <!-- Material sub type will get populated over here -->
        <!-- <select class="form-control">
          <option>ALL</option>
        </select>
      </span>
    </div>
 -->
  <br><br>
  <div class="panel panel-info filterable">

   <!-- Table -->
   <table class="table table-hover"> 
    <thead class="panel-info">
       <tr class="filters">
        <th><input type="text" class="form-control" placeholder="id" disabled></th>
        <th><input type="text" class="form-control" placeholder="Staff Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Staff Type" disabled></th>
        <th><input type="text" class="form-control" placeholder="Primary Clinic" disabled></th>
        <th><input type="text" class="form-control" placeholder="Visiting Clinic" disabled></th>

        <!-- <th><input type="text" class="form-control" placeholder="Gender" disabled></th>
        <th><input type="text" class="form-control" placeholder="Cell No" disabled></th>
        <th><input type="text" class="form-control" placeholder="Date of Birth" disabled></th>
        <th><input type="text" class="form-control" placeholder="Email" disabled></th> -->
        <th>Action</th>
        <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
      </tr>
    </thead>
    <tbody>
     @foreach($staff_reg_details as $staff_detail)
     <tr>
       <td>{{$staff_detail->staff_registration_id}}</td>
       <td>{{$staff_detail->staff_name}}</td>
       <td>{{$staff_detail->staff_type}}</td>
       <td>{{$staff_detail->primary_clinic}}</td> 
      <!-- <td>{{$staff_detail->email}}</td> 
       <td>{{$staff_detail->primary_clinic}}</td>
       <td></td> -->
       <td>
   <table class="table table-hover">
            @foreach($visiting_clinic_name as $visiting_clinic_names)
            @if($staff_detail->staff_registration_id == $visiting_clinic_names->staff_reg_id)
            <tr><td>{{$visiting_clinic_names->clinic_name}} </td></tr>
            @endif
            @endforeach
          </table>
        </td>
       <td>
     <a href="{{ url('master/clinic/staff_register/edit',[$staff_detail->staff_registration_id]) }}" class="btn btn-xs btn-primary"><i class="fa fa-edit fa-fw"></i>Edit</a>

      </td>
      <td>
       <a href="{{ url('master/clinic/staff_register/view',[$staff_detail->staff_registration_id]) }}" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-eye-open">View</a>

      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
