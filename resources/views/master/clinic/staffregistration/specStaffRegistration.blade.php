@extends('layouts.masterNav')

@section('title')
        Clinic->Staff Registration
@stop

@section('side_bar')


  <div class="navbar-default sidebar" role="navigation">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <!-- <li class="sidebar-search">
            <div class="input-group custom-search-form">
              <input type="text" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div> -->
            <!-- /input-group -->
         <!--  </li> -->
          <li>
            <a href="{{ url('master/clinic/staffregister/dashboard') }}" class="active">Staff Registration</a>
          </li>
          
          <li>
            <a href="{{ url('master/clinic/leavemanagement/dashboard') }}">Leave Management</a>
          </li>
          
          <li>
            <a href="{{ url('master/clinic/clinic/dashboard') }}">Clinic</a>
          </li>
          

          
        </ul>
      </div>
      <!-- /.sidebar-collapse -->
    </div>

@stop

@section('main')


<div id="wrapper">

 <div id="page-wrapper">
    <div class="panel panel-info">
            <ul style="width:100%" class="nav nav-pills">
              <li style="width:25%" role="presentation"><a href="{{ url('master/clinic/staffregister/add') }}">General</a></li>
              <li style="width:25%" role="presentation"><a href="{{ url('master/clinic/staffregister/qualifications') }}">Qualifications</a></li>
              <li style="width:25%" role="presentation"><a href="{{ url('master/clinic/staffregister/slottimedetails') }}">Slot time details</a></li>
              <li style="width:24%" role="presentation" class="active"><a href="{{ url('master/clinic/staffregister/specialization') }}">Specialization</a></li>
          </ul>
      </div>

      <div class="row">
        <form name='general'>
                <div class="col-xs-6">
                    <div class="form-group">
                            
                        </div>

                        <br><br><br>
                        <div class="form-group">
                           
                        </div>

                        <br><br><br>
                        <div class="form-group">
                           
                        </div>

                        <br><br><br>
                        <div class="form-group">
                           
                        </div>
                </div>

                <div class="col-xs-6">
                    <div class="form-group">
                           
                        </div>

                        <br><br><br>
                        <div class="form-group">
                           
                        </div>

                        <br><br><br>
                        <div class="form-group">
                           
                        </div>

                        <br><br><br>
                        <div class="form-group">
                            
                        </div>

                        <br><br><br>
                        <div class="form-group">
                            
                        </div>

                        <br><br>
                        <div>
                          <div class="col-xs-6"></div>
                            <button type="submit" class="btn btn-warning col-xs-2">Save</button>
                        </div>

                </div>
             </form>
      </div>

 </div>
</div>
<!-- /#wrapper -->

@stop
