@extends('layouts.masterNav')

@section('title')
Clinic->Staff Registration
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="row-fluid">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('master/clinic/staffregister/listview') }}" class="active">Staff Registration</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/leavemanagement/dashboard') }}">Leave Management</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/clinic/dashboard') }}">Clinic</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
 <div id="page-wrapper">

   <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation"><a href="{{ url('master/clinic/staffregister/add') }}">General</a></li>
      <li style="width:25%" role="presentation"><a href="{{ url('master/clinic/staffregister/qualifications') }}">Qualifications</a></li>
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('master/clinic/staffregister/slottimedetails') }}">Slot time details</a></li>
      <li style="width:24%" role="presentation"><a href="{{ url('master/clinic/staffregister/specialization') }}">Specialization</a></li>
    </ul>
  </div>

  <style>
  td {

    padding: 7px;

  }
  </style>

  <div class="panel panel-default filterable table-responsive">
    <table class="table table-hover">
      <thead class="panel-info">
        <tr>
          <th>Days</th>
          <th align="center" width="50%">{!! Form::label('visiting_clinic_name',$visitingClinicName,array('class'=>'form-control')) !!} </th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td height="2%" width="10%">Monday
            <input type="hidden" name="day[]" value="Monday">
          </td>
          <td>
            <div class="panel panel-default filterable table-responsive">
              <table class="table table-hover">

                @foreach($visiting_clinic_slot_timings as $visitingClinicSlotDetail)
                @if($visitingClinicSlotDetail->clinic_day == 'Monday')
                @if($visitingClinicSlotDetail->visiting_clinic_slot_id == $visitingClinicSlotDetail->staff_slot_id)

                <tr id="addDuplicaterMonday">
                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $visitingClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%','readonly')) !!}
                  </td>

                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $visitingClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%','readonly')) !!}
                  </td>
                  <td class="form-group" style="float: left;">
                  </td>
                </tr>
                @endif
                @endif
                @endforeach

              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td  height="2%" style="padding:5%">Tuesday
            <input type="hidden" name="day[]" value="Tuesday">
          </td>
          <td>
            <div class="panel panel-default filterable table-responsive">
              <table class="table table-hover">

                @foreach($visiting_clinic_slot_timings as $visitingClinicSlotDetail)
                @if($visitingClinicSlotDetail->clinic_day == 'Tuesday')
                @if($visitingClinicSlotDetail->visiting_clinic_slot_id == $visitingClinicSlotDetail->staff_slot_id)

                <tr id="addDuplicaterTuesday">
                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $visitingClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%','readonly')) !!}
                  </td>

                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $visitingClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%','readonly')) !!}
                  </td>
                  <td class="form-group" style="float: left;">
                  </td>


                </tr>

                @endif
                @endif
                @endforeach

              </table>
            </div>
          </td>
        </tr>

        <tr>
          <td style="padding:5%">Wednesday
            <input type="hidden" name="day[]" value="Wednesday">
          </td>

          <td>
            <div class="panel panel-default filterable table-responsive">
              <table class="table table-hover">

                @foreach($visiting_clinic_slot_timings as $visitingClinicSlotDetail)
                @if($visitingClinicSlotDetail->clinic_day == 'Wednesday')
                @if($visitingClinicSlotDetail->visiting_clinic_slot_id == $visitingClinicSlotDetail->staff_slot_id)

                <tr id="addDuplicaterWednesday">
                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $visitingClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%','readonly')) !!}
                  </td>

                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $visitingClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%','readonly')) !!}
                  </td>
                  <td class="form-group" style="float: left;">
                  </td>
                </tr>

                @endif
                @endif
                @endforeach

              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td style="padding:5%">Thursday
            <input type="hidden" name="day[]" value="Thursday">
          </td>
          <td>
            <div class="panel panel-default filterable table-responsive">
              <table class="table table-hover"> 
                @foreach($visiting_clinic_slot_timings as $visitingClinicSlotDetail)
                @if($visitingClinicSlotDetail->clinic_day == 'Thursday')
                @if($visitingClinicSlotDetail->visiting_clinic_slot_id == $visitingClinicSlotDetail->staff_slot_id)

                <tr id="addDuplicaterThursday">
                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $visitingClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%','readonly')) !!}
                  </td>

                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $visitingClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%','readonly')) !!}
                  </td>
                  <td class="form-group" style="float: left;">
                  </td>
                </tr>
                @endif
                @endif
                @endforeach

              </table>
            </div>
          </td>
          <tr>
            <td style="padding:5%">Friday
              <input type="hidden" name="day[]" value="Friday">
            </td>
            <td>
              <div class="panel panel-default filterable table-responsive">
                <table class="table table-hover">

                  @foreach($visiting_clinic_slot_timings as $visitingClinicSlotDetail)
                  @if($visitingClinicSlotDetail->clinic_day == 'Friday')
                  @if($visitingClinicSlotDetail->visiting_clinic_slot_id == $visitingClinicSlotDetail->staff_slot_id)

                  <tr id="addDuplicaterFriday">
                    <td class="form-group" style="float: left padding:8%;">
                      {!! Form::text(null, $visitingClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%','readonly')) !!}
                    </td>

                    <td class="form-group" style="float: left padding:8%;">
                      {!! Form::text(null, $visitingClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%','readonly')) !!}
                    </td>
                    <td class="form-group" style="float: left;">
                    </td>
                  </tr>
                  @endif
                  @endif
                  @endforeach

                </table>
              </div>
            </td>
          </tr>
        </tr>

        <tr>
          <td style="padding:5%">Saturday
            <input type="hidden" name="day[]" value="Saturday">
          </td>
          <td>
            <div class="panel panel-default filterable table-responsive">
              <table class="table table-hover">

                @foreach($visiting_clinic_slot_timings as $visitingClinicSlotDetail)
                @if($visitingClinicSlotDetail->clinic_day == 'Saturday')
                @if($visitingClinicSlotDetail->visiting_clinic_slot_id == $visitingClinicSlotDetail->staff_slot_id)

                <tr id="addDuplicaterSaturday">
                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $visitingClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%','readonly')) !!}
                  </td>

                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $visitingClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%','readonly')) !!}
                  </td>
                  <td class="form-group" style="float: left;">
                  </td>
                </tr>

                @endif
                @endif
                @endforeach
              </table>
            </div>
          </td>
          <tr>

            <td style="padding:5%">Sunday
              <input type="hidden" name="day[]" value="Sunday">
            </td>
            <td>
              <div class="panel panel-default filterable table-responsive">
                <table class="table table-hover">

                  @foreach($visiting_clinic_slot_timings as $visitingClinicSlotDetail)
                  @if($visitingClinicSlotDetail->clinic_day == 'Sunday')
                  @if($visitingClinicSlotDetail->visiting_clinic_slot_id == $visitingClinicSlotDetail->staff_slot_id)

                  <tr id="addDuplicaterSunday">
                    <td class="form-group" style="float: left padding:8%;">
                      {!! Form::text(null, $visitingClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%','readonly')) !!}
                    </td>

                    <td class="form-group" style="float: left padding:8%;">
                      {!! Form::text(null, $visitingClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%','readonly')) !!}
                    </td>
                    <td class="form-group" style="float: left;">
                    </td>
                  </tr>

                  @endif
                  @endif
                  @endforeach

                </table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <div>
     <a href="{{ url('master/clinic/staff_register/slot_time_details/view',[$staff_reg_id]) }}" class="btn btn-danger pull-right">&#10006;Cancel</a>
   </div>

   <!-- back to top of the page -->
   <p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
  </p>

</div>
</div>

@stop