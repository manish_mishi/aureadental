@extends('layouts.masterNav')

@section('title')
Clinic->Staff Registration
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/clinic/staff_register/listview') }}" class="active">Staff Registration</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/leave_management/listview') }}">Leave Management</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/clinic/listview') }}">Clinic</a>
      </li>      
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')


<div id="wrapper">

 <div id="page-wrapper">

  <div class="panel panel-info">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation" class="active"><a href="{{ url('master/clinic/staff_register/edit',[$staff_reg_id]) }}">General</a></li>
    <li style="width:25%" role="presentation"><a>Qualifications</a></li>
    <li style="width:25%" role="presentation"><a href="{{url('master/clinic/staff_register/slot_time_details/edit',[$staff_reg_id])}}">Slot time details</a></li>
    <li style="width:24%" role="presentation"><a>Specialization</a></li>
  </ul>
</div>

<div class="row">

  {!! Form::open(array('route' => 'updateStaffRegistration','class' => 'form')) !!}

  <div class="col-xs-6">
    {!! Form::label('Staff Type', null, array('class'=>'col-xs-4  control-label')) !!}
    <div class="col-md-6">
      {!! Form::text('staff_type', $staff_registration_details->staff_type,array('class'=>'form-control')) !!}
      {!! Form::hidden('staff_type_id', $staff_registration_details->staff_type_id) !!}
    </div>


    <br><br><br>
    <div class="form-group">
      {!! Form::label('Name', null, array('class'=>'col-xs-4  control-label')) !!}
      <div class="col-xs-6 col-md-6">
       {!! Form::text('name', $staff_registration_details->staff_name,array('class'=>'form-control')) !!}
     </div>
   </div>

   <br><br><br>
   <div class="form-group">
    {!! Form::label('Cell No', null, array('class'=>'col-xs-4  control-label')) !!}
    <div class="col-xs-6">
      {!! Form::text('cell_no',$staff_registration_details->cell_no,array('class'=>'form-control')) !!}
    </div>
  </div>

  <br><br><br>
  <div class="form-group">
    {!! Form::label('Gender', null, array('class'=>'col-xs-4  control-label')) !!}
    <div class="col-xs-6">
      @if($staff_registration_details->gender == 'male')
      {!! Form::radio('gender','male',true) !!}Male
      {!! Form::radio('gender','female') !!}Female
      @else
      {!! Form::radio('gender','male') !!}Male
      {!! Form::radio('gender','female',true) !!}Female
      @endif

    </div>
  </div>                       
</div>

<div class="col-xs-6">
  <div class="form-group">
    {!! Form::label('DoB', null, array('class'=>'col-xs-4  control-label')) !!}
    <div id="date1" class="col-xs-6 form-group">
     <input  data-provide="datepicker" data-date-format="dd/mm/yyyy" id="Work_Allocation_Date" class="form-control" name="date_of_birth" value="<?php echo $staff_registration_details->date_of_birth;?>">
   </div>
 </div>

 <br><br><br>
 <div class="form-group">
  {!! Form::label('Email', null, array('class'=>'col-xs-4  control-label')) !!}
  <div class="col-xs-6">
    {!! Form::email('email',$staff_registration_details->email,array('class'=>'form-control')) !!}
  </div>
</div>


<br><br><br>
{!! Form::label('Primary Clinic', null, array('class'=>'col-xs-4 control-label')) !!}

<div class="col-xs-6">
  <select class="form-control" name="primary_clinic_id" id ='primary_clinic' required>
   <option value='{{$staff_registration_details->clinic_id}}'>{{$staff_registration_details->primary_clinic}}</option>
   @foreach($clinic_details as $clinicdetails)
   <option value="{{$clinicdetails->id}}">{{$clinicdetails->name}}</option>
   @endForeach
 </select>
 {!! Form::hidden('clinic_type_id[]', $clinic_type[0]->id) !!}
</div>


<br><br><br>
<div class="form-group">
  {!! Form::label('Visiting Clinics', null, array('class'=>'col-xs-4 control-label')) !!}
  <div class="col-xs-6">
    <select class="form-control" name="visiting_clinic_id[]"  id ='visiting_clinic' >
      <option value>select</option>
      @foreach($clinic_details as $clinicdetails)
      <option value="{{$clinicdetails->id}}">{{$clinicdetails->name}}</option>
      @endForeach
    </select>
    {!! Form::hidden('clinic_type_id[]', $clinic_type[1]->id) !!}   
  </div>

  <div class="col-xs-1 col-lg-1 col-md-1">
    <a onClick="duplicateDateTime()" class="btn btn-info btn-md">
      <span class="glyphicon glyphicon-plus"></span> 
    </a>
  </div>

  <br>
  <div class="row " id="duplicater" style="display:none;margin-top:1%;">
    <div class="col-xs-4 col-lg-4 col-md-4">
    </div>   
    <div class="col-xs-61 col-lg-6 col-md-6">
      {!! form::text(null,null,array('id'=>'visiting_clinic1','class'=>'form-control','required','readonly'))!!}
      {!! form::hidden(null,null,array('id'=>'visiting_clinic_id','class'=>'form-control visiting_clinic','required','hidden'))!!}
    </div>
    <div class="col-xs-1 col-lg-1 col-md-1" id="delete">
      {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('id' =>'delete_btn','class'=>'btn btn-md btn-danger','style'=>'visibility:hidden','onClick'=>'removeduplicate(this)')) !!}
    </div>
  </div>

<br>
  @foreach($visiting_clinic_details as $visiting_clinic)
  <div class="row " style="display:block;margin-top:1%;">
    <div class="col-xs-4 col-lg-4 col-md-4">
    </div>   
    <div class="col-xs-61 col-lg-6 col-md-6">
      {!! form::text(null,$visiting_clinic->clinic_name,array('id'=>'visiting_clinic1','class'=>'form-control visiting_clinic','required','readonly'))!!}
      {!! form::hidden('visiting_clinic_id[]',$visiting_clinic->clinic_id,array('id'=>'visiting_clinic_id','class'=>'form-control type','required','hidden'))!!}
    </div>
    <div class="col-xs-1 col-lg-1 col-md-1" id="delete">
      <a href="{{ url('master/clinic/staff_register/delete',[$visiting_clinic->id, $visiting_clinic->staff_reg_id]) }}">{!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('id' =>'delete_btn','class'=>'btn btn-md btn-danger','onClick'=>'removeduplicate(this)')) !!}</a>
    </div>
  </div>
  @endForeach
</div>

<br>
<div>
  {!! HTML::link('master/clinic/staff_register/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
<!--  <a href="{{ url('master/clinic/staffregister/viewprimaryvisitingclinic',[$staff_registration_details->staff_reg_id]) }}" class="btn  btn-default pull-right">&#x2714;Next</a>
-->
{!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning','style'=>'margin-left:58%')) !!}

</div>
</div> 
{!! Form::hidden('staff_reg_id', $staff_registration_details->staff_reg_id) !!}

{!! Form::close();!!}

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>


<script>
var i = 0;
$("#visiting_clinic").change(function(){

  $('#visiting_clinic1').val($('option:selected',this).text());
  $('#visiting_clinic_id').val($(this).val());
});

function duplicateDateTime() {

  var visitingclinic = document.getElementById('visiting_clinic1').value;
  var visitingclinicID=document.getElementById('visiting_clinic_id').value;

  if(visitingclinic!=''){

    document.getElementById('delete_btn').style.visibility = "visible";
    var original = document.getElementById('duplicater');
    original.style.display = "block";

    document.getElementById('visiting_clinic1').value =visitingclinic;
    document.getElementById('visiting_clinic_id').value =visitingclinicID;


  var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicater" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);
    document.getElementById('delete_btn').style.visibility = "hidden";
    original.style.display = "none";


    document.getElementById('visiting_clinic').value ="";

    document.getElementById('visiting_clinic_id').value ="";


    var dateElement = document.getElementById("duplicater"+i).getElementsByClassName('visiting_clinic')[0];
    dateElement.setAttribute("name","visiting_clinic_id[]");

  }

}

function removeduplicate(element)
{
    element=element.parentNode.parentNode;//gets the id of the parent
    element.parentNode.removeChild(element);
  }
  </script>
  <!-- /#wrapper -->

  @stop
