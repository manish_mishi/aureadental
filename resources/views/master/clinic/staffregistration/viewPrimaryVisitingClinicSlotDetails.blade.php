@extends('layouts.masterNav')

@section('title')
Clinic->Staff Registration
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="row-fluid">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('master/clinic/staff_register/listview') }}" class="active">Staff Registration</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/leave_management/listview') }}">Leave Management</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/clinic/listview') }}">Clinic</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
 <div id="page-wrapper">

   <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation"><a href="{{ url('master/clinic/staff_register/view',[$staff_reg_id]) }}">General</a></li>
      <li style="width:25%" role="presentation"><a>Qualifications</a></li>
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('master/clinic/staff_register/slot_time_details/view',[$staff_reg_id]) }}">Slot time details</a></li>
      <li style="width:24%" role="presentation"><a>Specialization</a></li>
    </ul>
  </div>

  <style>
  td {

    padding: 7px;

  }
  </style>

  <div class="panel panel-default filterable table-responsive">
    <table class="table table-hover">
      <thead class="panel-info">
        <tr>
         <tr> 
          <th>Primary Clinic</th>
          <th align="center">
            {!! Form::label('primary_clinic',$primary_clinic_name,array('class'=>'form-control')) !!} 
          </th>
          <th> 
            <a href="{{ url('master/clinic/staff_register/primary_clinic_slot_time_details/view',[$staff_reg_id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-eye-open">ViewSlot</a>  
          </th>
        </tr>
      </tr>
      <tr>
        <th align="center">Visiting Clinic</th>
        <th>

          @foreach ($visiting_clinic_details as  $key =>$clinic_name) 
          @if($clinic_name != null)
          {!! Form::label('visiting_clinic', $clinic_name->clinic_name,array('class'=>'form-control')) !!}
          @endif
          @endforeach

        </th>
        <td width="20%">  
          @foreach ($visiting_clinic_details as  $key =>$id)
          @if($clinic_name != null)
          <a href="{{ url('master/clinic/staff_register/visiting_clinic_slot_time_details/view',[$id->id]) }}" class="btn btn-default" value="$id->id"><span class="glyphicon glyphicon-eye-open">ViewSlot</a>  
          @endif
          @endforeach
        </td>
      </tr>
    </thead>
  </table>
</div>

<div>
  {!! HTML::link('master/clinic/staff_register/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
@stop