@extends('layouts.masterNav')

@section('title')
Clinic->Staff Registration
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/clinic/staff_register/listview') }}" class="active">Staff Registration</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/leave_management/listview') }}">Leave Management</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/clinic/listview') }}">Clinic</a>
      </li>      
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
 <div id="page-wrapper">

  <div class="panel panel-info">
   <ul style="width:100%" class="nav nav-pills">
    <li style="width:25%" role="presentation" class="active"><a href="{{url('master/clinic/staff_register/view',[$staff_reg_id])}}">General</a></li>
    <li style="width:25%" role="presentation"><a>Qualifications</a></li>
    <li style="width:25%" role="presentation"><a href="{{ url('master/clinic/staff_register/slot_time_details/view',[$staff_reg_id]) }}">Slot time details</a></li>
    <li style="width:24%" role="presentation"><a>Specialization</a></li>
  </ul>
</div>

<div class="row">

  <div class="col-xs-6">
      {!! Form::label('Staff Type', null, array('class'=>'col-xs-4  control-label')) !!}
      <div class="col-md-6">
        {!! Form::text('staff_type', $staff_registration_details->staff_type,array('class'=>'form-control','readonly')) !!}
        {!! Form::hidden('staff_type_id', $staff_registration_details->staff_type_id) !!}
      </div>
     
     <br><br><br>
     <div class="form-group">
      {!! Form::label('Name', null, array('class'=>'col-xs-4  control-label')) !!}
      <div class="col-xs-6 col-md-6">
       {!! Form::text('name', $staff_registration_details->staff_name,array('class'=>'form-control','readonly')) !!}
     </div>
   </div>

   <br><br><br>
   <div class="form-group">
    {!! Form::label('Cell No', null, array('class'=>'col-xs-4  control-label')) !!}
    <div class="col-xs-6">
      {!! Form::text('cell_no',$staff_registration_details->cell_no,array('class'=>'form-control','readonly')) !!}
    </div>
  </div>

  <br><br><br>
  <div class="form-group">
    {!! Form::label('Gender', null, array('class'=>'col-xs-4  control-label')) !!}
    <div class="col-xs-6">
    @if($staff_registration_details->gender == 'male')
                  {!! Form::radio('gender','male',true) !!}Male
                  {!! Form::radio('gender','female') !!}Female
                @else
                  {!! Form::radio('gender','male') !!}Male
                  {!! Form::radio('gender','female',true) !!}Female
                @endif              
    </div>
  </div>                       
</div>

<div class="col-xs-6">
  <div class="form-group">
    {!! Form::label('DoB', null, array('class'=>'col-xs-4  control-label')) !!}
    <div id="date1" class="col-xs-6 form-group">
     <input readonly data-provide="datepicker" data-date-format="dd/mm/yyyy" id="Work_Allocation_Date" class="form-control" name="date_of_birth" value="<?php echo $staff_registration_details->date_of_birth;?>">
   </div>
 </div>

 <br><br><br>
 <div class="form-group">
  {!! Form::label('Email', null, array('class'=>'col-xs-4  control-label')) !!}
  <div class="col-xs-6">
    {!! Form::email('email',$staff_registration_details->email,array('class'=>'form-control','readonly')) !!}
  </div>
</div>


<br><br><br>
  {!! Form::label('Primary Clinic', null, array('class'=>'col-xs-4  control-label')) !!}
 
 <div class="col-md-6">
        {!! Form::text('primary_clinic', $staff_registration_details->primary_clinic,array('class'=>'form-control','readonly')) !!}
        {!! Form::hidden('staff_reg_id', $staff_registration_details->staff_reg_id) !!}
      </div>

<br><br><br>

 <div class="form-group">
  {!! Form::label('Visiting Clinics', null, array('class'=>'col-xs-4 control-label')) !!}
  <div class="col-md-6">
   @foreach($visiting_clinic_details as $visiting_clinic_details)
   {!! Form::text('visiting_clinic', $visiting_clinic_details->clinic_name,array('class'=>'form-control','style'=>'margin-top:2%;','readonly')) !!}
   {!! Form::hidden('visiting_clinic_id', $visiting_clinic_details->staff_reg_id) !!}
   @endForeach
 </div>

  <br>
  <div class="row " id="duplicater" style="display:none;">
    <br>
    <div class="col-xs-4 col-lg-4 col-md-4">
    </div>   
    <div class="col-xs-61 col-lg-6 col-md-6">
      {!! form::text(null,null,array('id'=>'visiting_clinic1','class'=>'form-control visiting_clinic','required','readonly'))!!}
      {!! form::hidden(null,null,array('id'=>'visiting_clinic_id','class'=>'form-control type','required','hidden'))!!}
    </div>
    <div class="col-xs-1 col-lg-1 col-md-1" id="delete">
      {!! Form::button('Delete',array('id' =>'delete_btn','class'=>'btn btn-xs btn-danger','style'=>'visibility:hidden','onClick'=>'removeduplicate(this)')) !!}
    </div>
  </div>
</div>

<br><br><br>
<div>
  <a href="{{ url('master/clinic/staff_register/slot_time_details/view',[$staff_registration_details->staff_reg_id]) }}" class="btn btn-default" style="margin-left:60%">&#x2714;Next</a>
{!! HTML::link('master/clinic/staff_register/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
</div>

</div> 
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

  @stop
