@extends('layouts.masterNav')

@section('title')
Clinic->Staff Registration
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="row-fluid">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('master/clinic/staff_register/listview') }}" class="active">Staff Registration</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/leave_management/listview') }}">Leave Management</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/clinic/listview') }}">Clinic</a>
      </li>



    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
 <div id="page-wrapper">

   <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:25%" role="presentation"><a href="{{ url('master/clinic/staff_register/add') }}">General</a></li>
      <li style="width:25%" role="presentation"><a href="{{ url('master/clinic/staff_register/qualifications') }}">Qualifications</a></li>
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('master/clinic/staff_register/slot_time_details') }}">Slot time details</a></li>
      <li style="width:24%" role="presentation"><a href="{{ url('master/clinic/staff_register/specialization') }}">Specialization</a></li>
    </ul>
  </div>

  <style>
  td {

    padding: 7px;

  }
  </style>

  {!! Form::open(array( 'route'=>'addstaffregistrationslotdetails','class'=>'form')); !!}
  <div class="panel panel-default filterable table-responsive">
    <table class="table table-hover">
      <thead class="panel-info">
        <tr>
          <th>Days</th>
          <th align="center" width="50%"><?php $primary_clinic = Session::get('primary_clinic_name') ?>{!! Form::label('primary_clinic',$primary_clinic_name,array('class'=>'form-control')) !!} </th>
        </tr>
      </thead>

      <tbody>

        <tr>
          <td height="2%" width="5%">Monday
            <input type="hidden" name="day[]" value="Monday">
          </td>

          <td>
            <div class="panel panel-default filterable table-responsive">
              <table class="table table-hover">
                <tr width="5%">
                  <td class="form-group" style="float: left padding:5%;">
                    {!! Form::label('From', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                    {!! Form::text('time[Monday][]', null,array('class'=>'form-control start_time col-xs-1','id'=>'originalMonStartTime','oninput'=>'inputMonday()')) !!}
                  </td>

                  <td class="form-group" style="float: left padding:5%;">
                    {!! Form::label('To', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                    {!! Form::text('time[Monday][]', null,array('class'=>'form-control end_time col-xs-1','id'=>'originalMonEndTime')) !!}
                  </td>
                  <br>
                  <td class="form-group" style="float: left padding:5%;">
                    {!! Form::button('<span class="glyphicon glyphicon-plus"></span>',array('class'=>'form-control btn btn-info col-xs-1','onClick'=>'duplicateMonTime()','id'=>'end_time')) !!}
                  </td>
                </tr>

                @foreach($primary_clinic_slot_timings as $primaryClinicSlotDetail)
                @if($primaryClinicSlotDetail->clinic_day == 'Monday')
                @if($primaryClinicSlotDetail->primary_clinic_slot_id == $primaryClinicSlotDetail->staff_slot_id)

                <tr id="addDuplicaterMonday">
                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $primaryClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%')) !!}
                  </td>

                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $primaryClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%')) !!}
                  </td>
                  <td class="form-group" style="float: left;">
                    <a href="{{ url('master/clinic/staff_register/primary_clinic_slot_time_details/status_delete',[$primaryClinicSlotDetail->staff_clinic_id,$primaryClinicSlotDetail->p_slot_timing_id])}}">{!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}</a>
                  </td>


                </tr>

                @endif

                @endif
                @endforeach



                <tr id="duplicaterMon" style="display:none;">
                  <td style="float: left padding:8%;">
                    {!! Form::text('null', null,array('class'=>'form-control start_time','id'=>'dupMonStartTime')) !!}
                  </td>

                  <td style="float: left padding:8%;">
                    {!! Form::text('null', null,array('class'=>'form-control end_time','id'=>'dupMonEndTime')) !!}
                  </td>

                  <td style="float: left padding:5%;">
                    {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}
                  </td>

                </tr>
              </table>
            </div>
          </td>
        </tr>

        <tr>
          <td  height="2%" style="padding:5%">Tuesday
            <input type="hidden" name="day[]" value="Tuesday">
          </td>
          <td>
            <div class="panel panel-default filterable table-responsive">
              <table class="table table-hover">
                <tr>
                  <td class="form-group" style="float: left padding:5%;">
                    {!! Form::label('From', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                    {!! Form::text('time[Tuesday][]', null,array('class'=>'form-control start_time col-xs-1','id'=>'originalTueStartTime','style'=>'50%','oninput'=>'inputTuesday()')) !!}
                  </td>

                  <td class="form-group" style="float: left;">
                    {!! Form::label('To', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                    {!! Form::text('time[Tuesday][]', null,array('class'=>'form-control end_time col-xs-1','id'=>'originalTueEndTime','style'=>'50%')) !!}
                  </td>

                  <td class="form-group" style="float: left padding:5%;">
                    {!! Form::button('<span class="glyphicon glyphicon-plus"></span>',array('class'=>'form-control btn btn-info col-xs-1','onClick'=>'duplicateTueTime()','id'=>'end_time')) !!}
                  </td>
                </tr>
                @foreach($primary_clinic_slot_timings as $primaryClinicSlotDetail)
                @if($primaryClinicSlotDetail->clinic_day == 'Tuesday')
                @if($primaryClinicSlotDetail->primary_clinic_slot_id == $primaryClinicSlotDetail->staff_slot_id)

                <tr id="addDuplicaterTuesday">
                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $primaryClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%')) !!}
                  </td>

                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $primaryClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%')) !!}
                  </td>
                  <td class="form-group" style="float: left;">
                    <a href="{{ url('master/clinic/staff_register/primary_clinic_slot_time_details/status_delete',[$primaryClinicSlotDetail->staff_clinic_id,$primaryClinicSlotDetail->p_slot_timing_id])}}">{!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}</a>
                  </td>


                </tr>

                @endif

                @endif
                @endforeach



                <tr id="duplicaterTue" style="display:none;">
                  <td class="form-group" style="float: left,padding:8%;">
                    {!! Form::text('null', null,array('class'=>'form-control start_time ','id'=>'dupTueStartTime','style'=>'50%')) !!}
                  </td>

                  <td class="form-group" style="float: left,padding:8%;">
                    {!! Form::text('null', null,array('class'=>'form-control end_time ','id'=>'dupTueEndTime','style'=>'50%')) !!}
                  </td>

                  <td class="form-group" style="float: left,padding:8%;">
                    {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}
                  </td>

                </tr>
              </table>
            </div>
          </td>
        </tr>

        <tr>
          <td style="padding:5%">Wednesday
            <input type="hidden" name="day[]" value="Wednesday">
          </td>

          <td>
            <div class="panel panel-default filterable table-responsive">
              <table class="table table-hover">
                <tr>
                  <td class="form-group" style="float: left padding:5%;">
                    {!! Form::label('From', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                    {!! Form::text('time[Wednesday][]', null,array('class'=>'form-control start_time col-xs-1','id'=>'originalWedStartTime','style'=>'50%','oninput'=>'inputWednesday()')) !!}
                  </td>

                  <td class="form-group" style="float: left;">
                    {!! Form::label('To', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                    {!! Form::text('time[Wednesday][]', null,array('class'=>'form-control end_time col-xs-1','id'=>'originalWedEndTime','style'=>'50%')) !!}
                  </td>

                  <td class="form-group" style="float: left padding:5%;">
                    {!! Form::button('<span class="glyphicon glyphicon-plus"></span>',array('class'=>'form-control btn btn-info col-xs-1','onClick'=>'duplicateWedTime()','id'=>'end_time')) !!}
                  </td>
                </tr>

                @foreach($primary_clinic_slot_timings as $primaryClinicSlotDetail)
                @if($primaryClinicSlotDetail->clinic_day == 'Wednesday')
                @if($primaryClinicSlotDetail->primary_clinic_slot_id == $primaryClinicSlotDetail->staff_slot_id)

                <tr id="addDuplicaterWednesday">
                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $primaryClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%')) !!}
                  </td>

                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text(null, $primaryClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%')) !!}
                  </td>
                  <td class="form-group" style="float: left;">
                    <a href="{{ url('master/clinic/staff_register/primary_clinic_slot_time_details/status_delete',[$primaryClinicSlotDetail->staff_clinic_id,$primaryClinicSlotDetail->p_slot_timing_id])}}">{!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}</a>
                  </td>


                </tr>

                @endif

                @endif
                @endforeach


                <tr id="duplicaterWed" style="display:none;">
                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text('null', null,array('class'=>'form-control start_time','id'=>'dupWedStartTime','style'=>'50%')) !!}
                  </td>

                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::text('null', null,array('class'=>'form-control end_time','id'=>'dupWedEndTime','style'=>'50%')) !!}
                  </td>

                  <td class="form-group" style="float: left padding:8%;">
                    {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}
                  </td>

                </tr>
              </table>
            </div>
          </td>
        </tr>

        <tr>
          <td style="padding:5%">Thursday
            <input type="hidden" name="day[]" value="Thursday">
          </td>
          <td>

            <div class="panel panel-default filterable table-responsive">
              <table class="table table-hover">              <tr>
                <td class="form-group" style="float: left padding:5%;">
                  {!! Form::label('From', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                  {!! Form::text('time[Thursday][]', null,array('class'=>'form-control start_time col-xs-1','id'=>'originalThuStartTime','style'=>'50%','oninput'=>'inputThursday()')) !!}
                </td>

                <td class="form-group" style="float: left;">
                  {!! Form::label('To', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                  {!! Form::text('time[Thursday][]', null,array('class'=>'form-control end_time col-xs-1','id'=>'originalThuEndTime','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:5%;">
                  {!! Form::button('<span class="glyphicon glyphicon-plus"></span>',array('class'=>'form-control btn btn-info col-xs-1','onClick'=>'duplicateThuTime()','id'=>'end_time')) !!}
                </td>
              </tr>

              @foreach($primary_clinic_slot_timings as $primaryClinicSlotDetail)
              @if($primaryClinicSlotDetail->clinic_day == 'Thursday')
              @if($primaryClinicSlotDetail->primary_clinic_slot_id == $primaryClinicSlotDetail->staff_slot_id)

              <tr id="addDuplicaterThursday">
                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text(null, $primaryClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text(null, $primaryClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%')) !!}
                </td>
                <td class="form-group" style="float: left;">
                  <a href="{{ url('master/clinic/staff_register/primary_clinic_slot_time_details/status_delete',[$primaryClinicSlotDetail->staff_clinic_id,$primaryClinicSlotDetail->p_slot_timing_id])}}">{!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}</a>
                </td>


              </tr>

              @endif

              @endif
              @endforeach



              <tr id="duplicaterThu" style="display:none;">
                <td class="form-group" style="float: left padding:8% ;">
                  {!! Form::text('null', null,array('class'=>'form-control start_time col-xs-1','id'=>'dupThuStartTime','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text('null', null,array('class'=>'form-control end_time col-xs-1','id'=>'dupThuEndTime','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}
                </td>

              </tr>
            </table>
          </div>
        </td>
      </tr>

      <tr>
        <td style="padding:5%">Friday
          <input type="hidden" name="day[]" value="Friday">
        </td>
        <td>
          <div class="panel panel-default filterable table-responsive">
            <table class="table table-hover">
              <tr>
                <td class="form-group" style="float: left padding:5%;">
                  {!! Form::label('From', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                  {!! Form::text('time[Friday][]', null,array('class'=>'form-control start_time col-xs-1','id'=>'originalFriStartTime','style'=>'50%','oninput'=>'inputFriday()')) !!}
                </td>

                <td class="form-group" style="float: left;">
                  {!! Form::label('To', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                  {!! Form::text('time[Friday][]', null,array('class'=>'form-control end_time col-xs-1','id'=>'originalFriEndTime','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:5%;">
                  {!! Form::button('<span class="glyphicon glyphicon-plus"></span>',array('class'=>'form-control btn btn-info col-xs-1','onClick'=>'duplicateFriTime()','id'=>'end_time')) !!}
                </td>
              </tr>

              @foreach($primary_clinic_slot_timings as $primaryClinicSlotDetail)
              @if($primaryClinicSlotDetail->clinic_day == 'Friday')

              <tr id="addDuplicaterFriday">
                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text('time[Friday]['.$primaryClinicSlotDetail->p_slot_timing_id.'][]', $primaryClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text('time[Friday]['.$primaryClinicSlotDetail->p_slot_timing_id.'][]', $primaryClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%')) !!}
                </td>
                <td class="form-group" style="float: left;">
                  <a href="{{ url('master/clinic/staff_register/primary_clinic_slot_time_details/status_delete',[$primaryClinicSlotDetail->staff_clinic_id,$primaryClinicSlotDetail->p_slot_timing_id])}}">{!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}</a>
                </td>


              </tr>

              @endif
              @endforeach

              <tr id="duplicaterFri" style="display:none;">
                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text('null', null,array('class'=>'form-control start_time ','id'=>'dupFriStartTime','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text('null', null,array('class'=>'form-control end_time','id'=>'dupFriEndTime','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}
                </td>

              </tr>
            </table>
          </div>
        </td>
      </tr>
      <tr>
        <td style="padding:5%">Saturday
          <input type="hidden" name="day[]" value="Saturday">
        </td>
        <td>
          <div class="panel panel-default filterable table-responsive">
            <table class="table table-hover">
              <tr>
                <td class="form-group" style="float: left padding:5%;">
                  {!! Form::label('From', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                  {!! Form::text('time[Saturday][]', null,array('class'=>'form-control start_time col-xs-1','id'=>'originalSatStartTime','style'=>'50%','oninput'=>'inputSaturday()')) !!}
                </td>

                <td class="form-group" style="float: left;">
                  {!! Form::label('To', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                  {!! Form::text('time[Saturday][]', null,array('class'=>'form-control end_time col-xs-1','id'=>'originalSatEndTime','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:5%;">
                  {!! Form::button('<span class="glyphicon glyphicon-plus"></span>',array('class'=>'form-control btn btn-info col-xs-1','onClick'=>'duplicateSatTime()','id'=>'end_time')) !!}
                </td>
              </tr>

              @foreach($primary_clinic_slot_timings as $primaryClinicSlotDetail)
              @if($primaryClinicSlotDetail->clinic_day == 'Saturday')
              @if($primaryClinicSlotDetail->primary_clinic_slot_id == $primaryClinicSlotDetail->staff_slot_id)

              <tr id="addDuplicaterSaturday">
                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text(null, $primaryClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text(null, $primaryClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%')) !!}
                </td>
                <td class="form-group" style="float: left;">
                  <a href="{{ url('master/clinic/staff_register/primary_clinic_slot_time_details/status_delete',[$primaryClinicSlotDetail->staff_clinic_id,$primaryClinicSlotDetail->p_slot_timing_id])}}">{!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}</a>
                </td>


              </tr>

              @endif
              @endif
              @endforeach 

              <tr id="duplicaterSat" style="display:none;">
                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text('null', null,array('class'=>'form-control start_time col-xs-1','id'=>'dupSatStartTime','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text('null', null,array('class'=>'form-control end_time col-xs-1','id'=>'dupSatEndTime','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left pading:8%;">
                  {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}
                </td>

              </tr>
            </table>
          </div>
        </td>
      </tr>

      <tr>
        <td style="padding:5%">Sunday
          <input type="hidden" name="day[]" value="Sunday">
        </td>
        <td>
          <div class="panel panel-default filterable table-responsive">
            <table class="table table-hover">
              <tr>
                <td class="form-group" style="float: left padding:5%;">
                  {!! Form::label('From', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                  {!! Form::text('time[Sunday][]', null,array('class'=>'form-control start_time col-xs-1','id'=>'originalSunStartTime','style'=>'50%','oninput'=>'inputSunday()')) !!}
                </td>

                <td class="form-group" style="float: left;">
                  {!! Form::label('To', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                  {!! Form::text('time[Sunday][]', null,array('class'=>'form-control end_time col-xs-1','id'=>'originalSunEndTime','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:5%;">
                  {!! Form::button('<span class="glyphicon glyphicon-plus"></span>',array('class'=>'form-control btn btn-info col-xs-1','onClick'=>'duplicateSunTime()','id'=>'end_time')) !!}
                </td>
              </tr>

              @foreach($primary_clinic_slot_timings as $primaryClinicSlotDetail)
              @if($primaryClinicSlotDetail->clinic_day == 'Sunday')
              @if($primaryClinicSlotDetail->primary_clinic_slot_id == $primaryClinicSlotDetail->staff_slot_id)

              <tr id="addDuplicaterSunday">
                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text(null, $primaryClinicSlotDetail->slot_start_time,array('class'=>'form-control start_time col-xs-1','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text(null, $primaryClinicSlotDetail->slot_end_time,array('class'=>'form-control end_time col-xs-1','style'=>'50%')) !!}
                </td>
                <td class="form-group" style="float: left;">
                  <a href="{{ url('master/clinic/staff_register/primary_clinic_slot_time_details/status_delete',[$primaryClinicSlotDetail->staff_clinic_id,$primaryClinicSlotDetail->p_slot_timing_id])}}">{!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}</a>
                </td>


              </tr>

              @endif

              @endif
              @endforeach


              <tr id="duplicaterSun" style="display:none;">
                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text('null', null,array('class'=>'form-control start_time col-xs-1','id'=>'dupSunStartTime','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::text('null', null,array('class'=>'form-control end_time col-xs-1','id'=>'dupSunEndTime','style'=>'50%')) !!}
                </td>

                <td class="form-group" style="float: left padding:8%;">
                  {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}
                </td>

              </tr>
            </table>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</div>

<div>
 @if($primary_clinic_id != null)
 {!! Form::hidden('primary_clinic_id', $primary_clinic_id) !!}
 @endif

 {!! HTML::link('master/clinic/staff_register/slot_time_details', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
 {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning pull-right','style'=>'margin-right:1%')) !!}

</div>
{!! Form::close() !!}

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

<script>
//Primary Clinic
function inputMonday(){
  $("#originalMonStartTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });

  $("#originalMonEndTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });
}
var i = 0;

function duplicateMonTime() {

  var startTime = document.getElementById('originalMonStartTime').value;
  var endTime = document.getElementById('originalMonEndTime').value;

  if(startTime!='' && endTime!=''){

    var original = document.getElementById('duplicaterMon');
    original.style.display = "block";

    document.getElementById('dupMonStartTime').value =startTime;
    document.getElementById('dupMonEndTime').value =endTime;

    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicaterMon" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);

    original.style.display = "none";

    document.getElementById('originalMonStartTime').value="";
    document.getElementById('originalMonEndTime').value="";

    var dateElement = document.getElementById("duplicaterMon"+i).getElementsByClassName('start_time')[0];
    dateElement.setAttribute("name","time[Monday][]");

    var timeElement = document.getElementById("duplicaterMon"+i).getElementsByClassName('end_time')[0];
    timeElement.setAttribute("name","time[Monday][]");

  }

}
//Primary Clinic

/*alert(tueKey+"Tues");

*/
function inputTuesday(){
  $("#originalTueStartTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });

  $("#originalTueEndTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });
}


var j = 0;

function duplicateTueTime() {

  var startTime = document.getElementById('originalTueStartTime').value;
  var endTime = document.getElementById('originalTueEndTime').value;

  if(startTime!='' && endTime!=''){

    var original = document.getElementById('duplicaterTue');
    original.style.display = "block";

    document.getElementById('dupTueStartTime').value =startTime;
    document.getElementById('dupTueEndTime').value =endTime;

    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicaterTue" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);

    original.style.display = "none";

    document.getElementById('originalTueStartTime').value="";
    document.getElementById('originalTueEndTime').value="";

    var dateElement = document.getElementById("duplicaterTue"+i).getElementsByClassName('start_time')[0];
    dateElement.setAttribute("name","time[Tuesday][]");

    var timeElement = document.getElementById("duplicaterTue"+i).getElementsByClassName('end_time')[0];
    timeElement.setAttribute("name","time[Tuesday][]");

  }

}
//Primary Clinic

function inputWednesday(){

  $("#originalWedStartTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });

  $("#originalWedEndTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });
}




var k = 0;

function duplicateWedTime() {

  var startTime = document.getElementById('originalWedStartTime').value;
  var endTime = document.getElementById('originalWedEndTime').value;

  if(startTime!='' && endTime!=''){

    var original = document.getElementById('duplicaterWed');
    original.style.display = "block";

    document.getElementById('dupWedStartTime').value =startTime;
    document.getElementById('dupWedEndTime').value =endTime;

    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicaterWed" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);

    original.style.display = "none";

    document.getElementById('originalWedStartTime').value="";
    document.getElementById('originalWedEndTime').value="";

    var dateElement = document.getElementById("duplicaterWed"+i).getElementsByClassName('start_time')[0];
    dateElement.setAttribute("name","time[Wednesday][]");

    var timeElement = document.getElementById("duplicaterWed"+i).getElementsByClassName('end_time')[0];
    timeElement.setAttribute("name","time[Wednesday][]");

  }

}
//Primary Clinic
function inputThursday(){
  $("#originalThuStartTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });

  $("#originalThuEndTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });
}



var l = 0;

function duplicateThuTime() {

  var startTime = document.getElementById('originalThuStartTime').value;
  var endTime = document.getElementById('originalThuEndTime').value;

  if(startTime!='' && endTime!=''){

    var original = document.getElementById('duplicaterThu');
    original.style.display = "block";

    document.getElementById('dupThuStartTime').value =startTime;
    document.getElementById('dupThuEndTime').value =endTime;

    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicaterThu" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);

    original.style.display = "none";

    document.getElementById('originalThuStartTime').value="";
    document.getElementById('originalThuEndTime').value="";

    var dateElement = document.getElementById("duplicaterThu"+i).getElementsByClassName('start_time')[0];
    dateElement.setAttribute("name","time[Thursday][]");

    var timeElement = document.getElementById("duplicaterThu"+i).getElementsByClassName('end_time')[0];
    timeElement.setAttribute("name","time[Thursday][]");

  }

}
//Primary CLinic
function inputFriday(){
  $("#originalFriStartTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });

  $("#originalFriEndTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });
}

var m = 0;

function duplicateFriTime() {

  var startTime = document.getElementById('originalFriStartTime').value;
  var endTime = document.getElementById('originalFriEndTime').value;

  if(startTime!='' && endTime!=''){

    var original = document.getElementById('duplicaterFri');
    original.style.display = "block";

    document.getElementById('dupFriStartTime').value =startTime;
    document.getElementById('dupFriEndTime').value =endTime;

    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicaterFri" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);

    original.style.display = "none";

    document.getElementById('originalFriStartTime').value="";
    document.getElementById('originalFriEndTime').value="";

    var dateElement = document.getElementById("duplicaterFri"+i).getElementsByClassName('start_time')[0];
    dateElement.setAttribute("name","time[Friday][]");

    var timeElement = document.getElementById("duplicaterFri"+i).getElementsByClassName('end_time')[0];
    timeElement.setAttribute("name","time[Friday][]");

  }

}
//Primary Clinic
function inputSaturday(){
  $("#originalSatStartTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });

  $("#originalSatEndTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });
}



var n = 0;

function duplicateSatTime() {

  var startTime = document.getElementById('originalSatStartTime').value;
  var endTime = document.getElementById('originalSatEndTime').value;

  if(startTime!='' && endTime!=''){

    var original = document.getElementById('duplicaterSat');
    original.style.display = "block";

    document.getElementById('dupSatStartTime').value =startTime;
    document.getElementById('dupSatEndTime').value =endTime;

    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicaterSat" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);

    original.style.display = "none";

    document.getElementById('originalSatStartTime').value="";
    document.getElementById('originalSatEndTime').value="";

    var dateElement = document.getElementById("duplicaterSat"+i).getElementsByClassName('start_time')[0];
    dateElement.setAttribute("name","time[Saturday][]");

    var timeElement = document.getElementById("duplicaterSat"+i).getElementsByClassName('end_time')[0];
    timeElement.setAttribute("name","time[Saturday][]");

  }

}
//Primary Clinic
function inputSunday(){
  $("#originalSunStartTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });

  $("#originalSunEndTime").timepicker({
    template: false,
    showInputs: false,
    minuteStep: 15
  });
}



var p = 0;

function duplicateSunTime() {

  var startTime = document.getElementById('originalSunStartTime').value;
  var endTime = document.getElementById('originalSunEndTime').value;

  if(startTime!='' && endTime!=''){

    var original = document.getElementById('duplicaterSun');
    original.style.display = "block";

    document.getElementById('dupSunStartTime').value =startTime;
    document.getElementById('dupSunEndTime').value =endTime;

    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicaterSun" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);

    original.style.display = "none";

    document.getElementById('originalSunStartTime').value="";
    document.getElementById('originalSunEndTime').value="";

    var dateElement = document.getElementById("duplicaterSun"+i).getElementsByClassName('start_time')[0];
    dateElement.setAttribute("name","time[Sunday][]");

    var timeElement = document.getElementById("duplicaterSun"+i).getElementsByClassName('end_time')[0];
    timeElement.setAttribute("name","time[Sunday][]");

  }

}

function removeduplicate(element)
{
    element=element.parentNode.parentNode;//gets the id of the parent
    element.parentNode.removeChild(element);
  }

  </script>
  @stop