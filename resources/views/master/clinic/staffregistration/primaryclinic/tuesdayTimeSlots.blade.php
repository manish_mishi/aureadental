@extends('layouts.masterNav')

@section('title')
Clinic->Staff Registration
@stop

@section('side_bar')


<div class="navbar-default sidebar" role="navigation">
  <div class="row-fluid">
    <ul class="nav" id="side-menu">
          <!-- <li class="sidebar-search">
            <div class="input-group custom-search-form">
              <input type="text" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div> -->
            <!-- /input-group -->
            <!--  </li> -->
            <li>
              <a href="{{ url('master/clinic/staffregister/dashboard') }}" class="active">Staff Registration</a>
            </li>
            
            <li>
              <a href="{{ url('master/clinic/leavemanagement/dashboard') }}">Leave Management</a>
            </li>
            
            <li>
              <a href="{{ url('master/clinic/clinic/dashboard') }}">Clinic</a>
            </li>
            

            
          </ul>
        </div>
        <!-- /.sidebar-collapse -->
      </div>

      @stop

      @section('main')


      <div id="wrapper">

       <div id="page-wrapper">
         <div class="panel panel-info">
          <ul style="width:100%" class="nav nav-pills">
            <li style="width:25%" role="presentation"><a href="{{ url('master/clinic/staffregister/add') }}">General</a></li>
            <li style="width:25%" role="presentation"><a href="{{ url('master/clinic/staffregister/qualifications') }}">Qualifications</a></li>
            <li style="width:25%" role="presentation" class="active"><a href="{{ url('master/clinic/staffregister/slottimedetails') }}">Slot time details</a></li>
            <li style="width:24%" role="presentation"><a href="{{ url('master/clinic/staffregister/specialization') }}">Specialization</a></li>
          </ul>
        </div>

        <style>
        td {

          padding: 7px;

        }
        </style>
     {!! Form::open(array('route' => 'addstaffregistrationslotdetails','class' => 'form')) !!}



  <div class="panel panel-default filterable table-responsive">
                      <table class="table table-hover">
     <tr>
          <td style="padding:5%">Tuesday
            <input type="hidden" name="day[]" value="Tuesday"></td>
            <td>
              <div class="panel panel-default filterable table-responsive">
                <table class="table table-hover">
                  <tr>
                    <td class="form-group" style="float: left padding:5%;">
                      {!! Form::label('From', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                      {!! Form::text('time[Tuesday][]', null,array('class'=>'form-control start_time col-xs-1','id'=>'originalTueStartTime','style'=>'50%','oninput'=>'inputTuesday()')) !!}
                    </td>

                    <td class="form-group" style="float: left;">
                      {!! Form::label('To', null, array('class'=>'col-xs-4 control-label','style'=>'50%')) !!}
                      {!! Form::text('time[Tuesday][]', null,array('class'=>'form-control end_time col-xs-1','id'=>'originalTueEndTime','style'=>'50%')) !!}
                    </td>

                    <td class="form-group" style="float: left padding:5%;">
                      {!! Form::button('<span class="glyphicon glyphicon-plus"></span>',array('class'=>'form-control btn btn-info col-xs-1','onClick'=>'duplicateTueTime()','id'=>'end_time')) !!}
                    </td>
                  </tr>

                  <tr id="duplicaterTue" style="visibility:hidden;">
                    <td class="form-group" style="float: left,padding:8%;">
                      {!! Form::text('null', null,array('class'=>'form-control start_time ','id'=>'dupTueStartTime','style'=>'50%')) !!}
                    </td>

                    <td class="form-group" style="float: left,padding:8%;">
                      {!! Form::text('null', null,array('class'=>'form-control end_time ','id'=>'dupTueEndTime','style'=>'50%')) !!}
                    </td>

                    <td class="form-group" style="float: left,padding:8%;">
                      {!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}
                    </td>

                  </tr>
                </table>
              </div>
            </td>
</td>
</tr>
</table>
</div>
<div>
       @if($staffreg_ID != null)
              {!! Form::hidden('staff_reg_id', $staffreg_ID) !!}
              @endif

         {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning pull-right','style'=>'margin-right:1%')) !!}

</div>
  {!! Form::close() !!}

  //Primary Clinic
<script>
/*alert(tueKey+"Tues");

*/
function inputTuesday(){
                      $("#originalTueStartTime").timepicker({
                        template: false,
                        showInputs: false,
                        minuteStep: 15
                      });

                      $("#originalTueEndTime").timepicker({
                        template: false,
                        showInputs: false,
                        minuteStep: 15
                      });
                    }


var j = 0;

function duplicateTueTime() {

  var startTime = document.getElementById('originalTueStartTime').value;
  var endTime = document.getElementById('originalTueEndTime').value;

  if(startTime!='' && endTime!=''){

    var original = document.getElementById('duplicaterTue');
    original.style.visibility = "visible";

    document.getElementById('dupTueStartTime').value =startTime;
    document.getElementById('dupTueEndTime').value =endTime;

    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicaterTue" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);

    original.style.visibility = "hidden";

    document.getElementById('originalTueStartTime').value="";
    document.getElementById('originalTueEndTime').value="";

    var dateElement = document.getElementById("duplicaterTue"+i).getElementsByClassName('start_time')[0];
    dateElement.setAttribute("name","time[Tuesday][]");

    var timeElement = document.getElementById("duplicaterTue"+i).getElementsByClassName('end_time')[0];
    timeElement.setAttribute("name","time[Tuesday][]");

  }

}
</script>
@stop