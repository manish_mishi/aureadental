@extends('layouts.masterNav')

@section('title')
Clinic->Dashboard
@stop

@section('side_bar')

<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/clinic/staff_register/listview') }}">Staff Registration</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/leave_management/listview') }}" class="active">Leave Management</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/clinic/listview') }}">Clinic</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
 <div id="page-wrapper">
  
  <div class="row">
{!! Form::open(array('route' => 'updateleavemanagement','class' => 'form')) !!}
    <div class="form-group">
       <br>
      {!! Form::label('Staff Type', null, array('class'=>'col-xs-2  control-label')) !!}
      <div class="col-xs-3">
        <select class="form-control" name="staff_type" id ='staffType' value="{{ old('name') }}">
          <option value=>Select</option>
          <option value="5">clinig</option>
          <option value="6">maintenance</option>
          <option value="7">managment</option>
        </select>
      </div>
 

     <div class="form-group">
      {!! Form::label('Staff Name', null, array('class'=>'col-xs-2  control-label')) !!}
      <div class="col-xs-3">
        <select class="form-control" name="staff_name" id ='staffName' value="{{ old('name') }}" >
          <option value>Select</option>
          <option value>jon</option>
          <option value>dev</option>
          <option value>rocky</option>
        </select>
      </div>
    </div>
  </div>
 </div>
  <br>
  <div>
    <div class="col-xs-2"><br><br><br>
      <label class="control-label">Full Day Leave</label>
      <br><br><br>
      <label class="control-label">Partial Leave</label>
    </div>
    <div class="col-xs-10 panel panel-default">
      <table class="table">
        <thead>
          <tr>
            <th class="col-xs-3">
              <label class="control-label">Leave ID</label>
            </th>
            <th class="col-xs-3">
              <input type="text" class="form-control" name="leave_id" value="{{ old('name') }}">
            </th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>FROM</td>
            @if(array_key_exists('date_from', $leave_full))
            <td>{!! form::text('from_full_date',$leave_full->date_from,array('class'=>'form-control',))!!}</td>
            <td>TO</td>
            <td>{!! form::text('to_full_date',$leave_full->date_to,array('class'=>'form-control'))!!}</td>
            @else
             <td>{!! form::text('from_full_date',null,array('class'=>'form-control'))!!}</td>
            <td>TO</td>
            <td>{!! form::text('to_full_date',null,array('class'=>'form-control'))!!}</td>
            @endif
          </tr>

          <tr>
            <td>Slot</td>
            <td class='hide-radio rad' role='group' data-toggle='buttons'><label class='btn btn-default'><input type="radio" class="form-control" name="leave_id" value="{{ old('name') }}">Radio Button</label></td>
            <td></td>
            <td>Date</td>
            @if(array_key_exists('date', $leave_Partial))
            <td>{!! form::text('date',$leave_Partial->date,array('class'=>'form-control'))!!}</td>
            @else
            <td>{!! form::text('date',null,array('class'=>'form-control'))!!}</td>
            @endif
          </tr>

          <tr>
            <td></td>
            <td colspan="9" class="panel panel-default">
              <table class="table">
                <thead>
                  <tr>
                    <th class="col-xs-3">
                      <label class="control-label">Timing</label>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>FROM</td>
                     @if(array_key_exists('time_from', $leave_Partial))
                    <td>{!! form::text('from_time',$leave_Partial->time_from,array('class'=>'form-control','required'))!!}</td>
                      @else
                     <td><input type="text" class="form-control" name="from_time" value="{{ old('name') }}"></td>
                      @endif
                    <td>TO</td>
                    @if(array_key_exists('time_to', $leave_Partial))
                    <td>{!! form::text('to_time',$leave_Partial->time_to,array('class'=>'form-control','required'))!!}</td>
                    @else
                     <td><input type="text" class="form-control" name="to_time" value="{{ old('name') }}"></td>
                    @endif
                  </tr>

                </tbody>
              </table>
            </td>
          </tr>

          <tr>
            <td colspan="10">{!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning pull-right')) !!}</td>
            {!! Form::hidden('staff_id', $staff_id) !!}
      
          </tr>
          {!! Form::close() !!}
        </tbody>

      </table>
    </div>
  </div>
  <tbody>
    <tr>
      <td>  </td>
      <td>  </td>
      <td> </td>
      <td>  </td>
      <td>  </td>
      <td></td>

    </tr>
  </tbody>
</table>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div> 
</div>

<!-- /#wrapper -->

<script type="text/javascript">

$(".start_time").timepicker({
  template: false,
  showInputs: false,
  minuteStep: 15
});

$(".end_time").timepicker({
  template: false,
  showInputs: false,
  minuteStep: 15
});

</script>

@stop
