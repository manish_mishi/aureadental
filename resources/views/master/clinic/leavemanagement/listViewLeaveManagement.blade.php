@extends('layouts.masterNav')

@section('title')
Clinic->Dashboard
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/clinic/staff_register/listview') }}">Staff Registration</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/leave_management/listview') }}" class="active">Leave Management</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/clinic/listview') }}">Clinic</a>
      </li>        
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">

 <div id="page-wrapper">

  <!-- successfullye added msg -->
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->    

  <br>  
  <div class="col-xs-4">
    <label class="control-label col-xs-5">Staff Type</label>
    <div class="col-xs-7">
      <input type="text" class="form-control" name="staff_type" value="{{ old('name') }}">
    </div>
  </div>

  <div class="col-xs-4">
    <label class="control-label col-xs-5">Staff Name</label>
    <div class="col-xs-7">
      <input type="text" class="form-control" name="staff_name" value="{{ old('name') }}">
    </div>
  </div>

  <a href="{{ url('#') }}" class="btn btn-default col-xs-1">Search</a>

  <br><br>
  <div>
    <!-- Add new material button -->
    <a href="{{ url('master/clinic/leave_management/add') }}" class="btn btn-success pull-right col-xs-2" ><i class="fa fa-plus fa-fw"></i>Add Leave</a>
  </div>


  <br>
  <div class="panel panel-default filterable" style="margin-top:3%">
   <table class="table">
    <thead>
      <tr><th class="panel panel-default">Staff Info</th></tr>
      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Dates" disabled></th>
        <th><input type="text" class="form-control" placeholder="Timings" disabled></th>
        <th>Action</th>
        <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
      </tr>
    </thead>
    <tbody>

       @foreach($leaves as $leave)
  
      <tr>
      
        <td>{{$leave->name}}</td>
        @if(array_key_exists('date_from', $leave))
        <td>{{$leave->date_from}}-{{$leave->date_to}}</td>
        @elseif(array_key_exists('date', $leave))
        <td>{{$leave->date}}</td>
        @else
        <td></td>
         @endif
      
          @if(array_key_exists('time_from', $leave))
        <td>{{$leave->time_from}}-{{$leave->time_to}}</td>
        @else
        <td></td>
         @endif
    
        <td>
         @if(array_key_exists('full_day_id', $leave))
        <td>
            <a href="{{ url('master/clinic/leave_management/full_day/edit',[$leave->full_day_id]) }}" class="btn btn-xs btn-primary"><i class="fa fa-edit fa-fw"></i>Edit</a>
             {!! Form::hidden('day_id', $leave->full_day_id,array('id'=>'day_id')) !!}
        </td>
         <td>
         <a class="btn btn-danger btn btn-xs btn-primary" onClick="changestatus({{$leave->full_day_id}},'full')">
         <i class="fa fa-trash-o fa-fw"></i> Delete</a>
        </td>

        @else
        <td>
            <a href="{{ url('master/clinic/leave_management/partial_leave/edit',[$leave->half_day_id]) }}" class="btn btn-xs btn-primary"><i class="fa fa-edit fa-fw"></i>Edit</a>
            {!! Form::hidden('day_id', $leave->half_day_id,array('id'=>'day_id')) !!}
        </td>
         <td>
         <a class="btn btn-danger btn btn-xs btn-primary" onClick="changestatus({{$leave->half_day_id}},'half')">
         <i class="fa fa-trash-o fa-fw"></i> Delete</a>
        </td>
         @endif
         
          
        </td>
       

      </tr>
      @endforeach
    </tbody>
  </table>

</div>

<script>
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});


function changestatus(day_id, day)
{
/*$full_day_id = $('#day_id').val();*/
if (day == "full"){

swal({
  title: "Are you sure?",
  text: "you want to cancel Leave ?",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},

 function(){ 

$.ajax({
method:'POST',
url: '{{url("master/clinic/leave_management/full_day/destroy")}}'+"/"+day_id,
success : function(data){
window.location.replace('listview');
}
});

});
}
else
{

swal({
  title: "Are you sure?",
  text: "you want to cancel Leave ?",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
},

 function(){ 

$.ajax({
method:'POST',
url: '{{url("master/clinic/leave_management/half_day/destroy")}}'+"/"+day_id,
success : function(data){
window.location.replace('listview');
}
});

});
}
}
</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
