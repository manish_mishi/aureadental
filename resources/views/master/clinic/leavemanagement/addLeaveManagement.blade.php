@extends('layouts.masterNav')

@section('title')
Clinic->Dashboard
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/clinic/staff_register/listview') }}">Staff Registration</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/leave_management/listview') }}" class="active">Leave Management</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/clinic/listview') }}">Clinic</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">

 <div id="page-wrapper">
  <div class="row">
{!! Form::open(array('route' => 'addleavemanagement','class' => 'form')) !!}
    <div class="form-group">
       <br>
      {!! Form::label('Staff Type', null, array('class'=>'col-xs-2  control-label')) !!}
      <div class="col-xs-3">
        <select class="form-control" name="staff_type" id ='staffType' required>
          <option value>Select</option>
          <option value="1">clinic</option>
          <option value="2">maintenance</option>
          <option value="3">management</option>
          <option value="4">stock</option>
        </select>
      </div>

     <div class="form-group">
      {!! Form::label('Staff Name', null, array('class'=>'col-xs-2  control-label')) !!}
      <div class="col-xs-3">
        <select class="form-control" name="staff_name" id ='staffName' value="{{ old('name') }}" >
          <option value>Select</option>
          <option value>jon</option>
          <option value>dev</option>
          <option value>rocky</option>
        </select>
      </div>
    </div>
  </div>
</div>
  <br>
  <div>
    <div class="col-xs-2"><br><br><br>
      <label class="control-label">Full Day Leave</label>
      <br><br><br>
      <label class="control-label">Partial Leave</label>
    </div>
    <div class="col-xs-10 panel panel-default">
      <table class="table">
        <thead>
          <tr>
            <th class="col-xs-3">
              <label class="control-label">Leave ID</label>
            </th>
            <th class="col-xs-3">
              <input type="text" class="form-control" name="leave_id" value="{{ old('name') }}">
            </th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>FROM</td>
            <td><input  data-provide="datepicker" data-date-format="dd/mm/yy" id="from_full_date" class="form-control" name="from_full_date"/></td>
            <td>TO</td>
            <td><input  data-provide="datepicker" data-date-format="dd/mm/yy" id="to_full_date" class="form-control" name="to_full_date"/></td>
          </tr>

          <tr>
            <td>Slot</td>
            <td class='hide-radio rad' role='group' data-toggle='buttons'><label class='btn btn-default'><input type="radio" class="form-control" name="leave_id" value="{{ old('name') }}">Radio Button</label></td>
            <td></td>
            <td>Date</td>
            <td><input  data-provide="datepicker" data-date-format="dd/mm/yy" id="date" class="form-control" name="date"/></td>
          </tr>

          <tr>
            <td></td>
            <td colspan="9" class="panel panel-default">
              <table class="table">
                <thead>
                  <tr>
                    <th class="col-xs-3">
                      <label class="control-label">Timing</label>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>FROM</td>
                    <td><input type="text" class="form-control start_time" name="from_time" value="{{ old('name') }}"></td>
                    <td>TO</td>
                    <td><input type="text" class="form-control end_time" name="to_time" value="{{ old('name') }}"></td>
                  </tr>

                </tbody>
              </table>
            </td>
          </tr>

          <tr>
            <td colspan="10">{!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning pull-right')) !!}</td>
          </tr>
          {!! Form::close() !!}
        </tbody>

      </table>
    </div>
  </div>
  <tbody>
    <tr>
      <td>  </td>
      <td>  </td>
      <td> </td>
      <td>  </td>
      <td>  </td>
      <td></td>

    </tr>
  </tbody>
</table>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div> 
</div>

<!-- /#wrapper -->

<script type="text/javascript">

$(".start_time").timepicker({
  template: false,
  showInputs: false,
  minuteStep: 15
});

$(".end_time").timepicker({
  template: false,
  showInputs: false,
  minuteStep: 15
});

</script>

@stop
