@extends('layouts.masterNav')

@section('title')
Clinic -> Details
@stop

@section('side_bar')

<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-3">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>

<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse" role="navigation" id="bs-sidebar-navbar-collapse-3">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/clinic/staff_register/listview') }}">Staff Registration</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/leave_management/listview') }}">Leave Management</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/clinic/listview') }}" class="active">Clinic</a>
      </li>



    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
 <div id="page-wrapper">

  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:49%" role="presentation" class="active"><a href="{{ url('master/clinic/clinic/details') }}">Details</a></li>
      <li style="width:50%" role="presentation"><a href="{{ url('master/clinic/clinic/work_timings') }}">Work Timings</a></li>
    </ul>
  </div>

  <!-- successfullye added msg -->
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->

  <div class="row">
   {!! Form::open(array('route' => 'addClinicDetails','class' => 'form')) !!}
   <div class="form-group">
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      {!! Form::label('Clinic Name', null, array('class'=>'control-label')) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      @if(Session::has('name'))
      <?php $name = Session::get('name') ?>
      {!! Form::text('name', $name,array('class'=>'form-control')) !!}
      @else
      {!! Form::text('name', null,array('class'=>'form-control','required')) !!}
      @endif
    </div>
  </div>

  <br><br><br>
  <div class="form-group">
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      {!! Form::label('Location', null, array('class'=>'control-label')) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      @if(Session::has('location'))
      <?php $location = Session::get('location') ?>
      {!! Form::text('location', $location,array('class'=>'form-control')) !!}
      @else
      {!! Form::text('location', null,array('class'=>'form-control')) !!}
      @endif
    </div>
  </div>
  <br><br><br>
  <div class="form-group">
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      {!! Form::label('Address', null, array('class'=>'control-label')) !!}
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      @if(Session::has('location'))
      <?php $address = Session::get('address') ?>
      {!! Form::textarea('address',$address,array('class'=>'form-control','rows' => 2, 'cols' => 40))!!}
      @else
      {!! Form::textarea('address',null,array('class'=>'form-control','rows' => 2, 'cols' => 40))!!}
      @endif
    </div>
  </div>

  <br><br><br>
  <div class="form-group">

    {!! Form::hidden('clinic_id', $clinicID->id+1) !!}
    {!! HTML::link('master/clinic/clinic/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
    {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default pull-right','style'=>'margin-right:1%')) !!}
  </div>

  {!! Form::close() !!}
</div>  

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
