@extends('layouts.masterNav')

@section('title')
Clinic-> List View
@stop

@section('side_bar')

<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-3">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>

<div class="sidebar">
  <div class="sidebar-nav navbar-collapse" role="navigation" id="bs-sidebar-navbar-collapse-3">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/clinic/staff_register/listview') }}">Staff Registration</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/leave_management/listview') }}">Leave Management</a>
      </li>

      <li>
        <a href="{{ url('master/clinic/clinic/listview') }}" class="active">Clinic</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">

 <div id="page-wrapper">

  <!-- successfullye added msg -->
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->    

<br>
  <div>
    <!-- Add new material button -->
    <a href="{{ url('master/clinic/clinic/details') }}"><button class="btn btn-success pull-right"><i class="fa fa-plus fa-fw"></i>New Clinic</button></a>
  </div>

  <br>
  <div class="panel panel-info filterable table-responsive" style="margin-top:3%">

   <!-- Table -->
   <table class="table table-hover"> 
    <thead class="panel-info">
      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="Clinic name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Location" disabled></th>
        <th><input type="text" class="form-control" placeholder="Address" disabled></th>
        <th><input type="text" class="form-control" placeholder="Clinic Day" disabled></th>
        <th>Action</th>
        <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
      </tr>
    </thead>
    <tbody>
      @foreach($clinic_details as $clinic_detail)

      <tr>
        <td> {{$clinic_detail->name}} </td>
        <td> {{$clinic_detail->location}} </td>
        <td> {{$clinic_detail->address}}</td>
        <td> 
          <table class="table table-hover">
            @foreach($clinic_slot_details as $clinic_slot_detail)
            @if($clinic_detail->id == $clinic_slot_detail->clinic_id)
            <tr><td>{{$clinic_slot_detail->clinic_day}} </td></tr>
            @endif
            @endforeach
          </table>
        </td>
        <td>
          <a href="{{ url('master/clinic/clinic/details/edit',[$clinic_detail->id]) }}" class="btn btn-xs btn-primary"><i class="fa fa-edit fa-fw"></i>Edit</a>
        </td>
        <td>
          {!! Form::open(array('class' => 'form')) !!}
          {!! Form::hidden('id', $clinic_detail->id) !!}
          {!! Form::button('<i class="fa fa-trash fa-fw"></i>Delete',array('type' => 'submit','class'=>'btn btn-xs btn-danger')) !!}
          {!! Form::close() !!}
        </td>

      </tr>
      @endforeach
    </tbody>
  </table>

</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
