@extends('layouts.masterNav')

@section('title')
        Patient->Attachments
@stop

@section('side_bar')


  <div class="navbar-default sidebar" role="navigation">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <!-- <li class="sidebar-search">
            <div class="input-group custom-search-form">
              <input type="text" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div> -->
            <!-- /input-group -->
         <!--  </li> -->
          <li>
            <a href="{{ url('master/patient') }}" class="active">Patient Registration</a>
          </li>
          
          
          
          

          
        </ul>
      </div>
      <!-- /.sidebar-collapse -->
    </div>

@stop

@section('main')


<div id="wrapper">

     <div id="page-wrapper">
        <div class="panel panel-info">
            <ul style="width:100%" class="nav nav-pills">
              <li style="width:33%" role="presentation"><a href="{{ url('master/patient/general') }}">General</a></li>
              <li style="width:33%" role="presentation"><a href="{{ url('master/patient/medicalhistory') }}">Medical History</a></li>
              <li style="width:33%" role="presentation" class="active"><a href="{{ url('master/patient/attachments') }}">Attachments</a></li>
          </ul>
      </div>

      <div class="row">
      	<form name='general'>
                <div class="col-xs-6">
                    <div class="form-group">
                            
                        </div>

                        <br><br><br>
                        <div class="form-group">
                           
                        </div>

                        <br><br><br>
                        <div class="form-group">
                           
                        </div>

                        <br><br><br>
                        <div class="form-group">
                           
                        </div>
                </div>

                <div class="col-xs-6">
                    <div class="form-group">
                           
                        </div>

                        <br><br><br>
                        <div class="form-group">
                           
                        </div>

                        <br><br><br>
                        <div class="form-group">
                           
                        </div>

                        <br><br><br>
                        <div class="form-group">
                            
                        </div>

                        <br><br><br>
                        <div>
                            <button type="submit" class="btn btn-warning">Save</button>
                        	<a href = "" class = "btn btn-info">New Appointment</a>
                        </div>

                </div>
             </form>
      </div>
      
  </div> <!-- /#page-wrapper -->

</div> <!-- /#wrapper -->

@stop