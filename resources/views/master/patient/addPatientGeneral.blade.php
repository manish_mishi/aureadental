@extends('layouts.masterNav')

@section('title')
Patient
@stop

@section('side_bar')


<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/patient') }}" class="active">Patient Registration</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')


<div id="wrapper">

 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:50%" role="presentation" class="active"><a href="{{ url('master/patient/general') }}">General</a></li>
      <li style="width:49%" role="presentation"><a href="{{ url('master/patient/medicalhistory') }}">Medical History</a></li>
    </ul>
  </div>

  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->

  <div class="row">
   {!! Form::open(array('route' => 'addpatientgeneral','class' => 'form')) !!}

   <div class="col-xs-6">

    <div class="form-group">
      {!! Form::label('Patient Name', null, array('class'=>'col-xs-4  control-label')) !!}
      <div class="col-md-6">
        @if(Session::has('patient_name'))
        <?php $name = Session::get('patient_name') ?>
        {!! Form::text('pat_name', $name,array('class'=>'form-control')) !!}
        @else
        {!! Form::text('pat_name', null,array('class'=>'form-control','required')) !!}
        @endif

      </div>
    </div>

    <br><br><br>
    <div class="form-group">
      {!! Form::label('Cell No', null, array('class'=>'col-xs-4 control-label')) !!}
      <div class="col-xs-6">
        @if(Session::has('patient_cell_no'))
        <?php $cell_no = Session::get('patient_cell_no') ?>
        {!! Form::input('number','pat_cell',$cell_no,array('class'=>'form-control')) !!}
        @else
        {!! Form::input('number','pat_cell', null,array('class'=>'form-control')) !!}
        @endif
        
      </div>
    </div>

    <br><br><br>
    <div class="form-group">
      {!! Form::label('Gender', null, array('class'=>'col-xs-5 control-label')) !!} 
      @if(Session::has('patient_gender'))
      <?php $gender = Session::get('patient_gender') ?>
      @if($gender=='male')
      {!! Form::radio('gender','male',true) !!}Male
      {!! Form::radio('gender','female') !!}Female
      @else
      {!! Form::radio('gender','male') !!}Male
      {!! Form::radio('gender','female',true) !!}Female
      @endif
      @else
      {!! Form::radio('gender','male',true) !!}Male
      {!! Form::radio('gender','female') !!}Female
      @endif            
      
    </div>

    <br>
    <div class="form-group">
      {!! Form::label('DoB', null, array('class'=>'col-xs-4 control-label')) !!}
      <div id="date1" class="col-xs-6">
        @if(Session::has('patient_dob'))
        <?php $dob = Session::get('patient_dob') ?>
        <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dob" class="form-control" name="dob" value="<?php echo $dob;?>">
        @else
        <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dob" class="form-control" name="dob" value="<?php echo date("d/m/Y");?>">
        @endif
        <!-- <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dob" class="form-control" name="dob" value="<?php echo date("d/m/Y");?>"> -->
      </div>
    </div>
  </div>

  <div class="col-xs-6">
    <div class="form-group">
      {!! Form::label('Email', null, array('class'=>'col-xs-4 control-label')) !!}
      <div class="col-md-6">
        @if(Session::has('patient_email'))
        <?php $email = Session::get('patient_email') ?>
        {!! Form::email('mat_email', $email,array('class'=>'form-control')) !!}
        @else
        {!! Form::email('mat_email', null,array('class'=>'form-control')) !!}
        @endif
        <!--  -->
      </div>
    </div>

    <br><br><br>
    <div class="form-group">
      {!! Form::label('Occupation', null, array('class'=>'col-xs-4 control-label')) !!}
      <div class="col-md-6">
        @if(Session::has('patient_occupation'))
        <?php $occupation = Session::get('patient_occupation') ?>
        {!! Form::text('mat_occ', $occupation,array('class'=>'form-control','id'=>'occup')) !!}
        @else
        {!! Form::text('mat_occ', null,array('class'=>'form-control','id'=>'occup')) !!}
        @endif

        <!--  -->
      </div>
    </div>

    <br><br><br>
    <div class="form-group">
      {!! Form::label('Address', null, array('class'=>'col-xs-4 control-label')) !!}
      <div class="col-md-6">
        @if(Session::has('patient_address'))
        <?php $address = Session::get('patient_address') ?>
        {!! Form::textarea('address', $address,array('size' => '30x2','class'=>'form-control')) !!}
        @else
        {!! Form::textarea('address', null,array('size' => '30x2','class'=>'form-control')) !!}
        @endif
        <!--  -->
      </div>
    </div>

    <br><br><br>

    <!-- if for reffered pat name not in Other -->
    <?php 
    $referred_patient_id = Session::get('referred_patient_id');
    $other_reff=Session::get('other_reff');
    /*dd($other_reff);*/
    ?>
    @if( $referred_patient_id != 'Other')
    @if(Session::has('referred_patient_name'))
    <?php $referred_patient_name = Session::get('referred_patient_name');
    ?>
    <div class="form-group">
      {!! Form::label('Referred Patient Name', null, array('class'=>'col-xs-4 control-label')) !!}
      <div class="col-md-6">
        <select class="form-control" name="referred_patient_id" id ='patientId'>
          <option value="{{$referred_patient_id}},{{$referred_patient_name}}">{{$referred_patient_name}}</option>
          @foreach($patientNames as $patientName)
          <option value="{{$patientName->id}},{{$patientName->name}}">{{$patientName->name}}</option>
          @endForeach
          <option value="Other">Other</option>
        </select>
      </div>
    </div>
    @else
    <div class="form-group">
      {!! Form::label('Referred Patient Name', null, array('class'=>'col-xs-4 control-label')) !!}
      <div class="col-md-6">
        <select class="form-control" name="referred_patient_id" id ='patientId'>
          <option>Select</option>
          @foreach($patientNames as $patientName)
          <option value="{{$patientName->id}},{{$patientName->name}}">{{$patientName->name}}</option>
          @endForeach
          <option value="Other">Other</option>
        </select>
      </div>
    </div>  
    @endif
    @else
    <div class="form-group">
      {!! Form::label('Referred Patient Name', null, array('class'=>'col-xs-4 control-label')) !!}
      <div class="col-md-6">
        <select class="form-control" name="referred_patient_id" id ='patientId'>
          <option value="Other">Other</option>
          @foreach($patientNames as $patientName)
          <option value="{{$patientName->id}},{{$patientName->name}}">{{$patientName->name}}</option>
          @endForeach
        </select>
      </div>
      
    </div><br><br>
    <div class="form-group">

      {!! Form::label('Other Referal', null, array('class'=>'col-xs-4 control-label')) !!}
      <div class="col-md-6">
        {!! Form::text('other_referrals',$other_reff,array('class'=>'form-control','id'=>'other')) !!}
      </div>
    </div>
    
    @endif
    
    <span  style="display:none" id="otherRef"> 
      <br><br><br>
      <div class="form-group">
       {!! Form::label('Other Referal', null, array('class'=>'col-xs-4 control-label')) !!}
       <div class="col-md-6">
         {!! Form::text('other_referrals', null,array('class'=>'form-control','id'=>'other')) !!}
       </div>
     </div>
   </span>
   
   
   <br><br><br><br>
   <div>
    {!! Form::hidden('patient_id', $patientID->id+1) !!}
    {!! Form::button('&#x2714; Next',array('type' => 'submit','class'=>'btn btn-warning')) !!}
    {!! HTML::link('master/patient', '&#10006; Cancel', array('id' => 'linkid','class'=>'btn btn-danger')) !!}
    {!! Form::button('<i class="fa fa-plus fa-fw"></i>New Appointment',array('class'=>'btn btn-info')) !!}
  </div>

</div>
{!! Form::close() !!}
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

<script type="text/javascript">
$('#patName').focus();

$('#patientId').change(function(){

  var patName=$(this).val();
  if(patName == 'Other')
  {
    document.getElementById('otherRef').style.display="block";
  }
  else
  {
    document.getElementById('otherRef').style.display="none";
  }
});
</script>

@stop
