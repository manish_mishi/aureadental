@extends('layouts.masterNav')

@section('title')
Patient
@stop

@section('side_bar')


<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

            <li>
              <a href="{{ url('master/patient') }}" class="active">Patient Registration</a>
            </li>

          </ul>
        </div>
        <!-- /.sidebar-collapse -->
      </div>

      @stop

      @section('main')
      <div id="wrapper">

       <div id="page-wrapper">
        <div class="panel panel-info">
          <ul style="width:100%" class="nav nav-pills">
            <li style="width:50%" role="presentation" class="active"><a href="{{ url('editPatient',[$patient_id])}}">General</a></li>
            <li style="width:49%" role="presentation"><a href="{{ url('editPatientMedicalHistory',[$patient_id])}}">Medical History</a></li>
          </ul>
        </div>

        <div class="row">
         {!! Form::open(array('route' => 'editPatientGeneral','class' => 'form','method' => 'post')) !!}

        <div class="col-xs-6">

          <div class="form-group">
            {!! Form::label('Patient Name', null, array('class'=>'col-xs-4  control-label')) !!}
            <div class="col-md-6">
              @if(Session::has('patient_name'))
              <?php $pat_name = Session::get('patient_name') ?>
              {!! Form::text('pat_name', $pat_name,array('class'=>'form-control')) !!}
              @else
              {!! Form::text('pat_name', $patient_detials['0']->name,array('class'=>'form-control','required')) !!}
              @endif
            </div>
         </div>

          <br><br><br>
          <div class="form-group">

            {!! Form::label('Cell No', null, array('class'=>'col-xs-4 control-label')) !!}
            <div class="col-xs-6">
              @if(Session::has('patient_cell_no'))
              <?php $patient_cell= Session::get('patient_cell_no')?>
              {!! Form::input('number','pat_cell', $patient_cell,array('class'=>'form-control')) !!}
              @else
              {!! Form::input('number','pat_cell', $patient_detials['0']->cell_no,array('class'=>'form-control')) !!}
              @endif
            </div>
          </div>

          <br><br><br>
          <div class="form-group">
            {!! Form::label('Gender', null, array('class'=>'col-xs-5 control-label')) !!}   
            @if(Session::has('patient_gender'))
              <?php $patient_gender= Session::get('patient_gender')?>
                @if($patient_gender == 'male')
                  {!! Form::radio('gender','male',true) !!}Male
                  {!! Form::radio('gender','female') !!}Female
                @else
                  {!! Form::radio('gender','male') !!}Male
                  {!! Form::radio('gender','female',true) !!}Female
                @endif

            @else
              {!! Form::radio('gender','male',true) !!}Male
              {!! Form::radio('gender','female') !!}Female
            @endif          
            
          </div>

          <br>
          <div class="form-group">
            {!! Form::label('DoB', null, array('class'=>'col-xs-4 control-label')) !!}
            <div id="date1" class="col-xs-6">
              @if(Session::has('patient_dob'))
              <?php $patient_dob= Session::get('patient_dob')?>
              <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dob" class="form-control" name="dob" value="<?php echo $patient_dob;?>">
              @else
              <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dob" class="form-control" name="dob" value="<?php echo $patient_detials['0']->dob;?>">
              @endif
            </div>
          </div>
        </div>

        <div class="col-xs-6">
          <div class="form-group">
            <label class="col-md-4 control-label">Email</label>
            <div class="col-md-6">
              @if(Session::has('patient_email'))
              <?php $patient_mail= Session::get('patient_email')?>
              {!! Form::email('mat_email', $patient_mail, array('class'=>'form-control','required')) !!}
              @else
              {!! Form::email('mat_email', $patient_detials['0']->email, array('class'=>'form-control','required')) !!}
              @endif
            </div>
          </div>

          <br><br><br>
          <div class="form-group">
            <label class="col-md-4 control-label">Occupation</label>
            <div class="col-md-6">
              @if(Session::has('patient_occupation'))
              <?php $patient_occ= Session::get('patient_occupation')?>
              {!! Form::text('mat_occ', $patient_occ, array('class'=>'form-control')) !!}
              @else
              {!! Form::text('mat_occ', $patient_detials['0']->occupation, array('class'=>'form-control')) !!}
              @endif
            </div>
          </div>

          <br><br><br>
          <div class="form-group">
            <label class="col-md-4 control-label">Address</label>
            <div class="col-md-6">
              @if(Session::has('patient_address'))
              <?php $patient_address= Session::get('patient_address')?>
              {!! Form::textarea('address', $patient_address, array('size' => '30x5','class'=>'form-control','required')) !!}
              @else
              {!! Form::textarea('address', $patient_detials['0']->address, array('size' => '30x5','class'=>'form-control','required')) !!}
              @endif
            </div><br><br>
          </div>
          <br><br><br><br>
          
           <?php 
              $other_reff=Session::get('other_reff');
            ?>


          @if($other_reff_pat_name == null && $other_reff == null)
          <div class="form-group">
            {!! Form::label('Referred Patient Name', null, array('class'=>'col-xs-4 control-label')) !!}
            <div class="col-md-6">
              @if(Session::has('referred_patient_name'))
              <?php 
                $referred_patient_name=Session::get('referred_patient_name');
                $referred_patient_id=Session::get('referred_patient_id');
              ?>
              <select class="form-control" name="referred_patient_id" id ='patientId'>
                <option value="{{$referred_patient_id}},{{$referred_patient_name}}">{{$referred_patient_name}}</option>
                @foreach($patientNames as $patientName)
                <option value="{{$patientName->id}},{{$patientName->name}}">{{$patientName->name}}</option>
                @endForeach
                <option value="Other">Other</option>
              </select>
              @else
              <select class="form-control" name="referred_patient_id" id ='patientId'>
                <option value="{{$reffered_patient_name['0']->referred_patient_id}},{{$reffered_patient_name['0']->name}}">{{$reffered_patient_name['0']->name}}</option>
                @foreach($patientNames as $patientName)
                <option value="{{$patientName->id}},{{$patientName->name}}">{{$patientName->name}}</option>
                @endForeach
                <option value="Other">Other</option>
              </select>
              @endif
          </div>
          </div>
          @else
          @if(Session::has('other_reff'))
                       <div class="form-group">
            {!! Form::label('Referred Patient Name', null, array('class'=>'col-xs-4 control-label')) !!}
            <div class="col-md-6">
            <select class="form-control" name="referred_patient_id" id ='patientId'>
                <option value="Other">Other</option>
                @foreach($patientNames as $patientName)
                <option value="{{$patientName->id}},{{$patientName->name}}">{{$patientName->name}}</option>
                @endForeach
            </select>
            </div>
          </div><br><br>
            <div class="form-group" id="otherRef">
              {!! Form::label('Other Refferal', null, array('class'=>'col-xs-4 control-label')) !!}
              <div class="col-md-6">
                {!! Form::text('other_referrals',$other_reff,array('class'=>'form-control','id'=>'other')) !!}
              </div>
            </div>

          @else
          <div class="form-group">
            {!! Form::label('Referred Patient Name', null, array('class'=>'col-xs-4 control-label')) !!}
            <div class="col-md-6">
            <select class="form-control" name="referred_patient_id" id ='patientId'>
                <option value="Other">Other</option>
                @foreach($patientNames as $patientName)
                <option value="{{$patientName->id}},{{$patientName->name}}">{{$patientName->name}}</option>
                @endForeach
            </select>
            </div>
          </div><br><br>
            <div class="form-group" id="otherRef">
              {!! Form::label('Other Refferal', null, array('class'=>'col-xs-4 control-label')) !!}
              <div class="col-md-6">
                {!! Form::text('other_referrals',$other_reff_pat_name['0']->other_referrals_name,array('class'=>'form-control','id'=>'other')) !!}
              </div>
            </div>
          
          @endif

          @endif

          <span  style="display:none" id="otherRef"> 
            <br><br><br>
          <div class="form-group">
          {!! Form::label('Other Referal', null, array('class'=>'col-xs-4 control-label')) !!}
          <div class="col-md-6">
            {!! Form::text(null, null,array('class'=>'form-control','id'=>'other')) !!}
          </div>
          </div>
          </span>
           
          <br><br><br>
          <div>
            {!! Form::hidden('pat_id', $patient_id) !!}
            {!! Form::button('&#x2714; Next',array('type' => 'submit','class'=>'btn btn-warning')) !!}
            {!! HTML::link('master/patient', '&#10006; Cancel', array('id' => 'linkid','class'=>'btn btn-danger')) !!}
            {!! Form::button('<i class="fa fa-plus fa-fw"></i>New Appointment',array('class'=>'btn btn-info')) !!}
          </div>

        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  <!-- /#wrapper -->

  <script type="text/javascript">
$('#patName').focus();

$('#patientId').change(function(){

  var patName=$(this).val();
  if(patName == 'Other')
  {
    document.getElementById('otherRef').style.display="block";

    var othertext = document.getElementById("other");
    othertext.setAttribute("name","other_referrals");
  }
  else
  {
    document.getElementById('otherRef').style.display="none";
  }
});


</script>

  @stop
