@extends('layouts.masterNav')

@section('title')
Patient
@stop

@section('side_bar')


<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('master/patient') }}" class="active">Patient Registration</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')


<div id="wrapper">

 <div id="page-wrapper">

  <!-- successfullye added msg -->
   <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->

  <div class="row">
    <a href="{{ url('master/patient/general') }}" class="btn btn-success pull-right col-xs-3" style="margin-top:2%"><i class="fa fa-plus fa-fw"></i>Add</a>
  </div>

  <div class="panel panel-info filterable">
    <!-- Table -->
    <table class="table table-hover"> 
      <thead class="panel-info">
        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="Name" disabled></th>
          <th><input type="text" class="form-control" placeholder="cell no" disabled></th>
          <th><input type="text" class="form-control" placeholder="Email" disabled></th>

          <th>Action</th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
        </tr> 
      </thead>
      <tbody>
        @foreach($patient_det as $patient)
        <tr>
          <td>{{$patient->name}}</td>
          <td>{{$patient->cell_no}}</td>
          <td>{{$patient->email}}</td>

          <td colspan="2">

            <a href="{{ url('editPatient',[ $patient->patient_id])}}" class="btn btn-primary btn-xs"><i class="fa fa-edit fa-fw"></i>Edit</a>
            
          </td>
        </tr> 
        @endforeach
      </tbody>
    </table>
  </div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
