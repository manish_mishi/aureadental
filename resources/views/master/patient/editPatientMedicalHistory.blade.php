@extends('layouts.masterNav')

@section('title')
Patient->MedicalHistory
<!-- {!! HTML::script('assets/js/angularJS/app.js') !!} -->
@stop

@section('side_bar')


<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/patient') }}" class="active">Patient Registration</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">

 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:50%" role="presentation"><a href="{{ url('editPatient',[$patient_id])}}">General</a></li>
      <li style="width:49%" role="presentation" class="active"><a href="{{ url('editPatientMedicalHistory',[$patient_id])}}">Medical History</a></li>
    </ul>
  </div>

  <!-- successfullye added msg -->
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->

  <div class="row">
   {!! Form::open(array('url' => 'master/patient/medicalhistory/update', 'class' => 'form','method' => 'post', 'enctype' =>"multipart/form-data")) !!}
   
   <div class="col-xs-6">
    <div class="row">
      {!! Form::textarea('med_hist', $patient_detials['0']->medical_history, array('size' => '40x5','class'=>'form-control','placeholder'=>'Enter any medical history here...')) !!}
    </div>
    
    <br><br>
    <div class="row">
      {!! Form::label('Upload attachments:', null, array('class'=>'col-xs-4 control-label')) !!}

      {!! Form::file('file', null,array('class'=>'form-control')) !!}

      <a href="{{url('storage/app/'.$patient_detials['0']->original_filename) }}" target="_blank"><label><u>{!! $patient_detials['0']->original_filename !!}</u></label></a>
      <div id="responseImage"> </div>
      {!! Form::hidden('pat_med_id',  $patient_detials['0']->id, array('value' => '0')) !!}
      {!! Form::hidden('pat_id',  $patient_detials['0']->patient_id, array('value' => '0')) !!}
    </div>
  </div>

  <div class="col-xs-6">

   <div style="margin-top:55%">
    {!! Form::button('&#x2714; Save',array('type' => 'submit','class'=>'btn btn-warning')) !!}
    {!! HTML::link('master/patient', '&#10006; Cancel', array('id' => 'linkid','class'=>'btn btn-danger')) !!}
    {!! Form::button('<i class="fa fa-plus fa-fw"></i>New Appointment',array('class'=>'btn btn-info')) !!}
  </div>

</div>
{!! Form::close() !!}
</div>

<script>
$(function(){
  $.post("/viewProfile/image", { id: 1 })
  .done(function( data ) {
    $('#responseImage').html('<img src="'+data+'">');
  });
})
</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div> <!-- /#page-wrapper -->
</div> <!-- /#wrapper --> 

@stop