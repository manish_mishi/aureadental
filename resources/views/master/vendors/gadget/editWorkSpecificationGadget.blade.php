@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')

<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/vendors/lab/listview') }}">Lab</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/material/listview') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/instrument/listview') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/gadget/listview') }}" class="active">Gadget</a>
      </li>
      <li>
        <a href="{{ url('master/vendors/machine/listview') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/maintenance/listview') }}">Maintenance</a>
      </li>         
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
  <div id="page-wrapper">

    <div class="panel panel-info">
      <ul style="width:100%" class="nav nav-pills">
        <li style="width:49%" role="presentation"><a href="{{ url('master/vendors/gadget/edit',[$vendorGadgetID]) }}">Vendor Details</a></li>
        <li style="width:50%" role="presentation" class="active"><a href="{{ url('master/vendors/gadget/work_specification/edit',[$vendorGadgetID]) }}">Vendor Work Specification</a></li>
      </ul>
    </div>

    {!! Form::open(array('route' => 'updateVendorGadgetWorkSpecification','class' => 'form')) !!}    
    <div id="main">

      <div class="row">
       <div class="form-group col-xs-5 col-sm-5 col-md-5 col-lg-5">
        {!! Form::label('Gadget Name', null, array('class'=>'col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label')) !!}
        <div class="col-xs-6">
         <select class="form-control" name="gadget_name_id[]" id ='gadgetName'>
          <option value>Select</option>
          @foreach($gadgetName as $gadgetTypes)
          <option value="{{$gadgetTypes->id}}">{{$gadgetTypes->gadget_name}}</option>
          @endForeach
        </select>
      </div>
    </div>

    <div class="form-group col-xs-5 col-sm-5 col-md-5 col-lg-5">
      {!! Form::label('Rate', null, array('class'=>'col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label')) !!}
      <div id="date1" class="col-xs-6">
        {!! Form::text('rate[]', null,array('id'=>'rate','class'=>'form-control')) !!}
      </div>
    </div>

    <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
      <button type="button"  onclick="duplicateGadget()" class="btn btn-info btn-md">
        <span class="glyphicon glyphicon-plus"></span> 
      </button>
    </div>
  </div>

  <div id="duplicater" class="row rbox" style="display:none;margin-top:1%;">
    <div class="form-group col-xs-5 col-sm-5 col-md-5 col-lg-5">
      {!! Form::label('Gadget Name', null, array('class'=>'col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        {!! Form::text(null, null, array('id'=>'dupGadgetName','class'=>'form-control','readonly'))!!}
        {!! Form::hidden(null, null, array('id'=>'dupGadgetNameId','class'=>'form-control gadget_name'))!!}
      </div>
    </div>

    <div class="form-group col-xs-5 col-sm-5 col-md-5 col-lg-5">
      {!! Form::label('Rate', null, array('class'=>'col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label')) !!}
      <div id="date1" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        {!! Form::text(null, null, array('id'=>'dupRate','class'=>'form-control rate','readonly'))!!}
      </div>
    </div>

    <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
      <button type="button" class="btn btn-danger btn-md" onclick="removeduplicate(this)">
        <span class="glyphicon glyphicon-trash"></span> 
      </button>
    </div>

  </div>

  @foreach($gadget_workspec_details as $gadget_workspec_detail)
  <div class="row rbox" style="display:block;margin-top:1%;">

    <div class="form-group col-xs-5 col-sm-5 col-md-5 col-lg-5">
      {!! Form::label('Gadget Name', null, array('class'=>'col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        {!! Form::text(null,  $gadget_workspec_detail->gadget_name, array('id'=>'dupGadgetName','class'=>'form-control','readonly'))!!}
        {!! Form::hidden(null, $gadget_workspec_detail->gadget_workspec_id, array('id'=>'dupGadgetNameId','class'=>'form-control gadget_name'))!!}
      </div>
    </div>

    <div class="form-group col-xs-5 col-sm-5 col-md-5 col-lg-5">
      {!! Form::label('Rate', null, array('class'=>'col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label')) !!}
      <div id="date1" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        {!! Form::text('null', $gadget_workspec_detail->rate, array('id'=>'dupRate','class'=>'form-control rate','readonly'))!!}
      </div>
    </div>

    <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
      <a href="{{  url('master/vendors/gadget/work_specification/delete', [$gadget_workspec_detail->gadget_workspec_id, $gadget_workspec_detail->vendor_gadget_id]) }}">
        <button type="button" class="btn btn-danger btn-md" onclick="removeduplicate(this)">
          <span class="glyphicon glyphicon-trash"></span> 
        </button>
      </a>
    </div>
    {!! Form::hidden('gadget_workspec_id[]', $gadget_workspec_detail->gadget_workspec_id) !!}
  </div>
  @endforeach

</div>

<br>
<div class="form-group">
  <div class="col-xs-7"></div>
  @if($vendorGadgetID != null)
  {!! Form::hidden('vendor_gadget_id', $vendorGadgetID) !!}
  @endif
  {!! Form::hidden('vendor_type_id', $vendor_type_id) !!}
  {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning','style'=>'margin-left:80%')) !!}
   {!! HTML::link('master/vendors/gadget/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}
</div>
{!! Form::close() !!}

<script type="text/javascript">

$(document).ready(function(){

  $("#gadgetName").change(function(){

    $('#dupGadgetName').val($('option:selected', this).text());
    $('#dupGadgetNameId').val($(this).val());
  });

});

var i = 0;

function duplicateGadget() 
{
  var gadgetId = document.getElementById('dupGadgetName').value;
  var rate = document.getElementById('rate').value;

  if(gadgetId != '')
  {
    var original = document.getElementById('duplicater');
    original.style.display = "block";

    document.getElementById('dupRate').value = rate;

  // "deep" clone
  var clone = original.cloneNode(true); 

  // there can only be one element with an ID
  clone.id = "duplicater" + ++i; 
  original.parentNode.appendChild(clone);

  original.style.display = "none";

  document.getElementById('gadgetName').selectedIndex = 0;
  document.getElementById('rate').value="";
  document.getElementById('dupRate').value="";

  var gadgetNameElement = document.getElementById("duplicater"+i).getElementsByClassName('gadget_name')[0];
  gadgetNameElement.setAttribute("name","gadget_name_id[]");

  var rateElement = document.getElementById("duplicater"+i).getElementsByClassName('rate')[0];
  rateElement.setAttribute("name","rate[]");

}

}

function removeduplicate(element)
{
element=element.parentNode.parentNode;//gets the id of the parent
element.parentNode.removeChild(element);
}


</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop