@extends('layouts.masterNav')

@section('title')
Lab -> Edit

@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
     <li>
      <a href="{{ url('master/vendors/lab/listview') }}" >Lab</a>
    </li>

    <li>
      <a href="{{ url('master/vendors/material/listview') }}">Material</a>
    </li>

    <li>
      <a href="{{ url('master/vendors/instrument/listview') }}">Instrument</a>
    </li>

    <li>
      <a href="{{ url('master/vendors/gadget/listview') }}" class="active">Gadget</a>
    </li>
    <li>
      <a href="{{ url('master/vendors/machine/listview') }}">Machine</a>
    </li>

    <li>
      <a href="{{ url('master/vendors/maintenance/listview') }}">Maintenance</a>
    </li>
  </ul>
</div>
<!-- /.sidebar-collapse -->
</div>

@stop

@section('main')


<div id="wrapper">

 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul class="nav nav-pills">
      <li style="width:50%" role="presentation" class="active"><a href="{{ url('master/vendors/gadget/edit',[$vendorGadgetID]) }}">Vendor Details</a></li>
      <li style="width:49%" role="presentation"><a href="{{ url('master/vendors/gadget/work_specification/edit',[$vendorGadgetID]) }}">Vendor Work Specification</a></li>
    </ul>
  </div>

  <div class="row">

   {!! Form::open(array('route' => 'updateVendorGadget','class' => 'form')) !!}

   <div class="col-xs-6">
    <div class="form-group">
      {!! Form::label('Vendor Type', null, array('class'=>'col-xs-4  control-label')) !!}
      <div class="col-md-6">
        {!! Form::text('vendor_type', $vendor_gadget_details->vendor_type_name,array('class'=>'form-control','readonly')) !!}
        {!! Form::hidden('vendor_type_id', $vendor_gadget_details->vendor_type_id) !!}
      </div>
    </div>

    <br><br><br>
    <div class="form-group">
      {!! Form::label('Vendor Name', null, array('class'=>'col-xs-4 control-label')) !!}
      <div class="col-xs-6">
        @if(Session::has('name'))
        <?php $name = Session::get('name') ?>
        {!! Form::text('gadget_name', $name,array('class'=>'form-control','required')) !!}
        @else
        {!! Form::text('gadget_name', $vendor_gadget_details->name,array('class'=>'form-control','required')) !!}
        @endif
      </div>
    </div>

    <br><br><br>
    <div class="form-group">
      <label class="col-md-4 control-label">Address</label>
      <div class="col-md-6">
       @if(Session::has('address'))
       <?php $address = Session::get('address') ?>
       {!! Form::text('address', $address,array('class'=>'form-control')) !!}
       @else
       {!! Form::text('address', $vendor_gadget_details->address,array('class'=>'form-control','required')) !!}
       @endif
     </div>
   </div>
 </div>

 <div class="col-xs-6">
  <div class="form-group">
    {!! Form::label('Contact Person', null, array('class'=>'col-xs-6 control-label')) !!}
    <div id="date1" class="col-xs-6">
      @if(Session::has('contact_person'))
      <?php $contact_person = Session::get('contact_person') ?>
      {!! Form::text('contact_person', $contact_person,array('class'=>'form-control')) !!}
      @else
      {!! Form::text('contact_person', $vendor_gadget_details->contact_person,array('class'=>'form-control','required')) !!}
      @endif
    </div>
  </div>

  <br><br><br>
  <div class="form-group">
    <label class="col-md-6 control-label">Email</label>
    <div class="col-md-6">
     @if(Session::has('email'))
     <?php $email = Session::get('email') ?>
     {!! Form::text('email', $email,array('class'=>'form-control')) !!}
     @else
     {!! Form::text('email', $vendor_gadget_details->email,array('class'=>'form-control','required')) !!}
     @endif
   </div>
 </div>

 <br><br><br>
 <div class="form-group">
  {!! Form::label('Cell',null,array('class'=>' col-xs-6 control-label')) !!}
  <div class="col-md-6">
    @if(Session::has('cell'))
    <?php $cell = Session::get('cell') ?>
    {!! Form::text('cell', $cell,array('class'=>'form-control')) !!}
    @else
    {!! Form::text('cell', $vendor_gadget_details->cell_no,array('class'=>'form-control','required')) !!}
    @endif
  </div>
</div>

<br><br><br>
<div class="form-group pull-right">
  {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default')) !!}
  {!! Form::hidden('vendorGadgetID',$vendor_gadget_details->id)!!}
  {!! Form::hidden('vendor_gadget_workspec_id',$vendor_gadget_details->m_vendor_gadget_workspec_id)!!}
  {!! HTML::link('master/vendors/gadget/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}
</div>

</div>
{!! Form::close() !!}
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
