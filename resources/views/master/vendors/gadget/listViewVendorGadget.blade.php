@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')

<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-3">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse" role="navigation" id="bs-sidebar-navbar-collapse-3">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/vendors/lab/listview')}}">Lab</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/material/listview') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/instrument/listview') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/gadget/listview') }}" class="active">Gadget</a>
      </li>
      <li>
        <a href="{{ url('master/vendors/machine/listview') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/maintenance/listview') }}">Maintenance</a>
      </li>    
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">

 <div id="page-wrapper">

  <!-- successfullye added msg -->
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->

  <br>
  <div>
    <a href="{{ url('master/vendors/gadget/add') }}" class="btn btn-success pull-right"><i class="fa fa-plus fa-fw"></i>New Gadget Vendor</a>  
  </div>

  <br>
  <div class="panel panel-info filterable  table-responsive" style="margin-top:3%">
   <!-- Table -->
   <table class="table table-hover"> 
    <thead class="panel-info">
      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="Gadget Vendor name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Gadget name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Rates" disabled></th>
        <th>Action</th>
        <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
      </tr>
    </thead>
    <tbody>
     @foreach($vendor_gadget_details as $vendor_gadget)
     <tr>
      <td>{{$vendor_gadget->name}}</td> 
      <td>{{$vendor_gadget->gadget_name}}</td>
      <td>{{$vendor_gadget->rate}}</td> 
      <td>
       <a href="{{ url('master/vendors/gadget/edit',[$vendor_gadget->id]) }}" class="btn btn-xs btn-primary"><i class="fa fa-edit fa-fw"></i>Edit</a>
     </td>
     <td>
       <a href="{{ url('master/vendors/gadget/view',[$vendor_gadget->id]) }}" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-eye-open">View</a>
     </td>
   </tr> 
   @endforeach
 </tbody>
</table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop