@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')

<!-- Brand and toggle get grouped for better mobile display -->

<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-3">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse" role="navigation" id="bs-sidebar-navbar-collapse-3">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/vendors/lab/listview') }}">Lab</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/material/listview') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/instrument/listview') }}" class="active">Instrument</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/gadget/listview') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('master/vendors/machine/listview') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/maintenance/listview') }}">Maintenance</a>
      </li>


    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
  <div id="page-wrapper">

    <div class="panel panel-info">
      <ul style="width:100%" class="nav nav-pills">
        <li style="width:50%" role="presentation"><a href="{{ url('master/vendors/instrument/add') }}">Vendor Details</a></li>
        <li style="width:49%" role="presentation" class="active"><a href="{{ url('master/vendors/instrument/work_specification') }}">Vendor Work Specification</a></li>
      </ul>
    </div>

    {!! Form::open(array('route' => 'addVendorInstrumentWorkSpecification','class' => 'form')) !!}

    <div id="main">
      <div class="row">
        <div class="form-group">
          {!! Form::label('Instrument Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
           <select class="form-control" name="instrument_type_id[]" id ='instType'>
            <option value>Select</option>
            @foreach($instType as $instTyp)
            <option value="{{$instTyp->id}}">{{$instTyp->name}}</option>
            @endForeach
          </select>
        </div>

        {!! Form::label('Instrument Sub-Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::select('instrument_subtype_id[]', array_merge([''=>'Select']), null, ['class' => 'form-control','id'=>'instSubtype'] )!!}
        </div>
      </div>

      <br><br>
      <div class="form-group">
        {!! Form::label('Instrument Name', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::select('instrument_name_id[]', array_merge([''=>'Select']), null, ['class' => 'form-control','id'=>'instName'] )!!}
        </div>

        {!! Form::label('Rate', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div id="date1" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text('rate[]', null,array('id'=>'rate','class'=>'form-control')) !!}
        </div>
      </div>

      <br><br>
      <div class="form-group">
        <button type="button"  onclick="duplicateInstrument()" class="btn btn-info btn-md" style="margin-left:85%">
          <span class="glyphicon glyphicon-plus"></span> 
        </button>
      </div>
    </div>

    <div id="duplicater" class="row rbox" style="display:none;margin-top:1%;">
      <div class="form-group">
        {!! Form::label('Instrument Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupInstType','class'=>'form-control','readonly'))!!}
          {!! Form::hidden(null, null, array('id'=>'dupInstTypeId','class'=>'form-control inst_type'))!!}
        </div>

        {!! Form::label('Instrument Sub-Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupInstSubtype','class'=>'form-control','readonly'))!!}
          {!! Form::hidden(null, null, array('id'=>'dupInstSubtypeId','class'=>'form-control inst_sub_type'))!!}
        </div>
      </div>

      <br><br>
      <div class="form-group">
        {!! Form::label('Instrument Name', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupInstName','class'=>'form-control','readonly'))!!}
          {!! Form::hidden(null, null, array('id'=>'dupInstNameId','class'=>'form-control inst_name'))!!}
        </div>

        {!! Form::label('Rate', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div id="date1" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupRate','class'=>'form-control rate','readonly'))!!}
        </div>
      </div>

      <br><br>
      <div class="form-group">
        <button type="button" class="btn btn-danger btn-md" onclick="removeduplicate(this)" style="margin-left:85%">
          <span class="glyphicon glyphicon-trash"></span> 
        </button>
      </div>

    </div>

  </div>

  <br>
  <div class="form-group">
   @if($vendorInstrumentID != null)
   {!! Form::hidden('vendor_instrument_id', $vendorInstrumentID) !!}
   @endif
   {!! Form::hidden('vendor_type_id', $vendor_type_id) !!}
   {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning','style'=>'margin-left:80%')) !!}
   {!! HTML::link('master/vendors/instrument/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
 </div>

 {!! Form::close() !!}

 <script type="text/javascript">

 $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

 $(document).ready(function(){
  $("#instType").change(function(){

    $('#dupInstType').val($('option:selected',this).text());
    $('#dupInstTypeId').val($(this).val());

    var instTypeID=$(this).val();
    var selectSubtype = document.getElementById('instSubtype');
    var selectName = document.getElementById('instName');

    if(instTypeID != "")
    {
      $.ajax({
        method: "POST",
        url: '{{url("master/instrument/instrument_type/")}}'+"/"+instTypeID,
        success : function(data){
          var comp="";

          $(selectSubtype).empty().append("<option value>Select</option>");
          $(selectName).empty().append("<option value>Select</option>");


          for (var i in data) {
            if(data[i]['instrument_subtype'] != comp)
            {
              $(selectSubtype).append('<option value=' + data[i]['instrument_subtype_id'] + '>' + data[i]['instrument_subtype'] + '</option>');
            }

            comp = data[i]['instrument_name']

            $(selectName).append('<option value=' + data[i]['id'] + '>' + data[i]['instrument_name'] + '</option>');
          }

        }

      }); 
    }
    else
    {
      $(selectSubtype).empty().append("<option value>Select</option>");
      $(selectName).empty().append("<option value>Select</option>");
    }

  });

$("#instSubtype").change(function(){
  $('#dupInstSubtype').val($('option:selected', this).text());
  $('#dupInstSubtypeId').val($(this).val());
});

$("#instName").change(function(){

  $('#dupInstName').val($('option:selected', this).text());
  $('#dupInstNameId').val($(this).val());
});

});

var i = 0;

function duplicateInstrument() 
{
  var instId = document.getElementById('dupInstType').value;
  var rate = document.getElementById('rate').value;

  if(instId != '')
  {
    var original = document.getElementById('duplicater');
    original.style.display = "block";

    document.getElementById('dupRate').value = rate;

  // "deep" clone
  var clone = original.cloneNode(true); 

  // there can only be one element with an ID
  clone.id = "duplicater" + ++i; 
  original.parentNode.appendChild(clone);

  original.style.display = "none";

  document.getElementById('instType').selectedIndex = 0;
  document.getElementById('dupInstType').value="";
  document.getElementById('dupInstTypeId').value="";

  var selectInstrumentSubtype = document.getElementById('instSubtype');

  for (m=0;m<selectInstrumentSubtype.length; m++) {
   if (selectInstrumentSubtype.options[m].value != '') {
     selectInstrumentSubtype.remove(m);
   }
 }

 document.getElementById('instSubtype').selectedIndex=0;
 document.getElementById('dupInstSubtype').value="";
 document.getElementById('dupInstSubtypeId').value="";

 var selectInstName = document.getElementById('instName');

 for (n=0;n<selectInstName.length; n++) {
   if (selectInstName.options[n].value != '') {
     selectInstName.remove(n);
   }
 }

 document.getElementById('dupInstName').value ="";
 document.getElementById('dupInstNameId').value="";

 document.getElementById('rate').value="";
 document.getElementById('dupRate').value="";

 var matTypeElement = document.getElementById("duplicater"+i).getElementsByClassName('inst_type')[0];
 matTypeElement.setAttribute("name","instrument_type_id[]");

 var workSubTypeElement = document.getElementById("duplicater"+i).getElementsByClassName('inst_sub_type')[0];
 workSubTypeElement.setAttribute("name","instrument_subtype_id[]");

 var workNameElement = document.getElementById("duplicater"+i).getElementsByClassName('inst_name')[0];
 workNameElement.setAttribute("name","instrument_name_id[]");

 var rateElement = document.getElementById("duplicater"+i).getElementsByClassName('rate')[0];
 rateElement.setAttribute("name","rate[]");

}

}

function removeduplicate(element)
{
element=element.parentNode.parentNode;//gets the id of the parent
element.parentNode.removeChild(element);
}


</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop