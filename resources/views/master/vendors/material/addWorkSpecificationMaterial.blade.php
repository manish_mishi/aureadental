@extends('layouts.masterNav')

@section('title')
Material Work Specification
@stop

@section('side_bar')

<!-- Brand and toggle get grouped for better mobile display -->

<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-3">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>

<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse" role="navigation" id="bs-sidebar-navbar-collapse-3">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/vendors/lab/listview') }}">Lab</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/material/listview') }}" class="active">Material</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/instrument/listview') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/gadget/listview') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('master/vendors/machine/listview') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/maintenance/listview') }}">Maintenance</a>
      </li>


    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
  <div id="page-wrapper">

    <div class="panel panel-info">
      <ul class="nav nav-pills">
        <li style="width:50%" role="presentation"><a href="{{ url('master/vendors/material/add') }}">Vendor Details</a></li>
        <li style="width:49%" role="presentation" class="active"><a href="{{ url('master/vendors/material/work_specification') }}">Vendor Work Specification</a></li>
      </ul>
    </div>

    {!! Form::open(array('route' => 'addVendorMaterialWorkSpecification','class' => 'form')) !!}

    <div id="main">
      <div  class="row">
        <div class="form-group">
          {!! Form::label('Material Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
           <select class="form-control" name="material_type_id[]" id ='matType'>
            <option value>Select</option>
            @foreach($matType as $workTyp)
            <option value="{{$workTyp->id}}">{{$workTyp->name}}</option>
            @endForeach
          </select>
        </div>

        {!! Form::label('Material Sub-Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::select('material_subtype_id[]', array_merge([''=>'Select']), null, ['class' => 'form-control','id'=>'materialSubtype'] )!!}
        </div>
      </div>

      <br><br>
      <div class="form-group">
        {!! Form::label('Material Name', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::select('material_name_id[]', array_merge([''=>'Select']), null, ['class' => 'form-control','id'=>'materialName'] )!!}
        </div>

        {!! Form::label('Rate', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div id="date1" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text('rate[]', null,array('id'=>'rate','class'=>'form-control')) !!}
        </div>
      </div>

      <br><br>
      <div class="form-group">
        {!! Form::label('Duration',null,array('class'=>' col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text('duration[]',null,array('class'=>'form-control','id'=>'duration'))!!}
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pull-right">
          <button type="button"  onclick="duplicateMaterial()" class="btn btn-info btn-md">
            <span class="glyphicon glyphicon-plus"></span> 
          </button>
        </div>
      </div>
    </div>

    <div id="duplicater" class="row rbox" style="display:none;margin-top:1%;">
      <div class="form-group">
        {!! Form::label('Material Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupMatType','class'=>'form-control','readonly'))!!}
          {!! Form::hidden(null, null, array('id'=>'dupMatTypeId','class'=>'form-control mat_type'))!!}
        </div>

        {!! Form::label('Material Sub-Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupMatSubtype','class'=>'form-control','readonly'))!!}
          {!! Form::hidden(null, null, array('id'=>'dupMatSubtypeId','class'=>'form-control mat_sub_type'))!!}
        </div>
      </div>

      <br><br>
      <div class="form-group">
        {!! Form::label('Material Name', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupMatName','class'=>'form-control','readonly'))!!}
          {!! Form::hidden(null, null, array('id'=>'dupMatNameId','class'=>'form-control mat_name'))!!}
        </div>

        {!! Form::label('Rate', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div id="date1" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupRate','class'=>'form-control rate','readonly'))!!}
        </div>
      </div>

      <br><br>
      <div class="form-group">
        {!! Form::label('Duration',null,array('class'=>' col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupDuration','class'=>'form-control duration','readonly'))!!}
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <button type="button" class="btn btn-danger btn-md" onclick="removeduplicate(this)">
            <span class="glyphicon glyphicon-trash"></span> 
          </button>
        </div>
      </div>

    </div>

  </div>

  <br><br><br>
  <div class="form-group">
   @if($vendorMaterialID != null)
   {!! Form::hidden('vendor_material_id', $vendorMaterialID) !!}
   @endif
   {!! Form::hidden('vendor_type_id', $vendor_type_id) !!}
   {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning','style'=>'margin-left:80%')) !!}
   {!! HTML::link('master/vendors/lab/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
 </div>

 {!! Form::close() !!}

 <script type="text/javascript">

 $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

 $(document).ready(function(){

  $("#matType").change(function(){

    $('#dupMatType').val($('option:selected',this).text());
    $('#dupMatTypeId').val($(this).val());

    var matTypeID=$(this).val();
    var selectSubtype = document.getElementById('materialSubtype');
    var selectName = document.getElementById('materialName');

    if(matTypeID != "")
    {
      $.ajax({
        method: "POST",
        url: '{{url("master/material/material_type/")}}'+"/"+matTypeID,
        success : function(data){
          var comp="";

          $(selectSubtype).empty().append("<option value>Select</option>");
          $(selectName).empty().append("<option value>Select</option>");

          for (var i in data) {
            if(data[i]['material_subtype'] != comp)
            {
              $(selectSubtype).append('<option value=' + data[i]['material_subtype_id'] + '>' + data[i]['material_subtype'] + '</option>');
            }

            comp = data[i]['material_name']

            $(selectName).append('<option value=' + data[i]['id'] + '>' + data[i]['material_name'] + '</option>');
          }


        }


      }); 
    }
    else
    {
      $(selectSubtype).empty().append("<option value>Select</option>");
      $(selectName).empty().append("<option value>Select</option>");
    }

  });

});

$("#materialSubtype").change(function(){
  $('#dupMatSubtype').val($('option:selected', this).text());
  $('#dupMatSubtypeId').val($(this).val());
});

$("#materialName").change(function(){

  $('#dupMatName').val($('option:selected', this).text());
  $('#dupMatNameId').val($(this).val());
});


var i = 0;

function duplicateMaterial() 
{
  var matId = document.getElementById('dupMatType').value;
  var rate = document.getElementById('rate').value;
  var duration = document.getElementById('duration').value;

  if(matId != '')
  {
    var original = document.getElementById('duplicater');
    original.style.display = "block";

    document.getElementById('dupRate').value = rate;
    document.getElementById('dupDuration').value = duration;

  // "deep" clone
  var clone = original.cloneNode(true); 

  // there can only be one element with an ID
  clone.id = "duplicater" + ++i; 
  original.parentNode.appendChild(clone);

  original.style.display = "none";

  document.getElementById('matType').selectedIndex = 0;
  document.getElementById('dupMatType').value="";
  document.getElementById('dupMatTypeId').value="";

  var selectMaterialSubtype = document.getElementById('materialSubtype');

  for (m=0;m<selectMaterialSubtype.length; m++) {
   if (selectMaterialSubtype.options[m].value != '') {
     selectMaterialSubtype.remove(m);
   }
 }

 document.getElementById('materialSubtype').selectedIndex=0;
 document.getElementById('dupMatSubtype').value="";
 document.getElementById('dupMatSubtypeId').value="";

 var selectMatName = document.getElementById('materialName');

 for (n=0;n<selectMatName.length; n++) {
   if (selectMatName.options[n].value != '') {
     selectMatName.remove(n);
   }
 }

 document.getElementById('dupMatName').value ="";
 document.getElementById('dupMatNameId').value="";

 document.getElementById('rate').value="";
 document.getElementById('dupRate').value="";
 document.getElementById('duration').value="";
 document.getElementById('dupDuration').value ="";

 var matTypeElement = document.getElementById("duplicater"+i).getElementsByClassName('mat_type')[0];
 matTypeElement.setAttribute("name","material_type_id[]");

 var workSubTypeElement = document.getElementById("duplicater"+i).getElementsByClassName('mat_sub_type')[0];
 workSubTypeElement.setAttribute("name","material_subtype_id[]");

 var workNameElement = document.getElementById("duplicater"+i).getElementsByClassName('mat_name')[0];
 workNameElement.setAttribute("name","material_name_id[]");

 var rateElement = document.getElementById("duplicater"+i).getElementsByClassName('rate')[0];
 rateElement.setAttribute("name","rate[]");

 var durationElement = document.getElementById("duplicater"+i).getElementsByClassName('duration')[0];
 durationElement.setAttribute("name","duration[]");

}

}

function removeduplicate(element)
{
element=element.parentNode.parentNode.parentNode;//gets the id of the parent
element.parentNode.removeChild(element);
}

</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop