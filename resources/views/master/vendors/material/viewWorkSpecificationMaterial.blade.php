@extends('layouts.masterNav')

@section('title')
Material Work Specification -> View
@stop

@section('side_bar')

<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/vendors/lab/listview') }}">Lab</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/material/listview') }}" class="active">Material</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/instrument/listview') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/gadget/listview') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('master/vendors/machine/listview') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/maintenance/listview') }}">Maintenance</a>
      </li>     
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
  <div id="page-wrapper">

    <div class="panel panel-info">
      <ul class="nav nav-pills">
        <li style="width:49%" role="presentation"><a href="{{url('master/vendors/material/view',[$vendorMaterialID])}}">Vendor Details</a></li>
        <li style="width:50%" role="presentation" class="active"><a href="{{url('master/vendors/material/work_specification/view',[$vendorMaterialID])}}">Vendor Work Specification</a></li>
      </ul>
    </div>

    <div id="main">

      @foreach($mat_workspec_details as $mat_workspec_detail)
      <div class="row rbox" style="display:block;margin-top:1%;">
        <div class="form-group">
          {!! Form::label('Material Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text(null, $mat_workspec_detail->material_type_name, array('id'=>'dupWorkType','class'=>'form-control','readonly'))!!}
          </div>

          {!! Form::label('Material Sub-Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text(null, $mat_workspec_detail->material_subtype_name, array('id'=>'dupWorkSubType','class'=>'form-control','readonly'))!!}
          </div>
        </div>

        <br><br>
        <div class="form-group">
          {!! Form::label('Material Name', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text(null, $mat_workspec_detail->name, array('id'=>'dupWorkName','class'=>'form-control','readonly'))!!}
          </div>

          {!! Form::label('Rate', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
          <div id="date1" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text('null', $mat_workspec_detail->rate, array('id'=>'dupRate','class'=>'form-control rate','readonly'))!!}
          </div>
        </div>

        <br><br>
        <div class="form-group">
          {!! Form::label('Duration',null,array('class'=>' col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text('null', $mat_workspec_detail->duration, array('id'=>'dupDuration','class'=>'form-control duration','readonly'))!!}
          </div>

        </div>

      </div>
      @endforeach

    </div>

    <br><br><br>
    <div class="form-group">
      {!! HTML::link('master/vendors/material/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
    </div>

    <!-- back to top of the page -->
    <p id="backTop" style="display: none;">
      <a href="#top"><span></span>Back to Top</a>
    </p>

  </div>
</div>
<!-- /#wrapper -->

@stop
