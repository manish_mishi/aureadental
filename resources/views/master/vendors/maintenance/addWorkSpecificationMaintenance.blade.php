@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')

<!-- Brand and toggle get grouped for better mobile display -->

<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-3">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse" role="navigation" id="bs-sidebar-navbar-collapse-3">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/vendors/lab/listview') }}">Lab</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/material/listview') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/instrument/listview') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/gadget/listview') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('master/vendors/machine/listview') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/maintenance/listview') }}" class="active">Maintenance</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
  <div id="page-wrapper">

    <div class="panel panel-info">
      <ul class="nav nav-pills">
        <li style="width:49%" role="presentation"><a href="{{url('master/vendors/maintenance/add')}}">Vendor Details</a></li>
        <li style="width:50%" role="presentation" class="active"><a href="{{ url('master/vendors/maintenance/work_specification') }}">Vendor Work Specification</a></li>
      </ul>
    </div>
    
    {!! Form::open(array('route' => 'addVendorMaintenanceWorkSpecification','class' => 'form')) !!}

    <div id="main">

      <div class="row">
        <div class="col-xs-5">
          <div class="form-group">
            {!! Form::label('Contract Start Date', null, array('class'=>'col-xs-6 control-label')) !!}
            <div class="col-md-6">
              <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="startDate" class="form-control" name="contract_start_date[]">
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Contract End Date', null, array('class'=>'col-xs-6  control-label')) !!}
            <div class="col-md-6">
              <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="endDate" class="form-control" name="contract_end_date[]">
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Contract Amount', null, array('class'=>'col-xs-6 control-label')) !!}
            <div class="col-xs-6">
              {!! Form::text('contract_amount[]', null,array('class'=>'form-control','id'=>'contractAmount')) !!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Expected Availability Time', null, array('class'=>'col-xs-6 control-label')) !!}
            <div class="col-xs-6">
              {!! Form::text('availability_time[]', null,array('class'=>'form-control','id'=>'availabilityTime')) !!}
            </div>
          </div>
        </div>

        <div class="col-xs-5">
          <div class="form-group">
            {!! Form::label('Gadget Name', null, array('class'=>'col-xs-6 control-label')) !!}
            <div id="date1" class="col-xs-6">
              <select class="form-control" name="gadget_name_id[]" id ='gadgetName'>
                <option value>Select</option>
                @foreach($gadgetName as $gadgetTypes)
                <option value="{{$gadgetTypes->id}}">{{$gadgetTypes->gadget_name}}</option>
                @endForeach
              </select>
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Duration',null,array('class'=>' col-xs-6 control-label')) !!}
            <div class="col-md-6">
              {!! Form::text('duration[]',null,array('class'=>'form-control','id'=>'duration'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Rates/Maintenance',null,array('class'=>' col-xs-6 control-label')) !!}
            <div class="col-md-6">
              {!! Form::text('rate[]',null,array('class'=>'form-control','id'=>'rate'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Machine Name',null,array('class'=>' col-xs-6 control-label')) !!}
            <div class="col-md-6">
              <select class="form-control" name="machine_name_id[]" id ='machineName'>
                <option value>Select</option>
                @foreach($machineName as $machineTypes)
                <option value="{{$machineTypes->id}}">{{$machineTypes->machine_name}}</option>
                @endForeach
              </select>
            </div>
          </div>
        </div>
        
        <br><br><br><br>
        <div style='margin-left:80%'>
          <button type="button" onclick="duplicateMaintenance()" class="btn btn-info btn-md">
            <span class="glyphicon glyphicon-plus"></span> 
          </button>
        </div>

      </div>

      <div id="duplicater" class="row rbox" style="display:none;margin-top:1%;">
        <div class="col-xs-5">
          <div class="form-group">
            {!! Form::label('Contract Start Date', null, array('class'=>'col-xs-6 control-label')) !!}
            <div class="col-md-6">
              {!! Form::text(null, null, array('id'=>'dupStartDate','class'=>'form-control start_date','readonly'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Contract End Date', null, array('class'=>'col-xs-6 control-label')) !!}
            <div class="col-md-6">
              {!! Form::text(null, null, array('id'=>'dupEndDate','class'=>'form-control end_date','readonly'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Contract Amount', null, array('class'=>'col-xs-6 control-label')) !!}
            <div class="col-xs-6">
              {!! Form::text(null, null, array('id'=>'dupContAmt','class'=>'form-control contract_amount','readonly'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Expected Availability Time', null, array('class'=>'col-xs-6 control-label')) !!}
            <div class="col-xs-6">
              {!! Form::text(null, null, array('id'=>'dupAvailTime','class'=>'form-control availability_time','readonly'))!!}
            </div>
          </div>
        </div>

        <div class="col-xs-5">
          <div class="form-group">
            {!! Form::label('Gadget Name', null, array('class'=>'col-xs-6 control-label')) !!}
            <div id="date1" class="col-xs-6">
              {!! Form::text(null, null, array('id'=>'dupGadgetName','class'=>'form-control','readonly'))!!}
              {!! Form::hidden(null, null, array('id'=>'dupGadgetNameId','class'=>'form-control gadget_name'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Duration',null,array('class'=>'col-xs-6 control-label')) !!}
            <div class="col-md-6">
              {!! Form::text('null',null,array('class'=>'form-control duration','id'=>'dupDuration','readonly'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Rates/Maintenance',null,array('class'=>' col-xs-6 control-label')) !!}
            <div class="col-md-6">
              {!! Form::text('null',null,array('class'=>'form-control rate','id'=>'dupRate','readonly'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Machine Name',null,array('class'=>' col-xs-6 control-label')) !!}
            <div class="col-md-6">
              {!! Form::text(null, null, array('id'=>'dupMachineName','class'=>'form-control','readonly'))!!}
              {!! Form::hidden(null, null, array('id'=>'dupMachineNameId','class'=>'form-control machine_name'))!!}
            </div>
          </div>
        </div>
        
        <br><br><br><br>
        <div style='margin-left:80%'>
          <button type="button" class="btn btn-danger btn-md" onclick="removeduplicate(this)">
            <span class="glyphicon glyphicon-trash"></span> 
          </button>
        </div>
      </div>

    </div>

    <br><br>
    <div>
      @if($vendorMaintenanceID != null)
      {!! Form::hidden('vendor_maintenance_id', $vendorMaintenanceID) !!}
      @endif
      {!! Form::hidden('vendor_type_id', $vendor_type_id) !!}
      {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning','style'=>'margin-left:80%')) !!}
      {!! HTML::link('master/vendors/maintenance/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
    </div>
    {!! Form::close() !!}

    <script type="text/javascript">

    $(document).ready(function(){

      $("#gadgetName").change(function(){

        $('#dupGadgetName').val($('option:selected', this).text());
        $('#dupGadgetNameId').val($(this).val());
      });

      $("#machineName").change(function(){

        $('#dupMachineName').val($('option:selected', this).text());
        $('#dupMachineNameId').val($(this).val());
      });

    });

    var i = 0;

    function duplicateMaintenance() 
    {
      var startDate = document.getElementById('startDate').value;
      var gadgetId = document.getElementById('dupGadgetNameId').value;
      var endDate = document.getElementById('endDate').value;
      var duration = document.getElementById('duration').value;
      var contAmt = document.getElementById('contractAmount').value;
      var rate = document.getElementById('rate').value;
      var availTime = document.getElementById('availabilityTime').value;
      var machineId = document.getElementById('dupMachineNameId').value;

      if(startDate != '' && endDate != '')
      {
        var original = document.getElementById('duplicater');
        original.style.display = "block";

        document.getElementById('dupRate').value = rate;
        document.getElementById('dupStartDate').value = startDate;
        document.getElementById('dupEndDate').value = endDate;
        document.getElementById('dupDuration').value = duration;
        document.getElementById('dupContAmt').value = contAmt;
        document.getElementById('dupRate').value = rate;
        document.getElementById('dupAvailTime').value = availTime;

      // "deep" clone
        var clone = original.cloneNode(true); 

      // there can only be one element with an ID
        clone.id = "duplicater" + ++i; 
        original.parentNode.appendChild(clone);

        original.style.display = "none";

        document.getElementById('startDate').value="";
        document.getElementById('dupStartDate').value="";
        document.getElementById('gadgetName').selectedIndex = 0;
        document.getElementById('dupGadgetName').value="";
        document.getElementById('dupGadgetNameId').value="";
        document.getElementById('endDate').value="";
        document.getElementById('dupEndDate').value="";
        document.getElementById('duration').value="";
        document.getElementById('dupDuration').value="";
        document.getElementById('contractAmount').value="";
        document.getElementById('dupContAmt').value="";
        document.getElementById('rate').value="";
        document.getElementById('dupRate').value="";
        document.getElementById('availabilityTime').value="";
        document.getElementById('machineName').selectedIndex = 0;
        document.getElementById('dupMachineName').value="";
        document.getElementById('dupMachineNameId').value="";


        var startDateElement = document.getElementById("duplicater"+i).getElementsByClassName('start_date')[0];
        startDateElement.setAttribute("name","contract_start_date[]");

        var gadgetNameElement = document.getElementById("duplicater"+i).getElementsByClassName('gadget_name')[0];
        gadgetNameElement.setAttribute("name","gadget_name_id[]");

        var endDateElement = document.getElementById("duplicater"+i).getElementsByClassName('end_date')[0];
        endDateElement.setAttribute("name","contract_end_date[]");

        var durationElement = document.getElementById("duplicater"+i).getElementsByClassName('duration')[0];
        durationElement.setAttribute("name","duration[]");

        var contAmtElement = document.getElementById("duplicater"+i).getElementsByClassName('contract_amount')[0];
        contAmtElement.setAttribute("name","contract_amount[]");

        var rateElement = document.getElementById("duplicater"+i).getElementsByClassName('rate')[0];
        rateElement.setAttribute("name","rate[]");

        var availTimeElement = document.getElementById("duplicater"+i).getElementsByClassName('availability_time')[0];
        availTimeElement.setAttribute("name","availability_time[]");

        var machineNameElement = document.getElementById("duplicater"+i).getElementsByClassName('machine_name')[0];
        machineNameElement.setAttribute("name","machine_name_id[]");

      }

    }

    function removeduplicate(element)
    {
      element=element.parentNode.parentNode;//gets the id of the parent
      element.parentNode.removeChild(element);
    }

</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
