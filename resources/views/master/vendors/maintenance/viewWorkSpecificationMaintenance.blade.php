@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')

<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/vendors/lab/listview') }}">Lab</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/material/listview') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/instrument/listview') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/gadget/listview') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('master/vendors/machine/listview') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/maintenance/listview') }}" class="active">Maintenance</a>
      </li>


    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
  <div id="page-wrapper">

    <div class="panel panel-info">
      <ul style="width:100%" class="nav nav-pills">
        <li style="width:50%" role="presentation"><a href="{{url('master/vendors/maintenance/view',[$vendorMaintenanceID])}}">Vendor Details</a></li>
        <li style="width:49%" role="presentation" class="active"><a href="{{url('master/vendors/maintenance/work_specification/view',[$vendorMaintenanceID])}}">Vendor Work Specification</a></li>
      </ul>
    </div>
    
    <div id="main">
      @foreach($maintenance_workspec_details as $maintenance_workspec_detail)
      <div id="duplicater" class="row rbox" style="display:block;margin-top:1%;">
        <div class="col-xs-6">
          <div class="form-group">
            {!! Form::label('Contract Start Date', null, array('class'=>'col-xs-6 control-label')) !!}
            <div class="col-md-6">
              {!! Form::text(null, $maintenance_workspec_detail->contract_start_date, array('id'=>'dupStartDate','class'=>'form-control start_date','readonly'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Contract End Date', null, array('class'=>'col-xs-6 control-label')) !!}
            <div class="col-md-6">
              {!! Form::text(null, $maintenance_workspec_detail->contract_end_date, array('id'=>'dupEndDate','class'=>'form-control end_date','readonly'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Contract Amount', null, array('class'=>'col-xs-6 control-label')) !!}
            <div class="col-xs-6">
              {!! Form::text(null, $maintenance_workspec_detail->contract_amount, array('id'=>'dupContAmt','class'=>'form-control contract_amount','readonly'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Expected Availability Time', null, array('class'=>'col-xs-6 control-label')) !!}
            <div class="col-xs-6">
              {!! Form::text(null, $maintenance_workspec_detail->expected_available_time, array('id'=>'dupAvailTime','class'=>'form-control availability_time','readonly'))!!}
            </div>
          </div>
        </div>

        <div class="col-xs-6">
          <div class="form-group">
            {!! Form::label('Gadget Name', null, array('class'=>'col-xs-6 control-label')) !!}
            <div id="date1" class="col-xs-6">
              {!! Form::text(null, $maintenance_workspec_detail->gadget_name, array('id'=>'dupGadgetName','class'=>'form-control','readonly'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Duration',null,array('class'=>'col-xs-6 control-label')) !!}
            <div class="col-md-6">
              {!! Form::text('null',$maintenance_workspec_detail->duration, array('class'=>'form-control duration','id'=>'dupDuration','readonly'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Rates/Maintenance',null,array('class'=>' col-xs-6 control-label')) !!}
            <div class="col-md-6">
              {!! Form::text('null',$maintenance_workspec_detail->rate, array('class'=>'form-control rate','id'=>'dupRate','readonly'))!!}
            </div>
          </div>

          <br><br>
          <div class="form-group">
            {!! Form::label('Machine Name',null,array('class'=>' col-xs-6 control-label')) !!}
            <div class="col-md-6">
              {!! Form::text(null, $maintenance_workspec_detail->machine_name, array('id'=>'dupMachineName','class'=>'form-control','readonly'))!!}
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>

      <br><br>
      <div class="form-group">
        {!! HTML::link('master/vendors/maintenance/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
      </div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
