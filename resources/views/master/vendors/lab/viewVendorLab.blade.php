@extends('layouts.masterNav')

@section('title')
Lab -> Edit

@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
     <li>
      <a href="{{ url('master/vendors/lab/listview') }}" class="active">Lab</a>
    </li>

    <li>
      <a href="{{ url('master/vendors/material/listview') }}">Material</a>
    </li>

    <li>
      <a href="{{ url('master/vendors/instrument/listview') }}">Instrument</a>
    </li>

    <li>
      <a href="{{ url('master/vendors/gadget/listview') }}">Gadget</a>
    </li>
    <li>
      <a href="{{ url('master/vendors/machine/listview') }}">Machine</a>
    </li>

    <li>
      <a href="{{ url('master/vendors/maintenance/listview') }}">Maintenance</a>
    </li>
  </ul>
</div>
<!-- /.sidebar-collapse -->
</div>

@stop

@section('main')


<div id="wrapper">

 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul class="nav nav-pills">
      <li style="width:50%" role="presentation" class="active"><a href="{{url('master/vendors/lab/view',[$vendor_lab_details->id])}}">Vendor Details</a></li>
      <li style="width:49%" role="presentation"><a href="{{url('master/vendors/lab/work_specification/view',[$vendor_lab_details->id])}}">Vendor Work Specification</a></li>
    </ul>
  </div>

  <div class="row">

   <div class="col-xs-6">
    <div class="form-group">
      {!! Form::label('Vendor Type', null, array('class'=>'col-xs-4  control-label')) !!}
      <div class="col-md-6">
        {!! Form::text('vendor_type', $vendor_lab_details->vendor_type_name,array('class'=>'form-control','readonly')) !!}
        {!! Form::hidden('vendor_type_id', $vendor_lab_details->vendor_type_id) !!}
      </div>
    </div>

    <br><br><br>
    <div class="form-group">
      {!! Form::label('Lab Name', null, array('class'=>'col-xs-4 control-label')) !!}
      <div class="col-xs-6">
        {!! Form::text('lab_name', $vendor_lab_details->name, array('class'=>'form-control','readonly')) !!}
      </div>
    </div>

    <br><br><br>
    <div class="form-group">
      <label class="col-md-4 control-label">Address</label>
      <div class="col-md-6">
        {!! Form::textarea('address', $vendor_lab_details->address, array('class'=>'form-control','rows' => 2, 'cols' => 40,'readonly'))!!}   

      </div>
    </div>

    <br>

  </div>

  <div class="col-xs-6">
    <div class="form-group">
      {!! Form::label('Contact Person', null, array('class'=>'col-xs-6 control-label')) !!}
      <div id="date1" class="col-xs-6">
        {!! Form::text('contact_person', $vendor_lab_details->contact_person, array('id'=>'day','class'=>'form-control','placeholder'=>'','readonly')) !!}
      </div>
    </div>
    <br><br><br>
    <div class="form-group">
      <label class="col-md-6 control-label">Email</label>
      <div class="col-md-6">
        {!! Form::email('email', $vendor_lab_details->email, array('class'=>'form-control','readonly')) !!}
      </div>
    </div>

    <br><br><br>
    <div class="form-group">
      {!! Form::label('Cell',null,array('class'=>' col-xs-6 control-label')) !!}
      <div class="col-md-6">
        {!! Form::input('number','cell',$vendor_lab_details->cell_no,array('class'=>'form-control','readonly'))!!}
      </div>
    </div>
    
    <br><br><br>
    <div class="form-group pull-right">
     {!! Form::hidden('vendor_lab_id',$vendor_lab_details->id)!!}
     {!! Form::hidden('vendor_lab_workspec_id',$vendor_lab_details->m_vendor_lab_workspec_id)!!}
     <a href="{{ url('master/vendors/lab/work_specification/view',[$vendor_lab_details->id]) }}" class="btn  btn-default">&#x2714;Next</a>
     {!! HTML::link('master/vendors/lab/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}
   </div>

 </div>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
