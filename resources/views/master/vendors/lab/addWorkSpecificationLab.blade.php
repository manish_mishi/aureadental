@extends('layouts.masterNav')

@section('title')
Lab Work Specification
@stop

@section('side_bar')

<!-- Brand and toggle get grouped for better mobile display -->

<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-3">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>

<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse" role="navigation" id="bs-sidebar-navbar-collapse-3">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/vendors/lab/listview') }}" class="active">Lab</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/material/listview') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/instrument/listview') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/gadget/listview') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('master/vendors/machine/listview') }}">Machine</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/maintenance/listview') }}">Maintenance</a>
      </li>


    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
  <div id="page-wrapper">

    <div class="panel panel-info">
      <ul class="nav nav-pills">
        <li style="width:50%" role="presentation"><a href="{{ url('master/vendors/lab/add') }}">Vendor Details</a></li>
        <li style="width:49%" role="presentation" class="active"><a href="{{ url('master/vendors/lab/work_specification') }}">Vendor Work Specification</a></li>
      </ul>
    </div>

    {!! Form::open(array('route' => 'addVendorLabWorkSpecification','class' => 'form')) !!}

    <div id="main">
      <div  class="row">
        <div class="form-group">
          {!! Form::label('Work Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
           <select class="form-control" name="work_type_id[]" id ='workType'>
            <option value>Select</option>
            @foreach($workType as $workTyp)
            <option value="{{$workTyp->id}}">{{$workTyp->name}}</option>
            @endForeach
          </select>
        </div>

        {!! Form::label('Work Sub-Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3  control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::select('work_subtype_id[]', array_merge([''=>'Select']), null, ['class' => 'form-control','id'=>'workSubtype'] )!!}
        </div>
      </div>

      <br><br>
      <div class="form-group">
        {!! Form::label('Work Name', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::select('work_name_id[]', array_merge([''=>'Select']), null, ['class' => 'form-control','id'=>'workName'] )!!}
        </div>

        {!! Form::label('Rate', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div id="date1" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text('rate[]', null,array('id'=>'rate','class'=>'form-control')) !!}
        </div>
      </div>

      <br><br>
      <div class="form-group">
        {!! Form::label('Duration',null,array('class'=>' col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text('duration[]',null,array('class'=>'form-control','id'=>'duration'))!!}
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pull-right">
          <button type="button"  onclick="duplicateWork()" class="btn btn-info btn-md">
            <span class="glyphicon glyphicon-plus"></span> 
          </button>
        </div>
      </div>
    </div>

    <div id="duplicater" class="row rbox" style="display:none;margin-top:1%;">
      <div class="form-group">
        {!! Form::label('Work Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupWorkType','class'=>'form-control','readonly'))!!}
          {!! Form::hidden(null, null, array('id'=>'dupWorkTypeId','class'=>'form-control work_type'))!!}
        </div>

        {!! Form::label('Work Sub-Type', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupWorkSubType','class'=>'form-control','readonly'))!!}
          {!! Form::hidden(null, null, array('id'=>'dupWorkSubTypeId','class'=>'form-control work_sub_type'))!!}
        </div>
      </div>

      <br><br>
      <div class="form-group">
        {!! Form::label('Work Name', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupWorkName','class'=>'form-control','readonly'))!!}
          {!! Form::hidden(null, null, array('id'=>'dupWorkNameId','class'=>'form-control work_name'))!!}
        </div>

        {!! Form::label('Rate', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div id="date1" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupRate','class'=>'form-control rate','readonly'))!!}
        </div>
      </div>

      <br><br>
      <div class="form-group">
        {!! Form::label('Duration',null,array('class'=>' col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, null, array('id'=>'dupDuration','class'=>'form-control duration','readonly'))!!}
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <button type="button" class="btn btn-danger btn-md" onclick="removeduplicate(this)">
            <span class="glyphicon glyphicon-trash"></span> 
          </button>
        </div>
      </div>

    </div>

  </div>

  <br><br><br>
  <div class="form-group">
   @if($vendorLabID != null)
   {!! Form::hidden('vendor_lab_id', $vendorLabID) !!}
   @endif
   {!! Form::hidden('vendor_type_id', $vendor_type_id) !!}
   {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning','style'=>'margin-left:80%')) !!}
   {!! HTML::link('master/vendors/lab/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
 </div>

 {!! Form::close() !!}

 <script type="text/javascript">

 $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

 $(document).ready(function(){

  $("#workType").change(function(){

    $('#dupWorkType').val($('option:selected',this).text());
    $('#dupWorkTypeId').val($(this).val());

    var workTypeID=$(this).val();
    var selectSubtype = document.getElementById('workSubtype');
    var selectName = document.getElementById('workName');

    if(workTypeID != "")
    {
      $.ajax({
        method: "POST",
        url: '{{url("master/lab/work_type/")}}'+"/"+workTypeID,
        success : function(data){
          var comp="";

          $(selectSubtype).empty().append("<option value>Select</option>");
          $(selectName).empty().append("<option value>Select</option>");

          for (var i in data) {console.log(data);
            if(data[i]['lab_work_subtype'] != comp)
            {
              $(selectSubtype).append('<option value=' + data[i]['lab_work_subtype_id'] + '>' + data[i]['lab_work_subtype'] + '</option>');
            }

            comp = data[i]['lab_work_subtype']

            $(selectName).append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
          }

        }


      }); 
    }
    else
    {
      $(selectSubtype).empty().append("<option value>Select</option>");
      $(selectName).empty().append("<option value>Select</option>");
    }

  });

});

$("#workSubtype").change(function(){

  $('#dupWorkSubType').val($('option:selected', this).text());
  $('#dupWorkSubTypeId').val($(this).val());
});

$("#workName").change(function(){

  $('#dupWorkName').val($('option:selected', this).text());
  $('#dupWorkNameId').val($(this).val());
});


var i = 0;

function duplicateWork() 
{
  var matId = document.getElementById('dupWorkType').value;
  var rate = document.getElementById('rate').value;
  var duration = document.getElementById('duration').value;

  if(matId != '')
  {
    var original = document.getElementById('duplicater');
    original.style.display = "block";

    document.getElementById('dupRate').value = rate;
    document.getElementById('dupDuration').value = duration;

  // "deep" clone
    var clone = original.cloneNode(true); 

  // there can only be one element with an ID
    clone.id = "duplicater" + ++i; 
    original.parentNode.appendChild(clone);

    original.style.display = "none";

    document.getElementById('workType').selectedIndex = 0;
    document.getElementById('dupWorkType').value="";
    document.getElementById('dupWorkTypeId').value="";

    var selectWorkSubtype = document.getElementById('workSubtype');

    for (m=0;m<selectWorkSubtype.length; m++) {
     if (selectWorkSubtype.options[m].value != '') {
       selectWorkSubtype.remove(m);
     }
   }

   document.getElementById('workSubtype').selectedIndex=0;
   document.getElementById('dupWorkSubType').value="";
   document.getElementById('dupWorkSubTypeId').value="";

   var selectWorkName = document.getElementById('workName');

   for (n=0;n<selectWorkName.length; n++) {
     if (selectWorkName.options[n].value != '') {
       selectWorkName.remove(n);
     }
   }

   document.getElementById('dupWorkName').value ="";
   document.getElementById('dupWorkNameId').value="";

   document.getElementById('rate').value="";
   document.getElementById('dupRate').value="";
   document.getElementById('duration').value="";
   document.getElementById('dupDuration').value ="";

   var workTypeElement = document.getElementById("duplicater"+i).getElementsByClassName('work_type')[0];
   workTypeElement.setAttribute("name","work_type_id[]");

   var workSubTypeElement = document.getElementById("duplicater"+i).getElementsByClassName('work_sub_type')[0];
   workSubTypeElement.setAttribute("name","work_subtype_id[]");

   var workNameElement = document.getElementById("duplicater"+i).getElementsByClassName('work_name')[0];
   workNameElement.setAttribute("name","work_name_id[]");

   var rateElement = document.getElementById("duplicater"+i).getElementsByClassName('rate')[0];
   rateElement.setAttribute("name","rate[]");

   var durationElement = document.getElementById("duplicater"+i).getElementsByClassName('duration')[0];
   durationElement.setAttribute("name","duration[]");

 }

}

function removeduplicate(element)
{
element=element.parentNode.parentNode.parentNode;//gets the id of the parent
element.parentNode.removeChild(element);
}

</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop