@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')

<!-- Brand and toggle get grouped for better mobile display -->

<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-3">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse" role="navigation" id="bs-sidebar-navbar-collapse-3">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/vendors/lab/listview') }}">Lab</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/material/listview') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/instrument/listview') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/gadget/listview') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('master/vendors/machine/listview') }}" class="active">Machine</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/maintenance/listview') }}">Maintenance</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
  <div id="page-wrapper">

    <div class="panel panel-info">
      <ul class="nav nav-pills">
        <li style="width:50%" role="presentation" class="active"><a href="{{ url('master/vendors/machine/add') }}">Vendor Details</a></li>
        <li style="width:49%" role="presentation"><a href="{{ url('master/vendors/machine/work_specification') }}">Vendor Work Specification</a></li>
      </ul>
    </div>

    <div class="row">
     {!! Form::open(array('route' => 'addmachinevendors','class' => 'form')) !!}
     <div class="col-xs-6">        
      <div class="form-group">
        {!! Form::label('Vendor Type', null, array('class'=>'col-xs-4 col-sm-4 col-md-4 col-lg-4   control-label')) !!}
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
         {!! Form::text('vendor_type', $vendor_type->name,array('class'=>'form-control','readonly')) !!}
         {!! Form::hidden('vendor_type_id', $vendor_type->id) !!}        
       </div>            
     </div>

     <br><br><br>
     <div class="form-group">
      {!! Form::label('Vendor Name', null, array('class'=>'col-xs-4 col-sm-4 col-md-4 col-lg-4  control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        @if(Session::has('name'))
        <?php $name = Session::get('name') ?>
        {!! Form::text('machine_name', $name,array('class'=>'form-control')) !!}
        @else
        {!! Form::text('machine_name', null,array('class'=>'form-control','required')) !!}
        @endif
      </div>
    </div>

    <br><br><br>
    <div class="form-group">
      <label class="col-xs-4 col-sm-4 col-md-4 col-lg-4  control-label">Address</label>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        @if(Session::has('address'))
        <?php $address = Session::get('address') ?>
        {!! Form::text('address',$address,array('class'=>'form-control')) !!}
        @else
        {!! Form::text('address',null,array('class'=>'form-control')) !!}
        @endif
      </div>
    </div>
  </div>

  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ">
    <div class="form-group">
      {!! Form::label('Contact Person', null, array('class'=>'col-xs-6 control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        @if(Session::has('contact_person'))
        <?php $contact_person = Session::get('contact_person') ?>
        {!! Form::text('contact_person', $contact_person,array('class'=>'form-control')) !!}
        @else
        {!! Form::text('contact_person', null,array('class'=>'form-control')) !!}
        @endif
      </div>
    </div>

    <br><br><br>
    <div class="form-group">
      <label class="col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label">Email</label>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        @if(Session::has('email'))
        <?php $email = Session::get('email') ?>
        {!! Form::text('email',$email,array('class'=>'form-control')) !!}
        @else
        {!! Form::text('email',null,array('class'=>'form-control')) !!}
        @endif
      </div>
    </div>

    <br><br><br>
    <div class="form-group">
      {!! Form::label('Cell',null,array('class'=>'col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        @if(Session::has('cell'))
        <?php $cell = Session::get('cell') ?>
        {!! Form::text('cell',$cell,array('class'=>'form-control')) !!}
        @else
        {!! Form::text('cell',null,array('class'=>'form-control')) !!}
        @endif
      </div>
    </div>

    <br><br><br>
    <div class="form-group">
      {!! HTML::link('master/vendors/machine/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
      {!! Form::hidden('vendor_machine_id', $vendorMachineID->id+1) !!}
      {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default pull-right','style'=>'margin-right:1%')) !!}
    </div>
  </div>
  {!! Form::close() !!}
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
