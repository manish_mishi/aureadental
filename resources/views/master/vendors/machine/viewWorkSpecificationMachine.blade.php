@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')

<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/vendors/lab/listview') }}">Lab</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/material/listview') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/instrument/listview') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/gadget/listview') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('master/vendors/machine/listview') }}" class="active">Machine</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/maintenance/listview') }}">Maintenance</a>
      </li>       
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
  <div id="page-wrapper">

    <div class="panel panel-info">
      <ul class="nav nav-pills">
        <li style="width:49%" role="presentation"><a href="{{url('master/vendors/machine/view',[$vendorMachineID])}}">Vendor Details</a></li>
        <li style="width:50%" role="presentation" class="active"><a href="{{url('master/vendors/machine/work_specification/view',[$vendorMachineID])}}">Vendor Work Specification</a></li>
      </ul>
    </div>

    <div id="main">

      @foreach($machine_workspec_details as $machine_workspec_detail)
      <div class="row rbox" style="display:block;margin-top:1%;">
        {!! Form::label('Machine Name', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text(null, $machine_workspec_detail->machine_name, array('id'=>'dupMachineName','class'=>'form-control','readonly'))!!}
        </div>

        {!! Form::label('Rate', null, array('class'=>'col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label')) !!}
        <div id="date1" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text('null', $machine_workspec_detail->rate, array('id'=>'dupRate','class'=>'form-control rate','readonly'))!!}
        </div>

      </div>
      @endforeach

    </div>

    <br>     
    <div class="form-group">
      {!! HTML::link('master/vendors/machine/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
    </div>
  </div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop