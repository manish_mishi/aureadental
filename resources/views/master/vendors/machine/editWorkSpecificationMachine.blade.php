@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')

<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/vendors/lab/listview') }}">Lab</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/material/listview') }}">Material</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/instrument/listview') }}">Instrument</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/gadget/listview') }}">Gadget</a>
      </li>
      <li>
        <a href="{{ url('master/vendors/machine/listview') }}" class="active">Machine</a>
      </li>

      <li>
        <a href="{{ url('master/vendors/maintenance/listview') }}">Maintenance</a>
      </li>       
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">
  <div id="page-wrapper">

    <div class="panel panel-info">
      <ul class="nav nav-pills">
        <li style="width:49%" role="presentation"><a href="{{ url('master/vendors/machine/edit',[$vendorMachineID]) }}">Vendor Details</a></li>
        <li style="width:50%" role="presentation" class="active"><a href="{{ url('master/vendors/machine/edit',[$vendorMachineID]) }}">Vendor Work Specification</a></li>
      </ul>
    </div>

    {!! Form::open(array('route' => 'updateVendorMachineWorkSpecification','class' => 'form')) !!}

    <div id="main">

      <div class="row">
       <div class="form-group col-xs-5 col-sm-5 col-md-5 col-lg-5">
        {!! Form::label('Machine Name', null, array('class'=>'col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label')) !!}
        <div class="col-xs-6">
          <select class="form-control" name="machine_name_id[]" id ='machineName'>
            <option value>Select</option>
            @foreach($machineName as $machineTypes)
            <option value="{{$machineTypes->id}}">{{$machineTypes->machine_name}}</option>
            @endForeach
          </select>
        </div>
      </div>

      <div class="form-group col-xs-5 col-sm-5 col-md-5 col-lg-5">
        {!! Form::label('Rate', null, array('class'=>'col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label')) !!}
        <div id="date1" class="col-xs-6">
          {!! Form::text('rate[]', null,array('id'=>'rate','class'=>'form-control')) !!}
        </div>
      </div>

      <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <button type="button"  onclick="duplicateGadget()" class="btn btn-info btn-md">
          <span class="glyphicon glyphicon-plus"></span> 
        </button>
      </div>
    </div>

    <div id="duplicater" class="row rbox" style="display:none;margin-top:1%;">
      <div class="form-group col-xs-5 col-sm-5 col-md-5 col-lg-5">
        {!! Form::label('Machine Name', null, array('class'=>'col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label')) !!}
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          {!! Form::text(null, null, array('id'=>'dupMachineName','class'=>'form-control','readonly'))!!}
          {!! Form::hidden(null, null, array('id'=>'dupMachineNameId','class'=>'form-control machine_name'))!!}
        </div>
      </div>

      <div class="form-group col-xs-5 col-sm-5 col-md-5 col-lg-5">
        {!! Form::label('Rate', null, array('class'=>'col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label')) !!}
        <div id="date1" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          {!! Form::text(null, null, array('id'=>'dupRate','class'=>'form-control rate','readonly'))!!}
        </div>
      </div>

      <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <button type="button" class="btn btn-danger btn-md" onclick="removeduplicate(this)">
          <span class="glyphicon glyphicon-trash"></span> 
        </button>
      </div>
    </div>

    @foreach($machine_workspec_details as $machine_workspec_detail)
    <div class="row rbox" style="display:block;margin-top:1%;">

      <div class="form-group col-xs-5 col-sm-5 col-md-5 col-lg-5">
        {!! Form::label('Machine Name', null, array('class'=>'col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label')) !!}
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          {!! Form::text(null,  $machine_workspec_detail->machine_name, array('id'=>'dupMachineName','class'=>'form-control','readonly'))!!}
          {!! Form::hidden(null, $machine_workspec_detail->machine_workspec_id, array('id'=>'dupMachineNameId','class'=>'form-control machine_name'))!!}
        </div>
      </div>

      <div class="form-group col-xs-5 col-sm-5 col-md-5 col-lg-5">
        {!! Form::label('Rate', null, array('class'=>'col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label')) !!}
        <div id="date1" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          {!! Form::text('null', $machine_workspec_detail->rate, array('id'=>'dupRate','class'=>'form-control rate','readonly'))!!}
        </div>
      </div>

      <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
        <a href="{{  url('master/vendors/machine/work_specification/delete', [$machine_workspec_detail->machine_workspec_id, $machine_workspec_detail->vendor_machine_id]) }}">
          <button type="button" class="btn btn-danger btn-md" onclick="removeduplicate(this)">
            <span class="glyphicon glyphicon-trash"></span> 
          </button>
        </a>
      </div>
      {!! Form::hidden('machine_workspec_id[]', $machine_workspec_detail->machine_workspec_id) !!}
    </div>
    @endforeach

  </div>

  <br>     
  <div class="form-group">
    <div class="col-xs-7"></div>
    @if($vendorMachineID != null)
    {!! Form::hidden('vendor_machine_id', $vendorMachineID) !!}
    @endif
    {!! Form::hidden('vendor_type_id', $vendor_type_id) !!}
    {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning','style'=>'margin-left:80%')) !!}
    {!! HTML::link('master/vendors/machine/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger pull-right')) !!}
  </div>
  {!! Form::close() !!}

  <script type="text/javascript">

  $(document).ready(function(){

    $("#machineName").change(function(){

      $('#dupMachineName').val($('option:selected', this).text());
      $('#dupMachineNameId').val($(this).val());
    });

  });

  var i = 0;

  function duplicateGadget() 
  {
    var gadgetId = document.getElementById('dupMachineName').value;
    var rate = document.getElementById('rate').value;

    if(gadgetId != '')
    {
      var original = document.getElementById('duplicater');
      original.style.display = "block";

      document.getElementById('dupRate').value = rate;

  // "deep" clone
  var clone = original.cloneNode(true); 

  // there can only be one element with an ID
  clone.id = "duplicater" + ++i; 
  original.parentNode.appendChild(clone);

  original.style.display = "none";

  document.getElementById('machineName').selectedIndex = 0;
  document.getElementById('rate').value="";
  document.getElementById('dupRate').value="";

  var gadgetNameElement = document.getElementById("duplicater"+i).getElementsByClassName('machine_name')[0];
  gadgetNameElement.setAttribute("name","machine_name_id[]");

  var rateElement = document.getElementById("duplicater"+i).getElementsByClassName('rate')[0];
  rateElement.setAttribute("name","rate[]");

}

}

function removeduplicate(element)
{
element=element.parentNode.parentNode;//gets the id of the parent
element.parentNode.removeChild(element);
}

</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop