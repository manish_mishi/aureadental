@extends('layouts.masterNav')

@section('title')
Alerts
@stop

@section('side_bar')


<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('master/inventory/materialmanagement/listview') }}" class="active">Material Management</a>
      </li>
      
      <li>
        <a href="{{ url('master/inventory/instrumentmanagement/listview') }}">Instrument Management</a>
      </li>
      
      <li>
        <a href="{{ url('master/inventory/machinemanagement/listview') }}">Machine Management</a>
      </li>
      
      <li>
        <a href="{{ url('master/inventory/gadgetmanagement/listview') }}">Gadget Management</a>
      </li>

      
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<!-- successfullye added msg -->
<div class="flash-message">
  @foreach (['danger', 'warning', 'success', 'info'] as $msg)
  @if(Session::has('alert-' . $msg))

  <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
  @endif
  @endforeach
</div> <!-- end .flash-message -->

<div id="wrapper">

 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/materialmanagement/add') }}">Material Detail</a></li>
      <li style="width:33%" role="presentation" class="active"><a href="{{ url('master/inventory/materialmanagement/alert') }}">Alerts Settings</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/materialmanagement/treatment') }}">Treatment</a></li>
    </ul>
  </div>

  <div class="row" >
    {!! Form::open(array('route' => 'addmaterialalert','class' => 'form')) !!}
    <div class="col-xs-2 form-group">

      {!! Form::label('Alert', null, array('class'=>'alert alert-warning control-label')) !!}
    </div>
    
    <div class="form-group">
      {!! form::label('Safety Stock ',null,array('class'=>'col-xs-2 control-label'))!!}
      
      <div class="col-xs-2">
        @if(Session::has('material_name'))
        <?php $SafetyStock = Session::get('materialSaftystock') ?>
        {!! Form::text('Safety_Stock', $SafetyStock,array('class'=>'form-control')) !!}
        @else
        {!! Form::text('Safety_Stock', null,array('class'=>'form-control')) !!}
        @endif
      </div>
    </div>
    
    <br><br><br>
    <div class="form-group pull-right">

     {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default')) !!}
     {!! HTML::link('master/inventory/materialmanagement/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}
   </div>
   {!! Form::close() !!}
 </div>

 <!-- back to top of the page -->
 <p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
