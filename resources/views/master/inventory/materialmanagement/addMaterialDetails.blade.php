@extends('layouts.masterNav')

@section('title')
Material Detail
@stop

@section('side_bar')


<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/inventory/materialmanagement/listview') }}" class="active">Material Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/instrumentmanagement/listview') }}">Instrument Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/machinemanagement/listview') }}">Machine Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/gadgetmanagement/listview') }}">Gadget Management</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')


<div id="wrapper">

 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:33%" role="presentation" class="active"><a href="{{ url('master/inventory/materialmanagement/add') }}">Material Detail</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/materialmanagement/alert') }}">Alerts Settings</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/materialmanagement/treatment') }}">Treatment</a></li>
    </ul>
  </div>
  <!-- successfullye added msg -->
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->

  <div class="row">
    {!! Form::open(array('route'=>'addmaterial','class'=>'form')) !!}

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      There were some problems adding the category.<br />
      <ul>
        @foreach ($errors->all() as $error)
        <li></li>
        @endforeach
      </ul>
    </div>
    @endif

    <div class="form-group">
      {!! form::label('Material Name',null,array('class'=>'col-xs-4 control-label'))!!}
      
      <div class="col-xs-4">
       @if(Session::has('material_name'))
       <?php $name = Session::get('material_name') ?>
       {!! Form::text('mat_nam', $name,array('class'=>'form-control')) !!}
       @else
       {!! Form::text('mat_nam', null,array('class'=>'form-control','required')) !!}
       @endif

     </div>
   </div>   <br><br><br><br>
   @if(Session::has('material_type'))
   <?php $material_type = Session::get('material_type');
   $mat_name=Session::get('mat_name');
   $mat_id=Session::get('mat_id');
   ?>
   <div class="form-group">
    {!! Form::label('Material Type',null,array('class'=>'col-xs-4 control-label'))!!} 
    <div class="col-xs-4">
      <select class="form-control" name="material_type_id" id ='matType' required>
       <option value="{{$mat_id}},{{$mat_name}}">{{$mat_name}}</option>
       @foreach($materialTypes as $materialType)
       <option value="{{$materialType->id}},{{$materialType->name}}">{{$materialType->name}}</option>
       @endForeach
     </select>
   </div>
   @else
   <div class="form-group">
    {!! Form::label('Material Type',null,array('class'=>'col-xs-4 control-label'))!!} 
    <div class="col-xs-4">
      <select class="form-control" name="material_type_id" id ='matType' required>
       <option value >Select</option>
       @foreach($materialTypes as $materialType)
       <option value="{{$materialType->id}},{{$materialType->name}}">{{$materialType->name}}</option>
       @endForeach
     </select>
   </div>
 </div>
 @endif
 <br><br><br>

 @if(Session::has('material_subtype'))
 <?php $material_type = Session::get('material_subtype');
 $mat_subType_id=Session::get('mat_subType_id');
 $mat_subType=Session::get('mat_subType');
 ?>
 <div class="form-group">
  {!! Form::label('Material Subtype',null,array('class'=>'col-xs-4 control-label')) !!}
  <div class="col-xs-4">
    <select class="form-control" name="material_subtype_id" id ='matSubtype' required>
      <option value="{{$mat_subType_id}},{{$mat_subType}}">{{$mat_subType}}</option>
      <option value></option>
    </select>
  </div>
</div>
@else
<div class="form-group">
  {!! Form::label('Material Subtype',null,array('class'=>'col-xs-4 control-label')) !!}
  <div class="col-xs-4">
    <select class="form-control" name="material_subtype_id" id ='matSubtype' required>
      <option value>Select Material Type</option>
    </select>
  </div>
</div>
@endif
<br><br><br>

@if(Session::has('material_type'))
<?php $material_types = Session::get('material_type');
$mat_unit_id=Session::get('mat_unit_id');
$mat_unit_nmae=Session::get('mat_unit_nmae');
?>
<div class="form-group">
  {!! form::label('Unit',null,array('class'=>'col-xs-4 control-label'))!!}
  <div class="col-xs-4">
    <select class="form-control" name="material_unit_id" id ='matUnit' required>
     <option value="{{$mat_unit_id}},{{$mat_unit_nmae}}">{{$mat_unit_nmae}}</option>
     @foreach($materialUnit as $matUnit)
     <option value="{{$matUnit->id}},{{$matUnit->name}}">{{$matUnit->name}}</option>
     @endForeach
   </select>
 </div>
</div>
@else
<div class="form-group">
  {!! form::label('Unit',null,array('class'=>'col-xs-4 control-label'))!!}
  <div class="col-xs-4">
    <select class="form-control" name="material_unit_id" id ='matUnit' required>
     <option value>Select</option>
     @foreach($materialUnit as $matUnit)
     <option value="{{$matUnit->id}},{{$matUnit->name}}">{{$matUnit->name}}</option>
     @endForeach
   </select>
 </div>
</div>
@endif
<br><br>
<div class="form-group">
  <div class="col-xs-9"></div>
  {!! form::hidden('mat_id',$materialId)!!}
  {!! form::hidden('token1', csrf_token(),array('id'=>'token'))!!}
  {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default')) !!}
  {!! HTML::link('master/inventory/materialmanagement/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}

</div>
{!! Form::close() !!}
</div>

<script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(document).ready(function(){

  var mat_typ_id = $('#matType').val();
  if(mat_typ_id != null)
  {
    var select = document.getElementById('matSubtype');

    $.ajax({
      method: "POST",
      url: '{{url("material_type/")}}'+"/"+mat_typ_id,
      success : function(data){console.log(data);
        /*$("#mat_subtype").val(data);*/
        for (var i in data) {
          $(select).append('<option value=' + data[i]['id'] +','+ data[i]['name'] +'>' + data[i]['name'] + '</option>');
        }

      }

    });
  }

  $("#matType").change(function(){

    var mat_type_id=$(this).val();
    var select = document.getElementById('matSubtype');

    $(select).empty();

    $.ajax({
      method: "POST",
      url: '{{url("material_type/")}}'+"/"+mat_type_id,
      success : function(data){
        /*$("#mat_subtype").val(data);*/
        for (var i in data) {
          $(select).append('<option value=' + data[i]['id'] +','+ data[i]['name'] +'>' + data[i]['name'] + '</option>');
        }

      }

    });


  });

});


</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
