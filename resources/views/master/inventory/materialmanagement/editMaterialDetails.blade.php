@extends('layouts.masterNav')

@section('title')
Master Detail
@stop

@section('side_bar')


<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/inventory/materialmanagement/listview') }}" class="active">Material Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/instrumentmanagement/listview') }}">Instrument Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/machinemanagement/listview') }}">Machine Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/gadgetmanagement/listview') }}">Gadget Management</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')


<div id="wrapper">

 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:33%" role="presentation" class="active"><a href="{{ url('master/inventory/materialmanagement/edit',[$material_detail->material_name_id]) }}">Material Detail</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/materialmanagement/alert/edit',[$material_detail->material_name_id]) }}">Alerts Settings</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/materialmanagement/treatment/edit',[$material_detail->material_name_id]) }}">Treatment</a></li>
    </ul>
  </div>

  <div class="row">
    {!! Form::open(array('route'=>'updateMaterialDetails','class'=>'form')) !!}

    <div class="form-group">
      {!! form::label('Material Name',null,array('class'=>'col-xs-4 control-label'))!!}

      <div class="col-xs-4">
        {!! form::text('mat_nam',$material_detail->material_name,array('class'=>'form-control','required'))!!}

      </div>
    </div>   <br><br><br>

    <div class="form-group">
      {!! Form::label('Material Type',null,array('class'=>'col-xs-4 control-label'))!!} 
      <div class="col-xs-4">
        <select class="form-control" name="material_type_id" id ='matType' required>
         <option value="{{$material_detail->material_type_id}},{{$material_detail->material_type_name}}">{{$material_detail->material_type_name}}</option>
         @foreach($materialTypes as $materialType)
         <option value="{{$materialType->id}},{{$materialType->name}}">{{$materialType->name}}</option>
         @endForeach
       </select>
      </div>

    </div>
    <br><br><br>
    <div class="form-group">
      {!! Form::label('Material Subtype',null,array('class'=>'col-xs-4 control-label')) !!}

      <div class="col-xs-4">
        <select class="form-control" name="material_subtype_id" id ='matSubtype' required>
         <option value="{{$material_detail->material_subtype_id}},{{$material_detail->material_subtype_name}}">{{$material_detail->material_subtype_name}}</option>
       </select>

      </div>
    </div>
    <br><br><br>

    <div class="form-group">
      {!! form::label('Unit',null,array('class'=>'col-xs-4 control-label'))!!}

      <div class="col-xs-4">
        <select class="form-control" name="material_unit_id" id ='matUnit' required>
         <option value="{{$material_detail->material_unit_id}},{{$material_detail->material_unit_name}}">{{$material_detail->material_unit_name}}</option>
         @foreach($materialUnit as $matUnit)
         <option value="{{$matUnit->id}},{{$matUnit->name}}">{{$matUnit->name}}</option>
         @endForeach
       </select>
      </div>
    </div>

    <br><br>
    <div class="form-group">
      <div class="col-xs-9"></div>
      {!! form::hidden('token1', csrf_token(),array('id'=>'token'))!!}
      {!! Form::hidden('mat_id', $material_detail->material_name_id) !!}
     {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default')) !!}
      {!! HTML::link('master/inventory/materialmanagement/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}

    </div>


    {!! Form::close() !!}
  </div>

  <script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(document).ready(function(){
  $("#matType").change(function(){

    var mat_type_id=$(this).val();
    var select = document.getElementById('matSubtype');

    $(select).empty();

    $.ajax({
      method: "POST",
      url: '{{url("material_type/")}}'+"/"+mat_type_id,
      success : function(data){
        /*$("#mat_subtype").val(data);*/
        for (var i in data) {
           $(select).append('<option value=' + data[i]['id'] +','+ data[i]['name'] +'>' + data[i]['name'] + '</option>');
        }

      }

    });


  });

});


</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop


@stop
