@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')


<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/inventory/materialmanagement/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/instrumentmanagement/listview') }}">Instrument Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/machinemanagement/listview') }}" class="active">Machine Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/gadgetmanagement/listview') }}">Gadget Management</a>
      </li>


    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')


<div id="wrapper">

 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:50%" role="presentation" class="active"><a href="{{ url('master/inventory/machinemanagement/edit',[$machine_detail->machine_name_id]) }}">Machine Detail</a></li>
      <li style="width:49%" role="presentation"><a href="{{ url('master/inventory/machinemanagement/treatment/edit',[$machine_detail->machine_name_id]) }}">Treatment</a></li>
    </ul>
  </div>

  <div class="row">
    {!! Form::open(array('route'=>'updateMachineDetails','class'=>'form')) !!}

    <div class="form-group">
      {!! form::label('Machine Name',null,array('class'=>'col-xs-4 control-label'))!!}

      <div class="col-xs-4">
        {!! form::text('machine_name',$machine_detail->machine_name,array('class'=>'form-control','required'))!!}

      </div>
    </div>   <br><br><br>

    <div class="form-group">
      {!! Form::label('Machine Type',null,array('class'=>'col-xs-4 control-label'))!!} 
      <div class="col-xs-4">
        <select class="form-control" name="machine_type_id" id ='mchType' required>
         <option value="{{$machine_detail->machine_type_id}},{{$machine_detail->machine_type_name}}">{{$machine_detail->machine_type_name}}</option>
         @foreach($machineTypes as $machineType)
         <option value="{{$machineType->id}},{{$machineType->name}}">{{$machineType->name}}</option>
         @endForeach
       </select>
     </div>
   </div>
   <br><br><br>
   <div class="form-group">
    {!! Form::label('Machine Subtype',null,array('class'=>'col-xs-4 control-label')) !!}

    <div class="col-xs-4">
      <select class="form-control" name="machine_subtype_id" id ='mchSubtype' required>
       <option value="{{$machine_detail->machine_subtype_id}},{{$machine_detail->machine_subtype_name}}">{{$machine_detail->machine_subtype_name}}</option>
     </select>

    </div>
  </div>

  <br><br>
  <div class="form-group">
    <div class="col-xs-9"></div>
    {!! form::hidden('token1', csrf_token(),array('id'=>'token'))!!}
    {!! Form::hidden('mch_id', $machine_detail->machine_name_id) !!}
   {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default')) !!}
    {!! HTML::link('master/inventory/machinemanagement/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}  
  </div>


  {!! Form::close() !!}
</div>

<script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(document).ready(function(){
  $("#mchType").change(function(){

    var mchType_id=$(this).val();
    var select = document.getElementById('mchSubtype');

    $(select).empty();

    $.ajax({
      method: "POST",
      url: '{{url("machine_type/")}}'+"/"+mchType_id,
      success : function(data){
        /*$("#mat_subtype").val(data);*/
        for (var i in data) {
         $(select).append('<option value=' + data[i]['id'] +','+ data[i]['name'] +'>' + data[i]['name'] + '</option>');
        }

      }

    });


  });

});


</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
