@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/inventory/materialmanagement/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/instrumentmanagement/listview') }}">Instrument Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/machinemanagement/listview') }}" class="active">Machine Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/gadgetmanagement/listview') }}">Gadget Management</a>
      </li>


    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')


<div id="wrapper">

 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:50%" role="presentation" class="active"><a href="{{ url('master/inventory/machinemanagement/add') }}">Machine Detail</a></li>
      <li style="width:49%" role="presentation"><a href="{{ url('master/inventory/machinemanagement/treatment') }}">Treatment</a></li>
    </ul>
  </div>
 <!-- successfullye added msg -->
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->
  <div class="row">
    {!! Form::open(array('route'=>'addMachine','class'=>'form')) !!}

    <div class="form-group">
      {!! form::label('Machine Name',null,array('class'=>'col-xs-4 control-label'))!!}

      <div class="col-xs-4">
        @if(Session::has('machine_name'))
        <?php $name = Session::get('machine_name') ?>
        {!! form::text('machine_name',$name,array('class'=>'form-control','required'))!!}
        @else
        {!! form::text('machine_name',null,array('class'=>'form-control','required'))!!}
         @endif

      </div>
    </div>   <br><br><br>
    @if(Session::has('machine_type_id'))
    <?php $instrument_type_id = Session::get('machine_type_id');
      $machineType_id=Session::get('machineType_id');
      $machine_Name=Session::get('instType_name');
    ?>
    <div class="form-group">
      {!! Form::label('Machine Type',null,array('class'=>'col-xs-4 control-label'))!!} 
      <div class="col-xs-4">
        <select class="form-control" name="machine_type_id" id ='mchType' required>
         <option value="{{$machineType_id}},{{$machine_Name}}">{{$machine_Name}}</option>
         @foreach($machineTypes as $machineType)
         <option value="{{$machineType->id}},{{$machineType->name}}">{{$machineType->name}}</option>
         @endForeach
       </select>
     </div>
   </div>
    @else
    <div class="form-group">
      {!! Form::label('Machine Type',null,array('class'=>'col-xs-4 control-label'))!!} 
      <div class="col-xs-4">
        <select class="form-control" name="machine_type_id" id ='mchType' required>
         <option value>Select</option>
         @foreach($machineTypes as $machineType)
         <option value="{{$machineType->id}},{{$machineType->name}}">{{$machineType->name}}</option>
         @endForeach
       </select>
     </div>
   </div>
    @endif
   <br><br>

@if(Session::has('machine_subtype_id'))
    <?php $instrument_type_id = Session::get('machine_subtype_id');
      $machineSubtype_id=Session::get('machineSubtype_id');
      $machineSubtype_name=Session::get('machineSubtype_name');
    ?>
   <div class="form-group">
    {!! Form::label('Machine Subtype',null,array('class'=>'col-xs-4 control-label')) !!}
    <div class="col-xs-4">
      <select class="form-control" name="machine_subtype_id" id ='mchSubtype' required>
         <option value="{{$machineSubtype_id}},{{$machineSubtype_name}}">{{$machineSubtype_name}}</option>
       </select>
    </div>
  </div>
@else
  <div class="form-group">
    {!! Form::label('Machine Subtype',null,array('class'=>'col-xs-4 control-label')) !!}
    <div class="col-xs-4">
      <select class="form-control" name="machine_subtype_id" id ='mchSubtype' required>
         <option value>Select</option>
       </select>
    </div>
  </div>
@endif
  <br><br>
  <div class="form-group">
    <div class="col-xs-9"></div>
     {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default')) !!}
    {!! HTML::link('master/inventory/machinemanagement/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}  
  </div>


  {!! Form::close() !!}
</div>

<script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(document).ready(function(){
  $("#mchType").change(function(){

    var mchType_id=$(this).val();
    var select = document.getElementById('mchSubtype');

    $(select).empty();

    $.ajax({
      method: "POST",
      url: '{{url("machine_type/")}}'+"/"+mchType_id,
      success : function(data){
        /*$("#mat_subtype").val(data);*/
        for (var i in data) {
           $(select).append('<option value=' + data[i]['id'] +','+ data[i]['name'] +'>' + data[i]['name'] + '</option>');
        }

      }

    });


  });

});


</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
