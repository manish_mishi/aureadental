@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/inventory/materialmanagement/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/instrumentmanagement/listview') }}" class="active">Instrument Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/machinemanagement/listview') }}">Machine Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/gadgetmanagement/listview') }}">Gadget Management</a>
      </li>


    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')


<div id="wrapper">

 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:33%" role="presentation" class="active"><a href="{{ url('master/inventory/instrumentmanagement/edit',[$instrument_detail->instrument_name_id]) }}">Instrument Detail</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/instrumentmanagement/alert/edit',[$instrument_detail->instrument_name_id]) }}">Alerts Settings</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/instrumentmanagement/treatment/edit',[$instrument_detail->instrument_name_id]) }}">Treatment</a></li>
    </ul>
  </div>

  <div class="row">
    {!! Form::open(array('route'=>'updateInstrumentDetails','class'=>'form')) !!}

    <br>
    <div class="form-group">
      {!! form::label('Instrument Name',null,array('class'=>'col-xs-4 control-label'))!!}

      <div class="col-xs-4">
        {!! form::text('instrument_name',$instrument_detail->instrument_name,array('class'=>'form-control','required'))!!}

      </div>
    </div>   <br><br><br>

    <div class="form-group">
      {!! Form::label('Instrument Type',null,array('class'=>'col-xs-4 control-label'))!!} 
      <div class="col-xs-4">
        <select class="form-control" name="instrument_type_id" id ='instType' required>
         <option value="{{$instrument_detail->instrument_type_id}},{{$instrument_detail->instrument_type_name}}">{{$instrument_detail->instrument_type_name}}</option>
         @foreach($instrumentTypes as $instrumentType)
         <option value="{{$instrumentType->id}},{{$instrumentType->name}}">{{$instrumentType->name}}</option>
         @endForeach
       </select>
     </div>
   </div>
   <br><br><br>
   <div class="form-group">
    {!! Form::label('Instrument Subtype',null,array('class'=>'col-xs-4 control-label')) !!}

    <div class="col-xs-4">
      <select class="form-control" name="instrument_subtype_id" id ='instSubtype' required>
       <option value="{{$instrument_detail->instrument_subtype_id}},{{$instrument_detail->instrument_subtype_name}}">{{$instrument_detail->instrument_subtype_name}}</option>
     </select>

   </div>
 </div>
 <br><br><br>

 <div class="form-group">
  {!! form::label('Unit',null,array('class'=>'col-xs-4 control-label'))!!}

  <div class="col-xs-4">
    <select class="form-control" name="instrument_unit_id" id ='instUnit' required>
     <option value="{{$instrument_detail->instrument_unit_id}},{{$instrument_detail->instrument_unit_name}}">{{$instrument_detail->instrument_unit_name}}</option>
     @foreach($instrumentUnit as $instUnit)
     <option value="{{$instUnit->id}}">{{$instUnit->name}}</option>
     @endForeach
   </select>
 </div>
</div>

<br><br>
<div class="form-group">
  <div class="col-xs-9"></div>
  {!! form::hidden('token1', csrf_token(),array('id'=>'token'))!!}
  {!! Form::hidden('inst_id', $instrument_detail->instrument_name_id) !!}
 {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default')) !!}
  {!! HTML::link('master/inventory/instrumentmanagement/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}
</div>


{!! Form::close() !!}
</div>

<script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $(document).ready(function(){
    $("#instType").change(function(){

      var instType_id=$(this).val();
      var select = document.getElementById('instSubtype');

      $(select).empty();

      $.ajax({
        method: "POST",
        url: '{{url("instrument_type/")}}'+"/"+instType_id,
        success : function(data){
          /*$("#mat_subtype").val(data);*/
          for (var i in data) {
            $(select).append('<option value=' + data[i]['id'] +','+ data[i]['name'] +'>' + data[i]['name'] + '</option>');
          }

        }

      });


    });

  });


  </script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
