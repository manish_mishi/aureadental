@extends('layouts.masterNav')

@section('title')
        Alerts
@stop

@section('side_bar')


  <div class="sidebar" role="navigation">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <li>
            <a href="{{ url('master/inventory/materialmanagement/listview') }}" >Material Management</a>
          </li>
          
          <li>
            <a href="{{ url('master/inventory/instrumentmanagement/listview') }}" class="active">Instrument Management</a>
          </li>
          
          <li>
            <a href="{{ url('master/inventory/machinemanagement/listview') }}">Machine Management</a>
          </li>
          
          <li>
            <a href="{{ url('master/inventory/gadgetmanagement/listview') }}">Gadget Management</a>
          </li>

          
        </ul>
      </div>
      <!-- /.sidebar-collapse -->
    </div>

@stop

@section('main')


<div id="wrapper">

 <div id="page-wrapper">
    <div class="panel panel-info">
            <ul style="width:100%" class="nav nav-pills">
              <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/instrumentmanagement/edit',[$instrument_id]) }}">Material Detail</a></li>
              <li style="width:33%" role="presentation" class="active"><a href="{{ url('master/inventory/instrumentmanagement/alert/edit',[$instrument_id]) }}">Alerts Settings</a></li>
              <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/instrumentmanagement/treatment/edit',[$instrument_id]) }}">Treatment</a></li>
          </ul>
      </div>

      <div class="row">
          {!! Form::open(array('route' => 'updateInstrumentAlerts','class' => 'form')) !!}
           
            <div class="form-group">
              {!! form::label('Safety Stock ',null,array('class'=>'col-xs-2 control-label'))!!}
         
              <div class="col-xs-2">
                {!! Form::text('Safety_Stock',$Safty_stocks['0']->safety_stock_value,null,array('class'=>'form-control','required')) !!}
            </div>
              </div>

            

            <br><br><br>
            <div class="form-group pull-right">
               {!! Form::hidden('inst_id', $instrument_id) !!}
              {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default')) !!}
               {!! HTML::link('master/inventory/instrumentmanagement/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}
            </div>
          {!! Form::close() !!}
      </div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

 </div>
</div>
<!-- /#wrapper -->

@stop
