@extends('layouts.masterNav')

@section('title')
Inventory -> Instrument
@stop

@section('side_bar')

<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-3">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>

<div class="sidebar">
   <div class="sidebar-nav navbar-collapse" role="navigation" id="bs-sidebar-navbar-collapse">
      <ul class="nav" id="side-menu">
        <li>
          <a href="{{ url('master/inventory/materialmanagement/listview') }}">Material Management</a>
        </li>

        <li>
          <a href="{{ url('master/inventory/instrumentmanagement/listview') }}" class="active">Instrument Management</a>
        </li>

        <li>
          <a href="{{ url('master/inventory/machinemanagement/listview') }}">Machine Management</a>
        </li>

        <li>
          <a href="{{ url('master/inventory/gadgetmanagement/listview') }}">Gadget Management</a>
        </li>
      </ul>
    </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">

 <div id="page-wrapper">

  <!-- successfullye added msg -->
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->
    
    <br>
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label class="control-label">Instrument Type</label>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <select class="form-control" name="category_id" id="material_category_select">
          <option value="0">ALL</option>
          <option value=""></option>
        </select>
      </div>
    </div>

    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label class="control-label">Instrument Sub-type</label>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <span id="material_type_select_div">
          <!-- Material type will get populated over here -->
          <select class="form-control">
            <option>ALL</option>
          </select>
        </span>
      </div>
    </div>

    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <label class="control-label">Instrument Name</label>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <span id="material_subtype_select_div">
          <!-- Material sub type will get populated over here -->
          <select class="form-control">
            <option>ALL</option>
          </select>
        </span>
      </div>
    </div>

  <div>
    <a href="{{ url('master/inventory/instrumentmanagement/add') }}"><button class="btn btn-success pull-right col-xs-3 col-sm-3 col-md-3 col-lg-3"><i class="fa fa-plus fa-fw"></i>Add Instrument</button></a> 
  </div>

  <br><br><br><br>
  <div class="panel panel-default filterable table-responsive" style="margin-top:3%">

    <!-- Table -->
    <table class="table table-hover"> 
      <thead class="panel-info">
        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="Name" disabled></th>
          <th><input type="text" class="form-control" placeholder="Type" disabled></th>
          <th><input type="text" class="form-control" placeholder="Sub-type" disabled></th>
          <th><input type="text" class="form-control" placeholder="Treatmnt-type" disabled></th>
          <th><input type="text" class="form-control" placeholder="Treatmnt-name" disabled></th>
          <th>Action</th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
        </tr> 
      </thead>
      <tbody>
        @foreach($instrument_details as $instrument)
        <tr>
          <td>{{$instrument->instrument_name}}</td>
          <td>{{$instrument->instrument_type_name}}</td>
          <td>{{$instrument->instrument_subtype_name}}</td>
          <td>{{$instrument->treatment_type_name}}</td>
          <td>{{$instrument->name}}</td>
          <td>  

           
            <a href="{{ url('master/inventory/instrumentmanagement/edit',[$instrument->instrument_name_id]) }}" class="btn btn-xs btn-primary"><i class="fa fa-edit fa-fw"></i>Edit</a>
            

          </td> 
          <td>
            {!! Form::open(array('class' => 'form')) !!}
            {!! Form::hidden('inst_id', $instrument->instrument_name_id) !!}
            {!! Form::button('<i class="fa fa-trash fa-fw"></i>Delete',array('type' => 'submit','class'=>'btn btn-xs btn-danger')) !!}
            {!! Form::close() !!}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>             
  </div>
  
<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
