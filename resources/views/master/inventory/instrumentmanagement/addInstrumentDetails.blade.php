@extends('layouts.masterNav')

@section('title')
Instrument Details
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/inventory/materialmanagement/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/instrumentmanagement/listview') }}" class="active">Instrument Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/machinemanagement/listview') }}">Machine Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/gadgetmanagement/listview') }}">Gadget Management</a>
      </li>


    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">

 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:33%" role="presentation" class="active"><a href="{{ url('master/inventory/instrumentmanagement/add') }}">Instrument Detail</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/instrumentmanagement/alert') }}">Alerts Settings</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/instrumentmanagement/treatment') }}">Treatment</a></li>
    </ul>
  </div>
 <!-- successfullye added msg -->
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->
  <div class="row">
    {!! Form::open(array('route'=>'addInstrument','class'=>'form')) !!}

    <br>
    <div class="form-group">
      {!! form::label('Instrument Name',null,array('class'=>'col-xs-4 control-label'))!!}

      <div class="col-xs-4">
         @if(Session::has('instrument_name'))
        <?php $name = Session::get('instrument_name') ?>
        {!! Form::text('inst_nam', $name,array('class'=>'form-control')) !!}
        @else
        {!! Form::text('inst_nam', null,array('class'=>'form-control','required')) !!}
        @endif
      </div>
    </div>   <br><br><br>

     @if(Session::has('instrument_type'))
    <?php $instrument_type_id = Session::get('instrument_type');
      $inst_type_id=Session::get('inst_type_id');
      $inst_type_name=Session::get('inst_type_name');
    ?>
    <div class="form-group">

      {!! Form::label('Instrument Type',null,array('class'=>'col-xs-4 control-label'))!!} 
      <div class="col-xs-4">
        <select class="form-control" name="instrument_type_id" id ='instType' required>
         <option value="{{$inst_type_id}},{{$inst_type_name}}">{{$inst_type_name}}</option>
         @foreach($instrumentTypes as $instrumentType)
         <option value="{{$instrumentType->id}},{{$instrumentType->name}}">{{$instrumentType->name}}</option>
         @endForeach
       </select>
      </div>
    </div>
  @else
    <div class="form-group">
      {!! Form::label('Instrument Type',null,array('class'=>'col-xs-4 control-label'))!!} 
      <div class="col-xs-4">
        <select class="form-control" name="instrument_type_id" id ='instType' required>
         <option>Select</option>
          @foreach($instrumentTypes as $instrumentType)
         <option value="{{$instrumentType->id}},{{$instrumentType->name}}">{{$instrumentType->name}}</option>
         @endForeach
       </select>
      </div>
    </div>
    @endif
    <br><br><br>

     @if(Session::has('instrument_subtype'))
    <?php $instrument_subtypes = Session::get('instrument_subtype');
      $inst_subt_id=Session::get('inst_subt_id');
      $inst_subt_name=Session::get('inst_subt_name');
    ?>
    <div class="form-group">
      {!! Form::label('Instrument Subtype',null,array('class'=>'col-xs-4 control-label')) !!}
      <div class="col-xs-4">
        <select class="form-control" name="instrument_subtype_id" id ='instSubtype' required>
         <option value="{{$inst_subt_id}},{{$inst_subt_name}}">{{$inst_subt_name}}</option>
       </select>
      </div>
    </div>
    @else
    <div class="form-group">
      {!! Form::label('Instrument Subtype',null,array('class'=>'col-xs-4 control-label')) !!}
      <div class="col-xs-4">
        <select class="form-control" name="instrument_subtype_id" id ='instSubtype' required>
         <option>Select</option>
       </select>
      </div>
    </div>
    @endif
    <br><br><br>
    
    @if(Session::has('instrument_unit'))
    <?php $instrument_units = Session::get('instrument_unit');
      $inst_unit_id=Session::get('inst_unit_id');
      $inst_unit_name=Session::get('inst_unit_name');
    ?>
    <div class="form-group">
      {!! form::label('Unit',null,array('class'=>'col-xs-4 control-label'))!!}
      <div class="col-xs-4">
        <select class="form-control" name="instrument_unit_id" id ='instUnit' required>
         <option value="{{$inst_unit_id}},{{$inst_unit_name}}">{{$inst_unit_name}}</option>
          @foreach($instrumentUnit as $instUnit)
         <option value="{{$instUnit->id}},{{$instUnit->name}}">{{$instUnit->name}}</option>
         @endForeach
       </select>
      </div>
    </div>
    @else
    <div class="form-group">
      {!! form::label('Unit',null,array('class'=>'col-xs-4 control-label'))!!}
      <div class="col-xs-4">
        <select class="form-control" name="instrument_unit_id" id ='instUnit' required>
         <option>Select</option>
         @foreach($instrumentUnit as $instUnit)
         <option value="{{$instUnit->id}},{{$instUnit->name}}">{{$instUnit->name}}</option>
         @endForeach
       </select>
      </div>
    </div>
     @endif
    <br><br>
    <div class="form-group">
      <div class="col-xs-9"></div>
       {!! Form::hidden('inst_id', $instrument_id) !!}
      {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default')) !!}
      {!! HTML::link('master/inventory/instrumentmanagement/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}
    </div>


    {!! Form::close() !!}
  </div>

  <script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(document).ready(function(){

var instType_id = $('#instType').val();
if(instType_id != null)
{
  var select = document.getElementById('instSubtype');

    $.ajax({
      method: "POST",
      url: '{{url("instrument_type/")}}'+"/"+instType_id,
      success : function(data){console.log(data);
        /*$("#inst_subtype").val(data);*/
        for (var i in data) {
          $(select).append('<option value=' + data[i]['id'] + data[i]['name'] + '>' + data[i]['name'] + '</option>');
        }

      }

    });
}

  $("#instType").change(function(){

    var inst_type_id=$(this).val();
    var select = document.getElementById('instSubtype');

    $(select).empty();

    $.ajax({
      method: "POST",
      url: '{{url("instrument_type/")}}'+"/"+inst_type_id,
      success : function(data){
        /*$("#inst_subtype").val(data);*/
        for (var i in data) {
          $(select).append('<option value=' + data[i]['id'] +','+ data[i]['name'] +'>' + data[i]['name'] + '</option>');
        }

      }

    });


  });

});


</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
