@extends('layouts.masterNav')

@section('title')
        Inventory->Materal Management
@stop

@section('side_bar')


  <div class="navbar-default sidebar" role="navigation">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
          <li>
            <a href="{{ url('master/inventory/materialmanagement/listview') }}">Material Management</a>
          </li>
          
          <li>
            <a href="{{ url('master/inventory/instrumentmanagement/listview') }}" class="active">Instrument Management</a>
          </li>
          
          <li>
            <a href="{{ url('master/inventory/machinemanagement/listview') }}">Machine Management</a>
          </li>
          
          <li>
            <a href="{{ url('master/inventory/gadgetmanagement/listview') }}">Gadget Management</a>
          </li>

          
        </ul>
      </div>
      <!-- /.sidebar-collapse -->
    </div>

@stop

@section('main')



<div id="wrapper">

 <div id="page-wrapper">
    <div class="panel panel-info">
            <ul style="width:100%" class="nav nav-pills">
              <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/instrumentmanagement/add') }}">Instrument Detail</a></li>
              <li style="width:33%" role="presentation" class="active"><a href="{{ url('master/inventory/instrumentmanagement/alert') }}">Alerts Settings</a></li>
              <li style="width:33%" role="presentation"><a href="{{ url('master/inventory/instrumentmanagement/treatment') }}">Treatment</a></li>
          </ul>
      </div>

      <div class="row">
        {!! Form::open(array('route' => 'addinstrumentalert','class' => 'form')) !!}
        
            <div class="form-group">
              {!! form::label('Safety Stock ',null,array('class'=>'col-xs-2 control-label'))!!}
            </div>

            <div class="col-xs-2">

               @if(Session::has('instrument_name'))
                <?php $SafetyStock = Session::get('instrumentSaftystock') ?>
               {!! Form::text('Safety_Stock', $SafetyStock,array('class'=>'form-control')) !!}
                @else
               {!! Form::text('Safety_Stock', null,array('class'=>'form-control')) !!}
               @endif
              </div>

            

            <br><br><br>
            <div class="form-group pull-right">
          
              {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default')) !!}
              {!! HTML::link('master/inventory/instrumentmanagement/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}
            </div>
        {!! Form::close() !!}
      </div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

 </div>
</div>
<!-- /#wrapper -->

@stop
