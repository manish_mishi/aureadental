@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/inventory/materialmanagement/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/instrumentmanagement/listview') }}">Instrument Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/machinemanagement/listview') }}">Machine Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/gadgetmanagement/listview') }}" class="active">Gadget Management</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')

<div id="wrapper">

 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:50%" role="presentation" class="active"><a href="{{ url('master/inventory/gadgetmanagement/add') }}">Gadget Detail</a></li>
      <li style="width:49%" role="presentation"><a href="{{ url('master/inventory/gadgetmanagement/treatment') }}">Treatment</a></li>
    </ul>
  </div>
  <!-- successfullye added msg -->
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->

  <div class="row">
    {!! Form::open(array('route'=>'addGadget','class'=>'form')) !!}


    <div class="form-group">
      {!! form::label('Gadget Name',null,array('class'=>'col-xs-4 control-label'))!!}
      <div class="col-xs-4">

        <div>
         @if(Session::has('gadget_name'))
         <?php $name = Session::get('gadget_name') ?>
         {!! Form::text('gadget_name', $name,array('class'=>'form-control')) !!}
         @else
         {!! Form::text('gadget_name', null,array('class'=>'form-control','required')) !!}
         @endif
       </div>
     </div>   <br><br><br>

     @if(Session::has('gadget_type_id'))
     <?php $gadgetType = Session::get('gadget_type_id');
     $gadgetType_id=Session::get('gadgetType_id');
     $gadgetType_name=Session::get('gadgetType_name');

     ?>
     <div class="form-group">
      {!! Form::label('Gadget Type',null,array('class'=>'col-xs-4 control-label'))!!} 
      <div class="col-xs-4">
        <select class="form-control" name="gadget_type_id" id ='gdgtType' required>
         <option value="{{$gadgetType_id}},{{$gadgetType_name}}">{{$gadgetType_name}}</option>
         @foreach($gadgetTypes as $gadgetType)
         <option value="{{$gadgetType->id}},{{$gadgetType->name}}">{{$gadgetType->name}}</option>
         @endForeach
       </select>
     </div>
   </div>
   @else
   <div class="form-group">
    {!! Form::label('Gadget Type',null,array('class'=>'col-xs-4 control-label'))!!} 
    <div class="col-xs-4">
      <select class="form-control" name="gadget_type_id" id ='gdgtType' required>
       <option value>Select</option>
       @foreach($gadgetTypes as $gadgetType)
       <option value="{{$gadgetType->id}},{{$gadgetType->name}}">{{$gadgetType->name}}</option>
       @endForeach
     </select>
   </div>
 </div>
 @endif
 <br><br>

 @if(Session::has('gadget_subtype_id'))
 <?php $gadgetSubtype = Session::get('gadget_subtype_id');
 $gadgetSubtype_id=Session::get('gadgetSubtype_id');
 $gadgetSubtype_name=Session::get('gadgetSubtype_name');
 ?>
 <div class="form-group">
  {!! Form::label('Gadget Subtype',null,array('class'=>'col-xs-4 control-label')) !!}
  <div class="col-xs-4">
    <select class="form-control" name="gadget_subtype_id" id ='gdgtSubtype' required>
      <option value="{{$gadgetSubtype_id}},{{$gadgetSubtype_name}}">{{$gadgetSubtype_name}}</option>
    </select>
  </div>
</div>
@else
<div class="form-group">
  {!! Form::label('Gadget Subtype',null,array('class'=>'col-xs-4 control-label')) !!}
  <div class="col-xs-4">
    <select class="form-control" name="gadget_subtype_id" id ='gdgtSubtype' required>
     <option value>Select</option>
   </select>
 </div>
</div>
@endif
<br><br>

<div class="form-group">
  <div class="col-xs-9"></div>
  {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default')) !!}
  {!! HTML::link('master/inventory/gadgetmanagement/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}    
</div>


{!! Form::close() !!}
</div>

<script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(document).ready(function(){
  $("#gdgtType").change(function(){

    var gdgtTypeID=$(this).val();
    var select = document.getElementById('gdgtSubtype');

    $(select).empty();

    $.ajax({
      method: "POST",
      url: '{{url("gadget_type/")}}'+"/"+gdgtTypeID,
      success : function(data){
        /*$("#mat_subtype").val(data);*/
        for (var i in data) {
         $(select).append('<option value=' + data[i]['id'] +','+ data[i]['name'] +'>' + data[i]['name'] + '</option>');
       }

     }

   });


  });

});


</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
