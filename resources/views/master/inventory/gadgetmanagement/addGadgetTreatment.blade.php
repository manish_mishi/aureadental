@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/inventory/materialmanagement/listview') }}">Material Management</a>
      </li>
      <li>
        <a href="{{ url('master/inventory/instrumentmanagement/listview') }}">Instrument Management</a>
      </li>
      <li>
        <a href="{{ url('master/inventory/machinemanagement/listview') }}">Machine Management</a>
      </li>
      <li>
        <a href="{{ url('master/inventory/gadgetmanagement/listview') }}" class="active">Gadget Management</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')
<div id="wrapper">
 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:50%" role="presentation"><a href="{{ url('master/inventory/gadgetmanagement/add') }}">Gadget Detail</a></li>
      <li style="width:49%" role="presentation" class="active"><a href="{{ url('master/inventory/gadgetmanagement/treatment') }}">Treatment</a></li>
    </ul>
  </div>
<div>
  <div class="row">

   {!! Form::open(array('route' => 'addgadgetmanagementtreatment','class' => 'form')) !!}
   <div class="form-group">
    {!! Form::label('Treatment Type', null, array('class'=>'col-xs-2  control-label')) !!}
    <div class="col-md-2">
      <select class="form-control" name="treatment_type_id" id ='treatmentType'>
       <option value>Select</option>
       @foreach($treatmentType as $treatType)
       <option value="{{$treatType->id}},{{$treatType->name}}">{{$treatType->name}}</option>
       @endForeach
     </select>
   </div>
 </div>
   
<div class="form-group">
  {!! Form::label('Treatment Name', null, array('class'=>'col-xs-2  control-label')) !!}
  <div class="col-md-2">
    <select class="form-control" name="treatment_name_id" id ='treatmentName'>
     <option value>Select</option>
   </select>
  </div>
  <a href="#" class="btn btn-info btn-md col-md-1" onClick="duplicateTreatmentTypeName()">
    <span class="glyphicon glyphicon-plus"></span> 
  </a>
</div>
</div>
  <div class="row " id="duplicater" style="display:none;">
<br>
<div class="col-xs-2 col-lg-2 col-md-2">
</div>
<div class="col-xs-2 col-lg-2 col-md-2">
{!! form::text(null,null,array('id'=>'treatment_type_div','class'=>'form-control','required','readonly'))!!}

{!! form::hidden(null,null,array('id'=>'treatment_type_id','class'=>'form-control type','required','hidden'))!!}

</div>
<div class="col-xs-2 col-lg-2 col-md-2">
</div>
<div class="col-xs-2 col-lg-2 col-md-2 ">
{!! form::text(null,null,array('id'=>'treatment_name_div','class'=>'form-control type','required','readonly'))!!}

{!! form::hidden(null,null,array('id'=>'treatment_name_id','class'=>'form-control name','required','hidden'))!!}


</div> 
<div class="col-xs-1 col-lg-1 col-md-1" id="delete">
{!! Form::button('Delete',array('id' =>'delete_btn','class'=>'btn btn-xs btn-danger','style'=>'visibility:hidden','onClick'=>'removeduplicate(this)')) !!}
</div>
</div>
</div>
<br>

<div class="form-group pull-right">
  {!! Form::hidden('gdgt_id', $gadget_id) !!}
  {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning')) !!}
  {!! HTML::link('master/inventory/machinemanagement/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}
</div>

{!! Form::close() !!}
</div>

<script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

var i = 0;

$(document).ready(function(){

  $("#treatmentType").change(function(){

$('#treatment_type_div').val($('option:selected',this).text());
$('#treatment_type_id').val($(this).val());


    var treat_type_id=$(this).val();
    var select = document.getElementById('treatmentName');

    $(select).empty();

    $.ajax({
      method: "POST",
      url: '{{url("treatment_type/")}}'+"/"+treat_type_id,
      success : function(data){
        /*$("#mat_subtype").val(data);*/
        for (var i in data) {
          $(select).append('<option value=' + data[i]['id'] +','+ data[i]['name'] +'>' + data[i]['name'] + '</option>');
        }
          assignTreatmentName();
      }

    });


  });

});

function assignTreatmentName()
{ 
$('#treatment_name_div').val($('option:selected','#treatmentName').text());
$('#treatment_name_id').val($('#treatmentName').val());

}



var i = 0;

function duplicateTreatmentTypeName() {

var treatmentType = document.getElementById('treatment_type_div').value;
var treatmentName = document.getElementById('treatment_name_div').value;
var treatmentTypeID=document.getElementById('treatment_type_id').value;
var treatmentNameID=document.getElementById('treatment_name_id').value;

if(treatmentType!=''&& treatmentTypeID!=''){

document.getElementById('delete_btn').style.visibility = "visible";
var original = document.getElementById('duplicater');
original.style.display = "block";

document.getElementById('treatment_type_div').value =treatmentType;
document.getElementById('treatment_type_id').value =treatmentTypeID;


document.getElementById('treatment_name_div').value =treatmentName;
document.getElementById('treatment_name_id').value =treatmentNameID;


var clone = original.cloneNode(true); // "deep" clone
clone.id = "duplicater" + ++i; // there can only be one element with an ID
original.parentNode.appendChild(clone);
document.getElementById('delete_btn').style.visibility = "hidden";
original.style.display = "none";

document.getElementById('treatmentType').value="";
document.getElementById('treatmentName').value="";
document.getElementById('treatment_type_id').value ="";
document.getElementById('treatment_name_id').value ="";


var treatmentTypeElement = document.getElementById("duplicater"+i).getElementsByClassName('type')[0];
treatmentTypeElement.setAttribute("name","treatment_type_id[]");

var treatmentNameElement = document.getElementById("duplicater"+i).getElementsByClassName('name')[0];
treatmentNameElement.setAttribute("name","treatment_name_id[]");

}

}
function removeduplicate(element)
{
element=element.parentNode.parentNode;//gets the id of the parent
element.parentNode.removeChild(element);
}
</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>

<!-- /#wrapper -->

@stop
