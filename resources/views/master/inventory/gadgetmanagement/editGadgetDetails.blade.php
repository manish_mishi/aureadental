@extends('layouts.masterNav')

@section('title')
Inventory->Materal Management
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('master/inventory/materialmanagement/listview') }}">Material Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/instrumentmanagement/listview') }}">Instrument Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/machinemanagement/listview') }}">Machine Management</a>
      </li>

      <li>
        <a href="{{ url('master/inventory/gadgetmanagement/listview') }}" class="active">Gadget Management</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

@stop

@section('main')


<div id="wrapper">

 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:50%" role="presentation" class="active"><a href="{{ url('master/inventory/gadgetmanagement/edit',[$gadget_detail->gadget_name_id]) }}">Gadget Detail</a></li>
      <li style="width:49%" role="presentation"><a href="{{ url('master/inventory/gadgetmanagement/treatment/edit',[$gadget_detail->gadget_name_id]) }}">Treatment</a></li>
    </ul>
  </div>

  <div class="row">
    {!! Form::open(array('route'=>'updateGadgetDetails','class'=>'form')) !!}
    <div class="form-group">
      {!! form::label('Gadget Name',null,array('class'=>'col-xs-4 control-label'))!!}

      <div class="col-xs-4">
        {!! form::text('gadget_name',$gadget_detail->gadget_name,array('class'=>'form-control','required'))!!}

      </div>
    </div>   <br><br><br>

    <div class="form-group">
      {!! Form::label('Gadget Type',null,array('class'=>'col-xs-4 control-label'))!!} 
      <div class="col-xs-4">
        <select class="form-control" name="gadget_type_id" id ='gdgtType' required>
         <option value="{{$gadget_detail->gadget_type_id}},{{$gadget_detail->gadget_type_name}}">{{$gadget_detail->gadget_type_name}}</option>
         @foreach($gadgetTypes as $gadgetType)
         <option value="{{$gadgetType->id}},{{$gadgetType->name}}">{{$gadgetType->name}}</option>
         @endForeach
       </select>
     </div>
   </div>
   <br><br><br>
   <div class="form-group">
    {!! Form::label('Gadget Subtype',null,array('class'=>'col-xs-4 control-label')) !!}

    <div class="col-xs-4">
      <select class="form-control" name="gadget_subtype_id" id ='gdgtSubtype' required>
       <option value="{{$gadget_detail->gadget_subtype_id}},{{$gadget_detail->gadget_subtype_name}}">{{$gadget_detail->gadget_subtype_name}}</option>
     </select>

    </div>
  </div>


  <br><br>
  <div class="form-group">
    <div class="col-xs-9"></div>
    {!! form::hidden('token1', csrf_token(),array('id'=>'token'))!!}
    {!! Form::hidden('gdgt_id', $gadget_detail->gadget_name_id) !!}
    {!! Form::submit('&#x2714; Next',array('class'=>'btn btn-default')) !!}
    {!! HTML::link('master/inventory/gadgetmanagement/listview', '&#10006; Cancel', array('id' => 'cancel','class'=>'btn btn-danger')) !!}    
  </div>


  {!! Form::close() !!}
</div>

<script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(document).ready(function(){
  $("#gdgtType").change(function(){

    var gdgtTypeID=$(this).val();
    var select = document.getElementById('gdgtSubtype');

    $(select).empty();

    $.ajax({
      method: "POST",
      url: '{{url("gadget_type/")}}'+"/"+gdgtTypeID,
      success : function(data){
        /*$("#mat_subtype").val(data);*/
        for (var i in data) {
          $(select).append('<option value=' + data[i]['id'] +','+ data[i]['name'] +'>' + data[i]['name'] + '</option>');
        }

      }

    });


  });

});


</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
<!-- /#wrapper -->

@stop
