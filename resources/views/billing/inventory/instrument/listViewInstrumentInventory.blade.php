@extends('layouts.menuNav')

@section('title')
Billing -> Inventory -> Instrument
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}">Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}"  class="active">Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}">Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment') }}">Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}">Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="panel panel-info">
    <ul class="nav nav-pills">
      <li style="width:25%" role="presentation"><a href="{{ url('billing/inventory/material') }}">Material</a></li>
      <li style="width:25%" role="presentation" class="active"><a href="{{ url('billing/inventory/instrument') }}">Instrument</a></li>
      <li style="width:24%" role="presentation"><a href="{{ url('billing/inventory/gadget') }}">Gadget</a></li>
      <li style="width:25%" role="presentation"><a href="{{ url('billing/inventory/machine') }}">Machine</a></li>
    </ul>
  </div>

  <div class="row">
    {!! Form::label('Vendor Name', null, array('class'=>'col-xs-2 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('vendor_name',null,array('class'=>'form-control','required')) !!}

    </div>
    <a href="#" class="btn btn-success  col-xs-1">Go</a>
    {!! Form::label('Billing Status', null, array('class'=>'col-xs-2 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('billing_status',null,array('class'=>'form-control','required')) !!}
    </div>
  </div>

  <br>
  <div class="panel panel-default filterable">
   <table class="table">
    <thead>
      <tr><th class="panel panel-default"></th></tr>
      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="PO Number" disabled></th>
        <th><input type="text" class="form-control" placeholder="Vendor Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Expected Delivery Date" disabled></th>
        <th><input type="text" class="form-control" placeholder="Delivery Date" disabled></th>
        <th><input type="text" class="form-control" placeholder="Chalan Number" disabled></th>
        <th><input type="text" class="form-control" placeholder="Vendor Bill No" disabled></th>
        <th><input type="text" class="form-control" placeholder="Bill Amount" disabled></th>
        <th><input type="text" class="form-control" placeholder="Pending Amount" disabled></th>
        <th></th>
        <th></th>



        <!-- <th><button class="btn btn-default btn-xs btn-filter">Filter</button></th> -->
      </tr>
    </thead>
    <tbody>


      <tr>

        <td>  </td>
        <td>  </td>
        <td> </td>
        <td>  </td>
        <td>  </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><a href="{{url('billing/inventory/instrument/makepayment')}}" class="btn btn-xs btn-primary"><i class="fa fa-usd fa-fw"></i>Make Payment</a></td>
      </tr>
    </tbody>
  </table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop