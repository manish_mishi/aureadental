@extends('layouts.menuNav')

@section('title')
Billing -> Inventory -> Gadget -> Payment
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}" >Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}"class="active">Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}">Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment') }}">Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}">Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
 <div id="page-wrapper">

  <form>
    <br>
    <div class="panel panel-info">
      <ul class="nav nav-pills">
        <li style="width:25%" role="presentation"><a href="{{ url('billing/inventory/material') }}">Material</a></li>
        <li style="width:24%" role="presentation"><a href="{{ url('billing/inventory/instrument') }}">Instrument</a></li>
        <li style="width:25%" role="presentation" class="active"><a href="{{ url('billing/inventory/gadget') }}">Gadget</a></li>
        <li style="width:25%" role="presentation"><a href="{{ url('billing/inventory/machine') }}">Machine</a></li>
      </ul>
    </div>

    <div class="row">
      {!! Form::label('Vendor Name', null, array('class'=>'col-xs-2 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('vendor_name',null,array('class'=>'form-control','readonly')) !!}
      </div>
    </div>

    <br> 
    <div class="row">
      {!! Form::label('Bill Number', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::label($billNumber, null,array('class'=>'control-label')) !!}
        {!! Form::hidden('bill_number',$billNumber) !!}
      </div>
      {!! Form::label('Bill Status', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        <select class="form-control" name="billing_status_id" id ='billingStatusId'>
         <option value>Select</option>
         @foreach($billingStatus as $bill)
         <option value="{{$bill->id}}">{{$bill->status}}</option>
         @endForeach
       </select>
     </div>
   </div><br>

   <div class="row">
    {!! Form::label('Date of Billing', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dateOfBilling" class="form-control" name="date_of_billing">
    </div>
    {!! Form::label('Date of Payment', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dateOfPayment" class="form-control" name="date_of_payment">
    </div>
  </div><br>

  <div class="row">
    <label class="col-xs-3">Mode of Payment:</label>
    <div class="col-xs-3">
      <select class="form-control" name="payment_mode_id" id ='paymentMode'>
       <option value>Select</option>
       @foreach($paymentMode as $mode)
       <option value="{{$mode->id}}">{{$mode->payment_mode}}</option>
       @endForeach
     </select>
   </div>
   {!! Form::label('Notes', null, array('class'=>'col-xs-3 control-label')) !!}
   <div class="col-xs-3">
    {!! Form::text('notes',null,array('class'=>'form-control')) !!}
  </div>
</div><br>
<div class="row">
  {!! Form::label('From Bank', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    {!! Form::text('from_bank',null,array('class'=>'form-control')) !!}
  </div>
  {!! Form::label('To Bank', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    {!! Form::text('to_bank',null,array('class'=>'form-control')) !!}
  </div>
</div></br>
<div class="row">
  {!! Form::label('Pending Amount', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    <span class="atend">Rs.</span>
    {!! Form::text('pending_amount',null,array('class'=>'form-control rs-input','maxlength'=>"20")) !!}
  </div>
  {!! Form::label('Final Amount', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    <span class="atend">Rs.</span>
    {!! Form::text('final_amount',null,array('class'=>'form-control rs-input','maxlength'=>"20")) !!}
  </div>
</div>
<br><br>
<div class="row">
  <div>
    <button type="submit" class="btn btn-primary" style="margin-left:81%"><i class="fa fa-usd fa-fw"></i>Pay</button>
    <a href="{{ url('billing/inventory/gadget') }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
  </div>
</div><br>

</form>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop