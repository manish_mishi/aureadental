@extends('layouts.menuNav')

@section('title')
Billing -> Salary -> Listview
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

     <li>
      <a href="{{ url('billing/treatment') }}" >Treatment</a>
    </li>

    <li>
      <a href="{{ url('billing/inventory/material') }}">Inventory</a>
    </li>

    <li>
      <a href="{{ url('billing/labwork') }}">Lab Work</a>
    </li>

    <li>
      <a href="{{ url('billing/maintenance/contractpayment')}}">Maintenance</a>
    </li>
    <li>
      <a href="{{ url('billing/salary') }}"class="active">Salary</a>
    </li>

    <li>
      <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
    </li>

    <li>
      <a href="{{ url('billing/miscellaneous') }}">Miscellaneous</a>
    </li>

  </ul>
</div>
<!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
 <div id="page-wrapper">

  <br>

  <a href="{{ url('billing/salary/addDetails') }}" class="btn btn-success pull-right col-xs-3"><i class="fa fa-plus fa-fw"></i>Generate Salary</a>
  
  <br><br>
  <div class="panel panel-default table-responsive filterable">
   <table class="table table-hover">
    <thead class="panel-info">

      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="Month-Year" disabled></th>
        <th><input type="text" class="form-control" placeholder="Date of Payment" disabled></th>
        <th><input type="text" class="form-control" placeholder="No of Leaves" disabled></th>
        <th><input type="text" class="form-control" placeholder="Amount Paid" disabled></th>
        <th><input type="text" class="form-control" placeholder="Advance Amount" disabled></th>
        <th>Action</th>
        <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
      </tr>
    </thead>
    <tbody>
      @foreach($salaryDetails as $salary)
      <tr>
        <td>{{$salary->month_year}}</td>
        <td>{{$salary->date_of_payment}}</td>
        <td>{{$salary->leaves}}</td>
        <td>{{$salary->net_amount}}</td>
        <td>{{$salary->advance_amount}}</td>
        <td colspan="2"><a href="{{url('billing/salary/view',[$salary->id])}}"><button class="btn btn-xs .btn-primary">View Details</button></a></td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop