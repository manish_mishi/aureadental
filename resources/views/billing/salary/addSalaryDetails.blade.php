@extends('layouts.menuNav')

@section('title')
Billing -> Salary -> Add
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}">Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}">Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}">Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment') }}">Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}" class="active">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}">Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  {!! Form::open(array('route'=>'storeSalaryDetails','class'=>'form')) !!}

  <br>
  <div class="row">
    {!! Form::label('Staff Type', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <select class="form-control" name="staff_type_id" id ='staffTypeId'>
       <option value>Select</option>
       <option value="1">Doctor</option>
       <option value="2">Nurse</option>
     </select>
   </div>
   {!! Form::label('Staff Name', null, array('class'=>'col-xs-3 control-label')) !!}
   <div class="col-xs-3">
    <select class="form-control" name="staff_name_id" id ='staffNameId'>
     <option value>Select</option>
   </select>
 </div>
</div>

<br><br>

<div class="col-md-6 col-sm-6 col-xs-6">
  <div class="row">
    {!! Form::label('Enter Month-Year', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
    <div class="col-xs-6 col-sm-6 col-xs-6">
      <input data-provide="datepicker" data-date-format="mm-yyyy" id="date" class="form-control" name="date">
    </div>
  </div> 
  
  <br>
  <div class="row">
    {!! Form::label('Date of Payment', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
    <div class="col-xs-6 col-sm-6 col-xs-6">
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dateOfPayment" class="form-control" name="date_of_payment">
    </div>
  </div>

  <br>
  <div class="row">
    {!! Form::label('Salary', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
    <div class="col-xs-6 col-sm-6 col-xs-6">
      <span class="atend">Rs.</span>
      {!! Form::text('salary',null,array('id'=>'salary','class'=>'form-control rs-input','maxlength'=>"20")) !!}
    </div>
  </div>

  <br><br><br><br>
  <div class="row">
    {!! Form::label('Mode of Payment', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
    <div class="col-xs-6 col-sm-6 col-xs-6">
      <select class="form-control" name="payment_mode_id" id ='paymentMode'>
       <option value>Select</option>
       @foreach($paymentMode as $mode)
       <option value="{{$mode->id}}">{{$mode->payment_mode}}</option>
       @endForeach
     </select>
   </div>
 </div>

 <br>
 <div class="row">
  {!! Form::label('Bank Name', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
  <div class="col-xs-6 col-sm-6 col-xs-6">
    {!! Form::text('bank_name',null,array('class'=>'form-control')) !!}
  </div>
</div>

<br>
<div class="row">
  {!! Form::label('Account Holder', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
  <div class="col-xs-6 col-sm-6 col-xs-6">
    {!! Form::text('account_holder',null,array('class'=>'form-control')) !!}
  </div>
</div>

</div>

<div class="col-md-6 col-sm-6 col-xs-6">

  <div class="rbox">
    <p style="font-size: 170%;" align="center"><u>Deduction</u></p>
    <div class="row">
      {!! Form::label('Advance Amount', null, array('class'=>'col-md-6 col-sm-6 col-xs-6 control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-xs-6">
        <span class="atend">Rs.</span>
        {!! Form::text('advance_amount',null,array('id'=>'advanceAmount','class'=>'form-control rs-input','maxlength'=>"20")) !!}
      </div>
    </div>

    <br> 
    <div class="row">
      {!! Form::label('Leaves', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-xs-6">
        {!! Form::text('leave',null,array('class'=>'form-control')) !!}
      </div>
    </div>

    <br>
    <div class="row">
      {!! Form::label('Total Deduction', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-xs-6">
        <span class="atend">Rs.</span>
        {!! Form::text('total_deduction',null,array('id'=>'totalDeduction','class'=>'form-control rs-input','maxlength'=>"20")) !!}
      </div>
    </div>

    <br>
    <div class="row">
     {!! Form::label('Net Amount', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
     <div class="col-xs-6 col-sm-6 col-xs-6">
      <span class="atend">Rs.</span>
      {!! Form::text('net_amount',null,array('id'=>'netAmount','class'=>'form-control rs-input','readonly')) !!}
    </div>
  </div>
</div>

<br>
<div class="row">
  {!! Form::label('Account No', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
  <div class="col-xs-6 col-sm-6 col-xs-6">
    {!! Form::text('account_no',null,array('class'=>'form-control')) !!}
  </div>
</div>

<br>
<div class="row">
  {!! Form::label('Notes', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
  <div class="col-xs-6 col-sm-6 col-xs-6">
    {!! Form::text('notes',null,array('class'=>'form-control')) !!}
  </div>
</div>

<br><br>
<div>
  <button type="submit" class="btn btn-primary" style="margin-left:58%"><i class="fa fa-usd fa-fw"></i>Pay</button>
  <a href="{{ url('billing/salary') }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
</div>

</div>

{!! Form::close() !!}

<script type="text/javascript">
$(document).ready(function(){

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $("#staffTypeId").change(function(){

    var staffTypeId=$(this).val();
    var select = document.getElementById('staffNameId');

    if(staffTypeId != "")
    {
      if(staffTypeId == 1)
      {
        $(select).empty().append("<option value>Select</option>");
        $(select).append("<option>Ravi</option>");
      }

      if(staffTypeId == 2)
      {
        $(select).empty().append("<option value>Select</option>");
        $(select).append("<option>Taylor</option>");  
      }
      
    }
    else
    {
      $(select).empty().append("<option value>Select</option>"); 
    }
    

/*    $.ajax({
      method: "POST",
      url: '{{url("staff_type/")}}'+"/"+staffTypeId,
      success : function(data){
        for (var i in data) {
          $(select).append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
        }

      }

    });*/


});

  $("#totalDeduction").change(function () {

    $totalDeduction = parseFloat($(this).val());
    $salary = parseFloat($("#salary").val());
    $advanceAmount = parseFloat($("#advanceAmount").val());
    
    if(!$.isNumeric($totalDeduction))
    {
      $totalDeduction = 0;
    }

    if(!$.isNumeric($salary))
    {
      $salary = 0;
    }

    if(!$.isNumeric($advanceAmount))
    {
      $advanceAmount = 0;
    }

    $netAmount = ($salary + $advanceAmount) - $totalDeduction;

    $("#netAmount").val($netAmount);

  });

});


</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop