@extends('layouts.menuNav')

@section('title')
Billing -> Salary -> View
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}">Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}">Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}">Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment') }}">Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}" class="active">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}">Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="row">
    {!! Form::label('Staff Type', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('staff_type','Doctor',array('class'=>'form-control', 'readonly')) !!}
    </div>
    {!! Form::label('Staff Name', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('staff_name','Ravi',array('class'=>'form-control', 'readonly')) !!}
    </div>
  </div>

  <br><br>

  <div class="col-md-6 col-sm-6 col-xs-6">
    <div class="row">
      {!! Form::label('Enter Month-Year', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-xs-6">
        {!! Form::text('date',$salaryDetails->month_year,array('class'=>'form-control', 'readonly')) !!}
      </div>
    </div> 

    <br>
    <div class="row">
      {!! Form::label('Date of Payment', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-xs-6">
        {!! Form::text('date_of_payment',$salaryDetails->date_of_payment,array('class'=>'form-control', 'readonly')) !!}
      </div>
    </div>

    <br>
    <div class="row">
      {!! Form::label('Salary', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-xs-6">
        <span class="atend">Rs.</span>
        {!! Form::text('salary',$salaryDetails->salary,array('class'=>'form-control rs-input','maxlength'=>"20",'readonly')) !!}
      </div>
    </div>

    <br><br><br><br>
    <div class="row">
      {!! Form::label('Mode of Payment', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-xs-6">
        {!! Form::text('payment_mode',$salaryDetails->payment_mode,array('class'=>'form-control', 'readonly')) !!}
      </div>
    </div>

    <br>
    <div class="row">
      {!! Form::label('Bank Name', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-xs-6">
        {!! Form::text('bank_name',$salaryDetails->bank_name,array('class'=>'form-control','readonly')) !!}
      </div>
    </div>

    <br>
    <div class="row">
      {!! Form::label('Account Holder', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
      <div class="col-xs-6 col-sm-6 col-xs-6">
        {!! Form::text('account_holder',$salaryDetails->account_holder,array('class'=>'form-control','readonly')) !!}
      </div>
    </div>

  </div>

  <div class="col-md-6 col-sm-6 col-xs-6">

    <div class="rbox">
      <p style="font-size: 170%;" align="center"><u>Deduction</u></p>
      <div class="row">
        {!! Form::label('Advance Amount', null, array('class'=>'col-md-6 col-sm-6 col-xs-6 control-label')) !!}
        <div class="col-xs-6 col-sm-6 col-xs-6">
          <span class="atend">Rs.</span>
          {!! Form::text('advance_amount',$salaryDetails->advance_amount,array('class'=>'form-control rs-input','maxlength'=>"20",'readonly')) !!}
        </div>
      </div>

      <br> 
      <div class="row">
        {!! Form::label('Leaves', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
        <div class="col-xs-6 col-sm-6 col-xs-6">
          {!! Form::text('leave',$salaryDetails->leaves,array('class'=>'form-control','readonly')) !!}
        </div>
      </div>

      <br>
      <div class="row">
        {!! Form::label('Total Deduction', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
        <div class="col-xs-6 col-sm-6 col-xs-6">
          <span class="atend">Rs.</span>
          {!! Form::text('total_deduction',$salaryDetails->total_deduction,array('class'=>'form-control rs-input','maxlength'=>"20",'readonly')) !!}
        </div>
      </div>

      <br>
      <div class="row">
       {!! Form::label('Net Amount', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
       <div class="col-xs-6 col-sm-6 col-xs-6">
        <span class="atend">Rs.</span>
        {!! Form::text('net_amount',$salaryDetails->net_amount,array('class'=>'form-control rs-input','maxlength'=>"20",'readonly')) !!}
      </div>
    </div>
  </div>

  <br>
  <div class="row">
    {!! Form::label('Account No', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
    <div class="col-xs-6 col-sm-6 col-xs-6">
      {!! Form::text('account_no',$salaryDetails->account_no,array('class'=>'form-control','readonly')) !!}
    </div>
  </div>

  <br>
  <div class="row">
    {!! Form::label('Notes', null, array('class'=>'col-xs-6 col-sm-6 col-xs-6 control-label')) !!}
    <div class="col-xs-6 col-sm-6 col-xs-6">
      {!! Form::text('notes',$salaryDetails->notes,array('class'=>'form-control','readonly')) !!}
    </div>
  </div>

  <br><br>
  <div>
    <a href="{{ url('billing/salary') }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
  </div>

</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop