@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}">Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}">Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}"  >Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment') }}" class="active">Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}">Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <form>
    <div class="panel panel-info">
      <ul class="nav nav-pills">
        <li style="width:49%" role="presentation" ><a href="{{ url('billing/maintenance/contractpayment') }}">Contract Payment</a></li>
        <li style="width:50%" role="presentation" class="active" ><a href="{{ url('billing/maintenance/serviceamount') }}">Service Amount</a></li>
      </ul>
    </div>

    <div class="row">
      {!! Form::label('Vendor Type', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('vendor_type',null,array('class'=>'form-control','required')) !!}

      </div>
      {!! Form::label('Vendor Name', null, array('class'=>'col-xs-2 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('vendor_name',null,array('class'=>'form-control','required')) !!}

      </div>

    </div><br><br> 
    <div class="row">
      {!! Form::label('Bill Number', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('bill_number',null,array('class'=>'form-control')) !!}
      </div>
      {!! Form::label('Bill Status', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('bill_status',null,array('class'=>'form-control')) !!}
      </div>
    </div><br>

    <div class="row">
      {!! Form::label('Date of Billing', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('date_of_billing',null,array('class'=>'form-control')) !!}
      </div>
      {!! Form::label('Date of Payment', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('date_of_payment',null,array('class'=>'form-control')) !!}
      </div>
    </div><br>

    <div class="row">
      {!! Form::label('Mode of Payment', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('mode_of_payment',null,array('class'=>'form-control')) !!}
      </div>
      {!! Form::label('Extra Charges', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('extra_charges',null,array('class'=>'form-control')) !!}
      </div>
    </div><br>
    {!! Form::label('Bank Name', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('bank_name',null,array('class'=>'form-control')) !!}
    </div>

    <br>
    <div class="row">
      {!! Form::label('Account No', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('account_no',null,array('class'=>'form-control')) !!}
      </div>
      {!! Form::label('Account Holder', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('account_holder',null,array('class'=>'form-control')) !!}
      </div>
    </div></br>
    <div class="row">
      {!! Form::label('Pending Amount', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('pending_amount',null,array('class'=>'form-control')) !!}
      </div>
      {!! Form::label('Final Amount', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('pending_amount',null,array('class'=>'form-control')) !!}
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-xs-12">
        <button type="submit" class="btn btn-primary pull-right">Pay</button>
      </div>
    </div><br>

  </form>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop