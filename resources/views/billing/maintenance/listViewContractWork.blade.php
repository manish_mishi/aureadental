@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}">Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}"  >Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}">Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment') }}" class="active">Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}">Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <form>
   <div class="panel panel-info">
    <ul class="nav nav-pills">
      <li style="width:50%" role="presentation" class="active"><a href="{{ url('billing/maintenance/contractpayment') }}">Contract Payment</a></li>
      <li style="width:49%" role="presentation"  ><a href="{{ url('billing/maintenance/serviceamount') }}">Service Amount</a></li>
    </ul>
  </div>

  <div class="row">
    <label class="col-xs-2" align="right">Sort By:</label>
    <div class="col-xs-2">
      <select class=" form-control">
        <option value="Paid">Paid</option>
        <option value="Pending">Pending</option>
      </select>

    </div>

  </div>

  <br>
  <div class="panel panel-default filterable">
   <table class="table">
    <thead>
      <tr><th class="panel panel-default"></th></tr>
      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="" disabled>Vendor Names</th>
        <th><input type="text" class="form-control" placeholder="" disabled>Contract Start Date</th>
        <th><input type="text" class="form-control" placeholder="" disabled>Contract End Date</th>
        <th><input type="text" class="form-control" placeholder="" disabled>Contract Amount</th>
        <th><input type="text" class="form-control" placeholder="" disabled>Pending Balance</th>
        <th><input type="text" class="form-control" placeholder="" disabled>Payment Status</th>
        <th><input type="text" class="form-control" placeholder="" disabled></th>
        <th><input type="text" class="form-control" placeholder="" disabled></th>
        <th></th>
        <th></th>



        <!-- <th><button class="btn btn-default btn-xs btn-filter">Filter</button></th> -->
      </tr>
    </thead>
    <tbody>


      <tr>

        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><a href="{{url('billing/maintenance/contractpayment/makepayment')}}">Make Payment</a></td>
      </tr>
    </tbody>
  </table>

</div>
</form>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop