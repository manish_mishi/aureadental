@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}">Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}">Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}"  >Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment') }}" >Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}" class="active">Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br><br>

  {!! Form::open(array('route' => 'storeMiscellaneous','class' => 'form')) !!}
  <div class="row">
    {!! Form::label('Date of Payment', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dob" class="form-control" name="dop">

    </div>
    {!! Form::label('Reason', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
     <textarea rows="3" cols="10" class="form-control" name="reason">
     </textarea>
   </div>

 </div><br> 
 <div class="row">
  {!! Form::label('Paid To', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    {!! Form::text('paid_to',null,array('class'=>'form-control')) !!}
  </div>
  {!! Form::label('Amount', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    {!! Form::text('amount',null,array('class'=>'form-control')) !!}
  </div>
</div><br>
<div class="rbox">
 <div class="row">
  {!! Form::label('Mode of Payment', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    <select class="form-control" name="payment_mode_id" id ='paymentMode'>
     <option value>Select</option>
     @foreach($paymentMode as $mode)
     <option value="{{$mode->id}}">{{$mode->payment_mode}}</option>
     @endForeach
   </select>
 </div>
 {!! Form::label('Bank Name', null, array('class'=>'col-xs-3 control-label')) !!}
 <div class="col-xs-3">
  {!! Form::text('bank_name',null,array('class'=>'form-control')) !!}
</div>
</div><br>

<div class="row">
  {!! Form::label('Account Holder', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    {!! Form::text('account_holder',null,array('class'=>'form-control')) !!}
  </div>
  {!! Form::label('Account No', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    {!! Form::text('account no',null,array('class'=>'form-control')) !!}
  </div>
</div><br>
<div class="row">
  <label class="col-xs-3">Notes:</label>
  <div class="col-xs-3">
    {!! Form::text('notes',null,array('class'=>'form-control')) !!}
  </div>
</div>
</div>
<br><br>
<div>
  <a href="{{ url('billing/miscellaneous') }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
  &nbsp;
  <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-usd fa-fw"></i>Pay</button>
</div>


<br>
</form>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop