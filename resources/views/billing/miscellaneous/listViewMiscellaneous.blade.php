@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}">Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}"  >Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}">Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment') }}">Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}" class="active">Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <form><br><br>
<div>
      <a href="{{ url('billing/miscellaneous/addDetails') }}" class="btn btn-success pull-right"><i class="fa fa-plus fa-fw"></i>Add Miscellaneous</a>  
    </div>
  <br><br>
  <div class="panel panel-default filterable">
   <table class="table">
    <thead>
      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="Date of Payment" disabled></th>
        <th><input type="text" class="form-control" placeholder="Paid To" disabled></th>
        <th><input type="text" class="form-control" placeholder="Amount" disabled></th>
        <th><input type="text" class="form-control" placeholder="Mode of Payment" disabled></th>
        <th><input type="text" class="form-control" placeholder="Bank Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Account Holder" disabled></th>
        <th><input type="text" class="form-control" placeholder="Account Number" disabled></th>
        <th><input type="text" class="form-control" placeholder="Notes" disabled></th>
        <th><input type="text" class="form-control" placeholder="Reason" disabled></th>
        <th>Action</th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>



        <!-- <th><button class="btn btn-default btn-xs btn-filter">Filter</button></th> -->
      </tr>
    </thead>
    <tbody>

@foreach($miscellaneous_details as $miscellaneous_detail)
      <tr>

        <td>{{$miscellaneous_detail->date_of_payment}}</td>
        <td>{{$miscellaneous_detail->paid_to}}</td>
        <td>{{$miscellaneous_detail->amount}}</td>
        <td>{{$miscellaneous_detail->payment_mode}}</td>
        <td>{{$miscellaneous_detail->bank_name}}</td>
        <td>{{$miscellaneous_detail->account_holder}}</td>
        <td>{{$miscellaneous_detail->account_number}}</td>
        <td>{{$miscellaneous_detail->account_holder}}</td>
        <td>{{$miscellaneous_detail->reason}}</td>
       <td> {!! Form::button('<i class="fa fa-trash fa-fw"></i>Delete',array('type' => 'submit','class'=>'btn btn-xs btn-danger')) !!}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>
</form>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop