@extends('layouts.menuNav')

@section('title')
Gadget->Maintenance->Assigned Maintenance

@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

     <li>
      <a href="{{ url('billing/treatment') }}">Treatment</a>
    </li>

    <li>
      <a href="{{ url('billing/inventory/material') }}">Inventory</a>
    </li>

    <li>
      <a href="{{ url('billing/labwork') }}"  >Lab Work</a>
    </li>

    <li>
      <a href="{{ url('billing/maintenance/contractpayment') }}" >Maintenance</a>
    </li>
    <li>
      <a href="{{ url('billing/salary') }}">Salary</a>
    </li>

    <li>
      <a href="{{ url('billing/cashmanagement') }}"class="active">Cash Management</a>
    </li>

    <li>
      <a href="{{ url('billing/miscellaneous') }}">Miscellaneous</a>
    </li>

  </ul>
</ul>
</div>
<!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    <label class="col-xs-2">Available Cash:</label>
    <div class="col-xs-2"><input type="number" class="form-control" name="patient_treaent_id">
    </div>

    <a href="{{url('billing/cashmanagement/transfercash')}}" class="btn btn-success pull-right col-xs-2"><i class="fa fa-plus fa-fw"></i>Transfer Cash</a>
    
    <br><br>
    <div class="panel panel-default table-responsive filterable">

     <table class="table table-hover">
      <thead class="panel-info">

        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="Date" disabled></th>
          <th><input type="text" class="form-control" placeholder="Particular" disabled></th>
          <th><input type="text" class="form-control" placeholder="Credit/Debit" disabled></th>
          <th><input type="text" class="form-control" placeholder="Amount" disabled></th>
          <th><input type="text" class="form-control" placeholder="Status" disabled></th>
          <th>Action</th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td>Open</td>
          <td><a href=""></td>
        </tr>
      </tbody>
    </table>

  </div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop