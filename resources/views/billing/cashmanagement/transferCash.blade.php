@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}">Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}">Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}"  >Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment') }}" >Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}" class="active">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}" >Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br><br>
  <form>
   <div class="row">
    {!! Form::label('Available Cash', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('available_cash',null,array('class'=>'form-control')) !!}
    </div>
  </div>

  <br> 
  <div class="row">
    {!! Form::label('Bank/Personal', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('bank',null,array('class'=>'form-control')) !!}
    </div>
  </div>

  <br>
  <div class="row">
    {!! Form::label('Amount', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('amount',null,array('class'=>'form-control')) !!}
    </div>
  </div>

  <br>
  <div class="row">
    {!! Form::label('Remarks', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      {!! Form::text('amount',null,array('class'=>'form-control')) !!}
    </div>
  </div>

  <br><br>
  <div class="row">
    <div class="col-xs-7">
      <button type="submit" class="btn btn-warning pull-right">&#x2714; Save</button>
    </div>
  </div>
</form>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop