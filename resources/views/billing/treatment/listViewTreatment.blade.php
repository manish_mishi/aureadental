@extends('layouts.menuNav')

@section('title')
Billing -> Treatment -> ListView
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}" class="active">Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}">Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}">Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment')}}">Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}">Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br><br>
  <caption><b>Treatment Billing Info</b></caption>
  <div class="panel panel-default table-responsive filterable">
   <table class="table table-hover">
    <thead class="panel-info">
      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Treatment Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Dentist/Consultant" disabled></th>
        <th><input type="text" class="form-control" placeholder="Treatment Status" disabled></th>
        <th><input type="text" class="form-control" placeholder="Start date" disabled></th>
        <th><input type="text" class="form-control" placeholder="Final Quotation" disabled></th>
        <th><input type="text" class="form-control" placeholder="Pending Balance" disabled></th>
        <th><input type="text" class="form-control" placeholder="Billing Status" disabled></th>
        <th>Action</th>
        <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
      </tr>
    </thead>
    <tbody>
      @foreach($treatmentDetails as $treatDet)
      @if($treatDet->patient_treatment_id != null && $treatDet->status_id != null)
      <tr>
        <td>{{$treatDet->patient_name}}</td>
        <td>{{$treatDet->treatment_name}}</td>
        <td>{{$treatDet->dentist_name}}</td>
        <td>{{$treatDet->treatment_status}}</td>
        

        <?php 
        $pendingAmount = 0;
        $sumAmount = 0;
        $finalAmount = 0;
        $k = false;

        foreach ($billInstallmentDetails as $key => $billInstall) 
        {
          if($billInstall->patient_treatment_id == $treatDet->patient_treatment_id)
          {
            $finalAmount = $billInstall->final_amount;
            $sumAmount += $billInstall->amount;
          }
        }

        $pendingAmount = $finalAmount - $sumAmount;
        ?>

        @foreach($billInstallmentDetails as $key => $billInstall)
        @if($billInstall->patient_treatment_id == $treatDet->patient_treatment_id)
        <td>{{$billInstall->date_of_billing}}</td>
        <td>{{$billInstall->final_amount}}</td>
        <td>{{$pendingAmount}}</td>
        <td>{{$billInstall->status}}</td>
        <?php 
        $k = true;
        break;?>
        @endif
        @endforeach

        @if($k == false)
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        @endif
        <td>
          <a href="{{url('billing/treatment/prevoius_bill_details',[$treatDet->patient_treatment_id])}}" class="btn btn-xs btn-prev-bill"><i class="fa fa-chevron-left fa-fw"></i>Prev</a> 
        </td>
        <td>
         <a href="{{url('billing/treatment/generate_bill',[$treatDet->patient_treatment_id])}}" class="btn btn-xs btn-bill"><i class="fa fa-usd fa-fw"></i>Gen</a>
       </td>
     </tr>
     @endif
     @endforeach

   </tbody>
 </table>

</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop