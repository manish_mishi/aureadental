@extends('layouts.menuNav')

@section('title')
Billing -> Previous Bill
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}" class="active">Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}">Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}">Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment') }}">Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}">Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <br>
    <div class="row">
      {!! Form::label('Name', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3" align="left">
        {!! Form::text('name', $patientDetails->patient_name, array('class'=>'form-control','readonly')) !!}
      </div>
      
      <a href="{{ url('billing/treatment') }}"><button type="button" class="btn btn-success pull-right"><i class="fa fa-arrow-left fa-fw"></i>Back</button></a>
    </div>  

    <br>
    <div class="panel panel-default table-responsive filterable">
     <table class="table table-hover">
      <thead class="panel-info">
        <tr><th class="panel panel-default">{{$patientDetails->treatment_name}}</th></tr>
        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="Install No" disabled></th>
          <th><input type="text" class="form-control" placeholder="Billing Date" disabled></th>
          <th><input type="text" class="form-control" placeholder="Payment Date" disabled></th>
          <th><input type="text" class="form-control" placeholder="Bill Number" disabled></th>
          <th><input type="text" class="form-control" placeholder="Bill Status" disabled></th>
          <th><input type="text" class="form-control" placeholder="Notes" disabled></th>
          <th><input type="text" class="form-control" placeholder="Pay Mode" disabled></th>
          <th><input type="text" class="form-control" placeholder="Pending Amount" disabled></th>
          <th><input type="text" class="form-control" placeholder="Final Amount" disabled></th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
        </tr>
      </thead>
      <tbody>
        @foreach($billInstallmentDetails as $billInstall)
       <tr>
        <td>{{$billInstall->id}}</td>
        <td>{{$billInstall->date_of_billing}}</td>
        <td>{{$billInstall->date_of_payment}}</td>
        <td>{{$billInstall->bill_number}}</td>
        <td>{{$billInstall->status}}</td>
        <td>{{$billInstall->notes}}</td>
        <td>{{$billInstall->payment_mode}}</td>
        <td>{{$billInstall->pending_amount}}</td>
        <td colspan="2">{{$billInstall->final_amount}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop