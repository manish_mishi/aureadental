@extends('layouts.menuNav')

@section('title')
Billing -> Generate Bill
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}" class="active">Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}">Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}">Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment') }}">Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}">Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
  <div id="page-wrapper">

    <br>
    {!! Form::open(array('route'=>'storeBill','class'=>'form')) !!}
    <div class="row">
      {!! Form::label('Patient Name', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('patient_name', $patientDetails->patient_name,array('id'=>'day','class'=>'form-control','readonly')) !!}
      </div>
      {!! Form::label('Treatment Name', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::text('treatment', $patientDetails->treatment_name,array('id'=>'day','class'=>'form-control','readonly')) !!}
      </div>
    </div>

    <br><br><br>
    <div class="row">
      {!! Form::label('Bill Number', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        {!! Form::label($billNumber, null,array('class'=>'control-label')) !!}
        {!! Form::hidden('bill_number',$billNumber) !!}
      </div>
      {!! Form::label('Billing Status', null, array('class'=>'col-xs-3 control-label')) !!}
      <div class="col-xs-3">
        <select class="form-control" name="billing_status_id" id ='billingStatusId'>
         <option value>Select</option>
         @foreach($billingStatus as $bill)
         <option value="{{$bill->id}}">{{$bill->status}}</option>
         @endForeach
       </select>
     </div>
   </div><br>

   <div class="row">
    {!! Form::label('Date of Billing', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dateOfBilling" class="form-control" name="date_of_billing">
    </div>
    {!! Form::label('Date of Payment', null, array('class'=>'col-xs-3 control-label')) !!}
    <div class="col-xs-3">
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dateOfPayment" class="form-control" name="date_of_payment">
    </div>
  </div><br>

  <div class="row">
    <label class="col-xs-3">Mode of Payment</label>
    <div class="col-xs-3">
      <select class="form-control" name="payment_mode_id" id ='paymentMode'>
       <option value>Select</option>
       @foreach($paymentMode as $mode)
       <option value="{{$mode->id}}">{{$mode->payment_mode}}</option>
       @endForeach
     </select>
   </div>
   {!! Form::label('Notes', null, array('class'=>'col-xs-3 control-label')) !!}
   <div class="col-xs-3">
    {!! Form::text('notes', null,array('id'=>'day','class'=>'form-control')) !!}
  </div>
</div>

<br>
<div class="row">
  {!! Form::label('Amount', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    <span class="atend">Rs.</span>
    {!! Form::text('amount', null,array('id'=>'amount','class'=>'form-control rs-input','maxlength'=>"20")) !!}
  </div>
  {!! Form::label('Pending Amount', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    <span class="atend">Rs.</span>
    {!! Form::text('pending_amount', $pendingAmount,array('id'=>'pendingAmount','class'=>'form-control rs-input','readonly')) !!}
  </div>
</div>

<br>
<div class="row">
  <div class="col-xs-6"></div>
  {!! Form::label('Final Amount', null, array('class'=>'col-xs-3 control-label')) !!}
  <div class="col-xs-3">
    <span class="atend">Rs.</span>
    {!! Form::text('final_amount', $patientDetails->cost, array('id'=>'finalAmount','class'=>'form-control rs-input','readonly','maxlength'=>"20")) !!}
    {!! Form::hidden('treatment_quotation_id', $patientDetails->treatment_quotation_id) !!}
  </div>
</div>

<br><br>
<div class="row">
  {!! Form::hidden('patient_treatment_id', $patientDetails->patient_treatment_id) !!}
  <button type="submit" class="btn btn-primary" style="margin-left:81%"><i class="fa fa-usd fa-fw"></i>Pay</button>
  <a href="{{ url('billing/treatment') }}"><button type="button" class="btn btn-danger pull-right">&#10006; Cancel</button></a>
</div><br>

{!! Form::close() !!}

<script type="text/javascript">

$("#amount").change(function () {

  $amount = parseFloat($(this).val());
  $pendingAmount = parseFloat($("#pendingAmount").val());
  $finalAmount = parseFloat($("#finalAmount").val());

  if($pendingAmount != 0)
  { 
    if($amount > $pendingAmount)
    {
      alert("Entered Amount should be smaller than Pending Amount and Final Amount");
      $(this).val('');
      return;
    }
  }

  if($amount > $finalAmount)
  {
    alert("Entered Amount should be smaller than Pending Amount and Final Amount");
    $(this).val('');
    return;
  }

});


</script>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop