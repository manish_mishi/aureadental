@extends('layouts.menuNav')

@section('title')
Billing -> Lab Work -> List View
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}" >Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}">Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}"class="active">Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment') }}">Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}">Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="row">
    <div class="col-xs-4">
    {!! Form::label('Lab Name', null, array('class'=>'control-label','style'=>'float:right')) !!}
  </div>
    <div class="col-xs-3">
      {!! Form::text('lab_name',null,array('class'=>'form-control','required')) !!}
    </div>
  </div>

  <a href="{{ url('#') }}" class="btn btn-default col-xs-2" style="margin-left:58%">Search</a> 

  <br><br>
  <div class="panel panel-default table-responsive filterable">
   <table class="table table-hover">
    <thead class="panel-info">
      <tr><th class="panel panel-default" colspan="2">Lab Work Info</th></tr>
      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="Job ID" disabled></th>
        <th><input type="text" class="form-control" placeholder="Treatment Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Patient Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Date" disabled></th>
        <th><input type="text" class="form-control" placeholder="Job Status" disabled></th>
        <th><input type="text" class="form-control" placeholder="Billing Status" disabled></th>
        <th><input type="text" class="form-control" placeholder="Bill Amount" disabled></th>
        <th><input type="text" class="form-control" placeholder="Pending Amount" disabled></th>
        <th><input type="text" class="form-control" placeholder="" disabled></th>
        <th></th>
        <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><a href="{{url('billing/labwork/getdetails')}}">Get Details</a></td>
        <td><a href="{{url('billing/labwork/material/makepayment')}}" class="btn btn-xs btn-primary"><i class="fa fa-usd fa-fw"></i>Make Payment</a></td>
      </tr>
    </tbody>
  </table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop