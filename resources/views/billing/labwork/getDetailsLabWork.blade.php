@extends('layouts.menuNav')

@section('title')
Billing -> Lab Work -> Details
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('billing/treatment') }}" >Treatment</a>
      </li>

      <li>
        <a href="{{ url('billing/inventory/material') }}">Inventory</a>
      </li>

      <li>
        <a href="{{ url('billing/labwork') }}" class="active">Lab Work</a>
      </li>

      <li>
        <a href="{{ url('billing/maintenance/contractpayment') }}">Maintenance</a>
      </li>
      <li>
        <a href="{{ url('billing/salary') }}">Salary</a>
      </li>

      <li>
        <a href="{{ url('billing/cashmanagement') }}">Cash Management</a>
      </li>

      <li>
        <a href="{{ url('billing/miscellaneous') }}">Miscellaneous</a>
      </li>

    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">

  <form>
    <br><br>
    <div class="panel panel-default table-responsive filterable">
     <table class="table table-hover">
      <thead class="panel-info">
        <tr><th class="panel panel-default">Job ID</th></tr>
        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="" disabled>Re-work ID</th>
          <th><input type="text" class="form-control" placeholder="" disabled>Treatment Name</th>
          <th><input type="text" class="form-control" placeholder="" disabled>Patient name</th>
          <th><input type="text" class="form-control" placeholder="" disabled>Date</th>
          <th><input type="text" class="form-control" placeholder="" disabled>Lab Status</th>
          <th><input type="text" class="form-control" placeholder="" disabled></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </tbody>
    </table>

  </div>
  <a href="{{url('billing/labwork')}}" class="btn btn-success pull-right col-xs-2">Back</a>

</form>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
    <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop