@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')



<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-3">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>

<div class="navbar-default sidebar" role="navigation" >
  <div class="sidebar-nav navbar-collapse" role="navigation"  id="bs-sidebar-navbar-collapse-3">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('labs/labdetails') }}" class="active">Lab Details</a>
      </li>

      <li>
        <a href="{{ url('labs/assign/labworkinfo') }}">Assign</a>
      </li>

      <li>
        <a href="{{ url('labs/trackworkdetails') }}">Track Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">

 <div id="page-wrapper">
  

    <div class="col-xs-12">
      <br>
      <div class="col-xs-2" align="left"><label class="control-label">Lab Vendors Name</label></div>
      <div class="col-xs-2" align="left"><select class="form-control"><option>ALL</option></select></div>
      <div class="col-xs-1" align="left"><label class="control-label">Work Type</label></div>
      <div class="col-xs-1" align="left"><select class="form-control"><option>ALL</option></select></div>
      <div class="col-xs-2" align="left"><label class="control-label">Work Sub Type</label></div>
      <div class="col-xs-2" align="left"><select class="form-control"><option>ALL</option></select></div>
      <div class="col-xs-1" align="left"><label class="control-label">Work Name</label></div>
      <div class="col-xs-1" align="left"><select class="form-control"><option>ALL</option></select></div>
    </div>
    <br><br><br><br>

    <a href="{{ url('master/vendors/lab/add') }}" class="btn btn-success pull-right"><i class="fa fa-plus fa-fw"></i>New Lab Vendor</a>  
    <br><br>
    <div class="panel panel-default filterable table-responsive">
     <table class="table table-hover">
      <thead class="panel-info">

        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="Lab Vendor Name" disabled></th>
          <th><input type="text" class="form-control" placeholder="Work Type" disabled></th>
          <th><input type="text" class="form-control" placeholder="Work Sub Type" disabled></th>
          <th><input type="text" class="form-control" placeholder="Work Name" disabled></th>
          <th><input type="text" class="form-control" placeholder="Rates" disabled></th>
          <th>Action</th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
          <!-- <th><button class="btn btn-default btn-xs btn-filter">Filter</button></th> -->
        </tr>
      </thead>
      <tbody>
        @foreach($vendor_details as $details)
        <tr>
         <td>{{$details->name}}</td>
         <td>{{$details->lab_work_type_name}}</td>
         <td>{{$details->lab_work_subtype_name}}</td>
         <td>{{$details->lab_work_name}}</td>
         <td>{{$details->rate}}</td>
         <td>
          <a href="{{ url('master/vendors/lab/edit',[$details->id]) }}" class="btn btn-xs btn-primary"><i class="fa fa-edit fa-fw"></i>Edit</a>
        </td>
        <td>
          <a href="{{ url('master/vendors/lab/view',[$details->id]) }}" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-eye-open">View</a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>

</div>
</div>

@stop