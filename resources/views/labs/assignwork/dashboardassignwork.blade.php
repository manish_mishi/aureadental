@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('labs/labdetails') }}">Lab Details</a>
      </li>

      <li>
        <a href="{{ url('labs/assign/labworkinfo') }}" class="active">Assign</a>
      </li>

      <li>
        <a href="{{ url('labs/trackworkdetails') }}">Track Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>


<div id="wrapper">

 <div id="page-wrapper">
  <br>
  <a href="{{ url('labs/assign/assignwork',[$lab_work_id]) }}" class="btn btn-success pull-right col-xs-2">New Lab Work</a>
  <br><br>
  <div class="panel panel-default filterable">
   <table class="table">
    <thead>
      <tr>
       <th>Lab Work Info</th>
       <th colspan="8"></th>
        </tr>
       <tr class="filters">
        <th><input type="text" class="form-control" placeholder="Job ID" disabled></th>
        <th><input type="text" class="form-control" placeholder="Rework ID" disabled></th>
        <th><input type="text" class="form-control" placeholder="Treatment Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Patient Name" disabled></th>
        <th><input type="text" class="form-control" placeholder="Date" disabled></th>
        <th><input type="text" class="form-control" placeholder="Lab Status" disabled></th>
        <th colspan="3"></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>  </td>
        <td>  </td>
        <td> </td>
        <td>  </td>
        <td>  </td>
        <td></td>
        <td><a href="{{url('labs/assign/viewdetails')}}">View details</a></td>
        <td><a href="{{url('#')}}">Assign Work</a></td>
        <td></td>
      </tr>
    </tbody>
  </table>

</div>

</div>
</div>

@stop