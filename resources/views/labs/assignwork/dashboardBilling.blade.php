@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
          <!-- <li class="sidebar-search">
            <div class="input-group custom-search-form">
              <input type="text" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div> -->
            <!-- /input-group -->
            <!--  </li> -->
            <li>
              <a href="{{ url('labs/labdetails') }}">Lab Details</a>
            </li>

            <li>
              <a href="{{ url('labs/assign/labworkinfo') }}" class="active">Assign</a>
            </li>

            <li>
              <a href="{{ url('labs/trackworkdetails') }}">Track Work</a>
            </li>
          </ul>
        </div>
        <!-- /.sidebar-collapse -->
      </div>




      <div id="wrapper">

       <div id="page-wrapper">
        <div class="row marketing">
          <div class="panel panel-info">
            <ul style="width:100%" class="nav nav-pills">
              <li style="width:23%" role="presentation" ><a href="{{ url('labs/assign/assignwork',[$lab_work_id])}}">Assign Work</a></li>
              <li style="width:23%" role="presentation"><a href="{{ url('labs/assign/labform',[$lab_work_id])}}">Lab Form</a></li>
              <li style="width:30%" role="presentation"  ><a href="{{ url('labs/assign/delivery',[$lab_work_id])}}">Delivery</a></li>
              <li style="width:23%" role="presentation" class="active"><a href="{{ url('labs/assign/billingr',[$lab_work_id])}}">Billing</a></li>

            </ul>
          </div>    


          @if($billing_details == null)
          {!! Form::open(array('route'=>'addBilling')) !!}
          <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              {!! Form::label('Bill Number', null, array()) !!}
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              {!! Form::text('bill_number',null,array('class'=>'form-control','required')) !!}
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              {!! Form::label('Date of Billing', null, array()) !!}
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="date_of_billing" class="form-control" name="date_of_billing" value="<?php echo date("d/m/Y");?>">

            </div><br><br><br><br>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              {!! Form::label('Billing Status', null, array()) !!}
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

              <select name="billing_status" class="form-control">
                <option>select</option>
                @foreach($bill_status as $billing)
                <option value="{{$billing->id}}">{{$billing->status}}</option>
                @endforeach
              </select>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              {!! Form::label('Final Cost', null, array()) !!}
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              {!! Form::text('final_cost',null,array('class'=>'form-control','required')) !!}
            </div>
          </div><br>

          <div class="row">
            <div class="col-xs-4"><button type="submit" class="btn btn-warning ">Save</button></div>

          </div>

          {!! Form::hidden('lab_work_id',$lab_work_id,array('class'=>'form-control','required')) !!}
          {!! Form::close() !!}

          @else

          {!! Form::open(array('route'=>'updateBilling')) !!}

          <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              {!! Form::label('Bill Number', null, array()) !!}
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              {!! Form::text('bill_number',$billing_details['0']->bill_number,array('class'=>'form-control','required')) !!}
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              {!! Form::label('Date of Billing', null, array()) !!}
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="date_of_billing" class="form-control" name="date_of_billing" value="<?php echo $billing_details['0']->bill_date;?>">

            </div><br><br><br><br>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              {!! Form::label('Billing Status', null, array()) !!}
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

              <select name="billing_status" class="form-control">
                <option value="{{$billing_details['0']->billing_status_id}}">{{$billing_details['0']->status}}</option>
                @foreach($bill_status as $billing)
                <option value="{{$billing->id}}">{{$billing->status}}</option>
                @endforeach
              </select>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              {!! Form::label('Final Cost', null, array()) !!}
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
              {!! Form::text('final_cost',$billing_details['0']->final_cost,array('class'=>'form-control','required')) !!}
            </div>
          </div><br>
          <div class="row">
            <div class="col-xs-4"><button type="submit" class="btn btn-warning ">Save</button></div>

          </div>
          {!! Form::hidden('lab_work_id',$lab_work_id,array('class'=>'form-control','required')) !!}

          {!! Form::close() !!}
          
          @endif
          <div class="row">
            <div class="col-xs-8"></div>
            
            <div class="col-xs-4"><a href="{{url('billing/inventory/material')}}"><button type="submit" class="btn btn-primary">Make Payment</button></a></div>

          </div>
        </div>

      </div>
    </div>
  </div>



</div>
</div>
</div>
</div>

@stop