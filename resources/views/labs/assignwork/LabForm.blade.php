@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
          <!-- <li class="sidebar-search">
            <div class="input-group custom-search-form">
              <input type="text" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div> -->
            <!-- /input-group -->
            <!--  </li> -->
            <li>
              <a href="{{ url('labs/labdetails') }}">Lab Details</a>
            </li>

            <li>
              <a href="{{ url('labs/assign/labworkinfo') }}" class="active">Assign</a>
            </li>

            <li>
              <a href="{{ url('labs/trackworkdetails') }}">Track Work</a>
            </li>
           </ul>
        </div>
        <!-- /.sidebar-collapse -->
      </div>




      <div id="wrapper">

       <div id="page-wrapper"><br>
        
        <div class="panel panel-info">
              <ul style="width:100%" class="nav nav-pills">
                <li style="width:23%" role="presentation" ><a href="{{ url('labs/assign/assignwork',[$lab_work_id]) }}">Assign Work</a></li>
                <li style="width:23%" role="presentation" class="active"><a href="{{ url('labs/assign/labform',[$lab_work_id]) }}">Lab Form</a></li>
                <li style="width:30%" role="presentation" ><a href="{{ url('labs/assign/delivery',[$lab_work_id]) }}">Delivery</a></li>
                <li style="width:23%" role="presentation" ><a href="{{ url('vendors/lab/byvendor',[$lab_work_id]) }}">Billing</a></li>

              </ul>
            </div>  
            <br><br>             <br><br> 
            <br><br> 
            <br><br> 
            <br><br> 
            <br><br> 
            

            <div class="row">
                <div class="col-xs-12"><button type="submit" class="btn btn-warning pull-right">Save</button>
                </div>     
         
      </div>
    
  </div>


          
        </div>
      </div>
    </div>
  </div>

  @stop