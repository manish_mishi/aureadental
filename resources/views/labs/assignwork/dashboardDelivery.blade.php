@extends('layouts.menuNav')

@section('title')
Dashboard
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('labs/labdetails') }}">Lab Details</a>
      </li>

      <li>
        <a href="{{ url('labs/assign/labworkinfo') }}" class="active">Assign</a>
      </li>

      <li>
        <a href="{{ url('labs/trackworkdetails') }}">Track Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">

 <div id="page-wrapper"><br>

  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:23%" role="presentation" ><a href="{{ url('labs/assign/assignwork',[$lab_work_id])}}">Assign Work</a></li>
      <li style="width:23%" role="presentation"><a href="{{ url('labs/assign/labform',[$lab_work_id])}}">Lab Form</a></li>
      <li style="width:30%" role="presentation" class="active" ><a href="{{ url('labs/assign/delivery',[$lab_work_id])}}">Delivery</a></li>
      <li style="width:23%" role="presentation" ><a href="{{ url('labs/assign/billing',[$lab_work_id])}}">Billing</a></li>

    </ul>
  </div>        
  {!! Form::open(array('route'=>'addDelivery')) !!}

  @if($labDeliveryDetails != null)  

  <div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Challan Number', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Date of Delivery', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Delivery Location', null, array()) !!}
    </div>
    <a onClick="duplicatechallannumber()" class="btn btn-info btn-md col-md-1">
      <span class="glyphicon glyphicon-plus"></span> 
    </a>
  </div>

  <div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text('challan_no[]',null,array('class'=>'form-control','id'=>'challanNumberId')) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dateofDelivery" class="form-control" name="date_of_delivery[]" value="<?php echo date("d/m/Y");?>">
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text('delivery_location[]',null,array('class'=>'form-control','id'=>'deliveryLocation')) !!}
    </div>
  </div><br>
 
  <div class="row">
    <div id="duplicater" style="display:none;">

      <div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <input type="text" class="form-control challanNo" id="challan_no" class="form-control" Readonly/><br>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <input type="text" class="form-control dateofDelivery" id="date_of_delivery" class="form-control" Readonly/><br>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <input type="text" class="form-control deliveryLocation" id="delivery_location" class="form-control" Readonly/><br>
      </div>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::button(null,array('id' =>'delete_btn','class'=>'btn btn-md btn-danger glyphicon glyphicon-trash','onClick'=>'removeduplicate(this)')) !!}
      </div><br><br><br>
    </div>
  </div>

  <!-- to display exist values -->
  @foreach($labDeliveryDetails as $delivery)
  <div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text(null,$delivery->chalan_number,array('class'=>'form-control','id'=>'challanNumberId','readonly')) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dateofDelivery" class="form-control"  value="{{$delivery->delivery_date}}" Readonly>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text(null,$delivery->delivery_location,array('class'=>'form-control','id'=>'deliveryLocation','readonly')) !!}
    </div>
    <div class="col-xs-8 col-sm-1 col-md-1 col-lg-1">
      <a href="{{url('labs/assign/delivery/deleteDeliveryDetails',[$delivery->id,$delivery->t_assign_lab_work_id])}}">{!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}</a>
      
    </div>
  </div><br>
  @endforeach
 
  
  <!-- exist values -->

  @else

  <div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Challan Number', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Date of Delivery', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Delivery Location', null, array()) !!}
    </div>
    <a onClick="duplicatechallannumber()" class="btn btn-info btn-md col-md-1">
      <span class="glyphicon glyphicon-plus"></span> 
    </a>
  </div>

  <div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text('challan_no[]',null,array('class'=>'form-control','id'=>'challanNumberId')) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="dateofDelivery" class="form-control" name="date_of_delivery[]" value="<?php echo date("d/m/Y");?>">
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text('delivery_location[]',null,array('class'=>'form-control','id'=>'deliveryLocation')) !!}
    </div>
  </div><br>
 
  <div class="row">
    <div id="duplicater" style="display:none;">

      <div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <input type="text" class="form-control challanNo" id="challan_no" class="form-control" Readonly/><br>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <input type="text" class="form-control dateofDelivery" id="date_of_delivery" class="form-control" Readonly/><br>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <input type="text" class="form-control deliveryLocation" id="delivery_location" class="form-control" Readonly/><br>
      </div>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::button(null,array('id' =>'delete_btn','class'=>'btn btn-md btn-danger glyphicon glyphicon-trash','onClick'=>'removeduplicate(this)')) !!}
      </div><br><br><br>
    </div>
  </div>

  @endif

  
   
    <div class="row">
      <div class="col-xs-12"><button type="submit" class="btn btn-warning pull-right">Generate Delivery</button>
    </div>
  </div><br>

  </div>

  {!! Form::hidden('lab_work_id',$lab_work_id,array('class'=>'form-control')) !!}
  {!! Form::close() !!}

  
</div>
</div>



</div>
</div>
</div>
</div>
<script>

var i=0;

function duplicatechallannumber() 
{

  var challannumber = document.getElementById('challanNumberId').value;
  var dateofdelivery = document.getElementById('dateofDelivery').value;
  var deliverylocation = document.getElementById('deliveryLocation').value;
  
  if(challannumber!='' )
  {

    var original = document.getElementById('duplicater');
    original.style.display = "block";

    document.getElementById('challan_no').value =challannumber;
    document.getElementById('date_of_delivery').value =dateofdelivery;
    document.getElementById('delivery_location').value =deliverylocation;

    
    var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicater" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);
    original.style.display = "none";

    document.getElementById('challanNumberId').value="";
    document.getElementById('dateofDelivery').value="";
    document.getElementById('deliveryLocation').value="";
    
    var nameElemnt = document.getElementById("duplicater"+i).getElementsByClassName('challanNo')[0];
    nameElemnt.setAttribute("name","challan_no[]");

    var nameElemnt = document.getElementById("duplicater"+i).getElementsByClassName('dateofDelivery')[0];
    nameElemnt.setAttribute("name","date_of_delivery[]");

    var nameElemnt = document.getElementById("duplicater"+i).getElementsByClassName('deliveryLocation')[0];
    nameElemnt.setAttribute("name","delivery_location[]");

  }

}

function removeduplicate(element)
{
    element=element.parentNode.parentNode;//gets the id of the parent
    element.parentNode.removeChild(element);
  }

  </script>

  @stop