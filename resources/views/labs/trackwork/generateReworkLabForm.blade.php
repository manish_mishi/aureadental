@extends('layouts.menuNav')

@section('title')
Generate Rework
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
          
            <li>
              <a href="{{ url('labs/labdetails') }}">Lab Details</a>
            </li>

            <li>
              <a href="{{ url('labs/assign/labworkinfo') }}">Assign</a>
            </li>

            <li>
              <a href="{{ url('labs/trackworkdetails') }}" class="active">Track Work</a>
            </li>
          </ul>
        </div>
        <!-- /.sidebar-collapse -->
      </div>

    
       <div id="wrapper">
       <div id="page-wrapper">
        <br>
        <div class="panel panel-info">
          <ul style="width:100%" class="nav nav-pills">
            <li style="width:23%" role="presentation"><a href="{{ url('labs/trackwork/generaterework/assignwork',[$id]) }}">Assign Work</a></li>
            <li style="width:23%" role="presentation" class="active"><a href="{{ url('labs/trackwork/generaterework/labform',[$id]) }}">Lab Form</a></li>
            <li style="width:30%" role="presentation" ><a href="{{ url('labs/trackwork/generaterework/delivery',[$id]) }}">Delivery</a></li>
            <li style="width:23%" role="presentation" ><a href="{{ url('labs/trackwork/generaterework/billing',[$id]) }}">Billing</a></li>
          </ul>
        </div>
      </div>
    </div>
@stop