@extends('layouts.menuNav')

@section('title')
Track Work View List
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
          
            <li>
              <a href="{{ url('labs/labdetails') }}">Lab Details</a>
            </li>

            <li>
              <a href="{{ url('labs/assign/labworkinfo') }}">Assign</a>
            </li>

            <li>
              <a href="{{ url('labs/trackworkdetails') }}" class="active">Track Work</a>
            </li>
          </ul>
        </div>
        <!-- /.sidebar-collapse -->
      </div>




      <div id="wrapper">

       <div id="page-wrapper">


        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" style="float: right">
          <br>

          <div class="col-xs-10 col-sm-2 col-md-2 col-lg-2" align="right"><label></label></div>
          <div class="col-xs-10 col-sm-2 col-md-2 col-lg-2" align="left"></div>
          <div class="col-xs-10 col-sm-2 col-md-2 col-lg-2" align="right"><label></label></div>
          <div class="col-xs-10 col-sm-2 col-md-2 col-lg-2" align="left"></div>
          <div class="col-xs-10 col-sm-2 col-md-2 col-lg-2" align="right"><label>Sort By</label></div>
          <div class="col-xs-10 col-sm-2 col-md-2 col-lg-2" align="left"><select><option>Instrument/Work</option></select></div>

        </div>
        <br><br><br><br>

        <div class="panel panel-default filterable table-responsive">
         <table class="table table-hover">
          <thead>
           <tr>
             <th>Track Work</th>
           </tr>
           <tr class="filters">


            <th><input type="text" class="form-control" placeholder="Rework ID" disabled></th>
            <th><input type="text" class="form-control" placeholder="Date" disabled></th>
            <th><input type="text" class="form-control" placeholder="Dentist/Consultant" disabled></th>
            <th><input type="text" class="form-control" placeholder="Lab Name" disabled></th>
            <th><input type="text" class="form-control" placeholder="Status" disabled></th>
            <th><input type="text" class="form-control" placeholder="Instrument Name" disabled></th>
            <!-- <th><input type="text" class="form-control" placeholder="Work" disabled></th> -->
            <th></th>
            <th></th>
            <!-- <th><button class="btn btn-default btn-xs btn-filter">Filter</button></th> -->
          </tr>
        </thead>
        <tbody>
          @foreach($job_details as $job)
          <tr>  
            <td></td>
            <td>{{$job->work_allocation_date}}</td>
            <td></td>
            <td>{{$job->labname}}</td>
            <td>{{$job->labstatus}}</td>
            <td>{{$job->instrument_name}}</td>
            <!-- <td></td> -->


            <td><a href="{{ url('labs/trackwork/getdetails/assignwork',[$job->id]) }}">Get Details</a></td>
            <td><a href="{{ url('labs/trackwork/generaterework/assignwork',[$job->id]) }}">Generate rework</a></td>
          </tr>
          @endforeach
        </tbody>
      </table>

    </div>

  </div>
</div>

@stop