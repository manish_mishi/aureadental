@extends('layouts.menuNav')

@section('title')
Assign Work
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
          
            <li>
              <a href="{{ url('labs/labdetails') }}">Lab Details</a>
            </li>

            <li>
              <a href="{{ url('labs/assign/labworkinfo') }}">Assign</a>
            </li>

            <li>
              <a href="{{ url('labs/trackworkdetails') }}" class="active">Track Work</a>
            </li>
          </ul>
        </div>
        <!-- /.sidebar-collapse -->
      </div>

      <div id="wrapper">
       <div id="page-wrapper">
        <br>
        <div class="panel panel-info">
          <ul style="width:100%" class="nav nav-pills">
            <li style="width:23%" role="presentation" class="active"><a href="{{ url('labs/trackwork/getdetails/assignwork',[$id]) }}">Assign Work</a></li>
            <li style="width:23%" role="presentation"><a href="{{ url('labs/trackwork/getdetails/labform',[$id]) }}">Lab Form</a></li>
            <li style="width:30%" role="presentation" ><a href="{{ url('labs/trackwork/getdetails/delivery',[$id]) }}">Delivery</a></li>
            <li style="width:23%" role="presentation" ><a href="{{ url('labs/trackwork/getdetails/billing',[$id]) }}">Billing</a></li>
          </ul>
        </div>
        @if($details == null)


        <div class="row">
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Lab Name', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
           

            {!! Form::text('lab_name',null,array('class'=>'form-control','required','readonly')) !!}

          </div>
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Work Allocation Date', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="Work_Allocation_Date" class="form-control" name="work_allocation_date" value="" Readonly>
            <!-- {!! Form::text('work_allocation_date',null,array('class'=>'form-control','required')) !!} -->
          </div>
        </div><br>

        <div class="row">
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Contact Details', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text('contact_details',null,array('class'=>'form-control','required', 'id'=>'contact_details', 'readonly')) !!}
          </div>
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Deadline', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text('deadline',null,array('class'=>'form-control','required','readonly')) !!}
          </div>


        </div><br>

        <div class="row">
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Address', null, array()) !!}
        </div>

          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text('address',null,array('class'=>'form-control','required','id'=>'address','readonly')) !!}
          </div>
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Instrument List', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            
            {!! Form::text('lab_person_responsible',null,array('class'=>'form-control','required','readonly')) !!}

          </div>
          
        </div><br>

        <div class="row">
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Status', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            
            {!! Form::text('lab_person_responsible',null,array('class'=>'form-control','required','readonly')) !!}
          </div>
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Lab Person Responsible', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text('lab_person_responsible',null,array('class'=>'form-control','required','readonly')) !!}
          </div>

        </div><br>
        <div class="row">
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Assistant', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            
            {!! Form::text('assistant',null,array('class'=>'form-control','required','readonly')) !!}
          </div>
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Responsible Dentist', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text('responsible_dentist',null,array('class'=>'form-control','readonly')) !!}
          </div>
        </div><br>


        @else

        <div class="row">
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Lab Name', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
           

            {!! Form::text('lab_name',$details['0']->labname,array('class'=>'form-control','required','readonly')) !!}

          </div>
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Work Allocation Date', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="Work_Allocation_Date" class="form-control" name="work_allocation_date" value="<?php echo $details['0']->work_allocation_date;?>" Readonly>
            <!-- {!! Form::text('work_allocation_date',null,array('class'=>'form-control','required')) !!} -->
          </div>
        </div><br>

        <div class="row">
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Contact Details', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text('contact_details',$details['0']->contact_person,array('class'=>'form-control','required', 'id'=>'contact_details', 'readonly')) !!}
          </div>
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Deadline', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text('deadline',$details['0']->deadline,array('class'=>'form-control','required','readonly')) !!}
          </div>


        </div><br>

        <div class="row">
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Address', null, array()) !!}
        </div>

          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text('address',$details['0']->address,array('class'=>'form-control','required','id'=>'address','readonly')) !!}
          </div>
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Instrument List', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            @if($instrumentNames == null)
            {!! Form::text(null,null,array('class'=>'form-control','required','id'=>'address','readonly')) !!}<br>
            @else
            @foreach($instrumentNames as $instrument)

              {!! Form::text(null,$instrument->instrument_name,array('class'=>'form-control','required','id'=>'address','readonly')) !!}<br>
            @endforeach
            @endif
          </div>
          

          

        </div><br>

        <div class="row">
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Status', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            
            {!! Form::text('lab_person_responsible',$details['0']->labstatus,array('class'=>'form-control','required','readonly')) !!}
          </div>
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Lab Person Responsible', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text('lab_person_responsible',$details['0']->lab_person_responsible,array('class'=>'form-control','required','readonly')) !!}
          </div>

        </div><br>
        <div class="row">
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Assistant', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            
            {!! Form::text('assistant',$details['0']->staff,array('class'=>'form-control','required','readonly')) !!}
          </div>
           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Responsible Dentist', null, array()) !!}
        </div>
          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            {!! Form::text('responsible_dentist',null,array('class'=>'form-control','readonly')) !!}
          </div>
        </div><br>
        @endif
        <div class="row">

          <div class="col-xs-10"></div>

          <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <a href="{{ url('labs/trackworkdetails')}}"><button class="btn btn-danger">&#10006; Cancel</button></a>
          </div>


        </div><br>
        
       </div>
     </div>

@stop