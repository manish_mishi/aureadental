@extends('layouts.menuNav')

@section('title')
Delivery
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('labs/labdetails') }}">Lab Details</a>
      </li>

      <li>
        <a href="{{ url('labs/assign/labworkinfo') }}">Assign</a>
      </li>

      <li>
        <a href="{{ url('labs/trackworkdetails') }}" class="active">Track Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>

<div id="wrapper">
 <div id="page-wrapper">
  <br>
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:23%" role="presentation"><a href="{{ url('labs/trackwork/getdetails/assignwork',[$id]) }}">Assign Work</a></li>
      <li style="width:23%" role="presentation"><a href="{{ url('labs/trackwork/getdetails/labform',[$id]) }}">Lab Form</a></li>
      <li style="width:30%" role="presentation" class="active" ><a href="{{ url('labs/trackwork/getdetails/delivery',[$id]) }}">Delivery</a></li>
      <li style="width:23%" role="presentation" ><a href="{{ url('labs/trackwork/getdetails/billing',[$id]) }}">Billing</a></li>
    </ul>
  </div>
  @if($labDeliveryDetails == null)
  <div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Challan Number', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Date of Delivery', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Delivery Location', null, array()) !!}
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text('challannumber',null,array('class'=>'form-control','id'=>'challanNumberId','readonly')) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="date_of_delivery" class="form-control" name="date_of_delivery" value="" Readonly>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text('delivery_location',null,array('class'=>'form-control','readonly')) !!}
    </div>
  </div>
  
  @else

  <div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Challan Number', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Date of Delivery', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Delivery Location', null, array()) !!}
    </div>
  </div>
  
  <div class="row">
    @foreach($labDeliveryDetails as $details)
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text('challannumber',$details->chalan_number,array('class'=>'form-control','id'=>'challanNumberId','readonly')) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text('challannumber',$details->delivery_date,array('class'=>'form-control','id'=>'challanNumberId','readonly')) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text('challannumber',$details->delivery_location,array('class'=>'form-control','id'=>'challanNumberId','readonly')) !!}
    </div><br><br>
    @endforeach
  </div>

  @endif




  <div class="row">

    <div class="col-xs-10"></div>

    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      <a href="{{ url('labs/trackworkdetails')}}"><button class="btn btn-danger">&#10006; Cancel</button></a>
    </div>


  </div><br>

  @stop