@extends('layouts.menuNav')

@section('title')
Assign Work
@stop

@section('content')

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">

      <li>
        <a href="{{ url('labs/labdetails') }}">Lab Details</a>
      </li>

      <li>
        <a href="{{ url('labs/assign/labworkinfo') }}">Assign</a>
      </li>

      <li>
        <a href="{{ url('labs/trackworkdetails') }}" class="active">Track Work</a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>
<div id="wrapper">
 <div id="page-wrapper">

  <br>
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:23%" role="presentation" class="active"><a href="{{ url('labs/trackwork/generaterework/assignwork',[$id]) }}">Assign Work</a></li>
      <li style="width:23%" role="presentation"><a href="{{ url('labs/trackwork/generaterework/labform',[$id]) }}">Lab Form</a></li>
      <li style="width:30%" role="presentation" ><a href="{{ url('labs/trackwork/generaterework/delivery',[$id]) }}">Delivery</a></li>
      <li style="width:23%" role="presentation" ><a href="{{ url('labs/trackwork/generaterework/billing',[$id]) }}">Billing</a></li>
    </ul>
  </div> 

  @if($details != null)       

  {!! Form::open(array('route'=>'editgeneraterework')) !!}
  <div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Lab Name', null, array('class'=>'control-label')) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      <select class="form-control" name="lab_id" id="labId">
        <option value="{{$details['0']->lab_work_name_id}}">{{$details['0']->labname}}</option>
        @foreach($lab_details as $lab)
        <option value="{{$lab->id}}">{{$lab->name}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Work Allocation Date', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="Work_Allocation_Date" class="form-control" name="work_allocation_date" value="<?php echo $details['0']->work_allocation_date;?>">
    </div>
  </div><br>

  <div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Contact Details', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text('contact_details',$details['0']->contact_person,array('class'=>'form-control','required', 'id'=>'contact_details')) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Deadline', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text('deadline',$details['0']->deadline,array('class'=>'form-control','required')) !!}
    </div>


  </div><br>

  <div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Address', null, array()) !!}
    </div>

    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text('address',$details['0']->address,array('class'=>'form-control','required','id'=>'address')) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Instrument List', null, array()) !!}
    </div>
    <div class="col-xs-2">
      <select class="form-control" id="instrumentId">
        <option>select</option>
        @foreach($instrument_list as $instrument)
        <option value="{{$instrument->id}}">{{$instrument->instrument_name}}</option>
        @endforeach
      </select><br>

    </div>

    <a href="#" onClick="duplicateInstrumentName()" class="btn btn-info btn-md col-md-1">
      <span class="glyphicon glyphicon-plus"></span> 
    </a>

    @foreach($instrumentnames as $names)
    <div>
      <div class="col-xs-9"></div>
      <div class="col-xs-2">
        {!! Form::text('null',$names->instrument_name,array('class'=>'form-control','required','readonly')) !!}<br>
      </div>
      <div class="col-xs-1">
        <a href="{{ url('labs/trackwork/deleteinstrument',[$names->assign_lab_work_id,$names->instrument_name_id])}}">{!! Form::button('<span class="glyphicon glyphicon-trash"></span>',array('class'=>'form-control btn btn-danger col-xs-1','id'=>'delete_btn','onClick'=>'removeduplicate(this)')) !!}</a>
      </div>
    </div>  
    @endforeach

    <div id="duplicater" style="display:none;">
      <div class="col-xs-9"></div>
      <div class="col-xs-2">
        <input type="text" class="form-control instr_name" id="instrumentName" class="form-control" Readonly/><br>
        <input type="hidden" class="form-control instr_id" id="instrumentNameId" class="form-control"/>
      </div>
      <div class="col-xs-1">
        {!! Form::button(null,array('id' =>'delete_btn','class'=>'btn btn-md btn-danger glyphicon glyphicon-trash','onClick'=>'removeduplicate(this)')) !!}
      </div>
    </div>

  </div>

  <div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Status', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      <select class="form-control" name="status" id="instrumentId">
        <option value="{{$details['0']->lab_work_status_id}}">{{$details['0']->labstatus}}</option>
        @foreach($statusDetails as $status )
        <option value="{{$status->id}}">{{$status->status}}</option>
        @endforeach
      </select>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Lab Person Responsible', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::text('lab_person_responsible',$details['0']->lab_person_responsible,array('class'=>'form-control','required')) !!}
    </div>

  </div><br>
  <div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      {!! Form::label('Assistant', null, array()) !!}
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      <select class="form-control" name="assistant">
        <option value="{{$details['0']->staff_id}}">{{$details['0']->staff}}</option>
        @foreach($staffDetails as $staff)
        <option value="{{$staff->id}}">{{$staff->name}}</optin>
          @endforeach
        </select>
        <!-- {!! Form::text('assistant',null,array('class'=>'form-control','required')) !!} -->
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::label('Responsible Dentist', null, array()) !!}
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::text('responsible_dentist',null,array('class'=>'form-control')) !!}
      </div>
    </div><br>
    <div class="row">
      <div class="col-xs-12"><button type="submit" class="btn btn-warning pull-right">Save</button>
      </div>
    </div><br>

    {!! Form::hidden('id',$id,array('class'=>'form-control','required')) !!}
    {!! Form::close() !!}

    @else
    <!-- for add start -->
    {!! Form::open(array('route'=>'addgeneraterework')) !!}
    <div class="row">
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::label('Lab Name', null, array()) !!}
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <select class="form-control" name="lab_id" id="labId">
          <option>select</option>
          @foreach($lab_details as $lab)
          <option value="{{$lab->id}}">{{$lab->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::label('Work Allocation Date', null, array()) !!}
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <input data-provide="datepicker" data-date-format="dd/mm/yyyy" id="Work_Allocation_Date" class="form-control" name="work_allocation_date" value="<?php echo date("d/m/Y");?>">
        <!-- {!! Form::text('work_allocation_date',null,array('class'=>'form-control','required')) !!} -->
      </div>
    </div><br>

    <div class="row">
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::label('Contact Details', null, array()) !!}
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::text('contact_details',null,array('class'=>'form-control','required', 'id'=>'contact_details')) !!}
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::label('Deadline', null, array()) !!}
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::text('deadline',null,array('class'=>'form-control','required')) !!}
      </div>


    </div><br>

    <div class="row">
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::label('Address', null, array()) !!}
      </div>

      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::text('address',null,array('class'=>'form-control','required','id'=>'address')) !!}
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::label('Instrument List', null, array()) !!}
      </div>
      <div class="col-xs-2">
        <select class="form-control" name="instrument_name_id" id="instrumentId">
          <option>select</option>
          @foreach($instrument_list as $instrument)
          <option value="{{$instrument->id}}">{{$instrument->instrument_name}}</option>
          @endforeach
        </select><br>

      </div>
      <a href="#" onClick="duplicateInstrumentName()" class="btn btn-info btn-md col-md-1">
        <span class="glyphicon glyphicon-plus"></span> 
      </a>

      <div id="duplicater" style="display:none;">
        <div class="col-xs-9"></div>
        <div class="col-xs-2">
          <input type="text" class="form-control instr_name" id="instrumentName" class="form-control" Readonly/><br>
          <input type="hidden" class="form-control instr_id" id="instrumentNameId" class="form-control"/>
        </div>
        <div class="col-xs-1">
          {!! Form::button(null,array('id' =>'delete_btn','class'=>'btn btn-md btn-danger glyphicon glyphicon-trash','onClick'=>'removeduplicate(this)')) !!}
        </div>

      </div>

    </div>

    <div class="row">
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::label('Status', null, array()) !!}
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <select class="form-control" name="status" id="instrumentId">
          <option>select</option>
          @foreach($statusDetails as $status )
          <option value="{{$status->id}}">{{$status->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::label('Lab Person Responsible', null, array()) !!}
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::text('lab_person_responsible',null,array('class'=>'form-control','required')) !!}
      </div>

    </div><br>
    <div class="row">
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        {!! Form::label('Assistant', null, array()) !!}
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
        <select class="form-control" name="assistant">
          <option>select</option>
          @foreach($staffDetails as $staff)
          <option value="{{$staff->id}}">{{$staff->name}}</optin>
            @endforeach
          </select>
          <!-- {!! Form::text('assistant',null,array('class'=>'form-control','required')) !!} -->
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::label('Responsible Dentist', null, array()) !!}
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          {!! Form::text('responsible_dentist',null,array('class'=>'form-control')) !!}
        </div>
      </div><br>
      <div class="row">
        <div class="col-xs-12"><button type="submit" class="btn btn-warning pull-right">Save</button>
        </div>
      </div><br>

      {!! Form::hidden('id',$id,array('class'=>'form-control','required')) !!}
      {!! Form::close() !!}
      @endif
      
    </div>
  </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">

$('#instrumentId').change(function(){

  $('#instrumentName').val($('option:selected',this).text());
  $('#instrumentNameId').val($(this).val());
});
var i=0;

function duplicateInstrumentName() {

  var instrumentname = document.getElementById('instrumentName').value;
  var instrumentid = document.getElementById('instrumentNameId').value;
  
  if(instrumentname!='' && instrumentid!=''){

    var original = document.getElementById('duplicater');
    original.style.display = "block";

    document.getElementById('instrumentName').value =instrumentname;
    document.getElementById('instrumentNameId').value =instrumentid;

  var clone = original.cloneNode(true); // "deep" clone
    clone.id = "duplicater" + ++i; // there can only be one element with an ID
    original.parentNode.appendChild(clone);
    original.style.display = "none";

    document.getElementById('instrumentName').value="";
    document.getElementById('instrumentNameId').value="";

    var nameElemnt = document.getElementById("duplicater"+i).getElementsByClassName('instr_name')[0];
    nameElemnt.setAttribute("name","instrument_name[]");

    var instrumentidElement = document.getElementById("duplicater"+i).getElementsByClassName('instr_id')[0];
    instrumentidElement.setAttribute("name","instrument_id[]");

  }

}

function removeduplicate(element)
{
    element=element.parentNode.parentNode;//gets the id of the parent
    element.parentNode.removeChild(element);
  }

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });


  $(document).ready(function(){
    $('#labId').change(function(){

      var labName=$(this).val();
      var selectContact =document.getElementById('contact_details');
      var selectAddress =document.getElementById('address');
      

      $(selectContact).val('');
      $(selectAddress).val('');
      

      $.ajax({
        method:'POST',
        url: '{{url("labs/assign/assignwork/labdetails/")}}'+"/"+labName,
        success : function(data){
          var comp='';
          for(var i in data)
          {
            if(data[i]['contact_person'] != comp)
            {
              $(selectContact).val(data[i]['contact_person']);
            }

            if(data[i]['address'] != comp)
            {
              $(selectAddress).val(data[i]['address']);
            }

            
          }
        }
      });

    });

  });
  </script>

  @stop