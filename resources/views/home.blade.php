@extends('layouts.menuNav')

@section('head')
    @parent
    	@section('title')
    		Home
		@stop
@stop

@section('content')
    @If(Session::has('success'))
    	<div class="alert alert-success">{{ Session::get('success') }}</div>
    @elseIf (Session::has('fail'))
    	<div class="alert alert-danger">{{ Session::get('fail') }}</div>
    @endIf

    
@stop	