@extends('layouts.settingsNav')

@section('title')
Settings -> Inventory -> Material Unit
@stop

@section('side_bar')
<div class="sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('settings/inventory/materialmanagement/addmaterialtype') }}" class="active">Material Management</a>
      </li>
      <li>
        <a href="{{ url('settings/inventory/instrumentmanagement/addinstrumenttype') }}">Instrument Management</a>
      </li>
      <li>
        <a href="{{ url('settings/inventory/machinemanagement/addmachinetype') }}">Machine Management</a>
      </li>
      <li>
        <a href="{{ url('settings/inventory/gadgetmanagement/addgadgettype') }}">Gadget Management</a>
      </li>
    </ul>
  </div>
</div>
@stop

@section('main')

<div id="wrapper">
  <div id="page-wrapper">
    
    <div class="panel panel-info">
      <ul style="width:100%" class="nav nav-pills">
        <li style="width:33%" role="presentation"><a href="{{ url('settings/inventory/materialmanagement/addmaterialtype') }}">Material Type</a></li>
        <li style="width:33%" role="presentation"><a href="{{ url('settings/inventory/materialmanagement/addmaterialsubtype') }}">Material Subtype</a></li>
        <li style="width:33%" role="presentation" class="active"><a href="{{ url('settings/inventory/materialmanagement/addmaterailunit') }}">Unit</a></li>
      </ul>
    </div>

    <div class="row">

      {!! Form::open(array('route'=>'addunit','class'=>'form')) !!}

      <!-- successfullye added msg -->
      <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
      </div> <!-- end .flash-message -->

      <div class="form-group">
        {!! Form::label('unit :',null,array('class'=>' col-xs-3 control-label')) !!}
        <div class="col-xs-4">
          {!! Form::text('unit',null,array('class'=>'form-control'))!!}
        </div>
      </div>

      <div class="form-group">
        <div class="col-xs-6"></div>
        {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning')) !!}

      </div>

      {!! Form::close() !!}

    </div>

    <div class="panel panel-info filterable">
      <!-- Table -->
      <table class="table table-hover"> 
        <thead class="panel-info">
          <tr class="filters">
            <th><input type="text" class="form-control" placeholder="#" disabled></th>
            <th><input type="text" class="form-control" placeholder="Unit" disabled></th>
            <th>Action</th>
            <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
          </tr> 
        </thead>
        <tbody>
          @foreach($matunit_details as $matunit)
          <tr>
           <td>{{$matunit->id}}</th>
             <td>{{$matunit->name}}</td>
             <td colspan="2">
              {!! Form::open(array('url'=>'deletematerialunit', 'class' => 'form')) !!}
              {!! Form::hidden('mat_unit_id', $matunit->id) !!}
              {!! Form::button('<i class="fa fa-trash fa-fw"></i>Delete',array('type' => 'submit','class'=>'btn btn-xs btn-danger')) !!}
              {!! Form::close() !!}
            </td>
          </tr>
          @endforeach 
        </tbody>
      </table>
    </div>

    <!-- back to top of the page -->
    <p id="backTop" style="display: none;">
      <a href="#top"><span></span>Back to Top</a>
    </p>

  </div>
</div>
@stop





