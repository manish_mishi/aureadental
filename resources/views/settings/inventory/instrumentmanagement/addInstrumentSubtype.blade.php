@extends('layouts.settingsNav')

@section('title')
Settings -> Inventory -> Instrument SubType
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
 <div class="sidebar-nav navbar-collapse">
   <ul class="nav" id="side-menu">

    <li>
      <a href="{{ url('settings/inventory/materialmanagement/addmaterialtype') }}">Material Management</a>
    </li>
    <li>
      <a href="{{ url('settings/inventory/instrumentmanagement/addinstrumenttype') }}" class="active">Instrument Management</a>
    </li>
    <li>
      <a href="{{ url('settings/inventory/machinemanagement/addmachinetype') }}">Machine Management</a>
    </li>
    <li>
      <a href="{{ url('settings/inventory/gadgetmanagement/addgadgettype') }}">Gadget Management</a>
    </li>
  </ul>
</div>
</div>
@stop

@section('main')

<div id="wrapper">
 <div id="page-wrapper">
  <div class="panel panel-info">
    <ul style="width:100%" class="nav nav-pills">
      <li style="width:33%" role="presentation"><a href="{{ url('settings/inventory/instrumentmanagement/addinstrumenttype') }}">Instrument Type</a></li>
      <li style="width:33%" role="presentation" class="active"><a href="{{ url('settings/inventory/instrumentmanagement/addinstrumentsubtype') }}">Instrument Subtype</a></li>
      <li style="width:33%" role="presentation"><a href="{{ url('settings/inventory/instrumentmanagement/addinstrumentunit') }}">Instrument Unit</a></li>
    </ul>
  </div>

  <div class="row">

    {!! Form::open(array('route'=>'addinstrumentsubtype','class'=>'form')) !!}

    <!-- successfullye added msg -->
    <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
    </div> <!-- end .flash-message -->

    <div class="form-group">
      {!! Form::label('Instrument SubType :',null,array('class'=>' col-xs-3 control-label')) !!}
      <div class="col-xs-4">
        {!! Form::text('instrument_subtype',null,array('class'=>'form-control'))!!}
      </div>
    </div>
    <br><br>
    <div class="form-group">
      {!! Form::label('Instrument type',null,array('class'=>'col-xs-3 control-label')) !!}

      <div class="col-xs-4">
        <select class="form-control" name="instrument_type_id" id ='instrumentType'>
        <option value>Select</option>
          @foreach($instrumentTypes as $instrumentType)
           <option value="{{$instrumentType->id}}">{{$instrumentType->name}}</option>
          @endForeach
        </select>

      </div>
      <div class="form-group">
        {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning')) !!}
      </div>
    </div>

    {!! Form::close() !!}

  </div>

  <div class="panel panel-info filterable">
    <!-- Table -->
    <table class="table table-hover"> 
      <thead class="panel-info">
        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="#" disabled></th>
          <th><input type="text" class="form-control" placeholder="Sub Type" disabled></th>
          <th><input type="text" class="form-control" placeholder="Type" disabled></th>
          <th>Action</th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
        </tr> 
      </thead>
      <tbody>
        @foreach($instsubtyp_details as $instsubtyp)
        <tr>
         <td>{{$instsubtyp->id}}</td>
         <td>{{$instsubtyp->name}}</td>
         <td colspan="2">{{$instsubtyp->instrument_type_name}}</td>
         <td>
          {!! Form::open(array('url'=>'deleteinstrumentsubtype', 'class' => 'form')) !!}
          {!! Form::hidden('inst_subtype_id', $instsubtyp->id) !!}
          {!! Form::button('<i class="fa fa-trash fa-fw"></i>Delete',array('type' => 'submit','class'=>'btn btn-xs btn-danger')) !!}
          {!! Form::close() !!}
        </td>
      </tr>
      @endforeach 
    </tbody>
  </table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
@stop





