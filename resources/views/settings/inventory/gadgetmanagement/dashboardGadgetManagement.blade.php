@extends('layouts.settingsNav')

@section('title')
Gadget Management
@stop

@section('side_bar')


<div class="navbar-default sidebar" role="navigation">
  	<div class="sidebar-nav navbar-collapse">
    	<ul class="nav" id="side-menu">
          <li>
              <a href="{{ url('settings/inventory/materialmanagementdashboard') }}">Material Management</a>
         	</li>
         	<li>
              <a href="{{ url('settings/inventory/instrumentmanagementdashboard') }}">Instrument Management</a>
         	</li>
         	<li>
              <a href="{{ url('settings/inventory/machinemanagementdashboard') }}">Machine Management</a>
         	</li>
         	<li>
              <a href="{{ url('settings/inventory/gadgetmanagementdashboard') }}">Gadget Management</a>
         	</li>
     	</ul>
  	</div>
</div>
@stop

@section('main')

<div id="wrapper">
	 <div id="page-wrapper">

	 	<div class="row">
          <a href="{{ url('settings/inventory/gadgetmanagement/addgadgettype') }}" class="btn btn-success pull-right col-xs-3" style="margin-top:4%">Add</a>
    </div>

    <br>
    <div class="panel panel-info">
          <!-- Table -->
              <table class="table table-hover"> 
                <thead class="panel-info">
                 <tr>
                  <th>#</th>
                  <th>Gadget Type</th>
                  <th>Gadget Sub type</th>
                </tr> 
              </thead>
              <tbody>
                <tr>
                 <th scope=row>1</th>
                 <td>Mark</td>
                 <td>Otto</td>
               </tr> 
               <tr>
                 <th scope=row>2</th> 
                 <td>Jacob</td> 
                 <td>Thornton</td> 
               </tr>
            </tbody>
          </table>
    </div>

     </div>
</div>

 @stop