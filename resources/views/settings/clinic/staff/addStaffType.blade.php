@extends('layouts.settingsNav')

@section('title')
Material Type
@stop

@section('side_bar')


<div class="navbar-default sidebar" role="navigation">
 <div class="sidebar-nav navbar-collapse">
   <ul class="nav" id="side-menu">

    <li>
      <a href="{{ url('settings/clinic/staff/add_staff_type') }}" class="active">Staff Type</a>
    </li>

  </ul>
</div>
</div>
@stop

@section('main')

<div id="wrapper">
 <div id="page-wrapper">

  <div class="row">

   {!! Form::open(array('route'=>'addstafftype','class'=>'form')) !!}

   <!-- successfullye added msg -->
   <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->

  <div class="form-group">
    {!! Form::label('Staff Type :',null,array('class'=>' col-xs-3 control-label')) !!}
    <div class="col-xs-4">
      {!! Form::text('staff_type',null,array('class'=>'form-control','required'))!!}
    </div>
  </div>

  <div class="form-group">
    {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning')) !!}
    
  </div>

  {!! Form::close() !!}

</div>

<div class="panel panel-info filterable">
  <!-- Table -->
  <table class="table table-hover"> 
    <thead class="panel-info">
      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="#" disabled></th>
        <th><input type="text" class="form-control" placeholder="Staff Type" disabled></th>
        <th>Action</th>
        <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
      </tr> 
    </thead>
     <tbody>
      @foreach($stafftype_details as $stafftyp)
      <tr>
       <td>{{$stafftyp->id}}</td>
       <td>{{$stafftyp->name}}</td>
       <td colspan='2'>
        {!! Form::open(array('url'=>'deletestafftype', 'class' => 'form')) !!}
        {!! Form::hidden('staff_type_id', $stafftyp->id) !!}
        {!! Form::button('<i class="fa fa-trash fa-fw"></i>Delete',array('type' => 'submit','class'=>'btn btn-xs btn-danger')) !!}
        {!! Form::close() !!}
      </td>
    </tr>
    @endforeach 
  </tbody>
</table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>

@stop





