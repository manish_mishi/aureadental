@extends('layouts.settingsNav')

@section('title')
Settings -> Billing -> Status
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
 <div class="sidebar-nav navbar-collapse">
   <ul class="nav" id="side-menu">
    <li>
      <a href="{{ url('settings/billing/paymentmode/view') }}" >Payment Mode</a>
    </li>
    <li>
      <a href="{{ url('settings/billing/billingstatus/view') }}"  class="active">Billing Status</a>
    </li>
  </ul>
</div>
</div>
@stop

@section('main')

<div id="wrapper">
 <div id="page-wrapper">

  <div class="panel panel-info filterable">
    <!-- Table -->
    <table class="table table-hover"> 
      <thead class="panel-info">
        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="#" disabled></th>
          <th><input type="text" class="form-control" placeholder="Dentist Name" disabled></th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
        </tr> 
      </thead>
      <tbody>
        @foreach($billing_status as $billing_status)
        <tr>
         <td>{{$billing_status->id}}</td>
         <td colspan='2'>{{$billing_status->status}}</td>
      </tr>
      @endforeach 
    </tbody>
  </table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
@stop





