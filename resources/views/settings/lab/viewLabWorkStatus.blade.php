@extends('layouts.settingsNav')

@section('title')
Settings -> Lab -> Work -> Status
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
 <div class="sidebar-nav navbar-collapse">
   <ul class="nav" id="side-menu">
    <li>
      <a href="{{ url('settings/lab/addworktype') }}">Work Type</a>
    </li>
    <li>
      <a href="{{ url('settings/lab/addworksubtype') }}">Work Subtype</a>
    </li>
    <li>
      <a href="{{ url('settings/lab/addworkname') }}">Work Name</a>
    </li>
    <li>
      <a href="{{ url('settings/lab/work/status') }}" class="active">Work Status</a>
    </li>
  </ul>
</div>
</div>
@stop

@section('main')

<div id="wrapper">
  <div id="page-wrapper">

    <div class="panel panel-info filterable">
      <!-- Table -->
      <table class="table table-hover"> 
        <thead class="panel-info">
          <tr class="filters">
            <th><input type="text" class="form-control" placeholder="#" disabled></th>
            <th><input type="text" class="form-control" placeholder="Status" disabled></th>
            <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
          </tr> 
        </thead>
        <tbody>
          @foreach($labWorkStatusDetails as $labWorkStatus)
          <tr>
           <td>{{$labWorkStatus->id}}</td>
           <td>{{$labWorkStatus->status}}</td>
           <td></td>
        </tr>
        @endforeach 
      </tbody>
    </table>
  </div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
@stop
