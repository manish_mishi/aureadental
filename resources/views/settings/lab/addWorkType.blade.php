@extends('layouts.settingsNav')

@section('title')
Settings -> Lab -> Work Type
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
 <div class="sidebar-nav navbar-collapse">
   <ul class="nav" id="side-menu">
    <li>
      <a href="{{ url('settings/lab/addworktype') }}" class="active">Work Type</a>
    </li>
    <li>
      <a href="{{ url('settings/lab/addworksubtype') }}">Work Subtype</a>
    </li>
    <li>
      <a href="{{ url('settings/lab/addworkname') }}">Work Name</a>
    </li>
    <li>
      <a href="{{ url('settings/lab/work/status') }}">Work Status</a>
    </li>
  </ul>
</div>
</div>
@stop

@section('main')

<div id="wrapper">
 <div id="page-wrapper">
  <div class="row">

   {!! Form::open(array('route'=>'addWorktype','class'=>'form')) !!}

   <!-- successfullye added msg -->
   <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
  </div> <!-- end .flash-message -->

  <div class="form-group">
    {!! Form::label('Work Type :',null,array('class'=>' col-xs-3 control-label')) !!}
    <div class="col-xs-4">
      {!! Form::text('work_type',null,array('class'=>'form-control','required'))!!}
    </div>
  </div>

  <div class="form-group">
    {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning')) !!}
    
  </div>

  {!! Form::close() !!}

</div>

<div class="panel panel-info filterable">
  <!-- Table -->
  <table class="table table-hover"> 
    <thead class="panel-info">
      <tr class="filters">
        <th><input type="text" class="form-control" placeholder="#" disabled></th>
        <th><input type="text" class="form-control" placeholder="Work Type" disabled></th>
        <th>Action</th>
        <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
      </tr> 
    </thead>
    <tbody>
      @foreach($worktyp_details as $worktyp)
      <tr>
       <td>{{$worktyp->id}}</td>
       <td>{{$worktyp->name}}</td>
       <td colspan='2'>
        {!! Form::open(array('url'=>'deleteSettingsLabWorktype', 'class' => 'form')) !!}
        {!! Form::hidden('work_type_id', $worktyp->id) !!}
       {!! Form::button('<i class="fa fa-trash fa-fw"></i>Delete',array('type' => 'submit','class'=>'btn btn-xs btn-danger')) !!}
        {!! Form::close() !!}
      </td>
    </tr>
    @endforeach 
  </tbody>
</table>
</div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
@stop





