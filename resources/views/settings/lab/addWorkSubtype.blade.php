@extends('layouts.settingsNav')

@section('title')
Settings -> Lab -> Work SubType
@stop

@section('side_bar')


<div class="sidebar" role="navigation">
 <div class="sidebar-nav navbar-collapse">
   <ul class="nav" id="side-menu">
    <li>
      <a href="{{ url('settings/lab/addworktype') }}">Work Type</a>
    </li>
    <li>
      <a href="{{ url('settings/lab/addworksubtype') }}" class="active">Work Subtype</a>
    </li>
    <li>
      <a href="{{ url('settings/lab/addworkname') }}">Work Name</a>
    </li>
    <li>
      <a href="{{ url('settings/lab/work/status') }}">Work Status</a>
    </li>
  </ul>
</div>
</div>
@stop

@section('main')

<div id="wrapper">
 <div id="page-wrapper">

  <div class="row">

    {!! Form::open(array('route'=>'addWorkSubtype','class'=>'form')) !!}

    <!-- successfullye added msg -->
    <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
      @endforeach
    </div> <!-- end .flash-message -->

    <div class="form-group">
      {!! Form::label('Work subType',null,array('class'=>' col-xs-3 control-label')) !!}
      <div class="col-xs-4">
        {!! Form::text('work_subtype',null,array('class'=>'form-control'))!!}
      </div>
    </div>

    <br><br>
    <div class="form-group">
      {!! Form::label('Work type',null,array('class'=>'col-xs-3 control-label')) !!}

      <div class="col-xs-4">
        <select class="form-control" name="work_type_id" id ='workType'>
        <option value>Select</option>
          @foreach($workType as $workTyp)
           <option value="{{$workTyp->id}}">{{$workTyp->name}}</option>
          @endForeach
        </select>
      </div>
      <div class="form-group">
        {!! Form::submit('&#x2714; Save',array('class'=>'btn btn-warning')) !!}
      </div>
    </div>



    {!! Form::close() !!}

  </div>

  <div class="panel panel-info filterable">
    <!-- Table -->
    <table class="table table-hover"> 
      <thead class="panel-info">
        <tr class="filters">
          <th><input type="text" class="form-control" placeholder="#" disabled></th>
          <th><input type="text" class="form-control" placeholder="Work SubType" disabled></th>
          <th><input type="text" class="form-control" placeholder="Work Type" disabled></th>
          <th>Action</th>
          <th><button class="btn btn-info btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span>Filter</button></th>
        </tr> 
      </thead>
      <tbody>
        @foreach($worksubtyp_details as $worksubtyp)
        <tr>
         <td>{{$worksubtyp->id}}</th>
           <td>{{$worksubtyp->name}}</td>
           <td>{{$worksubtyp->lab_work_type}}</td>
           <td colspan="2">
            {!! Form::open(array('url'=>'deleteSettingsLabWorkSubtype', 'class' => 'form')) !!}
             {!! Form::hidden('work_subtype_id', $worksubtyp->id) !!}

            {!! Form::button('<i class="fa fa-trash fa-fw"></i>Delete',array('type' => 'submit','class'=>'btn btn-xs btn-danger')) !!}
            {!! Form::close() !!}
          </td>
        </tr>
        @endforeach 
      </tbody>
    </table>
  </div>

<!-- back to top of the page -->
<p id="backTop" style="display: none;">
  <a href="#top"><span></span>Back to Top</a>
</p>

</div>
</div>
@stop





