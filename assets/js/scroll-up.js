// hide #backTop first
	$("p#backTop").hide();
	
	// fade in #backTop
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#backTop').fadeIn();
			} else {
				$('#backTop').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#backTop a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 100);
			return false;
		});
	});