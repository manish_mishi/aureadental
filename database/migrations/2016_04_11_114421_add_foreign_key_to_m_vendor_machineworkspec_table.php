<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMVendorMachineworkspecTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_vendor_machine_workspec_details', function($table)
		{
			$table->foreign('machine_name_id')
						->references('id')
						->on('m_machine_details')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('vendor_machine_id')
						->references('id')
						->on('m_vendor_machine_details')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_vendor_machine_workspec_details', function($table)
		{
			$table->dropForeign('machine_name_id');
			$table->dropForeign('vendor_machine_id');
		});
	}

}
