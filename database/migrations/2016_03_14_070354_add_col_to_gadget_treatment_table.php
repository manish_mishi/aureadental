<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToGadgetTreatmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_gadget_treatment',function($table)
		{
			$table->string('treatment_type_id')->after('id');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_gadget_treatment',function($table)
		{
			$table->dropcolumn('treatment_type_id');
		});
	}

}
