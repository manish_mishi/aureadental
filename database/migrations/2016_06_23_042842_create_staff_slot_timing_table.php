<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffSlotTimingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_staff_slot_timing', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('slot_start_time');
			$table->string('slot_end_time');
			$table->string('status');
			$table->integer('staff_slot_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_staff_slot_timing');
	}

}
