<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSittingsDiagnosisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_sittings_diagnosis', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('plan');
			$table->string('prescription');
			$table->string('findings');
			$table->string('notes');
			$table->integer('sittings_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_sittings_diagnosis');
	}

}
