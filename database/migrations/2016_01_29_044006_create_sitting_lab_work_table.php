<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSittingLabWorkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_sitting_lab_work', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('job_date');
			$table->string('delivery_date');
			$table->string('observation');
			$table->string('remarks');
			$table->integer('sittings_id')->unsigned()->index();
			/*$table->integer('patient_treatment_id')->unsigned()->index();*/
			$table->integer('lab_work_status_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_sitting_lab_work');
	}

}
