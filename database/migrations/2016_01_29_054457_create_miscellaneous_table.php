<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiscellaneousTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('miscellaneous', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('date_of_payment');
			$table->string('paid_to');
			$table->string('amount');
			$table->string('reason');
			$table->integer('payment_mode_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('miscellaneous');
	}

}
