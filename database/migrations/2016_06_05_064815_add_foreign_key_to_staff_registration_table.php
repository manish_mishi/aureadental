<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToStaffRegistrationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_staff_registration', function($table)
		{

			$table->foreign('staff_type_id')
						->references('id')
						->on('s_staff_type')
						->onDelete('restrict')
						->onUpdate('restrict');

		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_staff_registration', function($table)
		{
			$table->dropForeign('staff_type_id');
		});
	}

}