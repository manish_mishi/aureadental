<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToAppointmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('t_appointment', function($table)
		{
			$table->string('date')->after('id');
			$table->string('slot')->after('date');
			$table->string('assistant')->after('slot');
			$table->string('consultant')->after('assistant');
			$table->integer('appointment_status_id')->unsigned()->index()->after('consultant');
			$table->integer('patient_id')->unsigned()->index()->after('appointment_status_id');
			$table->integer('dentist_id')->unsigned()->index()->after('patient_id');
			$table->integer('chair_id')->unsigned()->index()->after('dentist_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('t_appointment', function($table)
		{
			$table->dropcolumn('date');
			$table->dropcolumn('slot');
			$table->dropcolumn('assistant');
			$table->dropcolumn('consultant');
			$table->dropcolumn('appointment_status_id');
			$table->dropcolumn('patient_id');
			$table->dropcolumn('dentist_id');
			$table->dropcolumn('chair_id');
		});
	}

}
