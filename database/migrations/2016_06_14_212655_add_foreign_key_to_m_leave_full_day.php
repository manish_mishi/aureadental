<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMLeaveFullDay extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_leave_full_day', function($table)
		{
			$table->foreign('staff_id')
						->references('id')
						->on('m_staff_registration')
						->onDelete('restrict')
						->onUpdate('restrict');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_leave_full_day', function($table)
		{
			$table->dropForeign('staff_id');
		});
	}

}