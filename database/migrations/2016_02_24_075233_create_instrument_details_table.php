<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstrumentDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_instrument_details',function(Blueprint $table)

			{
				$table->increments('id');
				$table->string('instrument_name');
				$table->integer('instrument_type_id');
				$table->integer('instrument_subtype_id');
				$table->integer('instrument_unit_id');
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_instrument_details');
	}

}
