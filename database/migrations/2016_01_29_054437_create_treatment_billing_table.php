<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreatmentBillingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_treatment_billing', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('patient_treatment_id')->unsigned()->index();
			$table->integer('quotation_id')->unsigned()->index();
			$table->integer('billing_status_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_treatment_billing');
	}

}
