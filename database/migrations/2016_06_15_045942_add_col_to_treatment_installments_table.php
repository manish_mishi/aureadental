<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToTreatmentInstallmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('t_treatment_installments', function($table)
		{
			$table->integer('amount')->after('id');
			$table->integer('final_amount')->after('pending_amount');
			$table->string('notes')->after('date_of_payment');
			$table->integer('billing_status_id')->unsigned()->index()->after('notes');
			$table->integer('patient_treatment_id')->unsigned()->index()->after('payment_mode_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('t_treatment_installments', function($table)
		{
			$table->dropcolumn('amount');
			$table->dropcolumn('final_amount');
			$table->dropcolumn('notes');
			$table->dropcolumn('billing_status_id');
			$table->dropcolumn('patient_treatment_id');
		});
	}

}
