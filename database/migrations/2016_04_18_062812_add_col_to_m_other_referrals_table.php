<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToMOtherReferralsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_other_referrals',function($table)
		{
			$table->integer('patient_id')->unsigned()->index()->after('other_referrals_name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_other_referrals',function($table)
		{
			$table->dropColumn('patient_id');
		});
	}

}
