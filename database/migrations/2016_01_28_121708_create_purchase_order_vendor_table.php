<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderVendorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_purchase_order_vendor', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('total_amount');
			$table->integer('discount');
			$table->integer('final_amount');
			$table->integer('vendor_name_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_purchase_order_vendor');
	}

}
