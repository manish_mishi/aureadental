<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowUpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_follow_up', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('cell_no');
			$table->string('notes');
/*			$table->string('follow_up_date');
			$table->string('follow_up_time');*/
			$table->integer('patient_id')->unsigned()->index();
			$table->integer('follow_up_status_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_follow_up');
	}

}
