<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_appointment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('appointment_date');
			$table->string('assistant_name');
			$table->string('doctore_name');
			$table->string('consultant_name');
			$table->integer('slot_details_id')->unsigned()->index();
			$table->integer('machine_id')->unsigned()->index();
			$table->integer('appointment_status_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_appointment');
	}

}
