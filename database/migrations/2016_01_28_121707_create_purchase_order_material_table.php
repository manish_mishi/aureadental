<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderMaterialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_purchase_order_material', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('quantity');
			$table->integer('gadget_id')->unsigned()->index();
			$table->integer('treatment_name_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_purchase_order_material');
	}

}
