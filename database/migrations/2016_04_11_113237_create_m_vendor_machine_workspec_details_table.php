<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMVendorMachineWorkspecDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_vendor_machine_workspec_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('rate');
			$table->integer('vendor_type_id')->unsigned()->index();
			$table->integer('machine_name_id')->unsigned()->index();
			$table->integer('vendor_machine_id')->unsigned()->index();
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_vendor_machine_workspec_details');
	}

}

