<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsToGadgetDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_gadget_details',function($table)
		{
			$table->string('gadget_type_id')->after('gadget_name');
			$table->string('gadget_subtype_id')->after('gadget_type_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_gadget_details',function($table)
		{
			$table->dropcolumn('gadget_type_id');
			$table->dropcolumn('gadget_subtype_id');
		});
	}

}
