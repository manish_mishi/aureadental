<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMMaterialDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_material_details', function($table)
		{
			$table->integer('material_type_id')->unsigned()->index()->change();
			$table->integer('material_subtype_id')->unsigned()->index()->change();
			$table->integer('material_unit_id')->unsigned()->index()->change();

			$table->foreign('material_type_id')
						->references('id')
						->on('s_material_type')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('material_subtype_id')
						->references('id')
						->on('s_material_subtype')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('material_unit_id')
						->references('id')
						->on('s_material_unit')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_material_details', function($table)
		{
			$table->dropForeign('material_type_id');
			$table->dropForeign('material_subtype_id');
			$table->dropForeign('material_unit_id');
		});
	}

}
