<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabBillingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_lab_billing', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('final_cost');
			$table->integer('assign_lab_work_id')->unsigned()->index();
			$table->integer('lab_delivery_id')->unsigned()->index();
			$table->integer('billing_status_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_lab_billing');
	}

}
