<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMVendorInstrumentWorkspecDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_vendor_inst_workspec_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('rate');
			$table->integer('duration');
			$table->integer('inst_type_id');
			$table->integer('inst_subtype_id');
			$table->integer('inst_name_id');
			$table->integer('vendor_inst_id');
			/*$table->foreign('work_type_id')->references('id')->on('s_lab_work_type')->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('work_subtype_id')->references('id')->on('s_lab_work_subtype')->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('work_name_id')->references('id')->on('s_lab_work_name')->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('vendor_id')->references('id')->on('m_vendor_lab_details')->onDelete('restrict')->onUpdate('restrict');*/
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_vendor_inst_workspec_details');
	}

}
