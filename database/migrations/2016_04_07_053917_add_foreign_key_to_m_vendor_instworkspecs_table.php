<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMVendorInstworkspecsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_vendor_inst_workspec_details', function($table)
		{
			$table->integer('inst_type_id')->unsigned()->index()->change();
			$table->integer('inst_subtype_id')->unsigned()->index()->change();
			$table->integer('inst_name_id')->unsigned()->index()->change();
			$table->integer('vendor_inst_id')->unsigned()->index()->change();

			$table->foreign('inst_type_id')
						->references('id')
						->on('s_instrument_type')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('inst_subtype_id')
						->references('id')
						->on('s_instrument_subtype')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('inst_name_id')
						->references('id')
						->on('m_instrument_details')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('vendor_inst_id')
						->references('id')
						->on('m_vendor_instrument_details')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_vendor_inst_workspec_details', function($table)
		{
			$table->dropForeign('inst_type_id');
			$table->dropForeign('inst_subtype_id');
			$table->dropForeign('inst_name_id');
			$table->dropForeign('vendor_inst_id');
		});
	}

}
