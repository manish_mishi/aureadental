<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMLeavePartialDaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
public function up()
	{
		Schema::create('m_leave_partial_days', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('date');
			$table->string('time_from');
			$table->string('time_to');
			$table->integer('staff_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_leave_partial_days');
	}

}