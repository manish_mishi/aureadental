<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMVendorMaintenanceWorkspecDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_vendor_maint_workspec_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('contract_start_date');
			$table->string('contract_end_date');
			$table->string('contract_amount');
			$table->string('expected_available_time');
			$table->string('duration');
		    $table->string('rate');
            $table->integer('vendor_type_id')->unsigned()->index();
			$table->integer('machine_name_id')->unsigned()->index();
			$table->integer('gadget_name_id')->unsigned()->index();
            $table->integer('vendor_maintenance_id')->unsigned()->index();
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_vendor_maint_workspec_details');
	}

}

