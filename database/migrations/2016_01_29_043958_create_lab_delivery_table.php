<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabDeliveryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_lab_delivery', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('chalan_number');
			$table->string('delivery_date');
			$table->string('delivery_location');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_lab_delivery');
	}

}
