<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMVendorGadgetworkspecTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_vendor_gadget_workspec_details', function($table)
		{
			$table->foreign('gadget_name_id')
						->references('id')
						->on('m_gadget_details')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('vendor_gadget_id')
						->references('id')
						->on('m_vendor_gadget_details')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_vendor_mat_workspec_details', function($table)
		{
			$table->dropForeign('gadget_name_id');
			$table->dropForeign('vendor_gadget_id');
		});
	}

}
