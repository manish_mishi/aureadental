<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSittingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_sittings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('patient_treatment_id')->unsigned()->index();
			$table->integer('appointment_id')->unsigned()->index();
			$table->integer('material_name_id')->unsigned()->index();
			$table->integer('instrument_name_id')->unsigned()->index();
			$table->integer('gadget_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_sittings');
	}

}
