<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMMachineTreatmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_machine_treatment', function($table)
		{
			$table->integer('treatment_type_id')->unsigned()->index()->change();

			$table->foreign('treatment_name_id')
						->references('id')
						->on('m_treatment_name')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('treatment_type_id')
						->references('id')
						->on('s_treatment_type')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('machine_name_id')
						->references('id')
						->on('m_instrument_details')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_machine_treatment', function($table)
		{
			$table->dropForeign('treatment_type_id');
			$table->dropForeign('treatment_name_id');
			$table->dropForeign('machine_name_id');
		});
	}

}
