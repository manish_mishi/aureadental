<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstrumentInstallmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_instrument_installments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('amount');
			$table->integer('bill_number');
			$table->string('date_of_billing');
			$table->string('date_of_payment');
			$table->integer('instrument_billing_id')->unsigned()->index();
			$table->integer('payment_mode_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_instrument_installments');
	}

}
