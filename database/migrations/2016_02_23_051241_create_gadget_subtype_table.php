<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGadgetSubtypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('s_gadget_subtype',function(Blueprint $table)
			{
				$table->increments('id');
				$table->String('name');
				$table->integer('gadget_type_id')->unsigned()->index();
				$table->timestamps();
			});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('s_gadget_subtype');
	}

}
