<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMMachineDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_machine_details', function($table)
		{
			$table->integer('machine_type_id')->unsigned()->index()->change();
			$table->integer('machine_subtype_id')->unsigned()->index()->change();

			$table->foreign('machine_type_id')
						->references('id')
						->on('s_machine_type')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('machine_subtype_id')
						->references('id')
						->on('s_machine_subtype')
						->onDelete('restrict')
						->onUpdate('restrict');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_machine_details', function($table)
		{
			$table->dropForeign('machine_type_id');
			$table->dropForeign('machine_subtype_id');
		});
	}

}
