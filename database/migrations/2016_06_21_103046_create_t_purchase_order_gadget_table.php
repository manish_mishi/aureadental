<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTPurchaseOrderGadgetTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_purchase_order_gadget', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('gadget_name_id')->unsigned()->index();
			$table->integer('quantity');
			$table->integer('purchase_order_id')->unsigned()->index();
			$table->timestamps();	
				
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_purchase_order_gadget');
	}

}
