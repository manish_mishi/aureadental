<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreatmentConsultantTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_treatment_consultant', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('consultant_name');
			$table->string('clinic');
			$table->string('location');
			$table->string('fees');
			$table->string('notes');
			$table->integer('patient_treatment_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_treatment_consultant');
	}

}
