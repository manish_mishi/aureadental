<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMLeavePartialDays extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
public function up()
	{
		Schema::table('m_leave_partial_days', function($table)
		{
			$table->foreign('staff_id')
						->references('id')
						->on('m_staff_registration')
						->onDelete('restrict')
						->onUpdate('restrict');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_leave_partial_days', function($table)
		{
			$table->dropForeign('staff_id');
		});
	}

}