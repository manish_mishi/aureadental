<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashTransferTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_cash_transfer', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('remarks');
			$table->integer('available_amount');
			$table->integer('amount');
			$table->integer('transfer_detail_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_cash_transfer');
	}

}
