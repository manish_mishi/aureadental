<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicSlotTimingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_clinic_slot_timing', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('slot_start_time');
			$table->string('slot_end_time');
			$table->integer('clinic_slot_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_clinic_slot_timing');
	}

}
