<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToStaffClinicTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_staff_clinic', function(Blueprint $table)
		{
			$table->integer('clinic_type_id')->unsigned()->index()->after('clinic_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_staff_clinic', function(Blueprint $table)
		{
			$table->dropColumn('clinic_type_id');
		});
	}

}
