<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForiegnKeyToClinicSlotDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_clinic_slot_details', function($table)
		{
			$table->foreign('clinic_id')
						->references('id')
						->on('m_clinic')
						->onDelete('restrict')
						->onUpdate('restrict');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_clinic_slot_details', function($table)
		{
			$table->dropForeign('clinic_id');
		});
	}

}
