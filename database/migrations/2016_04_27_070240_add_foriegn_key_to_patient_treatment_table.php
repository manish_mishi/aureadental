<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForiegnKeyToPatientTreatmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('t_patient_treatment', function($table)
		{
			$table->foreign('patient_id')
						->references('id')
						->on('m_patient_details')
						->onDelete('restrict')
						->onUpdate('restrict');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('t_patient_treatment', function($table)
		{
			$table->dropForeign('patient_id');
		});
	}

}
