<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('schedule_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('patient_name');
			$table->integer('cell_no');
			$table->string('assign_date');
			$table->string('dentist_name');
			$table->string('assign_slot');
			$table->string('chair');
			$table->string('assistant');
			$table->string('consultant');
			$table->string('status');

            $table->timestamps();
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
				Schema::drop('schedule_details');
	}

}
