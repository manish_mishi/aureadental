<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMGadgetDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_gadget_details', function($table)
		{
			$table->integer('gadget_type_id')->unsigned()->index()->change();
			$table->integer('gadget_subtype_id')->unsigned()->index()->change();

			$table->foreign('gadget_type_id')
						->references('id')
						->on('s_gadget_type')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('gadget_subtype_id')
						->references('id')
						->on('s_gadget_subtype')
						->onDelete('restrict')
						->onUpdate('restrict');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_gadget_details', function($table)
		{
			$table->dropForeign('gadget_type_id');
			$table->dropForeign('gadget_subtype_id');
		});
	}

}
