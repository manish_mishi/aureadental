<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoBillingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_po_billing', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('bill_number');
			$table->string('date_of_billing');
			$table->integer('advance');
			$table->integer('discount');
			$table->integer('amount');
			$table->integer('purchase_order_status_id')->unsigned()->index();
			$table->integer('purchase_order_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_po_billing');
	}

}
