<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToMMedicalHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_medical_history', function($table)
		{
			$table->string('mime')->after('medical_history');
			$table->string('original_filename')->after('mime');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_medical_history', function($table)
		{
			$table->dropcolumn('mime');
			$table->dropcolumn('original_filename');
		});
	}

}
