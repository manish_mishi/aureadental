<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForiegnKeyToFollowUpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('t_follow_up', function($table)
		{

			$table->foreign('patient_id')
						->references('id')
						->on('m_patient_details')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('follow_up_status_id')
						->references('id')
						->on('dnf_follow_up_status')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('t_follow_up', function($table)
		{
			$table->dropForeign('patient_id');
			$table->dropForeign('follow_up_status_id');
		});
	}

}
