<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTPurchaseOrderInstrumentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_purchase_order_instrument', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('instrument_name_id')->unsigned()->index();
			$table->integer('quantity');
			$table->integer('purchase_order_id')->unsigned()->index();
			$table->timestamps();	
				
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_purchase_order_instrument');
	}

}
