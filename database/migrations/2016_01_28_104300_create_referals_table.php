<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_referrals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('patient_id')->unsigned()->index();
			$table->integer('referred_patient_id')->unsigned()->index();
/*			$table->string('other');*/
			/*$table->foreign('other_ref_id')->references('id')->on('other_referals');*/
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_referals');
	}

}
