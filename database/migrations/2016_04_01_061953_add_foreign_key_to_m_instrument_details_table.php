<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMInstrumentDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_instrument_details', function($table)
		{
			$table->integer('instrument_type_id')->unsigned()->index()->change();
			$table->integer('instrument_subtype_id')->unsigned()->index()->change();
			$table->integer('instrument_unit_id')->unsigned()->index()->change();

			$table->foreign('instrument_type_id')
						->references('id')
						->on('s_instrument_type')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('instrument_subtype_id')
						->references('id')
						->on('s_instrument_subtype')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('instrument_unit_id')
						->references('id')
						->on('s_instrument_unit')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_instrument_details', function($table)
		{
			$table->dropForeign('instrument_type_id');
			$table->dropForeign('instrument_subtype_id');
			$table->dropForeign('instrument_unit_id');
		});
	}

}
