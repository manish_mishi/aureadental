<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTPurchaseOrderMachineTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_purchase_order_machine', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('machine_name_id')->unsigned()->index();
			$table->integer('quantity');
			$table->integer('purchase_order_id')->unsigned()->index();
			$table->timestamps();	
				
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_purchase_order_machine');	}

}
