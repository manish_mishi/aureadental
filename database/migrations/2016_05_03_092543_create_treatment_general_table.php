<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreatmentGeneralTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_treatment_general', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('patient_treatment_id')->unsigned()->index();
			$table->integer('treatment_name_id')->unsigned()->index();
			$table->integer('dentist_name_id')->unsigned()->index();
			$table->integer('status_id')->unsigned()->index();
			$table->integer('clinic_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_treatment_general');
	}

}
