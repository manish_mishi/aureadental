<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderVendorMaterialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_purchase_order_vendor_material', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('rate');
			$table->integer('amount');
			$table->integer('po_vendor_id')->unsigned()->index();
			$table->integer('material_order_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_purchase_order_vendor_material');
	}

}
