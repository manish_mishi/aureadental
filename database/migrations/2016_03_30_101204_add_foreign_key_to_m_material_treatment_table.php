<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMMaterialTreatmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_material_treatment', function($table)
		{
			$table->integer('treatment_type_id')->unsigned()->index()->change();

			$table->foreign('treatment_name_id')
						->references('id')
						->on('m_treatment_name')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('treatment_type_id')
						->references('id')
						->on('s_treatment_type')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_material_treatment', function($table)
		{
			$table->dropForeign('treatment_type_id');
			$table->dropForeign('treatment_name_id');
		});
	}

}
