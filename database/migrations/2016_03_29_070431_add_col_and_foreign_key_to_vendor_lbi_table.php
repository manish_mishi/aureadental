<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColAndForeignKeyToVendorLbiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_vendor_material_details',function($table)
		{
			$table->integer('vendor_type_id')->unsigned()->index()->after('email');
		});

		Schema::table('m_vendor_mat_workspec_details',function($table)
		{
			$table->integer('vendor_type_id')->unsigned()->index()->after('duration');
		});

		Schema::table('m_vendor_material_details', function($table)
		{
			$table->foreign('vendor_type_id')
						->references('id')
						->on('s_vendor_types')
						->onDelete('restrict')
						->onUpdate('restrict');
		});

		Schema::table('m_vendor_mat_workspec_details', function($table)
		{
			$table->foreign('vendor_type_id')
						->references('id')
						->on('s_vendor_types')
						->onDelete('restrict')
						->onUpdate('restrict');
		});

		Schema::table('m_vendor_lab_details',function($table)
		{
			$table->integer('vendor_type_id')->unsigned()->index()->after('email');
		});

		Schema::table('m_vendor_lab_workspec_details',function($table)
		{
			$table->integer('vendor_type_id')->unsigned()->index()->after('duration');
		});

		Schema::table('m_vendor_lab_details', function($table)
		{
			$table->foreign('vendor_type_id')
						->references('id')
						->on('s_vendor_types')
						->onDelete('restrict')
						->onUpdate('restrict');
		});

		Schema::table('m_vendor_lab_workspec_details', function($table)
		{
			$table->foreign('vendor_type_id')
						->references('id')
						->on('s_vendor_types')
						->onDelete('restrict')
						->onUpdate('restrict');
		});

		Schema::table('m_vendor_instrument_details',function($table)
		{
			$table->integer('vendor_type_id')->unsigned()->index()->after('email');
		});

		Schema::table('m_vendor_inst_workspec_details',function($table)
		{
			$table->integer('vendor_type_id')->unsigned()->index()->after('duration');
		});

		Schema::table('m_vendor_instrument_details', function($table)
		{
			$table->foreign('vendor_type_id')
						->references('id')
						->on('s_vendor_types')
						->onDelete('restrict')
						->onUpdate('restrict');
		});

		Schema::table('m_vendor_inst_workspec_details', function($table)
		{
			$table->foreign('vendor_type_id')
						->references('id')
						->on('s_vendor_types')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_vendor_material_details', function($table)
		{
			$table->dropForeign('vendor_type_id');
		});

		Schema::table('m_vendor_mat_workspec_details', function($table)
		{
			$table->dropForeign('vendor_type_id');
		});

		Schema::table('m_vendor_lab_details', function($table)
		{
			$table->dropForeign('vendor_type_id');
		});

		Schema::table('m_vendor_lab_workspec_details', function($table)
		{
			$table->dropForeign('vendor_type_id');
		});

		Schema::table('m_vendor_instrument_details', function($table)
		{
			$table->dropForeign('vendor_type_id');
		});

		Schema::table('m_vendor_inst_workspec_details', function($table)
		{
			$table->dropForeign('vendor_type_id');
		});
	}

}
