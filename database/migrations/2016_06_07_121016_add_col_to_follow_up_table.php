<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToFollowUpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('t_follow_up', function($table)
		{
			$table->string('date')->after('notes');
			$table->string('time')->after('date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('t_follow_up', function($table)
		{
			$table->dropcolumn('date');
			$table->dropcolumn('time');
		});
	}

}
