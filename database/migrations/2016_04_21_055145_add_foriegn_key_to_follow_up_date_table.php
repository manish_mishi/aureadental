<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForiegnKeyToFollowUpDateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('t_follow_up_date', function($table)
		{
			$table->foreign('follow_up_id')
						->references('id')
						->on('t_follow_up')
						->onDelete('restrict')
						->onUpdate('restrict');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('t_follow_up_date', function($table)
		{
			$table->dropForeign('follow_up_id');
		});
	}

}
