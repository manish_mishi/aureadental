<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForiegnKeyToClinicSlotTimingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_clinic_slot_timing', function($table)
		{
			$table->foreign('clinic_slot_id')
						->references('id')
						->on('m_clinic_slot_details')
						->onDelete('restrict')
						->onUpdate('restrict');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_clinic_slot_timing', function($table)
		{
			$table->dropForeign('clinic_slot_id');
		});
	}

}
