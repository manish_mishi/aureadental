<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientTreatmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_patient_treatment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('final_quotation');
			$table->integer('consultant_fees');
			$table->string('consultant_notes');
			$table->string('findings');
			$table->string('treatment_notes');
			$table->integer('treatment_showstopper_checklist_id')->unsigned()->index();
			$table->integer('patient_id')->unsigned()->index();
			$table->integer('treatment_name_id')->unsigned()->index();
			$table->integer('treatment_status_id')->unsigned()->index();
			$table->integer('clinic_id')->unsigned()->index();
			$table->integer('staff_id_dentist')->unsigned()->index();
			$table->integer('quotation_id')->unsigned()->index();
			$table->integer('referal_id')->unsigned()->index();
			$table->integer('staff_id_consultant')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_patient_treatment');
	}

}
