<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColFromAppointmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('t_appointment', function($table)
		{
			$table->dropcolumn('appointment_date');
			$table->dropcolumn('assistant_name');
			$table->dropcolumn('doctore_name');
			$table->dropcolumn('consultant_name');
			$table->dropcolumn('slot_details_id');
			$table->dropcolumn('machine_id');
			$table->dropcolumn('appointment_status_id');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
