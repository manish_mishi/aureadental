<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_material_details',function(Blueprint $table)
			{
				$table->increments('id');
				$table->String('material_name');
				$table->integer('material_type_id');
				$table->integer('material_subtype_id');
				$table->integer('material_unit_id');
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_material_details');
	}

}
