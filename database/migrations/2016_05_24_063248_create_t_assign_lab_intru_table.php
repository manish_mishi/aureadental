<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTAssignLabIntruTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_assign_lab_instru', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('instrument_name_id');
			$table->integer('assign_lab_work_id');
			$table->timestamps();	
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_assign_lab_instru');
	}

}
