<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignLabWorkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_assign_lab_work', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('work_allocation_date');
			$table->string('deadline');
			$table->string('lab_person_responsible');
			$table->integer('lab_work_name_id')->unsigned()->index();
			$table->integer('lab_work_status_id')->unsigned()->index();
			$table->integer('lab_form_id')->unsigned()->index();
			$table->integer('instrument_name_id')->unsigned()->index();
			$table->integer('staff_id')->unsigned()->index();
			$table->integer('treatment_lab_work_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_assign_lab_work');
	}

}
