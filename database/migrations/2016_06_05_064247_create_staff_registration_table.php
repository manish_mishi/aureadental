<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffRegistrationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_staff_registration', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('gender');
			$table->integer('cell_no');
			$table->string('date_of_birth');
			$table->string('email');
			$table->string('primary_clinic');
			$table->integer('staff_type_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_staff_registration');
	}

}
