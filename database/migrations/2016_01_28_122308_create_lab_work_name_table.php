<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabWorkNameTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('s_lab_work_name', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('lab_work_type_id')->unsigned()->index();
			$table->integer('lab_work_subtype_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('s_lab_work_name');
	}

}
