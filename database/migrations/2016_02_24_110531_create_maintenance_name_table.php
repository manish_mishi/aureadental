<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintenanceNameTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_maintenance_name', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('contract_start_date');
			$table->integer('contract_end_date');
			$table->integer('contract_amount');
			$table->integer('expected_availability_time');
			$table->string('gadget_name');
			$table->integer('duration');
			$table->integer('rates');


			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_maintenance_name');
	}

}
