<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMVendorLabworkspecsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_vendor_lab_workspec_details', function($table)
		{
			$table->integer('work_type_id')->unsigned()->index()->change();
			$table->integer('work_subtype_id')->unsigned()->index()->change();
			$table->integer('work_name_id')->unsigned()->index()->change();
			$table->integer('vendor_lab_id')->unsigned()->index()->change();

			$table->foreign('work_type_id')
						->references('id')
						->on('s_lab_work_type')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('work_subtype_id')
						->references('id')
						->on('s_lab_work_subtype')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('work_name_id')
						->references('id')
						->on('s_lab_work_name')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('vendor_lab_id')
						->references('id')
						->on('m_vendor_lab_details')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_vendor_lab_workspec_details', function($table)
		{
			$table->dropForeign('work_type_id');
			$table->dropForeign('work_subtype_id');
			$table->dropForeign('work_name_id');
			$table->dropForeign('vendor_lab_id');
		});
	}

}
