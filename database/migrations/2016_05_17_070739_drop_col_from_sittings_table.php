<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColFromSittingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('t_sittings', function($table)
		{
			$table->dropcolumn('appointment_id');
			$table->dropcolumn('material_name_id');
			$table->dropcolumn('instrument_name_id');
			$table->dropcolumn('gadget_id');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
