<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsToMachineDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_machine_details',function($table)
		{
			$table->string('machine_type_id')->after('machine_name');
			$table->string('machine_subtype_id')->after('machine_type_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_machine_details',function($table)
		{
			$table->dropcolumn('machine_type_id');
			$table->dropcolumn('machine_subtype_id');
		});
	}

}
