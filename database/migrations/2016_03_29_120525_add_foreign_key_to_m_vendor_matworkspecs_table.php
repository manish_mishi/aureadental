<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMVendorMatworkspecsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_vendor_mat_workspec_details', function($table)
		{
			$table->integer('mat_type_id')->unsigned()->index()->change();
			$table->integer('mat_subtype_id')->unsigned()->index()->change();
			$table->integer('mat_name_id')->unsigned()->index()->change();
			$table->integer('vendor_mat_id')->unsigned()->index()->change();

			$table->foreign('mat_type_id')
						->references('id')
						->on('s_material_type')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('mat_subtype_id')
						->references('id')
						->on('s_material_subtype')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('mat_name_id')
						->references('id')
						->on('m_material_details')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('vendor_mat_id')
						->references('id')
						->on('m_vendor_material_details')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_vendor_mat_workspec_details', function($table)
		{
			$table->dropForeign('mat_type_id');
			$table->dropForeign('mat_subtype_id');
			$table->dropForeign('mat_name_id');
			$table->dropForeign('vendor_mat_id');
		});
	}

}
