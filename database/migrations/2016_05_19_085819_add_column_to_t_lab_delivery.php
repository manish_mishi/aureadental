<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToTLabDelivery extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('t_lab_delivery',function($table)
		{
			
			$table->integer('t_assign_lab_work_id')->unsigned()->index()->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('t_lab_delivery',function($table)
		{
			$table->dropcolumn('t_assign_lab_work_id');
		});
	}

}
