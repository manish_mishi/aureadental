<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToSalaryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('t_salary', function($table)
		{
			$table->integer('total_deduction')->after('leaves');
			$table->integer('net_amount')->after('total_deduction');
			$table->string('bank_name')->after('net_amount');
			$table->string('account_no')->after('bank_name');
			$table->string('account_holder')->after('account_no');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('t_salary', function($table)
		{
			$table->dropcolumn('total_deduction');
			$table->dropcolumn('net_amount');
			$table->dropcolumn('bank_name');
			$table->dropcolumn('account_no');
			$table->dropcolumn('account_holder');
		});
	}

}
