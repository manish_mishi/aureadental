<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToInventorySubtypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('s_instrument_subtype', function($table)
		{
			$table->foreign('instrument_type_id')
						->references('id')
						->on('s_instrument_type')
						->onDelete('restrict')
						->onUpdate('restrict');
		});

		Schema::table('s_machine_subtype', function($table)
		{
			$table->foreign('machine_type_id')
						->references('id')
						->on('s_machine_type')
						->onDelete('restrict')
						->onUpdate('restrict');
		});

		Schema::table('s_gadget_subtype', function($table)
		{
			$table->foreign('gadget_type_id')
						->references('id')
						->on('s_gadget_type')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('s_instrument_subtype', function($table)
		{
			$table->dropForeign('instrument_type_id');
		});

		Schema::table('s_machine_subtype', function($table)
		{
			$table->dropForeign('machine_type_id');
		});

		Schema::table('s_gadget_subtype', function($table)
		{
			$table->dropForeign('gadget_type_id');
		});
	}

}
