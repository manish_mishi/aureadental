<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_salary', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('notes');
			$table->string('month_year');
			$table->string('date_of_payment');
			$table->string('salary');
			$table->string('advance_amount');
			$table->integer('leave');
			$table->integer('staff_id')->unsigned()->index();
			$table->integer('payment_mode_id')->unsigned()->index();
			/*$table->integer('leave_id')->unsigned()->index();*/
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_salary');
	}

}
