<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreatmentInstallmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_treatment_installments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('pending_amount');
			$table->integer('bill_number');
			$table->string('date_of_billing');
			$table->string('date_of_payment');
			$table->integer('treatment_billing_id')->unsigned()->index();
			$table->integer('payment_mode_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_treatment_installments');
	}

}
