<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_bank', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('account_holder');
			$table->string('bank_name');
			$table->integer('account_number');
			$table->string('branch');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_bank');
	}

}
