<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreatmentDiagnosisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_treatment_diagnosis', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('plan');
			$table->string('findings');
			$table->string('prescriptions');
			$table->string('notes');
			$table->integer('patient_treatment_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_treatment_diagnosis');
	}

}
