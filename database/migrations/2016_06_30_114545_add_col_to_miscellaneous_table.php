<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToMiscellaneousTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('miscellaneous',function($table)
		{

$table->string('bank_name')->after('date_of_payment');
$table->string('account_holder')->after('paid_to');
$table->integer('account_number')->after('amount');
$table->string('notes')->after('reason');


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('miscellaneous', function($table)
		{
			$table->dropcolumn('bank_name');
			$table->dropcolumn('account_holder');
			$table->dropcolumn('account_number');
			$table->dropcolumn('notes');
		});
	}

}
