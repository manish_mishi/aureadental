<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintenanceContractBillingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_maintenance_contract_billing', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('date_of_payment');
			$table->string('notes');
			$table->integer('vendor_work_specs_id')->unsigned()->index();
			$table->integer('payment_mode_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_maintenance_contract_billing');
	}

}
