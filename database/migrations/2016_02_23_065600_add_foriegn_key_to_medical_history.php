<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForiegnKeyToMedicalHistory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_medical_history', function($table)
		{
			$table->foreign('patient_id')
						->references('id')
						->on('m_patient_details')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_medical_history', function($table)
		{
			$table->dropForeign('patient_id');
		});
	}

}
