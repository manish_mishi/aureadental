<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMInstrumentAlertsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_instrument_alerts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('safety_stock_value');
			$table->string('time_before_expiry');
			$table->integer('instrument_name_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_instrument_alerts');
	}

}
