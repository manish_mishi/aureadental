<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTAssignMaitenanceMachine extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_assign_maintenance_machine', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('last_maintenance_date');
			$table->string('due_date');
			$table->integer('status');
			$table->string('set_maintenance_date');
			$table->string('time');
			$table->string('actual_date_of_maintenance');
			$table->string('actual_time');
			$table->string('actual_duration');
			$table->string('person_responsible');
			$table->string('cell_no');
			$table->string('remarks');
			$table->string('amount');
			$table->string('advance');
			$table->string('discount');
			$table->string('total_amount');
			$table->string('bill_no');
			$table->string('extra_charge_remarks');
			$table->integer('vendor_type_id')->unsigned()->index();
			$table->integer('vendor_id')->unsigned()->index();
			$table->timestamps();	
				
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_assign_maintenance_machine');
	}

}
