<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColAndForeignKeyToMVendorMaintenanceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_vendor_maintenance_details', function($table)
		{
			$table->foreign('vendor_type_id')
			->references('id')
			->on('s_vendor_types')
			->onDelete('restrict')
			->onUpdate('restrict');
		});

		Schema::table('m_vendor_maint_workspec_details', function($table)
		{
			$table->foreign('vendor_type_id')
			->references('id')
			->on('s_vendor_types')
			->onDelete('restrict')
			->onUpdate('restrict');
		});

		
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_vendor_maintenance_details', function($table)
		{
			$table->dropForeign('vendor_type_id');
		});

		Schema::table('m_vendor_maint_workspec_details', function($table)
		{
			$table->dropForeign('vendor_type_id');
		});
		
	}

}
