<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnFromPatientTreatmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('t_patient_treatment', function($table)
		{
			$table->dropColumn('final_quotation');
			$table->dropColumn('consultant_fees');
			$table->dropColumn('consultant_notes');
			$table->dropColumn('findings');
			$table->dropColumn('treatment_notes');
			$table->dropColumn('treatment_showstopper_checklist_id');
			$table->dropColumn('treatment_name_id');
			$table->dropColumn('treatment_status_id');
			$table->dropColumn('clinic_id');
			$table->dropColumn('staff_id_dentist');
			$table->dropColumn('quotation_id');
			$table->dropColumn('referal_id');
			$table->dropColumn('staff_id_consultant');
		});
	}


}
