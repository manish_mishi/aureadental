<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_clinic', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('location');
			$table->string('address');
			$table->string('time_from');
			$table->string('time_to');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_clinic');
	}

}
