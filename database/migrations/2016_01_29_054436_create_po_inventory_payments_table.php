<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoInventoryPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_po_inventory_payments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('date_of_payment');
			$table->integer('payment_mode_id')->unsigned()->index();
			$table->integer('billing_status_id')->unsigned()->index();
			$table->integer('po_billing_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_po_inventory_payments');
	}

}
