<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDropColTPurchaseOrderMaterial extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('t_purchase_order_material',function($table)
		{
			$table->dropcolumn('treatment_name_id');
			$table->integer('material_name_id')->after('id')->unsigned()->index();
			$table->integer('purchase_order_id')->after('quantity')->unsigned()->index();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
