<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicInfoPatientTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_patient_details', function(Blueprint $table)
		{
			$table->increments('id',20);
			$table->string('name',50);
			$table->string('cell_no');
			$table->string('gender',20);
			$table->string('email')->unique();
			$table->string('occupation',200);
			$table->string('address',500);
			$table->string('dob', 60);
			$table->integer('other_ref_id')->unsigned()->index();
			/*$table->foreign('other_ref_id')->references('id')->on('m_other_referals');*/
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('m_patient_details');
	}

}
