<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToSittingLabWorkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('t_sitting_lab_work',function($table)
		{
			$table->integer('work_type_id')->unsigned()->index()->after('lab_work_status_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('t_sitting_lab_work',function($table)
		{
			$table->dropColumn('work_type_id');
		});
	}

}
