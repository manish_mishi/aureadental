<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForiegnKeyConstraintsToMaterialSubtypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('s_material_subtype', function($table)
		{
			$table->foreign('material_type_id')
						->references('id')
						->on('s_material_type')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('s_material_subtype', function($table)
		{
			$table->dropForeign('material_type_id');
		});
	}

}
