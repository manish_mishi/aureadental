<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToMVendorMaintenanceworkspecTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('m_vendor_maint_workspec_details', function($table)
		{
			$table->foreign('gadget_name_id')
						->references('id')
						->on('m_gadget_details')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('machine_name_id')
						->references('id')
						->on('m_machine_details')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('vendor_maintenance_id')
						->references('id')
						->on('m_vendor_maintenance_details')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('m_vendor_maint_workspec_details', function($table)
		{
			$table->dropForeign('machine_name_id');
		   $table->dropForeign('gadget_name_id');

			$table->dropForeign('vendor_maintenance_id');
		});
	}

}
