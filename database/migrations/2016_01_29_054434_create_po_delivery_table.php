<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoDeliveryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_po_delivery', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('chalan_number');
			$table->string('delivery_date');
			$table->string('delivery_location');
			$table->integer('purchase_order_id')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_po_delivery');
	}

}
