<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTPurchaseOrderVendorInstrumentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('t_purchase_order_vendor_instrument', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('rate');
			$table->integer('amount');
			$table->integer('po_vendor_id');
			$table->integer('instrument_order_id');
			$table->timestamps();		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('t_purchase_order_vendor_instrument');
	}

}
