-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2016 at 11:59 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `aureadental_d8`
--

-- --------------------------------------------------------

--
-- Table structure for table `cheque`
--

CREATE TABLE IF NOT EXISTS `cheque` (
`id` int(10) unsigned NOT NULL,
  `cheque_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `followup_appointment_details`
--

CREATE TABLE IF NOT EXISTS `followup_appointment_details` (
`id` int(10) unsigned NOT NULL,
  `patient_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell_no` int(11) NOT NULL,
  `follow_up_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_01_14_113832_create_todo_table', 1),
('2016_01_28_054917_create_basic_info_patient_table', 1),
('2016_01_28_104013_create_medical_history_table', 1),
('2016_01_28_104108_create_patient_attachment_table', 1),
('2016_01_28_104300_create_referals_table', 1),
('2016_01_28_104353_create_other_referals_table', 1),
('2016_01_28_112104_create_vendor_type_table', 1),
('2016_01_28_112152_create_vendor_detail_table', 1),
('2016_01_28_121655_create_material_type_table', 1),
('2016_01_28_121656_create_material_subtype_table', 1),
('2016_01_28_121658_create_instrument_type_table', 1),
('2016_01_28_121658_create_material_treatment_table', 1),
('2016_01_28_121659_create_instrument_subtype_table', 1),
('2016_01_28_121701_create_instrument_treatment_table', 1),
('2016_01_28_121702_create_material_unit_table', 1),
('2016_01_28_121703_create_machine_details_table', 1),
('2016_01_28_121704_create_machine_treatment_table', 1),
('2016_01_28_121705_create_gadget_treatment_table', 1),
('2016_01_28_121706_create_purchase_order_table', 1),
('2016_01_28_121707_create_purchase_order_material_table', 1),
('2016_01_28_121708_create_purchase_order_vendor_table', 1),
('2016_01_28_121709_create_purchase_order_vendor_material_table', 1),
('2016_01_28_121710_create_material_alerts_table', 1),
('2016_01_28_121711_create_instrument_alerts_table', 1),
('2016_01_28_121711_create_purchase_order_status_table', 1),
('2016_01_28_121712_create_lab_instrument_status_table', 1),
('2016_01_28_121713_create_threshold_status_table', 1),
('2016_01_28_122307_create_lab_work_subtype_table', 1),
('2016_01_28_122307_create_lab_work_type_table', 1),
('2016_01_28_122308_create_lab_work_name_table', 1),
('2016_01_28_122309_create_lab_work_status_table', 1),
('2016_01_28_122310_create_assign_lab_work_table', 1),
('2016_01_28_122311_create_lab_form_table', 1),
('2016_01_29_043958_create_lab_delivery_table', 1),
('2016_01_29_043959_create_treatment_type_table', 1),
('2016_01_29_044000_create_treatment_name_table', 1),
('2016_01_29_044001_create_treatment_status_table', 1),
('2016_01_29_044002_create_treatment_showstopper_checklist_table', 1),
('2016_01_29_044003_create_patient_treatment_table', 1),
('2016_01_29_044004_create_quotation_table', 1),
('2016_01_29_044005_create_sittings_table', 1),
('2016_01_29_044006_create_sitting_lab_work_table', 1),
('2016_01_29_044007_create_treatment_attachments_table', 1),
('2016_01_29_044044_create_treatment_diagnosis_table', 1),
('2016_01_29_045140_create_maintenance_status_table', 1),
('2016_01_29_045141_create_assign_maintenance_table', 1),
('2016_01_29_045142_create_appointment_table', 1),
('2016_01_29_045143_create_appointment_status_table', 1),
('2016_01_29_045144_create_follow-up_status_table', 1),
('2016_01_29_045147_create_follow-up_table', 1),
('2016_01_29_050421_create_staff_type_table', 1),
('2016_01_29_050423_create_staff-clinic_table', 1),
('2016_01_29_050424_create_staff_slot_details_table', 1),
('2016_01_29_050425_create_staff_slot_timing_table', 1),
('2016_01_29_050426_create_clinic_type_table', 1),
('2016_01_29_050427_create_clinic_table', 1),
('2016_01_29_050428_create_clinic_slot_details_table', 1),
('2016_01_29_050429_create_clinic_slot_timing_table', 1),
('2016_01_29_050430_create_leave_full_day_table', 1),
('2016_01_29_050433_create_leave_partial_days_table', 1),
('2016_01_29_054429_create_payment_mode_table', 1),
('2016_01_29_054430_create_billing_status_table', 1),
('2016_01_29_054431_create_bank_table', 1),
('2016_01_29_054433_create_cheque_table', 1),
('2016_01_29_054434_create_po_delivery_table', 1),
('2016_01_29_054435_create_po_billing_table', 1),
('2016_01_29_054436_create_po_inventory_payments_table', 1),
('2016_01_29_054437_create_treatment_billing_table', 1),
('2016_01_29_054438_create_treatment_installments_table', 1),
('2016_01_29_054439_create_material_billing_table', 1),
('2016_01_29_054440_create_material_installments_table', 1),
('2016_01_29_054441_create_instrument_billing_table', 1),
('2016_01_29_054442_create_instrument_installments_table', 1),
('2016_01_29_054443_create_gadget_billing_table', 1),
('2016_01_29_054444_create_gadget_installments_table', 1),
('2016_01_29_054444_create_machine_billing_table', 1),
('2016_01_29_054445_create_machine_installments_table', 1),
('2016_01_29_054446_create_lab_billing_table', 1),
('2016_01_29_054447_create_lab_installments_table', 1),
('2016_01_29_054448_create_maintenance_contract_billing_table', 1),
('2016_01_29_054449_create_maintenance_service_billing_table', 1),
('2016_01_29_054450_create_salary_table', 1),
('2016_01_29_054451_create_cash_transfer_table', 1),
('2016_01_29_054452_create_transfer_detail_table', 1),
('2016_01_29_054457_create_miscellaneous_table', 1),
('2016_02_22_071048_create_instrument_unit_table', 1),
('2016_02_22_093226_create_machine_type_table', 1),
('2016_02_22_100751_create_machine_subtype_table', 1),
('2016_02_22_102107_add_details_and_attachments_to_medical_history', 1),
('2016_02_22_104710_create_machine_unit_table', 1),
('2016_02_23_044913_create_gadget_type_table', 1),
('2016_02_23_051241_create_gadget_subtype_table', 1),
('2016_02_23_065600_add_foriegn_key_to_medical_history', 1),
('2016_02_24_044845_create_material_details_table', 1),
('2016_02_24_075233_create_instrument_details_table', 1),
('2016_02_24_110531_create_maintenance_name_table', 1),
('2016_03_03_053130_create_drop_column_add_to_m_material_teratment_table', 1),
('2016_03_07_055324_create_gadget_details_table', 1),
('2016_03_11_055621_create_personal_table', 1),
('2016_03_11_065031_create_staff_registration_table', 1),
('2016_03_12_054254_add_col_to_instrument_treatment_table', 1),
('2016_03_14_052042_add_cols_to_machine_details_table', 1),
('2016_03_14_054928_add_col_to_machine_treatment_table', 1),
('2016_03_14_065841_add_cols_to_gadget_details_table', 1),
('2016_03_14_070354_add_col_to_gadget_treatment_table', 1),
('2016_03_21_072829_create_chair_details_table', 1),
('2016_03_21_090152_create_dentist_details_table', 1),
('2016_03_21_121356_create_schedule_details_table', 1),
('2016_03_22_120200_create_followup_appointments_table', 1),
('2016_03_23_063002_create_master_vendor_lab_details_table', 1),
('2016_03_25_064119_create_m_vendor_lab_workspec_details_table', 1),
('2016_03_26_043151_create_m_vendor_material_details_table', 1),
('2016_03_26_043220_create_m_vendor_material_workspec_details_table', 1),
('2016_03_26_043326_create_m_vendor_instrument_details_table', 1),
('2016_03_26_043341_create_m_vendor_instrument_workspec_details_table', 1),
('2016_03_28_114641_add_foriegn_key_constraints_to_material_subtype_table', 1),
('2016_03_29_053834_add_foreign_key_to_inventory_subtypes_table', 2),
('2016_03_29_070431_add_col_and_foreign_key_to_vendor_lbi_table', 3),
('2016_03_29_105857_add_foreign_key_to_m_vendor_labworkspecs_table', 4),
('2016_03_29_120525_add_foreign_key_to_m_vendor_matworkspecs_table', 5),
('2016_03_30_071935_add_foreign_key_to_m_treatment_table', 6),
('2016_03_30_101204_add_foreign_key_to_m_material_treatment_table', 7),
('2016_03_30_103530_add_foreign_key_to_m_material_details_table', 8),
('2016_04_01_061953_add_foreign_key_to_m_instrument_details_table', 9),
('2016_04_01_062016_add_foreign_key_to_m_instrument_treatment_table', 9),
('2016_04_01_113332_add_foreign_key_to_m_machine_details_table', 10),
('2016_04_01_113344_add_foreign_key_to_m_machine_treatment_table', 10),
('2016_04_04_055050_add_foreign_key_to_m_gadget_details_table', 11),
('2016_04_04_055111_add_foreign_key_to_m_gadget_treatment_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `miscellaneous`
--

CREATE TABLE IF NOT EXISTS `miscellaneous` (
`id` int(10) unsigned NOT NULL,
  `date_of_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paid_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_mode_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_bank`
--

CREATE TABLE IF NOT EXISTS `m_bank` (
`id` int(10) unsigned NOT NULL,
  `account_holder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_number` int(11) NOT NULL,
  `branch` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_clinic`
--

CREATE TABLE IF NOT EXISTS `m_clinic` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_clinic_slot_details`
--

CREATE TABLE IF NOT EXISTS `m_clinic_slot_details` (
`id` int(10) unsigned NOT NULL,
  `clinic-day` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `clinic_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_clinic_slot_timing`
--

CREATE TABLE IF NOT EXISTS `m_clinic_slot_timing` (
`id` int(10) unsigned NOT NULL,
  `slot_start_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slot_end_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `clinic_slot_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_clinic_type`
--

CREATE TABLE IF NOT EXISTS `m_clinic_type` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_gadget_details`
--

CREATE TABLE IF NOT EXISTS `m_gadget_details` (
`id` int(10) unsigned NOT NULL,
  `gadget_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gadget_type_id` int(10) unsigned NOT NULL,
  `gadget_subtype_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_gadget_details`
--

INSERT INTO `m_gadget_details` (`id`, `gadget_name`, `gadget_type_id`, `gadget_subtype_id`, `created_at`, `updated_at`) VALUES
(1, 'gs1', 1, 1, '2016-04-04 02:02:54', '2016-04-04 02:02:54');

-- --------------------------------------------------------

--
-- Table structure for table `m_gadget_treatment`
--

CREATE TABLE IF NOT EXISTS `m_gadget_treatment` (
`id` int(10) unsigned NOT NULL,
  `treatment_type_id` int(10) unsigned NOT NULL,
  `treatment_name_id` int(10) unsigned NOT NULL,
  `gadget_name_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_gadget_treatment`
--

INSERT INTO `m_gadget_treatment` (`id`, `treatment_type_id`, `treatment_name_id`, `gadget_name_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, '2016-04-04 02:04:53', '2016-04-04 02:04:53');

-- --------------------------------------------------------

--
-- Table structure for table `m_instrument_details`
--

CREATE TABLE IF NOT EXISTS `m_instrument_details` (
`id` int(10) unsigned NOT NULL,
  `instrument_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instrument_type_id` int(10) unsigned NOT NULL,
  `instrument_subtype_id` int(10) unsigned NOT NULL,
  `instrument_unit_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_instrument_details`
--

INSERT INTO `m_instrument_details` (`id`, `instrument_name`, `instrument_type_id`, `instrument_subtype_id`, `instrument_unit_id`, `created_at`, `updated_at`) VALUES
(1, 'int name1', 2, 2, 1, '2016-04-01 02:11:47', '2016-04-01 02:11:47'),
(2, 'inst name2', 2, 2, 1, '2016-04-01 04:47:45', '2016-04-01 04:47:45');

-- --------------------------------------------------------

--
-- Table structure for table `m_instrument_treatment`
--

CREATE TABLE IF NOT EXISTS `m_instrument_treatment` (
`id` int(10) unsigned NOT NULL,
  `treatment_type_id` int(10) unsigned NOT NULL,
  `treatment_name_id` int(10) unsigned NOT NULL,
  `instrument_name_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_instrument_treatment`
--

INSERT INTO `m_instrument_treatment` (`id`, `treatment_type_id`, `treatment_name_id`, `instrument_name_id`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 1, '2016-04-01 02:17:46', '2016-04-01 02:17:46'),
(2, 2, 3, 2, '2016-04-01 04:47:57', '2016-04-01 04:47:57');

-- --------------------------------------------------------

--
-- Table structure for table `m_leave_full_day`
--

CREATE TABLE IF NOT EXISTS `m_leave_full_day` (
`id` int(10) unsigned NOT NULL,
  `date_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `staff_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_leave_partial_days`
--

CREATE TABLE IF NOT EXISTS `m_leave_partial_days` (
`id` int(10) unsigned NOT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `staff_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_machine_details`
--

CREATE TABLE IF NOT EXISTS `m_machine_details` (
`id` int(10) unsigned NOT NULL,
  `machine_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `machine_type_id` int(10) unsigned NOT NULL,
  `machine_subtype_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_machine_details`
--

INSERT INTO `m_machine_details` (`id`, `machine_name`, `machine_type_id`, `machine_subtype_id`, `created_at`, `updated_at`) VALUES
(1, 'mac name1', 1, 1, '2016-04-01 06:48:48', '2016-04-01 06:48:48');

-- --------------------------------------------------------

--
-- Table structure for table `m_machine_treatment`
--

CREATE TABLE IF NOT EXISTS `m_machine_treatment` (
`id` int(10) unsigned NOT NULL,
  `treatment_type_id` int(10) unsigned NOT NULL,
  `treatment_name_id` int(10) unsigned NOT NULL,
  `machine_name_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_machine_treatment`
--

INSERT INTO `m_machine_treatment` (`id`, `treatment_type_id`, `treatment_name_id`, `machine_name_id`, `created_at`, `updated_at`) VALUES
(1, 2, 3, 1, '2016-04-01 06:52:23', '2016-04-01 06:52:23');

-- --------------------------------------------------------

--
-- Table structure for table `m_maintenance_name`
--

CREATE TABLE IF NOT EXISTS `m_maintenance_name` (
`id` int(10) unsigned NOT NULL,
  `contract_start_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contract_end_date` int(11) NOT NULL,
  `contract_amount` int(11) NOT NULL,
  `expected_availability_time` int(11) NOT NULL,
  `gadget_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `rates` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_material_alerts`
--

CREATE TABLE IF NOT EXISTS `m_material_alerts` (
`id` int(10) unsigned NOT NULL,
  `safety_stock_value` int(11) NOT NULL,
  `time_before_expiry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_name_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_material_details`
--

CREATE TABLE IF NOT EXISTS `m_material_details` (
`id` int(10) unsigned NOT NULL,
  `material_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_type_id` int(10) unsigned NOT NULL,
  `material_subtype_id` int(10) unsigned NOT NULL,
  `material_unit_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_material_details`
--

INSERT INTO `m_material_details` (`id`, `material_name`, `material_type_id`, `material_subtype_id`, `material_unit_id`, `created_at`, `updated_at`) VALUES
(1, 'mn2', 1, 1, 1, '2016-03-29 23:19:55', '2016-03-29 23:19:55'),
(2, 'mn2', 1, 1, 1, '2016-03-30 04:34:24', '2016-03-30 04:34:24'),
(3, 'test2', 1, 1, 1, '2016-03-31 22:56:08', '2016-03-31 22:56:08');

-- --------------------------------------------------------

--
-- Table structure for table `m_material_treatment`
--

CREATE TABLE IF NOT EXISTS `m_material_treatment` (
`id` int(10) unsigned NOT NULL,
  `treatment_type_id` int(10) unsigned NOT NULL,
  `treatment_name_id` int(10) unsigned NOT NULL,
  `material_name_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_material_treatment`
--

INSERT INTO `m_material_treatment` (`id`, `treatment_type_id`, `treatment_name_id`, `material_name_id`, `created_at`, `updated_at`) VALUES
(2, 2, 2, 2, '2016-03-30 04:53:44', '2016-03-30 04:53:44'),
(3, 2, 3, 3, '2016-03-31 22:56:16', '2016-03-31 22:56:16');

-- --------------------------------------------------------

--
-- Table structure for table `m_medical_history`
--

CREATE TABLE IF NOT EXISTS `m_medical_history` (
`id` int(10) unsigned NOT NULL,
  `medical_history` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attachments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `patient_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_other_referals`
--

CREATE TABLE IF NOT EXISTS `m_other_referals` (
`id` int(10) unsigned NOT NULL,
  `other_referals_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_patient_attachments`
--

CREATE TABLE IF NOT EXISTS `m_patient_attachments` (
`id` int(10) unsigned NOT NULL,
  `patient_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_patient_details`
--

CREATE TABLE IF NOT EXISTS `m_patient_details` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gender` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `occupation` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `dob` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `other_ref_id` int(10) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_personal`
--

CREATE TABLE IF NOT EXISTS `m_personal` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_referals`
--

CREATE TABLE IF NOT EXISTS `m_referals` (
`id` int(10) unsigned NOT NULL,
  `patient_id` int(10) unsigned NOT NULL,
  `refered_patient_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_staff-clinic`
--

CREATE TABLE IF NOT EXISTS `m_staff-clinic` (
`id` int(10) unsigned NOT NULL,
  `staff_id` int(10) unsigned NOT NULL,
  `clinic_id` int(10) unsigned NOT NULL,
  `clinic_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_staff_registration`
--

CREATE TABLE IF NOT EXISTS `m_staff_registration` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell_no` int(11) NOT NULL,
  `date_of_birth` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `primary_clinic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visiting_clinic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `staff_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_staff_slot_details`
--

CREATE TABLE IF NOT EXISTS `m_staff_slot_details` (
`id` int(10) unsigned NOT NULL,
  `staff-clinic_day` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `staff-clinic_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_staff_slot_timing`
--

CREATE TABLE IF NOT EXISTS `m_staff_slot_timing` (
`id` int(10) unsigned NOT NULL,
  `slot_start_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slot_end_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `staff_slot_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_treatment_attachments`
--

CREATE TABLE IF NOT EXISTS `m_treatment_attachments` (
`id` int(10) unsigned NOT NULL,
  `patient_treatment_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_treatment_diagnosis`
--

CREATE TABLE IF NOT EXISTS `m_treatment_diagnosis` (
`id` int(10) unsigned NOT NULL,
  `plan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `findings` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prescriptions` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `patient_treatment_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_treatment_name`
--

CREATE TABLE IF NOT EXISTS `m_treatment_name` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `treatment_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_treatment_name`
--

INSERT INTO `m_treatment_name` (`id`, `name`, `treatment_type_id`, `created_at`, `updated_at`) VALUES
(2, 'TN1', 2, '2016-03-30 01:41:50', '2016-03-30 01:41:50'),
(3, 'tn2', 2, '2016-03-30 01:42:16', '2016-03-30 01:42:16');

-- --------------------------------------------------------

--
-- Table structure for table `m_treatment_showstopper_checklist`
--

CREATE TABLE IF NOT EXISTS `m_treatment_showstopper_checklist` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_vendor_detail`
--

CREATE TABLE IF NOT EXISTS `m_vendor_detail` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` int(11) NOT NULL,
  `cell_no` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendor_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_vendor_instrument_details`
--

CREATE TABLE IF NOT EXISTS `m_vendor_instrument_details` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendor_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_vendor_inst_workspec_details`
--

CREATE TABLE IF NOT EXISTS `m_vendor_inst_workspec_details` (
`id` int(10) unsigned NOT NULL,
  `rate` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `vendor_type_id` int(10) unsigned NOT NULL,
  `inst_type_id` int(11) NOT NULL,
  `inst_subtype_id` int(11) NOT NULL,
  `inst_name_id` int(11) NOT NULL,
  `vendor_inst_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_vendor_lab_details`
--

CREATE TABLE IF NOT EXISTS `m_vendor_lab_details` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendor_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_vendor_lab_details`
--

INSERT INTO `m_vendor_lab_details` (`id`, `name`, `address`, `contact_person`, `cell_no`, `email`, `vendor_type_id`, `created_at`, `updated_at`) VALUES
(1, 'test1', 'dadar', 'amesh', '932655412', 'amy@gmail.com', 1, '2016-03-29 04:17:32', '2016-03-29 04:17:32'),
(2, 'renu mish', 'mumbai', 'Rane', '3256521', 'renu@gmail.com', 1, '2016-03-29 04:22:28', '2016-03-29 04:22:28'),
(3, 'test2', 'kalyan', 'jayesh', '74512451', 'jay@gmail.com', 1, '2016-03-29 06:23:03', '2016-03-29 06:23:03'),
(4, 'ret', '', '', '4354353454', '', 1, '2016-04-04 00:44:22', '2016-04-04 00:44:22'),
(6, 'erwdd', '', '', '346433465', 'rftg@ju.l', 1, '2016-04-04 00:48:01', '2016-04-04 00:48:01');

-- --------------------------------------------------------

--
-- Table structure for table `m_vendor_lab_workspec_details`
--

CREATE TABLE IF NOT EXISTS `m_vendor_lab_workspec_details` (
`id` int(10) unsigned NOT NULL,
  `rate` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `vendor_type_id` int(10) unsigned NOT NULL,
  `work_type_id` int(10) unsigned NOT NULL,
  `work_subtype_id` int(10) unsigned NOT NULL,
  `work_name_id` int(10) unsigned NOT NULL,
  `vendor_lab_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `m_vendor_lab_workspec_details`
--

INSERT INTO `m_vendor_lab_workspec_details` (`id`, `rate`, `duration`, `vendor_type_id`, `work_type_id`, `work_subtype_id`, `work_name_id`, `vendor_lab_id`, `created_at`, `updated_at`) VALUES
(1, 2511, 12, 1, 1, 1, 1, 2, '2016-03-29 04:22:39', '2016-03-29 04:22:39'),
(2, 5121, 222, 1, 1, 1, 1, 3, '2016-03-29 06:23:15', '2016-03-29 06:23:15');

-- --------------------------------------------------------

--
-- Table structure for table `m_vendor_material_details`
--

CREATE TABLE IF NOT EXISTS `m_vendor_material_details` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendor_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_vendor_mat_workspec_details`
--

CREATE TABLE IF NOT EXISTS `m_vendor_mat_workspec_details` (
`id` int(10) unsigned NOT NULL,
  `rate` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `vendor_type_id` int(10) unsigned NOT NULL,
  `mat_type_id` int(10) unsigned NOT NULL,
  `mat_subtype_id` int(10) unsigned NOT NULL,
  `mat_name_id` int(10) unsigned NOT NULL,
  `vendor_mat_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_details`
--

CREATE TABLE IF NOT EXISTS `schedule_details` (
`id` int(10) unsigned NOT NULL,
  `patient_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell_no` int(11) NOT NULL,
  `assign_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dentist_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assign_slot` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chair` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assistant` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `consultant` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_appointment_status`
--

CREATE TABLE IF NOT EXISTS `s_appointment_status` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_billing_status`
--

CREATE TABLE IF NOT EXISTS `s_billing_status` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_chair_details`
--

CREATE TABLE IF NOT EXISTS `s_chair_details` (
`id` int(10) unsigned NOT NULL,
  `chair_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_dentist_details`
--

CREATE TABLE IF NOT EXISTS `s_dentist_details` (
`id` int(10) unsigned NOT NULL,
  `dentist_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_follow-up`
--

CREATE TABLE IF NOT EXISTS `s_follow-up` (
`id` int(10) unsigned NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `follow-up_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `follow-up_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `patient_id` int(10) unsigned NOT NULL,
  `follow-up_status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_follow-up_status`
--

CREATE TABLE IF NOT EXISTS `s_follow-up_status` (
`id` int(10) unsigned NOT NULL,
  `follow-up_status_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_gadget_subtype`
--

CREATE TABLE IF NOT EXISTS `s_gadget_subtype` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gadget_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_gadget_subtype`
--

INSERT INTO `s_gadget_subtype` (`id`, `name`, `gadget_type_id`, `created_at`, `updated_at`) VALUES
(1, 'gs1', 1, '2016-03-29 00:37:59', '2016-03-29 00:37:59');

-- --------------------------------------------------------

--
-- Table structure for table `s_gadget_type`
--

CREATE TABLE IF NOT EXISTS `s_gadget_type` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_gadget_type`
--

INSERT INTO `s_gadget_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'gt1', '2016-03-29 00:37:50', '2016-03-29 00:37:50');

-- --------------------------------------------------------

--
-- Table structure for table `s_instrument_subtype`
--

CREATE TABLE IF NOT EXISTS `s_instrument_subtype` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instrument_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_instrument_subtype`
--

INSERT INTO `s_instrument_subtype` (`id`, `name`, `instrument_type_id`, `created_at`, `updated_at`) VALUES
(2, 'is2', 2, '2016-03-29 00:16:34', '2016-03-29 00:16:34');

-- --------------------------------------------------------

--
-- Table structure for table `s_instrument_type`
--

CREATE TABLE IF NOT EXISTS `s_instrument_type` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_instrument_type`
--

INSERT INTO `s_instrument_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'it1', '2016-03-29 00:15:13', '2016-03-29 00:15:13'),
(2, 'it2', '2016-03-29 00:15:20', '2016-03-29 00:15:20');

-- --------------------------------------------------------

--
-- Table structure for table `s_instrument_unit`
--

CREATE TABLE IF NOT EXISTS `s_instrument_unit` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_instrument_unit`
--

INSERT INTO `s_instrument_unit` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'meter', '2016-04-01 01:05:07', '2016-04-01 01:05:07');

-- --------------------------------------------------------

--
-- Table structure for table `s_lab_instrument_status`
--

CREATE TABLE IF NOT EXISTS `s_lab_instrument_status` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_lab_work_name`
--

CREATE TABLE IF NOT EXISTS `s_lab_work_name` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lab_work_type_id` int(10) unsigned NOT NULL,
  `lab_work_subtype_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_lab_work_name`
--

INSERT INTO `s_lab_work_name` (`id`, `name`, `lab_work_type_id`, `lab_work_subtype_id`, `created_at`, `updated_at`) VALUES
(1, 'wn1', 1, 1, '2016-03-29 04:21:52', '2016-03-29 04:21:52');

-- --------------------------------------------------------

--
-- Table structure for table `s_lab_work_status`
--

CREATE TABLE IF NOT EXISTS `s_lab_work_status` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_lab_work_subtype`
--

CREATE TABLE IF NOT EXISTS `s_lab_work_subtype` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lab_work_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_lab_work_subtype`
--

INSERT INTO `s_lab_work_subtype` (`id`, `name`, `lab_work_type_id`, `created_at`, `updated_at`) VALUES
(1, 'ws1', 1, '2016-03-29 04:21:40', '2016-03-29 04:21:40');

-- --------------------------------------------------------

--
-- Table structure for table `s_lab_work_type`
--

CREATE TABLE IF NOT EXISTS `s_lab_work_type` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_lab_work_type`
--

INSERT INTO `s_lab_work_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'wt1', '2016-03-29 00:39:38', '2016-03-29 00:39:38'),
(2, 'wt2', '2016-03-29 00:39:43', '2016-03-29 00:39:43');

-- --------------------------------------------------------

--
-- Table structure for table `s_machine_subtype`
--

CREATE TABLE IF NOT EXISTS `s_machine_subtype` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `machine_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_machine_subtype`
--

INSERT INTO `s_machine_subtype` (`id`, `name`, `machine_type_id`, `created_at`, `updated_at`) VALUES
(1, 'mcst1', 1, '2016-03-29 00:37:42', '2016-03-29 00:37:42');

-- --------------------------------------------------------

--
-- Table structure for table `s_machine_type`
--

CREATE TABLE IF NOT EXISTS `s_machine_type` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_machine_type`
--

INSERT INTO `s_machine_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'mct1', '2016-03-29 00:37:25', '2016-03-29 00:37:25');

-- --------------------------------------------------------

--
-- Table structure for table `s_machine_unit`
--

CREATE TABLE IF NOT EXISTS `s_machine_unit` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_maintenance_status`
--

CREATE TABLE IF NOT EXISTS `s_maintenance_status` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_material_subtype`
--

CREATE TABLE IF NOT EXISTS `s_material_subtype` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_type_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_material_subtype`
--

INSERT INTO `s_material_subtype` (`id`, `name`, `material_type_id`, `created_at`, `updated_at`) VALUES
(1, 'ms1', 1, '2016-03-29 00:28:42', '2016-03-29 00:28:42'),
(2, 'ms2', 1, '2016-03-29 23:14:00', '2016-03-29 23:14:00');

-- --------------------------------------------------------

--
-- Table structure for table `s_material_type`
--

CREATE TABLE IF NOT EXISTS `s_material_type` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_material_type`
--

INSERT INTO `s_material_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'mt1', '2016-03-29 00:28:21', '2016-03-29 00:28:21');

-- --------------------------------------------------------

--
-- Table structure for table `s_material_unit`
--

CREATE TABLE IF NOT EXISTS `s_material_unit` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_material_unit`
--

INSERT INTO `s_material_unit` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'kg', '2016-03-29 23:14:55', '2016-03-29 23:14:55');

-- --------------------------------------------------------

--
-- Table structure for table `s_payment_mode`
--

CREATE TABLE IF NOT EXISTS `s_payment_mode` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_purchase_order_status`
--

CREATE TABLE IF NOT EXISTS `s_purchase_order_status` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_staff_type`
--

CREATE TABLE IF NOT EXISTS `s_staff_type` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_threshold_status`
--

CREATE TABLE IF NOT EXISTS `s_threshold_status` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `s_treatment_status`
--

CREATE TABLE IF NOT EXISTS `s_treatment_status` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_treatment_status`
--

INSERT INTO `s_treatment_status` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'ts2', '2016-03-30 01:00:37', '2016-03-30 01:00:37');

-- --------------------------------------------------------

--
-- Table structure for table `s_treatment_type`
--

CREATE TABLE IF NOT EXISTS `s_treatment_type` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_treatment_type`
--

INSERT INTO `s_treatment_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'tt2', '2016-03-30 00:23:34', '2016-03-30 00:23:34');

-- --------------------------------------------------------

--
-- Table structure for table `s_vendor_types`
--

CREATE TABLE IF NOT EXISTS `s_vendor_types` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `s_vendor_types`
--

INSERT INTO `s_vendor_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Lab', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Material', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Instrument', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Gadget', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Machine', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Maintenance', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_appointment`
--

CREATE TABLE IF NOT EXISTS `t_appointment` (
`id` int(10) unsigned NOT NULL,
  `appointment_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assistant_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `doctore_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `consultant_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slot_details_id` int(10) unsigned NOT NULL,
  `machine_id` int(10) unsigned NOT NULL,
  `appointment_status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_assign_lab_work`
--

CREATE TABLE IF NOT EXISTS `t_assign_lab_work` (
`id` int(10) unsigned NOT NULL,
  `work_allocation_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deadline` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lab_person_responsible` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lab_work_name_id` int(10) unsigned NOT NULL,
  `lab_work_status_id` int(10) unsigned NOT NULL,
  `lab_form_id` int(10) unsigned NOT NULL,
  `instrument_name_id` int(10) unsigned NOT NULL,
  `staff_id` int(10) unsigned NOT NULL,
  `treatment_lab_work_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_assign_maintenance`
--

CREATE TABLE IF NOT EXISTS `t_assign_maintenance` (
`id` int(10) unsigned NOT NULL,
  `last_maintenance_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `due_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `set_maintenance_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `actual_date_of_maintenance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `actual_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `actual_duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `person_responsible` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell_no` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `advance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bill_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extra_charge_remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendor_types_id` int(10) unsigned NOT NULL,
  `gadget_id` int(10) unsigned NOT NULL,
  `machine_id` int(10) unsigned NOT NULL,
  `vendor_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_cash_transfer`
--

CREATE TABLE IF NOT EXISTS `t_cash_transfer` (
`id` int(10) unsigned NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `available_amount` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `transfer_detail_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_gadget_billing`
--

CREATE TABLE IF NOT EXISTS `t_gadget_billing` (
`id` int(10) unsigned NOT NULL,
  `purchase_order_id` int(10) unsigned NOT NULL,
  `billing_status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_gadget_installments`
--

CREATE TABLE IF NOT EXISTS `t_gadget_installments` (
`id` int(10) unsigned NOT NULL,
  `amount` int(11) NOT NULL,
  `bill_number` int(11) NOT NULL,
  `date_of_billing` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gadget_billing_id` int(10) unsigned NOT NULL,
  `payment_mode_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_instrument_billing`
--

CREATE TABLE IF NOT EXISTS `t_instrument_billing` (
`id` int(10) unsigned NOT NULL,
  `purchase_order_id` int(10) unsigned NOT NULL,
  `billing_status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_instrument_installments`
--

CREATE TABLE IF NOT EXISTS `t_instrument_installments` (
`id` int(10) unsigned NOT NULL,
  `amount` int(11) NOT NULL,
  `bill_number` int(11) NOT NULL,
  `date_of_billing` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instrument_billing_id` int(10) unsigned NOT NULL,
  `payment_mode_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_lab_billing`
--

CREATE TABLE IF NOT EXISTS `t_lab_billing` (
`id` int(10) unsigned NOT NULL,
  `final_cost` int(11) NOT NULL,
  `assign_lab_work_id` int(10) unsigned NOT NULL,
  `lab_delivery_id` int(10) unsigned NOT NULL,
  `billing_status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_lab_delivery`
--

CREATE TABLE IF NOT EXISTS `t_lab_delivery` (
`id` int(10) unsigned NOT NULL,
  `chalan_number` int(11) NOT NULL,
  `delivery_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_lab_form`
--

CREATE TABLE IF NOT EXISTS `t_lab_form` (
`id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_lab_installments`
--

CREATE TABLE IF NOT EXISTS `t_lab_installments` (
`id` int(10) unsigned NOT NULL,
  `amount` int(11) NOT NULL,
  `bill_number` int(11) NOT NULL,
  `date_of_billing` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lab_billing_id` int(10) unsigned NOT NULL,
  `payment_mode_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_machine_billing`
--

CREATE TABLE IF NOT EXISTS `t_machine_billing` (
`id` int(10) unsigned NOT NULL,
  `purchase_order_id` int(10) unsigned NOT NULL,
  `billing_status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_machine_installments`
--

CREATE TABLE IF NOT EXISTS `t_machine_installments` (
`id` int(10) unsigned NOT NULL,
  `amount` int(11) NOT NULL,
  `bill_number` int(11) NOT NULL,
  `date_of_billing` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `machine_billing_id` int(10) unsigned NOT NULL,
  `payment_mode_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_maintenance_contract_billing`
--

CREATE TABLE IF NOT EXISTS `t_maintenance_contract_billing` (
`id` int(10) unsigned NOT NULL,
  `date_of_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendor_work_specs_id` int(10) unsigned NOT NULL,
  `payment_mode_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_maintenance_service_billing`
--

CREATE TABLE IF NOT EXISTS `t_maintenance_service_billing` (
`id` int(10) unsigned NOT NULL,
  `date_of_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assign_maintenance_id` int(10) unsigned NOT NULL,
  `payment_mode_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_material_billing`
--

CREATE TABLE IF NOT EXISTS `t_material_billing` (
`id` int(10) unsigned NOT NULL,
  `purchase_order_id` int(10) unsigned NOT NULL,
  `billing_status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_material_installments`
--

CREATE TABLE IF NOT EXISTS `t_material_installments` (
`id` int(10) unsigned NOT NULL,
  `amount` int(11) NOT NULL,
  `bill_number` int(11) NOT NULL,
  `date_of_billing` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `material_billing_id` int(10) unsigned NOT NULL,
  `payment_mode_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_patient_treatment`
--

CREATE TABLE IF NOT EXISTS `t_patient_treatment` (
`id` int(10) unsigned NOT NULL,
  `final_quotation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `consultant_fees` int(11) NOT NULL,
  `consultant_notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `findings` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `treatment_notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `treatment_showstopper_checklist_id` int(10) unsigned NOT NULL,
  `patient_id` int(10) unsigned NOT NULL,
  `treatment_name_id` int(10) unsigned NOT NULL,
  `treatment_status_id` int(10) unsigned NOT NULL,
  `clinic_id` int(10) unsigned NOT NULL,
  `staff_id_dentist` int(10) unsigned NOT NULL,
  `quotation_id` int(10) unsigned NOT NULL,
  `referal_id` int(10) unsigned NOT NULL,
  `staff_id_consultant` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_po_billing`
--

CREATE TABLE IF NOT EXISTS `t_po_billing` (
`id` int(10) unsigned NOT NULL,
  `bill_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_billing` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `advance` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `purchase_order_status_id` int(10) unsigned NOT NULL,
  `purchase_order_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_po_delivery`
--

CREATE TABLE IF NOT EXISTS `t_po_delivery` (
`id` int(10) unsigned NOT NULL,
  `chalan_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `purchase_order_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_po_inventory_payments`
--

CREATE TABLE IF NOT EXISTS `t_po_inventory_payments` (
`id` int(10) unsigned NOT NULL,
  `date_of_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_mode_id` int(10) unsigned NOT NULL,
  `billing_status_id` int(10) unsigned NOT NULL,
  `po_billing_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_purchase_order`
--

CREATE TABLE IF NOT EXISTS `t_purchase_order` (
`id` int(10) unsigned NOT NULL,
  `date_of_order` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expected_date_of_delivery` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `purchase_order_status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_purchase_order_material`
--

CREATE TABLE IF NOT EXISTS `t_purchase_order_material` (
`id` int(10) unsigned NOT NULL,
  `quantity` int(11) NOT NULL,
  `gadget_id` int(10) unsigned NOT NULL,
  `treatment_name_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_purchase_order_vendor`
--

CREATE TABLE IF NOT EXISTS `t_purchase_order_vendor` (
`id` int(10) unsigned NOT NULL,
  `total_amount` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `final_amount` int(11) NOT NULL,
  `vendor_name_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_purchase_order_vendor_material`
--

CREATE TABLE IF NOT EXISTS `t_purchase_order_vendor_material` (
`id` int(10) unsigned NOT NULL,
  `rate` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `po_vendor_id` int(10) unsigned NOT NULL,
  `material_order_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_quotation`
--

CREATE TABLE IF NOT EXISTS `t_quotation` (
`id` int(10) unsigned NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `treatment_type_id` int(10) unsigned NOT NULL,
  `treatment_name_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_salary`
--

CREATE TABLE IF NOT EXISTS `t_salary` (
`id` int(10) unsigned NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `month_year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `basic_salary` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `advance_amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `staff_id` int(10) unsigned NOT NULL,
  `payment_mode_id` int(10) unsigned NOT NULL,
  `leave_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_sittings`
--

CREATE TABLE IF NOT EXISTS `t_sittings` (
`id` int(10) unsigned NOT NULL,
  `patient_treatment_id` int(10) unsigned NOT NULL,
  `appointment_id` int(10) unsigned NOT NULL,
  `material_name_id` int(10) unsigned NOT NULL,
  `instrument_name_id` int(10) unsigned NOT NULL,
  `gadget_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_sitting_lab_work`
--

CREATE TABLE IF NOT EXISTS `t_sitting_lab_work` (
`id` int(10) unsigned NOT NULL,
  `Job Date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sittings_id` int(10) unsigned NOT NULL,
  `patient_treatment_id` int(10) unsigned NOT NULL,
  `lab_work_status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_transfer_detail`
--

CREATE TABLE IF NOT EXISTS `t_transfer_detail` (
`id` int(10) unsigned NOT NULL,
  `Option` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_treatment_billing`
--

CREATE TABLE IF NOT EXISTS `t_treatment_billing` (
`id` int(10) unsigned NOT NULL,
  `patient_treatment_id` int(10) unsigned NOT NULL,
  `quotation_id` int(10) unsigned NOT NULL,
  `billing_status_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_treatment_installments`
--

CREATE TABLE IF NOT EXISTS `t_treatment_installments` (
`id` int(10) unsigned NOT NULL,
  `amount` int(11) NOT NULL,
  `bill_number` int(11) NOT NULL,
  `date_of_billing` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `treatment_billing_id` int(10) unsigned NOT NULL,
  `payment_mode_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'aureatech', 'aureatech@solutions.com', '$2y$10$5L.UacwqXm/EjzZiyirBMOHPbTWF1leK8k3WqmDHiOEOrs9BdowWC', NULL, '2016-03-29 00:06:40', '2016-03-29 00:06:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cheque`
--
ALTER TABLE `cheque`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `followup_appointment_details`
--
ALTER TABLE `followup_appointment_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `miscellaneous`
--
ALTER TABLE `miscellaneous`
 ADD PRIMARY KEY (`id`), ADD KEY `miscellaneous_payment_mode_id_index` (`payment_mode_id`);

--
-- Indexes for table `m_bank`
--
ALTER TABLE `m_bank`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_clinic`
--
ALTER TABLE `m_clinic`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_clinic_slot_details`
--
ALTER TABLE `m_clinic_slot_details`
 ADD PRIMARY KEY (`id`), ADD KEY `m_clinic_slot_details_clinic_id_index` (`clinic_id`);

--
-- Indexes for table `m_clinic_slot_timing`
--
ALTER TABLE `m_clinic_slot_timing`
 ADD PRIMARY KEY (`id`), ADD KEY `m_clinic_slot_timing_clinic_slot_id_index` (`clinic_slot_id`);

--
-- Indexes for table `m_clinic_type`
--
ALTER TABLE `m_clinic_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_gadget_details`
--
ALTER TABLE `m_gadget_details`
 ADD PRIMARY KEY (`id`), ADD KEY `m_gadget_details_gadget_type_id_index` (`gadget_type_id`), ADD KEY `m_gadget_details_gadget_subtype_id_index` (`gadget_subtype_id`);

--
-- Indexes for table `m_gadget_treatment`
--
ALTER TABLE `m_gadget_treatment`
 ADD PRIMARY KEY (`id`), ADD KEY `m_gadget_treatment_gadget_id_index` (`gadget_name_id`), ADD KEY `m_gadget_treatment_treatment_name_id_index` (`treatment_name_id`), ADD KEY `m_gadget_treatment_treatment_type_id_index` (`treatment_type_id`);

--
-- Indexes for table `m_instrument_details`
--
ALTER TABLE `m_instrument_details`
 ADD PRIMARY KEY (`id`), ADD KEY `m_instrument_details_instrument_type_id_index` (`instrument_type_id`), ADD KEY `m_instrument_details_instrument_subtype_id_index` (`instrument_subtype_id`), ADD KEY `m_instrument_details_instrument_unit_id_index` (`instrument_unit_id`);

--
-- Indexes for table `m_instrument_treatment`
--
ALTER TABLE `m_instrument_treatment`
 ADD PRIMARY KEY (`id`), ADD KEY `m_instrument_treatment_instrument_name_id_index` (`instrument_name_id`), ADD KEY `m_instrument_treatment_treatment_name_id_index` (`treatment_name_id`), ADD KEY `m_instrument_treatment_treatment_type_id_index` (`treatment_type_id`);

--
-- Indexes for table `m_leave_full_day`
--
ALTER TABLE `m_leave_full_day`
 ADD PRIMARY KEY (`id`), ADD KEY `m_leave_full_day_staff_id_index` (`staff_id`);

--
-- Indexes for table `m_leave_partial_days`
--
ALTER TABLE `m_leave_partial_days`
 ADD PRIMARY KEY (`id`), ADD KEY `m_leave_partial_days_staff_id_index` (`staff_id`);

--
-- Indexes for table `m_machine_details`
--
ALTER TABLE `m_machine_details`
 ADD PRIMARY KEY (`id`), ADD KEY `m_machine_details_machine_type_id_index` (`machine_type_id`), ADD KEY `m_machine_details_machine_subtype_id_index` (`machine_subtype_id`);

--
-- Indexes for table `m_machine_treatment`
--
ALTER TABLE `m_machine_treatment`
 ADD PRIMARY KEY (`id`), ADD KEY `m_machine_treatment_machine_id_index` (`machine_name_id`), ADD KEY `m_machine_treatment_treatment_name_id_index` (`treatment_name_id`), ADD KEY `m_machine_treatment_treatment_type_id_index` (`treatment_type_id`);

--
-- Indexes for table `m_maintenance_name`
--
ALTER TABLE `m_maintenance_name`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_material_alerts`
--
ALTER TABLE `m_material_alerts`
 ADD PRIMARY KEY (`id`), ADD KEY `m_material_alerts_material_name_id_index` (`material_name_id`);

--
-- Indexes for table `m_material_details`
--
ALTER TABLE `m_material_details`
 ADD PRIMARY KEY (`id`), ADD KEY `m_material_details_material_type_id_index` (`material_type_id`), ADD KEY `m_material_details_material_subtype_id_index` (`material_subtype_id`), ADD KEY `m_material_details_material_unit_id_index` (`material_unit_id`);

--
-- Indexes for table `m_material_treatment`
--
ALTER TABLE `m_material_treatment`
 ADD PRIMARY KEY (`id`), ADD KEY `m_material_treatment_material_name_id_index` (`material_name_id`), ADD KEY `m_material_treatment_treatment_name_id_index` (`treatment_name_id`), ADD KEY `m_material_treatment_treatment_type_id_index` (`treatment_type_id`);

--
-- Indexes for table `m_medical_history`
--
ALTER TABLE `m_medical_history`
 ADD PRIMARY KEY (`id`), ADD KEY `m_medical_history_patient_id_index` (`patient_id`);

--
-- Indexes for table `m_other_referals`
--
ALTER TABLE `m_other_referals`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_patient_attachments`
--
ALTER TABLE `m_patient_attachments`
 ADD PRIMARY KEY (`id`), ADD KEY `m_patient_attachments_patient_id_index` (`patient_id`);

--
-- Indexes for table `m_patient_details`
--
ALTER TABLE `m_patient_details`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `m_patient_details_email_unique` (`email`), ADD KEY `m_patient_details_other_ref_id_index` (`other_ref_id`);

--
-- Indexes for table `m_personal`
--
ALTER TABLE `m_personal`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_referals`
--
ALTER TABLE `m_referals`
 ADD PRIMARY KEY (`id`), ADD KEY `m_referals_patient_id_index` (`patient_id`), ADD KEY `m_referals_refered_patient_id_index` (`refered_patient_id`);

--
-- Indexes for table `m_staff-clinic`
--
ALTER TABLE `m_staff-clinic`
 ADD PRIMARY KEY (`id`), ADD KEY `m_staff_clinic_staff_id_index` (`staff_id`), ADD KEY `m_staff_clinic_clinic_id_index` (`clinic_id`), ADD KEY `m_staff_clinic_clinic_type_id_index` (`clinic_type_id`);

--
-- Indexes for table `m_staff_registration`
--
ALTER TABLE `m_staff_registration`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_staff_slot_details`
--
ALTER TABLE `m_staff_slot_details`
 ADD PRIMARY KEY (`id`), ADD KEY `m_staff_slot_details_staff_clinic_id_index` (`staff-clinic_id`);

--
-- Indexes for table `m_staff_slot_timing`
--
ALTER TABLE `m_staff_slot_timing`
 ADD PRIMARY KEY (`id`), ADD KEY `m_staff_slot_timing_staff_slot_id_index` (`staff_slot_id`);

--
-- Indexes for table `m_treatment_attachments`
--
ALTER TABLE `m_treatment_attachments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_treatment_diagnosis`
--
ALTER TABLE `m_treatment_diagnosis`
 ADD PRIMARY KEY (`id`), ADD KEY `m_treatment_diagnosis_patient_treatment_id_index` (`patient_treatment_id`);

--
-- Indexes for table `m_treatment_name`
--
ALTER TABLE `m_treatment_name`
 ADD PRIMARY KEY (`id`), ADD KEY `m_treatment_name_treatment_type_id_index` (`treatment_type_id`);

--
-- Indexes for table `m_treatment_showstopper_checklist`
--
ALTER TABLE `m_treatment_showstopper_checklist`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_vendor_detail`
--
ALTER TABLE `m_vendor_detail`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `m_vendor_detail_email_unique` (`email`), ADD KEY `m_vendor_detail_vendor_type_id_index` (`vendor_type_id`);

--
-- Indexes for table `m_vendor_instrument_details`
--
ALTER TABLE `m_vendor_instrument_details`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `m_vendor_instrument_details_email_unique` (`email`), ADD KEY `m_vendor_instrument_details_vendor_type_id_index` (`vendor_type_id`);

--
-- Indexes for table `m_vendor_inst_workspec_details`
--
ALTER TABLE `m_vendor_inst_workspec_details`
 ADD PRIMARY KEY (`id`), ADD KEY `m_vendor_inst_workspec_details_vendor_type_id_index` (`vendor_type_id`);

--
-- Indexes for table `m_vendor_lab_details`
--
ALTER TABLE `m_vendor_lab_details`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `m_vendor_lab_details_email_unique` (`email`), ADD KEY `m_vendor_lab_details_vendor_type_id_index` (`vendor_type_id`);

--
-- Indexes for table `m_vendor_lab_workspec_details`
--
ALTER TABLE `m_vendor_lab_workspec_details`
 ADD PRIMARY KEY (`id`), ADD KEY `m_vendor_lab_workspec_details_vendor_type_id_index` (`vendor_type_id`), ADD KEY `m_vendor_lab_workspec_details_work_type_id_index` (`work_type_id`), ADD KEY `m_vendor_lab_workspec_details_work_subtype_id_index` (`work_subtype_id`), ADD KEY `m_vendor_lab_workspec_details_work_name_id_index` (`work_name_id`), ADD KEY `m_vendor_lab_workspec_details_vendor_lab_id_index` (`vendor_lab_id`);

--
-- Indexes for table `m_vendor_material_details`
--
ALTER TABLE `m_vendor_material_details`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `m_vendor_material_details_email_unique` (`email`), ADD KEY `m_vendor_material_details_vendor_type_id_index` (`vendor_type_id`);

--
-- Indexes for table `m_vendor_mat_workspec_details`
--
ALTER TABLE `m_vendor_mat_workspec_details`
 ADD PRIMARY KEY (`id`), ADD KEY `m_vendor_mat_workspec_details_vendor_type_id_index` (`vendor_type_id`), ADD KEY `m_vendor_mat_workspec_details_mat_type_id_index` (`mat_type_id`), ADD KEY `m_vendor_mat_workspec_details_mat_subtype_id_index` (`mat_subtype_id`), ADD KEY `m_vendor_mat_workspec_details_mat_name_id_index` (`mat_name_id`), ADD KEY `m_vendor_mat_workspec_details_vendor_mat_id_index` (`vendor_mat_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `schedule_details`
--
ALTER TABLE `schedule_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_appointment_status`
--
ALTER TABLE `s_appointment_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_billing_status`
--
ALTER TABLE `s_billing_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_chair_details`
--
ALTER TABLE `s_chair_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_dentist_details`
--
ALTER TABLE `s_dentist_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_follow-up`
--
ALTER TABLE `s_follow-up`
 ADD PRIMARY KEY (`id`), ADD KEY `s_follow_up_patient_id_index` (`patient_id`), ADD KEY `s_follow_up_follow_up_status_id_index` (`follow-up_status_id`);

--
-- Indexes for table `s_follow-up_status`
--
ALTER TABLE `s_follow-up_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_gadget_subtype`
--
ALTER TABLE `s_gadget_subtype`
 ADD PRIMARY KEY (`id`), ADD KEY `s_gadget_subtype_gadget_type_id_index` (`gadget_type_id`);

--
-- Indexes for table `s_gadget_type`
--
ALTER TABLE `s_gadget_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_instrument_subtype`
--
ALTER TABLE `s_instrument_subtype`
 ADD PRIMARY KEY (`id`), ADD KEY `s_instrument_subtype_instrument_type_id_index` (`instrument_type_id`);

--
-- Indexes for table `s_instrument_type`
--
ALTER TABLE `s_instrument_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_instrument_unit`
--
ALTER TABLE `s_instrument_unit`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_lab_instrument_status`
--
ALTER TABLE `s_lab_instrument_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_lab_work_name`
--
ALTER TABLE `s_lab_work_name`
 ADD PRIMARY KEY (`id`), ADD KEY `s_lab_work_name_lab_work_type_id_index` (`lab_work_type_id`), ADD KEY `s_lab_work_name_lab_work_subtype_id_index` (`lab_work_subtype_id`);

--
-- Indexes for table `s_lab_work_status`
--
ALTER TABLE `s_lab_work_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_lab_work_subtype`
--
ALTER TABLE `s_lab_work_subtype`
 ADD PRIMARY KEY (`id`), ADD KEY `s_lab_work_subtype_lab_work_type_id_index` (`lab_work_type_id`);

--
-- Indexes for table `s_lab_work_type`
--
ALTER TABLE `s_lab_work_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_machine_subtype`
--
ALTER TABLE `s_machine_subtype`
 ADD PRIMARY KEY (`id`), ADD KEY `s_machine_subtype_machine_type_id_index` (`machine_type_id`);

--
-- Indexes for table `s_machine_type`
--
ALTER TABLE `s_machine_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_machine_unit`
--
ALTER TABLE `s_machine_unit`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_maintenance_status`
--
ALTER TABLE `s_maintenance_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_material_subtype`
--
ALTER TABLE `s_material_subtype`
 ADD PRIMARY KEY (`id`), ADD KEY `s_material_subtype_material_type_id_index` (`material_type_id`);

--
-- Indexes for table `s_material_type`
--
ALTER TABLE `s_material_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_material_unit`
--
ALTER TABLE `s_material_unit`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_payment_mode`
--
ALTER TABLE `s_payment_mode`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_purchase_order_status`
--
ALTER TABLE `s_purchase_order_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_staff_type`
--
ALTER TABLE `s_staff_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_threshold_status`
--
ALTER TABLE `s_threshold_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_treatment_status`
--
ALTER TABLE `s_treatment_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_treatment_type`
--
ALTER TABLE `s_treatment_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_vendor_types`
--
ALTER TABLE `s_vendor_types`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_appointment`
--
ALTER TABLE `t_appointment`
 ADD PRIMARY KEY (`id`), ADD KEY `t_appointment_slot_details_id_index` (`slot_details_id`), ADD KEY `t_appointment_machine_id_index` (`machine_id`), ADD KEY `t_appointment_appointment_status_id_index` (`appointment_status_id`);

--
-- Indexes for table `t_assign_lab_work`
--
ALTER TABLE `t_assign_lab_work`
 ADD PRIMARY KEY (`id`), ADD KEY `t_assign_lab_work_lab_work_name_id_index` (`lab_work_name_id`), ADD KEY `t_assign_lab_work_lab_work_status_id_index` (`lab_work_status_id`), ADD KEY `t_assign_lab_work_lab_form_id_index` (`lab_form_id`), ADD KEY `t_assign_lab_work_instrument_name_id_index` (`instrument_name_id`), ADD KEY `t_assign_lab_work_staff_id_index` (`staff_id`), ADD KEY `t_assign_lab_work_treatment_lab_work_id_index` (`treatment_lab_work_id`);

--
-- Indexes for table `t_assign_maintenance`
--
ALTER TABLE `t_assign_maintenance`
 ADD PRIMARY KEY (`id`), ADD KEY `t_assign_maintenance_vendor_types_id_index` (`vendor_types_id`), ADD KEY `t_assign_maintenance_gadget_id_index` (`gadget_id`), ADD KEY `t_assign_maintenance_machine_id_index` (`machine_id`), ADD KEY `t_assign_maintenance_vendor_id_index` (`vendor_id`);

--
-- Indexes for table `t_cash_transfer`
--
ALTER TABLE `t_cash_transfer`
 ADD PRIMARY KEY (`id`), ADD KEY `t_cash_transfer_transfer_detail_id_index` (`transfer_detail_id`);

--
-- Indexes for table `t_gadget_billing`
--
ALTER TABLE `t_gadget_billing`
 ADD PRIMARY KEY (`id`), ADD KEY `t_gadget_billing_purchase_order_id_index` (`purchase_order_id`), ADD KEY `t_gadget_billing_billing_status_id_index` (`billing_status_id`);

--
-- Indexes for table `t_gadget_installments`
--
ALTER TABLE `t_gadget_installments`
 ADD PRIMARY KEY (`id`), ADD KEY `t_gadget_installments_gadget_billing_id_index` (`gadget_billing_id`), ADD KEY `t_gadget_installments_payment_mode_id_index` (`payment_mode_id`);

--
-- Indexes for table `t_instrument_billing`
--
ALTER TABLE `t_instrument_billing`
 ADD PRIMARY KEY (`id`), ADD KEY `t_instrument_billing_purchase_order_id_index` (`purchase_order_id`), ADD KEY `t_instrument_billing_billing_status_id_index` (`billing_status_id`);

--
-- Indexes for table `t_instrument_installments`
--
ALTER TABLE `t_instrument_installments`
 ADD PRIMARY KEY (`id`), ADD KEY `t_instrument_installments_instrument_billing_id_index` (`instrument_billing_id`), ADD KEY `t_instrument_installments_payment_mode_id_index` (`payment_mode_id`);

--
-- Indexes for table `t_lab_billing`
--
ALTER TABLE `t_lab_billing`
 ADD PRIMARY KEY (`id`), ADD KEY `t_lab_billing_assign_lab_work_id_index` (`assign_lab_work_id`), ADD KEY `t_lab_billing_lab_delivery_id_index` (`lab_delivery_id`), ADD KEY `t_lab_billing_billing_status_id_index` (`billing_status_id`);

--
-- Indexes for table `t_lab_delivery`
--
ALTER TABLE `t_lab_delivery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_lab_form`
--
ALTER TABLE `t_lab_form`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_lab_installments`
--
ALTER TABLE `t_lab_installments`
 ADD PRIMARY KEY (`id`), ADD KEY `t_lab_installments_lab_billing_id_index` (`lab_billing_id`), ADD KEY `t_lab_installments_payment_mode_id_index` (`payment_mode_id`);

--
-- Indexes for table `t_machine_billing`
--
ALTER TABLE `t_machine_billing`
 ADD PRIMARY KEY (`id`), ADD KEY `t_machine_billing_purchase_order_id_index` (`purchase_order_id`), ADD KEY `t_machine_billing_billing_status_id_index` (`billing_status_id`);

--
-- Indexes for table `t_machine_installments`
--
ALTER TABLE `t_machine_installments`
 ADD PRIMARY KEY (`id`), ADD KEY `t_machine_installments_machine_billing_id_index` (`machine_billing_id`), ADD KEY `t_machine_installments_payment_mode_id_index` (`payment_mode_id`);

--
-- Indexes for table `t_maintenance_contract_billing`
--
ALTER TABLE `t_maintenance_contract_billing`
 ADD PRIMARY KEY (`id`), ADD KEY `t_maintenance_contract_billing_vendor_work_specs_id_index` (`vendor_work_specs_id`), ADD KEY `t_maintenance_contract_billing_payment_mode_id_index` (`payment_mode_id`);

--
-- Indexes for table `t_maintenance_service_billing`
--
ALTER TABLE `t_maintenance_service_billing`
 ADD PRIMARY KEY (`id`), ADD KEY `t_maintenance_service_billing_assign_maintenance_id_index` (`assign_maintenance_id`), ADD KEY `t_maintenance_service_billing_payment_mode_id_index` (`payment_mode_id`);

--
-- Indexes for table `t_material_billing`
--
ALTER TABLE `t_material_billing`
 ADD PRIMARY KEY (`id`), ADD KEY `t_material_billing_purchase_order_id_index` (`purchase_order_id`), ADD KEY `t_material_billing_billing_status_id_index` (`billing_status_id`);

--
-- Indexes for table `t_material_installments`
--
ALTER TABLE `t_material_installments`
 ADD PRIMARY KEY (`id`), ADD KEY `t_material_installments_material_billing_id_index` (`material_billing_id`), ADD KEY `t_material_installments_payment_mode_id_index` (`payment_mode_id`);

--
-- Indexes for table `t_patient_treatment`
--
ALTER TABLE `t_patient_treatment`
 ADD PRIMARY KEY (`id`), ADD KEY `t_patient_treatment_treatment_showstopper_checklist_id_index` (`treatment_showstopper_checklist_id`), ADD KEY `t_patient_treatment_patient_id_index` (`patient_id`), ADD KEY `t_patient_treatment_treatment_name_id_index` (`treatment_name_id`), ADD KEY `t_patient_treatment_treatment_status_id_index` (`treatment_status_id`), ADD KEY `t_patient_treatment_clinic_id_index` (`clinic_id`), ADD KEY `t_patient_treatment_staff_id_dentist_index` (`staff_id_dentist`), ADD KEY `t_patient_treatment_quotation_id_index` (`quotation_id`), ADD KEY `t_patient_treatment_referal_id_index` (`referal_id`), ADD KEY `t_patient_treatment_staff_id_consultant_index` (`staff_id_consultant`);

--
-- Indexes for table `t_po_billing`
--
ALTER TABLE `t_po_billing`
 ADD PRIMARY KEY (`id`), ADD KEY `t_po_billing_purchase_order_status_id_index` (`purchase_order_status_id`), ADD KEY `t_po_billing_purchase_order_id_index` (`purchase_order_id`);

--
-- Indexes for table `t_po_delivery`
--
ALTER TABLE `t_po_delivery`
 ADD PRIMARY KEY (`id`), ADD KEY `t_po_delivery_purchase_order_id_index` (`purchase_order_id`);

--
-- Indexes for table `t_po_inventory_payments`
--
ALTER TABLE `t_po_inventory_payments`
 ADD PRIMARY KEY (`id`), ADD KEY `t_po_inventory_payments_payment_mode_id_index` (`payment_mode_id`), ADD KEY `t_po_inventory_payments_billing_status_id_index` (`billing_status_id`), ADD KEY `t_po_inventory_payments_po_billing_id_index` (`po_billing_id`);

--
-- Indexes for table `t_purchase_order`
--
ALTER TABLE `t_purchase_order`
 ADD PRIMARY KEY (`id`), ADD KEY `t_purchase_order_purchase_order_status_id_index` (`purchase_order_status_id`);

--
-- Indexes for table `t_purchase_order_material`
--
ALTER TABLE `t_purchase_order_material`
 ADD PRIMARY KEY (`id`), ADD KEY `t_purchase_order_material_gadget_id_index` (`gadget_id`), ADD KEY `t_purchase_order_material_treatment_name_id_index` (`treatment_name_id`);

--
-- Indexes for table `t_purchase_order_vendor`
--
ALTER TABLE `t_purchase_order_vendor`
 ADD PRIMARY KEY (`id`), ADD KEY `t_purchase_order_vendor_vendor_name_id_index` (`vendor_name_id`);

--
-- Indexes for table `t_purchase_order_vendor_material`
--
ALTER TABLE `t_purchase_order_vendor_material`
 ADD PRIMARY KEY (`id`), ADD KEY `t_purchase_order_vendor_material_po_vendor_id_index` (`po_vendor_id`), ADD KEY `t_purchase_order_vendor_material_material_order_id_index` (`material_order_id`);

--
-- Indexes for table `t_quotation`
--
ALTER TABLE `t_quotation`
 ADD PRIMARY KEY (`id`), ADD KEY `t_quotation_treatment_type_id_index` (`treatment_type_id`), ADD KEY `t_quotation_treatment_name_id_index` (`treatment_name_id`);

--
-- Indexes for table `t_salary`
--
ALTER TABLE `t_salary`
 ADD PRIMARY KEY (`id`), ADD KEY `t_salary_staff_id_index` (`staff_id`), ADD KEY `t_salary_payment_mode_id_index` (`payment_mode_id`), ADD KEY `t_salary_leave_id_index` (`leave_id`);

--
-- Indexes for table `t_sittings`
--
ALTER TABLE `t_sittings`
 ADD PRIMARY KEY (`id`), ADD KEY `t_sittings_patient_treatment_id_index` (`patient_treatment_id`), ADD KEY `t_sittings_appointment_id_index` (`appointment_id`), ADD KEY `t_sittings_material_name_id_index` (`material_name_id`), ADD KEY `t_sittings_instrument_name_id_index` (`instrument_name_id`), ADD KEY `t_sittings_gadget_id_index` (`gadget_id`);

--
-- Indexes for table `t_sitting_lab_work`
--
ALTER TABLE `t_sitting_lab_work`
 ADD PRIMARY KEY (`id`), ADD KEY `t_sitting_lab_work_sittings_id_index` (`sittings_id`), ADD KEY `t_sitting_lab_work_patient_treatment_id_index` (`patient_treatment_id`), ADD KEY `t_sitting_lab_work_lab_work_status_id_index` (`lab_work_status_id`);

--
-- Indexes for table `t_transfer_detail`
--
ALTER TABLE `t_transfer_detail`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_treatment_billing`
--
ALTER TABLE `t_treatment_billing`
 ADD PRIMARY KEY (`id`), ADD KEY `t_treatment_billing_patient_treatment_id_index` (`patient_treatment_id`), ADD KEY `t_treatment_billing_quotation_id_index` (`quotation_id`), ADD KEY `t_treatment_billing_billing_status_id_index` (`billing_status_id`);

--
-- Indexes for table `t_treatment_installments`
--
ALTER TABLE `t_treatment_installments`
 ADD PRIMARY KEY (`id`), ADD KEY `t_treatment_installments_treatment_billing_id_index` (`treatment_billing_id`), ADD KEY `t_treatment_installments_payment_mode_id_index` (`payment_mode_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cheque`
--
ALTER TABLE `cheque`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `followup_appointment_details`
--
ALTER TABLE `followup_appointment_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `miscellaneous`
--
ALTER TABLE `miscellaneous`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_bank`
--
ALTER TABLE `m_bank`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_clinic`
--
ALTER TABLE `m_clinic`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_clinic_slot_details`
--
ALTER TABLE `m_clinic_slot_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_clinic_slot_timing`
--
ALTER TABLE `m_clinic_slot_timing`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_clinic_type`
--
ALTER TABLE `m_clinic_type`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_gadget_details`
--
ALTER TABLE `m_gadget_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_gadget_treatment`
--
ALTER TABLE `m_gadget_treatment`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_instrument_details`
--
ALTER TABLE `m_instrument_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_instrument_treatment`
--
ALTER TABLE `m_instrument_treatment`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_leave_full_day`
--
ALTER TABLE `m_leave_full_day`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_leave_partial_days`
--
ALTER TABLE `m_leave_partial_days`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_machine_details`
--
ALTER TABLE `m_machine_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_machine_treatment`
--
ALTER TABLE `m_machine_treatment`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_maintenance_name`
--
ALTER TABLE `m_maintenance_name`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_material_alerts`
--
ALTER TABLE `m_material_alerts`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_material_details`
--
ALTER TABLE `m_material_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_material_treatment`
--
ALTER TABLE `m_material_treatment`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_medical_history`
--
ALTER TABLE `m_medical_history`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_other_referals`
--
ALTER TABLE `m_other_referals`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_patient_attachments`
--
ALTER TABLE `m_patient_attachments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_patient_details`
--
ALTER TABLE `m_patient_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_personal`
--
ALTER TABLE `m_personal`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_referals`
--
ALTER TABLE `m_referals`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_staff-clinic`
--
ALTER TABLE `m_staff-clinic`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_staff_registration`
--
ALTER TABLE `m_staff_registration`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_staff_slot_details`
--
ALTER TABLE `m_staff_slot_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_staff_slot_timing`
--
ALTER TABLE `m_staff_slot_timing`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_treatment_attachments`
--
ALTER TABLE `m_treatment_attachments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_treatment_diagnosis`
--
ALTER TABLE `m_treatment_diagnosis`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_treatment_name`
--
ALTER TABLE `m_treatment_name`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_treatment_showstopper_checklist`
--
ALTER TABLE `m_treatment_showstopper_checklist`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_vendor_detail`
--
ALTER TABLE `m_vendor_detail`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_vendor_instrument_details`
--
ALTER TABLE `m_vendor_instrument_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_vendor_inst_workspec_details`
--
ALTER TABLE `m_vendor_inst_workspec_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_vendor_lab_details`
--
ALTER TABLE `m_vendor_lab_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `m_vendor_lab_workspec_details`
--
ALTER TABLE `m_vendor_lab_workspec_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_vendor_material_details`
--
ALTER TABLE `m_vendor_material_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_vendor_mat_workspec_details`
--
ALTER TABLE `m_vendor_mat_workspec_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `schedule_details`
--
ALTER TABLE `schedule_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_appointment_status`
--
ALTER TABLE `s_appointment_status`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_billing_status`
--
ALTER TABLE `s_billing_status`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_chair_details`
--
ALTER TABLE `s_chair_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_dentist_details`
--
ALTER TABLE `s_dentist_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_follow-up`
--
ALTER TABLE `s_follow-up`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_follow-up_status`
--
ALTER TABLE `s_follow-up_status`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_gadget_subtype`
--
ALTER TABLE `s_gadget_subtype`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `s_gadget_type`
--
ALTER TABLE `s_gadget_type`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `s_instrument_subtype`
--
ALTER TABLE `s_instrument_subtype`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `s_instrument_type`
--
ALTER TABLE `s_instrument_type`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `s_instrument_unit`
--
ALTER TABLE `s_instrument_unit`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `s_lab_instrument_status`
--
ALTER TABLE `s_lab_instrument_status`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_lab_work_name`
--
ALTER TABLE `s_lab_work_name`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `s_lab_work_status`
--
ALTER TABLE `s_lab_work_status`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_lab_work_subtype`
--
ALTER TABLE `s_lab_work_subtype`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `s_lab_work_type`
--
ALTER TABLE `s_lab_work_type`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `s_machine_subtype`
--
ALTER TABLE `s_machine_subtype`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `s_machine_type`
--
ALTER TABLE `s_machine_type`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `s_machine_unit`
--
ALTER TABLE `s_machine_unit`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_maintenance_status`
--
ALTER TABLE `s_maintenance_status`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_material_subtype`
--
ALTER TABLE `s_material_subtype`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `s_material_type`
--
ALTER TABLE `s_material_type`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `s_material_unit`
--
ALTER TABLE `s_material_unit`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `s_payment_mode`
--
ALTER TABLE `s_payment_mode`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_purchase_order_status`
--
ALTER TABLE `s_purchase_order_status`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_staff_type`
--
ALTER TABLE `s_staff_type`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_threshold_status`
--
ALTER TABLE `s_threshold_status`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `s_treatment_status`
--
ALTER TABLE `s_treatment_status`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `s_treatment_type`
--
ALTER TABLE `s_treatment_type`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `s_vendor_types`
--
ALTER TABLE `s_vendor_types`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_appointment`
--
ALTER TABLE `t_appointment`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_assign_lab_work`
--
ALTER TABLE `t_assign_lab_work`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_assign_maintenance`
--
ALTER TABLE `t_assign_maintenance`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_cash_transfer`
--
ALTER TABLE `t_cash_transfer`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_gadget_billing`
--
ALTER TABLE `t_gadget_billing`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_gadget_installments`
--
ALTER TABLE `t_gadget_installments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_instrument_billing`
--
ALTER TABLE `t_instrument_billing`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_instrument_installments`
--
ALTER TABLE `t_instrument_installments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_lab_billing`
--
ALTER TABLE `t_lab_billing`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_lab_delivery`
--
ALTER TABLE `t_lab_delivery`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_lab_form`
--
ALTER TABLE `t_lab_form`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_lab_installments`
--
ALTER TABLE `t_lab_installments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_machine_billing`
--
ALTER TABLE `t_machine_billing`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_machine_installments`
--
ALTER TABLE `t_machine_installments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_maintenance_contract_billing`
--
ALTER TABLE `t_maintenance_contract_billing`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_maintenance_service_billing`
--
ALTER TABLE `t_maintenance_service_billing`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_material_billing`
--
ALTER TABLE `t_material_billing`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_material_installments`
--
ALTER TABLE `t_material_installments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_patient_treatment`
--
ALTER TABLE `t_patient_treatment`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_po_billing`
--
ALTER TABLE `t_po_billing`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_po_delivery`
--
ALTER TABLE `t_po_delivery`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_po_inventory_payments`
--
ALTER TABLE `t_po_inventory_payments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_purchase_order`
--
ALTER TABLE `t_purchase_order`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_purchase_order_material`
--
ALTER TABLE `t_purchase_order_material`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_purchase_order_vendor`
--
ALTER TABLE `t_purchase_order_vendor`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_purchase_order_vendor_material`
--
ALTER TABLE `t_purchase_order_vendor_material`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_quotation`
--
ALTER TABLE `t_quotation`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_salary`
--
ALTER TABLE `t_salary`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_sittings`
--
ALTER TABLE `t_sittings`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_sitting_lab_work`
--
ALTER TABLE `t_sitting_lab_work`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_transfer_detail`
--
ALTER TABLE `t_transfer_detail`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_treatment_billing`
--
ALTER TABLE `t_treatment_billing`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_treatment_installments`
--
ALTER TABLE `t_treatment_installments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `m_gadget_details`
--
ALTER TABLE `m_gadget_details`
ADD CONSTRAINT `m_gadget_details_gadget_subtype_id_foreign` FOREIGN KEY (`gadget_subtype_id`) REFERENCES `s_gadget_subtype` (`id`),
ADD CONSTRAINT `m_gadget_details_gadget_type_id_foreign` FOREIGN KEY (`gadget_type_id`) REFERENCES `s_gadget_type` (`id`);

--
-- Constraints for table `m_gadget_treatment`
--
ALTER TABLE `m_gadget_treatment`
ADD CONSTRAINT `m_gadget_treatment_gadget_name_id_foreign` FOREIGN KEY (`gadget_name_id`) REFERENCES `m_gadget_details` (`id`),
ADD CONSTRAINT `m_gadget_treatment_treatment_name_id_foreign` FOREIGN KEY (`treatment_name_id`) REFERENCES `m_treatment_name` (`id`),
ADD CONSTRAINT `m_gadget_treatment_treatment_type_id_foreign` FOREIGN KEY (`treatment_type_id`) REFERENCES `s_treatment_type` (`id`);

--
-- Constraints for table `m_instrument_details`
--
ALTER TABLE `m_instrument_details`
ADD CONSTRAINT `m_instrument_details_instrument_subtype_id_foreign` FOREIGN KEY (`instrument_subtype_id`) REFERENCES `s_instrument_subtype` (`id`),
ADD CONSTRAINT `m_instrument_details_instrument_type_id_foreign` FOREIGN KEY (`instrument_type_id`) REFERENCES `s_instrument_type` (`id`),
ADD CONSTRAINT `m_instrument_details_instrument_unit_id_foreign` FOREIGN KEY (`instrument_unit_id`) REFERENCES `s_instrument_unit` (`id`);

--
-- Constraints for table `m_instrument_treatment`
--
ALTER TABLE `m_instrument_treatment`
ADD CONSTRAINT `m_instrument_treatment_instrument_name_id_foreign` FOREIGN KEY (`instrument_name_id`) REFERENCES `m_instrument_details` (`id`),
ADD CONSTRAINT `m_instrument_treatment_treatment_name_id_foreign` FOREIGN KEY (`treatment_name_id`) REFERENCES `m_treatment_name` (`id`),
ADD CONSTRAINT `m_instrument_treatment_treatment_type_id_foreign` FOREIGN KEY (`treatment_type_id`) REFERENCES `s_treatment_type` (`id`);

--
-- Constraints for table `m_machine_details`
--
ALTER TABLE `m_machine_details`
ADD CONSTRAINT `m_machine_details_machine_subtype_id_foreign` FOREIGN KEY (`machine_subtype_id`) REFERENCES `s_machine_subtype` (`id`),
ADD CONSTRAINT `m_machine_details_machine_type_id_foreign` FOREIGN KEY (`machine_type_id`) REFERENCES `s_machine_type` (`id`);

--
-- Constraints for table `m_machine_treatment`
--
ALTER TABLE `m_machine_treatment`
ADD CONSTRAINT `m_machine_treatment_machine_name_id_foreign` FOREIGN KEY (`machine_name_id`) REFERENCES `m_instrument_details` (`id`),
ADD CONSTRAINT `m_machine_treatment_treatment_name_id_foreign` FOREIGN KEY (`treatment_name_id`) REFERENCES `m_treatment_name` (`id`),
ADD CONSTRAINT `m_machine_treatment_treatment_type_id_foreign` FOREIGN KEY (`treatment_type_id`) REFERENCES `s_treatment_type` (`id`);

--
-- Constraints for table `m_material_details`
--
ALTER TABLE `m_material_details`
ADD CONSTRAINT `m_material_details_material_subtype_id_foreign` FOREIGN KEY (`material_subtype_id`) REFERENCES `s_material_subtype` (`id`),
ADD CONSTRAINT `m_material_details_material_type_id_foreign` FOREIGN KEY (`material_type_id`) REFERENCES `s_material_type` (`id`),
ADD CONSTRAINT `m_material_details_material_unit_id_foreign` FOREIGN KEY (`material_unit_id`) REFERENCES `s_material_unit` (`id`);

--
-- Constraints for table `m_material_treatment`
--
ALTER TABLE `m_material_treatment`
ADD CONSTRAINT `m_material_treatment_material_name_id_foreign` FOREIGN KEY (`material_name_id`) REFERENCES `m_material_details` (`id`),
ADD CONSTRAINT `m_material_treatment_treatment_name_id_foreign` FOREIGN KEY (`treatment_name_id`) REFERENCES `m_treatment_name` (`id`),
ADD CONSTRAINT `m_material_treatment_treatment_type_id_foreign` FOREIGN KEY (`treatment_type_id`) REFERENCES `s_treatment_type` (`id`);

--
-- Constraints for table `m_medical_history`
--
ALTER TABLE `m_medical_history`
ADD CONSTRAINT `m_medical_history_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `m_patient_details` (`id`);

--
-- Constraints for table `m_treatment_name`
--
ALTER TABLE `m_treatment_name`
ADD CONSTRAINT `m_treatment_name_treatment_type_id_foreign` FOREIGN KEY (`treatment_type_id`) REFERENCES `s_treatment_type` (`id`);

--
-- Constraints for table `m_vendor_instrument_details`
--
ALTER TABLE `m_vendor_instrument_details`
ADD CONSTRAINT `m_vendor_instrument_details_vendor_type_id_foreign` FOREIGN KEY (`vendor_type_id`) REFERENCES `s_vendor_types` (`id`);

--
-- Constraints for table `m_vendor_inst_workspec_details`
--
ALTER TABLE `m_vendor_inst_workspec_details`
ADD CONSTRAINT `m_vendor_inst_workspec_details_vendor_type_id_foreign` FOREIGN KEY (`vendor_type_id`) REFERENCES `s_vendor_types` (`id`);

--
-- Constraints for table `m_vendor_lab_details`
--
ALTER TABLE `m_vendor_lab_details`
ADD CONSTRAINT `m_vendor_lab_details_vendor_type_id_foreign` FOREIGN KEY (`vendor_type_id`) REFERENCES `s_vendor_types` (`id`);

--
-- Constraints for table `m_vendor_lab_workspec_details`
--
ALTER TABLE `m_vendor_lab_workspec_details`
ADD CONSTRAINT `m_vendor_lab_workspec_details_vendor_lab_id_foreign` FOREIGN KEY (`vendor_lab_id`) REFERENCES `m_vendor_lab_details` (`id`),
ADD CONSTRAINT `m_vendor_lab_workspec_details_vendor_type_id_foreign` FOREIGN KEY (`vendor_type_id`) REFERENCES `s_vendor_types` (`id`),
ADD CONSTRAINT `m_vendor_lab_workspec_details_work_name_id_foreign` FOREIGN KEY (`work_name_id`) REFERENCES `s_lab_work_name` (`id`),
ADD CONSTRAINT `m_vendor_lab_workspec_details_work_subtype_id_foreign` FOREIGN KEY (`work_subtype_id`) REFERENCES `s_lab_work_subtype` (`id`),
ADD CONSTRAINT `m_vendor_lab_workspec_details_work_type_id_foreign` FOREIGN KEY (`work_type_id`) REFERENCES `s_lab_work_type` (`id`);

--
-- Constraints for table `m_vendor_material_details`
--
ALTER TABLE `m_vendor_material_details`
ADD CONSTRAINT `m_vendor_material_details_vendor_type_id_foreign` FOREIGN KEY (`vendor_type_id`) REFERENCES `s_vendor_types` (`id`);

--
-- Constraints for table `m_vendor_mat_workspec_details`
--
ALTER TABLE `m_vendor_mat_workspec_details`
ADD CONSTRAINT `m_vendor_mat_workspec_details_mat_name_id_foreign` FOREIGN KEY (`mat_name_id`) REFERENCES `m_material_details` (`id`),
ADD CONSTRAINT `m_vendor_mat_workspec_details_mat_subtype_id_foreign` FOREIGN KEY (`mat_subtype_id`) REFERENCES `s_material_subtype` (`id`),
ADD CONSTRAINT `m_vendor_mat_workspec_details_mat_type_id_foreign` FOREIGN KEY (`mat_type_id`) REFERENCES `s_material_type` (`id`),
ADD CONSTRAINT `m_vendor_mat_workspec_details_vendor_mat_id_foreign` FOREIGN KEY (`vendor_mat_id`) REFERENCES `m_vendor_material_details` (`id`),
ADD CONSTRAINT `m_vendor_mat_workspec_details_vendor_type_id_foreign` FOREIGN KEY (`vendor_type_id`) REFERENCES `s_vendor_types` (`id`);

--
-- Constraints for table `s_gadget_subtype`
--
ALTER TABLE `s_gadget_subtype`
ADD CONSTRAINT `s_gadget_subtype_gadget_type_id_foreign` FOREIGN KEY (`gadget_type_id`) REFERENCES `s_gadget_type` (`id`);

--
-- Constraints for table `s_instrument_subtype`
--
ALTER TABLE `s_instrument_subtype`
ADD CONSTRAINT `s_instrument_subtype_instrument_type_id_foreign` FOREIGN KEY (`instrument_type_id`) REFERENCES `s_instrument_type` (`id`);

--
-- Constraints for table `s_machine_subtype`
--
ALTER TABLE `s_machine_subtype`
ADD CONSTRAINT `s_machine_subtype_machine_type_id_foreign` FOREIGN KEY (`machine_type_id`) REFERENCES `s_machine_type` (`id`);

--
-- Constraints for table `s_material_subtype`
--
ALTER TABLE `s_material_subtype`
ADD CONSTRAINT `s_material_subtype_material_type_id_foreign` FOREIGN KEY (`material_type_id`) REFERENCES `s_material_type` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
