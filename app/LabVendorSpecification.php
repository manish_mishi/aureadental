<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LabVendorSpecification extends Model 
{

	protected $table='m_vendor_lab_workspec_details';
	protected $fillable=['name','work_type','work_subtype','rate','duration'];

}