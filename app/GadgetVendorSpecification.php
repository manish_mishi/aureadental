<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GadgetVendorSpecification extends Model 
{

	protected $table='m_vendor_gadget_workspec_details';
	protected $fillable=['name','rate'];

}