<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Instrumentunit extends Model 
{

	protected $table='s_instrument_unit';
	protected $fillable=['name'];


}
