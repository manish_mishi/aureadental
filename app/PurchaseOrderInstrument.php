<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderInstrument extends Model {

	protected $table='t_purchase_order_instrument';

}
