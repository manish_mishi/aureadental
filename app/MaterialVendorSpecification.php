<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialVendorSpecification extends Model 
{

	protected $table='m_vendor_mat_workspec_details';

	protected $fillable=['name','material_type','material_subtype','rate','duration'];

}
