<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GadgetSubtype extends Model 
{

	protected $table='s_gadget_subtype';
	protected $fillable=['name'];

}
