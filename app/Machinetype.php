<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Machinetype extends Model
 {

	protected $table='s_machine_type';

	protected $fillable=['name'];

}
