<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineVendorSpecification extends Model 
{

	protected $table='m_vendor_machine_workspec_details';
	protected $fillable=['name','rate'];

}