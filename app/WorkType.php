<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Worktype extends Model 
{

	protected $table='s_lab_work_type';

	protected $fillable=['name'];

}
