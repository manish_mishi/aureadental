<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Machinesubtype extends Model 
{

protected $table='s_machine_subtype';
protected $fillable=['name'];

}
