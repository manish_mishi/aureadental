<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialTreatment extends Model {

	protected $table='m_material_treatment';
	protected $fillable=['treatment_type','treatment_name'];

}
