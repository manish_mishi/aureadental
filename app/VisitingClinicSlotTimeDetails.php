<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitingClinicSlotTimeDetails extends Model {

	protected $table='m_visiting_clinic_staff_slot_timing';

}