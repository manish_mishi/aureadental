<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model {

	protected $table = 'm_patient_details';

	protected $fillable = ['name','gender','email','occupation','address','dob'];

}
