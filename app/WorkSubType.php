<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkSubtype extends Model 
{

	protected $table='s_lab_work_subtype';

	protected $fillable=['name'];

}
