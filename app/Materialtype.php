<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Materialtype extends Model 
{

	protected $table='s_material_type';

	protected $fillable=['name'];

}
