<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class InstrumentVendorSpecification extends Model 
{

	protected $table='m_vendor_inst_workspec_details';

	protected $fillable=['name','instrument_type','instrument_subtype','rate'];

}