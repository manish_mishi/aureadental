<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Instrumenttype extends Model {

	protected $table='s_instrument_type';

	protected $fillable=['name'];
}
