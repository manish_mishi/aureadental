<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GadgetType extends Model
 {

	protected $table = 's_gadget_type'; 
	protected $fillable=['name'];

}
