<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonDetails extends Model {

	protected $table='m_personal';

	protected $fillable=['person_name','cell','staff_id'];
}
