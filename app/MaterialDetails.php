<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialDetails extends Model {

	protected $table='m_material_details';
	protected $fillable=['material_name','	material_type','material_subtype','material_unit'];

}
