<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintenanceVendorSpecification extends Model 
{

	protected $table='m_vendor_maint_workspec_details';
	protected $fillable=['contract_start_date','contract_end_date','contract_amount','expected_available_time','duration','rate'];

}