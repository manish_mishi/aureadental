<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', 'WelcomeController@index');*/

/*Route::get('/', 'HomeController@index');*/

Route::get('/', function(){
	return view('auth.login');
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('home', 'HomeController@index');

	
Route::get('settings','SettingsController@index');

/////////////////*******************Settings - Patient *********************/////////////////

Route::get('settings/patient/dentist/adddentistname','SettingsController@createDentistDetails');
Route::post('settings/patient/dentist/adddentistname', array('as'=>'adddentistname','uses'=>'SettingsController@storeDentistDetails'));
Route::post('deletedentistname', 'SettingsController@destroyDentistDetails');

Route::get('settings/patient/chair/addchairdetails','SettingsController@createChairDetails');
Route::post('settings/patient/chair/addchairdetails',array('as'=>'addchairdetails','uses'=>'SettingsController@storeChairDetails'));
Route::post('deletechairdetails','SettingsController@destroyChairDetails');

/////////////////*******************Settings - Patient *********************/////////////////

Route::get('settings/planner/followup/status','SettingsController@viewFollowUpStatus');
Route::get('settings/planner/appointment/status','SettingsController@viewAppointmentStatus');

/////////////////*******************Settings - Treatment *********************/////////////////

Route::get('settings/treatment/type','SettingsController@createTreatmentType');
Route::post('settings/treatment/type', array('as'=>'addTreatmentType','uses'=>'SettingsController@storeTreatmentType'));
Route::post('deleteTreatmentType', 'SettingsController@destroyTreatmentType');

Route::get('settings/treatment/status','SettingsController@viewTreatmentStatus');
Route::post('settings/treatment/status', array('as'=>'addTreatmentStatus','uses'=>'SettingsController@storeTreatmentStatus'));
Route::post('deleteTreatmentStatus', 'SettingsController@destroyTreatmentStatus');

/////////////////*******************Settings - Inventory *********************/////////////////

/*Route::get('settings/inventory/materialmanagementdashboard','SettingsController@Dashboardmaterialmanagement');*/
Route::get('settings/inventory/materialmanagement/addmaterialtype','SettingsController@createMaterialType');
Route::post('settings/inventory/materialmanagement/addmaterialtype', array('as'=>'addmaterialtype','uses'=>'SettingsController@storeMaterialType'));
Route::post('deletematerialtype', 'SettingsController@destroyMaterialType');
Route::get('settings/inventory/materialmanagement/addmaterialsubtype','SettingsController@createMaterialSubtype');
Route::post('settings/inventory/materialmanagement/addmaterialsubtype', array('as'=>'addmaterialsubtype','uses'=>'SettingsController@storeMaterialSubtype'));
Route::post('deletematerialsubtype', 'SettingsController@destroyMaterialSubtype');
Route::get('settings/inventory/materialmanagement/addmaterailunit','SettingsController@createMaterialUnit');
Route::post('settings/inventory/materialmanagement/addmaterailunit', array('as'=>'addunit','uses'=>'SettingsController@storeMateriaUnit'));
Route::post('deletematerialunit', 'SettingsController@destroyMaterialUnit');

/*Route::get('settings/inventory/instrumentmanagementdashboard','SettingsController@Dashboardinstrumentmanagement');*/
Route::get('settings/inventory/instrumentmanagement/addinstrumenttype','SettingsController@createInstrumentType');
Route::post('settings/inventory/instrumentmanagement/addinstrumenttype',array('as'=>'addinstrumenttype','uses'=>'SettingsController@storeInstrumentType'));
Route::post('deleteinstrumenttype', 'SettingsController@destroyInstrumentType');
Route::get('settings/inventory/instrumentmanagement/addinstrumentsubtype','SettingsController@createInstrumentSubtype');
Route::post('settings/inventory/instrumentmanagement/addinstrumentsubtype',array('as'=>'addinstrumentsubtype','uses'=>'SettingsController@storeInstrumentSubtype'));
Route::post('deleteinstrumentsubtype', 'SettingsController@destroyInstrumentSubtype');
Route::get('settings/inventory/instrumentmanagement/addinstrumentunit','SettingsController@createInstrumentUnit');
Route::post('settings/inventory/instrumentmanagement/addinstrumentunit',array('as'=>'addinstrumentunit','uses'=>'SettingsController@storeInstrumentUnit'));
Route::post('deleteinstrumentunit', 'SettingsController@destroyInstrumentUnit');

/*Route::get('settings/inventory/machinemanagementdashboard','SettingsController@Dashboardmachinemanagement');*/
Route::get('settings/inventory/machinemanagement/addmachinetype','SettingsController@createMachineType');
Route::post('settings/inventory/machinemanagement/addmachinetype',array('as'=>'addmachinetype','uses'=>'SettingsController@storeMachineType'));
Route::post('deleteimachinetype', 'SettingsController@destroyMachineType');
Route::get('settings/inventory/machinemanagement/addmachinesubtype','SettingsController@createMachineSubtype');
Route::post('settings/inventory/machinemanagement/addmachinesubtype',array('as'=>'addmachinesubtype','uses'=>'SettingsController@storeMachineSubtype'));
Route::post('deletemachinesubtype', 'SettingsController@destroyMachineSubtype');
Route::get('settings/inventory/machinemanagement/addmachineunit','SettingsController@createMachineUnit');
Route::post('settings/inventory/machinemanagement/addmachineunit',array('as'=>'addmachineunit','uses'=>'SettingsController@storeMachineUnit'));
Route::post('deletemachineunit', 'SettingsController@destroyMachineUnit');

/*Route::get('settings/inventory/gadgetmanagementdashboard','SettingsController@Dashboardgadgetmanagement');*/
Route::get('settings/inventory/gadgetmanagement/addgadgettype','SettingsController@createGadgetType');
Route::post('settings/inventory/gadgetmanagement/addgadgettype',array('as'=>'addgadgettype','uses'=>'SettingsController@storeGadgetType'));
Route::post('deletegadgettype', 'SettingsController@destroyGadgetType');
Route::get('settings/inventory/gadgetmanagement/addgadgetsubtype','SettingsController@createGadgetSubtype');
Route::post('settings/inventory/gadgetmanagement/addgadgetsubtype',array('as'=>'addgadgetsubtype','uses'=>'SettingsController@storeGadgetSubtype'));
Route::post('deletegadgetsubtype', 'SettingsController@destroyGadgetSubtype');

Route::get('settings/clinic/staff','SettingsController@createStaff');

/////////////////*******************Settings - Lab *********************/////////////////

Route::get('settings/lab/addworktype','SettingsController@createLabWorkType');
Route::post('settings/lab/addworktype', array('as'=>'addWorktype','uses'=>'SettingsController@storeLabWorkType'));
Route::post('deleteSettingsLabWorktype', 'SettingsController@destroyLabWorkType');
Route::get('settings/lab/addworksubtype','SettingsController@createLabWorkSubtype');
Route::post('settings/lab/addworksubtype', array('as'=>'addWorkSubtype','uses'=>'SettingsController@storeLabWorkSubtype'));
Route::post('deleteSettingsLabWorkSubtype', 'SettingsController@destroyLabWorkSubType');
Route::get('settings/lab/addworkname','SettingsController@createLabWorkName');
Route::post('settings/lab/addworkname', array('as'=>'addWorkName','uses'=>'SettingsController@storeLabWorkName'));
Route::post('deleteSettingsLabWorkName', 'SettingsController@destroyLabWorkName');
Route::get('settings/lab/work/status','SettingsController@viewLabWorkStatus');

/////////////////*******************Settings - Clinic *********************/////////////////

Route::get('settings/clinic/staff/add_staff_type','SettingsController@createStaffType');
Route::post('settings/clinic/staff/add_staff_type', array('as'=>'addstafftype','uses'=>'SettingsController@storeStaffType'));
Route::post('deletestafftype', 'SettingsController@destroyStaffType');

/////////////////*******************Settings - Billing *********************/////////////////
Route::get('settings/billing/paymentmode/view','SettingsController@viewPaymentMode');

Route::get('settings/billing/billingstatus/view','SettingsController@viewBillingStatus');

////////////////////////////************- MASTER - Patient *********************////////////////

Route::get('master', 'MasterController@index');

Route::get('master/patient','MasterController@listViewPatient');

Route::get('master/patient/general', 'MasterController@createPatientGeneral');
Route::post('master/patient/general',  array('as' => 'addpatientgeneral', 'uses' => 'MasterController@storePatientGeneral'));
Route::get('master/patient/medicalhistory', 'MasterController@createPatientMedicalHistory');
Route::post('master/patient/medicalhistory',  array('as' => 'file', 'uses' => 'MasterController@storePatientMedicalHistory'));

Route::get('editPatient/{patient_id}','MasterController@editPatientGeneral');
Route::post('master/patient/general/update',array('as'=>'editPatientGeneral','uses'=>'MasterController@updatePatientGeneral'));
Route::get('editPatientMedicalHistory/{patient_id}','MasterController@editPatientMedicalHistory');
Route::post('master/patient/medicalhistory/update',  'MasterController@updatePatientMedicalHistory');
Route::get('master/patient/attachments', 'MasterController@patientAttachments');

/////////////////*******************MASTER - Treatment *********************/////////////////

Route::get('master/treatment/name','MasterController@createTreatmentName');
Route::post('master/treatment/name', array('as'=>'addTreatmentName','uses'=>'MasterController@storeTreatmentName'));
Route::post('deleteTreatmentName', 'MasterController@destroyTreatmentName');

/////////////////*******************MASTER - Inventory *********************/////////////////

Route::get('master/inventory/materialmanagement/listview', 'MasterController@listViewMaterialManagement');
Route::get('master/inventory/materialmanagement/add', 'MasterController@createMaterialManagementDetails');
Route::post('master/inventory/materialmanagement/add',array('as'=>'addmaterial','uses'=>'MasterController@storeMaterialManagementDetails'));
Route::get('master/inventory/materialmanagement/alert', 'MasterController@createMaterialManagementlAlert');
Route::post('master/inventory/materialmanagement/alert',array('as'=>'addmaterialalert','uses'=>'MasterController@storeMaterialManagementAlert'));
Route::get('master/inventory/materialmanagement/treatment', 'MasterController@createMaterialManagementTreatment');
Route::post('master/inventory/materialmanagement/treatment',array('as'=>'addmaterialmanagementtreatment','uses'=>'MasterController@storeMaterialManagementTreatment'));
Route::get('master/inventory/materialmanagement/edit/{material_id}','MasterController@editMaterialManagementDetails');
Route::post('master/inventory/materialmanagement/update',array('as'=>'updateMaterialDetails','uses'=>'MasterController@updateMaterialManagementDetails'));
Route::get('master/inventory/materialmanagement/alert/edit/{material_id}','MasterController@editMaterialManagementAlert');
Route::post('master/inventory/materialmanagement/alert/update',array('as'=>'updateMaterialAlerts','uses'=>'MasterController@updateMaterialManagementAlert'));
Route::get('master/inventory/materialmanagement/treatment/edit/{material_id}','MasterController@editMaterialManagementTreatment');
Route::post('master/inventory/materialmanagement/treatment/update',array('as'=>'updateMaterialTreatment','uses'=>'MasterController@updateMaterialManagementTreatment'));

Route::get('master/inventory/instrumentmanagement/listview', 'MasterController@listViewInstrumentManagement');
Route::get('master/inventory/instrumentmanagement/add', 'MasterController@createInstrumentManagementDetails');
Route::post('master/inventory/instrumentmanagement/add',array('as'=>'addInstrument','uses'=>'MasterController@storeInstrumentManagementDetails'));
Route::get('master/inventory/instrumentmanagement/alert', 'MasterController@createInstrumentManagementAlert');
Route::post('master/inventory/instrumentmanagement/alert',array('as'=>'addinstrumentalert','uses'=>'MasterController@storeInstrumentManagementAlert'));
Route::get('master/inventory/instrumentmanagement/treatment', 'MasterController@createInstrumentManagementTreatment');
Route::post('master/inventory/instrumentmanagement/treatment',array('as'=>'addinstrumentmanagementtreatment','uses'=>'MasterController@storeInstrumentManagementTreatment'));
Route::get('master/inventory/instrumentmanagement/edit/{instrument_id}','MasterController@editInstrumentManagementDetails');
Route::post('master/inventory/instrumentmanagement/update',array('as'=>'updateInstrumentDetails','uses'=>'MasterController@updateInstrumentManagementDetails'));
Route::get('master/inventory/instrumentmanagement/alert/edit/{instrument_id}','MasterController@editInstrumentManagementAlert');
Route::get('master/inventory/instrumentmanagement/treatment/edit/{instrument_id}','MasterController@editInstrumentManagementTreatment');
Route::post('master/inventory/instrumentmanagement/treatment/edit',array('as'=>'updateInstrumentAlerts','uses'=>'MasterController@updateInstrumentManagementAlert'));
Route::post('master/inventory/instrumentmanagement/treatment/update',array('as'=>'updateInstrumentTreatment','uses'=>'MasterController@updateInstrumentManagementTreatment'));

Route::get('master/inventory/machinemanagement/listview', 'MasterController@listViewMachineManagement');
Route::get('master/inventory/machinemanagement/add', 'MasterController@createMachineManagementDetails');
Route::post('master/inventory/machinemanagement/add',array('as'=>'addMachine','uses'=>'MasterController@storeMachineManagementDetails'));
Route::get('master/inventory/machinemanagement/treatment', 'MasterController@createMachineManagementTreatment');
Route::post('master/inventory/machinemanagement/treatment',array('as'=>'addmachinemanagementtreatment','uses'=>'MasterController@storeMachineManagementTreatment'));
Route::get('master/inventory/machinemanagement/edit/{machine_id}',array('as'=>'editMachineDetails','uses'=>'MasterController@editMachineManagementDetails'));
Route::post('master/inventory/machinemanagement/update',array('as'=>'updateMachineDetails','uses'=>'MasterController@updateMachineManagementDetails'));
Route::get('master/inventory/machinemanagement/treatment/edit/{machine_id}','MasterController@editMachineManagementTreatment');
Route::post('master/inventory/machinemanagement/treatment/update',array('as'=>'updateMachineTreatment','uses'=>'MasterController@updateMachineManagementTreatment'));

Route::get('master/inventory/gadgetmanagement/listview', 'MasterController@listViewGadgetManagement');
Route::get('master/inventory/gadgetmanagement/add', 'MasterController@createGadgetManagementDetails');
Route::post('master/inventory/gadgetmanagement/add',array('as'=>'addGadget','uses'=>'MasterController@storeGadgetManagementDetails'));
Route::get('master/inventory/gadgetmanagement/treatment', 'MasterController@createGadgetManagementTreatment');
Route::post('master/inventory/gadgetmanagement/treatment',array('as'=>'addgadgetmanagementtreatment','uses'=>'MasterController@storeGadgetManagementTreatment'));
Route::get('master/inventory/gadgetmanagement/edit/{gadget_id}',array('as'=>'editGadgetDetails','uses'=>'MasterController@editGadgetManagementDetails'));
Route::post('master/inventory/gadgetmanagement/update',array('as'=>'updateGadgetDetails','uses'=>'MasterController@updateGadgetManagementDetails'));
Route::get('master/inventory/gadgetmanagement/treatment/edit/{gadget_id}','MasterController@editGadgetManagementTreatment');

Route::post('master/inventory/gadgetmanagement/treatment/update',array('as'=>'updateGadgetTreatment','uses'=>'MasterController@updateGadgetManagementTreatment'));



////////************************MASTER Vendor section*********************//////////////


Route::get('master/vendors/lab/listview','MasterController@listViewVendorLab');
Route::get('master/vendors/lab/add',  'MasterController@createVendorLab');
Route::post('master/vendors/lab/add', array('as'=>'addlabvendors','uses'=>'MasterController@storeVendorLab'));
Route::get('master/vendors/lab/work_specification','MasterController@createVendorLabWorkSpecification');
Route::post('master/vendors/lab/work_specification/add', array('as'=>'addVendorLabWorkSpecification','uses'=>'MasterController@storeVendorLabWorkSpecification'));
Route::get('master/vendors/lab/edit/{vendor_lab_id}','MasterController@editVendorLab');
Route::post('master/vendors/lab/update',array('as'=>'updateVendorLab','uses'=>'MasterController@updateVendorLab'));
Route::get('master/vendors/lab/work_specification/edit/{vendor_lab_id}','MasterController@editVendorLabWorkSpecification');
Route::get('master/vendors/lab/work_specification/delete/{lab_workspec_id}/{vendor_lab_id}','MasterController@deleteVendorLabWork');
Route::post('master/vendors/lab/work_specification/update',array('as'=>'updateVendorLabWorkSpecification','uses'=>'MasterController@updateVendorLabWorkSpecification'));
Route::get('master/vendors/lab/view/{vendor_lab_id}','MasterController@viewVendorLab');
Route::get('master/vendors/lab/work_specification/view/{vendor_lab_id}','MasterController@viewVendorLabWorkSpecification');

Route::get('master/vendors/material/listview','MasterController@listViewVendorMaterial');
Route::get('master/vendors/material/add','MasterController@createVendorMaterial');
Route::post('master/vendors/material/add', array('as'=>'addmaterialvendors','uses'=>'MasterController@storeVendorMaterial'));
Route::get('master/vendors/material/work_specification','MasterController@createVendorMaterialWorkSpecification');
Route::post('master/vendors/material/work_specification', array('as'=>'addVendorMaterialWorkSpecification','uses'=>'MasterController@storeVendorMaterialWorkSpecification'));
Route::get('master/vendors/material/edit/{vendor_material_id}','MasterController@editVendorMaterial');
Route::post('master/vendors/material/update',array('as'=>'updateVendorMaterial','uses'=>'MasterController@updateVendorMaterial'));
Route::get('master/vendors/material/work_specification/edit/{vendor_material_id}','MasterController@editVendorMaterialWorkSpecification');
Route::get('master/vendors/material/work_specification/delete/{mat_workspec_id}/{vendor_material_id}','MasterController@deleteVendorMaterial');
Route::post('master/vendors/material/work_specification/update',array('as'=>'updateVendorMaterialWorkSpecification','uses'=>'MasterController@updateVendorMaterialWorkSpecification'));
Route::get('master/vendors/material/view/{vendor_material_id}','MasterController@viewVendorMaterial');
Route::get('master/vendors/material/work_specification/view/{vendor_material_id}','MasterController@viewVendorMaterialWorkSpecification');

Route::get('master/vendors/instrument/listview','MasterController@listViewVendorInstrument');
Route::get('master/vendors/instrument/add','MasterController@createVendorInstrument');
Route::post('master/vendors/instrument/add', array('as'=>'addinstrumentvendors','uses'=>'MasterController@storeVendorInstrument'));
Route::get('master/vendors/instrument/work_specification','MasterController@createVendorInstrumentWorkSpecification');
Route::post('master/vendors/instrument/work_specification/add', array('as'=>'addVendorInstrumentWorkSpecification','uses'=>'MasterController@storeVendorInstrumentWorkSpecification'));
Route::get('master/vendors/instrument/edit/{vendor_instrument_id}','MasterController@editVendorInstrument');
Route::post('master/vendors/instrument/update',array('as'=>'updateVendorInstrument','uses'=>'MasterController@updateVendorInstrument'));
Route::get('master/vendors/instrument/work_specification/edit/{vendor_instrument_id}','MasterController@editVendorInstrumentWorkSpecification');
Route::get('master/vendors/instrument/work_specification/delete/{inst_workspec_id}/{vendor_instrument_id}','MasterController@deleteVendorInstrument');
Route::post('master/vendors/instrument/work_specification/update',array('as'=>'updateVendorInstrumentWorkSpecification','uses'=>'MasterController@updateVendorInstrumentWorkSpecification'));
Route::get('master/vendors/instrument/view/{vendor_instrument_id}', 'MasterController@viewVendorInstrument');
Route::get('master/vendors/instrument/work_specification/view/{vendor_instrument_id}','MasterController@viewVendorInstrumentWorkSpecification');

Route::get('master/vendors/gadget/listview','MasterController@listViewVendorGadget');
Route::get('master/vendors/gadget/add','MasterController@createVendorGadget');
Route::post('master/vendors/gadget/add', array('as'=>'addgadgetvendors','uses'=>'MasterController@storeVendorGadget'));
Route::get('master/vendors/gadget/work_specification','MasterController@createVendorGadgetWorkSpecification');
Route::post('master/vendors/gadget/work_specification/add', array('as'=>'addVendorGadgetWorkSpecification','uses'=>'MasterController@storeVendorGadgetWorkSpecification'));
Route::get('master/vendors/gadget/edit/{vendor_gadget_id}','MasterController@editVendorGadget');
Route::post('master/vendors/gadget/update',array('as'=>'updateVendorGadget','uses'=>'MasterController@updateVendorGadget'));
Route::get('master/vendors/gadget/work_specification/edit/{vendor_gadget_id}','MasterController@editVendorGadgetWorkSpecification');
Route::get('master/vendors/gadget/work_specification/delete/{gdgt_workspec_id}/{vendor_gadget_id}','MasterController@deleteVendorGadgetName');
Route::post('master/vendors/gadget/work_specification/update',array('as'=>'updateVendorGadgetWorkSpecification','uses'=>'MasterController@updateVendorGadgetWorkSpecification'));
Route::get('master/vendors/gadget/view/{vendor_gadget_id}', 'MasterController@viewVendorGadget');
Route::get('master/vendors/gadget/work_specification/view/{vendor_gadget_id}','MasterController@viewVendorGadgetWorkSpecification');

Route::get('master/vendors/machine/listview','MasterController@listViewVendorMachine');
Route::get('master/vendors/machine/add','MasterController@createVendorMachine');
Route::post('master/vendors/machine/add', array('as'=>'addmachinevendors','uses'=>'MasterController@storeVendorMachine'));
Route::get('master/vendors/machine/work_specification','MasterController@createVendorMachineWorkSpecification');
Route::post('master/vendors/machine/work_specification/add', array('as'=>'addVendorMachineWorkSpecification','uses'=>'MasterController@storeVendorMachineWorkSpecification'));
Route::get('master/vendors/machine/edit/{vendor_machine_id}','MasterController@editVendorMachine');
Route::post('master/vendors/machine/work_specification/edit',array('as'=>'updateVendorMachine','uses'=>'MasterController@updateVendorMachine'));
Route::get('master/vendors/machine/work_specification/edit/{vendor_machine_id}','MasterController@editVendorMachineWorkSpecification');
Route::get('master/vendors/machine/work_specification/delete/{mach_workspec_id}/{vendor_machine_id}','MasterController@deleteVendorMachineName');
Route::post('master/vendors/machine/work_specification/update',array('as'=>'updateVendorMachineWorkSpecification','uses'=>'MasterController@updateVendorMachineWorkSpecification'));
Route::get('master/vendors/machine/view/{vendor_machine_id}','MasterController@viewVendorMachine');
Route::get('master/vendors/machine/work_specification/view/{vendor_machine_id}','MasterController@viewVendorMachineWorkSpecification');

Route::get('master/vendors/maintenance/listview','MasterController@listViewVendorMaintenance');
Route::get('master/vendors/maintenance/add','MasterController@createVendorMaintenace');
Route::post('master/vendors/maintenance/add', array('as'=>'addmaintenancevendors','uses'=>'MasterController@storeVendorMaintenance'));
Route::get('master/vendors/maintenance/work_specification','MasterController@createVendorMaintenaceWorkSpecification');
Route::post('master/vendors/maintenance/work_specification/add', array('as'=>'addVendorMaintenanceWorkSpecification','uses'=>'MasterController@storeVendorMaintenanceWorkSpecification'));
Route::get('master/vendors/maintenance/edit/{vendor_maintenance_id}','MasterController@editVendorMaintenance');
Route::post('master/vendors/maintenance/work_specification/edit',array('as'=>'updateVendorMaintenance','uses'=>'MasterController@updateVendorMaintenance'));
Route::get('master/vendors/maintenance/work_specification/delete/{maint_workspec_id}/{vendor_maintenance_id}','MasterController@deleteVendorMaintenanceWork');
Route::get('master/vendors/maintenance/work_specification/edit/{vendor_maintenance_id}','MasterController@editVendorMaintenanceWorkSpecification');
Route::post('master/vendors/maintenance/work_specification/update',array('as'=>'updateVendorMaintenanceWorkSpecification','uses'=>'MasterController@updateVendorMaintenanceWorkSpecification'));
Route::get('master/vendors/maintenance/view/{vendor_maintenance_id}','MasterController@viewVendorMaintenance');
Route::get('master/vendors/maintenance/work_specification/view/{vendor_maintenance_id}','MasterController@viewVendorMaintenanceWorkSpecification');

////////************************MASTER Clinic section*********************//////////////

Route::get('master/clinic/staff_register/listview', 'MasterController@listViewStaffRegistration');
Route::get('master/clinic/staff_register/add', 'MasterController@createStaffRegistration');
Route::post('master/clinic/staff_register/add', array('as'=>'addgeneralstaffregistration','uses'=>'MasterController@storeStaffRegistration'));
Route::get('master/clinic/staff_register/view/{staffRegID}','MasterController@viewStaffRegistration');
Route::get('master/clinic/staff_register/slot_time_details/view/{staffRegID}','MasterController@viewPrimaryVisitingClinic');
Route::get('master/clinic/staff_register/primary_clinic_slot_time_details/view/{staff_id}', 'MasterController@viewPrimaryClinicSlotTimeDetails');
Route::get('master/clinic/staff_register/visiting_clinic_slot_time_details/view/{staff_id}', 'MasterController@viewVisitingClinicSlotTimeDetails');
Route::get('master/clinic/staff_register/edit/{staffRegID}','MasterController@editStaffRegistration');
Route::post('master/clinic/staff_register/update',array('as'=>'updateStaffRegistration','uses'=>'MasterController@updateStaffRegistration'));
/*Route::get('master/clinic/staffregister/slotdetails/add', 'MasterController@createStaffRegistrationSlotDetails');
*/

Route::get('master/clinic/staff_register/slot_time_details', 'MasterController@createStaffRegistrationSlotTimeDetails');
Route::get('master/clinic/staff_register/slot_time_details/edit/{staffRegID}', 'MasterController@editStaffRegistrationSlotTimeDetails');
Route::post('master/clinic/staff_register/slot_time_details/add', array('as'=>'addstaffregistrationslotdetails','uses'=>'MasterController@storeClinicSlotTimeDetails'));

Route::get('master/clinic/staff_register/visiting_clinic_slot_time_details/{id}','MasterController@createVisitingClinicSlotTimeDetails');
Route::post('master/clinic/staff_register/visiting_clinic_slot_time_details/add', array('as'=>'addvisitingclinicslotdetails','uses'=>'MasterController@storeVisitingClinicSlotTimeDetails'));
Route::get('master/clinic/staff_register/delete/{staff_clinic_id}/{staff_reg_id}','MasterController@deleteVisitingClinic');

Route::get('master/clinic/staff_register/primary_clinic_slot_time_details/{staff_id}', 'MasterController@createPrimaryClinicSlotTimeDetails');
Route::get('master/clinic/staff_register/primary_clinic_slot_time_details/status_delete/{clinicSlotTimingId}/{clinicId}', 'MasterController@makeStatusDeletePrimarySlotTimings');
Route::get('master/clinic/staff_register/visiting_clinic_slot_time_details/status_delete/{clinicSlotTimingId}/{clinicId}', 'MasterController@makeStatusDeleteVisitingSlotTimings');

/*Route::post('master/clinic/staffregister/primaryslotdetails/add', array('as'=>'addstaffregistrationslotdetails','uses'=>'MasterController@storeClinicSlotTimeDetails'));
*/

Route::get('master/clinic/staff_register/qualifications', 'MasterController@createStaffRegistrationQualifications');
Route::get('master/clinic/staff_register/specialization', 'MasterController@createStaffRegistrationSpecialization');

Route::get('master/clinic/leave_management/listview', 'MasterController@listViewLeaveManagement');
Route::get('master/clinic/leave_management/add', 'MasterController@createLeaveManagement');
Route::post('master/clinic/leave_management/addleave',array('as'=>'addleavemanagement','uses'=>'MasterController@storeLeaveManagement'));
Route::get('master/clinic/leave_management/full_day/edit/{staff_id}', 'MasterController@editStaffFullDayLeaveDetails');
Route::get('master/clinic/leave_management/partial_leave/edit/{staff_id}', 'MasterController@editStaffPartialLeaveDetails');
Route::post('master/clinic/leave_management/full_day/update',array('as'=>'updatefullleavemanagement','uses'=> 'MasterController@updateStaffFullDayLeaveDetails'));
Route::post('master/clinic/leave_management/partial_day/update',array('as'=>'updateleavemanagement','uses'=> 'MasterController@updateStaffPartialLeaveDetails'));
Route::post('master/clinic/leave_management/full_day/destroy/{id}', 'MasterController@destroyStaffFullDayLeaveDetails');
Route::post('master/clinic/leave_management/half_day/destroy/{id}', 'MasterController@destroyStaffHalfDayLeaveDetails');

Route::get('master/clinic/clinic/listview', 'MasterController@listViewClinic');
Route::get('master/clinic/clinic/details', 'MasterController@createClinicDetails');
Route::post('master/clinic/clinic/details/add', array('as'=>'addClinicDetails','uses'=>'MasterController@storeClinicDetails'));
Route::get('master/clinic/clinic/work_timings', 'MasterController@createClinicWorkTimings');
Route::post('master/clinic/clinic/work_timings/add', array('as'=>'addClinicWorkTimings','uses'=>'MasterController@storeClinicWorkTimings'));
Route::get('master/clinic/clinic/details/edit/{clinicId}', 'MasterController@editClinicDetails');
Route::post('master/clinic/clinic/details/edit', array('as'=>'updateClinicDetails','uses'=>'MasterController@updateClinicDetails'));
Route::get('master/clinic/clinic/work_timings/edit/{clinicId}', 'MasterController@editClinicWorkTimings');
Route::get('master/clinic/clinic/work_timings/status_delete/{clinicSlotTimingId}/{clinicId}', 'MasterController@makeStatusDeleteWorkTimings');
Route::post('master/clinic/clinic/work_timings/edit', array('as'=>'updateClinicWorkTimings','uses'=>'MasterController@updateClinicWorkTimings'));


////////************************MASTER Billing section*********************//////////////

Route::get('master/billing/bank/dashboard','MasterController@dashboardBank');
Route::post('master/billing/bank/dashboard/add1', array('as'=>'addbillingbank','uses'=>'MasterController@storeBillingBank'));


Route::get('master/billing/personal/dashboard','MasterController@dashboadPersonal');
Route::post('master/billing/personal/dashboard/add1', array('as'=>'addbillingpersonal','uses'=>'MasterController@storeBillingPersonal'));

////////************************PATIENT section*********************//////////////

Route::get('patient', 'PatientController@listView');
Route::post('patient/search', array('as'=>'patientdetails','uses'=>'PatientController@listViewPatientDetails'));
Route::get('patient/appointment/details/{patient_id}','PatientController@listViewAppointmentDetails');
Route::get('patient/treatment/details/{patient_id}','PatientController@listViewTreatmentDetails');
Route::get('patient/patient/details/{patient_id}','PatientController@listViewPatientInformation');
Route::get('patient/referrals/details/{patient_id}','PatientController@listViewReferralsInformation');

////////************************PLANNER section*********************//////////////

Route::get('planner/viewplanner', 'PlannerController@listView');

Route::get('planner/scheduling', 'PlannerController@listViewScheduling');
Route::post('planner/scheduling/search', array('as' => 'searchPatientDetails', 'uses' => 'PlannerController@listViewSearchScheduling'));
Route::post('planner/scheduling/newappointment/edit', array('as' => 'reschedulePatientInformation', 'uses' => 'PlannerController@reschedulePatientInformation'));
Route::post('planner/scheduling/newappointment/update',array('as'=>'updatereschedulePatientInformation','uses'=>'PlannerController@updateReschedulePatientInformation'));
Route::post('deleteRescheduleDetails', 'PlannerController@destroyRescheduleDetails');

Route::get('planner/scheduling/appointment/details/{patient_id}', 'PlannerController@listViewAppointmentDetails');
Route::get('planner/scheduling/treatment/details/{patient_id}', 'PlannerController@listViewTreatmentDetails');
Route::get('planner/scheduling/patient/details/{patient_id}', 'PlannerController@listViewPatientInformation');
Route::get('planner/scheduling/referrals/details/{patient_id}', 'PlannerController@listViewReferralsInformation');

Route::post('planner/scheduling/newappointment', array('as'=>'newAppointment','uses'=>'PlannerController@createNewAppointmentDetails'));
Route::post('planner/scheduling/newappointment/add', array('as'=>'addnewappointment','uses'=>'PlannerController@storeAppointmentDetails'));

Route::get('planner/followup/appointments', 'PlannerController@listViewAppointmentFollowUp');
Route::post('planner/followup/appointments/followup/search', array('as'=>'searchFollowUpDetails','uses'=>'PlannerController@searchFollowUpDetails'));
Route::get('planner/followup/appointments/newfollowup', 'PlannerController@createNewFollowUp');
Route::post('planner/followup/appointments/newfollowup/addnewfollowupdetails', array('as'=>'addnewfollowupdetails','uses'=>'PlannerController@storeFollowUpAppointmentDetails'));

Route::get('planner/followup/inventory/material/newfollowup', 'PlannerController@listViewMaterialInventoryFollowUp');
Route::get('planner/followup/inventory/instrument/newfollowup', 'PlannerController@listViewInstrumentInventoryFollowUp');
Route::get('planner/followup/inventory/labwork/newfollowup', 'PlannerController@listViewLabWorkFollowUp');
Route::get('planner/followup/maintenance/gadget/newfollowup', 'PlannerController@listViewGadgetMaintenanceFollowUp');
Route::get('planner/followup/maintenance/machine/newfollowup', 'PlannerController@listViewMachineMaintenanceFollowUp');


////////************************TREATMENT section*********************//////////////

Route::get('treatment','TreatmentController@index');
Route::post('treatment/search', array('as'=>'patientBasicDetails','uses'=>'TreatmentController@searchPatientBasicDetails'));
Route::get('treatment/getpatientdetails/{patient_id}', 'TreatmentController@getPatientDetails');
Route::get('treatment/treatmentnotes/GetDetailsTreatmentInfo','TreatmentController@GetDetailsTreatmentInfo');
Route::get('treatment/treatmentnotes/dashboardTreatmentNotes','TreatmentController@DashboardTreatmentNotes');
Route::get('treatment/treatmentnotes/GetDetailsTreatmentSitting','TreatmentController@DashboardTratmentSitting');

Route::get('treatment/treatmentnotes/{patient_id}','TreatmentController@storePatientTreament');
Route::get('treatment/treatmentnotes/general/{patient_treatment_id}','TreatmentController@createGeneralTreamentNotes');
Route::post('treatment/treatmentnotes/general/add', array('as'=>'storeGeneralTreamentNotes','uses'=>'TreatmentController@storeGeneralTreamentNotes'));
Route::post('treatment/treatmentnotes/general/update', array('as'=>'updateGeneralTreamentNotes','uses'=>'TreatmentController@updateGeneralTreamentNotes'));
Route::get('treatment/treatmentnotes/general/view/{patient_treatment_id}','TreatmentController@viewGeneralTreamentNotes');

Route::get('treatment/treatmentnotes/showstopper/{patient_treatment_id}','TreatmentController@createShowstopperTreamentNotes');
Route::post('treatment/treatmentnotes/showstopper/add', array('as'=>'storeShowstopperTreamentNotes','uses'=>'TreatmentController@storeShowstopperTreamentNotes'));
Route::post('treatment/treatmentnotes/showstopper/update', array('as'=>'updateShowstopperTreamentNotes','uses'=>'TreatmentController@updateShowstopperTreamentNotes'));
Route::get('treatment/treatmentnotes/showstopper/view/{patient_treatment_id}','TreatmentController@viewShowstopperTreamentNotes');

Route::get('treatment/treatmentnotes/consultant/{patient_treatment_id}','TreatmentController@createConsultantTreamentNotes');
Route::post('treatment/treatmentnotes/consultant/add', array('as'=>'storeConsultantTreamentNotes','uses'=>'TreatmentController@storeConsultantTreamentNotes'));
Route::post('treatment/treatmentnotes/consultant/update', array('as'=>'updateConsultantTreamentNotes','uses'=>'TreatmentController@updateConsultantTreamentNotes'));
Route::get('treatment/treatmentnotes/consultant/view/{patient_treatment_id}','TreatmentController@viewConsultantTreamentNotes');

Route::get('treatment/treatmentnotes/quatations/{patient_treatment_id}','TreatmentController@createQuatationsTreamentNotes');
Route::post('treatment/treatmentnotes/quatations/add', array('as'=>'storeQuatationsTreamentNotes','uses'=>'TreatmentController@storeQuatationsTreamentNotes'));
Route::post('treatment/treatmentnotes/quatations/update', array('as'=>'updateQuatationsTreamentNotes','uses'=>'TreatmentController@updateQuatationsTreamentNotes'));
Route::get('treatment/treatmentnotes/quatations/view/{patient_treatment_id}','TreatmentController@viewQuatationsTreamentNotes');

Route::get('treatment/treatmentnotes/visitingclinic/{patient_treatment_id}','TreatmentController@createVisitingClinicTreamentNotes');
Route::post('treatment/treatmentnotes/visitingclinic/add', array('as'=>'storeVisitingClinicTreamentNotes','uses'=>'TreatmentController@storeVisitingClinicTreamentNotes'));
Route::post('treatment/treatmentnotes/visitingclinic/update', array('as'=>'updateVisitingClinicTreamentNotes','uses'=>'TreatmentController@updateVisitingClinicTreamentNotes'));
Route::get('treatment/treatmentnotes/visitingclinic/view/{patient_treatment_id}','TreatmentController@viewVisitingClinicTreamentNotes');

Route::get('treatment/treatmentnotes/findingnotes/{patient_treatment_id}','TreatmentController@createFindingNotesTreamentNotes');
Route::post('treatment/treatmentnotes/findingnotes/add', array('as'=>'storeFindingNotesTreamentNotes','uses'=>'TreatmentController@storeFindingNotesTreamentNotes'));
Route::post('treatment/treatmentnotes/findingnotes/update', array('as'=>'updateFindingNotesTreamentNotes','uses'=>'TreatmentController@updateFindingNotesTreamentNotes'));
Route::get('treatment/treatmentnotes/findingnotes/view/{patient_treatment_id}','TreatmentController@viewFindingNotesTreamentNotes');

Route::get('treatment/sittings/{patient_treatment_id}','TreatmentController@listViewSitting');
Route::get('treatment/sittings/view/{patient_treatment_id}','TreatmentController@viewListViewSitting');
Route::get('treatment/sittings/create/{patient_treatment_id}','TreatmentController@storeSitting');

Route::get('treatment/sittings/general/{sittings_id}','TreatmentController@createGeneralSitting');
Route::post('treatment/sittings/general/add', array('as'=>'storeGeneralSitting','uses'=>'TreatmentController@storeGeneralSitting'));
Route::post('treatment/sittings/general/update', array('as'=>'updateGeneralSitting','uses'=>'TreatmentController@updateGeneralSitting'));
Route::get('treatment/sittings/general/view/{sittings_id}','TreatmentController@viewGeneralSitting');

Route::get('treatment/sittings/labwork/{sittings_id}','TreatmentController@listViewLabWorkSitting');
Route::post('treatment/sittings/labwork/status/rework', array('as'=>'makeStatusRework','uses'=>'TreatmentController@makeStatusRework'));
Route::post('treatment/sittings/labwork/status/complete', array('as'=>'makeStatusComplete','uses'=>'TreatmentController@makeStatusComplete'));
Route::get('treatment/sittings/labwork/view/{sittings_id}','TreatmentController@viewListViewLabWorkSitting');
Route::get('treatment/sittings/labwork/add/{sittings_id}','TreatmentController@createLabWorkSitting');
Route::post('treatment/sittings/labwork/add', array('as'=>'addlabWorkSitting','uses'=>'TreatmentController@storeLabWorkSitting'));
Route::get('treatment/sittings/labwork/add/view/{lab_work_id}','TreatmentController@viewLabWorkSitting');

Route::get('treatment/sittings/attachment/{sittings_id}','TreatmentController@createAttachmentSitting');
Route::post('treatment/sittings/attachment/add', array('as'=>'storeAttachmentSitting','uses'=>'TreatmentController@storeAttachmentSitting'));
Route::get('treatment/sittings/attachment/view/{sittings_id}','TreatmentController@viewAttachmentSitting');

Route::get('treatment/sittings/diagonosis/{sittings_id}','TreatmentController@createDiagnosisSitting');
Route::post('treatment/sittings/diagonosis/add', array('as'=>'storeDiagnosisSitting','uses'=>'TreatmentController@storeDiagnosisSitting'));
Route::post('treatment/sittings/diagonosis/update', array('as'=>'updateDiagnosisSitting','uses'=>'TreatmentController@updateDiagnosisSitting'));
Route::get('treatment/sittings/diagonosis/view/{sittings_id}','TreatmentController@viewDiagnosisSitting');

Route::get('treatment/labwork/{patient_treatment_id}','TreatmentController@listViewLabWork');
Route::get('treatment/labwork/view/{patient_treatment_id}','TreatmentController@viewListViewLabWork');
Route::get('treatment/labwork/add/view/{patient_treatment_id}','TreatmentController@viewLabWork');


////////************************LAB section*********************//////////////

Route::get('labs/labdetails', 'LabsController@index');
Route::get('labs/trackworkdetails', 'LabsController@trackWork');
Route::get('labs/assign/labworkinfo', 'LabsController@assignlabinfo');
Route::get('labs/assign/viewdetails','LabsController@viewdetails');
Route::get('labs/assign/assignwork/{lab_work_id}', 'LabsController@assignwork');
Route::post('labs/assign/assignwork/labdetails/{labName}','LabsController@getLabDetails');
//to add assignwork
Route::post('labs/assign/assignwork/addassignwork',array('as'=>'addassignwork','uses'=>'LabsController@storeAssignWorkDetails'));
Route::post('labs/assign/assignwork/updateassignwork',array('as'=>'updateassignwork','uses'=>'LabsController@updateAssignWorkDetails'));
Route::get('labs/assign/labform/{lab_work_id}', 'LabsController@assignworklabform');
Route::get('labs/assign/delivery/{lab_work_id}', 'LabsController@assignworkdelivery');
Route::post('labs/assign/delivery/add',array('as'=>'addDelivery','uses'=>'LabsController@storeDelivery'));
Route::get('labs/assign/billing/{lab_work_id}', 'LabsController@assignworkbilling');
Route::post('labs/assign/billing/add',array('as'=>'addBilling','uses'=>'LabsController@storeBillinDetails'));
Route::post('labs/assign/billing/update',array('as'=>'updateBilling','uses'=>'LabsController@updateBillinDetails'));
//to display information of Assign work under when user click on the get details link under Labs->Track work

Route::get('labs/trackwork/getdetails/assignwork/{id}','LabsController@getDetailsWork');
Route::geT('labs/trackwork/getdetails/labform/{id}','LabsController@getDetailsLabForm');
Route::get('labs/trackwork/getdetails/delivery/{id}','LabsController@getDetailsDelivery');
Route::get('labs/trackwork/getdetails/billing/{id}','LabsController@getDetailsBilling');
// to generate rework
Route::get('labs/trackwork/generaterework/assignwork/{id}','LabsController@generateRework');
Route::post('labs/trackwork/editgeneraterework',array('as'=>'editgeneraterework','uses'=>'LabsController@editGenerateRework'));
Route::post('labs/trackwork/addgeneraterework',array('as'=>'addgeneraterework','uses'=>'LabsController@addGenerateRework'));

Route::get('labs/trackwork/generaterework/labform/{id}','LabsController@generateReworkLabForm');

Route::get('labs/trackwork/generaterework/delivery/{id}','LabsController@generateReworkDelivery');
Route::post('labs/trackwork/generaterework/updategenerateDelivery',array('as'=>'updategenerateDelivery','uses'=>'LabsController@updateGenerateReworkDelivery'));

Route::get('labs/trackwork/generaterework/billing/{id}','LabsController@generateReworkBilling');
Route::post('labs/trackwork/generaterework/addGenerateReworkBilling',array('as'=>'addGenerateReworkBilling','uses'=>'LabsController@addGenerateReworkBilling'));
Route::post('labs/trackwork/generaterework/updateGenerateReworkBilling',array('as'=>'updateGenerateReworkBilling','uses'=>'LabsController@updateGenerateReworkBilling'));

Route::get('labs/assign/delivery/deletechallanno/{id}','LabsController@DeleteChallanNo');
Route::get('labs/assign/assignwork/deleteinstrument/{assign_lab_work_id}/{instrument_name_id}','LabsController@deleteinstrumentList');

Route::get('labs/assign/delivery/deleteDeliveryDetails/{id}/{t_assign_lab_work_id}','LabsController@deleteDeliveryDetails');

Route::get('labs/trackwork/deleteinstrument/{assign_lab_work_id}/{instrument_name_id}','LabsController@deleteTrackworkinstrument');
Route::get('labs/trackwork/deletechallan/{challan_id}/{t_assign_lab_work_id}','LabsController@deleteTrachworkchallannumber');


////////************************INVENTORY section*********************//////////////

Route::get('inventory/material_management/listview','InventoryController@listViewMaterialManagement');
Route::get('inventory/material_management/get_details/{material_id}','InventoryController@getDetailsMaterial');

Route::get('inventory/instrument_management/listview','InventoryController@listViewInstrumentManagement');
Route::get('inventory/instrument_management/get_details/{instrument_id}','InventoryController@getDetailsInstrumentManagement');
Route::get('inventory/instrument_management/instrument_in_lab','InventoryController@InstrumentInLab');

Route::get('inventory/machine_management/listview','InventoryController@listViewMachineManagement');
Route::get('inventory/machine_management/get_details/{po_id}','InventoryController@getdetailsMachine');

Route::get('inventory/gadget_management/listview','InventoryController@listViewGadgetManagement');
Route::get('inventory/gadget_management/get_details/{po_id}','InventoryController@getdetailsGadget');

/*material*/
Route::get('inventory/purchase_order/material/listview','InventoryController@dashboardPurchaseorder');
Route::get('inventory/purchase_order/material/add/{po_id}','InventoryController@PurchaseOrderAdd');
Route::post('inventory/purchase_order/material/add',array('as'=>'addPurchaseOrder','uses'=>'InventoryController@strorePurchaseorder'));
Route::post('inventory/purchase_order/material/edit',array('as'=>'editPurchaseOrder','uses'=>'InventoryController@updatePurchaseorder'));
Route::get('inventory/purchase_order/material/vendor_details/{po_id}','InventoryController@VendorDetails');
Route::post('inventory/purchase_order/material/vendor_details/material/add',array('as'=>'addVendorDetailsMaterial','uses'=>'InventoryController@storevendorDetailsMaterial'));
Route::Post('inventory/purchase_order/material/vendor_details/vendor/add',array('as'=>'addPurchaseOrderVendor','uses'=>'InventoryController@storePurchaseorderVendor'));
Route::get('inventory/purchase_order/material/generate_delivery/{po_id}','InventoryController@GenerateDeliveryAdd');
Route::post('inventory/purchase_order/material/generate_delivery/add',array('as'=>'addGenarateDelivery','uses'=>'InventoryController@storeGenarateDelivery'));
Route::post('inventory/purchase_order/material/generate_delivery/edit',array('as'=>'editGenarateDelivery','uses'=>'InventoryController@updateGenarateDelivery'));
Route::get('inventory/purchase_order/material/generate_delivery/delete/{po_id}/{id}','InventoryController@deleteDelivery');
Route::get('inventory/purchase_order/material/generate_billing/{po_id}','InventoryController@GenerateBillingAdd');
Route::post('inventory/purchase_order/material/generate_billing/add',array('as'=>'addGenarateBilling','uses'=>'InventoryController@storeGenerateBilling'));
Route::post('inventory/purchase_order/material/generate_billing/edit',array('as'=>'editGenarateBilling','uses'=>'InventoryController@updateGeneralBilling'));

Route::post('inventory/purchase_order/material/vendor_details/update',array('as'=>'updateVendorDetailsMaterial','uses'=>'InventoryController@updateVendorDetailsMaterial'));


// Route::get('inventory/purchase_order/material/generate_billing/edit/{po_id}','InventoryController@listviewGenerateBillingMaterial');
/*view po*/
Route::get('inventory/purchase_order/material/view/{po_id}','InventoryController@viewPurchaseOrderMaterial');
Route::get('inventory/purchase_order/material/vendor_details/view/{po_id}','InventoryController@viewvendorDetailsMaterial');
Route::get('inventory/purchase_order/material/generate_delivery/view/{po_id}','InventoryController@viewGenerateDeliveryMaterial');
Route::get('inventory/purchase_order/material/generate_billing/view/{po_id}','InventoryController@viewgenerateBillingMaterial');

/*instrument*/
Route::get('inventory/purchase_order/instrument/listview','InventoryController@listviewInstrument');
Route::get('inventory/purchase_order/instrument/add/{po_id}','InventoryController@addPurchaseorderInstrument');
Route::post('inventory/purchase_order/instrument/add',array('as'=>'addPurchaseOrderInstrument','uses'=>'InventoryController@storePurchaseOrderInstrument'));
Route::post('inventory/purchase_order/instrument/update',array('as'=>'updatePurchaseOrderInstrument','uses'=>'InventoryController@updatePurchaseOrderInstrument'));
Route::get('inventory/purchase_order/instrument/delete/{instrument_name_id}/{po_id}','InventoryController@deleteInstrument');
Route::get('inventory/purchase_order/instrument/vendor_details/{po_id}','InventoryController@addVendorDetailsInstrument');
Route::post('inventory/purchase_order/instrument/vendor_details/add',array('as'=>'addVendorDetailsInstrument','uses'=>'InventoryController@storeVendorDetailsInstrument'));
Route::get('inventory/purchase_order/instrument/generate_delivery/{po_id}','InventoryController@addGenerateDeliveryInstrument');
Route::post('inventory/purchase_order/instrument/generate_delivery/add',array('as'=>'addDeliveyrInstrument','uses'=>'InventoryController@storeDeliveryInstrument'));
Route::get('inventory/purchase_order/instrument/generate_delivery/delete/{po_id}/{id}','InventoryController@deleteInstrumentChallan');
Route::get('inventory/purchase_order/instrument/generate_billing/{po_id}','InventoryController@addGenerateBillingInstrument');
Route::post('inventory/purchase_order/instrument/generate_billing/add',array('as'=>'addGenerateBillingInstrument','uses'=>'InventoryController@storegenareteBillingInstrument'));

Route::post('inventory/purchase_order/instrument/vendor_details/update',array('as'=>'updateVendorDetailsInstrument','uses'=>'InventoryController@updateVendorDetailsInstrument'));

Route::post('inventory/purchase_order/instrument/generate_billing/edit',array('as'=>'editGenarateBillingInstrument','uses'=>'InventoryController@updateGenerateBillingInstrument'));
/*view*/
Route::get('inventory/purchase_order/instrument/view/{po_id}','InventoryController@viewPurchaseOrderInstrument');
Route::get('inventory/purchase_order/instrument/vendor_details/view/{po_id}','InventoryController@viewVendorDetailsInstrument');
Route::get('inventory/purchase_order/instrument/generate_delivery/view/{po_id}','InventoryController@viewGenerateDeliveryInstrument');
Route::get('inventory/purchase_order/instrument/generate_billing/view/{po_id}','InventoryController@viewGenerateBillingInstrument');

Route::get('inventory/purchase_order/instrument/generate_billing/edit','InventoryController@listviewGenerateBillingInstrument');
/*gadget*/
Route::get('inventory/purchase_order/gadget/listview','InventoryController@listviewGadget');
Route::get('inventory/purchase_order/gadget/add/{po_id}','InventoryController@addPurcahseorderGadget');
Route::post('inventory/purchase_order/gadget/add',array('as'=>'addPurchaseOrderGadget','uses'=>'InventoryController@storePurchaseOrderGadget'));
Route::post('inventory/purchase_order/gadget/update',array('as'=>'updatePurchaseOrderGadget','uses'=>'InventoryController@updatePurchaseOrderGadget'));
Route::get('inventory/purchase_order/gadget/delete/{gadget_name_id}/{po_id}','InventoryController@deleteGadget');
Route::get('inventory/purchase_order/gadget/vendor_details/{po_id}','InventoryController@addVendorDetailsGadget');
Route::post('inventory/purchase_order/gadget/vendor_details/add',array('as'=>'addVendorDetailsGadget','uses'=>'InventoryController@storeVendorDetailsGadget'));

Route::get('inventory/purchase_order/gadget/generate_delivery/{po_id}','InventoryController@addGenerateDeliveryGadget');
Route::post('inventory/purchase_order/gadget/generate_delivery/',array('as'=>'addGenerateDeliveryGadget','uses'=>'InventoryController@storeGenerateDeliveryGadget'));
Route::get('inventory/purchase_order/gadget/generate_delivery/delete/{po_id}/{id}','InventoryController@deleteGadgetChallan');
Route::get('inventory/purchase_order/gadget/generate_billing/{po_id}','InventoryController@addGenerateBillingGadget');
Route::post('inventory/purchase_order/gadget/generate_billing/add',array('as'=>'addGenerateBillingGadget','uses'=>'InventoryController@storeganerateBillingGadget'));
Route::post('inventory/purchase_order/gadget/generate_billing/edit',array('as'=>'editGenarateBillingGadget','uses'=>'InventoryController@updateGenerateBillingGadget'));

Route::post('inventory/purchase_order/gadget/vendor_details/update',array('as'=>'updateVendorDetailsGadget','uses'=>'InventoryController@updateVendorDetailsGadget'));
/*view*/

Route::get('inventory/purchase_order/gadget/view/{po_id}','InventoryController@viewPurchaseOrderGadget');
Route::get('inventory/purchase_order/gadget/vendor_details/view/{po_id}','InventoryController@viewVendorDetailsGadget');
Route::get('inventory/purchase_order/gadget/generate_delivery/view/{po_id}','InventoryController@viewGenerateDeliveryGadget');
Route::get('inventory/purchase_order/gadget/generate_billing/view/{po_id}','InventoryController@viewGenerateBillingGadget');

Route::get('inventory/purchase_order/gadget/generate_billing/edit/{po_id}','InventoryController@generateBillingGadgetListview');

/*machine*/
Route::get('inventory/purchase_order/machine/listview','InventoryController@listviewMachine');

Route::get('inventory/purchase_order/machine/add/{po_id}','InventoryController@addPurchaseOrderMachine');
Route::post('inventory/purchase_order/machine/add',array('as'=>'addPurchaseOrderMachine','uses'=>'InventoryController@storePurchaseOrderMachine'));
Route::post('inventory/purchase_order/machine/update',array('as'=>'updatePurchaseOrderMachine','uses'=>'InventoryController@updatePurchaceOrderMachine'));
Route::get('inventory/purchase_order/machine/vendor_details/{po_id}','InventoryController@addVendorDetailsMachine');
Route::post('inventory/purchase_order/machine/vendor_details/add',array('as'=>'addVendorDetailsMachine','uses'=>'InventoryController@storeVendorDetailsMachine'));

Route::get('inventory/purchase_order/machine/delete/{machine_name_id}/{po_id}','InventoryController@deleteMachine');
Route::get('inventory/purchase_order/machine/generate_delivery/{po_id}','InventoryController@addGenerateDeliveryMachine');
Route::post('inventory/purchase_order/machine/generate_delivery/add',array('as'=>'adddeliveyrmachine','uses'=>'InventoryController@storeGenerateDeliveryMachine'));
Route::get('inventory/purchase_order/machine/generate_delivery/delete/{po_id}/{id}','InventoryController@deleteMachineChallan');
Route::get('inventory/purchase_order/machine/generate_billing/{po_id}','InventoryController@addGenerateBillingMachine');
Route::Post('inventory/purchase_order/machine/generate_billing/add',array('as'=>'addgeneratebillingmachine','uses'=>'InventoryController@storeGenerateBillingMachine'));


Route::post('inventory/purchase_order/machine/generate_billing/edit',array('as'=>'editgenaratebillingmachine','uses'=>'InventoryController@updateGenerateBillingMachine'));

Route::get('inventory/purchase_order/machine/delete/{mat_id}/{po_id}','InventoryController@deleteMaterial');

Route::get('inventory/purchase_order/machine/generate_billing/edit/{po_id}','InventoryController@listviewMachineGenerateBilling');

Route::post('inventory/purchase_order/machine/vendor_details/update',array('as'=>'updatevendordetailsmachine','uses'=>'InventoryController@updateVendordetailsMachine'));

/*view*/

Route::get('inventory/purchase_order/machine/view/{po_id}','InventoryController@viewDetailsaddMachine');
Route::get('inventory/purchase_order/machine/vendor_details/view/{po_id}','InventoryController@viewVendorDetailsMachine');
Route::get('inventory/purchase_order/machine/generate_delivery/view/{po_id}','InventoryController@viewGenerateDeliveryMachine');
Route::get('inventory/purchase_order/machine/generate_billing/view/{po_id}','InventoryController@viewGenerateBillingMachine');


Route::get('inventory/alert/listview','InventoryController@dashboardAlert');

Route::get('inventory/alert/instrument/listview','InventoryController@DashboardInstrumentSafetyStock');



////////************************VENDORS section*********************//////////////

Route::get('vendors/lab/by_vendor', 'VendorsController@index');
Route::get('vendors/lab/by_vendor/track_work/{vendor_lab_id}', 'VendorsController@labVendorTrackWork');
Route::get('vendors/lab/by_vendor/track_work', array('as' => 'trackWorkVendorLab', 'uses' => 'VendorsController@labVendorTrackWork'));
/*Route::get('vendor/lab/byvendor/instrumentdetails','VendorsController@labVendorInstrumentDetails');
*/
Route::get('vendors/lab/by_work/work/listview','VendorsController@listViewLabWork');
Route::post('vendors/lab/by_work/track_work/view', array('as' => 'viewVendorLabWork', 'uses' => 'VendorsController@viewByWorkLab'));


Route::get('vendors/material/byvendor', 'VendorsController@listViewMaterialVendor');
Route::post('vendor/material/byvendor/trackwork', array('as' => 'trackVendorMaterial', 'uses' => 'VendorsController@materialVendorTrackWork'));
Route::get('vendor/material/byvendor/trackwork','VendorsController@materialVendorTrackWork');
Route::get('vendor/material/bywork/workdashboard','VendorsController@listViewMaterialWork');
Route::post('vendor/material/bywork/trackwork/viewmaterialbywork', array('as' => 'viewVendorMaterialWork', 'uses' => 'VendorsController@viewByWorkMaterial'));



Route::get('vendors/instrument/byvendor', 'VendorsController@listViewMaterialInstrument');
Route::post('vendor/instrument/byvendor/trackwork', array('as' => 'trackVendorInstrument', 'uses' => 'VendorsController@instrumentVendorTrackWork'));
Route::get('vendor/instrument/bywork/workdashboard','VendorsController@listViewInstrumentWork');
Route::post('vendor/instrument/bywork/trackwork/viewinstrumentbywork', array('as' => 'viewVendorInstrumentWork', 'uses' => 'VendorsController@viewByWorkInstrument'));


Route::get('vendors/gadget/byvendor', 'VendorsController@listViewGadgetVendor');
Route::post('vendor/gadget/byvendor/trackwork', array('as' => 'trackVendorGadget', 'uses' => 'VendorsController@gadgetVendorTrackWork'));
Route::get('vendor/gadget/bywork/workdashboard','VendorsController@listViewGadgetWork');
Route::post('vendor/gadget/bywork/trackwork/viewgadgetbywork', array('as' => 'viewVendorGadgetWork', 'uses' => 'VendorsController@viewByWorkGadget'));



Route::get('vendors/machine/byvendor', 'VendorsController@listViewMachineVendor');
Route::post('vendor/machine/byvendor/trackwork', array('as' => 'trackVendorMachine', 'uses' => 'VendorsController@machineVendorTrackWork'));
Route::get('vendor/machine/bywork/workdashboard','VendorsController@listViewMachineWork');
Route::post('vendor/machine/bywork/trackwork/viewmachinebywork', array('as' => 'viewVendorMachineWork', 'uses' => 'VendorsController@viewByWorkMachine'));



Route::get('vendors/maintenance/byvendor', 'VendorsController@listViewMaintenanceVendor');
Route::post('vendor/maintenance/byvendor/trackwork', array('as' => 'trackVendorMaintenance', 'uses' => 'VendorsController@maintenanceVendorTrackWork'));
Route::get('vendor/maintenance/bywork/workdashboard','VendorsController@listViewMaintenanceWork');
Route::post('vendor/maintenance/bywork/trackwork/viewmaintenancebywork', array('as' => 'viewVendorMaintenanceWork', 'uses' => 'VendorsController@viewByWorkMaintenance'));


////////************************MAINTENANCE section*********************//////////////

Route::get('maintenance/gadget/nextduemaintenance','MaintenanceController@index');
Route::get('maintenance/gadget/maintenance','MaintenanceController@gadgetMaintenance');
Route::get('maintenance/gadget/assignedmaintenance','MaintenanceController@gadgetAssignedMaintenance');
Route::post('maintenance/gadget/assignedmaintenance/assignwork/add',array('as'=>'addmaintenanceassignwork','uses'=>'MaintenanceController@storeAssignWork'));
Route::post('maintenance/gadget/assignedmaintenance/assignwork/update',array('as'=>'editmaintenanceassignwork','uses'=>'MaintenanceController@updateAssignWork'));

Route::get('maintenance/machine/nextduemaintenance','MaintenanceController@machine');
Route::get('maintenance/machine/nextduemaintenance/assignmaintenance/{machine_id}','MaintenanceController@machineAssignWorkMaintenance');
Route::post('maintenance/machine/nextduemaintenance/assignmaintenance/addmaintenanceassignworkmachine',array('as'=>'addmaintenanceassignworkmachine','uses'=>'MaintenanceController@storeMachineWorkAssign'));
/**/
Route::post('maintenance/machine/nextduemaintenance/assignmaintenance/updatemaintenanceassignworkmachine',array('as'=>'updatemaintenanceassignworkmachine','uses'=>'MaintenanceController@updateMachineWorkAssign'));

Route::get('maintenance/machine/nextduemaintenance/assignmaintenance/complteMaintenance/{machine_id}','MaintenanceController@completeMaintenanceMachine');
Route::post('maintenance/machine/nextduemaintenance/assignmaintenance/complteMaintenance/addcompletemaintenancemachine',array('as'=>'addcompletemaintenancemachine','uses'=>'MaintenanceController@storeCompletemaintenanceMachine'));
/**/
Route::post('maintenance/machine/nextduemaintenance/assignmaintenance/complteMaintenance/editcompletemaintenancemachine',array('as'=>'editcompletemaintenancemachine','uses'=>'MaintenanceController@updateCompletemaintenanceMachine'));
Route::get('maintenance/machine/nextduemaintenance/assignmaintenance/extracharges/{machine_id}','MaintenanceController@extraChargegMachine');
Route::post('maintenance/machine/nextduemaintenance/assignmaintenance/extracharges/addextrachargesmachine',array('as'=>'addextrachargesmachine','uses'=>'MaintenanceController@storeExtrechargesMachine'));

Route::post('maintenance/machine/nextduemaintenance/assignmaintenance/extracharges/updateextrachargesmachine',array('as'=>'updateextrachargesmachine','uses'=>'MaintenanceController@updateExtrechargesMachine'));

Route::get('maintenance/machine/completedmaintenance','MaintenanceController@machineCompletedMaintenance');
Route::get('maintenance/machine/assignedmaintenance','MaintenanceController@machineAssignedMaintenance');

Route::get('maintenance/machine/maintenance/completedmaintenance/getdetailsassignwork/{machine_id}','MaintenanceController@getDetailsAssignWorkMachine');
Route::get('maintenance/machine/maintenance/completedmaintenance/getdetailscompletedmaintenance/{machine_id}','MaintenanceController@getDetailsCompletedworkMachine');
Route::get('maintenance/machine/maintenance/completedmaintenance/getdetailsextracharges/{machine_id}','MaintenanceController@getDetailsextrachargesMachine');

Route::get('maintenance/machine/maintenance/completedmaintenance/assignmaintenancemachine/{machine_id}','MaintenanceController@completedMaintenanceAssignWorkMachine');
Route::post('maintenance/machine/maintenance/completedmaintenance/assignmaintenancemachine/addmaintenanceassignworkm',array('as'=>'addmaintenanceassignworkm','uses'=>'MaintenanceController@storeAssignWorkCompletedMachine'));
Route::get('maintenance/machine/maintenance/completedmaintenance/assignmaintenancemachine/completedmaintenance/{machine_id}','MaintenanceController@CompleteMaintenanceComoletedMaintenance');
Route::post('maintenance/machine/maintenance/completedmaintenance/assignmaintenancemachine/completedmaintenance/addcompletemaintenancemachineform',array('as'=>'addcompletemaintenancemachineform','uses'=>'MaintenanceController@storeCompletedMaintenancMachineM'));
Route::get('maintenance/machine/maintenance/completedmaintenance/assignmaintenancemachine/extracharges/{machine_id}','MaintenanceController@CompleteMaintenanceExtracharges');
Route::post('maintenance/machine/maintenance/completedmaintenance/assignmaintenancemachine/extracharges/addextrachargesmachineCompltedform',array('as'=>'addextrachargesmachineCompltedform','uses'=>'MaintenanceController@stroreExtrachargesMachineCompleted'));

Route::get('gadget/maintenance/assignmaintenance/assignwork/{id}','MaintenanceController@machineAssignWork');
Route::get('gadget/maintenance/assignmaintenance/completedmaintenance/{id}','MaintenanceController@gadgetCompletedMaintenance');
Route::post('gadget/maintenance/assignmaintenance/completedmaintenance/addcompletemaintenance',array('as'=>'addcompletemaintenance','uses'=>'MaintenanceController@storeCompletemaintenance'));
Route::post('gadget/maintenance/assignmaintenance/completedmaintenance/editcompletemaintenance',array('as'=>'editcompletemaintenance','uses'=>'MaintenanceController@editCompletemaintenance'));
Route::get('gadget/maintenance/assignmaintenance/extracharges/{id}','MaintenanceController@extraCharges');
Route::Post('gadget/maintenance/assignmaintenance/extracharges/addgadgetextracharges',array('as'=>'addgadgetextracharges','uses'=>'MaintenanceController@storegadgetextracharges'));
Route::post('gadget/maintenance/assignmaintenance/extracharges/addextracharges',array('as'=>'addextracharges','uses'=>'MaintenanceController@storeExtracharges'));
Route::Post('gadget/maintenance/assignmaintenance/extracharges/addextracharges',array('as'=>'editextracharges','uses'=>'MaintenanceController@updateExteracharges'));

Route::get('maintenance/gadget/maintenance/completemaintenance/getdetails/{id}','MaintenanceController@getDetailsAssignwork');
Route::get('maintenance/gadget/maintenance/completemaintenance/getdetails/completedmaintenance/{id}','MaintenanceController@getDetailsCompletedMaintenance');
Route::get('maintenance/gadget/maintenance/completemaintenance/getdetails/extracharges/{id}','MaintenanceController@getDetailsExteracharges');

Route::get('maintenance/gadget/maintenance/completedmaintenance/assignmaintenance/{id}','MaintenanceController@gadgetCompletedMaintenanceAssignWork');
Route::post('maintenance/gadget/maintenance/completedmaintenance/assignmaintenance/addmaintenancegadgetcompleteassignwork',array('as'=>'addgadgetassignworkcomplete','uses'=>'MaintenanceController@storeGadgetCompleteAssignWork'));
Route::get('maintenance/gadget/maintenance/completedmaintenance/assignmaintenance/completedmaintenance/{id}','MaintenanceController@gadgetCompletedMaintenanceCompltedM');
Route::post('maintenance/gadget/maintenance/completedmaintenance/assignmaintenance/completedmaintenance/addgadgetcompletemaintenance',array('as'=>'addgadgetcompletemaintenance','uses'=>'MaintenanceController@storeGadgetCompleteMaintenance'));
Route::get('maintenance/gadget/maintenance/completedmaintenance/assignmaintenance/extracharges/{id}','MaintenanceController@gadgetCompletedMaintenanceExtracharges');
Route::post('maintenance/gadget/maintenance/completedmaintenance/assignmaintenance/extracharges/addgadgetcompleteextracharges',array('as'=>'addgadgetcompleteextracharges','uses'=>'MaintenanceController@storeGadgetCompleteExteracharges'));


////////************************BILLING section*********************//////////////

Route::get('billing/treatment', 'BillingController@index');
Route::get('billing/treatment/prevoius_bill_details/{patient_treatment_id}', 'BillingController@prevoiusBillDetails');
Route::get('billing/treatment/generate_bill/{patient_treatment_id}', 'BillingController@generateBill');
Route::post('billing/treatment/generate_bill/store', array('as' => 'storeBill', 'uses' => 'BillingController@storeBill'));

Route::get('billing/inventory/material', 'BillingController@listViewMaterial');
Route::get('billing/inventory/material/makepayment', 'BillingController@makePaymentMaterial');
Route::get('billing/inventory/instrument', 'BillingController@listViewInstrument');
Route::get('billing/inventory/instrument/makepayment', 'BillingController@makePaymentInstrument');
Route::get('billing/inventory/gadget', 'BillingController@listViewGadget');
Route::get('billing/inventory/gadget/makepayment', 'BillingController@makePaymentGadget');
Route::get('billing/inventory/machine', 'BillingController@listViewMachine');
Route::get('billing/inventory/machine/makepayment', 'BillingController@makePaymentMachine');

Route::get('billing/labwork', 'BillingController@listViewLabWork');
Route::get('billing/labwork/getdetails', 'BillingController@getDetailsLabWork');
Route::get('billing/labwork/material/makepayment', 'BillingController@makePaymentLabwork');

Route::get('billing/maintenance/contractpayment', 'BillingController@listViewContractWorkMaintenance');
Route::get('billing/maintenance/contractpayment/makepayment', 'BillingController@makePaymentContractWorkMaintenance');
Route::get('billing/maintenance/serviceamount', 'BillingController@listViewServiceAmountMaintenance');

Route::get('billing/salary', 'BillingController@listViewSalary');
Route::get('billing/salary/addDetails', 'BillingController@createSalaryDetails');
Route::post('billing/salary/store', array('as' => 'storeSalaryDetails', 'uses' => 'BillingController@storeSalaryDetails'));
Route::get('billing/salary/view/{salary_id}', 'BillingController@viewSalaryDetails');

Route::get('billing/cashmanagement', 'BillingController@listViewCashManagement');
Route::get('billing/cashmanagement/transfercash', 'BillingController@transferCashCashManagement');

Route::get('billing/miscellaneous', 'BillingController@listViewMiscellaneous');
Route::get('billing/miscellaneous/addDetails', 'BillingController@createMiscellaneous');

Route::post('billing/miscellaneous/store',array('as'=>'storeMiscellaneous','uses'=>'BillingController@storeMiscellaneous'));


//Ajax functions

Route::post('work_type/{work_type}','SettingsController@getWorkSubtype');

Route::post('master/lab/work_type/{work_type_id}','MasterController@getLabWorkDetails');
Route::post('master/material/material_type/{material_type_id}','MasterController@getMaterialDetails');
Route::post('master/instrument/instrument_type/{instrument_type_id}','MasterController@getInstrumentDetails');
Route::post('material_type/{material_type_id}','MasterController@getMaterialSubtype');
Route::post('instrument_type/{insrument_type_id}','MasterController@getInstrumentSubtype');
Route::post('machine_type/{machine_type_id}','MasterController@getMachineSubtype');
Route::post('gadget_type/{gadget_type_id}','MasterController@getGadgetSubtype');
Route::post('treatment_type/{treat_type_id}','MasterController@getTreatmentName');

Route::post('treatment/treatmentnotes/general/{clinic_id}','TreatmentController@getClinicName');
Route::post('treatment/sittings/general/{appointment_id}','TreatmentController@getAppointmentDetails');

Route::post('planner/followup/appointment/{patient_id}','PlannerController@getPatientCellNo');
Route::post('maintenance/gadget/assignedmaintenance/changestatus/{gadgetId}','MaintenanceController@changeStatusComplete');
Route::post('maintenance/machine/assignedmaintenance/changestatus/{machineId}','MaintenanceController@changeStatusCompleteMachine');

Route::post('inventory/purchaseorder/materialsubtype/{material_id}','InventoryController@getmaterialsubtype');

Route::post('inventory/purchaseorder/materialname/{materialsubtype_id}','InventoryController@getmaterilaname');

Route::post('inventory/purchaseorder/instrumentsubtype/{instrument_id}','InventoryController@getinstrumentsubtype');

Route::post('inventory/purchaseorder/instrumentname/{instrument_subtypeid}','InventoryController@getinstrumentname');

Route::post('inventory/purchaseorder/gadgetsubtype/{gadget_id}','InventoryController@getgadgetsubtype');

Route::post('inventory/purchaseorder/gadgetname/{gadget_subtypeid}','InventoryController@getgadgetname');

Route::post('inventory/purchaseorder/machinesubtype/{machine_id}','InventoryController@getmachinesubtype');

Route::post('inventory/purchaseorder/machinename/{machine_subtypeid}','InventoryController@getmachinename');
/*material names acc to vendor in inventory->po->material->vendor details*/
Route::post('inventory/purchaseorder/material/vendordetails/getmaterial/{van_id}/{po_id}','InventoryController@vendorMaterialNames');

/*Instrument names acc to vendor in inventory->po->instrument->vendor det*/

Route::post('inventory/purchaseorder/instrument/vendordetails/getinstrument/{van_id}/{po_id}','InventoryController@vendorInstrumentNames');

/*gadger names acc to vendor*/

Route::post('inventory/purchaseorder/gadget/vendordetails/getgadget/{van_id}/{po_id}','InventoryController@vendorGadgetNames');

/*machine names acc to vendor*/

Route::post('inventory/purchaseorder/machine/vendordetails/getmachine/{van_id}/{po_id}','InventoryController@vendorMachineNames');
