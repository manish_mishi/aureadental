<?php namespace App\Http\Controllers;

use App\Http\Requests;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VendorsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	
	public function index(Request $request)

	{
		$vendor_lab_details = DB::table('m_vendor_lab_workspec_details')

		->join('s_lab_work_type','m_vendor_lab_workspec_details.work_type_id','=','s_lab_work_type.id')
		->join('s_lab_work_subtype','m_vendor_lab_workspec_details.work_subtype_id','=','s_lab_work_subtype.id')
		->join('s_lab_work_name','m_vendor_lab_workspec_details.work_name_id','=','s_lab_work_name.id')
		->join('m_vendor_lab_details','m_vendor_lab_workspec_details.vendor_lab_id','=','m_vendor_lab_details.id')
		->select('*','s_lab_work_type.name as lab_work_type_name','s_lab_work_subtype.name as lab_work_subtype_name','s_lab_work_name.name as lab_work_name','m_vendor_lab_workspec_details.id as lab_workspec_id')
		->get();

		return view('vendors.lab.listViewVendorLab',compact('vendor_lab_details'));
	}


	public function labVendorTrackWork($vendor_lab_id, Request $request)
	{

		$vendor_lab_detail = DB::table('m_vendor_lab_workspec_details')
		->join('s_vendor_types','m_vendor_lab_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_vendor_lab_details','m_vendor_lab_details.id','=','m_vendor_lab_workspec_details.vendor_lab_id')
		->where('m_vendor_lab_details.id',$vendor_lab_id)
		->select('*','m_vendor_lab_workspec_details.id as m_vendor_lab_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		$vendor_lab_details = $vendor_lab_detail['0'];


		$lab_workspec_details = DB::table('m_vendor_lab_workspec_details')
		->where('m_vendor_lab_workspec_details.vendor_lab_id',$vendor_lab_id)
		->join('s_vendor_types','m_vendor_lab_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('s_lab_work_type','m_vendor_lab_workspec_details.work_type_id','=','s_lab_work_type.id')
		->join('s_lab_work_subtype','m_vendor_lab_workspec_details.work_subtype_id','=','s_lab_work_subtype.id')
		->join('s_lab_work_name','m_vendor_lab_workspec_details.work_name_id','=','s_lab_work_name.id')
		->select('*','s_lab_work_type.name as lab_work_type_name','s_lab_work_subtype.name as lab_work_subtype_name','m_vendor_lab_workspec_details.id as lab_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		$lab_workspec_detail = $lab_workspec_details['0'];

		return view('vendors.lab.viewVendorLab',compact('lab_workspec_detail','vendor_lab_details','vendor_lab_id'));
	}


	public function labVendorInstrumentDetails()
	{
		return view('vendors.lab.viewInstrumentDetails');
	}

	public function listViewLabWork()
	{ 
		$vendor_lab_details = DB::table('m_vendor_lab_workspec_details')

		->join('s_lab_work_type','m_vendor_lab_workspec_details.work_type_id','=','s_lab_work_type.id')
		->join('s_lab_work_subtype','m_vendor_lab_workspec_details.work_subtype_id','=','s_lab_work_subtype.id')
		->join('s_lab_work_name','m_vendor_lab_workspec_details.work_name_id','=','s_lab_work_name.id')
		->join('m_vendor_lab_details','m_vendor_lab_workspec_details.vendor_lab_id','=','m_vendor_lab_details.id')
		->select('*','s_lab_work_type.name as lab_work_type_name','s_lab_work_subtype.name as lab_work_subtype_name','s_lab_work_name.name as lab_work_name','m_vendor_lab_workspec_details.id as lab_workspec_id')
		->get();

		return view('vendors.lab.listViewWorkLab',compact('vendor_lab_details'));
	}

	public function listViewMaterialVendor()
	{

		$vendor_material_details=DB::table('t_purchase_order_material')
		->join('m_material_details','m_material_details.id','=','t_purchase_order_material.material_name_id')
		->join('m_vendor_material_details','m_vendor_material_details.id','=','t_purchase_order_material.material_name_id')
		->join('m_vendor_mat_workspec_details','m_vendor_mat_workspec_details.mat_name_id','=','t_purchase_order_material.material_name_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_material.purchase_order_id')
		->select('*','m_vendor_material_details.name as vendor_name','m_vendor_material_details.id as vendormat_id','m_vendor_mat_workspec_details.id as vendor_matworkspec_id')
		->get();

		/*$vendor_material_details = DB::table('m_vendor_mat_workspec_details')

		->join('s_material_type','m_vendor_mat_workspec_details.mat_type_id','=','s_material_type.id')
		->join('s_material_subtype','m_vendor_mat_workspec_details.mat_subtype_id','=','s_material_subtype.id')
		->join('m_material_details','m_vendor_mat_workspec_details.mat_name_id','=','m_material_details.id')
		->join('m_vendor_material_details','m_vendor_mat_workspec_details.vendor_mat_id','=','m_vendor_material_details.id')
		->select('*','s_material_type.name as material_type_name','s_material_subtype.name as material_subtype_name','m_material_details.material_name as material_name')
		->get();*/

		return view('vendors.material.listViewVendorMaterial',compact('vendor_material_details'));
	}

	public function materialVendorTrackWork(Request $request)
	{

		$vendor_material_id = $request->get('vendor_material_id');

$vendor_material_details=DB::table('t_purchase_order_material')
        ->where('material_name_id',$vendor_material_id)
		->join('m_material_details','m_material_details.id','=','t_purchase_order_material.material_name_id')
		->join('m_vendor_material_details','m_vendor_material_details.id','=','t_purchase_order_material.material_name_id')
		->join('m_vendor_mat_workspec_details','m_vendor_mat_workspec_details.mat_name_id','=','t_purchase_order_material.material_name_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_material.purchase_order_id')
		->select('*','m_vendor_material_details.name as vendor_name','m_vendor_material_details.id as vendormat_id','m_vendor_mat_workspec_details.id as vendor_matworkspec_id')
		->get();

$vendor_material_detail = $vendor_material_details['0'];


		/*$vendor_material_detail = DB::table('m_vendor_mat_workspec_details')
		->join('m_vendor_material_details','m_vendor_material_details.id','=','m_vendor_mat_workspec_details.vendor_mat_id')
		->where('m_vendor_material_details.id',$vendor_material_id)
		->select('*','m_vendor_mat_workspec_details.id as m_vendor_mat_workspec_id')
		->get();

		$vendor_material_details = $vendor_material_detail['0'];

		$vendor_mat_workspec_id = $request->get('vendor_mat_workspec_id');

		$material_workspec_details = DB::table('m_vendor_mat_workspec_details')
		->where('m_vendor_mat_workspec_details.vendor_mat_id',$vendor_material_id)
		->join('s_vendor_types','m_vendor_mat_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('s_material_type','m_vendor_mat_workspec_details.mat_type_id','=','s_material_type.id')
		->join('s_material_subtype','m_vendor_mat_workspec_details.mat_subtype_id','=','s_material_subtype.id')
		->join('m_material_details','m_vendor_mat_workspec_details.mat_name_id','=','m_material_details.id')
		->select('*','s_material_type.name as material_type_name','s_material_subtype.name as material_subtype_name','m_vendor_mat_workspec_details.id as mat_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		$mat_workspec_detail = $material_workspec_details['0'];
*/
		return view('vendors.material.viewVendorMaterial',compact('vendor_material_detail','mat_workspec_detail'));
	}

	public function listViewMaterialWork()
	{
		$vendor_material_details=DB::table('t_purchase_order_material')
		->join('m_material_details','m_material_details.id','=','t_purchase_order_material.material_name_id')
		->join('m_vendor_material_details','m_vendor_material_details.id','=','t_purchase_order_material.material_name_id')
		->join('m_vendor_mat_workspec_details','m_vendor_mat_workspec_details.mat_name_id','=','t_purchase_order_material.material_name_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_material.purchase_order_id')
		->select('*','m_vendor_material_details.name as vendor_name','m_vendor_material_details.id as vendormat_id','m_vendor_mat_workspec_details.id as vendor_matworkspec_id')
		->get();

		/*$vendor_material_details = DB::table('m_vendor_mat_workspec_details')

		->join('s_material_type','m_vendor_mat_workspec_details.mat_type_id','=','s_material_type.id')
		->join('s_material_subtype','m_vendor_mat_workspec_details.mat_subtype_id','=','s_material_subtype.id')
		->join('m_material_details','m_vendor_mat_workspec_details.mat_name_id','=','m_material_details.id')
		->join('m_vendor_material_details','m_vendor_mat_workspec_details.vendor_mat_id','=','m_vendor_material_details.id')
		->select('*','s_material_type.name as material_type_name','s_material_subtype.name as material_subtype_name','m_material_details.material_name as material_name')
		->get();
*/
		return view('vendors.material.listViewWorkMaterial',compact('vendor_material_details'));
	}

	public function listViewMaterialInstrument()
	{
		/*$vendor_instrument_details = DB::table('m_vendor_inst_workspec_details')

		->join('s_instrument_type','m_vendor_inst_workspec_details.inst_type_id','=','s_instrument_type.id')
		->join('s_instrument_subtype','m_vendor_inst_workspec_details.inst_subtype_id','=','s_instrument_subtype.id')
		->join('m_instrument_details','m_vendor_inst_workspec_details.inst_name_id','=','m_instrument_details.id')
		->join('m_vendor_instrument_details','m_vendor_inst_workspec_details.vendor_inst_id','=','m_vendor_instrument_details.id')
		->select('*','s_instrument_type.name as instrument_type_name','s_instrument_subtype.name as instrument_subtype_name')
		->get();
*/

		$vendor_instrument_details=DB::table('t_purchase_order_instrument')
		->join('m_instrument_details','m_instrument_details.id','=','t_purchase_order_instrument.instrument_name_id')
		->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','t_purchase_order_instrument.instrument_name_id')
		->join('m_vendor_inst_workspec_details','m_vendor_inst_workspec_details.inst_name_id','=','t_purchase_order_instrument.instrument_name_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_instrument.purchase_order_id')
		->select('*','m_vendor_instrument_details.name as vendor_name','m_vendor_instrument_details.id as vendorinst_id','m_vendor_inst_workspec_details.id as vendor_instworkspec_id')
		->get();


		return view('vendors.instrument.listViewVendorInstrument',compact('vendor_instrument_details'));
	}

	public function instrumentVendorTrackWork(Request $request)
	{
		$vendor_instrument_id = $request->get('vendor_instrument_id');

		$vendor_instrument_details=DB::table('t_purchase_order_instrument')
        ->where('instrument_name_id',$vendor_instrument_id)
		->join('m_instrument_details','m_instrument_details.id','=','t_purchase_order_instrument.instrument_name_id')
		->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','t_purchase_order_instrument.instrument_name_id')
		->join('m_vendor_inst_workspec_details','m_vendor_inst_workspec_details.inst_name_id','=','t_purchase_order_instrument.instrument_name_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_instrument.purchase_order_id')
		->select('*','m_vendor_instrument_details.name as vendor_name','m_vendor_instrument_details.id as vendorinst_id','m_vendor_inst_workspec_details.id as vendor_instworkspec_id')
		->get();

$vendor_instrument_detail = $vendor_instrument_details['0'];

		/*$vendor_instrument_id = $request->get('vendor_instrument_id');
        
		$vendor_instrument_detail = DB::table('m_vendor_inst_workspec_details')
		->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','m_vendor_inst_workspec_details.vendor_inst_id')
		->where('m_vendor_instrument_details.id',$vendor_instrument_id)
		->select('*','m_vendor_inst_workspec_details.id as m_vendor_inst_workspec_id')
		->get();

		$vendor_instrument_details= $vendor_instrument_detail['0'];

		$instrument_workspec_details = DB::table('m_vendor_inst_workspec_details')
		->where('m_vendor_inst_workspec_details.vendor_inst_id',$vendor_instrument_id)
		->join('s_vendor_types','m_vendor_inst_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('s_instrument_type','m_vendor_inst_workspec_details.inst_type_id','=','s_instrument_type.id')
		->join('s_instrument_subtype','m_vendor_inst_workspec_details.inst_subtype_id','=','s_instrument_subtype.id')
		->join('m_instrument_details','m_vendor_inst_workspec_details.inst_name_id','=','m_instrument_details.id')
		->select('*','s_instrument_type.name as instrument_type_name','s_instrument_subtype.name as instrument_subtype_name','m_vendor_inst_workspec_details.id as inst_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		$inst_workspec_detail = $instrument_workspec_details['0'];*/

		return view ('vendors.instrument.viewVendorInstrument',compact('vendor_instrument_detail','inst_workspec_detail'));
	}

	public function listViewInstrumentWork(Request $request)
	{
		/*$vendor_instrument_details = DB::table('m_vendor_inst_workspec_details')

		->join('s_instrument_type','m_vendor_inst_workspec_details.inst_type_id','=','s_instrument_type.id')
		->join('s_instrument_subtype','m_vendor_inst_workspec_details.inst_subtype_id','=','s_instrument_subtype.id')
		->join('m_instrument_details','m_vendor_inst_workspec_details.inst_name_id','=','m_instrument_details.id')
		->join('m_vendor_instrument_details','m_vendor_inst_workspec_details.vendor_inst_id','=','m_vendor_instrument_details.id')
		->select('*','s_instrument_type.name as instrument_type_name','s_instrument_subtype.name as instrument_subtype_name')
		->get();*/
		$vendor_instrument_id = $request->get('vendor_instrument_id');
         
		$vendor_instrument_details=DB::table('t_purchase_order_instrument')
		->join('m_instrument_details','m_instrument_details.id','=','t_purchase_order_instrument.instrument_name_id')
		->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','t_purchase_order_instrument.instrument_name_id')
		->join('m_vendor_inst_workspec_details','m_vendor_inst_workspec_details.inst_name_id','=','t_purchase_order_instrument.instrument_name_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_instrument.purchase_order_id')
		->select('*','m_vendor_instrument_details.name as vendor_name','m_vendor_instrument_details.id as vendorinst_id','m_vendor_inst_workspec_details.id as vendor_instworkspec_id')
		->get();


		return view ('vendors.instrument.listViewWorkInstrument',compact('vendor_instrument_details'));
	}

	public function listViewGadgetVendor()
	{
		/*$vendor_gadget_details=DB::table('m_vendor_gadget_workspec_details')
		->join('m_gadget_details','m_vendor_gadget_workspec_details.gadget_name_id','=','m_gadget_details.id')
		->join('m_vendor_gadget_details','m_vendor_gadget_workspec_details.vendor_gadget_id','=','m_vendor_gadget_details.id')
		->select('*')
		->get();*/
		$vendor_gadget_details=DB::table('t_purchase_order_gadget')
		->join('m_gadget_details','m_gadget_details.id','=','t_purchase_order_gadget.gadget_name_id')
		->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','t_purchase_order_gadget.gadget_name_id')
		->join('m_vendor_gadget_workspec_details','m_vendor_gadget_workspec_details.gadget_name_id','=','t_purchase_order_gadget.gadget_name_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_gadget.purchase_order_id')
		->select('*','m_vendor_gadget_details.name as vendor_name','m_vendor_gadget_details.id as vendorgadget_id','m_vendor_gadget_workspec_details.id as vendor_gadgetworkspec_id')
		->get();


		return view ('vendors.gadget.listViewVendorGadget',compact('vendor_gadget_details'));

	}

	public function gadgetVendorTrackWork(Request $request)
	{

        $vendor_gadget_id = $request->get('vendor_gadget_id');

		$vendor_gadget_details=DB::table('t_purchase_order_gadget')
        ->where('t_purchase_order_gadget.gadget_name_id',$vendor_gadget_id)
		->join('m_gadget_details','m_gadget_details.id','=','t_purchase_order_gadget.gadget_name_id')
		->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','t_purchase_order_gadget.gadget_name_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_gadget.purchase_order_id')
        ->join('m_vendor_gadget_workspec_details','m_vendor_gadget_workspec_details.gadget_name_id','=','t_purchase_order_gadget.gadget_name_id')
		->select('*','m_vendor_gadget_details.name as vendor_name','m_vendor_gadget_details.id as vendorgadget_id','m_vendor_gadget_workspec_details.id as vendor_gadgetworkspec_id')
		->get();

$vendor_gadget_detail = $vendor_gadget_details['0'];			/*$vendor_gadget_detail = DB::table('m_vendor_gadget_workspec_details')
		->join('s_vendor_types','m_vendor_gadget_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','m_vendor_gadget_workspec_details.vendor_gadget_id')
		->where('m_vendor_gadget_details.id',$vendor_gadget_id)
		->select('*','m_vendor_gadget_workspec_details.id as m_vendor_gadget_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		$vendor_gadget_details = $vendor_gadget_detail['0'];

		$gadget_workspec_details = DB::table('m_vendor_gadget_workspec_details')
		->where('m_vendor_gadget_workspec_details.vendor_gadget_id',$vendor_gadget_id)
		->join('s_vendor_types','m_vendor_gadget_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_gadget_details','m_vendor_gadget_workspec_details.gadget_name_id','=','m_gadget_details.id')
		->select('*','m_vendor_gadget_workspec_details.id as gadget_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();
*/
		
		return view('vendors.gadget.viewVendorGadget',compact('vendor_gadget_detail','gadget_workspec_detail'));
	}

	public function listViewGadgetWork()
	{
		/*$vendor_gadget_details=DB::table('m_vendor_gadget_workspec_details')
		->join('m_gadget_details','m_vendor_gadget_workspec_details.gadget_name_id','=','m_gadget_details.id')
		->join('m_vendor_gadget_details','m_vendor_gadget_workspec_details.vendor_gadget_id','=','m_vendor_gadget_details.id')
		->select('*')
		->get();*/
        $vendor_gadget_details=DB::table('t_purchase_order_gadget')																														
		->join('m_gadget_details','m_gadget_details.id','=','t_purchase_order_gadget.gadget_name_id')
		->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','t_purchase_order_gadget.gadget_name_id')
		->join('m_vendor_gadget_workspec_details','m_vendor_gadget_workspec_details.gadget_name_id','=','t_purchase_order_gadget.gadget_name_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_gadget.purchase_order_id')
		->select('*','m_vendor_gadget_details.name as vendor_name','m_vendor_gadget_details.id as vendorgadget_id','m_vendor_gadget_workspec_details.id as vendor_gadgetworkspec_id')
		->get();



		return view ('vendors.gadget.listViewWorkGadget',compact('vendor_gadget_details'));
	}


	public function listViewMachineVendor()
	{
		/*$vendor_machine_details=DB::table('m_vendor_machine_workspec_details')
		->join('m_machine_details','m_vendor_machine_workspec_details.machine_name_id','=','m_machine_details.id')
		->join('m_vendor_machine_details','m_vendor_machine_workspec_details.vendor_machine_id','=','m_vendor_machine_details.id')
		->select('*')
		->get();
*/
		$vendor_machine_details=DB::table('t_purchase_order_machine')
		->join('m_machine_details','m_machine_details.id','=','t_purchase_order_machine.machine_name_id')
		->join('m_vendor_machine_details','m_vendor_machine_details.id','=','t_purchase_order_machine.machine_name_id')
		->join('m_vendor_machine_workspec_details','m_vendor_machine_workspec_details.machine_name_id','=','t_purchase_order_machine.machine_name_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_machine.purchase_order_id')
		->select('*','m_vendor_machine_details.name as vendor_name','m_vendor_machine_details.id as vendormachine_id','m_vendor_machine_workspec_details.id as vendor_machineworkspec_id')
		->get();


		return view('vendors.machine.listViewVendorMachine',compact('vendor_machine_details'));
	}

	public function machineVendorTrackWork(Request $request)
	{

		$vendor_machine_id = $request->get('vendor_machine_id');
		
		$vendor_machine_detail = DB::table('m_vendor_machine_workspec_details')
		->join('s_vendor_types','m_vendor_machine_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_vendor_machine_details','m_vendor_machine_details.id','=','m_vendor_machine_workspec_details.vendor_machine_id')
		->where('m_vendor_machine_details.id',$vendor_machine_id)
		->select('*','m_vendor_machine_workspec_details.id as m_vendor_machine_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();
		
		$vendor_machine_details = $vendor_machine_detail['0'];

		$machine_workspec_details = DB::table('m_vendor_machine_workspec_details')
		->where('m_vendor_machine_workspec_details.vendor_machine_id',$vendor_machine_id)
		->join('s_vendor_types','m_vendor_machine_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_machine_details','m_vendor_machine_workspec_details.machine_name_id','=','m_machine_details.id')
		->select('*','m_vendor_machine_workspec_details.id as machine_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		$machine_workspec_detail = $machine_workspec_details['0'];

		return view('vendors.machine.viewVendorMachine',compact('vendor_machine_details','machine_workspec_detail'));
	}

	public function listViewMachineWork()
	{
		/*$vendor_machine_details=DB::table('m_vendor_machine_workspec_details')
		->join('m_machine_details','m_vendor_machine_workspec_details.machine_name_id','=','m_machine_details.id')
		->join('m_vendor_machine_details','m_vendor_machine_workspec_details.vendor_machine_id','=','m_vendor_machine_details.id')
		->select('*')
		->get();
*/
		$vendor_machine_details=DB::table('t_purchase_order_machine')
		->join('m_machine_details','m_machine_details.id','=','t_purchase_order_machine.machine_name_id')
		->join('m_vendor_machine_details','m_vendor_machine_details.id','=','t_purchase_order_machine.machine_name_id')
		->join('m_vendor_machine_workspec_details','m_vendor_machine_workspec_details.machine_name_id','=','t_purchase_order_machine.machine_name_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_machine.purchase_order_id')
		->select('*','m_vendor_machine_details.name as vendor_name','m_vendor_machine_details.id as vendormachine_id','m_vendor_machine_workspec_details.id as vendor_machineworkspec_id')
		->get();

		return view('vendors.machine.listViewWorkMachine',compact('vendor_machine_details'));
	}


	public function listViewMaintenanceVendor()
	{
		$vendor_maintenance_details=DB::table('m_vendor_maint_workspec_details')

		->join('m_vendor_maintenance_details','m_vendor_maint_workspec_details.vendor_maintenance_id','=','m_vendor_maintenance_details.id')
		->join('m_gadget_details','m_vendor_maint_workspec_details.gadget_name_id','=','m_gadget_details.id')
		->join('m_machine_details','m_vendor_maint_workspec_details.machine_name_id','=','m_machine_details.id')
		->select('*')
		->get();

		return view('vendors.maintenance.listViewVendorMaintenance',compact('vendor_maintenance_details'));
	}
	public function maintenanceVendorTrackWork(Request $request)
	{
		$vendor_maintenance_id = $request->get('vendor_maintenance_id');

		$vendor_maintenance_detail = DB::table('m_vendor_maint_workspec_details')
		->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','m_vendor_maint_workspec_details.vendor_maintenance_id')
		->where('m_vendor_maintenance_details.id',$vendor_maintenance_id)
		->select('*','m_vendor_maint_workspec_details.id as m_vendor_maintenance_workspec_id')
		->get();

		$vendor_maintenance_details = $vendor_maintenance_detail['0'];

		$maintenance_workspec_details = DB::table('m_vendor_maint_workspec_details')
		->where('m_vendor_maint_workspec_details.vendor_maintenance_id',$vendor_maintenance_id)
		->join('s_vendor_types','m_vendor_maint_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_gadget_details','m_vendor_maint_workspec_details.gadget_name_id','=','m_gadget_details.id')
		->join('m_machine_details','m_vendor_maint_workspec_details.machine_name_id','=','m_machine_details.id')
		->select('*','m_vendor_maint_workspec_details.id as maintenance_workspec_id')
		->get();

		$maintenance_workspec_detail = $maintenance_workspec_details['0'];

		return view('vendors.maintenance.viewVendorMaintenance',compact('vendor_maintenance_details','maintenance_workspec_detail'));
	}

	public function listViewMaintenanceWork()
	{
		$vendor_maintenance_details=DB::table('m_vendor_maint_workspec_details')

		->join('m_vendor_maintenance_details','m_vendor_maint_workspec_details.vendor_maintenance_id','=','m_vendor_maintenance_details.id')
		->join('m_gadget_details','m_vendor_maint_workspec_details.gadget_name_id','=','m_gadget_details.id')
		->join('m_machine_details','m_vendor_maint_workspec_details.machine_name_id','=','m_machine_details.id')
		->select('*')
		->get();

		return view('vendors.maintenance.listViewWorkMaintenance',compact('vendor_maintenance_details'));
	}

	public function viewByWorkLab(Request $request)

	{
		$vendor_lab_id = $request->get('vendor_lab_id');

		$lab_workspec_details = DB::table('m_vendor_lab_workspec_details')
		->where('m_vendor_lab_workspec_details.vendor_lab_id',$vendor_lab_id)
		->join('s_vendor_types','m_vendor_lab_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('s_lab_work_type','m_vendor_lab_workspec_details.work_type_id','=','s_lab_work_type.id')
		->join('s_lab_work_subtype','m_vendor_lab_workspec_details.work_subtype_id','=','s_lab_work_subtype.id')
		->join('s_lab_work_name','m_vendor_lab_workspec_details.work_name_id','=','s_lab_work_name.id')
		->join('m_vendor_lab_details','m_vendor_lab_details.id','=','m_vendor_lab_workspec_details.vendor_lab_id')
		->select('*','s_lab_work_type.name as lab_work_type_name','s_lab_work_subtype.name as lab_work_subtype_name','m_vendor_lab_workspec_details.id as lab_workspec_id','s_vendor_types.name as vendor_type_name','m_vendor_lab_details.name as vendor_lab_name','s_lab_work_name.name as lab_work_name ')
		->get();


		$lab_workspec_detail = $lab_workspec_details['0'];

		return view('vendors.lab.viewWorkLab',compact('lab_workspec_detail'));


	}
	public function viewByWorkMaterial(Request $request)

	{

		$vendor_material_id = $request->get('vendor_material_id');

		$material_workspec_details = DB::table('m_vendor_mat_workspec_details')
		->where('m_vendor_mat_workspec_details.vendor_mat_id',$vendor_material_id)
		->join('s_vendor_types','m_vendor_mat_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('s_material_type','m_vendor_mat_workspec_details.mat_type_id','=','s_material_type.id')
		->join('s_material_subtype','m_vendor_mat_workspec_details.mat_subtype_id','=','s_material_subtype.id')
		->join('m_material_details','m_vendor_mat_workspec_details.mat_name_id','=','m_material_details.id')
		->join('m_vendor_material_details','m_vendor_material_details.id','=','m_vendor_mat_workspec_details.vendor_mat_id')
		->select('*','s_material_type.name as material_type_name','s_material_subtype.name as material_subtype_name','m_vendor_mat_workspec_details.id as mat_workspec_id','s_vendor_types.name as vendor_type_name','m_material_details.material_name as work_material_name')
		->get();

		$mat_workspec_detail = $material_workspec_details['0'];

		return view('vendors.material.viewWorkMaterial',compact('mat_workspec_detail'));


	}
	public function viewByWorkInstrument(Request $request)
	{
		$vendor_instrument_id = $request->get('vendor_instrument_id');

		$instrument_workspec_details = DB::table('m_vendor_inst_workspec_details')
		->where('m_vendor_inst_workspec_details.vendor_inst_id',$vendor_instrument_id)
		->join('s_vendor_types','m_vendor_inst_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('s_instrument_type','m_vendor_inst_workspec_details.inst_type_id','=','s_instrument_type.id')
		->join('s_instrument_subtype','m_vendor_inst_workspec_details.inst_subtype_id','=','s_instrument_subtype.id')
		->join('m_instrument_details','m_vendor_inst_workspec_details.inst_name_id','=','m_instrument_details.id')
		->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','m_vendor_inst_workspec_details.vendor_inst_id')
		->select('*','s_instrument_type.name as instrument_type_name','s_instrument_subtype.name as instrument_subtype_name','m_vendor_inst_workspec_details.id as inst_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		$inst_workspec_detail = $instrument_workspec_details['0'];


		return view('vendors.instrument.viewWorkInstrument',compact('inst_workspec_detail'));

	}

	public function viewByWorkGadget(Request $request)
	{
		$vendor_gadget_id = $request->get('vendor_gadget_id');

		$gadget_workspec_details = DB::table('m_vendor_gadget_workspec_details')
		->where('m_vendor_gadget_workspec_details.vendor_gadget_id',$vendor_gadget_id)
		->join('s_vendor_types','m_vendor_gadget_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_gadget_details','m_vendor_gadget_workspec_details.gadget_name_id','=','m_gadget_details.id')
		->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','m_vendor_gadget_workspec_details.vendor_gadget_id')

		->select('*','m_vendor_gadget_workspec_details.id as gadget_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		$gadget_workspec_detail = $gadget_workspec_details['0'];
		
		return view('vendors.gadget.viewWorkGadget',compact('gadget_workspec_detail'));
	}

	public function viewByWorkMachine(Request $request)
	{
		
		$vendor_machine_id = $request->get('vendor_machine_id');

		$machine_workspec_details = DB::table('m_vendor_machine_workspec_details')
		->where('m_vendor_machine_workspec_details.vendor_machine_id',$vendor_machine_id)
		->join('s_vendor_types','m_vendor_machine_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_machine_details','m_vendor_machine_workspec_details.machine_name_id','=','m_machine_details.id')
		->join('m_vendor_machine_details','m_vendor_machine_details.id','=','m_vendor_machine_workspec_details.vendor_machine_id')
		->select('*','m_vendor_machine_workspec_details.id as machine_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		$machine_workspec_detail = $machine_workspec_details['0'];

		return view('vendors.machine.viewWorkMachine',compact('machine_workspec_detail'));

	}

	public function viewByWorkMaintenance(Request $request)
	{
		$vendor_maintenance_id = $request->get('vendor_maintenance_id');

		$maintenance_workspec_details = DB::table('m_vendor_maint_workspec_details')
		->where('m_vendor_maint_workspec_details.vendor_maintenance_id',$vendor_maintenance_id)
		->join('s_vendor_types','m_vendor_maint_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_gadget_details','m_vendor_maint_workspec_details.gadget_name_id','=','m_gadget_details.id')
		->join('m_machine_details','m_vendor_maint_workspec_details.machine_name_id','=','m_machine_details.id')
		->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','m_vendor_maint_workspec_details.vendor_maintenance_id')
		->select('*','m_vendor_maint_workspec_details.id as maintenance_workspec_id')
		->get();

		$maintenance_workspec_detail = $maintenance_workspec_details['0'];

		return view('vendors.maintenance.viewWorkMaintenance',compact('maintenance_workspec_detail'));
	}

}
