<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use DB;
use App\LabAssignwork;
use App\LabsDelivery;
use App\LabBilling;
use App\LabAssignworkInstrument;

class LabsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		$vendor_details= DB::table('m_vendor_lab_workspec_details')
		->join('s_lab_work_type','m_vendor_lab_workspec_details.work_type_id','=','s_lab_work_type.id')
		->join('s_lab_work_subtype','m_vendor_lab_workspec_details.work_subtype_id','=','s_lab_work_subtype.id')
		->join('s_lab_work_name','m_vendor_lab_workspec_details.work_name_id','=','s_lab_work_name.id')
		->join('m_vendor_lab_details','m_vendor_lab_workspec_details.vendor_lab_id','=','m_vendor_lab_details.id')
		->select('*','s_lab_work_type.name as lab_work_type_name','s_lab_work_subtype.name as lab_work_subtype_name','s_lab_work_name.name as lab_work_name','m_vendor_lab_workspec_details.id as lab_workspec_id')
		->get();


		return view('labs.viewlabdetails.dashboardlabdetails',compact('vendor_details'));
	}

	public function trackWork()
	{
		$job_details=DB::table('t_assign_lab_work')
		->orderBy('t_assign_lab_work.id', 'asc')
		->join('m_vendor_lab_details','m_vendor_lab_details.id','=','t_assign_lab_work.lab_work_name_id')
		->join('dnf_lab_work_status','dnf_lab_work_status.id','=','t_assign_lab_work.lab_work_status_id')
		->join('t_assign_lab_instru','t_assign_lab_instru.assign_lab_work_id','=','t_assign_lab_work.id')
		->join('m_instrument_details','m_instrument_details.id','=','t_assign_lab_instru.instrument_name_id')
		->select('*','m_instrument_details.instrument_name','t_assign_lab_work.id as id','dnf_lab_work_status.status as labstatus','m_vendor_lab_details.name as labname')
		->get();
		
		return view('labs.trackwork.dashboardtrackwork',compact('job_details'));
	}

	public function getDetailsWork($id)
	{
		$lab_details=DB::table('m_vendor_lab_details')
		->get();

		$instrument_list=DB::table('m_instrument_details')
		->get();
		$instrumentNames=DB::table('t_assign_lab_instru')
						->where('assign_lab_work_id',$id)
						->join('m_instrument_details','m_instrument_details.id','=','t_assign_lab_instru.instrument_name_id')
						->get();
					
		$details=DB::table('t_assign_lab_work')
		->where('t_assign_lab_work.id',$id)
		->join('m_vendor_lab_details','m_vendor_lab_details.id','=','t_assign_lab_work.lab_work_name_id')
		->join('dnf_lab_work_status','dnf_lab_work_status.id','=','t_assign_lab_work.lab_work_status_id')
		->join('m_staff_registration','m_staff_registration.id','=','t_assign_lab_work.staff_id')
		->select('*','m_vendor_lab_details.name as labname','dnf_lab_work_status.status as labstatus','m_staff_registration.name as staff name')
		->get();


		return view('labs.trackwork.viewassignwork',compact('details','lab_details','instrument_list','instrumentNames','id'));
	}

	public function getDetailsLabForm($id)
	{
		return view('labs.trackwork.viewlabform',compact('id'));
	}

	public function getDetailsDelivery($id)
	{

		$labDeliveryDetails=DB::table('t_lab_delivery')
		->where('t_assign_lab_work_id',$id)
		->get();
		/*dd($labDeliveryDetails);*/

		$challan_no=DB::table('t_lab_delivery')
					->where('t_assign_lab_work_id',$id)
					->select('chalan_number')
					->get();


		return view('labs.trackWork.viewdelivery',compact('id','labDeliveryDetails','challan_no'));
	}

	public function getDetailsBilling($id)
	{
		$bill_status=DB::table('dnf_billing_status')
		->get();

		$billing_details=DB::table('t_lab_billing')
		->where('assign_lab_work_id',$id)
		->join('dnf_billing_status','dnf_billing_status.id','=','t_lab_billing.billing_status_id')
		->get();

		return view('labs.trackwork.viewbilling',compact('id','bill_status','billing_details'));
	}

	public function generateRework($id)
	{
		$details=DB::table('t_assign_lab_work')
		->where('t_assign_lab_work.id',$id)
		->join('m_vendor_lab_details','m_vendor_lab_details.id','=','t_assign_lab_work.lab_work_name_id')
		->join('dnf_lab_work_status','dnf_lab_work_status.id','=','t_assign_lab_work.lab_work_status_id')
		->join('m_staff_registration','m_staff_registration.id','=','t_assign_lab_work.staff_id')
		->select('*','m_vendor_lab_details.name as labname','dnf_lab_work_status.status as labstatus','m_staff_registration.name as staff name')
		->get();
	
		$lab_details=DB::table('m_vendor_lab_details')
		->get();

		$instrument_list=DB::table('m_instrument_details')
		->get();
		
		$instrumentnames=DB::table('t_assign_lab_instru')
						->where('assign_lab_work_id',$id)
						->join('m_instrument_details','m_instrument_details.id','=','t_assign_lab_instru.instrument_name_id')
						->get();

		$statusDetails=DB::table('dnf_lab_work_status')
		->get();

		$staffDetails=DB::table('m_staff_registration')
		->get();

		return view('labs.trackwork.generaterework',compact('details','lab_details','instrument_list','statusDetails','staffDetails','id','instrumentnames'));
	}

	/*public function addGenerateRework(Request $request)
	{
		$id=$request->get('id');

		$lab_id=$request->get('lab_id');
		$work_allocation_date=$request->get('work_allocation_date');
		$contact_details=$request->get('contact_details');
		$deadline=$request->get('deadline');
		$address=$request->get('address');
		$inst_name_id=$request->get('instrument_name_id');

		$status=$request->get('status');
		$lab_person_responsible=$request->get('lab_person_responsible');
		$assistant=$request->get('assistant');
		$responsible_dentist=$request->get('responsible_dentist');
	}*/

	public function editGenerateRework(Request $request)
	{

		$id=$request->get('id');

		$lab_id=$request->get('lab_id');
		$work_allocation_date=$request->get('work_allocation_date');
		$contact_details=$request->get('contact_details');
		$deadline=$request->get('deadline');
		$address=$request->get('address');
		$inst_name_id=$request->get('instrument_name_id');

		$status=$request->get('status');
		$lab_person_responsible=$request->get('lab_person_responsible');
		$assistant=$request->get('assistant');
		$responsible_dentist=$request->get('responsible_dentist');

		DB::table('t_assign_lab_work')
		->where('id',$id)
		->update(['lab_work_name_id'=>$lab_id,
			'work_allocation_date'=>$work_allocation_date,
			'deadline'=>$deadline,
			'instrument_name_id'=>$inst_name_id,
			'lab_work_status_id'=>$status,
			'staff_id'=>$assistant,
			'lab_person_responsible'=>$lab_person_responsible]);

		/*for instrument list*/
		$instrument_id=$request->get('instrument_id');
		if($instrument_id == null)
		{
			return redirect()->action('LabsController@generateRework',[$id]);
		}
		else
		{
			foreach($instrument_id as $instrument)
			{
			
				$instrumentlist = new LabAssignworkInstrument;
				$instrumentlist->assign_lab_work_id=$id;
				$instrumentlist->instrument_name_id=$instrument;

				$instrumentlist->save();
			}
		}
		
			
		return redirect()->action('LabsController@generateRework',[$id]);
	}

	public function generateReworkLabForm($id)
	{
		return view('labs.trackwork.generateReworkLabForm',compact('id'));
	}

	public function generateReworkDelivery($id)
	{
		$labDeliveryDetails=DB::table('t_lab_delivery')
				->where('t_assign_lab_work_id',$id)
				->select('*','id as challan_id')
				->get();
				
		$challanDetails=DB::table('t_lab_delivery')
				->where('t_assign_lab_work_id',$id)
				->select('chalan_number','t_assign_lab_work_id')
				->get();
				
		return view('labs.trackwork.generatereworkdelivery',compact('id','labDeliveryDetails','challanDetails'));
	}

	public function updateGenerateReworkDelivery(Request $request)
	{
		$id=$request->get('lab_work_id');

		$challan_no=$request->get('challan_no');
		$dateofdelivery=$request->get('date_of_delivery');
		$delivery_location=$request->get('delivery_location');

		foreach($challan_no as $key=>$challan)
		{
			$labdelivery=new LabsDelivery;
			$labdelivery->t_assign_lab_work_id=$id;
			$labdelivery->chalan_number=$challan;
			$labdelivery->delivery_date=$dateofdelivery[$key];
			$labdelivery->delivery_location=$delivery_location[$key];

			$labdelivery->save();
		}
			
		return redirect()->action('LabsController@generateReworkDelivery',[$id]);
	}

	public function generateReworkBilling($id)
	{
		$bill_status=DB::table('dnf_billing_status')
		->get();

		$billing_details=DB::table('t_lab_billing')
		->where('assign_lab_work_id',$id)
		->join('dnf_billing_status','dnf_billing_status.id','=','t_lab_billing.billing_status_id')
		->get();

		return view('labs.trackwork.generatereworkbilling',compact('id','bill_status','billing_details'));
	}

	public function addGenerateReworkBilling(Request $request)
	{
		$id=$request->get('lab_work_id');

		$bill_number=$request->get('bill_number');
		$date_of_billing=$request->get('date_of_billing');
		$billing_status=$request->get('billing_status');
		$final_cost=$request->get('final_cost');

		$labbilling=new LabBilling;

		$labbilling->assign_lab_work_id=$id;

		$labbilling->bill_number=$bill_number;
		$labbilling->bill_date=$date_of_billing;
		$labbilling->billing_status_id=$billing_status;
		$labbilling->final_cost=$final_cost;
		$labbilling->save();

		return redirect()->action('LabsController@generateReworkBilling',[$id]);		
	}

	public function updateGenerateReworkBilling(Request $request)
	{
		$id=$request->get('lab_work_id');

		$bill_number=$request->get('bill_number');
		$date_of_billing=$request->get('date_of_billing');
		$billing_status=$request->get('billing_status');
		$final_cost=$request->get('final_cost');

		$billing_details=DB::table('t_lab_billing')
		->where('assign_lab_work_id',$id)
		->get();

		
		DB::table('t_lab_billing')
			->where('assign_lab_work_id',$id)
			->update(['bill_number'=>$bill_number,
				'bill_date'=>$date_of_billing,
				'billing_status_id'=>$billing_status,
				'final_cost'=>$final_cost
				]);
		
		return redirect()->action('LabsController@generateReworkBilling',[$id]);		
	}

	public function assignlabinfo()
	{
		$lab_work_id=DB::table('t_assign_lab_work')
		->orderBy('id', 'desc')
		->first();

		if($lab_work_id == null)
		{
			$lab_work_id = (object)array('id' => 0);
		}

		$lab_work_id = $lab_work_id->id + 1;

		$labdetails=DB::table('t_assign_lab_work')
					->get();
					

		return view('labs.assignwork.dashboardassignwork',compact('lab_work_id','labdetails'));
	}

	public function viewdetails()
	{
		return view('labs.assignwork.viewdetails');
	}

	public function assignwork($lab_work_id)
	{
		$lab_details=DB::table('m_vendor_lab_details')
		->get();

		$instrument_list=DB::table('m_instrument_details')
		->get();


		$statusDetails=DB::table('dnf_lab_work_status')
		->get();

		$staffDetails=DB::table('m_staff_registration')
		->get();

		$details=DB::table('t_assign_lab_work')
		->where('t_assign_lab_work.id',$lab_work_id)
		->join('m_vendor_lab_details','m_vendor_lab_details.id','=','t_assign_lab_work.lab_work_name_id')
		->join('dnf_lab_work_status','dnf_lab_work_status.id','=','t_assign_lab_work.lab_work_status_id')
		->join('m_staff_registration','m_staff_registration.id','=','t_assign_lab_work.staff_id')
		->select('*','m_vendor_lab_details.name as labname','dnf_lab_work_status.status as labstatus','m_staff_registration.name as staff name')
		->get();

		$instrument_details=DB::table('t_assign_lab_instru')
		->where('t_assign_lab_instru.assign_lab_work_id',$lab_work_id)
		->join('m_instrument_details','m_instrument_details.id','=','t_assign_lab_instru.instrument_name_id')
		->select('*','t_assign_lab_instru.id as id')
		->get();

		return view('labs.assignwork.AssignWork',compact('lab_details','instrument_list','statusDetails','staffDetails','lab_work_id','details','instrument_details'));
	}

	public function storeAssignWorkDetails(Request $request)
	{
		$lab_work_id=$request->get('lab_work_id');

		$lab_id=$request->get('lab_id');
		$work_allocation_date=$request->get('work_allocation_date');
		$contact_details=$request->get('contact_details');
		$deadline=$request->get('deadline');
		$address=$request->get('address');

		$instrument_name_id=$request->get('instrument_id');
		$id=$request->get('id');
		/*dd($id);*/

		$status=$request->get('status');
		$lab_person_responsible=$request->get('lab_person_responsible');
		$assistant=$request->get('assistant');
		$responsible_dentist=$request->get('responsible_dentist');

			/*create object for LabAssignwork and add data*/
			$assignwork = new LabAssignwork;
			$assignwork->lab_work_name_id=$lab_id;
			$assignwork->work_allocation_date=$work_allocation_date;
			$assignwork->deadline=$deadline;

			$assignwork->lab_work_status_id=$status;
			$assignwork->staff_id=$assistant;
			$assignwork->lab_person_responsible=$lab_person_responsible;

			$assignwork->save();
		
		foreach($instrument_name_id as $instrumentId)
		{
			
			$instrumentlist = new LabAssignworkInstrument;
			$instrumentlist->instrument_name_id=$instrumentId;
			$instrumentlist->assign_lab_work_id=$lab_work_id;

			$instrumentlist->save();	
		}

		return redirect()->action('LabsController@assignwork',[$lab_work_id]);

	}

	public function updateAssignWorkDetails(Request $request)
	{
		$lab_work_id=$request->get('lab_work_id');

		$lab_id=$request->get('lab_id');
		$work_allocation_date=$request->get('work_allocation_date');
		$contact_details=$request->get('contact_details');
		$deadline=$request->get('deadline');
		$address=$request->get('address');

		$instrument_name_id=$request->get('instrument_id');
		$id=$request->get('id');
		/*dd($id);*/

		$status=$request->get('status');
		$lab_person_responsible=$request->get('lab_person_responsible');
		$assistant=$request->get('assistant');
		$responsible_dentist=$request->get('responsible_dentist');

		DB::table('t_assign_lab_work')
			->where('id',$lab_work_id)
			->update(['lab_work_name_id'=>$lab_id,
				'work_allocation_date'=>$work_allocation_date,
				'deadline'=>$deadline,
				'lab_work_status_id'=>$status,
				'staff_id'=>$assistant,
				'lab_person_responsible'=>$lab_person_responsible]);

		foreach($instrument_name_id as $instrumentId)
		{
			
			$instrumentlist = new LabAssignworkInstrument;
			$instrumentlist->instrument_name_id=$instrumentId;
			$instrumentlist->assign_lab_work_id=$lab_work_id;

			$instrumentlist->save();	
		}

		return redirect()->action('LabsController@assignwork',[$lab_work_id]);
	}

	public function deleteinstrumentList($assign_lab_work_id,$instrument_name_id)
	{
		$lab_work_id=$assign_lab_work_id;
		DB::table('t_assign_lab_instru')
		->where('instrument_name_id',$instrument_name_id)
		->where('assign_lab_work_id',$assign_lab_work_id)
		->delete();

		return redirect()->action('LabsController@assignwork',[$lab_work_id]);

	}

	public function assignworklabform($lab_work_id)
	{
		return view('labs.assignwork.LabForm',compact('lab_work_id'));
	}

	public function assignworkdelivery($lab_work_id)
	{
		$labDeliveryDetails=DB::table('t_lab_delivery')
		->where('t_assign_lab_work_id',$lab_work_id)
		->get();
		
		$challan_no=DB::table('t_lab_delivery')
		->where('t_assign_lab_work_id',$lab_work_id)
		->select('chalan_number','id')
		->get();
		
		return view('labs.assignwork.dashboardDelivery',compact('lab_work_id','labDeliveryDetails','challan_no'));
	}	

	public function storeDelivery(Request $request)
	{
		
		$lab_work_id=$request->get('lab_work_id');
		$challan_no=$request->get('challan_no');
		$dateofdelivery=$request->get('date_of_delivery');
		$delivery_location=$request->get('delivery_location');
		
		foreach($challan_no as $key=>$challan)
		{
			$delivery=new LabsDelivery;
			$delivery->t_assign_lab_work_id=$lab_work_id;
			$delivery->chalan_number=$challan;
			$delivery->delivery_date=$dateofdelivery[$key];
			$delivery->delivery_location=$delivery_location[$key];

			$delivery->save();
		}

		return redirect()->action('LabsController@assignworkdelivery',[$lab_work_id]);
	}

	public function deleteDeliveryDetails($id,$t_assign_lab_work_id)
	{
		
		$lab_work_id=$t_assign_lab_work_id;

		DB::table('t_lab_delivery')
		->where('id',$id)
		->where('t_assign_lab_work_id',$t_assign_lab_work_id)
		->delete();

		return redirect()->action('LabsController@assignworkdelivery',[$lab_work_id]);

	}

	public function DeleteChallanNo($id)
	{
		$labdetails=DB::table('t_lab_delivery')
		->where('id',$id)
		->get();
		$lab_work_id=$labdetails['0']->t_assign_lab_work_id;
		
		DB::table('t_lab_delivery')
		->where('id',$id)
		->delete();

		return redirect()->action('LabsController@assignworkdelivery',[$lab_work_id]);
	}

	public function deleteTrackworkinstrument($assign_lab_work_id,$instrument_name_id)
	{
		$id=$assign_lab_work_id;

		DB::table('t_assign_lab_instru')
		->where('assign_lab_work_id',$assign_lab_work_id)
		->where('instrument_name_id',$instrument_name_id)
		->delete();

		return redirect()->action('LabsController@generateRework',[$id]);
	}

	public function deleteTrachworkchallannumber($challan_id,$t_assign_lab_work_id)
	{
		
		$id=$t_assign_lab_work_id;
		DB::table('t_lab_delivery')
		->where('id',$challan_id)
		->where('t_assign_lab_work_id',$t_assign_lab_work_id)
		->delete();

		return redirect()->action('LabsController@generateReworkDelivery',[$id]);

	}

	public function assignworkbilling($lab_work_id)
	{
		$bill_status=DB::table('dnf_billing_status')
		->get();

		$billing_details=DB::table('t_lab_billing')
		->where('assign_lab_work_id',$lab_work_id)
		->join('dnf_billing_status','dnf_billing_status.id','=','t_lab_billing.billing_status_id')
		->get();

		/*dd($billing_details);*/

		return view('labs.assignwork.dashboardBilling',compact('lab_work_id','bill_status','billing_details'));
	}

	public function storeBillinDetails(Request $request)
	{
		$lab_work_id=$request->get('lab_work_id');

		$bill_number=$request->get('bill_number');
		$date_of_billing=$request->get('date_of_billing');
		$billing_status=$request->get('billing_status');
		$final_cost=$request->get('final_cost');

			$labbilling=new LabBilling;

			$labbilling->assign_lab_work_id=$lab_work_id;

			$labbilling->bill_number=$bill_number;
			$labbilling->bill_date=$date_of_billing;
			$labbilling->billing_status_id=$billing_status;
			$labbilling->final_cost=$final_cost;
			$labbilling->save();

		return redirect()->action('LabsController@assignworkbilling',[$lab_work_id]);

	}

	public function updateBillinDetails(Request $request)
	{
		$lab_work_id=$request->get('lab_work_id');

		$bill_number=$request->get('bill_number');
		$date_of_billing=$request->get('date_of_billing');
		$billing_status=$request->get('billing_status');
		$final_cost=$request->get('final_cost');

		DB::table('t_lab_billing')
			->where('assign_lab_work_id',$lab_work_id)
			->update(['bill_number'=>$bill_number,
				'bill_date'=>$date_of_billing,
				'billing_status_id'=>$billing_status,
				'final_cost'=>$final_cost
				]);

		return redirect()->action('LabsController@assignworkbilling',[$lab_work_id]);
	}


//***********Ajax function

	public function getLabDetails($labName)
	{
		$lab_details=DB::table('m_vendor_lab_details')
		->where('id',$labName)
		->get();

		return $lab_details;
	}

}
