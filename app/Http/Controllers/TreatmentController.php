<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Alert;

use Illuminate\Http\Request;

use App\Appointment;
use App\Patient;
use App\PatientTreatment;
use App\TreatmentGeneral;
use App\TreatmentConsultant;
use App\TreatmentQuotation;
use App\TreatmentFindingsNotes;
use App\TreatmentVisitingClinic;
use App\Sittings;
use App\SittingsDiagnosis;
use App\SittingsGeneral;
use App\SittingsLabWork;

class TreatmentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$patient = "";

		return view('treatment.listViewTreatment',compact('patient'));
	}

	public function searchPatientBasicDetails(Request $request)
	{
		$name = $request->get('name');
		$cell_no = $request->get('cell_no');
		$patient = "patient";

		//checking entered patient name and cell no exist or not
		$result = Patient::where('name', '=', $name)->where('cell_no', '=', $cell_no)->get();

		if (!count($result))
		{
			$patient = "";

			return view('treatment.listViewSearchPatientTreatment',compact('patient','name','cell_no'));
		}

		else
		{
			$patient = DB::table('m_patient_details')
			->where('cell_no', $cell_no)
			->where('name', $name)
			->get();

			return view('treatment.listViewSearchPatientTreatment',compact('patient','name','cell_no'));
		}
		
	}

	public function getPatientDetails($patientId, Request $request)
	{
	//patient-details
		$patient = DB::table('m_patient_details')
		->where('id', $patientId)
		->get();

		$patientHistory = DB::table('m_medical_history')
		->where('patient_id', $patientId)
		->get();

		if($patientHistory == null)
		{
			$patientHistory[0] = (object)array('medical_history'=>'');
		}

	//treatment-details
		$patientTreatmentDetails = DB::table('t_patient_treatment')
		->where('t_patient_treatment.patient_id', $patientId)
		->leftJoin('t_treatment_general','t_treatment_general.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_consultant','t_treatment_consultant.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_quotation','t_treatment_quotation.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_visit_clinic','t_treatment_visit_clinic.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_findings','t_treatment_findings.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('dnf_treatment_status','dnf_treatment_status.id','=','t_treatment_general.status_id')
		->leftJoin('s_dentist_details','s_dentist_details.id','=','t_treatment_general.dentist_name_id')		
		->select('*','t_treatment_general.clinic_id as general_clinic_id','t_treatment_visit_clinic.clinic_id as visit_clinic_id','t_patient_treatment.id as patient_treatment_id')
		->get();
		
		return view('treatment.getDetailsPatient',compact('patient','patientTreatmentDetails','patientHistory'));
	}

	public function storePatientTreament($patientId)
	{
		$patienttreatment=new PatientTreatment;
		$patienttreatment->patient_id=$patientId;
		$patienttreatment->save();

	//to select and assign latest patient treatment ID
		$patientTreatmentId = DB::table('t_patient_treatment')
		->orderBy('id', 'desc')
		->first();

		$patientTreatmentId = $patientTreatmentId->id;

		return redirect()->action('TreatmentController@createGeneralTreamentNotes',[$patientTreatmentId]);
	}

	public function createGeneralTreamentNotes($patientTreatmentId)
	{
	//to get treatment details for dropdown
		$treatmentType = DB::table('s_treatment_type')
		->get();

	//to get dentist details for dropdown
		$dentistName = DB::table('s_dentist_details')
		->get();

	//to get treatment status for dropdown
		$status = DB::table('dnf_treatment_status')
		->get();

	//to get clinic details for dropdown
		$clinics = DB::table('m_clinic')
		->get();

	//to get patient id
		$patientTreatment = DB::table('t_patient_treatment')
			->where('id', $patientTreatmentId)
			->get();

		if($patientTreatment != null)
		{
			$patientId = $patientTreatment[0]->patient_id;
		}

	//treatment general details
		$treatmentGeneralDetails = DB::table('t_treatment_general')
		->where('t_treatment_general.patient_treatment_id',$patientTreatmentId)
		->join('m_treatment_name','m_treatment_name.id','=','t_treatment_general.treatment_name_id')
		->join('s_treatment_type','s_treatment_type.id','=','m_treatment_name.treatment_type_id')
		->join('s_dentist_details','s_dentist_details.id','=','t_treatment_general.dentist_name_id')
		->join('dnf_treatment_status','dnf_treatment_status.id','=','t_treatment_general.status_id')
		->join('m_clinic','m_clinic.id','=','t_treatment_general.clinic_id')
		->select('*','m_treatment_name.name as treatment_name','s_treatment_type.name as treatment_type','t_treatment_general.id as treatment_general_id')
		->get();

	//condition to display add or edit page
		if($treatmentGeneralDetails == null)
		{
			return view('treatment.treatmentnotes.general.addGeneralTreatmentNotes',compact('treatmentType','dentistName','status','patientTreatmentId','clinics','treatmentGeneralDetails','patientId'));			
		}
		else
		{
			return view('treatment.treatmentnotes.general.editGeneralTreatmentNotes',compact('treatmentType','dentistName','status','patientTreatmentId','clinics','treatmentGeneralDetails','patientId'));
		}		
	}

	public function storeGeneralTreamentNotes(Request $request)
	{
		$patientTreatmentId = $request->get('patient_treatment_id');

		//to add details
		$treatmentgeneral=new TreatmentGeneral;
		$treatmentgeneral->patient_treatment_id=$patientTreatmentId;
		$treatmentgeneral->treatment_name_id=$request->get('treatment_name_id');
		$treatmentgeneral->dentist_name_id=$request->get('dentist_name_id');
		$treatmentgeneral->status_id=$request->get('status_id');
		$treatmentgeneral->clinic_id=$request->get('clinic_id');
		$treatmentgeneral->save();

		return redirect()->action('TreatmentController@createGeneralTreamentNotes',[$patientTreatmentId]);
	}

	public function updateGeneralTreamentNotes(Request $request)
	{
		$patientTreatmentId = $request->get('patient_treatment_id');

		$treatmentGeneralId = $request->get('treatment_general_id');

	//Logic to add or update details
		$treatmentGeneralDetails = DB::table('t_treatment_general')
		->where('id',$treatmentGeneralId)
		->get();
		
		//to update details
		DB::table('t_treatment_general')
		->where('id', $treatmentGeneralId)
		->update(['patient_treatment_id' => $patientTreatmentId,
			'treatment_name_id' => $request->get('treatment_name_id'),
			'dentist_name_id' => $request->get('dentist_name_id'),
			'status_id' => $request->get('status_id'),
			'clinic_id' => $request->get('clinic_id')]);

		return redirect()->action('TreatmentController@createGeneralTreamentNotes',[$patientTreatmentId]);
	}

	public function viewGeneralTreamentNotes($patientTreatmentId)
	{
	//treatment general details
		$treatmentGeneralDetails = DB::table('t_treatment_general')
		->where('t_treatment_general.patient_treatment_id',$patientTreatmentId)
		->join('m_treatment_name','m_treatment_name.id','=','t_treatment_general.treatment_name_id')
		->join('s_treatment_type','s_treatment_type.id','=','m_treatment_name.treatment_type_id')
		->join('s_dentist_details','s_dentist_details.id','=','t_treatment_general.dentist_name_id')
		->join('dnf_treatment_status','dnf_treatment_status.id','=','t_treatment_general.status_id')
		->join('m_clinic','m_clinic.id','=','t_treatment_general.clinic_id')
		->select('*','m_treatment_name.name as treatment_name','s_treatment_type.name as treatment_type','t_treatment_general.id as treatment_general_id')
		->get();

	//if patient treatment Id is not null
		if($treatmentGeneralDetails[0]->patient_treatment_id != null)
		{
			$patientTreatment = DB::table('t_patient_treatment')
			->where('id', $patientTreatmentId)
			->get();

			$patientId = $patientTreatment[0]->patient_id;
		}

		return view('treatment.treatmentnotes.general.viewGeneralTreatmentNotes',compact('treatmentType','dentistName','status','patientTreatmentId','clinics','treatmentGeneralDetails','patientId'));
	}

	public function createShowstopperTreamentNotes($patientTreatmentId)
	{
	//to get patient id
		$patientTreatment = DB::table('t_patient_treatment')
			->where('id', $patientTreatmentId)
			->get();

		if($patientTreatment != null)
		{
			$patientId = $patientTreatment[0]->patient_id;
		}

		return view('treatment.treatmentnotes.showstopper.addShowstopperTreatmentNotes',compact('patientTreatmentId','patientId'));
	}

	public function storeShowstopperTreamentNotes(Request $request)
	{
		$patientTreatmentId = $request->get('patient_treatment_id');

		return redirect()->action('TreatmentController@createShowstopperTreamentNotes',[$patientTreatmentId]);
	}

	public function viewShowstopperTreamentNotes($patientTreatmentId)
	{
	//to get patient treatment Id
		$patientTreatment = DB::table('t_patient_treatment')
		->where('id', $patientTreatmentId)
		->get();

		if($patientTreatment != null)
		{
			$patientId = $patientTreatment[0]->patient_id;
		}

		return view('treatment.treatmentnotes.showstopper.viewShowstopperTreamentNotes',compact('patientTreatmentId','patientId'));
	}

	public function createConsultantTreamentNotes($patientTreatmentId)
	{
	//to get patient id
		$patientTreatment = DB::table('t_patient_treatment')
			->where('id', $patientTreatmentId)
			->get();

		if($patientTreatment != null)
		{
			$patientId = $patientTreatment[0]->patient_id;
		}

	//to get treatment consultant
		$treatmentConsultDetails = DB::table('t_treatment_consultant')
		->where('patient_treatment_id',$patientTreatmentId)
		->get();

		if($treatmentConsultDetails == null)
		{
			return view('treatment.treatmentnotes.consultant.addConsultantTreatmentNotes',compact('patientTreatmentId','treatmentConsultDetails','patientId'));
		}
		else
		{
			return view('treatment.treatmentnotes.consultant.editConsultantTreatmentNotes',compact('patientTreatmentId','treatmentConsultDetails','patientId'));
		}
		
	}

	public function storeConsultantTreamentNotes(Request $request)
	{
		$patientTreatmentId = $request->get('patient_treatment_id');

		$consultantName = $request->get('consultant_name');

		if($consultantName != null)
		{
			//to add new details
			$treatmentconsultant = new TreatmentConsultant;
			$treatmentconsultant->consultant_name=$request->get('consultant_name');
			$treatmentconsultant->clinic=$request->get('clinic');
			$treatmentconsultant->location=$request->get('location');
			$treatmentconsultant->fees=$request->get('fees');
			$treatmentconsultant->notes=$request->get('notes');
			$treatmentconsultant->patient_treatment_id=$patientTreatmentId;
			$treatmentconsultant->save();
		}

		return redirect()->action('TreatmentController@createConsultantTreamentNotes',[$patientTreatmentId]);
	}

	public function updateConsultantTreamentNotes(Request $request)
	{
		$patientTreatmentId = $request->get('patient_treatment_id');

		$treatmentConsultId = $request->get('treatment_consult_id');

		//to update details
		DB::table('t_treatment_consultant')
		->where('id', $treatmentConsultId)
		->update([
			'consultant_name' => $request->get('consultant_name'),
			'clinic' => $request->get('clinic'),
			'location' => $request->get('location'),
			'fees' => $request->get('fees'),
			'notes' => $request->get('notes'),
			'patient_treatment_id' => $patientTreatmentId]);

		return redirect()->action('TreatmentController@createConsultantTreamentNotes',[$patientTreatmentId]);
	}

	public function viewConsultantTreamentNotes($patientTreatmentId)
	{
	//to display treatment consultant details
		$treatmentConsultDetails = DB::table('t_treatment_consultant')
		->where('patient_treatment_id',$patientTreatmentId)
		->join('t_patient_treatment','t_patient_treatment.id','=','t_treatment_consultant.id')
		->select('*')
		->get();

	//to get patient treatment Id
		$patientTreatment = DB::table('t_patient_treatment')
		->where('id', $patientTreatmentId)
		->get();

		if($patientTreatment != null)
		{
			$patientId = $patientTreatment[0]->patient_id;
		}

		return view('treatment.treatmentnotes.consultant.viewConsultantTreamentNotes',compact('patientTreatmentId','treatmentConsultDetails','patientId'));
	}

	public function createQuatationsTreamentNotes($patientTreatmentId)
	{
	//to get patient id
		$patientTreatment = DB::table('t_patient_treatment')
			->where('id', $patientTreatmentId)
			->get();

		if($patientTreatment != null)
		{
			$patientId = $patientTreatment[0]->patient_id;
		}

	//to get treatment quotation details
		$treatmentQuotDetails = DB::table('t_treatment_quotation')
		->where('patient_treatment_id',$patientTreatmentId)
		->get();

	//to get patient Id
		$patient_id = DB::table('t_patient_treatment')
		->where('id',$patientTreatmentId)
		->get();

		$patient_id = $patient_id[0]->patient_id;

	//to fetch referral details for above patient Id
		$referrals = DB::table('m_referrals')
		->where('m_referrals.referred_patient_id',$patient_id)
		->join('m_patient_details','m_patient_details.id','=','m_referrals.patient_id')
		->select('*')
		->get();

		if($treatmentQuotDetails == null)
		{
			return view('treatment.treatmentnotes.quotations.addQuotationsTreatmentNotes',compact('patientTreatmentId','referrals','treatmentQuotDetails','patientId'));
		}
		else
		{
			return view('treatment.treatmentnotes.quotations.editQuotationsTreatmentNotes',compact('patientTreatmentId','referrals','treatmentQuotDetails','patientId'));
		}

	}

	public function storeQuatationsTreamentNotes(Request $request)
	{
		$patientTreatmentId = $request->get('patient_treatment_id');

		//to add new details
		$treatmentquotation = new TreatmentQuotation;
		$treatmentquotation->description=$request->get('desc');
		$treatmentquotation->cost=$request->get('cost');
		$treatmentquotation->patient_treatment_id=$patientTreatmentId;
		$treatmentquotation->save();

		return redirect()->action('TreatmentController@createQuatationsTreamentNotes',[$patientTreatmentId]);
	}

	public function updateQuatationsTreamentNotes(Request $request)
	{
		$patientTreatmentId = $request->get('patient_treatment_id');

		$treatmentQuotId = $request->get('treatment_quot_id');

		//to update details
		DB::table('t_treatment_quotation')
		->where('id', $treatmentQuotId)
		->update([
			'description' => $request->get('desc'),
			'cost' => $request->get('cost'),
			'patient_treatment_id' => $patientTreatmentId]);

		return redirect()->action('TreatmentController@createQuatationsTreamentNotes',[$patientTreatmentId]);
	}

	public function viewQuatationsTreamentNotes($patientTreatmentId)
	{
	//to get treatment quotation details
		$treatmentQuotDetails = DB::table('t_treatment_quotation')
		->where('t_treatment_quotation.patient_treatment_id',$patientTreatmentId)
		->join('t_patient_treatment','t_patient_treatment.id','=','t_treatment_quotation.patient_treatment_id')
		->select('*')
		->get();

	//to get patient id
		$patientTreatment = DB::table('t_patient_treatment')
		->where('id', $patientTreatmentId)
		->get();

		if($patientTreatment != null)
		{
			$patientId = $patientTreatment[0]->patient_id;
		}

	//to get referrals details for avove patieent Id
		$referrals = DB::table('m_referrals')
		->where('m_referrals.referred_patient_id', $patientId)
		->join('m_patient_details','m_patient_details.id','=','m_referrals.patient_id')
		->select('*')
		->get();

		return view('treatment.treatmentnotes.quotations.viewQuatationsTreamentNotes',compact('patientTreatmentId','treatmentQuotDetails','referrals','patientId'));
	}

	public function createVisitingClinicTreamentNotes($patientTreatmentId)
	{
	//to get patient id
		$patientTreatment = DB::table('t_patient_treatment')
			->where('id', $patientTreatmentId)
			->get();

		if($patientTreatment != null)
		{
			$patientId = $patientTreatment[0]->patient_id;
		}

	//to get clinic details for dropdown
		$clinics = DB::table('m_clinic')
		->get();

	//to get treatment visiting clinic details
		$treatmentVisitDetails = DB::table('t_treatment_visit_clinic')
		->where('patient_treatment_id',$patientTreatmentId)
		->join('m_clinic','m_clinic.id','=','t_treatment_visit_clinic.clinic_id')
		->select('*','t_treatment_visit_clinic.id as t_treatment_visit_clinic_id')
		->get();

		if($treatmentVisitDetails == null)
		{
			return view('treatment.treatmentnotes.visitingclinic.addVisitingClinicTreatmentNotes',compact('patientTreatmentId','clinics','treatmentVisitDetails','patientId'));
		}
		else
		{
			return view('treatment.treatmentnotes.visitingclinic.editVisitingClinicTreatmentNotes',compact('patientTreatmentId','clinics','treatmentVisitDetails','patientId'));
		}

	}

	public function storeVisitingClinicTreamentNotes(Request $request)
	{
		$patientTreatmentId = $request->get('patient_treatment_id');

		//add details
		$treatmentvisitingclinic = new TreatmentVisitingClinic;
		$treatmentvisitingclinic->fees=$request->get('fees');
		$treatmentvisitingclinic->notes=$request->get('notes');
		$treatmentvisitingclinic->clinic_id=$request->get('clinic_id');
		$treatmentvisitingclinic->patient_treatment_id=$patientTreatmentId;
		$treatmentvisitingclinic->save();

		return redirect()->action('TreatmentController@createVisitingClinicTreamentNotes',[$patientTreatmentId]);
	}

	public function updateVisitingClinicTreamentNotes(Request $request)
	{
		$patientTreatmentId = $request->get('patient_treatment_id');

		$treatmentVisitId = $request->get('treatment_visit_id');

		//update details
		DB::table('t_treatment_visit_clinic')
		->where('id', $treatmentVisitId)
		->update(['fees' => $request->get('fees'),
			'notes' => $request->get('notes'),
			'clinic_id' => $request->get('clinic_id'),
			'patient_treatment_id' => $patientTreatmentId]);

		return redirect()->action('TreatmentController@createVisitingClinicTreamentNotes',[$patientTreatmentId]);
	}

	public function viewVisitingClinicTreamentNotes($patientTreatmentId)
	{
		$clinics = DB::table('m_clinic')
		->get();

		$treatmentVisitDetails = DB::table('t_treatment_visit_clinic')
		->where('patient_treatment_id',$patientTreatmentId)
		->join('m_clinic','m_clinic.id','=','t_treatment_visit_clinic.clinic_id')
		->join('t_patient_treatment','t_patient_treatment.id','=','t_treatment_visit_clinic.patient_treatment_id')
		->select('*','t_treatment_visit_clinic.id as t_treatment_visit_clinic_id')
		->get();

		$patientTreatment = DB::table('t_patient_treatment')
		->where('id', $patientTreatmentId)
		->get();

		if($patientTreatment != null)
		{
			$patientId = $patientTreatment[0]->patient_id;
		}

		return view('treatment.treatmentnotes.visitingclinic.viewVisitingClinicTreamentNotes',compact('patientTreatmentId','treatmentVisitDetails','clinics','patientId'));
	}

	public function createFindingNotesTreamentNotes($patientTreatmentId)
	{
	//to get patient id
		$patientTreatment = DB::table('t_patient_treatment')
			->where('id', $patientTreatmentId)
			->get();

		if($patientTreatment != null)
		{
			$patientId = $patientTreatment[0]->patient_id;
		}

	//to get treatment findings details
		$treatmentFindingDetails = DB::table('t_treatment_findings')
		->where('patient_treatment_id',$patientTreatmentId)
		->get();

		if($treatmentFindingDetails == null)
		{
			return view('treatment.treatmentnotes.findingnotes.addFindingNotesTreatmentNotes',compact('patientTreatmentId','treatmentFindingDetails','patientId'));
		}
		else
		{
			return view('treatment.treatmentnotes.findingnotes.editFindingNotesTreatmentNotes',compact('patientTreatmentId','treatmentFindingDetails','patientId'));
		}

	}

	public function storeFindingNotesTreamentNotes(Request $request)
	{
		$patientTreatmentId = $request->get('patient_treatment_id');

		//add details
		$treatmentfindingsnotes = new TreatmentFindingsNotes;
		$treatmentfindingsnotes->findings=$request->get('findings');
		$treatmentfindingsnotes->notes=$request->get('notes');
		$treatmentfindingsnotes->patient_treatment_id=$patientTreatmentId;
		$treatmentfindingsnotes->save();

		return redirect()->action('TreatmentController@createFindingNotesTreamentNotes',[$patientTreatmentId]);
	}

	public function updateFindingNotesTreamentNotes(Request $request)
	{
		$patientTreatmentId = $request->get('patient_treatment_id');

		$treatmentFindingId = $request->get('treatment_finding_id');

		//update details
		DB::table('t_treatment_findings')
		->where('id', $treatmentFindingId)
		->update(['findings' => $request->get('findings'),
			'notes' => $request->get('notes'),
			'patient_treatment_id' => $patientTreatmentId]);

		return redirect()->action('TreatmentController@createFindingNotesTreamentNotes',[$patientTreatmentId]);
	}

	public function viewFindingNotesTreamentNotes($patientTreatmentId)
	{
	//to get treatment finding notes details
		$treatmentFindingDetails = DB::table('t_treatment_findings')
		->where('patient_treatment_id',$patientTreatmentId)
		->join('t_patient_treatment','t_patient_treatment.id','=','t_treatment_findings.patient_treatment_id')
		->get();

	//to get patient id
		$patientTreatment = DB::table('t_patient_treatment')
		->where('id', $patientTreatmentId)
		->get();

		if($patientTreatment != null)
		{
			$patientId = $patientTreatment[0]->patient_id;
		}

		return view('treatment.treatmentnotes.findingnotes.viewFindingNotesTreamentNotes',compact('patientTreatmentId','treatmentFindingDetails','patientId'));
	}
/*
	public function GetDetailsTreatmentInfo()
	{
		return view('treatment.treatmentnotes.GetDetailsTreatmentInfo');
	}

	public function DashboardTreatmentNotes()
	{
		return view('treatment.treatmentnotes.GetDetailsTreatmentInfo');
	}*/

	public function DashboardTreatmentSitting()
	{
		return view('treatment.sitting.DashboardSitting');
	}

	public function listViewSitting($patientTreatmentId)
	{
	//to fetch sittings Id
		$sittings = DB::table('t_sittings')
		->where('patient_treatment_id',$patientTreatmentId)
		->get();

		if($sittings == null)
		{
			$sittingsDetails = null;
		}
		else
		{
			$sittingsId = $sittings[0]->id;

		//to fetch all sittings details
			$sittingsDetails = DB::table('t_sittings_general')
			->where('t_sittings_general.sittings_id', $sittingsId)
			->join('t_sittings_diagnosis','t_sittings_diagnosis.sittings_id','=','t_sittings_general.sittings_id')
			->join('t_appointment','t_appointment.id','=','t_sittings_general.appointment_id')
			->join('s_dentist_details','s_dentist_details.id','=','t_appointment.dentist_id')
			->select('*')
			->get();

	//to fetch all lab work sittings details
			$labWork = DB::table('t_sitting_lab_work')
			->join('dnf_lab_work_status','dnf_lab_work_status.id','=','t_sitting_lab_work.lab_work_status_id')
			->select('*')
			->get();
		}

		return view('treatment.sitting.listViewSitting',compact('patientTreatmentId','sittingsDetails','labWork'));
	}

	public function viewListViewSitting($patientTreatmentId)
	{
	//to fetch sittings Id
		$sittings = DB::table('t_sittings')
		->where('patient_treatment_id',$patientTreatmentId)
		->get();

		if($sittings == null)
		{
			$sittingsDetails = null;
		}
		else
		{
			$sittingsId = $sittings[0]->id;

		//to fetch all sittings details
			$sittingsDetails = DB::table('t_sittings_general')
			->where('t_sittings_general.sittings_id', $sittingsId)
			->join('t_sittings_diagnosis','t_sittings_diagnosis.sittings_id','=','t_sittings_general.sittings_id')
			->join('t_appointment','t_appointment.id','=','t_sittings_general.appointment_id')
			->join('s_dentist_details','s_dentist_details.id','=','t_appointment.dentist_id')
			->select('*')
			->get();

	//to fetch all lab work sittings details
			$labWork = DB::table('t_sitting_lab_work')
			->join('dnf_lab_work_status','dnf_lab_work_status.id','=','t_sitting_lab_work.lab_work_status_id')
			->select('*')
			->get();
		}

		return view('treatment.sitting.viewListViewSitting',compact('patientTreatmentId','sittingsDetails','labWork'));
	}

	public function storeSitting($patientTreatmentId)
	{
		$sittings = new Sittings;
		$sittings->patient_treatment_id = $patientTreatmentId;
		$sittings->save();

		$sittingsId = DB::table('t_sittings')
		->orderBy('id', 'desc')
		->first();

		$sittingsId = $sittingsId->id;

		return redirect()->action('TreatmentController@createGeneralSitting',[$sittingsId]);
	}

	public function createGeneralSitting($sittingsId)
	{
	//to fetch patient treatment ID
		$sittings = DB::table('t_sittings')
		->where('id', $sittingsId)
		->get();

		$patientTreatmentId = $sittings['0']->patient_treatment_id;

	//to fetch patient ID
		$patientId = DB::table('t_patient_treatment')
		->where('id',$patientTreatmentId)
		->get();

		$patientId = $patientId[0]->patient_id;

	//to get appointment details for above patient id
		$appointmentDetails = DB::table('t_appointment')
		->where('t_appointment.patient_id',$patientId)
		->join('s_chair_details','s_chair_details.id','=','t_appointment.chair_id')
		->select('*','t_appointment.id as appointmentId')
		->get();

	//to get material details for dropdown
		$materialName = DB::table('m_material_details')
		->get();

	//to get instrument details for dropdown
		$instrumentName = DB::table('m_instrument_details')
		->get();

	//to get gadget details for dropdown
		$gadgetName = DB::table('m_gadget_details')
		->get();

	//to fetch general sittings details
		$generalSittingsDetails = DB::table('t_sittings_general')
		->where('t_sittings_general.sittings_id',$sittingsId)
		->join('t_appointment','t_appointment.id','=','t_sittings_general.appointment_id')
		->join('m_material_details','m_material_details.id','=','t_sittings_general.material_name_id')
		->join('m_instrument_details','m_instrument_details.id','=','t_sittings_general.instrument_name_id')
		->join('m_gadget_details','m_gadget_details.id','=','t_sittings_general.gadget_id')
		->select('*','t_sittings_general.id as id')
		->get();

		if($generalSittingsDetails == null)
		{
			return view('treatment.sitting.general.addGeneralSitting', compact('sittingsId','patientTreatmentId','appointmentDetails','materialName','instrumentName','gadgetName','generalSittingsDetails'));
		}
		else
		{
			$generalSittingsDetails = $generalSittingsDetails[0];

			return view('treatment.sitting.general.editGeneralSitting', compact('sittingsId','patientTreatmentId','appointmentDetails','materialName','instrumentName','gadgetName','generalSittingsDetails'));
		}

	}

	public function storeGeneralSitting(Request $request)
	{
		$sittingsId = $request->get('settings_id');

		$sittingsgeneral = new SittingsGeneral;
		$sittingsgeneral->sittings_id = $sittingsId;
		$sittingsgeneral->appointment_id = $request->get('appointment_id');
		$sittingsgeneral->material_name_id = $request->get('material_id');
		$sittingsgeneral->instrument_name_id = $request->get('instrument_id');
		$sittingsgeneral->gadget_id = $request->get('gadget_id');
		$sittingsgeneral->save(); 

		return redirect()->action('TreatmentController@createGeneralSitting',[$sittingsId]);
	}

	public function updateGeneralSitting(Request $request)
	{
		$sittingsGeneralId = $request->get('settings_general_id');

		$sittingsId = $request->get('settings_id');

		DB::table('t_sittings_general')
		->where('id', $sittingsGeneralId)
		->update(['sittings_id' => $sittingsId,
			'appointment_id' => $request->get('appointment_id'),
			'material_name_id' => $request->get('material_id'),
			'instrument_name_id' => $request->get('instrument_id'),
			'gadget_id' => $request->get('gadget_id')
			]);

		return redirect()->action('TreatmentController@createGeneralSitting',[$sittingsId]);
	}

	public function viewGeneralSitting($sittingsId)
	{
	//to fetch patient treatment ID
		$sittings = DB::table('t_sittings')
		->where('t_sittings.id', $sittingsId)
		->join('t_treatment_general','t_treatment_general.patient_treatment_id','=','t_sittings.patient_treatment_id')
		->join('dnf_treatment_status','dnf_treatment_status.id','=','t_treatment_general.status_id')
		->select('*')
		->get();

		$patientTreatmentId = $sittings['0']->patient_treatment_id;

		$view = $sittings['0']->status;

	//to fetch general sittings details
		$generalSittingsDetails = DB::table('t_sittings_general')
		->where('t_sittings_general.sittings_id',$sittingsId)
		->join('t_appointment','t_appointment.id','=','t_sittings_general.appointment_id')
		->join('m_material_details','m_material_details.id','=','t_sittings_general.material_name_id')
		->join('m_instrument_details','m_instrument_details.id','=','t_sittings_general.instrument_name_id')
		->join('m_gadget_details','m_gadget_details.id','=','t_sittings_general.gadget_id')
		->select('*','t_sittings_general.id as id')
		->get();

		$generalSittingsDetails = $generalSittingsDetails[0];

		return view('treatment.sitting.general.viewGeneralSitting', compact('sittingsId','patientTreatmentId','generalSittingsDetails','view'));
	}

	public function listViewLabWorkSitting($sittingsId)
	{
	//to fetch patient treatment ID
		$sittings = DB::table('t_sittings')
		->where('id', $sittingsId)
		->get();

		$patientTreatmentId = $sittings['0']->patient_treatment_id;

	//to get lab work details
		$labWorkStatus = DB::table('dnf_lab_work_status')
		->get();

	//to display all values in lab work sittings
		$labWork = DB::table('t_sitting_lab_work')
		->where('t_sitting_lab_work.sittings_id', $sittingsId)
		->join('dnf_lab_work_status','dnf_lab_work_status.id','=','t_sitting_lab_work.lab_work_status_id')
		->select('*','t_sitting_lab_work.id as id')
		->get();

		return view('treatment.sitting.labwork.listViewLabWorkSitting',compact('sittingsId','patientTreatmentId','labWork','labWorkStatus'));
	}

	public function makeStatusRework(Request $request)
	{
		$labWorkId = $request->get('lab_work_id');

		$labWorkStatusId = $request->get('lab_work_status_id');

		$sittingsId = $request->get('settings_id');

		DB::table('t_sitting_lab_work')
		->where('id', $labWorkId)
		->update(['lab_work_status_id' => $labWorkStatusId
			]);

		return redirect()->action('TreatmentController@listViewLabWorkSitting',[$sittingsId]);
	}

	public function makeStatusComplete(Request $request)
	{
		$labWorkId = $request->get('lab_work_id');

		$labWorkStatusId = $request->get('lab_work_status_id');

		$sittingsId = $request->get('settings_id');

		DB::table('t_sitting_lab_work')
		->where('id', $labWorkId)
		->update(['lab_work_status_id' => $labWorkStatusId
			]);

		return redirect()->action('TreatmentController@listViewLabWorkSitting',[$sittingsId]);
	}

	public function createLabWorkSitting($sittingsId)
	{
	//to fetch patient treatment ID
		$sittings = DB::table('t_sittings')
		->where('id', $sittingsId)
		->get();

		$patientTreatmentId = $sittings['0']->patient_treatment_id;

	//to get lab work status for dropdown
		$labWorkStatus = DB::table('dnf_lab_work_status')
		->get();

	//to get gadget details for dropdown
		$workType = DB::table('s_lab_work_type')
		->get();

		return view('treatment.sitting.labwork.addLabWorkSitting',compact('sittingsId','patientTreatmentId','labWorkStatus','workType'));
	}

	public function storeLabWorkSitting(Request $request)
	{
		$sittingsId = $request->get('settings_id');

		$sittingslabwork = new SittingsLabWork;
		$sittingslabwork->job_date = $request->get('job_date');
		$sittingslabwork->delivery_date = $request->get('delivery_date');
		$sittingslabwork->observation = $request->get('observation');
		$sittingslabwork->remarks = $request->get('remarks');
		$sittingslabwork->sittings_id = $sittingsId;
		$sittingslabwork->lab_work_status_id = $request->get('lab_work_status_id');
		$sittingslabwork->work_type_id = $request->get('work_type_id');
		$sittingslabwork->save(); 

		return redirect()->action('TreatmentController@listViewLabWorkSitting',[$sittingsId]);
	}

	public function viewListViewLabWorkSitting($sittingsId)
	{
	//to fetch patient treatment ID
		$sittings = DB::table('t_sittings')
		->where('t_sittings.id', $sittingsId)
		->join('t_treatment_general','t_treatment_general.patient_treatment_id','=','t_sittings.patient_treatment_id')
		->join('dnf_treatment_status','dnf_treatment_status.id','=','t_treatment_general.status_id')
		->select('*')
		->get();

		$patientTreatmentId = $sittings['0']->patient_treatment_id;

		$view = $sittings['0']->status;

	//to display all values in lab work sittings
		$labWork = DB::table('t_sitting_lab_work')
		->where('t_sitting_lab_work.sittings_id', $sittingsId)
		->join('dnf_lab_work_status','dnf_lab_work_status.id','=','t_sitting_lab_work.lab_work_status_id')
		->select('*','t_sitting_lab_work.id as id')
		->get();

		return view('treatment.sitting.labwork.viewListViewLabWorkSitting',compact('sittingsId','patientTreatmentId','labWork','view'));
	}

	public function viewLabWorkSitting($lab_work_id)
	{

	//to display all values in lab work sittings
		$labWork = DB::table('t_sitting_lab_work')
		->where('t_sitting_lab_work.id', $lab_work_id)
		->join('dnf_lab_work_status','dnf_lab_work_status.id','=','t_sitting_lab_work.lab_work_status_id')
		->join('s_lab_work_type','s_lab_work_type.id','=','t_sitting_lab_work.work_type_id')
		->select('*','t_sitting_lab_work.id as id')
		->get();

		$labWork = $labWork['0'];

		$sittingsId = $labWork->sittings_id;

	//to fetch patient treatment ID
		$sittings = DB::table('t_sittings')
		->where('t_sittings.id', $sittingsId)
		->join('t_treatment_general','t_treatment_general.patient_treatment_id','=','t_sittings.patient_treatment_id')
		->join('dnf_treatment_status','dnf_treatment_status.id','=','t_treatment_general.status_id')
		->select('*')
		->get();

		$patientTreatmentId = $sittings['0']->patient_treatment_id;

		$view = $sittings['0']->status;

		return view('treatment.sitting.labwork.viewLabWorkSitting',compact('sittingsId','patientTreatmentId','labWork','view'));
	}

	public function createAttachmentSitting($sittingsId)
	{
	//to fetch patient treatment ID
		$sittings = DB::table('t_sittings')
		->where('id', $sittingsId)
		->get();

		$patientTreatmentId = $sittings['0']->patient_treatment_id;

		return view('treatment.sitting.attachment.addAttachmentSitting',compact('sittingsId','patientTreatmentId'));
	}

	public function storeAttachmentSitting(Request $request)
	{
		$sittingsId = $request->get('settings_id');
		
		return redirect()->action('TreatmentController@createAttachmentSitting',[$sittingsId]);
	}

	public function viewAttachmentSitting($sittingsId)
	{
	//to fetch patient treatment ID
		$sittings = DB::table('t_sittings')
		->where('t_sittings.id', $sittingsId)
		->join('t_treatment_general','t_treatment_general.patient_treatment_id','=','t_sittings.patient_treatment_id')
		->join('dnf_treatment_status','dnf_treatment_status.id','=','t_treatment_general.status_id')
		->select('*')
		->get();

		$patientTreatmentId = $sittings['0']->patient_treatment_id;

		$view = $sittings['0']->status;

		return view('treatment.sitting.attachment.viewAttachmentSitting',compact('sittingsId','patientTreatmentId','view'));
	}

	public function createDiagnosisSitting($sittingsId)
	{
	//to fetch patient treatment ID
		$sittings = DB::table('t_sittings')
		->where('id', $sittingsId)
		->get();

		$patientTreatmentId = $sittings['0']->patient_treatment_id;

	//to fetch general sittings details
		$diagnosisSittingsDetails = DB::table('t_sittings_diagnosis')
		->where('sittings_id',$sittingsId)
		->select('*')
		->get();

		if($diagnosisSittingsDetails == null)
		{
			return view('treatment.sitting.diagnosis.addDiagnosisSitting',compact('sittingsId','patientTreatmentId','diagnosisSittingsDetails'));
		}
		else
		{
			$diagnosisSittingsDetails = $diagnosisSittingsDetails[0];

			return view('treatment.sitting.diagnosis.editDiagnosisSitting',compact('sittingsId','patientTreatmentId','diagnosisSittingsDetails'));
		}
		
	}

	public function storeDiagnosisSitting(Request $request)
	{
		$sittingsId = $request->get('settings_id');

		$sittingsdiagnosis = new SittingsDiagnosis;
		$sittingsdiagnosis->plan = $request->get('plan');
		$sittingsdiagnosis->prescription = $request->get('prescription');
		$sittingsdiagnosis->findings = $request->get('findings');
		$sittingsdiagnosis->notes = $request->get('notes');
		$sittingsdiagnosis->sittings_id = $sittingsId;
		$sittingsdiagnosis->save();

		//to fetch patient treatment ID
		$sittings = DB::table('t_sittings')
		->where('id', $sittingsId)
		->get();

		$patientTreatmentId = $sittings['0']->patient_treatment_id;
		
		return redirect()->action('TreatmentController@listViewSitting',[$patientTreatmentId]);
	}

	public function updateDiagnosisSitting(Request $request)
	{
		$sittingsDiagnosisId = $request->get('settings_diagnosis_id');

		$sittingsId = $request->get('settings_id');

	//to update the records
		DB::table('t_sittings_diagnosis')
		->where('id', $sittingsDiagnosisId)
		->update([
			'plan' => $request->get('plan'),
			'prescription' => $request->get('prescription'),
			'findings' => $request->get('findings'),
			'notes' => $request->get('notes'),
			'sittings_id' => $sittingsId
			]);

	//to fetch patient treatment ID
		$sittings = DB::table('t_sittings')
		->where('id', $sittingsId)
		->get();

		$patientTreatmentId = $sittings['0']->patient_treatment_id;

		return redirect()->action('TreatmentController@listViewSitting',[$patientTreatmentId]);
	}

	public function viewDiagnosisSitting($sittingsId)
	{
	//to fetch patient treatment ID
		$sittings = DB::table('t_sittings')
		->where('t_sittings.id', $sittingsId)
		->join('t_treatment_general','t_treatment_general.patient_treatment_id','=','t_sittings.patient_treatment_id')
		->join('dnf_treatment_status','dnf_treatment_status.id','=','t_treatment_general.status_id')
		->select('*')
		->get();

		$patientTreatmentId = $sittings['0']->patient_treatment_id;

		$view = $sittings['0']->status;

	//to fetch general sittings details
		$diagnosisSittingsDetails = DB::table('t_sittings_diagnosis')
		->where('sittings_id',$sittingsId)
		->select('*')
		->get();

		$diagnosisSittingsDetails = $diagnosisSittingsDetails[0];

		return view('treatment.sitting.diagnosis.viewDiagnosisSitting',compact('sittingsId','patientTreatmentId','diagnosisSittingsDetails','view'));
	}

	public function listViewLabWork($patientTreatmentId)
	{
	//to fetch sittings Id
		$sittings = DB::table('t_sittings')
		->where('patient_treatment_id',$patientTreatmentId)
		->get();

		if($sittings == null)
		{
			$labWork = null;
		}
		else
		{
			$sittingsId = $sittings[0]->id;
			
		//to display all values in lab work sittings
			$labWork = DB::table('t_sitting_lab_work')
			->where('t_sitting_lab_work.sittings_id', $sittingsId)
			->join('dnf_lab_work_status','dnf_lab_work_status.id','=','t_sitting_lab_work.lab_work_status_id')
			->select('*','t_sitting_lab_work.id as id')
			->get();
		}

		return view('treatment.labwork.listViewLabWork',compact('patientTreatmentId','labWork'));
	}

	public function viewListViewLabWork($patientTreatmentId)
	{
	//to fetch sittings Id
		$sittings = DB::table('t_sittings')
		->where('patient_treatment_id',$patientTreatmentId)
		->get();

		if($sittings == null)
		{
			$labWork = null;
		}
		else
		{
			$sittingsId = $sittings[0]->id;
			
		//to display all values in lab work sittings
			$labWork = DB::table('t_sitting_lab_work')
			->where('t_sitting_lab_work.sittings_id', $sittingsId)
			->join('dnf_lab_work_status','dnf_lab_work_status.id','=','t_sitting_lab_work.lab_work_status_id')
			->select('*','t_sitting_lab_work.id as id')
			->get();
		}

		return view('treatment.labwork.viewListViewLabWork',compact('patientTreatmentId','labWork'));
	}

	public function viewLabWork($lab_work_id)
	{

	//to display all values in lab work sittings
		$labWork = DB::table('t_sitting_lab_work')
		->where('t_sitting_lab_work.id', $lab_work_id)
		->join('dnf_lab_work_status','dnf_lab_work_status.id','=','t_sitting_lab_work.lab_work_status_id')
		->join('s_lab_work_type','s_lab_work_type.id','=','t_sitting_lab_work.work_type_id')
		->select('*','t_sitting_lab_work.id as id')
		->get();

		$labWork = $labWork['0'];

		$sittingsId = $labWork->sittings_id;

	//to fetch patient treatment ID
		$sittings = DB::table('t_sittings')
		->where('t_sittings.id', $sittingsId)
		->join('t_treatment_general','t_treatment_general.patient_treatment_id','=','t_sittings.patient_treatment_id')
		->join('dnf_treatment_status','dnf_treatment_status.id','=','t_treatment_general.status_id')
		->select('*')
		->get();

		$patientTreatmentId = $sittings['0']->patient_treatment_id;

		$view = $sittings['0']->status;

		return view('treatment.labwork.viewLabWork',compact('sittingsId','patientTreatmentId','labWork','view'));
	}

//Ajax Functions
	public function getClinicName($clinic_id)
	{
		$clinic_name = DB::table('m_clinic')
		->where("id", $clinic_id)
		->get();

		return $clinic_name;

	}

	public function getAppointmentDetails($appointment_id)
	{
		$appointmentDetails = DB::table('t_appointment')
		->where("id", $appointment_id)
		->get();

		return $appointmentDetails;

	}

}
