<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Filesystem\Factory;
use DB;
use Input;
use Session;

use Illuminate\Http\Request;
use App\BankDetails;
use App\BillingBank;
use App\BillingPersonal;
use App\ClinicDetails;
use App\ClinicSlotDetails;
use App\ClinicSlotTimeDetails;
use App\GadgetDetails;
use App\GadgetType;
use App\GadgetSubtype;
use App\GadgetTreatment;
use App\GadgetVendorSpecification;
use App\GeneralStaffRegistration;
use App\InstrumentDetails;
use App\InstrumentTreatment;
use App\Instrumenttype;
use App\InstrumentSafetyStock;
use App\Instrumentsubtype;
use App\Instrumentunit;
use App\InstrumentVendorSpecification;
use App\LeaveFullDay;
use App\LeavePartialDays;
use App\LabVendorSpecification;
use App\MachineVendorSpecification;
use App\MachineDetails;
use App\Machinetype;
use App\Machinesubtype;
use App\Machineunit;
use App\MachineTreatment;
use App\MaintenanceVendorSpecification;
use App\MaterialVendorSpecification;
use App\MaterialDetails;
use App\MaterialTreatment;
use App\Materialtype;
use App\Materialsubtype;
use App\MaterialSaftyStock;
use App\MedicalHistory;
use App\OtherReferrals;
use App\Patient;
use App\PersonDetails;
use App\PrimaryClinic;
use App\PrimaryClinicSlotDetails;
use App\PrimaryClinicSlotTimeDetails;
use App\Referrals;
use App\StaffClinic;
use App\StaffSlotDetails;
use App\StaffSlotTiming;
use App\TreatmentName;
use App\Unit;
use App\VendorDetails;
use App\VendorLabDetails;
use App\VendorWorkSpecification;
use App\VendorMaintenanceDetails;
use App\VendorMachineDetails;
use App\VendorInstrumentDetails;
use App\VendorMaterialDetails;
use App\VendorGadgetDetails;
use App\VisitingClinic;
use App\VisitingClinicSlotDetails;
use App\VisitingClinicSlotTimeDetails;
use App\WorkType;
use App\WorkSubType;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class MasterController extends Controller {

	/**
	 * Display a listing of patient section.
	 *
	 * 
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	
	public function index()
	{
		return view('layouts.masterNav');
	}

	public function listViewPatient()
	{
		Session::forget('patient_id');
		Session::forget('patient_name');
		Session::forget('patient_cell_no');
		Session::forget('patient_gender');
		Session::forget('patient_email');
		Session::forget('patient_occupation');
		Session::forget('patient_address');
		Session::forget('patient_dob');
		Session::forget('referred_patient_id');
		Session::forget('referred_patient_name');
		Session::forget('other_reff');

		

		$patient_det = DB::table('m_patient_details')
		->join('m_medical_history', 'm_patient_details.id', '=', 'm_medical_history.patient_id')
		->select('*')
		->get();

		return view('master.patient.dashboardPatient',compact('patient_det'));
	}
	
	public function createPatientGeneral()
	{
		/*$d=Session::all();
		dd($d);*/
		$patientNames = DB::table('m_patient_details')
		->get();

		$patientID = DB::table('m_patient_details')
		->orderBy('id','desc')
		->first();

		if($patientID == null)
		{
			$patientID = (object)array('id' => 0);
		}

		return view('master.patient.addPatientGeneral',compact('patientNames','patientID'));
	}

	public function storePatientGeneral(Request $request)
	{
		$patient_id=$request->get('patient_id');
		$patient_name=$request->get('pat_name');

		$patient_cell_no = $request->get('pat_cell');
		$patient_gender = $request->get('gender');
		$patient_email = $request->get('mat_email');
		$patient_occupation = $request->get('mat_occ');
		$patient_address = $request->get('address');
		$patient_dob = $request->get('dob');
		$referred_patient_id=$request->get('referred_patient_id');
		
		$referred=explode(",",$referred_patient_id);
		

		if($referred_patient_id =='Other')
		{	
			$other_reff=$request->get('other_referrals');
			$referred_patient_name="";
		}
		else
		{
			$referred_patient_id=$referred[0];

			$referred_patient_name=$referred[1];
			$other_reff="";
		}

		session(['patient_id'=>$patient_id,
			'patient_name'=>$patient_name,
			'patient_cell_no'=>$patient_cell_no,
			'patient_gender'=>$patient_gender,
			'patient_email'=>$patient_email,
			'patient_occupation'=>$patient_occupation,
			'patient_address'=>$patient_address,
			'patient_dob'=>$patient_dob,
			'referred_patient_id'=>$referred_patient_id,
			'referred_patient_name'=>$referred_patient_name,
			'other_reff'=>$other_reff]);

		return redirect()->action('MasterController@createPatientMedicalHistory');
	}

	public function createPatientMedicalHistory(Request $request)
	{
		
		if($request->session()->has('patient_name') == true)
		{
			$users=null;

			if($request->session()->has('patient_id') == false)
			{
				$users = DB::table('m_patient_details')
				->orderBy('id', 'desc')
				->first();

				if($users == null)
				{
					$users = (object)array('id' => 0);
				}

				$users = $users->id + 1;
			}
			else
			{
				$users = Session::get('patient_id');

				$patient_name = Session::get('patient_name');
			}
		}
		else
		{
			//flash-message
			$request->session()->flash('alert-danger', 'Patient Name is required!');

			return redirect()->action('MasterController@createPatientGeneral');
		}
		

		return view('master.patient.addPatientMedicalHistory', compact('users'));
	}

	public function storePatientMedicalHistory(Request $request)
	{
		
		$patient_id = Session::get('patient_id');
		$patient_name  = Session::get('patient_name');
		$patient_cell_no  = Session::get('patient_cell_no');
		$patient_gender  = Session::get('patient_gender');
		$patient_email  = Session::get('patient_email');
		$patient_occupation  = Session::get('patient_occupation');
		$patient_address  = Session::get('patient_address');
		$patient_dob  = Session::get('patient_dob');
		$referred_patient_id  = Session::get('referred_patient_id');
		$referred_patient_name  = Session::get('referred_patient_name');
		$other_reff  = Session::get('other_reff');
		
		$file = $request->file('file');

		if($file != null)
		{
			if(file_exists(storage_path().'/app/'.$file->getClientOriginalName()))
			{
			//flash-message
				$request->session()->flash('alert-danger', 'File already exist!');

				return redirect()->action('MasterController@createPatientMedicalHistory');   
			}
			else
			{
				$patient = new Patient;

				$patient->name = $patient_name;
				$patient->cell_no = $patient_cell_no;
				$patient->gender = $patient_gender;
				$patient->email = $patient_email;
				$patient->occupation = $patient_occupation;
				$patient->address = $patient_address;
				$patient->dob = $patient_dob;

				$patient->save();

				if($request->get('referred_patient_id') != 'Other')
				{
					$referrals = new Referrals;
					$referrals->patient_id = $patient_id;
					$referrals->referred_patient_id = $referred_patient_id;
					$referrals->save();
				}
				else
				{
					$otherreferrals = new OtherReferrals;
					$otherreferrals->other_referrals_name = $other_reff;
					$otherreferrals->patient_id = $patient_id;
					$otherreferrals->save();
				}

				$extension = $file->getClientOriginalExtension();
				Storage::disk('local')->put($file->getClientOriginalName(),  File::get($file));

		//$destinationPath = 'uploads'; // upload path
		//$destinationPath = Input::file('file')->getRealPath();
		//$fileName = $file->getFilename().'.'.$extension; // renameing image
		//$request->file('file')->move($destinationPath, $fileName); // uploading file to given path

		// Logo URL
				/*$logoUrl = url('/').'/'.$destinationPath.'/'.$fileName;*/

		// Input::file('file')->move(storage_path().'/app',Input::file('file')->getClientOriginalName());
		// $fileName =  Input::file('file')->getClientOriginalName();
		// $logoUrl = storage_path()."/app/".$fileName;

				$medicalhistory = new MedicalHistory();
				$medicalhistory->medical_history = $request->get('med_hist');
				$medicalhistory->mime = $file->getClientMimeType();
				$medicalhistory->original_filename = $file->getClientOriginalName();
				$medicalhistory->patient_id = $request->get('pat_id');

				$medicalhistory->save();

			}

		}
		else
		{
			$patient = new Patient;

			$patient->name = $patient_name;
			$patient->cell_no = $patient_cell_no;
			$patient->gender = $patient_gender;
			$patient->email = $patient_email;
			$patient->occupation = $patient_occupation;
			$patient->address = $patient_address;
			$patient->dob = $patient_dob;

			$patient->save();

			if($request->get('referred_patient_id') != 'Other')
			{
				$referrals = new Referrals;
				$referrals->patient_id = $patient_id;
				$referrals->referred_patient_id = $referred_patient_id;
				$referrals->save();
			}
			else
			{
				$otherreferrals = new OtherReferrals;
				$otherreferrals->other_referrals_name = $other_reff;
				$otherreferrals->patient_id = $patient_id;
				$otherreferrals->save();
			}
		}
		

		//flash-message
		$request->session()->flash('alert-success', 'Patient Details was successfully added!');

		return redirect()->action('MasterController@listViewPatient');
	}

	public function editPatientGeneral($patient_id)
	{
		/*$patient_id = $request->get('pat_id');*/

		$patientNames = DB::table('m_patient_details')
		->get();

		$patient_detials = DB::table('m_patient_details')
		->join('m_medical_history', 'm_patient_details.id', '=', 'm_medical_history.patient_id')
		->select('*')
		->where('m_patient_details.id', $patient_id)
		->get();
		
		$reffered_patient_name=DB::table('m_referrals')
		->where('patient_id',$patient_id)
		->join('m_patient_details','m_referrals.referred_patient_id','=','m_patient_details.id')
		->select('patient_id','referred_patient_id','m_patient_details.name')
		->get();

		$other_reff_pat_name=DB::table('m_other_referrals')
		->where('patient_id',$patient_id)
		->get();

		/*dd($other_reff_pat_name);*/
		
		return view('master.patient.editPatientGeneral', compact('patient_id','patient_detials','patientNames','reffered_patient_name','other_reff_pat_name'));
	}

	public function updatePatientGeneral(Request $request)
	{
		//get values
		$patient_id = $request->get('pat_id');
		$patient_name=$request->get('pat_name');
		$patient_cell=$request->get('pat_cell');
		$patient_gender=$request->get('gender');
		$patient_mail=$request->get('mat_email');
		$patient_occ=$request->get('mat_occ');
		$patient_address=$request->get('address');
		$patient_dob=$request->get('dob');
		
		$patient_reff=$request->get('referred_patient_id');
		
		$referred=explode(",",$patient_reff);

		if($patient_reff == 'Other')
		{
			$other_reff=$request->get('other_referrals');
			$referred_patient_name="";
			$referred_patient_id="";
		}
		else
		{
			$referred_patient_id=$referred[0];
			$referred_patient_name=$referred[1];
			$other_reff="";
		}

		//set session for get values
		session(['patient_id'=>$patient_id,
			'patient_name'=>$patient_name,
			'patient_cell_no'=>$patient_cell,
			'patient_gender'=>$patient_gender,
			'patient_email'=>$patient_mail,
			'patient_occupation'=>$patient_occ,
			'patient_address'=>$patient_address,
			'patient_dob'=>$patient_dob,
			'referred_patient_id'=>$referred_patient_id,
			'referred_patient_name'=>$referred_patient_name,
			'other_reff'=>$other_reff]);

		return redirect()->action('MasterController@editPatientMedicalHistory',[$patient_id]);
	}

	public function editPatientMedicalHistory($patient_id,Request $request)
	{
		
		if($request->session()->has('patient_name') == true || $patient_id != null)
		{
			$patient_detials = DB::table('m_patient_details')
			->join('m_medical_history', 'm_patient_details.id', '=', 'm_medical_history.patient_id')
			->select('*')
			->where('m_patient_details.id', $patient_id)
			->get();

			return view('master.patient.editPatientMedicalHistory', compact('patient_id','patient_detials'));
		}
		else
		{
			$request->session()->flash('alert-danger', 'Patient Name is required!');

			return redirect()->action('MasterController@updatePatientGeneral',[$patient_id]);
		}

	}

	public function updatePatientMedicalHistory(Request $request)
	{
		$patient_id = $request->get('pat_id');
		$patient_med_id = $request->get('pat_med_id');


		$pat_id=Session::get('patient_id');
		$pat_name=Session::get('patient_name');
		$pat_cell=Session::get('patient_cell_no');
		$pat_gender=Session::get('patient_gender');
		$pat_mail=Session::get('patient_email');
		$pat_occ=Session::get('patient_occupation');
		$pat_address=Session::get('patient_address');
		$pat_dob=Session::get('patient_dob');
		$referred_patient_id  = Session::get('referred_patient_id');
		$referred_patient_name  = Session::get('referred_patient_name');
		$other_reff  = Session::get('other_reff');

		DB::table('m_patient_details')
		->where('id', $patient_id)
		->update(['name' => $pat_name,
			'cell_no'=> $pat_cell,
			'gender' => $pat_gender,
			'email' => $pat_mail,
			'occupation' => $pat_occ,
			'address' => $pat_address,
			'dob' => $pat_dob
			]);

		if($referred_patient_id != null)
		{
			$reff_avail=DB::table('m_referrals')
			->where('patient_id',$patient_id)
			->get();
			if($reff_avail == null)
			{
				$reffobj=new Referrals;
				$reffobj->patient_id=$patient_id;
				$reffobj->referred_patient_id=$referred_patient_id;

				$reffobj->save();
			}
			else
			{
				DB::table('m_referrals')
				->where('patient_id',$patient_id)
				->update([
					'referred_patient_id'=>$referred_patient_id
					]);
			}
		}
		else
		{
			$other_avail=DB::table('m_other_referrals')
			->where('patient_id',$patient_id)
			->get();

			if($other_avail == null)
			{
				$value=new OtherReferrals;
				$value->patient_id=$patient_id;
				$value->other_referrals_name=$other_reff;

				$value->save();
			}
			else
			{
				DB::table('m_other_referrals')
				->where('patient_id',$patient_id)
				->update(['other_referrals_name'=>$other_reff]);
			}
		}

		$file = $request->file('file');

		if($file != null)
		{
			$extension = $file->getClientOriginalExtension();
			Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));

			$medicalHistory = DB::table('m_medical_history')
			->where('patient_id',$patient_id);

			if($medicalHistory != null)
			{
				DB::table('m_medical_history')
				->where('id', $patient_med_id)
				->update(['medical_history' => $request->get('med_hist'),
					'mime' =>  $file->getClientMimeType(),
					'original_filename' =>  $file->getClientOriginalName(),
					/*					'filename' =>  $file->getFilename().'.'.$extension,*/
					'patient_id' => $patient_id
					]);
			}
			else
			{
				$medicalhistory = new MedicalHistory();
				$medicalhistory->medical_history = $request->get('med_hist');
				$medicalhistory->mime = $file->getClientMimeType();
				$medicalhistory->original_filename = $file->getClientOriginalName();
				/*				$medicalhistory->filename = $file->getFilename().'.'.$extension;*/
				$medicalhistory->patient_id = $request->get('pat_id');

				$medicalhistory->save();
			}	

		}

		//flash-message
		$request->session()->flash('alert-info', 'Patient Details was successfully updated!');

		return redirect()->action('MasterController@listViewPatient');
	}


	/********************** Treatment ************************/

	public function createTreatmentName()
	{
		$treatname_details = DB::table('s_treatment_type')
		->join('m_treatment_name','m_treatment_name.treatment_type_id','=','s_treatment_type.id')
		->select('*','s_treatment_type.name as treatment_type_name')
		->get();

		$treatmentTypes = DB::table('s_treatment_type')
		->get();

		return view('master.treatment.addTreatmentName',compact('treatmentTypes','treatname_details'));
	}

	public function storeTreatmentName(Request $request)
	{
		$materialsubtype = new TreatmentName;
		$materialsubtype->name=$request->get('treat_name');
		$materialsubtype->treatment_type_id	=$request->get('treatment_type_id');

		$materialsubtype->save();

		//flash-message
		$request->session()->flash('alert-success', 'Treatment Name was successfully added!');

		return redirect()->action('MasterController@createTreatmentName');
	}

	public function destroyTreatmentName(Request $request)
	{
		$treatname_id=$request->get('treat_name_id');

		DB::table('m_treatment_name')->where('id', $treatname_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Treatment Name was successfully deleted!');

		return redirect()->action('MasterController@createTreatmentName');
	}

	/********************** End Treatment ******************************/

	public function listViewMaterialManagement()
	{
		Session::forget('material_id');
		Session::forget('material_name');
		Session::forget('material_type');
		Session::forget('material_subtype');
		Session::forget('material_unit');
		Session::forget('materialSaftystock');

		$material_details = DB::table('m_material_details')
		->orderBy('m_material_details.id')
		->leftjoin('m_material_treatment', 'm_material_details.id', '=', 'm_material_treatment.material_name_id')
		->leftjoin('s_material_type', 's_material_type.id', '=', 'm_material_details.material_type_id')
		->leftjoin('s_material_subtype', 's_material_subtype.id', '=', 'm_material_details.material_subtype_id')
		->leftjoin('s_material_unit', 's_material_unit.id', '=', 'm_material_details.material_unit_id')
		->leftjoin('s_treatment_type', 's_treatment_type.id', '=', 'm_material_treatment.treatment_type_id')
		->leftjoin('m_treatment_name', 'm_treatment_name.id', '=', 'm_material_treatment.treatment_name_id')
		->select('*','m_material_details.id as material_name_id','s_material_type.name as material_type_name','s_material_subtype.name as material_subtype_name','s_material_unit.name as material_unit_name','s_treatment_type.name as treatment_type_name')
		->get();

		return view('master.inventory.materialmanagement.listViewMaterialManagement',compact('material_details'));
	}

	public function createMaterialManagementDetails(Request $request)
	{


		$materialId = DB::table('m_material_details')
		->orderBy('id','desc')
		->first();

		if($materialId == null)
		{
			$materialId = (object)array('id' => 0);
		}

		$materialId = $materialId->id + 1; 

		

		//array dropdown for material type
		$materialTypes = DB::table('s_material_type')
		->get();

		//array dropdown for material unit
		$materialUnit = DB::table('s_material_unit')
		->get();
		return view('master.inventory.materialmanagement.addMaterialDetails',compact('materialId','materialName','materialTypes','materialUnit'));
		


	}

	public function storeMaterialManagementDetails(Request $request)
	{
		$material_id=$request->get('mat_id');
		$material_name=$request->get('mat_nam');
		$material_type = $request->get('material_type_id');
		$material_subtype = $request->get('material_subtype_id');
		$material_unit = $request->get('material_unit_id');

			//for material type

		$mat_type=explode(",",$material_type);

		$mat_id=$mat_type[0];

		$mat_name=$mat_type[1];

			// for material sub type
		$mat_subtype=explode(",",$material_subtype);

		$mat_subType_id=$mat_subtype[0];

		$mat_subType=$mat_subtype[1];

			// for material unit
		$mat_unit=explode(",",$material_unit);

		$mat_unit_id=$mat_unit[0];

		$mat_unit_nmae=$mat_unit[1];

		session(['material_id'=>$material_id,
			'material_name'=>$material_name,
			'material_type'=>$material_type,
			'material_subtype'=>$material_subtype,
			'material_unit'=>$material_unit,
			'mat_id'=>$mat_id,
			'mat_name'=>$mat_name,
			'mat_subType'=>$mat_subType,
			'mat_subType_id'=>$mat_subType_id,
			'mat_unit_id'=>$mat_unit_id,
			'mat_unit_nmae'=>$mat_unit_nmae,
			]);

		return redirect()->action('MasterController@createMaterialManagementlAlert');
	}

	public function createMaterialManagementlAlert(Request $request)
	{
		if($request->session()->has('material_name') == true)
		{
			$materialId = null; 

			if($request->session()->has('id') == false)
			{
				$materialId = DB::table('m_material_details')
				->orderBy('id','desc')
				->first();

				if($materialId == null)
				{
					$materialId = (object)array('id' => 0);
				}

				$materialId = $materialId->id + 1; 
			}
			else
			{
				$materialId = Session::get('id');
				$materialName = Session::get('mat_nam');
			}

			return view('master.inventory.materialmanagement.addMaterialAlerts',compact('materialId','materialName'));
		}
		else
		{
			//flash-message
			$request->session()->flash('alert-danger', 'Material Name is required!');

			return redirect()->action('MasterController@createMaterialManagementDetails');
		}
	}

	public function storeMaterialManagementAlert(Request $request)
	{

		$materialSaftystock=$request->get('Safety_Stock');

		session(['materialSaftystock'=>$materialSaftystock,
			]);
		
		return redirect()->action('MasterController@createMaterialManagementTreatment');
	}

	public function createMaterialManagementTreatment(Request $request)
	{

		if($request->session()->has('material_name') == true)
		{
			$material_id =  Session::get('material_id');

			$treatmentType = DB::table('s_treatment_type')
			->get();

			return view('master.inventory.materialmanagement.addMaterialTreatment',compact('material_id','treatmentType'));
		}
		else
		{
				//flash-message
			$request->session()->flash('alert-danger', 'Material Name is required!');

			return redirect()->action('MasterController@createMaterialManagementDetails');
		}
	}

	public function storeMaterialManagementTreatment(Request $request)
	{

		$material_id =  Session::get('material_id');
		$material_name = Session::get('material_name');
		$material_type = Session::get('material_type');
		$material_subtype = Session::get('material_subtype');
		$material_unit = Session::get('material_unit');

		$materialSaftystock = Session::get('materialSaftystock');

		$MaterialDetails = new MaterialDetails;

		$MaterialDetails->material_name = $material_name;
		$MaterialDetails->material_type_id = $material_type;
		$MaterialDetails->material_subtype_id = $material_subtype;
		$MaterialDetails->material_unit_id = $material_unit;
		$MaterialDetails->save();

		$materialsaftystock = new MaterialSaftyStock;
		$materialsaftystock->material_name_id = $request->get('mat_id');
		$materialsaftystock->safety_stock_value = $materialSaftystock;
		$materialsaftystock->save();

		$materiatreatment = new MaterialTreatment;
		$materiatreatment->material_name_id = $request->get('mat_id');
		$materiatreatment->treatment_type_id = $request->get('treatment_type_id');
		$materiatreatment->treatment_name_id = $request->get('treatment_name_id');

		$materiatreatment->save();

		$request->session()->flash('alert-success', 'Material was successfully added!');

		return redirect()->action('MasterController@listViewMaterialManagement');
	}

	public function editMaterialManagementDetails($material_id,Request $request)
	{
		

		//array dropdown for material type
		$materialTypes = DB::table('s_material_type')
		->get();

		//array dropdown for material unit
		$materialUnit = DB::table('s_material_unit')
		->get();

		$material_details=DB::table('m_material_details')
		->where('m_material_details.id',$material_id)
		->leftjoin('s_material_type', 's_material_type.id', '=', 'm_material_details.material_type_id')
		->leftjoin('s_material_subtype', 's_material_subtype.id', '=', 'm_material_details.material_subtype_id')
		->leftjoin('s_material_unit', 's_material_unit.id', '=', 'm_material_details.material_unit_id')
		->select('*','m_material_details.id as material_name_id','s_material_type.name as material_type_name','s_material_subtype.name as material_subtype_name','s_material_unit.name as material_unit_name')
		->get();

		$material_detail = $material_details['0'];

		
		return view('master.inventory.materialmanagement.editMaterialDetails',compact('material_detail','materialTypes','materialUnit'));
	}

	

	public function updateMaterialManagementDetails(Request $request)
	{

		$material_id=$request->get('mat_id');
		$material_name=$request->get('mat_nam');
		$material_type = $request->get('material_type_id');
		$material_subtype = $request->get('material_subtype_id');
		$material_unit = $request->get('material_unit_id');
		

		$material_id=$request->get('mat_id');

		DB::table('m_material_details')
		->where('id', $material_id)
		->update(['material_name' => $request->get('mat_nam'),
			'material_type_id' => $request->get('material_type_id'),
			'material_subtype_id' => $request->get('material_subtype_id'),
			'material_unit_id' => $request->get('material_unit_id')
			]);
		

		$mat_type=explode(",",$material_type);

		$mat_id=$mat_type[0];

		$mat_name=$mat_type[1];

					// for material sub type
		$mat_subtype=explode(",",$material_subtype);

		$mat_subType_id=$mat_subtype[0];

		$mat_subType=$mat_subtype[1];

					// for material unit
		$mat_unit=explode(",",$material_unit);

		$mat_unit_id=$mat_unit[0];

		$mat_unit_nmae=$mat_unit[1];

		session(['material_id'=>$material_id,
			'material_name'=>$material_name,
			'material_type'=>$material_type,
			'material_subtype'=>$material_subtype,
			'material_unit'=>$material_unit,
			'mat_id'=>$mat_id,
			'mat_name'=>$mat_name,
			'mat_subType'=>$mat_subType,
			'mat_subType_id'=>$mat_subType_id,
			'mat_unit_id'=>$mat_unit_id,
			'mat_unit_nmae'=>$mat_unit_nmae,

			]);
		return redirect()->action('MasterController@editMaterialManagementAlert',[$material_id]);

	}

	public function editMaterialManagementAlert($material_id,Request $request)
	{
		$Safty_stocks = DB::table('m_material_alerts')
		->where('material_name_id',$material_id)
		->get();

		return view('master.inventory.materialmanagement.editMaterialAlerts',compact('Safty_stocks','material_id'));
		
	}

	public function updateMaterialManagementAlert(Request $request)
	{
		$materialSaftystock = $request->get('Safety_Stock');
		
		session(['materialSaftystock'=>$materialSaftystock,
			]);
		$material_id=$request->get('mat_id');

		DB::table('m_material_alerts')
		->where('material_name_id',$material_id)

		->update(['safety_stock_value' => $request->get('Safety_Stock'),
			]);
		return redirect()->action('MasterController@editMaterialManagementTreatment',[$material_id]);
	}

	public function editMaterialManagementTreatment($material_id,Request $request)
	{
		$treatment_details = DB::table('m_material_treatment')
		->where('m_material_treatment.material_name_id',$material_id)
		->leftjoin('s_treatment_type', 's_treatment_type.id', '=', 'm_material_treatment.treatment_type_id')
		->leftjoin('m_treatment_name', 'm_treatment_name.id', '=', 'm_material_treatment.treatment_name_id')
		->select('*','s_treatment_type.name as treatment_type_name','m_material_treatment.id as treatment_id')
		->get();

		$treatment_detail = $treatment_details['0'];

		$treatmentType = DB::table('s_treatment_type')
		->get();

		return view('master.inventory.materialmanagement.editMaterialTreatment',compact('treatment_detail','treatmentType'));
	}

	public function updateMaterialManagementTreatment(Request $request)
	{
		$treatment_id=$request->get('treatment_id');

		DB::table('m_material_treatment')
		->where('id', $treatment_id)
		->update(['material_name_id' => $request->get('mat_id'),
			'treatment_type_id' => $request->get('treatment_type_id'),
			'treatment_name_id' => $request->get('treatment_name_id')
			]);

		$request->session()->flash('alert-info', 'Material was successfully updated!');

		return redirect()->action('MasterController@listViewMaterialManagement');
	}

	public function listViewInstrumentManagement()
	{

		Session::forget('instrument_id');
		Session::forget('instrument_name');
		Session::forget('instrument_type');
		Session::forget('instrument_subtype');
		Session::forget('instrument_unit');
		Session::forget('instrumentSaftystock');

		$instrument_details = DB::table('m_instrument_details')
		->orderBy('m_instrument_details.id')
		->join('m_instrument_treatment', 'm_instrument_details.id', '=', 'm_instrument_treatment.instrument_name_id')
		->join('s_instrument_type', 's_instrument_type.id', '=', 'm_instrument_details.instrument_type_id')
		->join('s_instrument_subtype', 's_instrument_subtype.id', '=', 'm_instrument_details.instrument_subtype_id')
		->join('s_instrument_unit', 's_instrument_unit.id', '=', 'm_instrument_details.instrument_unit_id')
		->join('s_treatment_type', 's_treatment_type.id', '=', 'm_instrument_treatment.treatment_type_id')
		->join('m_treatment_name', 'm_treatment_name.id', '=', 'm_instrument_treatment.treatment_name_id')
		->select('*','m_instrument_details.id as instrument_name_id','s_instrument_type.name as instrument_type_name','s_instrument_subtype.name as instrument_subtype_name','s_instrument_unit.name as instrument_unit_name','s_treatment_type.name as treatment_type_name')
		->get();

		return view('master.inventory.instrumentmanagement.listViewInstrumentManagement',compact('instrument_details'));
	}

	public function createInstrumentManagementDetails(Request $request)
	{
		
		$instrument_id = DB::table('m_instrument_details')
		->orderBy('id','desc')
		->first();

		if($instrument_id == null)
		{
			$instrument_id = (object)array('id' => 0);
		}

		$instrument_id = $instrument_id->id + 1; 

			//array dropdown for material type
		$instrumentTypes = DB::table('s_instrument_type')
		->get();

		//array dropdown for material unit
		$instrumentUnit = DB::table('s_instrument_unit')
		->get();


		return view('master.inventory.instrumentmanagement.addInstrumentDetails',compact('instrument_id','instrument_name','instrumentTypes','instrumentUnit'));

	}

	public function storeInstrumentManagementDetails(Request $request)
	{
		$instrument_id = $request->get('inst_id');
		$instrument_name = $request->get('inst_nam');
		$instrument_type = $request->get('instrument_type_id');
		$instrument_subtype = $request->get('instrument_subtype_id');
		$instrument_unit = $request->get('instrument_unit_id');

			//for instrument type
		$inst_type=explode(",",$instrument_type);

		$inst_type_id=$inst_type[0];


		$inst_type_name=$inst_type[1];

			//for instrument subtype
		$inst_subtype=explode(",",$instrument_subtype);

		$inst_subt_id=$inst_subtype[0];


		$inst_subt_name=$inst_subtype[1];

			//for instrument unit
		$inst_unit=explode(",",$instrument_unit);

		$inst_unit_id=$inst_unit[0];


		$inst_unit_name=$inst_unit[1];



		session([
			'instrument_id'=>$instrument_id,
			'instrument_name'=>$instrument_name,
			'instrument_type'=>$instrument_type,
			'instrument_subtype'=>$instrument_subtype,
			'instrument_unit'=>$instrument_unit,
			'inst_type_id'=>$inst_type_id,
			'inst_type_name'=>$inst_type_name,
			'inst_subt_id'=>$inst_subt_id,
			'inst_subt_name'=>$inst_subt_name,
			'inst_unit_id'=>$inst_unit_id,
			'inst_unit_name'=>$inst_unit_name,
			]);

		return redirect()->action('MasterController@createInstrumentManagementAlert');
	}

	public function createInstrumentManagementAlert(Request $request){
		if($request->session()->has('instrument_name') == true)
		{
			$instrumentId = null; 

			if($request->session()->has('id') == false)
			{
				$instrumentId = DB::table('m_instrument_details')
				->orderBy('id','desc')
				->first();

				if($instrumentId == null)
				{
					$instrumentId = (object)array('id' => 0);
				}

				$instrumentId = $instrumentId->id + 1; 
			}
			else
			{
				$instrumentId = Session::get('id');
				$instrument_name = Session::get('inst_nam');
			}
			return view('master.inventory.instrumentmanagement.addInstrumentAlerts',compact('instrumentId','instrument_name'));

		}
		else
		{
					//flash-message
			$request->session()->flash('alert-danger', 'Instrument Name is required!');

			return redirect()->action('MasterController@createInstrumentManagementDetails');
		}
	}

	public function storeInstrumentManagementAlert(Request $request)
	{

		$instrumentSaftystock=$request->get('Safety_Stock');

		session(['instrumentSaftystock'=>$instrumentSaftystock,
			]);
		return redirect()->action('MasterController@createInstrumentManagementTreatment');
	}

	public function createInstrumentManagementTreatment(Request $request)
	{

		if($request->session()->has('instrument_name') == true)
		{
			$instrument_id =  Session::get('instrument_id');

			$treatmentType = DB::table('s_treatment_type')
			->get();

			return view('master.inventory.instrumentmanagement.addInstrumentTreatment',compact('instrument_id','treatmentType'));
		}
		else
		{
				//flash-message
			$request->session()->flash('alert-danger', 'Instrument Name is required!');

			return redirect()->action('MasterController@createInstrumentManagementDetails');
		}
	}

	public function storeInstrumentManagementTreatment(Request $request)
	{

		$instrument_id =  Session::get('instrument_id');
		$instrument_name = Session::get('instrument_name');
		$instrument_type = Session::get('instrument_type');
		$instrument_subtype = Session::get('instrument_subtype');
		$instrument_unit = Session::get('instrument_unit');

		$instrumentSaftystock = Session::get('instrumentSaftystock');

		$InstrumentDetails = new InstrumentDetails;
		$InstrumentDetails->instrument_name = $instrument_name;
		$InstrumentDetails->instrument_type_id = $instrument_type;
		$InstrumentDetails->instrument_subtype_id = $instrument_subtype;
		$InstrumentDetails->instrument_unit_id = $instrument_unit;
		$InstrumentDetails->save();

		$instrumentsafetystock = new InstrumentSafetyStock;
		$instrumentsafetystock->instrument_name_id = $request->get('inst_id');
		$instrumentsafetystock->safety_stock_value = $instrumentSaftystock;
		$instrumentsafetystock->save();

		$instrumentmanagement = new InstrumentTreatment;
		$instrumentmanagement->instrument_name_id = $request->get('inst_id');
		$instrumentmanagement->treatment_type_id = $request->get('treatment_type_id');
		$instrumentmanagement->treatment_name_id = $request->get('treatment_name_id');
		$instrumentmanagement->save();

		$request->session()->flash('alert-success', 'Instrument was successfully added!');

		return redirect()->action('MasterController@listViewInstrumentManagement');
	}

	public function editInstrumentManagementDetails($instrument_id,Request $request)
	{
		//array dropdown for material type
		$instrumentTypes = DB::table('s_instrument_type')
		->get();

		//array dropdown for material unit
		$instrumentUnit = DB::table('s_instrument_unit')
		->get();

		$instrument_details=DB::table('m_instrument_details')
		->where('m_instrument_details.id',$instrument_id)
		->leftjoin('s_instrument_type', 's_instrument_type.id', '=', 'm_instrument_details.instrument_type_id')
		->leftjoin('s_instrument_subtype', 's_instrument_subtype.id', '=', 'm_instrument_details.instrument_subtype_id')
		->leftjoin('s_instrument_unit', 's_instrument_unit.id', '=', 'm_instrument_details.instrument_unit_id')
		->select('*','m_instrument_details.id as instrument_name_id','s_instrument_type.name as instrument_type_name','s_instrument_subtype.name as instrument_subtype_name','s_instrument_unit.name as instrument_unit_name')
		->get();

		$instrument_detail = $instrument_details['0'];

		return view('master.inventory.instrumentmanagement.editInstrumentDetails',compact('instrument_id','instrument_detail','instrumentTypes','instrumentUnit'));
	}

	public function updateInstrumentManagementDetails(Request $request)
	{
		$instrument_id = $request->get('inst_id');
		$instrument_name = $request->get('instrument_name');
		$instrument_type = $request->get('instrument_type_id');
		$instrument_subtype = $request->get('instrument_subtype_id');
		$instrument_unit = $request->get('instrument_unit_id');

		DB::table('m_instrument_details')
		->where('id', $instrument_id)
		->update(['instrument_name' => $request->get('instrument_name'),
			'instrument_type_id' => $request->get('instrument_type_id'),
			'instrument_subtype_id' => $request->get('instrument_subtype_id'),
			'instrument_unit_id' => $request->get('instrument_unit_id')
			]);

			//for instrument type
		$inst_type=explode(",",$instrument_type);

		$inst_type_id=$inst_type[0];


		$inst_type_name=$inst_type[1];

			//for instrument subtype
		$inst_subtype=explode(",",$instrument_subtype);

		$inst_subt_id=$inst_subtype[0];


		$inst_subt_name=$inst_subtype[1];

			//for instrument unit
		$inst_unit=explode(",",$instrument_unit);

		$inst_unit_id=$inst_unit[0];


		$inst_unit_name=$inst_unit[1];



		session([
			'instrument_id'=>$instrument_id,
			'instrument_name'=>$instrument_name,
			'instrument_type'=>$instrument_type,
			'instrument_subtype'=>$instrument_subtype,
			'instrument_unit'=>$instrument_unit,
			'inst_type_id'=>$inst_type_id,
			'inst_type_name'=>$inst_type_name,
			'inst_subt_id'=>$inst_subt_id,
			'inst_subt_name'=>$inst_subt_name,
			'inst_unit_id'=>$inst_unit_id,
			'inst_unit_name'=>$inst_unit_name,
			]);

		
		return redirect()->action('MasterController@editInstrumentManagementAlert',[$instrument_id]);
	}

	public function editInstrumentManagementAlert($instrument_id,Request $request)
	{
		$instrumentSaftystock = $request->get('Safety_Stock');

		$Safty_stocks = DB::table('m_instrument_alerts')
		->where('instrument_name_id',$instrument_id)
		->get();

		return view('master.inventory.instrumentmanagement.editInstrumentAlerts',compact('Safty_stocks','instrument_id'));
	}

	public function updateInstrumentManagementAlert(Request $request)
	{
		$instrumentSaftystock = $request->get('Safety_Stock');

		session(['instrumentSaftystock'=>$instrumentSaftystock,
			]);

		$instrument_id=$request->get('inst_id');

		DB::table('m_instrument_alerts')
		->where('instrument_name_id',$instrument_id)

		->update(['safety_stock_value' => $request->get('Safety_Stock'),
			]);

		return redirect()->action('MasterController@editInstrumentManagementTreatment',[$instrument_id]);
		
	}

	public function editInstrumentManagementTreatment($instrument_id,Request $request)	
	{
		$treatment_details = DB::table('m_instrument_treatment')
		->where('m_instrument_treatment.instrument_name_id',$instrument_id)
		->leftjoin('s_treatment_type', 's_treatment_type.id', '=', 'm_instrument_treatment.treatment_type_id')
		->leftjoin('m_treatment_name', 'm_treatment_name.id', '=', 'm_instrument_treatment.treatment_name_id')
		->select('*','s_treatment_type.name as treatment_type_name','m_instrument_treatment.id as treatment_id')
		->get();

		$treatment_detail = $treatment_details['0'];

		$treatmentType = DB::table('s_treatment_type')
		->get();

		return view('master.inventory.instrumentmanagement.editInstrumentTreatment',compact('treatment_detail','treatmentType'));

	}

	public function updateInstrumentManagementTreatment(Request $request)
	{
		$treatment_id=$request->get('treatment_id');

		DB::table('m_instrument_treatment')
		->where('id', $treatment_id)
		->update(['instrument_name_id' => $request->get('inst_id'),
			'treatment_type_id' => $request->get('treatment_type_id'),
			'treatment_name_id' => $request->get('treatment_name_id')
			]);

		$request->session()->flash('alert-info', 'Instrument was successfully updated!');

		return redirect()->action('MasterController@listViewInstrumentManagement');
	}

	public function listViewMachineManagement()
	{
		Session::forget('machine_id');
		Session::forget('machine_name');
		Session::forget('machine_type_id');
		Session::forget('machine_subtype_id');

		$machine_details = DB::table('m_machine_details')
		->orderBy('m_machine_details.id')
		->leftjoin('m_machine_treatment', 'm_machine_details.id', '=', 'm_machine_treatment.machine_name_id')
		->leftjoin('s_machine_type', 's_machine_type.id', '=', 'm_machine_details.machine_type_id')
		->leftjoin('s_machine_subtype', 's_machine_subtype.id', '=', 'm_machine_details.machine_subtype_id')
		->leftjoin('s_treatment_type', 's_treatment_type.id', '=', 'm_machine_treatment.treatment_type_id')
		->leftjoin('m_treatment_name', 'm_treatment_name.id', '=', 'm_machine_treatment.treatment_name_id')
		->select('*','m_machine_details.id as machine_name_id','s_machine_type.name as machine_type_name','s_machine_subtype.name as machine_subtype_name','s_treatment_type.name as treatment_type_name')
		->get();

		return view('master.inventory.machinemanagement.listViewMachineManagement',compact('machine_details'));
	}

	public function createMachineManagementDetails()
	{
		//array dropdown for material type
		$machineTypes = DB::table('s_machine_type')
		->orderBy('id','desc')
		->get();

		return view('master.inventory.machinemanagement.addMachineDetails',compact('machineTypes'));

	}

	public function storeMachineManagementDetails(Request $request)
	{
		$machine_id = $request->get('machine_id');
		$machine_name = $request->get('machine_name');
		$machine_type_id = $request->get('machine_type_id');
		$machine_subtype_id = $request->get('machine_subtype_id');

		$machine_type=explode(",",$machine_type_id);

		$machineType_id=$machine_type[0];


		$machine_Name=$machine_type[1];

		$machine_Subtype=explode(",",$machine_subtype_id);

		$machineSubtype_id=$machine_Subtype[0];


		$machineSubtype_name=$machine_Subtype[1];

		session([
			'machine_name'=>$machine_name,
			'machine_type_id'=>$machine_type_id,
			'machine_subtype_id'=>$machine_subtype_id,
			'machineType_id'=>$machineType_id,
			'machine_Name'=>$machine_Name,
			'machineSubtype_id'=>$machineSubtype_id,
			'machineSubtype_name'=>$machineSubtype_name,
			]);

		return redirect()->action('MasterController@createMachineManagementTreatment');
	}

	public function createMachineManagementTreatment(Request $request)
	{
		if($request->session()->has('machine_name') == true)
		{
			$machine_id = null; 
			if($request->session()->has('id') == false)
			{
				$machine_id = DB::table('m_machine_details')
				->orderBy('id', 'desc')
				->first();
				if($machine_id == null)
				{
					$machine_id = (object)array('id' => 0);
				}

				$machine_id = $machine_id->id + 1; 

				$treatmentType = DB::table('s_treatment_type')
				->get();
			}
			else
			{
				$machine_id = Session::get('id');
				$material_name = Session::get('machine_name');
			}

			return view('master.inventory.machinemanagement.addMachineTreatment',compact('machine_id','treatmentType'));
		}
		else
		{
			//flash-message
			$request->session()->flash('alert-danger', 'Machine Name is required!');

			return redirect()->action('MasterController@createMachineManagementDetails');
		}
	}

	public function storeMachineManagementTreatment(Request $request)
	{	
		$machine_id =  Session::get('machine_id');
		$machine_name = Session::get('machine_name');
		$machine_type_id = Session::get('machine_type_id');
		$machine_subtype_id = Session::get('machine_subtype_id');
		
		$MachineDetails = new MachineDetails;
		$MachineDetails->machine_name = $machine_name;
		$MachineDetails->machine_type_id = $machine_type_id;
		$MachineDetails->machine_subtype_id = $machine_subtype_id;
		$MachineDetails->save();

		$machinemanagement = new MachineTreatment;
		$machinemanagement->machine_name_id = $request->get('mch_id');
		$machinemanagement->treatment_type_id = $request->get('treatment_type_id');
		$machinemanagement->treatment_name_id = $request->get('treatment_name_id');
		$machinemanagement->save();

		$request->session()->flash('alert-success', 'Machine was successfully added!');

		return redirect()->action('MasterController@listViewMachineManagement');
	}

	public function editMachineManagementDetails($machine_id,Request $request)
	{

		//array dropdown for material type
		$machineTypes = DB::table('s_machine_type')
		->get();

		$machine_details=DB::table('m_machine_details')
		->where('m_machine_details.id',$machine_id)
		->leftjoin('s_machine_type', 's_machine_type.id', '=', 'm_machine_details.machine_type_id')
		->leftjoin('s_machine_subtype', 's_machine_subtype.id', '=', 'm_machine_details.machine_subtype_id')
		->select('*','m_machine_details.id as machine_name_id','s_machine_type.name as machine_type_name','s_machine_subtype.name as machine_subtype_name')
		->get();

		$machine_detail = $machine_details['0'];

		return view('master.inventory.machinemanagement.editMachineDetails',compact('machine_detail','machineTypes'));
	}

	public function updateMachineManagementDetails(Request $request)
	{

		$machine_id = $request->get('mch_id');
		$machine_name = $request->get('machine_name');
		$machine_type_id = $request->get('machine_type_id');
		$machine_subtype_id = $request->get('machine_subtype_id');

		$machine_type=explode(",",$machine_type_id);

		$machineType_id=$machine_type[0];


		$machine_Name=$machine_type[1];

		$machine_Subtype=explode(",",$machine_subtype_id);

		$machineSubtype_id=$machine_Subtype[0];


		$machineSubtype_name=$machine_Subtype[1];

		session([
			'machine_id'=>$machine_id,
			'machine_name'=>$machine_name,
			'machine_type_id'=>$machine_type_id,
			'machine_subtype_id'=>$machine_subtype_id,
			'machineType_id'=>$machineType_id,
			'machine_Name'=>$machine_Name,
			'machineSubtype_id'=>$machineSubtype_id,
			'machineSubtype_name'=>$machineSubtype_name,
			]);

		DB::table('m_machine_details')
		->where('id', $machine_id)
		->update(['machine_name' => $request->get('machine_name'),
			'machine_type_id' => $request->get('machine_type_id'),
			'machine_subtype_id' => $request->get('machine_subtype_id')
			]);

		return redirect()->action('MasterController@editMachineManagementTreatment',[$machine_id]);
	}

	public function editMachineManagementTreatment($machine_id,Request $request)
	{
		$treatment_details = DB::table('m_machine_treatment')
		->where('m_machine_treatment.machine_name_id',$machine_id)
		->leftjoin('s_treatment_type', 's_treatment_type.id', '=', 'm_machine_treatment.treatment_type_id')
		->leftjoin('m_treatment_name', 'm_treatment_name.id', '=', 'm_machine_treatment.treatment_name_id')
		->select('*','s_treatment_type.name as treatment_type_name','m_machine_treatment.id as treatment_id')
		->get();

		$treatment_detail = $treatment_details['0'];
		$treatmentType = DB::table('s_treatment_type')
		->get();

		return view('master.inventory.machinemanagement.editMachineTreatment',compact('treatment_detail','treatmentType','machine_id'));
	}

	public function updateMachineManagementTreatment(Request $request)
	{
		$treatment_id=$request->get('treatment_id');

		DB::table('m_machine_treatment')
		->where('id', $treatment_id)
		->update(['machine_name_id' => $request->get('mch_id'),
			'treatment_type_id' => $request->get('treatment_type_id'),
			'treatment_name_id' => $request->get('treatment_name_id')
			]);

		$request->session()->flash('alert-info', 'Machine was successfully updated!');

		return redirect()->action('MasterController@listViewMachineManagement');
	}

	public function listViewGadgetManagement()
	{
		Session::forget('gadget_id');
		Session::forget('gadget_name');
		Session::forget('gadget_type_id');
		Session::forget('gadget_subtype_id');

		$gadget_details = DB::table('m_gadget_details')
		->orderBy('m_gadget_details.id')
		->join('m_gadget_treatment', 'm_gadget_details.id', '=', 'm_gadget_treatment.gadget_name_id')
		->join('s_gadget_type', 's_gadget_type.id', '=', 'm_gadget_details.gadget_type_id')
		->join('s_gadget_subtype', 's_gadget_subtype.id', '=', 'm_gadget_details.gadget_subtype_id')
		->join('s_treatment_type', 's_treatment_type.id', '=', 'm_gadget_treatment.treatment_type_id')
		->join('m_treatment_name', 'm_treatment_name.id', '=', 'm_gadget_treatment.treatment_name_id')
		->select('*','m_gadget_details.id as gadget_name_id','s_gadget_type.name as gadget_type_name','s_gadget_subtype.name as gadget_subtype_name','s_treatment_type.name as treatment_type_name')
		->get();

		return view('master.inventory.gadgetmanagement.listViewGadgetManagement',compact('gadget_details'));

	}

	public function createGadgetManagementDetails()
	{
		//array dropdown for material type
		$gadgetTypes = DB::table('s_gadget_type')
		->get();

		return view('master.inventory.gadgetmanagement.addGadgetDetails',compact('gadgetTypes'));

	}

	public function storeGadgetManagementDetails(Request $request)
	{

		$gadget_id = $request->get('gadget_id');
		$gadget_name = $request->get('gadget_name');
		$gadget_type_id = $request->get('gadget_type_id');
		$gadget_subtype_id = $request->get('gadget_subtype_id');

		$gadget_type=explode(",",$gadget_type_id);

		$gadgetType_id=$gadget_type[0];

		$gadgetType_name=$gadget_type[1];

		$gadget_subtype=explode(",",$gadget_subtype_id);

		$gadgetSubtype_id=$gadget_subtype[0];

		$gadgetSubtype_name=$gadget_subtype[1];

		session([
			'gadget_name'=>$gadget_name,
			'gadget_type_id'=>$gadget_type_id,
			'gadget_subtype_id'=>$gadget_subtype_id,
			'gadgetType_id'=>$gadgetType_id,
			'gadgetType_name'=>$gadgetType_name,
			'gadgetSubtype_id'=>$gadgetSubtype_id,
			'gadgetSubtype_name'=>$gadgetSubtype_name,
			]);

		return redirect()->action('MasterController@createGadgetManagementTreatment');
	}

	public function createGadgetManagementTreatment(Request $request)
	{
		if($request->session()->has('gadget_name') == true)
		{
			$gadget_id = null; 
			if($request->session()->has('id') == false)
			{
				$gadget_id = DB::table('m_gadget_details')
				->orderBy('id', 'desc')
				->first();

				if($gadget_id == null)
				{
					$gadget_id = (object)array('id' => 0);
				}
				$gadget_id = $gadget_id->id + 1; 

				$treatmentType = DB::table('s_treatment_type')
				->get();
			}
			else
			{
				$gadget_id = Session::get('id');
				$gadget_name = Session::get('gadget_name');

				$treatmentType = DB::table('s_treatment_type')
				->get();
			}

			return view('master.inventory.gadgetmanagement.addGadgetTreatment',compact('gadget_id','treatmentType'));

		}
		else
		{
			//flash-message
			$request->session()->flash('alert-danger', 'Material Name is required!');

			return redirect()->action('MasterController@createGadgetManagementDetails');
		}
	}

	public function storeGadgetManagementTreatment(Request $request)
	{
		$gadget_id =  Session::get('gadget_id');
		$gadget_name = Session::get('gadget_name');
		$gadget_type_id = Session::get('gadget_type_id');
		$gadget_subtype_id = Session::get('gadget_subtype_id');

		$gadget_details = new GadgetDetails;

		$gadget_details->gadget_name = $gadget_name;
		$gadget_details->gadget_type_id = $gadget_type_id;
		$gadget_details->gadget_subtype_id = $gadget_subtype_id;

		$gadget_details->save();

		$gadgetmanagement = new GadgetTreatment;
		$gadgetmanagement->gadget_name_id = $request->get('gdgt_id');
		$gadgetmanagement->treatment_type_id = $request->get('treatment_type_id');
		$gadgetmanagement->treatment_name_id = $request->get('treatment_name_id');

		$gadgetmanagement->save();

		$request->session()->flash('alert-success', 'Gadget was successfully added!');

		return redirect()->action('MasterController@listViewGadgetManagement');
	}

	public function editGadgetManagementDetails($gadget_id,Request $request)
	{
		//array dropdown for material type
		$gadgetTypes = DB::table('s_gadget_type')
		->get();

		$gadget_details=DB::table('m_gadget_details')
		->where('m_gadget_details.id',$gadget_id)
		->join('s_gadget_type', 's_gadget_type.id', '=', 'm_gadget_details.gadget_type_id')
		->join('s_gadget_subtype', 's_gadget_subtype.id', '=', 'm_gadget_details.gadget_subtype_id')
		->select('*','m_gadget_details.id as gadget_name_id','s_gadget_type.name as gadget_type_name','s_gadget_subtype.name as gadget_subtype_name')
		->get();

		$gadget_detail = $gadget_details['0'];

		return view('master.inventory.gadgetmanagement.editGadgetDetails',compact('gadget_detail','gadgetTypes'));
	}

	public function updateGadgetManagementDetails(Request $request)
	{

		$gadget_id = $request->get('gadget_id');
		$gadget_name = $request->get('gadget_name');
		$gadget_type_id = $request->get('gadget_type_id');
		$gadget_subtype_id = $request->get('gadget_subtype_id');

		$gadget_id=$request->get('gdgt_id');

		$gadget_type=explode(",",$gadget_type_id);

		$gadgetType_id=$gadget_type[0];

		$gadgetType_name=$gadget_type[1];

		$gadget_subtype=explode(",",$gadget_subtype_id);

		$gadgetSubtype_id=$gadget_subtype[0];

		$gadgetSubtype_name=$gadget_subtype[1];

		session([
			'gadget_id'=>$gadget_id,
			'gadget_name'=>$gadget_name,
			'gadget_type_id'=>$gadget_type_id,
			'gadget_subtype_id'=>$gadget_subtype_id,
			'gadgetType_id'=>$gadgetType_id,
			'gadgetType_name'=>$gadgetType_name,
			'gadgetSubtype_id'=>$gadgetSubtype_id,
			'gadgetSubtype_name'=>$gadgetSubtype_name,
			]);
		

		DB::table('m_gadget_details')
		->where('id', $gadget_id)
		->update(['gadget_name' => $request->get('gadget_name'),
			'gadget_type_id' => $request->get('gadget_type_id'),
			'gadget_subtype_id' => $request->get('gadget_subtype_id')
			]);

		return redirect()->action('MasterController@editGadgetManagementTreatment',[$gadget_id]);
	}

	public function editGadgetManagementTreatment($gadget_id,Request $request)
	{

		$treatment_details = DB::table('m_gadget_treatment')
		->where('m_gadget_treatment.gadget_name_id',$gadget_id)
		->join('s_treatment_type', 's_treatment_type.id', '=', 'm_gadget_treatment.treatment_type_id')
		->join('m_treatment_name', 'm_treatment_name.id', '=', 'm_gadget_treatment.treatment_name_id')
		->select('*','s_treatment_type.name as treatment_type_name','m_gadget_treatment.id as treatment_id')
		->get();

		$treatment_detail = $treatment_details['0'];

		$treatmentType = DB::table('s_treatment_type')
		->get();

		return view('master.inventory.gadgetmanagement.editGadgetTreatment',compact('treatment_detail','treatmentType'));
	}

	public function updateGadgetManagementTreatment(Request $request)
	{
		$treatment_id=$request->get('treatment_id');

		DB::table('m_gadget_treatment')
		->where('id', $treatment_id)
		->update(['gadget_name_id' => $request->get('gdgt_id'),
			'treatment_type_id' => $request->get('treatment_type_id'),
			'treatment_name_id' => $request->get('treatment_name_id')
			]);

		$request->session()->flash('alert-info', 'Gadget was successfully updated!');

		return redirect()->action('MasterController@listViewGadgetManagement');
	}

	/********************** Vendor ************************/

	public function listViewVendorLab()
	{
		Session::forget('id');
		Session::forget('name');
		Session::forget('address');
		Session::forget('email');
		Session::forget('cell');
		Session::forget('contact_person');
/*
		$vendor_lab_details = DB::table('m_vendor_lab_details')
		->join('m_vendor_lab_workspec_details','m_vendor_lab_workspec_details.vendor_lab_id','=','m_vendor_lab_details.id')
		->select('name','m_vendor_lab_details.id as id')
		->distinct()
		->get();
*/
		$vendor_lab_details = DB::table('m_vendor_lab_workspec_details')
		->join('s_lab_work_type','m_vendor_lab_workspec_details.work_type_id','=','s_lab_work_type.id')
		->join('s_lab_work_subtype','m_vendor_lab_workspec_details.work_subtype_id','=','s_lab_work_subtype.id')
		->join('s_lab_work_name','m_vendor_lab_workspec_details.work_name_id','=','s_lab_work_name.id')
		->join('m_vendor_lab_details','m_vendor_lab_workspec_details.vendor_lab_id','=','m_vendor_lab_details.id')
		->select('*','s_lab_work_type.name as lab_work_type_name','s_lab_work_subtype.name as lab_work_subtype_name','s_lab_work_name.name as lab_work_name','m_vendor_lab_workspec_details.id as lab_workspec_id')
		->get();

		return view('master.vendors.lab.listViewVendorLab',compact('vendor_lab_details'));
	}

	public function createVendorLab()
	{
		$vendor_type = DB::table('s_vendor_types')
		->where('name','Lab')
		->get();

		$vendorLabID = DB::table('m_vendor_lab_details')
		->orderBy('id','desc')
		->first();

		if($vendorLabID == null)
		{
			$vendorLabID = (object)array('id' => 0);
		}


		$vendor_type = $vendor_type['0'];

		return view('master.vendors.lab.addVendorLab',compact('vendor_type','vendorLabID'));
	}

	public function storeVendorLab(Request $request)
	{
		
		$id = $request->get('vendor_lab_id'); 
		$name=$request->get('lab_name');
		$address =$request->get('address');
		$contact_person=$request->get('contact_person');
		$cell_no=$request->get('cell');
		$email=$request->get('email');
		$vendor_type_id=$request->get('vendor_type_id');

		session(['id' => $id,
			'name' => $name,
			'contact_person' => $contact_person,
			'address'=>$address,
			'cell'=>$cell_no,
			'email'=>$email,
			'vendor_type_id'=>$vendor_type_id]);

		return redirect()->action('MasterController@createVendorLabWorkSpecification');
	}

	public function createVendorLabWorkSpecification(Request $request)
	{

		if($request->session()->has('name') == true)
		{
			$vendorLabID = null; 

			if($request->session()->has('id') == false)
			{
				
				$vendorLabID = DB::table('m_vendor_lab_details')
				->orderBy('id','desc')
				->first();

				if($vendorLabID == null)
				{
					$vendorLabID = (object)array('id' => 0);
				}

				$vendorLabID = $vendorLabID->id + 1; 
			}
			else
			{
				$vendorLabID = Session::get('id');
				$VendorLabName = Session::get('name');

			}
			
			$vendor_type_id = Session::get('vendor_type_id');

			$workType = DB::table('s_lab_work_type')
			->get();

			return view('master.vendors.lab.addWorkSpecificationLab',compact('VendorLabName','workType','vendorLabID','vendor_lab_id','vendor_type_id'));
		}
		else
		{
			//flash-message
			$request->session()->flash('alert-danger', 'Lab Name is required!');

			return redirect()->action('MasterController@createVendorLab');
		}
	}


	public function storeVendorLabWorkSpecification(Request $request)
	{

		$id  = $request->get('vendor_lab_id');

		$work_type_id=$request->get('work_type_id');
		$work_subtype_id=$request->get('work_subtype_id');
		$work_name_id=$request->get('work_name_id');
		$rate=$request->get('rate');
		$duration=$request->get('duration');
		$vendor_type_id=$request->get('vendor_type_id');
		$vendor_lab_id=$request->get('vendor_lab_id');

		$name  = Session::get('name');
		$address  = Session::get('address');
		$contact_person  = Session::get('contact_person');
		$cell  = Session::get('cell');
		$email  = Session::get('email');

		$vendordetails= new VendorLabDetails;
		$vendordetails->name=$name;
		$vendordetails->address	=$address;
		$vendordetails->contact_person	=$contact_person;
		$vendordetails->cell_no	=$cell;
		$vendordetails->email=$email;
		$vendordetails->vendor_type_id=$request->get('vendor_type_id');
		$vendordetails->save();

		foreach ($work_type_id as $key => $work_type) {

			$labvendorspecification= new LabVendorSpecification;
			$labvendorspecification->rate=$rate[$key];
			$labvendorspecification->duration=$duration[$key];
			$labvendorspecification->vendor_type_id=$vendor_type_id;
			$labvendorspecification->work_type_id=$work_type;
			$labvendorspecification->work_subtype_id=$work_subtype_id[$key];
			$labvendorspecification->work_name_id=$work_name_id[$key];
			$labvendorspecification->vendor_lab_id=$vendor_lab_id;
			$labvendorspecification->save();

		}

		//flash-message
		$request->session()->flash('alert-success', 'Lab Vendor was successfully added!');

		return redirect()->action('MasterController@listViewVendorLab');
	}

	public function editVendorLab($vendorLabID,Request $request)
	{
		$vendor_lab_detail = DB::table('m_vendor_lab_workspec_details')
		->join('s_vendor_types','m_vendor_lab_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_vendor_lab_details','m_vendor_lab_details.id','=','m_vendor_lab_workspec_details.vendor_lab_id')
		->where('m_vendor_lab_details.id',$vendorLabID)
		->select('*','m_vendor_lab_workspec_details.id as m_vendor_lab_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		$vendor_lab_details = $vendor_lab_detail['0'];

		return view('master.vendors.lab.editVendorLab', compact('vendor_lab_details'));
	}

	public function updateVendorLab(Request $request)
	{
		
		$id  = $request->get('vendorLabID'); 
		$name=$request->get('lab_name');
		$address =$request->get('address');
		$contact_person=$request->get('contact_person');
		$cell_no=$request->get('cell');
		$email=$request->get('email');
		$vendor_type_id=$request->get('vendor_type_id');


		session(['id' => $id,
			'name' => $name,
			'contact_person' => $contact_person,
			'address'=>$address,
			'cell'=>$cell_no,
			'email'=>$email,
			'vendor_type_id'=>$vendor_type_id,
			]);

		return redirect()->action('MasterController@editVendorLabWorkSpecification',[$id]);
	}

	public function editVendorLabWorkSpecification($vendorLabID,Request $request)
	{
		if($request->session()->has('name') == true || $vendorLabID != null)
		{
			$lab_details = DB::table('m_vendor_lab_details')
			->where('id',$vendorLabID)
			->get();

			$vendor_type_id = $lab_details[0]->vendor_type_id;

			$vendor_lab_id = $lab_details[0]->id;

			$lab_workspec_details = DB::table('m_vendor_lab_workspec_details')
			->where('m_vendor_lab_workspec_details.vendor_lab_id',$vendorLabID)
			->join('s_vendor_types','m_vendor_lab_workspec_details.vendor_type_id','=','s_vendor_types.id')
			->join('s_lab_work_type','m_vendor_lab_workspec_details.work_type_id','=','s_lab_work_type.id')
			->join('s_lab_work_subtype','m_vendor_lab_workspec_details.work_subtype_id','=','s_lab_work_subtype.id')
			->join('s_lab_work_name','m_vendor_lab_workspec_details.work_name_id','=','s_lab_work_name.id')
			->select('*','s_lab_work_type.name as lab_work_type_name','s_lab_work_subtype.name as lab_work_subtype_name','m_vendor_lab_workspec_details.id as lab_workspec_id','s_vendor_types.name as vendor_type_name')
			->get();

			$workType = DB::table('s_lab_work_type')
			->get();

			return view('master.vendors.lab.editWorkSpecificationLab', compact('lab_workspec_details','workType','vendorLabID','vendor_type_id','vendor_lab_id'));

		}
		else
		{
			$request->session()->flash('alert-danger', 'Clinic Name is required!');

			return redirect()->action('MasterController@editVendorLab',[$vendorLabID]);
		}

	}

	public function deleteVendorLabWork($lab_workspec_id, $vendorLabID)
	{
		DB::table('m_vendor_lab_workspec_details')
		->where('id', $lab_workspec_id)
		->delete();

		return redirect()->action('MasterController@editVendorLabWorkSpecification',[$vendorLabID]);
	}

	public function updateVendorLabWorkSpecification(Request $request)
	{
		$vendorLabId = $request->get('vendorLabID');
		
		$work_type_id=$request->get('work_type_id');
		$work_subtype_id=$request->get('work_subtype_id');
		$work_name_id=$request->get('work_name_id');
		$rate=$request->get('rate');
		$duration=$request->get('duration');
		$vendor_type_id=$request->get('vendor_type_id');

		if($request->session()->has('id') == true)
		{
			$name  = Session::get('name');
			$address  = Session::get('address');
			$contact_person  = Session::get('contact_person');
			$cell  = Session::get('cell');
			$email  = Session::get('email');

			DB::table('m_vendor_lab_details')
			->where('id', $vendorLabId)
			->update(['name' => $name,
				'address' => $address,
				'contact_person' => $contact_person,
				'cell_no' => $cell,
				'email' => $email, 
				]);

		}

		foreach ($work_type_id as $key => $work_type) 
		{
			if($work_type != null)
			{
				$labvendorspecification= new LabVendorSpecification;
				$labvendorspecification->rate=$rate[$key];
				$labvendorspecification->duration=$duration[$key];
				$labvendorspecification->vendor_type_id=$vendor_type_id;
				$labvendorspecification->work_type_id=$work_type;
				$labvendorspecification->work_subtype_id=$work_subtype_id[$key];
				$labvendorspecification->work_name_id=$work_name_id[$key];
				$labvendorspecification->vendor_lab_id=$vendorLabId;
				$labvendorspecification->save();
			}

		}

		//flash-message
		$request->session()->flash('alert-info', 'Lab Vendor was successfully updated!');

		return redirect()->action('MasterController@listViewVendorLab');
	}


	public function viewVendorLab($vendorLabID,Request $request)
	{

		$vendor_lab_detail = DB::table('m_vendor_lab_workspec_details')
		->join('s_vendor_types','m_vendor_lab_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_vendor_lab_details','m_vendor_lab_details.id','=','m_vendor_lab_workspec_details.vendor_lab_id')
		->where('m_vendor_lab_details.id',$vendorLabID)
		->select('*','m_vendor_lab_workspec_details.id as m_vendor_lab_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		$vendor_lab_details = $vendor_lab_detail['0'];

		return view('master.vendors.lab.viewVendorLab', compact('vendor_lab_details'));

	}
	public function viewVendorLabWorkSpecification($vendorLabID,Request $request)
	{

		$vendor_lab_workspec_id = $request->get('vendor_lab_workspec_id');

		$lab_workspec_details = DB::table('m_vendor_lab_workspec_details')
		->where('m_vendor_lab_workspec_details.vendor_lab_id',$vendorLabID)
		->join('s_vendor_types','m_vendor_lab_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('s_lab_work_type','m_vendor_lab_workspec_details.work_type_id','=','s_lab_work_type.id')
		->join('s_lab_work_subtype','m_vendor_lab_workspec_details.work_subtype_id','=','s_lab_work_subtype.id')
		->join('s_lab_work_name','m_vendor_lab_workspec_details.work_name_id','=','s_lab_work_name.id')
		->select('*','s_lab_work_type.name as lab_work_type_name','s_lab_work_subtype.name as lab_work_subtype_name','m_vendor_lab_workspec_details.id as lab_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		return view('master.vendors.lab.viewWorkSpecificationLab', compact('lab_workspec_details'));


	}

	public function listViewVendorMaterial()
	{
		Session::forget('id');
		Session::forget('name');
		Session::forget('address');
		Session::forget('email');
		Session::forget('cell_no');
		Session::forget('contact_person');

		$vendor_material_details = DB::table('m_vendor_mat_workspec_details')
		->join('s_material_type','m_vendor_mat_workspec_details.mat_type_id','=','s_material_type.id')
		->join('s_material_subtype','m_vendor_mat_workspec_details.mat_subtype_id','=','s_material_subtype.id')
		->join('m_material_details','m_vendor_mat_workspec_details.mat_name_id','=','m_material_details.id')
		->join('m_vendor_material_details','m_vendor_mat_workspec_details.vendor_mat_id','=','m_vendor_material_details.id')
		->select('*','s_material_type.name as material_type_name','s_material_subtype.name as material_subtype_name','m_material_details.material_name as material_name')
		->get();

		return view('master.vendors.material.listViewVendorMaterial',compact('vendor_material_details'));
	}

	public function createVendorMaterial()
	{ 


		$vendor_type = DB::table('s_vendor_types')
		->where('name','Material')
		->get();

		$vendorMaterialID = DB::table('m_vendor_material_details')
		->orderBy('id','desc')
		->first();

		if($vendorMaterialID == null)
		{
			$vendorMaterialID = (object)array('id' => 0);
		}


		$vendor_type = $vendor_type['0'];
		
		return view('master.vendors.material.addVendorMaterial',compact('vendor_type','vendorMaterialID'));
	}

	public function storeVendorMaterial(Request $request)
	{
		
		$id = $request->get('vendor_material_id'); 
		$name=$request->get('mat_name');
		$address =$request->get('address');
		$contact_person=$request->get('contact_person');
		$cell_no=$request->get('cell_no');
		$email=$request->get('email');
		$vendor_type_id=$request->get('vendor_type_id');


		session(['id' => $id,
			'name' => $name,
			'contact_person' =>$contact_person,
			'address'=>$address,
			'cell_no'=>$cell_no,
			'email'=>$email,
			'vendor_type_id'=>$vendor_type_id]);

		return redirect()->action('MasterController@createVendorMaterialWorkSpecification');
	}

	public function createVendorMaterialWorkSpecification(Request $request)
	{
		if($request->session()->has('name') == true)
		{
			$vendorMaterialID = null; 

			if($request->session()->has('id') == false)
			{
				
				$vendorMaterialID = DB::table('m_vendor_material_details')
				->orderBy('id','desc')
				->first();

				if($vendorMaterialID == null)
				{
					$vendorMaterialID = (object)array('id' => 0);
				}

				$vendorMaterialID = $vendorMaterialID->id + 1; 
			}
			else
			{
				$vendorMaterialID = Session::get('id');
				$VendorMaterialName = Session::get('name');
			}
			
			$vendor_type_id = Session::get('vendor_type_id');

			$matType = DB::table('s_material_type')
			->get();

			return view('master.vendors.material.addWorkSpecificationMaterial',compact('VendorMaterialName','matType','vendorMaterialID','vendor_material_id','vendor_type_id'));
		}
		else
		{
			//flash-message
			$request->session()->flash('alert-danger', 'Vendor Name is required!');

			return redirect()->action('MasterController@createVendorMaterial');
		}
	}

	public function storeVendorMaterialWorkSpecification(Request $request)
	{
		$vendor_type_id=$request->get('vendor_type_id');

		$name  = Session::get('name');
		$address  = Session::get('address');
		$contact_person  = Session::get('contact_person');
		$cell_no  = Session::get('cell_no');
		$email  = Session::get('email');

		$vendordetails= new VendorMaterialDetails;
		$vendordetails->name=$name;
		$vendordetails->address=$address;
		$vendordetails->contact_person=$contact_person;
		$vendordetails->cell_no=$cell_no;
		$vendordetails->email=$email;
		$vendordetails->vendor_type_id=$vendor_type_id;
		$vendordetails->save();

		$material_type_id=$request->get('material_type_id');
		$material_subtype_id=$request->get('material_subtype_id');
		$material_name_id=$request->get('material_name_id');
		$rate=$request->get('rate');
		$duration=$request->get('duration');
		$vendor_material_id=$request->get('vendor_material_id');

		foreach ($material_type_id as $key => $material_type) {

			$materialvendorspecification= new MaterialVendorSpecification;
			$materialvendorspecification->rate=$rate[$key];
			$materialvendorspecification->duration=$duration[$key];
			$materialvendorspecification->vendor_type_id=$vendor_type_id;
			$materialvendorspecification->mat_type_id=$material_type;
			$materialvendorspecification->mat_subtype_id=$material_subtype_id[$key];
			$materialvendorspecification->mat_name_id=$material_name_id[$key];
			$materialvendorspecification->vendor_mat_id= $vendor_material_id;
			$materialvendorspecification->save();

		}

		//flash-message
		$request->session()->flash('alert-success', 'Material Vendor was successfully added!');

		return redirect()->action('MasterController@listViewVendorMaterial');

	}

	public function editVendorMaterial($vendorMaterialID, Request $request)
	{
		$vendor_material_detail = DB::table('m_vendor_mat_workspec_details')
		->join('m_vendor_material_details','m_vendor_material_details.id','=','m_vendor_mat_workspec_details.vendor_mat_id')
		->where('m_vendor_material_details.id',$vendorMaterialID)
		->select('*','m_vendor_mat_workspec_details.id as m_vendor_mat_workspec_id')
		->get();

		$vendor_material_details = $vendor_material_detail['0'];

		return view('master.vendors.material.editVendorMaterial', compact('vendor_material_details','vendorMaterialID'));
	}

	public function updateVendorMaterial(Request $request)
	{
		$id  = $request->get('vendor_material_id'); 
		$name=$request->get('mat_name');
		$address =$request->get('address');
		$contact_person=$request->get('contact_person');
		$cell_no=$request->get('cell_no');
		$email=$request->get('email');
		$vendor_type_id=$request->get('vendor_type_id');


		session(['id' => $id,
			'name' => $name,
			'contact_person' => $contact_person,
			'address'=>$address,
			'cell_no'=>$cell_no,
			'email'=>$email,
			'vendor_type_id'=>$vendor_type_id]);


		return redirect()->action('MasterController@editVendorMaterialWorkSpecification',[$id]);
	}

	public function editVendorMaterialWorkSpecification($vendorMaterialID, Request $request)
	{
		if($request->session()->has('name') == true || $vendorMaterialID != null)
		{
			$mat_details = DB::table('m_vendor_material_details')
			->where('id',$vendorMaterialID)
			->get();

			$vendor_type_id = $mat_details[0]->vendor_type_id;

			$mat_workspec_details = DB::table('m_vendor_mat_workspec_details')
			->where('m_vendor_mat_workspec_details.vendor_mat_id',$vendorMaterialID)
			->join('s_vendor_types','m_vendor_mat_workspec_details.vendor_type_id','=','s_vendor_types.id')
			->join('s_material_type','m_vendor_mat_workspec_details.mat_type_id','=','s_material_type.id')
			->join('s_material_subtype','m_vendor_mat_workspec_details.mat_subtype_id','=','s_material_subtype.id')
			->join('m_material_details','m_vendor_mat_workspec_details.mat_name_id','=','m_material_details.id')
			->select('*','s_material_type.name as material_type_name','s_material_subtype.name as material_subtype_name','m_vendor_mat_workspec_details.id as mat_workspec_id','s_vendor_types.name as vendor_type_name')
			->get();

			$matType = DB::table('s_material_type')
			->get();

			return view('master.vendors.material.editWorkSpecificationMaterial', compact('mat_workspec_details','matType','vendorMaterialID','vendor_type_id'));

		}
		else
		{
			$request->session()->flash('alert-danger', 'Clinic Name is required!');

			return redirect()->action('MasterController@editVendorMaterial',[$vendorMaterialID]);
		}
	}

	public function deleteVendorMaterial($mat_workspec_id, $vendorMaterialID)
	{
		DB::table('m_vendor_mat_workspec_details')
		->where('id', $mat_workspec_id)
		->delete();

		return redirect()->action('MasterController@editVendorMaterialWorkSpecification',[$vendorMaterialID]);
	}

	public function updateVendorMaterialWorkSpecification(Request $request)
	
	{
		$vendor_material_id = $request->get('vendor_material_id');

		if($request->session()->has('id') == true)
		{
			$name  = Session::get('name');
			$address  = Session::get('address');
			$contact_person  = Session::get('contact_person');
			$cell_no = Session::get('cell_no');
			$email  = Session::get('email');

			DB::table('m_vendor_material_details')
			->where('id', $vendor_material_id)
			->update(['name' => $name,
				'address' => $address,
				'contact_person' => $contact_person,
				'cell_no' => $cell_no,
				'email' => $email, 
				]);

		}

		$material_type_id=$request->get('material_type_id');
		$material_subtype_id=$request->get('material_subtype_id');
		$material_name_id=$request->get('material_name_id');
		$rate=$request->get('rate');
		$duration=$request->get('duration');
		$vendor_type_id=$request->get('vendor_type_id');

		foreach ($material_type_id as $key => $material_type) {

			$materialvendorspecification= new MaterialVendorSpecification;
			$materialvendorspecification->rate=$rate[$key];
			$materialvendorspecification->duration=$duration[$key];
			$materialvendorspecification->vendor_type_id=$vendor_type_id;
			$materialvendorspecification->mat_type_id=$material_type;
			$materialvendorspecification->mat_subtype_id=$material_subtype_id[$key];
			$materialvendorspecification->mat_name_id=$material_name_id[$key];
			$materialvendorspecification->vendor_mat_id= $vendor_material_id;
			$materialvendorspecification->save();

		}
		
		//flash-message
		$request->session()->flash('alert-info', 'Material Vendor was successfully updated!');

		return redirect()->action('MasterController@listViewVendorMaterial');

	}

	public function viewVendorMaterial($vendorMaterialID,Request $request)
	{
		$vendor_material_detail = DB::table('m_vendor_mat_workspec_details')
		->join('m_vendor_material_details','m_vendor_material_details.id','=','m_vendor_mat_workspec_details.vendor_mat_id')
		->where('m_vendor_material_details.id',$vendorMaterialID)
		->select('*','m_vendor_mat_workspec_details.id as m_vendor_mat_workspec_id')
		->get();

		$vendor_material_details = $vendor_material_detail['0'];

		return view('master.vendors.material.viewVendorMaterial', compact('vendor_material_details'));

	}

	public function viewVendorMaterialWorkSpecification($vendorMaterialID, Request $request)
	{
		$mat_workspec_details = DB::table('m_vendor_mat_workspec_details')
		->where('m_vendor_mat_workspec_details.vendor_mat_id',$vendorMaterialID)
		->join('s_vendor_types','m_vendor_mat_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('s_material_type','m_vendor_mat_workspec_details.mat_type_id','=','s_material_type.id')
		->join('s_material_subtype','m_vendor_mat_workspec_details.mat_subtype_id','=','s_material_subtype.id')
		->join('m_material_details','m_vendor_mat_workspec_details.mat_name_id','=','m_material_details.id')
		->select('*','s_material_type.name as material_type_name','s_material_subtype.name as material_subtype_name','m_vendor_mat_workspec_details.id as mat_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();
		
		return view('master.vendors.material.viewWorkSpecificationMaterial', compact('mat_workspec_details','vendorMaterialID'));
	}

	public function listViewVendorInstrument()
	{
		Session::forget('id');
		Session::forget('name');
		Session::forget('address');
		Session::forget('email');
		Session::forget('cell');
		Session::forget('contact_person');

		$vendor_instrument_details = DB::table('m_vendor_inst_workspec_details')
		->join('s_instrument_type','m_vendor_inst_workspec_details.inst_type_id','=','s_instrument_type.id')
		->join('s_instrument_subtype','m_vendor_inst_workspec_details.inst_subtype_id','=','s_instrument_subtype.id')
		->join('m_instrument_details','m_vendor_inst_workspec_details.inst_name_id','=','m_instrument_details.id')
		->join('m_vendor_instrument_details','m_vendor_inst_workspec_details.vendor_inst_id','=','m_vendor_instrument_details.id')
		->select('*','s_instrument_type.name as instrument_type_name','s_instrument_subtype.name as instrument_subtype_name')
		->get();

		return view('master.vendors.instrument.listViewVendorInstrument',compact('vendor_instrument_details'));
	}

	public function createVendorInstrument()
	{
		$vendor_type = DB::table('s_vendor_types')
		->where('name','Instrument')
		->get();

		$vendorInstrumentID = DB::table('m_vendor_instrument_details')
		->orderBy('id','desc')
		->first();

		if($vendorInstrumentID == null)
		{
			$vendorInstrumentID = (object)array('id' => 0);
		}

		$vendor_type = $vendor_type['0'];
		
		return view('master.vendors.instrument.addVendorInstrument',compact('vendor_type','vendorInstrumentID'));
	}

	public function storeVendorInstrument(Request $request)
	{
		$id = $request->get('vendor_instrument_id'); 
		$name=$request->get('instrument_name');
		$address =$request->get('address');
		$contact_person=$request->get('contact_person');
		$cell_no=$request->get('cell');
		$email=$request->get('email');
		$vendor_type_id=$request->get('vendor_type_id');


		session(['id' => $id,
			'name' => $name,
			'contact_person' => $contact_person,
			'address'=>$address,
			'cell'=>$cell_no,
			'email'=>$email,
			'vendor_type_id'=>$vendor_type_id]);


		return redirect()->action('MasterController@createVendorInstrumentWorkSpecification');
	}

	public function createVendorInstrumentWorkSpecification(Request $request)
	{
		if($request->session()->has('name') == true)
		{
			$vendorInstrumentID = null; 

			if($request->session()->has('id') == false)
			{
				
				$vendorInstrumentID = DB::table('m_vendor_instrument_details')
				->orderBy('id','desc')
				->first();

				if($vendorInstrumentID == null)
				{
					$vendorInstrumentID = (object)array('id' => 0);
					
				}

				$vendorInstrumentID = $vendorInstrumentID->id + 1; 
			}
			else
			{
				$vendorInstrumentID = Session::get('id');
				$VendorInstrumentName = Session::get('name');
			}
			
			$vendor_type_id = Session::get('vendor_type_id');

			$instType = DB::table('s_instrument_type')
			->get();


			return view('master.vendors.instrument.addWorkSpecificationInstrument',compact('VendorInstrumentName','instType','vendorInstrumentID','vendor_instrument_id','vendor_type_id'));
		}
		else
		{
			//flash-message
			$request->session()->flash('alert-danger', 'Vendor Name is required!');

			return redirect()->action('MasterController@createVendorInstrument');
		}
	}
	
	public function storeVendorInstrumentWorkSpecification(Request $request)
	{
		$vendor_type_id  = $request->get('vendor_type_id');
	//save details to m_clinic table

		$name  = Session::get('name');
		$address  = Session::get('address');
		$contact_person  = Session::get('contact_person');
		$cell  = Session::get('cell');
		$email  = Session::get('email');

		$vendor= new VendorInstrumentDetails;
		$vendor->name=$name;
		$vendor->address	=$address;
		$vendor->contact_person	=$contact_person;
		$vendor->cell_no=$cell;
		$vendor->email	=$email;
		$vendor->vendor_type_id=$vendor_type_id;
		$vendor->save();

		$instrument_type_id=$request->get('instrument_type_id');
		$instrument_subtype_id=$request->get('instrument_subtype_id');
		$instrument_name_id=$request->get('instrument_name_id');
		$rate=$request->get('rate');
		$vendor_instrument_id=$request->get('vendor_instrument_id');

		foreach ($instrument_type_id as $key => $instrument_type) {

			$instrumentvendorspecification= new InstrumentVendorSpecification;
			$instrumentvendorspecification->rate=$rate[$key];
			$instrumentvendorspecification->vendor_type_id=$vendor_type_id;
			$instrumentvendorspecification->inst_type_id=$instrument_type;
			$instrumentvendorspecification->inst_subtype_id=$instrument_subtype_id[$key];
			$instrumentvendorspecification->inst_name_id=$instrument_name_id[$key];
			$instrumentvendorspecification->vendor_inst_id= $vendor_instrument_id;
			$instrumentvendorspecification->save();

		}

		//flash-message
		$request->session()->flash('alert-success', 'Instrument Vendor was successfully added!');

		return redirect()->action('MasterController@listViewVendorInstrument');

	}

	public function editVendorInstrument($vendorInstrumentID,Request $request)
	{
		$vendor_instrument_detail = DB::table('m_vendor_inst_workspec_details')
		->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','m_vendor_inst_workspec_details.vendor_inst_id')
		->where('m_vendor_instrument_details.id',$vendorInstrumentID)
		->select('*','m_vendor_inst_workspec_details.id as m_vendor_inst_workspec_id')
		->get();

		$vendor_instrument_details= $vendor_instrument_detail['0'];

		return view('master.vendors.instrument.editVendorInstrument', compact('vendor_instrument_details'));

	}

	public function updateVendorInstrument(Request $request)
	{
		$id  = $request->get('vendorInstrumentID'); 
		$name=$request->get('instrument_name');
		$address =$request->get('address');
		$contact_person=$request->get('contact_person');
		$cell=$request->get('cell');
		$email=$request->get('email');
		$vendor_type_id=$request->get('vendor_type_id');


		session(['id' => $id,
			'name' => $name,
			'contact_person' => $contact_person,
			'address'=>$address,
			'cell'=>$cell,
			'email'=>$email,
			'vendor_type_id'=>$vendor_type_id]);

		return redirect()->action('MasterController@editVendorInstrumentWorkSpecification',[$id]);


	}

	public function editVendorInstrumentWorkSpecification($vendorInstrumentID,Request $request)
	{
		if($request->session()->has('name') == true || $vendorInstrumentID != null)
		{
			$inst_details = DB::table('m_vendor_instrument_details')
			->where('id',$vendorInstrumentID)
			->get();

			$vendor_type_id = $inst_details[0]->vendor_type_id;

			$vendor_inst_id = $inst_details[0]->id;

			$inst_workspec_details = DB::table('m_vendor_inst_workspec_details')
			->where('m_vendor_inst_workspec_details.vendor_inst_id',$vendorInstrumentID)
			->join('s_vendor_types','m_vendor_inst_workspec_details.vendor_type_id','=','s_vendor_types.id')
			->join('s_instrument_type','m_vendor_inst_workspec_details.inst_type_id','=','s_instrument_type.id')
			->join('s_instrument_subtype','m_vendor_inst_workspec_details.inst_subtype_id','=','s_instrument_subtype.id')
			->join('m_instrument_details','m_vendor_inst_workspec_details.inst_name_id','=','m_instrument_details.id')
			->select('*','s_instrument_type.name as instrument_type_name','s_instrument_subtype.name as instrument_subtype_name','m_vendor_inst_workspec_details.id as inst_workspec_id','s_vendor_types.name as vendor_type_name')
			->get();

			$instType = DB::table('s_instrument_type')
			->get();		

			return view('master.vendors.instrument.editWorkSpecificationInstrument', compact('inst_workspec_details','instType','vendorInstrumentID','vendor_type_id','vendor_inst_id'));

		}
		else
		{
			$request->session()->flash('alert-danger', 'Vendor Name is required!');

			return redirect()->action('MasterController@editVendorInstrument',[$vendorInstrumentID]);
		}

	}

	public function deleteVendorInstrument($inst_workspec_id, $vendorInstrumentID)
	{
		DB::table('m_vendor_inst_workspec_details')
		->where('id', $inst_workspec_id)
		->delete();

		return redirect()->action('MasterController@editVendorInstrumentWorkSpecification',[$vendorInstrumentID]);
	}

	public function updateVendorInstrumentWorkSpecification(Request $request)
	{
		$vendor_instrument_id=$request->get('vendor_instrument_id');

		if($request->session()->has('id') == true)
		{
			$name  = Session::get('name');
			$address  = Session::get('address');
			$contact_person  = Session::get('contact_person');
			$cell  = Session::get('cell');
			$email  = Session::get('email');

			DB::table('m_vendor_instrument_details')
			->where('id', $vendor_instrument_id)
			->update(['name' => $name,
				'address' => $address,
				'contact_person' => $contact_person,
				'cell_no' => $cell,
				'email' => $email, 
				]);

		}

		$instrument_type_id=$request->get('instrument_type_id');
		$instrument_subtype_id=$request->get('instrument_subtype_id');
		$instrument_name_id=$request->get('instrument_name_id');
		$rate=$request->get('rate');
		$vendor_type_id=$request->get('vendor_type_id');

		foreach ($instrument_type_id as $key => $instrument_type) {

			$instrumentvendorspecification= new InstrumentVendorSpecification;
			$instrumentvendorspecification->rate=$rate[$key];
			$instrumentvendorspecification->vendor_type_id=$vendor_type_id;
			$instrumentvendorspecification->inst_type_id=$instrument_type;
			$instrumentvendorspecification->inst_subtype_id=$instrument_subtype_id[$key];
			$instrumentvendorspecification->inst_name_id=$instrument_name_id[$key];
			$instrumentvendorspecification->vendor_inst_id= $vendor_instrument_id;
			$instrumentvendorspecification->save();

		}
		
		//flash-message
		$request->session()->flash('alert-info', 'Instrument Work specification was successfully updated!');

		return redirect()->action('MasterController@listViewVendorInstrument');
	}

	public function viewVendorInstrument($vendorInstrumentID,Request $request)
	{
		$vendor_instrument_id = $request->get('vendor_instrument_id');

		$vendor_instrument_detail = DB::table('m_vendor_inst_workspec_details')
		->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','m_vendor_inst_workspec_details.vendor_inst_id')
		->where('m_vendor_instrument_details.id',$vendorInstrumentID)
		->select('*','m_vendor_inst_workspec_details.id as m_vendor_inst_workspec_id')
		->get();

		$vendor_instrument_details= $vendor_instrument_detail['0'];

		return view('master.vendors.instrument.viewVendorInstrument', compact('vendor_instrument_details'));

	}
	public function viewVendorInstrumentWorkSpecification($vendorInstrumentID,Request $request)
	{
		$inst_workspec_details = DB::table('m_vendor_inst_workspec_details')
		->where('m_vendor_inst_workspec_details.vendor_inst_id',$vendorInstrumentID)
		->join('s_vendor_types','m_vendor_inst_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('s_instrument_type','m_vendor_inst_workspec_details.inst_type_id','=','s_instrument_type.id')
		->join('s_instrument_subtype','m_vendor_inst_workspec_details.inst_subtype_id','=','s_instrument_subtype.id')
		->join('m_instrument_details','m_vendor_inst_workspec_details.inst_name_id','=','m_instrument_details.id')
		->select('*','s_instrument_type.name as instrument_type_name','s_instrument_subtype.name as instrument_subtype_name','m_vendor_inst_workspec_details.id as inst_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		return view('master.vendors.instrument.viewWorkSpecificationInstrument', compact('inst_workspec_details','vendorInstrumentID'));
	}

	public function listViewVendorGadget()
	{
		Session::forget('id');
		Session::forget('name');
		Session::forget('address');
		Session::forget('email');
		Session::forget('cell');
		Session::forget('contact_person');

		$vendor_gadget_details=DB::table('m_vendor_gadget_workspec_details')
		->join('m_gadget_details','m_vendor_gadget_workspec_details.gadget_name_id','=','m_gadget_details.id')
		->join('m_vendor_gadget_details','m_vendor_gadget_workspec_details.vendor_gadget_id','=','m_vendor_gadget_details.id')
		->select('*')
		->get();
		
		return view('master.vendors.gadget.listViewVendorGadget',compact('vendor_gadget_details'));
	}

	public function createVendorGadget()
	{

		$vendor_type = DB::table('s_vendor_types')
		->where('name','Gadget')
		->get();

		$vendorGadgetID = DB::table('m_vendor_gadget_details')
		->orderBy('id','desc')
		->first();

		if($vendorGadgetID == null)
		{
			$vendorGadgetID = (object)array('id' => 0);
		}


		$vendor_type = $vendor_type['0'];
		
		return view('master.vendors.gadget.addVendorGadget',compact('vendor_type','vendorGadgetID'));
	}
	
	public function storeVendorGadget(Request $request)
	{
		$id  = $request->get('vendor_gadget_id'); 
		$name=$request->get('gadget_name');
		$address =$request->get('address');
		$contact_person=$request->get('contact_person');
		$cell_no=$request->get('cell');
		$email=$request->get('email');
		$vendor_type_id=$request->get('vendor_type_id');


		session(['id' => $id,
			'name' => $name,
			'contact_person' => $contact_person,
			'address'=>$address,
			'cell'=>$cell_no,
			'email'=>$email,
			'vendor_type_id'=>$vendor_type_id]);


		return redirect()->action('MasterController@createVendorGadgetWorkSpecification');
	}

	public function createVendorGadgetWorkSpecification(Request $request)
	{

		if($request->session()->has('name') == true)
		{
			$vendorGadgetID = null; 

			if($request->session()->has('id') == false)
			{
				
				$vendorGadgetID = DB::table('m_vendor_gadget_details')
				->orderBy('id','desc')
				->first();

				if($vendorGadgetID == null)
				{
					$vendorGadgetID = (object)array('id' => 0);
				}

				$vendorGadgetID = $vendorGadgetID->id + 1; 
			}
			else
			{
				$vendorGadgetID = Session::get('id');
				$VendorGadgetName = Session::get('name');
			}
			
			$vendor_type_id = Session::get('vendor_type_id');

			$gadgetName = DB::table('m_gadget_details')
			->get();

			return view('master.vendors.gadget.addWorkSpecificationGadget',compact('VendorGadgetName','gadgetName','vendorGadgetID','vendor_gadget_id','vendor_type_id'));
		}
		else
		{
			//flash-message
			$request->session()->flash('alert-danger', 'Vendor Name is required!');

			return redirect()->action('MasterController@createVendorGadget');
		}
	}

	public function storeVendorGadgetWorkSpecification(Request $request)
	{
		$vendor_type_id=$request->get('vendor_type_id');

		$name  = Session::get('name');
		$address  = Session::get('address');
		$contact_person  = Session::get('contact_person');
		$cell  = Session::get('cell');
		$email  = Session::get('email');

		$vendor= new VendorGadgetDetails;
		$vendor->name=$name;
		$vendor->address=$address;
		$vendor->contact_person	=$contact_person;
		$vendor->cell_no=$cell;
		$vendor->email	=$email;
		$vendor->vendor_type_id=$vendor_type_id;
		$vendor->save();

		$gadget_name_id=$request->get('gadget_name_id');
		$rate=$request->get('rate');
		$vendor_gadget_id=$request->get('vendor_gadget_id');

		foreach ($gadget_name_id as $key => $gadget_name) {

			$GadgetVendorSpecification= new GadgetVendorSpecification;
			$GadgetVendorSpecification->rate=$rate[$key];
			$GadgetVendorSpecification->vendor_type_id=$vendor_type_id;
			$GadgetVendorSpecification->gadget_name_id=$gadget_name;
			$GadgetVendorSpecification->vendor_gadget_id= $vendor_gadget_id;
			$GadgetVendorSpecification->save();

		}
		
		//flash-message
		$request->session()->flash('alert-success', 'Gadget Vendor was successfully added!');

		return redirect()->action('MasterController@listViewVendorGadget');
	}

	public function editVendorGadget($vendorGadgetID, Request $request)
	{	
		$vendor_gadget_detail = DB::table('m_vendor_gadget_workspec_details')
		->join('s_vendor_types','m_vendor_gadget_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','m_vendor_gadget_workspec_details.vendor_gadget_id')
		->where('m_vendor_gadget_details.id',$vendorGadgetID)
		->select('*','m_vendor_gadget_workspec_details.id as m_vendor_gadget_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		$vendor_gadget_details = $vendor_gadget_detail['0'];

		return view('master.vendors.gadget.editVendorGadget', compact('vendor_gadget_details','vendorGadgetID'));
	}

	public function updateVendorGadget(Request $request)
	{
		$id  = $request->get('vendorGadgetID'); 
		$name=$request->get('gadget_name');
		$address =$request->get('address');
		$contact_person=$request->get('contact_person');
		$cell_no=$request->get('cell');
		$email=$request->get('email');
		$vendor_type_id=$request->get('vendor_type_id');


		session(['id' => $id,
			'name' => $name,
			'contact_person' => $contact_person,
			'address'=>$address,
			'cell'=>$cell_no,
			'email'=>$email,
			'vendor_type_id'=>$vendor_type_id]);

		return redirect()->action('MasterController@editVendorGadgetWorkSpecification',[$id]);

	}

	public function editVendorGadgetWorkSpecification($vendorGadgetID,Request $request)
	{
		if($request->session()->has('name') == true || $vendorGadgetID != null)
		{
			$gadget_details = DB::table('m_vendor_gadget_details')
			->where('id',$vendorGadgetID)
			->get();

			$vendor_type_id = $gadget_details[0]->vendor_type_id;

			$gadget_workspec_details = DB::table('m_vendor_gadget_workspec_details')
			->where('m_vendor_gadget_workspec_details.vendor_gadget_id',$vendorGadgetID)
			->join('s_vendor_types','m_vendor_gadget_workspec_details.vendor_type_id','=','s_vendor_types.id')
			->join('m_gadget_details','m_vendor_gadget_workspec_details.gadget_name_id','=','m_gadget_details.id')
			->select('*','m_vendor_gadget_workspec_details.id as gadget_workspec_id','s_vendor_types.name as vendor_type_name')
			->get();

			$gadgetName = DB::table('m_gadget_details')
			->get();

			return view('master.vendors.gadget.editWorkSpecificationGadget', compact('gadget_workspec_details','gadgetName','vendorGadgetID','vendor_type_id','vendor_gadget_id'));

		}
		else
		{
			$request->session()->flash('alert-danger', 'Vendor Name is required!');

			return redirect()->action('MasterController@editVendorGadget',[$vendorGadgetID]);
		}
	}

	public function deleteVendorGadgetName($gdgt_workspec_id, $vendorGadgetID)
	{
		DB::table('m_vendor_gadget_workspec_details')
		->where('id', $gdgt_workspec_id)
		->delete();

		return redirect()->action('MasterController@editVendorGadgetWorkSpecification',[$vendorGadgetID]);
	}

	public function updateVendorGadgetWorkSpecification(Request $request)
	{
		$vendor_gadget_id = $request->get('vendor_gadget_id');

		$vendor_type_id = $request->get('vendor_type_id');
		
		if($request->session()->has('id') == true)
		{
			$name  = Session::get('name');
			$address  = Session::get('address');
			$contact_person  = Session::get('contact_person');
			$cell  = Session::get('cell');
			$email  = Session::get('email');

			DB::table('m_vendor_gadget_details')
			->where('id', $vendor_gadget_id)
			->update(['name' => $name,
				'address' => $address,
				'contact_person' => $contact_person,
				'cell_no' => $cell,
				'email' => $email, 
				]);

		}

		$gadget_name_id=$request->get('gadget_name_id');
		$rate=$request->get('rate');
		$vendor_gadget_id=$request->get('vendor_gadget_id');

		foreach ($gadget_name_id as $key => $gadget_name) {

			$GadgetVendorSpecification= new GadgetVendorSpecification;
			$GadgetVendorSpecification->rate=$rate[$key];
			$GadgetVendorSpecification->vendor_type_id=$vendor_type_id;
			$GadgetVendorSpecification->gadget_name_id=$gadget_name;
			$GadgetVendorSpecification->vendor_gadget_id= $vendor_gadget_id;
			$GadgetVendorSpecification->save();

		}

		//flash-message
		$request->session()->flash('alert-info', 'Gadget Vendor was successfully updated!');

		return redirect()->action('MasterController@listViewVendorGadget');

	}

	public function viewVendorGadget($vendorGadgetID,Request $request)
	{		
		$vendor_gadget_detail = DB::table('m_vendor_gadget_workspec_details')
		->join('s_vendor_types','m_vendor_gadget_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','m_vendor_gadget_workspec_details.vendor_gadget_id')
		->where('m_vendor_gadget_details.id',$vendorGadgetID)
		->select('*','m_vendor_gadget_workspec_details.id as m_vendor_gadget_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		$vendor_gadget_details = $vendor_gadget_detail['0'];

		return view('master.vendors.gadget.viewVendorGadget', compact('vendor_gadget_details','vendorGadgetID'));
	}

	public function viewVendorGadgetWorkSpecification($vendorGadgetID, Request $request)
	{		
		$gadget_workspec_details = DB::table('m_vendor_gadget_workspec_details')
		->where('m_vendor_gadget_workspec_details.vendor_gadget_id',$vendorGadgetID)
		->join('s_vendor_types','m_vendor_gadget_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_gadget_details','m_vendor_gadget_workspec_details.gadget_name_id','=','m_gadget_details.id')
		->select('*','m_vendor_gadget_workspec_details.id as gadget_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		return view('master.vendors.gadget.viewWorkSpecificationGadget', compact('gadget_workspec_details','vendorGadgetID'));

	}
	public function listViewVendorMachine()
	{
		Session::forget('id');
		Session::forget('name');
		Session::forget('address');
		Session::forget('email');
		Session::forget('cell');
		Session::forget('contact_person');

		$vendor_machine_details=DB::table('m_vendor_machine_workspec_details')
		->join('m_machine_details','m_vendor_machine_workspec_details.machine_name_id','=','m_machine_details.id')
		->join('m_vendor_machine_details','m_vendor_machine_workspec_details.vendor_machine_id','=','m_vendor_machine_details.id')
		->select('*')
		->get();

		return view('master.vendors.machine.listViewVendorMachine',compact('vendor_machine_details'));
	}

	public function createVendorMachine()
	{
		$vendor_type = DB::table('s_vendor_types')
		->where('name','Machine')
		->get();

		$vendorMachineID = DB::table('m_vendor_machine_details')
		->orderBy('id','desc')
		->first();

		if($vendorMachineID == null)
		{
			$vendorMachineID = (object)array('id' => 0);
		}

		$vendor_type = $vendor_type['0'];
		
		return view('master.vendors.machine.addVendorMachine',compact('vendor_type','vendorMachineID'));
	}

	public function storeVendorMachine(Request $request)
	{
		$id  = $request->get('vendor_machine_id'); 
		$name=$request->get('machine_name');
		$address =$request->get('address');
		$contact_person=$request->get('contact_person');
		$cell_no=$request->get('cell');
		$email=$request->get('email');
		$vendor_type_id=$request->get('vendor_type_id');


		session(['id' => $id,
			'name' => $name,
			'contact_person' => $contact_person,
			'address'=>$address,
			'cell'=>$cell_no,
			'email'=>$email,
			'vendor_type_id'=>$vendor_type_id]);


		return redirect()->action('MasterController@createVendorMachineWorkSpecification');

	}

	public function createVendorMachineWorkSpecification(Request $request)
	{

		if($request->session()->has('name') == true)
		{
			$vendorMachineID = null; 

			if($request->session()->has('id') == false)
			{
				
				$vendorMachineID = DB::table('m_vendor_machine_details')
				->orderBy('id','desc')
				->first();

				if($vendorMachineID == null)
				{
					$vendorMachineID = (object)array('id' => 0);
				}

				$vendorMachineID = $vendorMachineID->id + 1; 
			}
			else
			{
				$vendorMachineID = Session::get('id');
				$VendorMachineName = Session::get('name');
			}
			
			$vendor_type_id = Session::get('vendor_type_id');

			$machineName = DB::table('m_machine_details')
			->get();

			return view('master.vendors.machine.addWorkSpecificationMachine',compact('VendorMachineName','machineName','vendorMachineID','vendor_machine_id','vendor_type_id'));
		}
		else
		{
			//flash-message
			$request->session()->flash('alert-danger', 'Vendor Name is required!');

			return redirect()->action('MasterController@createVendorGadget');
		}
	}


	public function storeVendorMachineWorkSpecification(Request $request)
	{
		
		$vendor_type_id = $request->get('vendor_type_id');

		$name  = Session::get('name');
		$address  = Session::get('address');
		$contact_person  = Session::get('contact_person');
		$cell  = Session::get('cell');
		$email  = Session::get('email');

		$vendor= new VendorMachineDetails;
		$vendor->name=$name;
		$vendor->address=$address;
		$vendor->contact_person	=$contact_person;
		$vendor->cell_no=$cell;
		$vendor->email	=$email;
		$vendor->vendor_type_id=$vendor_type_id;
		$vendor->save();

		$machine_name_id=$request->get('machine_name_id');
		$rate=$request->get('rate');
		$vendor_machine_id=$request->get('vendor_machine_id');

		foreach ($machine_name_id as $key => $machine_name) {

			$machinevendorspecification= new MachineVendorSpecification;
			$machinevendorspecification->rate=$rate[$key];
			$machinevendorspecification->vendor_type_id=$vendor_type_id;
			$machinevendorspecification->machine_name_id=$machine_name;
			$machinevendorspecification->vendor_machine_id= $vendor_machine_id;
			$machinevendorspecification->save();

		}
		
		//flash-message
		$request->session()->flash('alert-success', 'Machine Vendor was successfully added!');

		return redirect()->action('MasterController@listViewVendorMachine');
	}

	public function editVendorMachine($vendorMachineID,Request $request)
	{
		$vendor_machine_detail = DB::table('m_vendor_machine_workspec_details')
		->join('s_vendor_types','m_vendor_machine_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_vendor_machine_details','m_vendor_machine_details.id','=','m_vendor_machine_workspec_details.vendor_machine_id')
		->where('m_vendor_machine_details.id',$vendorMachineID)
		->select('*','m_vendor_machine_workspec_details.id as m_vendor_machine_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();
		
		$vendor_machine_details = $vendor_machine_detail['0'];

		return view('master.vendors.machine.editVendorMachine', compact('vendor_machine_details','vendorMachineID'));
	}

	public function updateVendorMachine(Request $request)
	{
		$id  = $request->get('vendorMachineID'); 
		$name=$request->get('machine_name');
		$address =$request->get('address');
		$contact_person=$request->get('contact_person');
		$cell_no=$request->get('cell');
		$email=$request->get('email');
		$vendor_type_id=$request->get('vendor_type_id');


		session(['id' => $id,
			'name' => $name,
			'contact_person' => $contact_person,
			'address'=>$address,
			'cell'=>$cell_no,
			'email'=>$email,
			'vendor_type_id'=>$vendor_type_id]);

		return redirect()->action('MasterController@editVendorMachineWorkSpecification',[$id]);


	}

	public function editVendorMachineWorkSpecification($vendorMachineID,Request $request)
	{
		if($request->session()->has('name') == true || $vendorMachineID != null)
		{
			$machine_details = DB::table('m_vendor_machine_details')
			->where('id',$vendorMachineID)
			->get();

			$vendor_type_id = $machine_details[0]->vendor_type_id;

			$machine_workspec_details = DB::table('m_vendor_machine_workspec_details')
			->where('m_vendor_machine_workspec_details.vendor_machine_id',$vendorMachineID)
			->join('s_vendor_types','m_vendor_machine_workspec_details.vendor_type_id','=','s_vendor_types.id')
			->join('m_machine_details','m_vendor_machine_workspec_details.machine_name_id','=','m_machine_details.id')
			->select('*','m_vendor_machine_workspec_details.id as machine_workspec_id','s_vendor_types.name as vendor_type_name')
			->get();


			$machineName = DB::table('m_machine_details')
			->get();

			return view('master.vendors.machine.editWorkSpecificationMachine', compact('machine_workspec_details','machineName','vendorMachineID','vendor_type_id'));

		}
		else
		{
			$request->session()->flash('alert-danger', 'Clinic Name is required!');

			return redirect()->action('MasterController@editVendorMachine',[$vendorMaterialID]);
		}
	}

	public function deleteVendorMachineName($mach_workspec_id, $vendorMaterialID)
	{
		DB::table('m_vendor_machine_workspec_details')
		->where('id', $mach_workspec_id)
		->delete();

		return redirect()->action('MasterController@editVendorMachineWorkSpecification',[$vendorMaterialID]);
	}

	public function updateVendorMachineWorkSpecification(Request $request)
	{
		$vendor_machine_id = $request->get('vendor_machine_id');

		$vendor_type_id = $request->get('vendor_type_id');

		if($request->session()->has('id') == true)
		{
			$name  = Session::get('name');
			$address  = Session::get('address');
			$contact_person  = Session::get('contact_person');
			$cell  = Session::get('cell');
			$email  = Session::get('email');

			DB::table('m_vendor_machine_details')
			->where('id', $vendor_machine_id)
			->update(['name' => $name,
				'address' => $address,
				'contact_person' => $contact_person,
				'cell_no' => $cell,
				'email' => $email, 
				]);

		}

		$machine_name_id=$request->get('machine_name_id');
		$rate=$request->get('rate');
		$vendor_machine_id=$request->get('vendor_machine_id');

		foreach ($machine_name_id as $key => $machine_name) {

			$machinevendorspecification= new MachineVendorSpecification;
			$machinevendorspecification->rate=$rate[$key];
			$machinevendorspecification->vendor_type_id=$vendor_type_id;
			$machinevendorspecification->machine_name_id=$machine_name;
			$machinevendorspecification->vendor_machine_id= $vendor_machine_id;
			$machinevendorspecification->save();

		}

		//flash-message
		$request->session()->flash('alert-info', 'Gadget Vendor was successfully updated!');

		return redirect()->action('MasterController@listViewVendorMachine');
		
		

	}


	public function viewVendorMachine($vendorMachineID,Request $request)
	{
		$vendor_machine_id = $request->get('vendor_machine_id');
		
		$vendor_machine_detail = DB::table('m_vendor_machine_workspec_details')
		->join('s_vendor_types','m_vendor_machine_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_vendor_machine_details','m_vendor_machine_details.id','=','m_vendor_machine_workspec_details.vendor_machine_id')
		->where('m_vendor_machine_details.id',$vendorMachineID)
		->select('*','m_vendor_machine_workspec_details.id as m_vendor_machine_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();
		
		$vendor_machine_details = $vendor_machine_detail['0'];

		return view('master.vendors.machine.viewVendorMachine', compact('vendor_machine_details','vendorMachineID'));


	}

	public function viewVendorMachineWorkSpecification($vendorMachineID, Request $request)
	{
		$machine_workspec_details = DB::table('m_vendor_machine_workspec_details')
		->where('m_vendor_machine_workspec_details.vendor_machine_id',$vendorMachineID)
		->join('s_vendor_types','m_vendor_machine_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_machine_details','m_vendor_machine_workspec_details.machine_name_id','=','m_machine_details.id')
		->select('*','m_vendor_machine_workspec_details.id as machine_workspec_id','s_vendor_types.name as vendor_type_name')
		->get();

		return view('master.vendors.machine.viewWorkSpecificationMachine', compact('machine_workspec_details','machineType','vendorMachineID'));
	}

	public function listViewVendorMaintenance()
	{
		Session::forget('id');
		Session::forget('name');
		Session::forget('address');
		Session::forget('email');
		Session::forget('cell');
		Session::forget('contact_person');


		$vendor_maintenance_details=DB::table('m_vendor_maint_workspec_details')

		->join('m_vendor_maintenance_details','m_vendor_maint_workspec_details.vendor_maintenance_id','=','m_vendor_maintenance_details.id')
		->join('m_gadget_details','m_vendor_maint_workspec_details.gadget_name_id','=','m_gadget_details.id')
		->join('m_machine_details','m_vendor_maint_workspec_details.machine_name_id','=','m_machine_details.id')
		->select('*','m_vendor_maintenance_details.id as maintenance_id')
		->get();

		return view('master.vendors.maintenance.listViewVendorMaintenance',compact('vendor_maintenance_details'));
	}
	
	public function createVendorMaintenace()
	{
		$vendor_type = DB::table('s_vendor_types')
		->where('name','Maintenance')
		->get();

		$vendorMaintenanceID = DB::table('m_vendor_maintenance_details')
		->orderBy('id','desc')
		->first();

		if($vendorMaintenanceID == null)
		{
			$vendorMaintenanceID = (object)array('id' => 0);
		}



		$vendor_type = $vendor_type['0'];
		
		return view('master.vendors.maintenance.addVendorMaintenance',compact('vendor_type','vendorMaintenanceID'));
	}

	public function storeVendorMaintenance(Request $request)
	{
		$id  = $request->get('vendor_maintenance_id'); 
		$name=$request->get('maintenance_name');
		$address =$request->get('address');
		$contact_person=$request->get('contact_person');
		$cell_no=$request->get('cell');
		$email=$request->get('email');
		$vendor_type_id=$request->get('vendor_type_id');


		session(['id' => $id,
			'name' => $name,
			'contact_person' => $contact_person,
			'address'=>$address,
			'cell'=>$cell_no,
			'email'=>$email,
			'vendor_type_id'=>$vendor_type_id]);


		return redirect()->action('MasterController@createVendorMaintenaceWorkSpecification');
	}

	public function createVendorMaintenaceWorkSpecification(Request $request)
	{
		if($request->session()->has('name') == true)
		{
			$vendorMaintenanceID = null; 

			if($request->session()->has('id') == false)
			{
				
				$vendorMaintenanceID = DB::table('m_vendor_maintenance_details')
				->orderBy('id','desc')
				->first();

				if($vendorMaintenanceID == null)
				{
					$vendorMaintenanceID = (object)array('id' => 0);
				}

				$vendorMaintenanceID = $vendorMaintenanceID->id + 1; 
			}
			else
			{
				$vendorMaintenanceID = Session::get('id');
				$VendorMaintenanceName = Session::get('name');
			}
			

			$vendor_type_id = Session::get('vendor_type_id');

			$gadgetName = DB::table('m_gadget_details')
			->get();

			$machineName = DB::table('m_machine_details')
			->get();

			return view('master.vendors.maintenance.addWorkSpecificationMaintenance',compact('VendorMaintenanceName','gadgetName','vendorMaintenanceID','vendor_maintenance_id','machineName','vendor_type_id'));
		}
		else
		{
			//flash-message
			$request->session()->flash('alert-danger', 'Vendor Name is required!');

			return redirect()->action('MasterController@createVendorMaintenace');
		}
	}

	public function storeVendorMaintenanceWorkSpecification(Request $request)
	{
		$vendor_maintenance_id=$request->get('vendor_maintenance_id');

		$vendor_type_id=$request->get('vendor_type_id');

		$name  = Session::get('name');
		$address  = Session::get('address');
		$contact_person  = Session::get('contact_person');
		$cell  = Session::get('cell');
		$email  = Session::get('email');

		$vendor= new VendorMaintenanceDetails;
		$vendor->name=$name;
		$vendor->address=$address;
		$vendor->contact_person	=$contact_person;
		$vendor->cell_no=$cell;
		$vendor->email	=$email;
		$vendor->vendor_type_id=$vendor_type_id;
		$vendor->save();

		$contract_start_date=$request->get('contract_start_date');
		$contract_end_date=$request->get('contract_end_date');
		$contract_amount=$request->get('contract_amount');
		$availability_time=$request->get('availability_time');
		$rate=$request->get('rate');
		$gadget_name_id=$request->get('gadget_name_id');
		$machine_name_id=$request->get('machine_name_id');
		$duration=$request->get('duration');

		foreach ($contract_start_date as $key => $start_date) {

			$maintenancevendorspecification= new MaintenanceVendorSpecification;
			$maintenancevendorspecification->contract_start_date=$start_date;
			$maintenancevendorspecification->contract_end_date=$contract_end_date[$key];
			$maintenancevendorspecification->contract_amount=$contract_amount[$key];
			$maintenancevendorspecification->expected_available_time=$availability_time[$key];
			$maintenancevendorspecification->rate=$rate[$key];
			$maintenancevendorspecification->vendor_type_id=$vendor_type_id;
			$maintenancevendorspecification->gadget_name_id=$gadget_name_id[$key];
			$maintenancevendorspecification->machine_name_id=$machine_name_id[$key];
			$maintenancevendorspecification->vendor_maintenance_id=$vendor_maintenance_id;
			$maintenancevendorspecification->duration=$duration[$key];
			$maintenancevendorspecification->save();

		}

		//flash-message
		$request->session()->flash('alert-success', 'Maintenace Vendor was successfully added!');

		return redirect()->action('MasterController@listViewVendorMaintenance');

	}

	public function editVendorMaintenance($vendorMaintenanceID,Request $request)
	{

		$vendor_maintenance_detail = DB::table('m_vendor_maint_workspec_details')
		->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','m_vendor_maint_workspec_details.vendor_maintenance_id')
		->where('m_vendor_maintenance_details.id',$vendorMaintenanceID)
		->select('*','m_vendor_maint_workspec_details.id as m_vendor_maintenance_workspec_id')
		->get();

		$vendor_maintenance_details = $vendor_maintenance_detail['0'];
		
		return view('master.vendors.maintenance.editVendorMaintenance', compact('vendor_maintenance_details','vendorMaintenanceID'));
	}

	public function updateVendorMaintenance(Request $request)
	{

		$id  = $request->get('vendor_maintenance_id'); 
		$name=$request->get('maintenance_name');
		$address =$request->get('address');
		$contact_person=$request->get('contact_person');
		$cell_no=$request->get('cell');
		$email=$request->get('email');
		$vendor_type_id=$request->get('vendor_type_id');


		session(['id' => $id,
			'name' => $name,
			'contact_person' => $contact_person,
			'address'=>$address,
			'cell'=>$cell_no,
			'email'=>$email,
			'vendor_type_id'=>$vendor_type_id]);

		return redirect()->action('MasterController@editVendorMaintenanceWorkSpecification',[$id]);

	}

	public function editVendorMaintenanceWorkSpecification($vendorMaintenanceID, Request $request)
	{
		if($request->session()->has('name') == true || $vendorMaintenanceID != null)
		{
			$maintenance_details = DB::table('m_vendor_maintenance_details')
			->where('id',$vendorMaintenanceID)
			->get();

			$vendor_type_id = $maintenance_details[0]->vendor_type_id;

			$maintenance_workspec_details = DB::table('m_vendor_maint_workspec_details')
			->where('m_vendor_maint_workspec_details.vendor_maintenance_id',$vendorMaintenanceID)
			->join('s_vendor_types','m_vendor_maint_workspec_details.vendor_type_id','=','s_vendor_types.id')
			->join('m_gadget_details','m_vendor_maint_workspec_details.gadget_name_id','=','m_gadget_details.id')
			->join('m_machine_details','m_vendor_maint_workspec_details.machine_name_id','=','m_machine_details.id')
			->select('*','m_vendor_maint_workspec_details.id as maintenance_workspec_id')
			->get();

			$gadgetName = DB::table('m_gadget_details')
			->get();

			$machineName = DB::table('m_machine_details')
			->get();

			return view('master.vendors.maintenance.editWorkSpecificationMaintenance', compact('maintenance_workspec_details','gadgetName','machineName','vendorMaintenanceID','vendor_type_id'));

		}
		else
		{
			$request->session()->flash('alert-danger', 'Vendor Name is required!');

			return redirect()->action('MasterController@editVendorMaintenance',[$vendorMaintenanceID]);
		}
	}

	public function deleteVendorMaintenanceWork($maint_workspec_id, $vendorMaintenanceID)
	{
		DB::table('m_vendor_maint_workspec_details')
		->where('id', $maint_workspec_id)
		->delete();

		return redirect()->action('MasterController@editVendorMaintenanceWorkSpecification',[$vendorMaintenanceID]);
	}

	public function updateVendorMaintenanceWorkSpecification(Request $request)
	{

		$vendor_maintenance_id = $request->get('vendor_maintenance_id');

		$vendor_type_id = $request->get('vendor_type_id');

		if($request->session()->has('id') == true)
		{
			$name  = Session::get('name');
			$address  = Session::get('address');
			$contact_person  = Session::get('contact_person');
			$cell  = Session::get('cell');
			$email  = Session::get('email');

			DB::table('m_vendor_maintenance_details')
			->where('id', $vendor_maintenance_id)
			->update(['name' => $name,
				'address' => $address,
				'contact_person' => $contact_person,
				'cell_no' => $cell,
				'email' => $email, 
				]);

		}

		$contract_start_date=$request->get('contract_start_date');
		$contract_end_date=$request->get('contract_end_date');
		$contract_amount=$request->get('contract_amount');
		$availability_time=$request->get('availability_time');
		$rate=$request->get('rate');
		$gadget_name_id=$request->get('gadget_name_id');
		$machine_name_id=$request->get('machine_name_id');
		$duration=$request->get('duration');

		foreach ($contract_start_date as $key => $start_date) {

			$maintenancevendorspecification= new MaintenanceVendorSpecification;
			$maintenancevendorspecification->contract_start_date=$start_date;
			$maintenancevendorspecification->contract_end_date=$contract_end_date[$key];
			$maintenancevendorspecification->contract_amount=$contract_amount[$key];
			$maintenancevendorspecification->expected_available_time=$availability_time[$key];
			$maintenancevendorspecification->rate=$rate[$key];
			$maintenancevendorspecification->vendor_type_id=$vendor_type_id;
			$maintenancevendorspecification->gadget_name_id=$gadget_name_id[$key];
			$maintenancevendorspecification->machine_name_id=$machine_name_id[$key];
			$maintenancevendorspecification->vendor_maintenance_id=$vendor_maintenance_id;
			$maintenancevendorspecification->duration=$duration[$key];
			$maintenancevendorspecification->save();

		}
		
		//flash-message
		$request->session()->flash('alert-info', 'Maintenance Vendor was successfully updated!');

		return redirect()->action('MasterController@listViewVendorMaintenance');

	}

	public function viewVendorMaintenance($vendorMaintenanceID,Request $request)
	{
		$vendor_maintenance_detail = DB::table('m_vendor_maint_workspec_details')
		->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','m_vendor_maint_workspec_details.vendor_maintenance_id')
		->where('m_vendor_maintenance_details.id',$vendorMaintenanceID)
		->select('*','m_vendor_maint_workspec_details.id as m_vendor_maintenance_workspec_id')
		->get();

		$vendor_maintenance_details = $vendor_maintenance_detail['0'];

		return view('master.vendors.maintenance.viewVendorMaintenance', compact('vendor_maintenance_details','vendorMaintenanceID'));

	}
	public function viewVendorMaintenanceWorkSpecification($vendorMaintenanceID,Request $request)
	{
		$maintenance_workspec_details = DB::table('m_vendor_maint_workspec_details')
		->where('m_vendor_maint_workspec_details.vendor_maintenance_id',$vendorMaintenanceID)
		->join('s_vendor_types','m_vendor_maint_workspec_details.vendor_type_id','=','s_vendor_types.id')
		->join('m_gadget_details','m_vendor_maint_workspec_details.gadget_name_id','=','m_gadget_details.id')
		->join('m_machine_details','m_vendor_maint_workspec_details.machine_name_id','=','m_machine_details.id')
		->select('*','m_vendor_maint_workspec_details.id as maintenance_workspec_id')
		->get();

		return view('master.vendors.maintenance.viewWorkSpecificationMaintenance', compact('maintenance_workspec_details','vendorMaintenanceID'));
	}

	/********************** End of Vendor ************************/


	public function listViewStaffRegistration()
	{
		$staff_reg_details = DB::table('m_staff_registration')
		->where('m_staff_clinic.clinic_type_id', 1)
		->join('m_staff_clinic','m_staff_clinic.staff_reg_id','=','m_staff_registration.id')
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->join('s_staff_type','m_staff_registration.staff_type_id','=','s_staff_type.id')
		->select('*','s_staff_type.name as staff_type','m_staff_registration.name as staff_name','m_staff_registration.id as staff_registration_id','m_clinic.name as primary_clinic')
		->get();

		$visiting_clinic_name=DB::table('m_staff_clinic')
		->where('m_staff_clinic.clinic_type_id', 2)
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->select('*','m_clinic.name as clinic_name','m_staff_clinic.id as id')
		->get();

		return view('master.clinic.staffregistration.listViewStaffRegistration',compact('staff_reg_details','visiting_clinic_name'));
	}

	public function createStaffRegistration()
	{  
		$clinic_details=DB::table('m_clinic')
		->select('*')
		->get();

		$staff_type=DB::table('s_staff_type')
		->get();

		$clinic_type=DB::table('dnf_clinic_type')
		->get();

		return view('master.clinic.staffregistration.addStaffRegistration',compact('staff_type','clinic_details','staffreg_ID','clinic_type'));
	}

	public function storeStaffRegistration(Request $request)
	{
		$name=$request->get('name');
		$staff_type_name =$request->get('staff_type_name');
		$staff_type_id =$request->get('staff_type_id');
		$cell_no=$request->get('cell_no');
		$gender=$request->get('gender');
		$date_of_birth=$request->get('date_of_birth');
		$email=$request->get('email');

		$stafftypename=explode(",",$staff_type_id);

		$staff_type_id=$stafftypename[0];
		$staff_type_name=$stafftypename[1];

		$clinic1= new GeneralStaffRegistration;
		$clinic1->staff_type_id=$staff_type_id;
		$clinic1->name=$name;
		$clinic1->gender=$gender;
		$clinic1->date_of_birth=$date_of_birth;
		$clinic1->email=$email;
		$clinic1->cell_no=$cell_no;
		$clinic1->save();

		$staffId = DB::table('m_staff_registration')
		->orderBy('id','desc')
		->select('id')
		->first();

		$primary_clinic_id = $request->get('primary_clinic_id');
		$visitingClinic = $request->get('visiting_clinic_id');

		$clinic_type_id = $request->get('clinic_type_id');

		if($clinic_type_id[0] == 1)
		{
			$staffclinic=new StaffClinic;
			$staffclinic->clinic_id=$primary_clinic_id;
			$staffclinic->clinic_type_id=$clinic_type_id[0];
			$staffclinic->staff_reg_id=$staffId->id;
			$staffclinic->save();
		}

		if($clinic_type_id[1] == 2)
		{
			foreach ($visitingClinic as  $key =>$clinic) 
			{
				if($clinic != null)
				{
					$staffclinic=new StaffClinic;
					$staffclinic->clinic_id=$clinic;
					$staffclinic->clinic_type_id=$clinic_type_id[1];
					$staffclinic->staff_reg_id=$staffId->id;
					$staffclinic->save();
				}
			}
		}

		return redirect()->action('MasterController@createStaffRegistrationSlotTimeDetails');
	}

	public function createStaffRegistrationSlotTimeDetails(Request $request)
	{
	//to get staff id and primiray clinic name
		$staffregDetails = DB::table('m_staff_registration')
		->orderBy('id','desc')
		->select('*')
		->first();

		$staffreg_ID = $staffregDetails->id;

		$primary_clinic_details = DB::table('m_staff_clinic')
		->where('staff_reg_id', $staffreg_ID)
		->where('clinic_type_id', 1)
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->select('*','m_clinic.name as primary_clinic','m_staff_clinic.id as primary_clinic_id')
		->get();

		$primary_clinic_id = $primary_clinic_details[0]->primary_clinic_id;

		$primary_clinic_name = $primary_clinic_details[0]->primary_clinic;

	//to get visiting clinic details for above taken staff-id
		$visiting_clinic_details = DB::table('m_staff_clinic')
		->where('staff_reg_id',$staffreg_ID)
		->where('clinic_type_id', 2)
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->select('*','m_clinic.name as clinic_name','m_staff_clinic.id as id')
		->get();

		$k=count($visiting_clinic_details);

		$keys = array_keys($visiting_clinic_details);

		for($i=0; $i < count($keys); $i++){          							
			foreach ($visiting_clinic_details as $name => $value) {
				if($keys[$i] == $value->clinic_name)
				{    

					$visitingclinic_name = $value->clinic_name;
					$visitingclinic_id = $value->id;

				}


			}
		}

		return view('master.clinic.staffregistration.slotStaffRegistration',compact('staffreg_ID','primary_clinic_id','visiting_clinic_details','primary_clinic_name'));
	}


	public function createPrimaryClinicSlotTimeDetails($primary_clinic_id, Request $request)
	{
	//to get staff id and primiray clinic name
		$staffClinicDetails = DB::table('m_staff_clinic')
		->where('m_staff_clinic.id', $primary_clinic_id)
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->select('*','m_staff_clinic.id as id','m_clinic.name as primary_clinic')
		->first();

		$primary_clinic_name = $staffClinicDetails->primary_clinic;

		$primary_clinic_slot_timings = DB::table('m_staff_slot_details')
		->where('staff_clinic_id',$primary_clinic_id)
		->where('status','open')
		->join('m_staff_slot_timing','m_staff_slot_timing.staff_slot_id','=','m_staff_slot_details.id')
		->select('*','m_staff_slot_details.id as primary_clinic_slot_id','m_staff_slot_timing.id as p_slot_timing_id')
		->get();

		return view('master.clinic.staffregistration.primaryclinic.primaryClinicTimeSlots',compact('primary_clinic_id','visiting_clinicID','primary_clinic_slot_timings','primary_clinic_name'));
	}

	public function storeClinicSlotTimeDetails(Request $request)
	{
		$primary_clinic_id = $request->get('primary_clinic_id');

		$day = $request->get('day');
		$time=$request->get('time');

		$keys = array_keys($time);

//save details to m_primary_clinic_slot_details
		for($k = 0; $k < count($keys);$k++)
		{
			foreach($time[$keys[$k]] as $key => $value)
			{
				if($value != null && ($key%2 ==0))
				{ 
					$staffslotdetails = new StaffSlotDetails;
					$staffslotdetails->clinic_day = $keys[$k];
					$staffslotdetails->staff_clinic_id = $primary_clinic_id;
					$staffslotdetails->save();

					break;
				}
			}

		}

//save details to m_primary_clinic_staff_slot_timing
		$primaryclinicSlotDetails = DB::table('m_staff_slot_details')
		->where('staff_clinic_id',$primary_clinic_id)
		->select('*')
		->get();

		for($k = 0; $k < count($keys);$k++)
		{
			//to select Id for perticular day
			foreach($primaryclinicSlotDetails as $key => $value)
			{
				if($keys[$k] == $value->clinic_day)
				{    

					$primaryclinicSlotId = $value->id;
				}
			}

			foreach($time[$keys[$k]] as $key => $value)
			{
				if($value != null && ($key%2 == 0))
				{
					$StaffSlotTiming = new StaffSlotTiming;
					$StaffSlotTiming->slot_start_time = $value; 
					$StaffSlotTiming->slot_end_time = $time[$keys[$k]][$key+1];
					$StaffSlotTiming->status ='open';

					$StaffSlotTiming->staff_slot_id = $primaryclinicSlotId;
					$StaffSlotTiming->save();
				}
			}

		}

		$request->session()->flash('alert-success', 'Staff Registered  successfully added!');

		return redirect()->action('MasterController@createPrimaryClinicSlotTimeDetails',[$primary_clinic_id]);
	}

	public function createVisitingClinicSlotTimeDetails(Request $request, $id)
	{
		$visiting_clinic_name = DB::table('m_staff_clinic')
		->where('m_staff_clinic.id',$id)
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->select('*','m_clinic.name as clinic_name','m_staff_clinic.id as id')
		->get();

		$visitingClinicName=$visiting_clinic_name['0']->clinic_name;

		$visitingClinicID=$visiting_clinic_name['0']->id;

		$staff_reg_id=$visiting_clinic_name['0']->staff_reg_id;

/*		$visiting_clinic_day_id = DB::table('m_visiting_clinic_slot_details')
		->where('visiting_clinic_id',$visitingClinicID)
		->select('id')
		->get();

		foreach($visiting_clinic_day_id as $key => $values) {

			$visiting_clinic_day_id=$values;
		}*/ 

		$visiting_clinic_slot_timings = DB::table('m_staff_slot_details')
		->where('staff_clinic_id',$visitingClinicID)
		->where('status','open')
		->join('m_staff_slot_timing','m_staff_slot_timing.staff_slot_id','=','m_staff_slot_details.id')
		->select('*','m_staff_slot_details.id as visiting_clinic_slot_id','m_staff_slot_timing.id as v_slot_timing_id')
		->get();

		return view('master.clinic.staffregistration.visitingclinic.visitingClinicSlotTimeDetails',compact('staff_reg_id','visiting_clinicID','id','visitingClinicName','visiting_clinic_slot_timings','visitingClinicID','visitingClinicName'));
	}

	public function storeVisitingClinicSlotTimeDetails(Request $request)
	{
		$day = $request->get('day');
		$time=$request->get('time');
		$keys = array_keys($time);
		$visitingclinic_ID=$request->get('visiting_clinic_id');

//save details to m_visiting_clinic_slot_details
		for($k = 0; $k < count($keys);$k++)
		{
			foreach($time[$keys[$k]] as $key => $value)
			{
				if($value != null && ($key%2 ==0))
				{ 

					$staffslotdetails = new StaffSlotDetails;
					$staffslotdetails->clinic_day = $keys[$k];
					$staffslotdetails->staff_clinic_id = $visitingclinic_ID;
					$staffslotdetails->save();

					break;
				}
			}

		}

//save details to m_visiting_clinic_staff_slot_timing
		$visitingclinicSlotDetails = DB::table('m_staff_slot_details')
		->where('staff_clinic_id',$visitingclinic_ID)
		->select('*')
		->get();

		for($k = 0; $k < count($keys);$k++)
		{
			//to select Id for perticular day
			foreach($visitingclinicSlotDetails as $key => $value)
			{
				if($keys[$k] == $value->clinic_day)
				{    
					$visitingclinicSlotId = $value->id;
				}
			}

			foreach($time[$keys[$k]] as $key => $value)
			{
				if($value != null && ($key%2 ==0))
				{
					$staffslottiming = new staffslottiming;
					$staffslottiming->slot_start_time = $value; 
					$staffslottiming->slot_end_time = $time[$keys[$k]][$key+1];
					$staffslottiming->status = 'open';
					$staffslottiming->staff_slot_id = $visitingclinicSlotId;
					$staffslottiming->save();
				}
			}

		}

		return redirect()->action('MasterController@createVisitingClinicSlotTimeDetails',[$visitingclinic_ID]);
	}

	public function editStaffRegistration(Request $request,$staff_reg_id)
	{
		$clinic_details=DB::table('m_clinic')
		->select('*')
		->get();

		$clinic_type=DB::table('dnf_clinic_type')
		->get();

		$staff_registration_details=DB::table('m_staff_registration')
		->join('m_staff_clinic','m_staff_clinic.staff_reg_id','=','m_staff_registration.id')
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->join('s_staff_type','m_staff_registration.staff_type_id','=','s_staff_type.id')
		->where('m_staff_registration.id',$staff_reg_id)
		->select('*','s_staff_type.id as staff_type_id','m_staff_registration.name as staff_name','s_staff_type.name as staff_type','m_staff_registration.id as staff_reg_id','m_clinic.name as primary_clinic','m_staff_clinic.id as primary_clinic_id')
		->get();

		$staff_registration_details = $staff_registration_details['0'];

		$visiting_clinic_details=DB::table('m_staff_clinic')
		->where('staff_reg_id',$staff_reg_id)
		->where('clinic_type_id', 2)
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->select('*','m_clinic.name as clinic_name','m_staff_clinic.id as id')
		->get();

		return view('master.clinic.staffregistration.editStaffRegistration',compact('staff_registration_details','visiting_clinic_details','clinic_details','staff_reg_id','clinic_type'));
	}

	public function updateStaffRegistration(Request $request)
	{
		$staff_reg_id=$request->get('staff_reg_id');

		$staff_type  = $request->get('staff_type'); 
		$dob=$request->get('date_of_birth');
		$staff_name =$request->get('name');
		$staff_type_id=$request->get('staff_type_id');
		$email=$request->get('email');
		$cell_no=$request->get('cell_no');
		$primary_clinic_id=$request->get('primary_clinic_id');
		$gender=$request->get('gender');
		$visiting_clinic_id=$request->get('visiting_clinic_id');
		$clinic_type_id = $request->get('clinic_type_id');

	//update staff registration table
		DB::table('m_staff_registration')
		->where('id', $staff_reg_id)
		->update(['name' => $staff_name,
			'gender' => $gender,
			'cell_no' => $cell_no,
			'date_of_birth' => $dob,
			'email' => $email, 
			'staff_type_id'=>$staff_type_id
			]);

	//update primary clinic tuple present in staff clinic table
		$primary_staff_clinic = DB::table('m_staff_clinic')
		->where('staff_reg_id',$staff_reg_id)
		->where('clinic_type_id', 1)
		->select('*')
		->get();

	//if primary clinic is changed
		if($primary_clinic_id != $primary_staff_clinic[0]->clinic_id)
		{
			$staff_clinic_id = $primary_staff_clinic[0]->id;

		//take slot details for staff clinic id defined above
			$staff_slot_details = DB::table('m_staff_slot_details')
			->where('staff_clinic_id', $staff_clinic_id)
			->get();

		//delete each and every slot assigned to previous primary clinic id
			foreach ($staff_slot_details as $key => $staff_clinic) 
			{
			//delete slots
				DB::table('m_staff_slot_details')
				->where('staff_clinic_id', $staff_clinic->staff_clinic_id)
				->delete();
			}

		//update primary clinic id with new id
			DB::table('m_staff_clinic')
			->where('id', $staff_clinic_id)
			->update(['clinic_id' => $primary_clinic_id
				]);

		}

	//update visiting clinic tuple present in staff clinic table
		$visiting_staff_clinic = DB::table('m_staff_clinic')
		->where('staff_reg_id',$staff_reg_id)
		->where('clinic_type_id', 2)
		->select('*')
		->get();

	//if visting staff clinic is null, then value should get added
		if($visiting_staff_clinic == null && $clinic_type_id[1] == 2)
		{
			foreach ($visiting_clinic_id as  $key =>$clinic) 
			{
				if($clinic != null)
				{
				//add
					$staffclinic=new StaffClinic;
					$staffclinic->clinic_id=$clinic;
					$staffclinic->clinic_type_id=$clinic_type_id[1];
					$staffclinic->staff_reg_id=$staff_reg_id;
					$staffclinic->save();
				}
			}
		}

	//to change the value of variable $visiting_staff_clinic
		$visiting_staff_clinic = DB::table('m_staff_clinic')
		->where('staff_reg_id',$staff_reg_id)
		->where('clinic_type_id', 2)
		->select('*')
		->get();

		foreach($visiting_staff_clinic as $key => $val)
		{
		// Search clinic id which is present in $visiting_clinic_id
			$pos = array_search($val->clinic_id, $visiting_clinic_id);

		// Remove from array which is already present in table
			unset($visiting_clinic_id[$pos]);
		}

	//updated array to add in staff clinic table
		$visiting_clinic_id = array_values($visiting_clinic_id);

		if($clinic_type_id[1] == 2)
		{
			foreach ($visiting_clinic_id as  $key =>$clinic) 
			{
				if($clinic != null)
				{
				//add
					$staffclinic=new StaffClinic;
					$staffclinic->clinic_id=$clinic;
					$staffclinic->clinic_type_id=$clinic_type_id[1];
					$staffclinic->staff_reg_id=$staff_reg_id;
					$staffclinic->save();
				}
			}
		}

		return redirect()->action('MasterController@editStaffRegistrationSlotTimeDetails',[$staff_reg_id]);
	}

	public function editStaffRegistrationSlotTimeDetails($staffreg_ID, Request $request)
	{
	//to get staff id and primiray clinic name
		$staffregDetails = DB::table('m_staff_registration')
		->where('m_staff_registration.id', $staffreg_ID)
		->where('clinic_type_id', 1)
		->join('m_staff_clinic','m_staff_clinic.staff_reg_id','=','m_staff_registration.id')
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->select('m_staff_registration.id as id','m_clinic.name as primary_clinic','m_staff_clinic.id as primary_clinic_id')
		->get();

		$primary_clinic_id = $staffregDetails[0]->primary_clinic_id;

		$primary_clinic_name = $staffregDetails[0]->primary_clinic;

	//to get visiting clinic details for above taken staff-id
		$visiting_clinic_details = DB::table('m_staff_clinic')
		->where('staff_reg_id',$staffreg_ID)
		->where('clinic_type_id', 2)
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->select('*','m_clinic.name as clinic_name','m_staff_clinic.id as id')
		->get();

		$k=count($visiting_clinic_details);

		$keys = array_keys($visiting_clinic_details);

		for($i=0; $i < count($keys); $i++){          							
			foreach ($visiting_clinic_details as $name => $value) {
				if($keys[$i] == $value->clinic_name)
				{    

					$visitingclinic_name = $value->clinic_name;
					$visitingclinic_id = $value->id;

				}


			}
		}
		return view('master.clinic.staffregistration.editSlotStaffRegistration',compact('staffreg_ID','primary_clinic_id','visiting_clinic_details','primary_clinic_name'));
	}

	public function deleteVisitingClinic($staff_clinic_id, $staff_reg_id)
	{
		DB::table('m_staff_clinic')
		->where('id', $staff_clinic_id)
		->delete();

		return redirect()->action('MasterController@editStaffRegistration',[$staff_reg_id]);
	}

	public function viewStaffRegistration(Request $request, $staff_reg_id)
	{
		$staff_registration_details=DB::table('m_staff_registration')
		->where('m_staff_registration.id',$staff_reg_id)
		->where('clinic_type_id', 1)
		->join('m_staff_clinic','m_staff_clinic.staff_reg_id','=','m_staff_registration.id')
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->join('s_staff_type','m_staff_registration.staff_type_id','=','s_staff_type.id')
		->select('*','s_staff_type.id as staff_type_id','m_staff_registration.name as staff_name','s_staff_type.name as staff_type','m_staff_registration.id as staff_reg_id','m_clinic.name as primary_clinic')
		->get();

		$staff_registration_details = $staff_registration_details['0'];

		$visiting_clinic_details=DB::table('m_staff_clinic')
		->where('staff_reg_id',$staff_reg_id)
		->where('clinic_type_id', 2)
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->select('*','m_clinic.name as clinic_name')
		->get();

		return view('master.clinic.staffregistration.viewStaffRegistration',compact('staff_registration_details','visiting_clinic_details','staff_reg_id'));
	}

	public function viewPrimaryVisitingClinic(Request $request,$staff_reg_id)
	{
		$primaryClinicDetails = DB::table('m_staff_registration')
		->where('m_staff_registration.id',$staff_reg_id)
		->where('clinic_type_id', 1)
		->join('m_staff_clinic','m_staff_clinic.staff_reg_id','=','m_staff_registration.id')
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->select('*','m_clinic.name as primary_clinic')
		->first();

		$primary_clinic_name = $primaryClinicDetails->primary_clinic;

//to get visiting clinic details for above taken staff-id
		$visiting_clinic_details = DB::table('m_staff_clinic')
		->where('staff_reg_id',$staff_reg_id)
		->where('clinic_type_id', 2)
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->select('*','m_staff_clinic.id as id','m_clinic.name as clinic_name')
		->get();

		$k=count($visiting_clinic_details);

		$keys = array_keys($visiting_clinic_details);

		for($i=0; $i < count($keys); $i++){          							
			foreach ($visiting_clinic_details as $name => $value) {
				if($keys[$i] == $value->clinic_name)
				{    
					$visitingclinic_name = $value->clinic_name;
					$visitingclinic_id = $value->id;
				}
			}
		}

		return view('master.clinic.staffregistration.viewPrimaryVisitingClinicSlotDetails',compact('primary_clinic_name','visiting_clinic_details','primaryClinicDetails','staff_reg_id'));
	}

	public function viewPrimaryClinicSlotTimeDetails(Request $request, $staff_reg_id)
	{
		$staffregDetails = DB::table('m_staff_registration')
		->where('m_staff_registration.id',$staff_reg_id)
		->where('clinic_type_id', 1)
		->join('m_staff_clinic','m_staff_clinic.staff_reg_id','=','m_staff_registration.id')
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->select('*','m_clinic.name as primary_clinic','m_staff_clinic.id as staff_clinic_id')
		->get();

		$primary_clinic_name = $staffregDetails[0]->primary_clinic;

		$staff_clinic_id = $staffregDetails[0]->staff_clinic_id;

		$primary_clinic_slot_timings = DB::table('m_staff_slot_details')
		->where('m_staff_slot_details.staff_clinic_id',$staff_clinic_id)
		->where('status','open')
		->join('m_staff_slot_timing','m_staff_slot_timing.staff_slot_id','=','m_staff_slot_details.id')
		->select('*','m_staff_slot_details.id as primary_clinic_slot_id','m_staff_slot_timing.id as p_slot_timing_id')
		->get();

		return view('master.clinic.staffregistration.primaryclinic.viewPrimaryClinicTimeSlots',compact('primary_clinic_name','primary_clinic_slot_timings','staff_reg_id'));		
	}

	public function viewVisitingClinicSlotTimeDetails(Request $request,$staff_reg_id)
	{
		$visiting_clinic_name = DB::table('m_staff_clinic')
		->where('m_staff_clinic.id',$staff_reg_id)
		->where('clinic_type_id', 2)
		->join('m_clinic','m_clinic.id','=','m_staff_clinic.clinic_id')
		->select('*','m_staff_clinic.id as id','m_clinic.name as clinic_name')
		->get();

		$visitingClinicName=$visiting_clinic_name['0']->clinic_name;

		$visitingClinicID=$visiting_clinic_name['0']->id;

		$staff_reg_id=$visiting_clinic_name['0']->staff_reg_id;

	/*	$visiting_clinic_day_id = DB::table('m_visiting_clinic_slot_details')
		->where('visiting_clinic_id',$visitingClinicID)
		->select('id')
		->get();

		foreach($visiting_clinic_day_id as $key => $values)
		{
			$visiting_clinic_day_id=$values;
		} */

		$visiting_clinic_slot_timings = DB::table('m_staff_slot_details')
		->where('staff_clinic_id',$visitingClinicID)
		->where('status','open')
		->join('m_staff_slot_timing','m_staff_slot_timing.staff_slot_id','=','m_staff_slot_details.id')
		->select('*','m_staff_slot_details.id as visiting_clinic_slot_id','m_staff_slot_timing.id as v_slot_timing_id')
		->get();

		return view('master.clinic.staffregistration.visitingclinic.viewVisitingClinicTimeSlots',compact('staff_reg_id','visiting_clinicID','visitingClinicName','visiting_clinic_slot_timings','visitingClinicID','visitingClinicName'));
	}

	public function listViewLeaveManagement()
	{
		$leave_full=DB::table('m_leave_full_day')
		->leftjoin('m_staff_registration','m_staff_registration.id','=','m_leave_full_day.staff_id')
		->select('*','m_staff_registration.name as staff_name','m_leave_full_day.id as full_day_id')
		->get();

		$leave_half=DB::table('m_leave_partial_days')
		->leftjoin('m_staff_registration','m_staff_registration.id','=','m_leave_partial_days.staff_id')
		->select('*','m_staff_registration.name as staff_name','m_leave_partial_days.id as half_day_id')
		->get();

		$leaves = array_merge($leave_full,$leave_half);

		return view('master.clinic.leavemanagement.listViewLeaveManagement',compact('leaves'));
	}

	public function createLeaveManagement()
	{
		$staffID = DB::table('m_staff_registration')
		->orderBy('id','desc')
		->first();

		if($staffID == null)
		{
			$staffID = (object)array('id' => 0);
		}

		$staffID = $staffID->id + 1; 

		return view('master.clinic.leavemanagement.addLeaveManagement');
	}
	public function storeLeaveManagement(Request $request)
	{

		$StaffID=$request->get('staff_type');
		$FromDate=$request->get('from_full_date');
		$ToDate=$request->get('to_full_date');

		$leavefullday = new LeaveFullDay;
		$leavefullday ->staff_id = $request->get('staff_type');
		$leavefullday ->date_from = $request->get('from_full_date');
		if ($FromDate != null){
			$leavefullday ->date_to = $request->get('to_full_date');
			$leavefullday ->save();
		}

		$Date=$request->get('date');
		$From_Time=$request->get('from_time');
		$To_Time=$request->get('to_time');

		$leavepartialdays = new LeavePartialDays;
		$leavepartialdays ->staff_id = $request->get('staff_type');
		$leavepartialdays ->date = $request->get('date');

		if ($Date != null){
			$leavepartialdays ->time_from = $request->get('from_time');
			$leavepartialdays ->time_to = $request->get('to_time');
			$leavepartialdays ->save();
		}

		$request->session()->flash('alert-success', 'Leave was successfully added!');

		return redirect() -> action('MasterController@listViewLeaveManagement');
	}

	public function editStaffFullDayLeaveDetails($staff_id)
	{
		$leaves_full=DB::table('m_leave_full_day')
		->where('id', $staff_id) 
		->get();

		$leave_full = $leaves_full[0];

		$leaves_Partial =DB::table('m_leave_partial_days')
		->where('id', $staff_id) 
		->get();

		$leave_Partial = $leaves_Partial;

		return view('master.clinic.leavemanagement.editLeaveManagement',compact('staff_id','leave_full','leave_Partial'));
	}

	public function editStaffPartialLeaveDetails($staff_id)
	{
		$leaves_full=DB::table('m_leave_full_day')
		->where('id', $staff_id) 
		->get();

		$leave_full = $leaves_full;

		$leaves_Partial =DB::table('m_leave_partial_days')
		->where('id', $staff_id) 
		->get();

		$leave_Partial = $leaves_Partial[0];

		return view('master.clinic.leavemanagement.editLeaveManagementhalfDay',compact('leave_Partial','leave_full','staff_id'));

	}
	public function updateStaffFullDayLeaveDetails(Request $request)
	{
		$staff_id=$request->get('staff_id');

		$leaves_full=DB::table('m_leave_full_day')
		->get();

		$leave_full = $leaves_full[0];


		DB::table('m_leave_full_day')
		->where('id', $staff_id)
		->update([
			'date_from' => $request->get('from_full_date'),
			'date_to' => $request->get('to_full_date')
			]);

		$request->session()->flash('alert-info', 'Leave was successfully updated!');

		return redirect() -> action('MasterController@listViewLeaveManagement');

	}
	public function updateStaffPartialLeaveDetails(Request $request)
	{

		$leaves_Partial =DB::table('m_leave_partial_days')
		->get();

		$leave_Partial = $leaves_Partial[0];

		$staff_id=$request->get('staff_id');

		DB::table('m_leave_partial_days')
		->where('id', $staff_id)
		->update([
			'date' => $request->get('date'),
			'time_to' => $request->get('to_time'),
			'time_from' => $request->get('from_time')
			]);

		$request->session()->flash('alert-info', 'Leave was successfully updated!');

		return redirect() -> action('MasterController@listViewLeaveManagement');

	}

	public function destroyStaffFullDayLeaveDetails($id, Request $request)
	{


		$delet_leaves_full=DB::table('m_leave_full_day')
		->where('id', $id)
		->delete();

		$request->session()->flash('alert-danger', 'Leave was successfully deleted!');

		return redirect() -> action('MasterController@listViewLeaveManagement');
	}

	public function destroyStaffHalfDayLeaveDetails($id, Request $request)
	{


		DB::table('m_leave_partial_days')
		->where('id', $id)
		->delete();

		$request->session()->flash('alert-danger', 'Leave was successfully deleted!');

		return redirect() -> action('MasterController@listViewLeaveManagement');
	}

	public function listViewClinic(Request $request)
	{
		Session::forget('id');
		Session::forget('name');
		Session::forget('address');
		Session::forget('location');

		$clinic_details=DB::table('m_clinic')
		->select('*')
		->get();

		$clinic_slot_details=DB::table('m_clinic_slot_details')
		->get();

		return view('master.clinic.clinic.listViewClinic',compact('clinic_details','clinic_slot_details'));
	}

	public function createClinicDetails()
	{
		$clinicID = DB::table('m_clinic')
		->orderBy('id','desc')
		->first();

		if($clinicID == null)
		{
			$clinicID = (object)array('id' => 0);
		}

		return view('master.clinic.clinic.addClinicDetails',compact('clinicID'));
	}

	public function storeClinicDetails(Request $request)
	{
		$id  = $request->get('clinic_id');
		$name  = $request->get('name');
		$location  = $request->get('location');
		$address  = $request->get('address');

		session(['id' => $id,
			'name' => $name,
			'location' => $location,
			'address'=>$address]);

		return redirect()->action('MasterController@createClinicWorkTimings');
	}

	public function createClinicWorkTimings(Request $request)
	{
		if($request->session()->has('name') == true)
		{
			$clinicID = null; 

			if($request->session()->has('id') == false)
			{
				$clinicID = DB::table('m_clinic')
				->orderBy('id','desc')
				->first();

				if($clinicID == null)
				{
					$clinicID = (object)array('id' => 0);
				}

				$clinicID = $clinicID->id + 1; 
			}
			else
			{
				$clinicID = Session::get('id');
				$clinicName = Session::get('name');
			}

			return view('master.clinic.clinic.addClinicWorkTime',compact('clinicID','clinicName'));
		}
		else
		{
			//flash-message
			$request->session()->flash('alert-danger', 'Clinic Name is required!');

			return redirect()->action('MasterController@createClinicDetails');
		}
	}

	public function storeClinicWorkTimings(Request $request)
	{
		$id  = $request->get('clinic_id');

	//save details to m_clinic table
		if($request->session()->has('id') == false)
		{
			$name  = '';
			$location  = '';
			$address  = '';
		}
		else
		{
			$name  = Session::get('name');
			$location  = Session::get('location');
			$address  = Session::get('address');
		}

		$clinic1=new ClinicDetails;
		$clinic1->name=$name;
		$clinic1->location=$location;
		$clinic1->address=$address;
		$clinic1->save();

		$day = $request->get('day');
		$time = $request->get('time');


		$keys = array_keys($time);

//save details to m_clinic_slot_details table
		for($k = 0; $k < count($keys);$k++)
		{
			foreach($time[$keys[$k]] as $key => $value)
			{
				if($value != null && ($key%2 ==0))
				{
					$clinicslotdetails = new ClinicSlotDetails;
					$clinicslotdetails->clinic_day = $keys[$k];
					$clinicslotdetails->clinic_id = $id;
					$clinicslotdetails->save();

					break;
				}
			}

		}

	//save details to m_clinic_slot_timing table
		$clinicSlotDetails = DB::table('m_clinic_slot_details')
		->where('clinic_id',$id)
		->select('*')
		->get();


		for($k = 0; $k < count($keys);$k++)
		{
			//to select Id for perticular day
			foreach($clinicSlotDetails as $key => $value)
			{
				if($keys[$k] == $value->clinic_day)
				{
					$clinicSlotId = $value->id;
				}
			}

			foreach($time[$keys[$k]] as $key => $value)
			{
				if($value != null && ($key%2 ==0))
				{
					$ClinicSlotTimeDetails = new ClinicSlotTimeDetails;
					$ClinicSlotTimeDetails->slot_start_time = $value; 
					$ClinicSlotTimeDetails->slot_end_time = $time[$keys[$k]][$key+1];
					$ClinicSlotTimeDetails->status = 'open';
					$ClinicSlotTimeDetails->clinic_slot_id = $clinicSlotId;
					$ClinicSlotTimeDetails->save();
				}
			}

		}

		//flash-message
		$request->session()->flash('alert-success', 'Clinic Details was successfully added!');

		return redirect()->action('MasterController@listViewClinic');
	}

	public function editClinicDetails($clinicId, Request $request)
	{
		$clinicDetails = DB::table('m_clinic')
		->where('id',$clinicId)
		->get();

		$clinicDetails = $clinicDetails['0'];

		return view('master.clinic.clinic.editClinicDetails',compact('clinicDetails'));
	}

	public function updateClinicDetails(Request $request)
	{
		$id  = $request->get('clinic_id');
		$name  = $request->get('name');
		$location  = $request->get('location');
		$address  = $request->get('address');

		session(['id' => $id,
			'name' => $name,
			'location' => $location,
			'address'=>$address]);

		return redirect()->action('MasterController@editClinicWorkTimings',[$id]);
	}

	public function editClinicWorkTimings($clinicId, Request $request)
	{

		if($request->session()->has('name') == true || $clinicId != null)
		{

			$clinicSlotDetails = DB::table('m_clinic_slot_details')
			->where('m_clinic_slot_details.clinic_id',$clinicId)
			->join('m_clinic','m_clinic_slot_details.clinic_id','=','m_clinic.id')
			->select('*','m_clinic_slot_details.id as m_clinic_slot_details_id')
			->get();

			$clinicSlotTimeDetails = DB::table('m_clinic_slot_timing')
			->where('status','open')
			->get();

			return view('master.clinic.clinic.editClinicWorkTime',compact('clinicSlotDetails','clinicSlotTimeDetails'));
		}
		else
		{
			//flash-message
			$request->session()->flash('alert-danger', 'Clinic Name is required!');

			return redirect()->action('MasterController@editClinicDetails',[$clinicId]);
		}
	}

	public function makeStatusDeleteWorkTimings($clinicSlotTimingId,$clinicId)
	{
		DB::table('m_clinic_slot_timing')
		->where('id', $clinicSlotTimingId)
		->update(['status' => 'delete'
			]);

		return redirect()->action('MasterController@editClinicWorkTimings',[$clinicId]);
	}

	public function updateClinicWorkTimings(Request $request)
	{
		$clinicId  = $request->get('clinic_id');

	//save details to m_clinic table
		if($request->session()->has('id') == true)
		{
			$name  = Session::get('name');
			$location  = Session::get('location');
			$address  = Session::get('address');

			DB::table('m_clinic')
			->where('id', $clinicId)
			->update(['name' => $name,
				'location' => $location,
				'address' => $address
				]);
		}


		$day = $request->get('day');
		$time = $request->get('time');

		$keys = array_keys($time);

		$newKeys = array_keys($time);

//update details to m_clinic_slot_details table

		$clinicSlotDets = DB::table('m_clinic_slot_details')
		->where('clinic_id',$clinicId)
		->get();

		foreach($clinicSlotDets as $key => $val)
		{
		// Search
			$pos = array_search($val->clinic_day, $newKeys);

		// Remove from array
			unset($newKeys[$pos]);
		}
		$newKeys = array_values($newKeys);

		/*dd($clinicSlotDets);*/
		for($k = 0; $k < count($newKeys);$k++)
		{
			foreach($time[$newKeys[$k]] as $key => $value)
			{
				if($value != null && ($key%2 ==0))
				{
					$clinicslotdetails = new ClinicSlotDetails;
					$clinicslotdetails->clinic_day = $newKeys[$k];
					$clinicslotdetails->clinic_id = $clinicId;
					$clinicslotdetails->save();

					break;
				}
			}

		}

//update details to m_clinic_slot_timing table
		$clinicDetails = DB::table('m_clinic_slot_details')
		->where('m_clinic_slot_details.clinic_id',$clinicId)
		->where('m_clinic_slot_timing.status','open')
		->join('m_clinic_slot_timing','m_clinic_slot_details.id','=','m_clinic_slot_timing.clinic_slot_id')
		->select('*')
		->get();

		$clinicSlotDetails = DB::table('m_clinic_slot_details')
		->where('clinic_id',$clinicId)
		->select('*')
		->get();


		for($k = 0; $k < count($keys);$k++)
		{

			foreach($time[$keys[$k]] as $key => $value)
			{
				foreach($clinicDetails as $key1 => $detail)
				{
					if($detail->id == $key)
					{
						$even = 0;

						//update if already exits
						foreach ($value as $key2 => $val) {
							if($key2%2 == 0)
							{
								DB::table('m_clinic_slot_timing')
								->where('id', $key)
								->update(['slot_start_time' => $value[$even],
									'slot_end_time' => $value[$even+1],
									'status' => 'open',
									'clinic_slot_id' => $detail->clinic_slot_id
									]);
							}
							$even = $even + 2;
						}
						break;
					}

				}

			}

		}

		for($k = 0; $k < count($keys);$k++)
		{
			//to select Id for perticular day
			foreach($clinicSlotDetails as $key => $value)
			{
				if($keys[$k] == $value->clinic_day)
				{
					$clinicSlotId = $value->id;
				}
			}

			foreach($time[$keys[$k]] as $key => $value)
			{
				foreach ($clinicDetails as $key1 => $val) {

					if($value != null && ($key%2 ==0) && is_array($value) != 1)
					{
						$ClinicSlotTimeDetails = new ClinicSlotTimeDetails;
						$ClinicSlotTimeDetails->slot_start_time = $value; 
						$ClinicSlotTimeDetails->slot_end_time = $time[$keys[$k]][$key+1];
						$ClinicSlotTimeDetails->status = 'open';
						$ClinicSlotTimeDetails->clinic_slot_id = $clinicSlotId;
						$ClinicSlotTimeDetails->save();

						break;
					}

				}


			}
		}

		//flash-message
		$request->session()->flash('alert-info', 'Clinic Details was successfully Updated!');

		return redirect()->action('MasterController@listViewClinic');
	}

	public function dashboardBank()
	{
		return view('master.billing.bank.dashboardBank');
	}

	public function storeBillingBank(Request $request)
	{
		$account_holder1=new BillingBank;
		$account_holder1->account_holder=$request->get('account_holder');
		$account_holder1->bank_name=$request->get('bank_name');
		$account_holder1->account_number=$request->get('account_number');
		$account_holder1->branch=$request->get('branch');
		$account_holder1->save();

		//flash-message
		$request->session()->flash('alert-success', 'Bank Details was successfully added!');

		return redirect()->action('MasterController@dashboardBank');
	}

/*public function store_bank_add(Request $request)
{
	$vendor1=new BankDetails;
	$vendor1->account_holder	=$request->get('account_holder');
	$vendor1->bank_name		=$request->get('bank_name');
	$vendor1->account_number	=$request->get('account_number');
	$vendor1->branch	=$request->get('branch');
	$vendor1->save();
	return Redirect::to('master/billing/person/add1')->with('message', 'Successfully!!');
}*/

public function dashboadPersonal()
{
	return view('master.billing.personal.dashboardPersonal');
}

public function storeBillingPersonal(Request $request)
{
	$account_holder1=new BillingPersonal;
	$account_holder1->name=$request->get('name');
	$account_holder1->cell=$request->get('cell');
	$account_holder1->save();
	
	//flash-message
	$request->session()->flash('alert-success', 'Personal was successfully added!');

	return redirect()->action('MasterController@dashboadPersonal');
}

/*public function store_person_add(Request $request)
{
	$vendor1=new PersonDetails;
	$vendor1->person_name	=$request->get('person_name');
	$vendor1->id		=$request->get('staff_id');
	$vendor1->cell	=$request->get('cell');
	
	$vendor1->save();
	return Redirect::to('master/billing/bank/add1')->with('message', 'Successfully!!');

}*/



//***********Ajax function

public function getMaterialSubtype($material_type_id)
{
	$material_subtype = DB::table('s_material_subtype')
	->where("material_type_id", $material_type_id)
	->get();

	return $material_subtype;

}

public function getInstrumentSubtype($instrument_type_id)
{
	$instrument_subtype = DB::table('s_instrument_subtype')
	->where("instrument_type_id", $instrument_type_id)
	->get();

	return $instrument_subtype;
}

public function getMachineSubtype($machine_type_id)
{
	$machine_subtype = DB::table('s_machine_subtype')
	->where("machine_type_id", $machine_type_id)
	->get();

	return $machine_subtype;

}

public function getGadgetSubtype($gadget_type_id)
{
	$gadget_subtype = DB::table('s_gadget_subtype')
	->where("gadget_type_id", $gadget_type_id)
	->get();

	return $gadget_subtype;

}

public function getLabWorkDetails($work_type_id)
{
	$work_name = DB::table('s_lab_work_subtype')
	->join('s_lab_work_type', 's_lab_work_subtype.lab_work_type_id', '=', 's_lab_work_type.id')
	->join('s_lab_work_name', 's_lab_work_name.lab_work_type_id', '=', 's_lab_work_type.id')
	->where("s_lab_work_name.lab_work_type_id", $work_type_id)
	->select('*','s_lab_work_type.name as lab_work_type','s_lab_work_subtype.name as lab_work_subtype')
	->get();

	return $work_name;

}

public function getMaterialDetails($material_type_id)
{
	$material_name = DB::table('s_material_type')
	->join('s_material_subtype', 's_material_subtype.material_type_id', '=', 's_material_type.id')
	->join('m_material_details', 'm_material_details.material_type_id', '=', 's_material_type.id')
	->where("m_material_details.material_type_id", $material_type_id)
	->select('*','s_material_type.name as material_type','s_material_subtype.name as material_subtype')
	->get();

	return $material_name;
}

public function getInstrumentDetails($instrument_type_id)
{
	$instrument_name=DB::table('s_instrument_subtype')
	->join('s_instrument_type', 's_instrument_subtype.instrument_type_id', '=', 's_instrument_type.id')
	->join('m_instrument_details', 'm_instrument_details.instrument_type_id', '=', 's_instrument_type.id')
	->where("m_instrument_details.instrument_type_id", $instrument_type_id)
	->select('*','s_instrument_type.name as instrument_type','s_instrument_subtype.name as instrument_subtype')
	->get();
	
	return $instrument_name;
}

public function getTreatmentName($treatment_type_id)
{
	$treatment_name = DB::table('m_treatment_name')
	->where("treatment_type_id", $treatment_type_id)
	->get();

	return $treatment_name;

}

}
