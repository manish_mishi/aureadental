<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\ValidationException;

use App\Appointment;
use App\DentistDetails;
use App\ChairDetails;
use App\FollowUpDetails;
use App\FollowUpDate;
use App\Patient;
use App\ScheduleDetails;
use DB;

class PlannerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function listView()
	{
		return view('planner.viewplanner.listViewPlanner');
	}

	public function listViewScheduling()

	{
		$dashboardValue = "schedule_dashboard";

		$patient = "schedule_dashboard";

		$appointmentDetails = DB::table('t_appointment')
		->join('m_patient_details','m_patient_details.id','=','t_appointment.patient_id')
		->join('dnf_appointment_status','dnf_appointment_status.id','=','t_appointment.appointment_status_id')
		->join('s_dentist_details','s_dentist_details.id','=','t_appointment.dentist_id')
		->join('s_chair_details','s_chair_details.id','=','t_appointment.chair_id')
		->get();

		return view('planner.scheduling.listViewScheduling',compact('appointmentDetails','dashboardValue','patient'));
	} 

	public function listViewSearchScheduling(Request $request)
	{
		$dashboardValue = "";
		$cell_no=$request->get('cell_no');
		$name=$request->get('name');

//checking entered patient name and cell_no exists or not and if not make entry in patient details table
		$result = Patient::where('name', '=', $name)->where('cell_no', '=', $cell_no)->get();

		if(!count($result))
		{
			$patient = new Patient;

			$patient->name = $name;
			$patient->cell_no = $cell_no;
			$patient->save();

		}

//fetching patient details
		$patientDetails = DB::table('m_patient_details')
		->where('cell_no', $cell_no)
		->where('name', $name)
		->get();

		$patientId = $patientDetails[0]->id;

		$patientName = $patientDetails[0]->name;

		$patientCell = $patientDetails[0]->cell_no;

//checking patient has appointment or not
		$check = Appointment::where('patient_id', '=', $patientId)->get();

		if (!count($check))
		{
			$appointment = "";

			return view('planner.scheduling.listViewScheduling',compact('appointment','patientName','patientCell','dashboardValue','patientId'));
		}

		else
		{

			$appointment = DB::table('t_appointment')
			->where('t_appointment.patient_id', $patientId)
			->join('m_patient_details','m_patient_details.id','=','t_appointment.patient_id')
			->join('dnf_appointment_status','dnf_appointment_status.id','=','t_appointment.appointment_status_id')
			->join('s_dentist_details','s_dentist_details.id','=','t_appointment.dentist_id')
			->join('s_chair_details','s_chair_details.id','=','t_appointment.chair_id')
			->get();

			return view('planner.scheduling.listViewScheduling',compact('appointment','patientName','patientCell','dashboardValue','patientId'));
		}

	} 

	public function listViewAppointmentDetails($patientId)
	{
	//fetching patient details
		$patientDetails = DB::table('m_patient_details')
		->where('id', $patientId)
		->get();

		$patientName = $patientDetails[0]->name;

		$patientCell = $patientDetails[0]->cell_no;

	//to fetch appoinment details
		$appointmentDetails = DB::table('t_appointment')
		->where('t_appointment.patient_id', $patientId)
		->join('m_patient_details','m_patient_details.id','=','t_appointment.patient_id')
		->join('dnf_appointment_status','dnf_appointment_status.id','=','t_appointment.appointment_status_id')
		->join('s_dentist_details','s_dentist_details.id','=','t_appointment.dentist_id')
		->join('s_chair_details','s_chair_details.id','=','t_appointment.chair_id')
		->get();

		return view('planner.scheduling.listViewAppointmentDetails',compact('patientId','patientName','patientCell','appointmentDetails'));
	}

	public function listViewTreatmentDetails($patientId)
	{
	//fetching patient details
		$patientDetails = DB::table('m_patient_details')
		->where('id', $patientId)
		->get();

		$patientName = $patientDetails[0]->name;

		$patientCell = $patientDetails[0]->cell_no;

	//treatment-details
		$patientTreatmentDetails = DB::table('t_patient_treatment')
		->where('t_patient_treatment.patient_id', $patientId)
		->leftJoin('t_treatment_general','t_treatment_general.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('dnf_treatment_status','dnf_treatment_status.id','=','t_treatment_general.status_id')
		->leftJoin('s_dentist_details','s_dentist_details.id','=','t_treatment_general.dentist_name_id')
		->leftJoin('t_treatment_consultant','t_treatment_consultant.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_quotation','t_treatment_quotation.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_visit_clinic','t_treatment_visit_clinic.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_findings','t_treatment_findings.patient_treatment_id', '=', 't_patient_treatment.id')
		->select('*','t_treatment_general.clinic_id as general_clinic_id','t_treatment_visit_clinic.clinic_id as visit_clinic_id')
		->get();

		return view('planner.scheduling.listViewTreatmentDetails',compact('patientId','patientName','patientCell','patientTreatmentDetails'));
	}

	public function listViewPatientInformation($patientId)
	{
	//fetching patient details
		$patient = DB::table('m_patient_details')
		->where('id', $patientId)
		->get();

		return view('planner.scheduling.listViewPatientInfo',compact('patient'));
	}

	public function listViewReferralsInformation($patientId)
	{
	//fetching patient details
		$patientDetails = DB::table('m_patient_details')
		->where('id', $patientId)
		->get();

		$patientName = $patientDetails[0]->name;

		$patientCell = $patientDetails[0]->cell_no;

	//to get referrals details for avove patieent Id
		$referrals = DB::table('m_referrals')
		->where('m_referrals.referred_patient_id', $patientId)
		->join('m_patient_details','m_patient_details.id','=','m_referrals.patient_id')
		->select('*')
		->get();

		return view('planner.scheduling.listViewReferrals',compact('patientId','patientName','patientCell','referrals'));
	}

	public function createNewAppointmentDetails(Request $request)
	{
		$patientId = $request->get('patient_id');

		if($patientId != null)
		{
			$patientDetails = DB::table('m_patient_details')
			->where('id', $patientId)
			->get();
			$patientDetails = $patientDetails[0];	
		}
		else
		{
			$id = $request->get('patient_id');
			$name = $request->get('patient_name');
			$cellNo = $request->get('patient_cellno');

			$patientDetails = (object)array('name' => $name,'cell_no' => $cellNo, 'id'=>$id);
		}
		

		$dentistName = DB::table('s_dentist_details')
		->get();

		$chairName = DB::table('s_chair_details')
		->get();

		$appointmentStatus = DB::table('dnf_appointment_status')
		->get();
		
		return view('planner.scheduling.addNewAppointment',compact('dentistName','chairName','appointmentStatus','patientDetails'));
	}
	
	public function storeAppointmentDetails(Request $request)
	{
		$patientId = $request->get('patient_id');

		$name = $request->get('patient_name');

		$cell_no = $request->get('cell_no');

		if($patientId == null)
		{
			$patient = new Patient;

			$patient->name = $name;
			$patient->cell_no = $cell_no;
			$patient->save();

			$patientDetails = DB::table('m_patient_details')
			->orderBy('id','desc')
			->first();

			$patientId = $patientDetails->id;
		}

		$appointment = new Appointment;
		$appointment->date=$request->get('assign_date');
		$appointment->slot=$request->get('assign_slot');
		$appointment->assistant=$request->get('assistant');
		$appointment->consultant=$request->get('consultant');
		$appointment->patient_id=$patientId;
		$appointment->dentist_id=$request->get('dentist_id');
		$appointment->chair_id=$request->get('chair_id');
		$appointment->appointment_status_id=$request->get('appointment_status_id');
		$appointment->save();

		$request->session()->flash('alert-success', 'Appointment was successfully added!');

		return redirect()->action('PlannerController@listViewScheduling');
	}

	public function listViewAppointmentFollowUp()
	{
		$status = DB::table('dnf_follow_up_status')
		->get();

		$followUpDetails = DB::table('t_follow_up')
		->join('m_patient_details','m_patient_details.id','=','t_follow_up.patient_id')
		->join('dnf_follow_up_status','dnf_follow_up_status.id','=','t_follow_up.follow_up_status_id')
		->select('*')
		->get();

		return view('planner.followup.appointment.listViewAppointmentFollowUp',compact('followUpDetails','status'));
	}

	public function createNewFollowUp()
	{
		$patientNames = DB::table('m_patient_details')
		->get();

		$status = DB::table('dnf_follow_up_status')
		->get();

		return view('planner.followup.appointment.addAppointmentFollowUp',compact('patientNames','status'));
	}

	public function storeFollowUpAppointmentDetails(Request $request)
	{
		$followupdetails=new FollowUpDetails;

		$followupdetails->notes=$request->get('notes');
		$followupdetails->date=$request->get('follow_up_date');
		$followupdetails->time=$request->get('follow_up_time');
		$followupdetails->patient_id=$request->get('patient_id');
		$followupdetails->follow_up_status_id=$request->get('status_id');
		$followupdetails->save();

		$request->session()->flash('alert-success', 'New Appointment was successfully added!');

		return redirect()->action('PlannerController@listViewAppointmentFollowUp');

	}

	public function searchFollowUpDetails(Request $request)
	{
		$date = $request->get('date');

		$followUpStatusId = $request->get('follow_up_status_id');

		$status = DB::table('dnf_follow_up_status')
		->get();

	//checking entered date and status exists or not and if not make entry in patient details table
		$result = FollowUpDetails::where('date', '=', $date)->where('follow_up_status_id', '=', $followUpStatusId)->get();

		if(!count($result))
		{
			$dateResult = FollowUpDetails::where('date', '=', $date)->get();

			if(count($dateResult))
			{
				$followUpDetails = DB::table('t_follow_up')
				->where('t_follow_up.date',$date)
				->join('m_patient_details','m_patient_details.id','=','t_follow_up.patient_id')
				->join('dnf_follow_up_status','dnf_follow_up_status.id','=','t_follow_up.follow_up_status_id')
				->select('*')
				->get();
				
				return view('planner.followup.appointment.listViewAppointmentFollowUp',compact('followUpDetails','status'));

			}

			$followUpResult = FollowUpDetails::where('follow_up_status_id', '=', $followUpStatusId)->get();

			if(count($followUpResult))
			{
				$followUpDetails = DB::table('t_follow_up')
				->where('t_follow_up.follow_up_status_id',$followUpStatusId)
				->join('m_patient_details','m_patient_details.id','=','t_follow_up.patient_id')
				->join('dnf_follow_up_status','dnf_follow_up_status.id','=','t_follow_up.follow_up_status_id')
				->select('*')
				->get();

				return view('planner.followup.appointment.listViewAppointmentFollowUp',compact('followUpDetails','status'));
			}

		}
		else
		{
			$followUpDetails = DB::table('t_follow_up')
			->where('t_follow_up.date',$date)
			->where('t_follow_up.follow_up_status_id',$followUpStatusId)
			->join('m_patient_details','m_patient_details.id','=','t_follow_up.patient_id')
			->join('dnf_follow_up_status','dnf_follow_up_status.id','=','t_follow_up.follow_up_status_id')
			->select('*')
			->get();
			
			return view('planner.followup.appointment.listViewAppointmentFollowUp',compact('followUpDetails','status'));
		}

		return redirect()->action('PlannerController@listViewAppointmentFollowUp');
		
	}

	public function listViewMaterialInventoryFollowUp()
	{
		return view('planner.followup.inventory.listViewMaterial');
	}

	public function listViewInstrumentInventoryFollowUp()
	{
		return view('planner.followup.inventory.listViewInstrument');
	}

	public function listViewGadgetMaintenanceFollowUp()
	{
		$gadgetdetails=DB::table('m_gadget_details')
		->leftJoin('t_assign_maintenance','t_assign_maintenance.gadget_id','=','m_gadget_details.id')
		->select('*','m_gadget_details.gadget_name','m_gadget_details.id as id')
		->get();

		return view('planner.followup.maintenance.listViewGadget',compact('gadgetdetails'));
	}

	public function listViewMachineMaintenanceFollowUp()
	{
		$machinedetails=DB::table('m_machine_details')
		->leftjoin('t_assign_maintenance_machine','t_assign_maintenance_machine.machine_id','=','m_machine_details.id')
		->select('*','m_machine_details.id as machine_id')
		->get();

		return view('planner.followup.maintenance.listViewMachine',compact('machinedetails'));
	}

	public function listViewLabWorkFollowUp()
	{
		return view('planner.followup.labwork.listViewLabWork');
	}

	public function reschedulePatientInformation(Request $request)
	{
		$appointmentId = $request->get('appointment_id');

		$dentist_names = DB::table('s_dentist_details')
		->get();

		$chair_names=DB::table('s_chair_details')
		->get();	

		$appointmentStatus = DB::table('dnf_appointment_status')
		->get();	

		$appointment = DB::table('t_appointment')
		->where('t_appointment.id', $appointmentId)
		->join('m_patient_details','m_patient_details.id','=','t_appointment.patient_id')
		->join('dnf_appointment_status','dnf_appointment_status.id','=','t_appointment.appointment_status_id')
		->join('s_dentist_details','s_dentist_details.id','=','t_appointment.dentist_id')
		->join('s_chair_details','s_chair_details.id','=','t_appointment.chair_id')
		->get();

		$appointment = $appointment['0'];

		return view('planner.scheduling.rescheduleAppointment', compact('appointment','dentist_names','chair_names','appointmentStatus'));

	}

	public function updateReschedulePatientInformation(Request $request)
	{
		$appointmentId = $request->get('appointment_id');
		$patientId = $request->get('patient_id');

		$date=$request->get('assign_date');
		$slot=$request->get('assign_slot');
		$assistant=$request->get('assistant');
		$consultant=$request->get('consultant');
		$dentistId=$request->get('dentist_id');
		$chairId=$request->get('chair_id');
		$appointmentStatusId=$request->get('appointment_status_id');

		DB::table('t_appointment')
		->where('id', $appointmentId)
		->update([
			'date' => $date,
			'slot' => $slot,
			'assistant'=> $assistant,
			'consultant'=> $consultant,
			'patient_id' => $patientId,
			'dentist_id' => $dentistId,
			'chair_id'=> $chairId,
			'appointment_status_id'=> $appointmentStatusId
			]);

		$request->session()->flash('alert-info', 'Appointment was successfully Updated!');

		return redirect()->action('PlannerController@listViewScheduling');
	}

	public function destroyRescheduleDetails(Request $request)
	{
		$newappointment = $request->get('reschedule_id');
		DB::table('schedule_details')->where('id', $newappointment)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Rescheduled details  was successfully deleted!');

		return redirect()->action('PlannerController@listViewScheduling');
	}

//Ajax Functions

	public function getPatientCellNo($patientId)
	{
		$patientCellNo = DB::table('m_patient_details')
		->where('id',$patientId)
		->get();

		return $patientCellNo;
	}
}
