<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use DB;
use App\BillingTreatment;
use App\Miscellaneous;
use App\Salary;

class BillingController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
	//treatment-details
		$treatmentDetails = DB::table('t_patient_treatment')
		->leftJoin('m_patient_details','m_patient_details.id', '=', 't_patient_treatment.patient_id')
		->leftJoin('t_treatment_general','t_treatment_general.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_consultant','t_treatment_consultant.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_quotation','t_treatment_quotation.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_visit_clinic','t_treatment_visit_clinic.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_findings','t_treatment_findings.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('dnf_treatment_status','dnf_treatment_status.id','=','t_treatment_general.status_id')
		->leftJoin('s_dentist_details','s_dentist_details.id','=','t_treatment_general.dentist_name_id')		
		->leftJoin('m_treatment_name','m_treatment_name.id','=','t_treatment_general.treatment_name_id')
		->select('*','t_treatment_general.clinic_id as general_clinic_id','t_treatment_visit_clinic.clinic_id as visit_clinic_id','t_patient_treatment.id as patient_treatment_id','m_treatment_name.name as treatment_name','dnf_treatment_status.status as treatment_status','m_patient_details.name as patient_name')
		->get();

	//to fetch bill installments details
		$billInstallmentDetails = DB::table('t_treatment_installments')
		->join('dnf_billing_status','dnf_billing_status.id','=','t_treatment_installments.billing_status_id')
		->join('dnf_payment_mode','dnf_payment_mode.id','=','t_treatment_installments.payment_mode_id')
		->orderBy('t_treatment_installments.id','desc')
		->select('*','t_treatment_installments.id as id')
		->get();

		return view('billing.treatment.listViewTreatment',compact('treatmentDetails','billInstallmentDetails'));
	}

	public function prevoiusBillDetails($patientTreatmentId)
	{
		$patientDetails = DB::table('t_patient_treatment')
		->where('t_patient_treatment.id',$patientTreatmentId)
		->Join('m_patient_details','m_patient_details.id', '=', 't_patient_treatment.patient_id')
		->leftJoin('t_treatment_general','t_treatment_general.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('m_treatment_name','m_treatment_name.id','=','t_treatment_general.treatment_name_id')
		->select('*','m_treatment_name.name as treatment_name','m_patient_details.name as patient_name')
		->get();

		$patientDetails = $patientDetails[0];

	//to fetch bill installments details
		$billInstallmentDetails = DB::table('t_treatment_installments')
		->where('t_treatment_installments.patient_treatment_id',$patientTreatmentId)
		->join('dnf_billing_status','dnf_billing_status.id','=','t_treatment_installments.billing_status_id')
		->join('dnf_payment_mode','dnf_payment_mode.id','=','t_treatment_installments.payment_mode_id')
		->select('*','t_treatment_installments.id as id')
		->get();

		return view('billing.treatment.previousBillDetails',compact('patientDetails','billInstallmentDetails'));
	}

	public function generateBill($patientTreatmentId)
	{
		$patientDetails = DB::table('t_patient_treatment')
		->where('t_patient_treatment.id',$patientTreatmentId)
		->Join('m_patient_details','m_patient_details.id', '=', 't_patient_treatment.patient_id')
		->leftJoin('t_treatment_general','t_treatment_general.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('m_treatment_name','m_treatment_name.id','=','t_treatment_general.treatment_name_id')
		->leftJoin('t_treatment_quotation','t_treatment_quotation.patient_treatment_id', '=', 't_patient_treatment.id')
		->select('*','m_treatment_name.name as treatment_name','m_patient_details.name as patient_name','t_treatment_quotation.id as treatment_quotation_id')
		->get();

		$patientDetails = $patientDetails[0];

		if($patientDetails->cost == null)
		{
			$patientDetails->cost = 0;
		}

	//fetching value for billing dropdown
		$billingStatus = DB::table('dnf_billing_status') 
		->get();

	//generating bill number
		$billNumber = DB::table('t_treatment_installments')
		->orderBy('id','desc')
		->select('bill_number')
		->first();

		if($billNumber == null)
		{
			$billNumber = 1;
		}
		else
		{
			$billNumber = $billNumber->bill_number + 1;
		}

	//fetching value for billing dropdown
		$paymentMode = DB::table('dnf_payment_mode') 
		->get();

	//select previous final amount
		$treatInstallDetails = DB::table('t_treatment_installments')
		->where('patient_treatment_id', $patientTreatmentId)
		->select('*')
		->get();

		if($treatInstallDetails != null)
		{
			$finalAmount = $treatInstallDetails[0]->final_amount;

			$sumAmount = 0;

			foreach ($treatInstallDetails as $treatInstall) 
			{
				$sumAmount += $treatInstall->amount;
			}

			$pendingAmount = $finalAmount - $sumAmount;
		}
		else
		{
			$pendingAmount = 0;
		}

		
		return view('billing.treatment.generateBillDetails',compact('patientDetails','billingStatus','billNumber','paymentMode','pendingAmount'));
	}

	public function storeBill(Request $request)
	{
		$billingtreatment = new BillingTreatment;
		$billingtreatment->amount = $request->get('amount');
		$billingtreatment->pending_amount = $request->get('pending_amount');
		$billingtreatment->final_amount = $request->get('final_amount');
		$billingtreatment->bill_number = $request->get('bill_number');
		$billingtreatment->date_of_billing = $request->get('date_of_billing');
		$billingtreatment->date_of_payment = $request->get('date_of_payment');
		$billingtreatment->notes = $request->get('notes');
		$billingtreatment->billing_status_id = $request->get('billing_status_id');
		$billingtreatment->payment_mode_id = $request->get('payment_mode_id');
		$billingtreatment->patient_treatment_id = $request->get('patient_treatment_id');
		$billingtreatment->save();

		return redirect()->action('BillingController@index');
	}

	public function listViewMaterial()
	{
		return view('billing.inventory.material.listViewMaterialInventory');
	}

	public function makePaymentMaterial()
	{
	//fetching value for billing dropdown
		$billingStatus = DB::table('dnf_billing_status') 
		->get();

	//generating bill number
		$billNumber = DB::table('t_material_installments')
		->orderBy('id','desc')
		->select('bill_number')
		->first();

		if($billNumber == null)
		{
			$billNumber = 1;
		}
		else
		{
			$billNumber = $billNumber->bill_number + 1;
		}

		//fetching value for billing dropdown
		$paymentMode = DB::table('dnf_payment_mode') 
		->get();

		return view('billing.inventory.material.makePaymentMaterial',compact('billingStatus','billNumber','paymentMode'));
	}

	public function listViewInstrument()
	{
		return view('billing.inventory.instrument.listViewInstrumentInventory');
	}

	public function makePaymentInstrument()
	{
	//fetching value for billing dropdown
		$billingStatus = DB::table('dnf_billing_status') 
		->get();

	//generating bill number
		$billNumber = DB::table('t_instrument_installments')
		->orderBy('id','desc')
		->select('bill_number')
		->first();

		if($billNumber == null)
		{
			$billNumber = 1;
		}
		else
		{
			$billNumber = $billNumber->bill_number + 1;
		}

		//fetching value for billing dropdown
		$paymentMode = DB::table('dnf_payment_mode') 
		->get();

		return view('billing.inventory.instrument.makePaymentInstrument',compact('billingStatus','billNumber','paymentMode'));
	}

	public function listViewGadget()
	{
		return view('billing.inventory.gadget.listViewGadgetInventory');
	}

	public function makePaymentGadget()
	{
	//fetching value for billing dropdown
		$billingStatus = DB::table('dnf_billing_status') 
		->get();

	//generating bill number
		$billNumber = DB::table('t_gadget_installments')
		->orderBy('id','desc')
		->select('bill_number')
		->first();

		if($billNumber == null)
		{
			$billNumber = 1;
		}
		else
		{
			$billNumber = $billNumber->bill_number + 1;
		}

		//fetching value for billing dropdown
		$paymentMode = DB::table('dnf_payment_mode') 
		->get();

		return view('billing.inventory.gadget.makePaymentGadget',compact('billingStatus','billNumber','paymentMode'));
	}

	public function listViewMachine()
	{
		return view('billing.inventory.machine.listViewMachineInventory');
	}

	public function makePaymentMachine()
	{
	//fetching value for billing dropdown
		$billingStatus = DB::table('dnf_billing_status') 
		->get();

	//generating bill number
		$billNumber = DB::table('t_machine_installments')
		->orderBy('id','desc')
		->select('bill_number')
		->first();

		if($billNumber == null)
		{
			$billNumber = 1;
		}
		else
		{
			$billNumber = $billNumber->bill_number + 1;
		}

		//fetching value for billing dropdown
		$paymentMode = DB::table('dnf_payment_mode') 
		->get();

		return view('billing.inventory.machine.makePaymentMachine',compact('billingStatus','billNumber','paymentMode'));
	}

	public function listViewLabWork()
	{
		return view('billing.labwork.listViewLabWork');
	}

	public function getDetailsLabWork()
	{
		return view('billing.labwork.getDetailsLabWork');
	}

	public function makePaymentLabwork()
	{
	//fetching value for billing dropdown
		$billingStatus = DB::table('dnf_billing_status') 
		->get();

	//generating bill number
		$billNumber = DB::table('t_lab_installments')
		->orderBy('id','desc')
		->select('bill_number')
		->first();

		if($billNumber == null)
		{
			$billNumber = 1;
		}
		else
		{
			$billNumber = $billNumber->bill_number + 1;
		}

	//fetching value for billing dropdown
		$paymentMode = DB::table('dnf_payment_mode') 
		->get();

		return view('billing.labwork.makePayment',compact('billingStatus','billNumber','paymentMode'));
	}

	public function listViewContractWorkMaintenance()
	{
		return view('billing.maintenance.listViewContractWork');
	}

	public function makePaymentContractWorkMaintenance()
	{
		return view('billing.maintenance.makePaymentContract');
	}

	public function listViewServiceAmountMaintenance()
	{
		return view('billing.maintenance.listViewServiceAmount');
	}

	public function listViewSalary()
	{
	//fetching salary details
		$salaryDetails = DB::table('t_salary') 
		->get();

		return view('billing.salary.listViewSalary',compact('salaryDetails'));
	}

	public function createSalaryDetails()
	{
	//fetching value for billing dropdown
		$paymentMode = DB::table('dnf_payment_mode') 
		->get();

		return view('billing.salary.addSalaryDetails',compact('paymentMode'));
	}

	public function storeSalaryDetails(Request $request)
	{
		$salary = new Salary;
		$salary->notes = $request->get('notes');
		$salary->month_year = $request->get('date');
		$salary->date_of_payment = $request->get('date_of_payment');
		$salary->salary = $request->get('salary');
		$salary->advance_amount = $request->get('advance_amount');
		$salary->leaves = $request->get('leave');
		$salary->total_deduction = $request->get('total_deduction');
		$salary->net_amount = $request->get('net_amount');
		$salary->bank_name = $request->get('bank_name');
		$salary->account_no = $request->get('account_no');
		$salary->account_holder = $request->get('account_holder');
		$salary->staff_id = $request->get('staff_type_id');
		$salary->payment_mode_id = $request->get('payment_mode_id');
		$salary->save(); 

		return redirect()->action('BillingController@listViewSalary');
	}

	public function viewSalaryDetails($salaryId)
	{
	//fetching value for salary
		$salaryDetails = DB::table('t_salary')
		->where('t_salary.id', $salaryId) 
		->join('dnf_payment_mode','dnf_payment_mode.id','=','t_salary.payment_mode_id')
		->select('*')
		->get();

		$salaryDetails = $salaryDetails[0];

		return view('billing.salary.viewSalaryDetails',compact('salaryDetails'));
	}

	public function listViewCashManagement()
	{
		return view('billing.cashmanagement.listViewCashManagement');
	}

	public function transferCashCashManagement()
	{
		return view('billing.cashmanagement.transferCash');
	}

	public function listViewMiscellaneous()
	{
		$miscellaneous_details=DB::table('miscellaneous')
		->join('dnf_payment_mode','dnf_payment_mode.id','=','miscellaneous.payment_mode_id')
		->select('*')
		->get();

		return view('billing.miscellaneous.listViewMiscellaneous',compact('miscellaneous_details'));
	}
	public function createMiscellaneous()
	{
	//fetching value for billing dropdown
		$paymentMode = DB::table('dnf_payment_mode') 
		->get();

		return view('billing.miscellaneous.addMiscellaneous','paymentMode');
	}

	public function storeMiscellaneous(Request $request)
	{
		$miscellaneous=new Miscellaneous;

		$miscellaneous->date_of_payment=$request->get('dop');
		$miscellaneous->paid_to=$request->get('paid_to');
		$miscellaneous->amount=$request->get('amount');
		$miscellaneous->bank_name=$request->get('bank_name');
		$miscellaneous->account_holder=$request->get('account_holder');
		$miscellaneous->account_number=$request->get('account_no');
		$miscellaneous->notes=$request->get('notes');
		$miscellaneous->reason=$request->get('reason');
		$miscellaneous->payment_mode_id=$request->get('payment_mode_id');

		$miscellaneous->save();

		return redirect()->action('BillingController@listViewMiscellaneous');


	}

}
