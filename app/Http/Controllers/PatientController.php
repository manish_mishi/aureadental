<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Appointment;
use App\Patient;
use App\ScheduleDetails;

use Illuminate\Http\Request;
use DB;


class PatientController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function listView()
	{
		return view('patient.listViewPatient');
	}

	public function listViewPatientDetails(Request $request)
	{
		$check = null;
		$cell_no = $request->get('cell_no');
		$name = $request->get('name');

	//checking entered patient name and cell_no exists or not
		$result = Patient::where('name', '=', $name)->where('cell_no', '=', $cell_no)->get();

		if(count($result) != 0)
		{
	//fetching patient details
		$patientDetails = DB::table('m_patient_details')
		->where('cell_no', $cell_no)
		->where('name', $name)
		->get();

		$patientId = $patientDetails[0]->id;

		$patientName = $patientDetails[0]->name;

		$patientCell = $patientDetails[0]->cell_no;

	//checking patient has appointment or not
		$check = Appointment::where('patient_id', '=', $patientId)->get();
		}

		if (count($check) == 0)
		{
			$appointmentDetails = "";
			$patientId = "";

			return view('patient.searchPatientDetails',compact('patientId','appointmentDetails','name','cell_no'));
		}

		else
		{

			$appointmentDetails = DB::table('t_appointment')
			->where('t_appointment.patient_id', $patientId)
			->join('m_patient_details','m_patient_details.id','=','t_appointment.patient_id')
			->join('dnf_appointment_status','dnf_appointment_status.id','=','t_appointment.appointment_status_id')
			->join('s_dentist_details','s_dentist_details.id','=','t_appointment.dentist_id')
			->join('s_chair_details','s_chair_details.id','=','t_appointment.chair_id')
			->get();

			return view('patient.searchPatientDetails',compact('patientId','appointmentDetails','name','cell_no'));
			}
	}

	public function listViewAppointmentDetails($patientId)
	{
	//fetching patient details
		$patientDetails = DB::table('m_patient_details')
		->where('id', $patientId)
		->get();

		$patientName = $patientDetails[0]->name;

		$patientCell = $patientDetails[0]->cell_no;

	//to fetch appoinment details
		$appointmentDetails = DB::table('t_appointment')
			->where('t_appointment.patient_id', $patientId)
			->join('m_patient_details','m_patient_details.id','=','t_appointment.patient_id')
			->join('dnf_appointment_status','dnf_appointment_status.id','=','t_appointment.appointment_status_id')
			->join('s_dentist_details','s_dentist_details.id','=','t_appointment.dentist_id')
			->join('s_chair_details','s_chair_details.id','=','t_appointment.chair_id')
			->get();

		return view('patient.appointmentdetails.listViewAppointmentDetails',compact('patientId','patientName','patientCell','appointmentDetails'));
	}

	public function listViewTreatmentDetails($patientId)
	{
	//fetching patient details
		$patientDetails = DB::table('m_patient_details')
		->where('id', $patientId)
		->get();

		$patientName = $patientDetails[0]->name;

		$patientCell = $patientDetails[0]->cell_no;

	//treatment-details
		$patientTreatmentDetails = DB::table('t_patient_treatment')
		->where('t_patient_treatment.patient_id', $patientId)
		->leftJoin('t_treatment_general','t_treatment_general.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('dnf_treatment_status','dnf_treatment_status.id','=','t_treatment_general.status_id')
		->leftJoin('s_dentist_details','s_dentist_details.id','=','t_treatment_general.dentist_name_id')
		->leftJoin('t_treatment_consultant','t_treatment_consultant.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_quotation','t_treatment_quotation.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_visit_clinic','t_treatment_visit_clinic.patient_treatment_id', '=', 't_patient_treatment.id')
		->leftJoin('t_treatment_findings','t_treatment_findings.patient_treatment_id', '=', 't_patient_treatment.id')
		->select('*','t_treatment_general.clinic_id as general_clinic_id','t_treatment_visit_clinic.clinic_id as visit_clinic_id')
		->get();

		return view('patient.treatmentinformation.listViewTreatmentDetails',compact('patientId','patientName','patientCell','patientTreatmentDetails'));
	}

	public function listViewPatientInformation($patientId)
	{
	//fetching patient details
		$patient = DB::table('m_patient_details')
		->where('id', $patientId)
		->get();

		return view('patient.patientinformation.listViewPatientInfo',compact('patient'));
	}

	public function listViewReferralsInformation($patientId)
	{
	//fetching patient details
		$patientDetails = DB::table('m_patient_details')
		->where('id', $patientId)
		->get();

		$patientName = $patientDetails[0]->name;

		$patientCell = $patientDetails[0]->cell_no;

	//to get referrals details for avove patieent Id
		$referrals = DB::table('m_referrals')
		->where('m_referrals.referred_patient_id', $patientId)
		->join('m_patient_details','m_patient_details.id','=','m_referrals.patient_id')
		->select('*')
		->get();

		return view('patient.referrals.listViewReferrals',compact('patientId','patientName','patientCell','referrals'));
	}


}
