<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use DB;
use App\AssignMaintenance;
use App\AssignMaintenanceMachine;
use Input;
use Session;

class MaintenanceController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{

		//unset the session
		Session::forget('gadget_machine_id');
		Session::forget('last_maintenance_date');
		Session::forget('vendor_id');
		Session::forget('vendor_name');
		Session::forget('duedate');
		Session::forget('status_id');
		Session::forget('status_name');
		Session::forget('set_maintenance_date');
		Session::forget('time');
		Session::forget('actual_date_of_maintenance');
		Session::forget('remark');
		Session::forget('actual_time');
		Session::forget('actual_duration');
		Session::forget('person_responsible');
		Session::forget('cell_no');
		Session::forget('amount');
		Session::forget('remark_extracharge');
		Session::forget('advance');
		Session::forget('discount');
		Session::forget('total_amount');
		Session::forget('bill');

		$gadgetdetails=DB::table('m_gadget_details')
					->leftJoin('t_assign_maintenance','t_assign_maintenance.gadget_id','=','m_gadget_details.id')
					->select('*','m_gadget_details.gadget_name','m_gadget_details.id as id')
					->get();
				/*dd($gadgetdetails);*/
		return view('maintenance.gadget.dashboardNextDueMaintenance',compact('gadgetdetails'));
	}

	public function gadgetMaintenance()
	{
		//unset the session
		Session::forget('gadget_machine_id');
		Session::forget('last_maintenance_date');
		Session::forget('vendor_id');
		Session::forget('vendor_name');
		Session::forget('duedate');
		Session::forget('status_id');
		Session::forget('status_name');
		Session::forget('set_maintenance_date');
		Session::forget('time');
		Session::forget('actual_date_of_maintenance');
		Session::forget('remark');
		Session::forget('actual_time');
		Session::forget('actual_duration');
		Session::forget('person_responsible');
		Session::forget('cell_no');
		Session::forget('amount');
		Session::forget('remark_extracharge');
		Session::forget('advance');
		Session::forget('discount');
		Session::forget('total_amount');
		Session::forget('bill');

		$gadgetdetails=DB::table('t_assign_maintenance')
						->where('status',2)
						->join('m_gadget_details','m_gadget_details.id','=','t_assign_maintenance.gadget_id')
						->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','t_assign_maintenance.vendor_id')
						->select('*','m_gadget_details.gadget_name','m_vendor_maintenance_details.name','t_assign_maintenance.gadget_id as id')
						->get();
					
		return view('maintenance.gadget.dashboardMaintenance',compact('gadgetdetails'));
	}

	public function gadgetAssignedMaintenance()
	{

		$gadgetdetails=DB::table('t_assign_maintenance')
						
						->orderBy('t_assign_maintenance.set_maintenance_date','desc')
						->where('status',1)
						->join('m_gadget_details','m_gadget_details.id','=','t_assign_maintenance.gadget_id')
						->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','t_assign_maintenance.vendor_id')
						->select('*','m_gadget_details.gadget_name as gadget_name','m_vendor_maintenance_details.name as vendor_name','t_assign_maintenance.gadget_id as id')
						->get();
					/*dd($gadgetdetails);		*/
		return view('maintenance.gadget.dashboardAssignedMaintenance',compact('gadgetdetails'));
	}



	public function machine()
	{
		//unset the session
		Session::forget('gadget_machine_id');
		Session::forget('last_maintenance_date');
		Session::forget('vendor_id');
		Session::forget('vendor_name');
		Session::forget('duedate');
		Session::forget('status_id');
		Session::forget('status_name');
		Session::forget('set_maintenance_date');
		Session::forget('time');
		Session::forget('actual_date_of_maintenance');
		Session::forget('remark');
		Session::forget('actual_time');
		Session::forget('actual_duration');
		Session::forget('person_responsible');
		Session::forget('cell_no');
		Session::forget('amount');
		Session::forget('remark_extracharge');
		Session::forget('advance');
		Session::forget('discount');
		Session::forget('total_amount');
		Session::forget('bill');
		$machinedetails=DB::table('m_machine_details')
						->leftjoin('t_assign_maintenance_machine','t_assign_maintenance_machine.machine_id','=','m_machine_details.id')
						->select('*','m_machine_details.id as machine_id')
						->get();

		return view('maintenance.machine.dashboardNextDueMaintenance',compact('machinedetails'));
	}

    public function machineAssignWorkMaintenance($machine_id)
    {

    	$machinedetails=DB::table('m_machine_details')
						->select('*','id as machine_id')
						->get(); 

		$vendordetails=DB::table('m_vendor_maintenance_details')
					->select('*')
					 ->get();

		$maintenance_status=DB::table('dnf_maintenance_status')
							->get();

    	$current_machineDet=DB::table('t_assign_maintenance_machine')
    							->where('t_assign_maintenance_machine.machine_id',$machine_id)
    							->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','t_assign_maintenance_machine.vendor_id')
    							->join('dnf_maintenance_status','dnf_maintenance_status.id','=','t_assign_maintenance_machine.status')
    							->select('*','m_vendor_maintenance_details.name as vendor_name','dnf_maintenance_status.name as status_name')
    							->get();
    						
		if($current_machineDet == null)
		{
			return view('maintenance.machine.assignworkmachine',compact('machine_id','machinedetails','vendordetails','maintenance_status','current_machineDet'));
		}   
		else
		{
			return view('maintenance.machine.editassignworkmachine',compact('machine_id','machinedetails','vendordetails','maintenance_status','current_machineDet'));
		} 						
    	
    }

    public function storeMachineWorkAssign(Request $request)
    {

    	$machine_id=$request->get('gadget_machine_id');
    	
		$last_maintenance_date=$request->get('last_maintenance_date');
		$vendor=$request->get('vendor');
		
		$duedate=$request->get('duedate');
		$status=$request->get('status');
		
		$set_maintenance_date=$request->get('set_maintenance_date');
		$time=$request->get('time');

		//explde id's and names
		//for vendor
		
		$vendordetails=explode(",",$vendor);
		$vendor_id=$vendordetails[0];
		$vendor_name=$vendordetails[1];
		$vendor_type_id=$vendordetails[2];
		
		//for status
		$statusdetails=explode(",",$status);
		$status_id=$statusdetails[0];
		$status_name=$statusdetails[1];
		

		session(['gadget_machine_id'=>$machine_id,
				 'last_maintenance_date'=>$last_maintenance_date,
				 'vendor_id'=>$vendor_id,
				 'vendor_name'=>$vendor_name,
				 'vendor_type_id'=>$vendor_type_id,
				 'duedate'=>$duedate,
				 'status_id'=>$status_id,
				 'status_name'=>$status_name,
				 'set_maintenance_date'=>$set_maintenance_date,
				 'time'=>$time]);

		return redirect()->action('MaintenanceController@completeMaintenanceMachine',[$machine_id]);
    }

    public function completeMaintenanceMachine($machine_id)
    {

    	$current_machineDet=DB::table('t_assign_maintenance_machine')
    							->where('machine_id',$machine_id)
    							->get();

    	if($current_machineDet == null)
    	{
    		return view('maintenance.machine.completemaintenance',compact('machine_id','current_machineDet'));
    	}
    	else
    	{
    		return view('maintenance.machine.editcompletemaintenance',compact('machine_id','current_machineDet'));
    	}
    	
    }

    public function storeCompletemaintenanceMachine(Request $request)
    {
    	
    	$machine_id=$request->get('gadget_machine_id');
		$date_of_maintenance=$request->get('date_of_maintenance');
		$remark=$request->get('remark');
		$time_complete=$request->get('time_complete');
		$duration=$request->get('duration');
		$person_responsible=$request->get('person_responsible');
		$cell=$request->get('cell');

		session(['actual_date_of_maintenance'=>$date_of_maintenance,
				 'remark'=>$remark,
				 'actual_time'=>$time_complete,
				 'actual_duration'=>$duration,
				 'person_responsible'=>$person_responsible,
				 'cell_no'=>$cell]);
		
		return redirect()->action('MaintenanceController@extraChargegMachine',[$machine_id]);
    }

    public function extraChargegMachine($machine_id)
    {
    	$current_machineDet=DB::table('t_assign_maintenance_machine')
    							->where('machine_id',$machine_id)
    							->get();

    	if($current_machineDet == null)
    	{
    		return view('maintenance.machine.extracharges',compact('machine_id','current_machineDet'));
    	}
    	else
    	{
    		return view('maintenance.machine.editextracharges',compact('machine_id','current_machineDet'));
    	}
    	
    }

    public function storeExtrechargesMachine(Request $request)
    {
    	
    	$machine_id=$request->get('gadget_machine_id');
		$amount=$request->get('amount');
		$remark_extracharge=$request->get('remark_extracharge');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$total_amount=$request->get('total_amount');
		$bill=$request->get('bill');

		session(['amount'=>$amount,
				 'remark_extracharge'=>$remark_extracharge,
				 'advance'=>$advance,
				 'discount'=>$discount,
				 'total_amount'=>$total_amount,
				 'bill'=>$bill]);

		
			$assignmaintenance=new AssignMaintenanceMachine;

			$assignmaintenance->last_maintenance_date=Session::get('last_maintenance_date');
			$assignmaintenance->status=Session::get('status_id');
			
			$assignmaintenance->due_date=Session::get('duedate');
			$assignmaintenance->set_maintenance_date=Session::get('set_maintenance_date');
			$assignmaintenance->time=Session::get('time');

			$assignmaintenance->actual_date_of_maintenance=Session::get('actual_date_of_maintenance');
			$assignmaintenance->actual_time=Session::get('actual_time');
			$assignmaintenance->actual_duration=Session::get('actual_duration');
			$assignmaintenance->person_responsible=Session::get('person_responsible');
			$assignmaintenance->cell_no=Session::get('cell_no');
			$assignmaintenance->remarks=Session::get('remark');
			$assignmaintenance->amount=Session::get('amount');
			$assignmaintenance->advance=Session::get('advance');
			$assignmaintenance->discount=Session::get('discount');
			$assignmaintenance->total_amount=Session::get('total_amount');
			$assignmaintenance->bill_no=Session::get('bill');

			$assignmaintenance->extra_charge_remarks=Session::get('remark_extracharge');
			$assignmaintenance->vendor_id=Session::get('vendor_id');
			$assignmaintenance->vendor_type_id=Session::get('vendor_type_id');
			$assignmaintenance->machine_id=Session::get('gadget_machine_id');

			$assignmaintenance->save();
		

		return redirect()->action('MaintenanceController@machine');
		
    }

    public function updateMachineWorkAssign(Request $request)
    {
    	$machine_id=$request->get('gadget_machine_id');
    	
		$last_maintenance_date=$request->get('last_maintenance_date');
		$vendor=$request->get('vendor');
		
		$duedate=$request->get('duedate');
		$status=$request->get('status');
		
		$set_maintenance_date=$request->get('set_maintenance_date');
		$time=$request->get('time');

		//explde id's and names
		//for vendor
		
		$vendordetails=explode(",",$vendor);
		$vendor_id=$vendordetails[0];
		$vendor_name=$vendordetails[1];
		$vendor_type_id=$vendordetails[2];
		
		//for status
		$statusdetails=explode(",",$status);
		$status_id=$statusdetails[0];
		$status_name=$statusdetails[1];
		

		session(['gadget_machine_id'=>$machine_id,
				 'last_maintenance_date'=>$last_maintenance_date,
				 'vendor_id'=>$vendor_id,
				 'vendor_name'=>$vendor_name,
				 'vendor_type_id'=>$vendor_type_id,
				 'duedate'=>$duedate,
				 'status_id'=>$status_id,
				 'status_name'=>$status_name,
				 'set_maintenance_date'=>$set_maintenance_date,
				 'time'=>$time]);

		return redirect()->action('MaintenanceController@completeMaintenanceMachine',[$machine_id]);
    }

    public function updateCompletemaintenanceMachine(Request $request)
    {
    	$machine_id=$request->get('gadget_machine_id');
		$date_of_maintenance=$request->get('date_of_maintenance');
		$remark=$request->get('remark');
		$time_complete=$request->get('time_complete');
		$duration=$request->get('duration');
		$person_responsible=$request->get('person_responsible');
		$cell=$request->get('cell');

		session(['actual_date_of_maintenance'=>$date_of_maintenance,
				 'remark'=>$remark,
				 'actual_time'=>$time_complete,
				 'actual_duration'=>$duration,
				 'person_responsible'=>$person_responsible,
				 'cell_no'=>$cell]);

		return redirect()->action('MaintenanceController@extraChargegMachine',[$machine_id]);
    }

    public function updateExtrechargesMachine(Request $request)
    {
    	$machine_id=$request->get('gadget_machine_id');
		$amount=$request->get('amount');
		$remark_extracharge=$request->get('remark_extracharge');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$total_amount=$request->get('total_amount');
		$bill=$request->get('bill');

		session(['amount'=>$amount,
				 'remark_extracharge'=>$remark_extracharge,
				 'advance'=>$advance,
				 'discount'=>$discount,
				 'total_amount'=>$total_amount,
				 'bill'=>$bill]);

		DB::table('t_assign_maintenance_machine')
					->where('machine_id',$machine_id)
					->update(['last_maintenance_date'=>Session::get('last_maintenance_date'),
							  'status'=>Session::get('status_id'),
							  'due_date'=>Session::get('duedate'),
							  'set_maintenance_date'=>Session::get('set_maintenance_date'),
							  'time'=>Session::get('time'),
							  'actual_date_of_maintenance'=>Session::get('actual_date_of_maintenance'),
							  'actual_time'=>Session::get('actual_time'),
							  'actual_duration'=>Session::get('actual_duration'),
							  'person_responsible'=>Session::get('person_responsible'),
							  'cell_no'=>Session::get('cell_no'),
							  'remarks'=>Session::get('remark'),
							  'amount'=>Session::get('amount'),
							  'advance'=>Session::get('advance'),
							  'discount'=>Session::get('discount'),
							  'total_amount'=>Session::get('total_amount'),
							  'bill_no'=>Session::get('bill'),
							  'extra_charge_remarks'=>Session::get('remark_extracharge'),
							  'vendor_id'=>Session::get('vendor_id'),
							  'vendor_type_id'=>Session::get('vendor_type_id')]);
		

		return redirect()->action('MaintenanceController@machine');
    }

	public function machineCompletedMaintenance()
	{
		//unset the session
		Session::forget('gadget_machine_id');
		Session::forget('last_maintenance_date');
		Session::forget('vendor_id');
		Session::forget('vendor_name');
		Session::forget('duedate');
		Session::forget('status_id');
		Session::forget('status_name');
		Session::forget('set_maintenance_date');
		Session::forget('time');
		Session::forget('actual_date_of_maintenance');
		Session::forget('remark');
		Session::forget('actual_time');
		Session::forget('actual_duration');
		Session::forget('person_responsible');
		Session::forget('cell_no');
		Session::forget('amount');
		Session::forget('remark_extracharge');
		Session::forget('advance');
		Session::forget('discount');
		Session::forget('total_amount');
		Session::forget('bill');

		$machinedetails=DB::table('t_assign_maintenance_machine')
					->where('status',2)
					->join('m_machine_details','m_machine_details.id','=','t_assign_maintenance_machine.machine_id')
					->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','t_assign_maintenance_machine.vendor_id')
					->select('*','m_machine_details.machine_name','m_vendor_maintenance_details.name as vendor_name','t_assign_maintenance_machine.id as machine_id')
					->get();
					
		return view ('maintenance.machine.dashboardCompletedMaintenance',compact('machinedetails'));
	}

	public function getDetailsAssignWorkMachine($machine_id)
	{
		$machinedetails=DB::table('m_machine_details')
						->select('*','id as machine_id')
						->get(); 

		$vendordetails=DB::table('m_vendor_maintenance_details')
					->select('*')
					 ->get();

		$maintenance_status=DB::table('dnf_maintenance_status')
							->get();
		$current_machineDet=DB::table('t_assign_maintenance_machine')
    							->where('t_assign_maintenance_machine.id',$machine_id)
    							->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','t_assign_maintenance_machine.vendor_id')
    							->join('dnf_maintenance_status','dnf_maintenance_status.id','=','t_assign_maintenance_machine.status')
    							->select('*','m_vendor_maintenance_details.name as vendor_name','dnf_maintenance_status.name as status_name')
    							->get(); 

		return view('maintenance.machine.maintenance.completem.getdetailsassignwork',compact('machine_id','current_machineDet','machinedetails','vendordetails','maintenance_status'));
	}

	public function getDetailsCompletedworkMachine($machine_id)
	{
		$current_machineDet=DB::table('t_assign_maintenance_machine')
    							->where('id',$machine_id)
    							->get();
		return view('maintenance.machine.maintenance.completem.getdetailscompletedmaintenance',compact('machine_id','current_machineDet'));
	}

	public function getDetailsextrachargesMachine($machine_id)
	{
		$current_machineDet=DB::table('t_assign_maintenance_machine')
    							->where('t_assign_maintenance_machine.id',$machine_id)
    							->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','t_assign_maintenance_machine.vendor_id')
    							->join('dnf_maintenance_status','dnf_maintenance_status.id','=','t_assign_maintenance_machine.status')
    							->select('*','m_vendor_maintenance_details.name as vendor_name','dnf_maintenance_status.name as status_name')
    							->get(); 
		return view('maintenance.machine.maintenance.completem.getdetailsextracharge',compact('machine_id','current_machineDet'));
	}

	public function completedMaintenanceAssignWorkMachine($machine_id)
	{
		$machinedetails=DB::table('m_machine_details')
						->select('*','id as machine_id')
						->get();

		$vendordetails=DB::table('m_vendor_maintenance_details')
					->select('*')
					 ->get();

		$maintenance_status=DB::table('dnf_maintenance_status')
							->get();
		return view('maintenance.machine.maintenance.completem.assignwork',compact('machine_id','machinedetails','vendordetails','maintenance_status'));
	}

	

	public function storeAssignWorkCompletedMachine(Request $request)
	{
		$machine_id=$request->get('gadget_machine_id');
    	
		$last_maintenance_date=$request->get('last_maintenance_date');
		$vendor=$request->get('vendor');
		
		$duedate=$request->get('duedate');
		$status=$request->get('status');
		
		$set_maintenance_date=$request->get('set_maintenance_date');
		$time=$request->get('time');

		//explde id's and names
		//for vendor
		
		$vendordetails=explode(",",$vendor);
		$vendor_id=$vendordetails[0];
		$vendor_name=$vendordetails[1];
		$vendor_type_id=$vendordetails[2];
		
		//for status
		$statusdetails=explode(",",$status);
		$status_id=$statusdetails[0];
		$status_name=$statusdetails[1];
		

		session(['gadget_machine_id'=>$machine_id,
				 'last_maintenance_date'=>$last_maintenance_date,
				 'vendor_id'=>$vendor_id,
				 'vendor_name'=>$vendor_name,
				 'vendor_type_id'=>$vendor_type_id,
				 'duedate'=>$duedate,
				 'status_id'=>$status_id,
				 'status_name'=>$status_name,
				 'set_maintenance_date'=>$set_maintenance_date,
				 'time'=>$time]);

		return redirect()->action('MaintenanceController@CompleteMaintenanceComoletedMaintenance',[$machine_id]);
	}

	public function CompleteMaintenanceComoletedMaintenance($machine_id)
	{
		return view('maintenance.machine.maintenance.completem.completedmaintenance',compact('machine_id'));
	}

	public function storeCompletedMaintenancMachineM(Request $request)
	{
		$machine_id=$request->get('gadget_machine_id');
		$date_of_maintenance=$request->get('date_of_maintenance');
		$remark=$request->get('remark');
		$time_complete=$request->get('time_complete');
		$duration=$request->get('duration');
		$person_responsible=$request->get('person_responsible');
		$cell=$request->get('cell');

		session(['actual_date_of_maintenance'=>$date_of_maintenance,
				 'remark'=>$remark,
				 'actual_time'=>$time_complete,
				 'actual_duration'=>$duration,
				 'person_responsible'=>$person_responsible,
				 'cell_no'=>$cell]);

		return redirect()->action('MaintenanceController@CompleteMaintenanceExtracharges',[$machine_id]);
	}

	public function CompleteMaintenanceExtracharges($machine_id)
	{
		return view('maintenance.machine.maintenance.completem.extracharges',compact('machine_id'));
	}

	public function stroreExtrachargesMachineCompleted(Request $request)
	{
		$machine_id=$request->get('gadget_machine_id');
		$amount=$request->get('amount');
		$remark_extracharge=$request->get('remark_extracharge');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$total_amount=$request->get('total_amount');
		$bill=$request->get('bill');

		session(['amount'=>$amount,
				 'remark_extracharge'=>$remark_extracharge,
				 'advance'=>$advance,
				 'discount'=>$discount,
				 'total_amount'=>$total_amount,
				 'bill'=>$bill]);

		
			$assignmaintenance=new AssignMaintenanceMachine;

			$assignmaintenance->last_maintenance_date=Session::get('last_maintenance_date');
			$assignmaintenance->status=Session::get('status_id');
			
			$assignmaintenance->due_date=Session::get('duedate');
			$assignmaintenance->set_maintenance_date=Session::get('set_maintenance_date');
			$assignmaintenance->time=Session::get('time');

			$assignmaintenance->actual_date_of_maintenance=Session::get('actual_date_of_maintenance');
			$assignmaintenance->actual_time=Session::get('actual_time');
			$assignmaintenance->actual_duration=Session::get('actual_duration');
			$assignmaintenance->person_responsible=Session::get('person_responsible');
			$assignmaintenance->cell_no=Session::get('cell_no');
			$assignmaintenance->remarks=Session::get('remark');
			$assignmaintenance->amount=Session::get('amount');
			$assignmaintenance->advance=Session::get('advance');
			$assignmaintenance->discount=Session::get('discount');
			$assignmaintenance->total_amount=Session::get('total_amount');
			$assignmaintenance->bill_no=Session::get('bill');

			$assignmaintenance->extra_charge_remarks=Session::get('remark_extracharge');
			$assignmaintenance->vendor_id=Session::get('vendor_id');
			$assignmaintenance->vendor_type_id=Session::get('vendor_type_id');
			$assignmaintenance->machine_id=Session::get('gadget_machine_id');

			$assignmaintenance->save();
		
		return redirect()->action('MaintenanceController@machineCompletedMaintenance');
	}

	public function machineAssignedMaintenance()
	{
		$machinedetails=DB::table('t_assign_maintenance_machine')
			->where('status',1)
			->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','t_assign_maintenance_machine.vendor_id')
			->join('m_machine_details','m_machine_details.id','=','t_assign_maintenance_machine.machine_id')
			->select('*','m_vendor_maintenance_details.name as vendor_name','m_machine_details.machine_name as machine_name')
			->get();

		return view ('maintenance.machine.dashboardAssignedMaintenance',compact('machinedetails'));
	}
//for simple assign work
	public function machineAssignWork($id)
	{
		$gadgetdeails=DB::table('m_gadget_details')
					->where('id',$id)
					->get();
					
		$vendordetails=DB::table('m_vendor_maintenance_details')
					 ->get();

		$maintenance_status=DB::table('dnf_maintenance_status')
							->get();

		$current_gadgetDet=DB::table('t_assign_maintenance')
						->where('gadget_id',$id)
						->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','t_assign_maintenance.vendor_id')
						->join('dnf_maintenance_status','dnf_maintenance_status.id','=','t_assign_maintenance.status')
						->select('*','m_vendor_maintenance_details.name as vendor_name','dnf_maintenance_status.name as status_name')
						->get();

		if($current_gadgetDet == null)
		{
			return view ('maintenance.gadget.AssignWork',compact('current_gadgetDet','gadgetdeails','vendordetails','maintenance_status','id'));
			
		} 	
		else
		{
			return view ('maintenance.gadget.editAssignWork',compact('current_gadgetDet','gadgetdeails','vendordetails','maintenance_status','id'));
		}
	
	}

	public function storeAssignWork(Request $request)
	{

		//to get requested values from form
		
		$id=$request->get('gadget_machine_id');
		$last_maintenance_date=$request->get('last_maintenance_date');
		$vendor=$request->get('vendor');
		$vendor_type_id=$request->get('vendor_type_id');
		$duedate=$request->get('duedate');
		$status=$request->get('status');
		
		$set_maintenance_date=$request->get('set_maintenance_date');
		$time=$request->get('time');

		//explde id's and names
		//for vendor
		$vendordetails=explode(",",$vendor);
		
		$vendor_id=$vendordetails[0];
		$vendor_name=$vendordetails[1];
		$vendor_type_id=$vendordetails[2];
		
		//for status
		$statusdetails=explode(",",$status);
		$status_id=$statusdetails[0];
		$status_name=$statusdetails[1];
		

		session(['gadget_machine_id'=>$id,
				 'last_maintenance_date'=>$last_maintenance_date,
				 'vendor_id'=>$vendor_id,
				 'vendor_name'=>$vendor_name,
				 'vendor_type_id'=>$vendor_type_id,
				 'duedate'=>$duedate,
				 'status_id'=>$status_id,
				 'status_name'=>$status_name,
				 'set_maintenance_date'=>$set_maintenance_date,
				 'time'=>$time]);
		

		return redirect()->action('MaintenanceController@gadgetCompletedMaintenance',[$id]);
	}

	public function gadgetCompletedMaintenance($id)
	{
		$gadgetdeails=DB::table('m_gadget_details')
					->where('id',$id)
					->get();

		$current_gadgetDet=DB::table('t_assign_maintenance')
						->where('gadget_id',$id)
						->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','t_assign_maintenance.vendor_id')
						->join('dnf_maintenance_status','dnf_maintenance_status.id','=','t_assign_maintenance.status')
						->select('*','m_vendor_maintenance_details.name as vendor_name','dnf_maintenance_status.name as status_name','t_assign_maintenance.cell_no as cell_no')
						->get();
						
		if($current_gadgetDet == null)
		{
			return view('maintenance.gadget.CompletedMaintenance',compact('id','current_gadgetDet','gadgetdeails'));
		}
		else
		{
			return view('maintenance.gadget.editCompletedMaintenance',compact('id','current_gadgetDet','gadgetdeails'));
			
		}
		
	}


	public function storeCompletemaintenance(Request $request)
	{
		$id=$request->get('gadget_machine_id');
		$date_of_maintenance=$request->get('date_of_maintenance');
		$remark=$request->get('remark');
		$time_complete=$request->get('time_complete');
		$duration=$request->get('duration');
		$person_responsible=$request->get('person_responsible');
		$cell=$request->get('cell');

		session(['actual_date_of_maintenance'=>$date_of_maintenance,
				 'remark'=>$remark,
				 'actual_time'=>$time_complete,
				 'actual_duration'=>$duration,
				 'person_responsible'=>$person_responsible,
				 'cell_no'=>$cell]);

		
		return redirect()->action('MaintenanceController@extraCharges',[$id]);
	}
	
	public function extraCharges($id)
	{
		$current_gadgetDet=DB::table('t_assign_maintenance')
						->where('gadget_id',$id)
						->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','t_assign_maintenance.vendor_id')
						->join('dnf_maintenance_status','dnf_maintenance_status.id','=','t_assign_maintenance.status')
						->select('*','m_vendor_maintenance_details.name as vendor_name','dnf_maintenance_status.name as status_name')
						->get();

		if($current_gadgetDet ==null)
		{
			return view('maintenance.gadget.extraCharges',compact('id','current_gadgetDet'));
		}
		else
		{
			return view('maintenance.gadget.editextraCharges',compact('id','current_gadgetDet'));
			
		}
		
	}

	public function storegadgetextracharges(Request $request)
	{
		
		$id=$request->get('gadget_machine_id');
		$amount=$request->get('amount');
		$remark_extracharge=$request->get('remark_extracharge');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$total_amount=$request->get('total_amount');
		$bill=$request->get('bill');

		session(['amount'=>$amount,
				 'remark_extracharge'=>$remark_extracharge,
				 'advance'=>$advance,
				 'discount'=>$discount,
				 'total_amount'=>$total_amount,
				 'bill'=>$bill]);


		
			$assignmaintenance=new AssignMaintenance;

			$assignmaintenance->last_maintenance_date=Session::get('last_maintenance_date');
			$assignmaintenance->status=Session::get('status_id');
			
			$assignmaintenance->due_date=Session::get('duedate');
			$assignmaintenance->set_maintenance_date=Session::get('set_maintenance_date');
			$assignmaintenance->time=Session::get('time');

			$assignmaintenance->actual_date_of_maintenance=Session::get('actual_date_of_maintenance');
			$assignmaintenance->actual_time=Session::get('actual_time');
			$assignmaintenance->actual_duration=Session::get('actual_duration');
			$assignmaintenance->person_responsible=Session::get('person_responsible');
			$assignmaintenance->cell_no=Session::get('cell_no');
			$assignmaintenance->remarks=Session::get('remark');
			$assignmaintenance->amount=Session::get('amount');
			$assignmaintenance->advance=Session::get('advance');
			$assignmaintenance->discount=Session::get('discount');
			$assignmaintenance->total_amount=Session::get('total_amount');
			$assignmaintenance->bill_no=Session::get('bill');

			$assignmaintenance->extra_charge_remarks=Session::get('remark_extracharge');
			$assignmaintenance->vendor_id=Session::get('vendor_id');
			$assignmaintenance->vendor_type_id=Session::get('vendor_type_id');
			$assignmaintenance->gadget_id=Session::get('gadget_machine_id');

			$assignmaintenance->save();
		
		return redirect()->action('MaintenanceController@index',[$id]);
	}

	
	public function updateAssignWork(Request $request)
	{

		//to get requested values from form
		
		$id=$request->get('gadget_machine_id');
		$last_maintenance_date=$request->get('last_maintenance_date');
		$vendor=$request->get('vendor');
		$vendor_type_id=$request->get('vendor_type_id');
		$duedate=$request->get('duedate');
		$status=$request->get('status');
		
		$set_maintenance_date=$request->get('set_maintenance_date');
		$time=$request->get('time');

		//explde id's and names
		//for vendor
		$vendordetails=explode(",",$vendor);
		
		$vendor_id=$vendordetails[0];
		$vendor_name=$vendordetails[1];
		$vendor_type_id=$vendordetails[2];
		
		//for status
		$statusdetails=explode(",",$status);
		$status_id=$statusdetails[0];
		$status_name=$statusdetails[1];
		

		session(['gadget_machine_id'=>$id,
				 'last_maintenance_date'=>$last_maintenance_date,
				 'vendor_id'=>$vendor_id,
				 'vendor_name'=>$vendor_name,
				 'vendor_type_id'=>$vendor_type_id,
				 'duedate'=>$duedate,
				 'status_id'=>$status_id,
				 'status_name'=>$status_name,
				 'set_maintenance_date'=>$set_maintenance_date,
				 'time'=>$time]);
		

		return redirect()->action('MaintenanceController@gadgetCompletedMaintenance',[$id]);
	}

	public function editCompletemaintenance(Request $request)
	{
		$id=$request->get('gadget_machine_id');
		$date_of_maintenance=$request->get('date_of_maintenance');
		$remark=$request->get('remark');
		$time_complete=$request->get('time_complete');
		$duration=$request->get('duration');
		$person_responsible=$request->get('person_responsible');
		$cell=$request->get('cell');

		session(['actual_date_of_maintenance'=>$date_of_maintenance,
				 'remark'=>$remark,
				 'actual_time'=>$time_complete,
				 'actual_duration'=>$duration,
				 'person_responsible'=>$person_responsible,
				 'cell_no'=>$cell]);

		
		return redirect()->action('MaintenanceController@extraCharges',[$id]);
	}

	public function updateExteracharges(Request $request)
	{
		$id=$request->get('gadget_machine_id');
		$amount=$request->get('amount');
		$remark_extracharge=$request->get('remark_extracharge');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$total_amount=$request->get('total_amount');
		$bill=$request->get('bill');

		session(['amount'=>$amount,
				 'remark_extracharge'=>$remark_extracharge,
				 'advance'=>$advance,
				 'discount'=>$discount,
				 'total_amount'=>$total_amount,
				 'bill'=>$bill]);

		DB::table('t_assign_maintenance')
					->where('gadget_id',$id)
					->update(['last_maintenance_date'=>Session::get('last_maintenance_date'),
							  
							  'status'=>Session::get('status_id'),
							  'due_date'=>Session::get('duedate'),
							  'set_maintenance_date'=>Session::get('set_maintenance_date'),
							  'time'=>Session::get('time'),
							  'actual_date_of_maintenance'=>Session::get('actual_date_of_maintenance'),
							  'actual_time'=>Session::get('actual_time'),
							  'actual_duration'=>Session::get('actual_duration'),
							  'person_responsible'=>Session::get('person_responsible'),
							  'cell_no'=>Session::get('cell_no'),
							  'remarks'=>Session::get('remark'),
							  'amount'=>Session::get('amount'),
							  'advance'=>Session::get('advance'),
							  'discount'=>Session::get('discount'),
							  'total_amount'=>Session::get('total_amount'),
							  'bill_no'=>Session::get('bill'),
							  'extra_charge_remarks'=>Session::get('remark_extracharge'),
							  'vendor_id'=>Session::get('vendor_id'),
							  'vendor_type_id'=>Session::get('vendor_type_id')]);

		return redirect()->action('MaintenanceController@index',[$id]);

	}
	//for simple assign work finish

	//for completed maintenance gadget assign maintenance

	public function gadgetCompletedMaintenanceAssignWork($id)
	{
		$gadgetdeails=DB::table('m_gadget_details')
					->where('id',$id)
					->get();
					
		$vendordetails=DB::table('m_vendor_maintenance_details')
					 ->get();

		$maintenance_status=DB::table('dnf_maintenance_status')
							->get();

		return view('maintenance.gadget.maintenance.completem.assignwork',compact('id','gadgetdeails','vendordetails','maintenance_status'));
	}

	public function storeGadgetCompleteAssignWork(Request $request)
	{
		//to get requested values from form
		
		$id=$request->get('gadget_machine_id');
		$last_maintenance_date=$request->get('last_maintenance_date');
		$vendor=$request->get('vendor');
		$vendor_type_id=$request->get('vendor_type_id');
		$duedate=$request->get('duedate');
		$status=$request->get('status');
		
		$set_maintenance_date=$request->get('set_maintenance_date');
		$time=$request->get('time');

		//explde id's and names
		//for vendor
		$vendordetails=explode(",",$vendor);
		
		$vendor_id=$vendordetails[0];
		$vendor_name=$vendordetails[1];
		$vendor_type_id=$vendordetails[2];
		
		//for status
		$statusdetails=explode(",",$status);
		$status_id=$statusdetails[0];
		$status_name=$statusdetails[1];
		

		session(['gadget_machine_id'=>$id,
				 'last_maintenance_date'=>$last_maintenance_date,
				 'vendor_id'=>$vendor_id,
				 'vendor_name'=>$vendor_name,
				 'vendor_type_id'=>$vendor_type_id,
				 'duedate'=>$duedate,
				 'status_id'=>$status_id,
				 'status_name'=>$status_name,
				 'set_maintenance_date'=>$set_maintenance_date,
				 'time'=>$time]);
		
		
		return redirect()->action('MaintenanceController@gadgetCompletedMaintenanceCompltedM',[$id]);
	}
	public function gadgetCompletedMaintenanceCompltedM($id)
	{
		return view('maintenance.gadget.maintenance.completem.completedmaintenance',compact('id'));
	}

	public function storeGadgetCompleteMaintenance(Request $request)
	{
		$id=$request->get('gadget_machine_id');
		$date_of_maintenance=$request->get('date_of_maintenance');
		$remark=$request->get('remark');
		$time_complete=$request->get('time_complete');
		$duration=$request->get('duration');
		$person_responsible=$request->get('person_responsible');
		$cell=$request->get('cell');

		session(['actual_date_of_maintenance'=>$date_of_maintenance,
				 'remark'=>$remark,
				 'actual_time'=>$time_complete,
				 'actual_duration'=>$duration,
				 'person_responsible'=>$person_responsible,
				 'cell_no'=>$cell]);

		return redirect()->action('MaintenanceController@gadgetCompletedMaintenanceExtracharges',[$id]);
	}

	public function  gadgetCompletedMaintenanceExtracharges($id)
	{
		return view('maintenance.gadget.maintenance.completem.extracharges',compact('id'));
	}

	public function storeGadgetCompleteExteracharges(Request $request)
	{
		$id=$request->get('gadget_machine_id');
		$amount=$request->get('amount');
		$remark_extracharge=$request->get('remark_extracharge');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$total_amount=$request->get('total_amount');
		$bill=$request->get('bill');

		session(['amount'=>$amount,
				 'remark_extracharge'=>$remark_extracharge,
				 'advance'=>$advance,
				 'discount'=>$discount,
				 'total_amount'=>$total_amount,
				 'bill'=>$bill]);

			$assignmaintenance=new AssignMaintenance;

			$assignmaintenance->last_maintenance_date=Session::get('last_maintenance_date');
			$assignmaintenance->status=Session::get('status_id');
			
			$assignmaintenance->due_date=Session::get('duedate');
			$assignmaintenance->set_maintenance_date=Session::get('set_maintenance_date');
			$assignmaintenance->time=Session::get('time');

			$assignmaintenance->actual_date_of_maintenance=Session::get('actual_date_of_maintenance');
			$assignmaintenance->actual_time=Session::get('actual_time');
			$assignmaintenance->actual_duration=Session::get('actual_duration');
			$assignmaintenance->person_responsible=Session::get('person_responsible');
			$assignmaintenance->cell_no=Session::get('cell_no');
			$assignmaintenance->remarks=Session::get('remark');
			$assignmaintenance->amount=Session::get('amount');
			$assignmaintenance->advance=Session::get('advance');
			$assignmaintenance->discount=Session::get('discount');
			$assignmaintenance->total_amount=Session::get('total_amount');
			$assignmaintenance->bill_no=Session::get('bill');

			$assignmaintenance->extra_charge_remarks=Session::get('remark_extracharge');
			$assignmaintenance->vendor_id=Session::get('vendor_id');
			$assignmaintenance->vendor_type_id=Session::get('vendor_type_id');
			$assignmaintenance->gadget_id=Session::get('gadget_machine_id');

			$assignmaintenance->save();

		return redirect()->action('MaintenanceController@gadgetMaintenance');
	}

	//for completed maintenance gadget assign maintenance finish

	// get details for completed maintenance in gadget
	public function getDetailsAssignwork($id)
	{
				$gadgetdetails=DB::table('t_assign_maintenance')
						->where('t_assign_maintenance.gadget_id',$id)
						->join('m_gadget_details','m_gadget_details.id','=','t_assign_maintenance.gadget_id')
						->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','t_assign_maintenance.vendor_id')
						->join('dnf_maintenance_status','dnf_maintenance_status.id','=','t_assign_maintenance.status')
						->select('*','m_gadget_details.gadget_name','m_vendor_maintenance_details.name as vendorname','dnf_maintenance_status.name as statusname')
						->get();
						
		return view('maintenance.gadget.maintenance.completem.getdetailsassignwork',compact('id','gadgetdetails'));
	}

	public function getDetailsCompletedMaintenance($id)
	{
				$gadgetdetails=DB::table('t_assign_maintenance')
						->where('t_assign_maintenance.gadget_id',$id)
						->join('m_gadget_details','m_gadget_details.id','=','t_assign_maintenance.gadget_id')
						->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','t_assign_maintenance.vendor_id')
						->join('dnf_maintenance_status','dnf_maintenance_status.id','=','t_assign_maintenance.status')
						->select('*','m_gadget_details.gadget_name','m_vendor_maintenance_details.name as vendorname','dnf_maintenance_status.name as statusname')
						->get();

						/*dd($gadgetdetails);*/
		return view('maintenance.gadget.maintenance.completem.getdetailscompletedmaintenance',compact('id','gadgetdetails'));
	}

	public function getDetailsExteracharges($id)
	{
		$gadgetdetails=DB::table('t_assign_maintenance')
						->where('t_assign_maintenance.gadget_id',$id)
						->join('m_gadget_details','m_gadget_details.id','=','t_assign_maintenance.gadget_id')
						->join('m_vendor_maintenance_details','m_vendor_maintenance_details.id','=','t_assign_maintenance.vendor_id')
						->join('dnf_maintenance_status','dnf_maintenance_status.id','=','t_assign_maintenance.status')
						->select('*','m_gadget_details.gadget_name','m_vendor_maintenance_details.name as vendorname','dnf_maintenance_status.name as statusname')
						->get();

		return view('maintenance.gadget.maintenance.completem.getdetailsextracharges',compact('id','gadgetdetails'));
	}

	//ajax functions

	public function changeStatusComplete($gadgetId)
	{
		DB::table('t_assign_maintenance')
				->where('gadget_id',$gadgetId)
				->update(['status'=>2]);

		return 'success';
	}

	public function changeStatusCompleteMachine($machineId)
	{
		DB::table('t_assign_maintenance_machine')
				->where('machine_id',$machineId)
				->update(['status'=>2]);

		return 'success';
	}

}