<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;

use App\PurchaseOrder;
use App\PurchaseOrderMaterial;
use App\purchaseOrderVendor;
use App\purchaseOrderVendorMaterial;
use App\purchaseOrderVendorInstrument;
use App\purchaseOrderVendorGadget;
use App\purchaseOrderVendorMachine;
use App\PurchaseOrderDelivery;
use App\PurchaseOrderBilling;
use App\PurchaseOrderInstrument;
use App\PurchseOrderGadget;
use App\PurchaseOrderMachine;

class InventoryController extends Controller 
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function listViewMaterialManagement()
	{
		$materialdetails=DB::table('t_purchase_order_material')
		->join('m_material_details','t_purchase_order_material.material_name_id','=','m_material_details.id')
		->join('m_material_alerts','m_material_alerts.material_name_id','=','m_material_details.id')
		->join('s_material_type','s_material_type.id','=','m_material_details.material_type_id')
		->join('s_material_subtype','s_material_subtype.id','=','m_material_details.material_subtype_id')
		->select('*','m_material_details.id as material_id','s_material_type.name as material_type','s_material_subtype.name as material_subtype',DB::raw('sum(quantity) as quantity'))
		->groupby('t_purchase_order_material.purchase_order_id')
		->get();

		return view('inventory.materialmanagement.listViewMaterialManagement',compact('materialdetails'));
	}

	public function getDetailsMaterial($material_id)
	{
		$vendor_material_details=DB::table('t_purchase_order_material')
		->where('t_purchase_order.id',$material_id)
		->join('m_material_details','t_purchase_order_material.material_name_id','=','m_material_details.id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_material.purchase_order_id')
		->leftjoin('t_po_billing','t_po_billing.purchase_order_id','=','t_purchase_order_material.purchase_order_id')
		->select('*','t_purchase_order.id as po_id','t_purchase_order_material.id as id','t_purchase_order.id as purchase_order_id')
		->get();

		$material_name=$vendor_material_details[0]->material_name;

		$po_id = $vendor_material_details[0]->purchase_order_id;

		$po_delivery=DB::table('t_po_delivery')
		->orderBy('id','desc')
		->where('purchase_order_id',$po_id)
		->select('*')
		->get();

		return view('inventory.materialmanagement.getDetailsInventory',compact('material_name','vendor_material_details','po_id','po_delivery'));
	}


	public function listViewInstrumentManagement()
	{
		$instrumentdetails=DB::table('t_purchase_order_instrument')
		->join('m_instrument_details','t_purchase_order_instrument.instrument_name_id','=','m_instrument_details.id')
		->join('s_instrument_type','s_instrument_type.id','=','m_instrument_details.instrument_type_id')
		->join('s_instrument_subtype','s_instrument_subtype.instrument_type_id','=','m_instrument_details.instrument_subtype_id')
		->select('*','m_instrument_details.id as instrument_id','s_instrument_type.name as instrument_type','s_instrument_subtype.name as instrument_subtype')
		->get();

		return view('inventory.instrumentmanagement.dashboardInstrumentmanagement',compact('instrumentdetails'));
	}

	public function getDetailsInstrument($instrument_id)
	{
		$vendor_instrument_details=DB::table('t_purchase_order_instrument')
		->where('t_purchase_order_instrument.instrument_name_id',$instrument_id)
		->join('m_instrument_details','t_purchase_order_instrument.instrument_name_id','=','m_instrument_details.id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_instrument.purchase_order_id')
		->join('t_po_billing','t_po_billing.purchase_order_id','=','t_purchase_order_instrument.purchase_order_id')
		->get();

		$vendor_instrument_details=$vendor_instrument_details[0];

		return view('inventory.instrumentmanagement.getdetailsInstrument',compact('vendor_instrument_details'));
	}

	public function InstrumentInLab()
	{
		return view('inventory.instrumentmanagement.instrumentInLab');
	}

	public function listViewMachineManagement()
	{
		$machinedetails=DB::table('t_purchase_order_machine')
		->join('m_machine_details','t_purchase_order_machine.machine_name_id','=','m_machine_details.id')
		->join('s_machine_type','s_machine_type.id','=','m_machine_details.machine_type_id')
		->join('s_machine_subtype','s_machine_subtype.machine_type_id','=','m_machine_details.machine_subtype_id')
		->select('*','m_machine_details.id as machine_id','s_machine_type.name as machine_type','s_machine_subtype.name as machine_subtype')
		->get();

		return view('inventory.machinemanagement.dashboardmachinemanagement',compact('machinedetails'));
	}

	public function getdetailsMachine($machine_id)
	{     
		$vendor_machine_details=DB::table('t_purchase_order_machine')
		->where('t_purchase_order_machine.machine_name_id',$machine_id)
		->join('m_machine_details','t_purchase_order_machine.machine_name_id','=','m_machine_details.id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_machine.purchase_order_id')
		->join('t_po_billing','t_po_billing.purchase_order_id','=','t_purchase_order_machine.purchase_order_id')
		->get();

		$vendor_machine_details=$vendor_machine_details[0];                    

		return view('inventory.machinemanagement.getdetailsMachine',compact('vendor_machine_details'));
	}

	public function listViewGadgetManagement()
	{
		$gadgetdetails=DB::table('t_purchase_order_gadget')
		->join('m_gadget_details','t_purchase_order_gadget.gadget_name_id','=','m_gadget_details.id')
		->join('s_gadget_type','s_gadget_type.id','=','m_gadget_details.gadget_type_id')
		->join('s_gadget_subtype','s_gadget_subtype.id','=','m_gadget_details.gadget_subtype_id')
		->select('*','m_gadget_details.id as gadget_id','s_gadget_type.name as gadget_type','s_gadget_subtype.name as gadget_subtype',DB::raw('sum(quantity) as quantity'))
		->groupby('t_purchase_order_gadget.purchase_order_id')
		
		->select('*','purchase_order_id as po_id','m_gadget_details.id as gadget_id','s_gadget_type.name as gadget_type','s_gadget_subtype.name as gadget_subtype',DB::raw('sum(quantity) as quantity'))
		->get();

		return view('inventory.gadgetmanagement.dashboardgadgetmanagement',compact('gadgetdetails'));
	}

	public function getdetailsGadget($gadget_id)
	{
		$vendor_gadget_details=DB::table('t_purchase_order_gadget')
		->where('t_purchase_order.id',$po_id)
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_gadget.purchase_order_id')
		->join('m_gadget_details','t_purchase_order_gadget.gadget_name_id','=','m_gadget_details.id')
		->leftjoin('t_po_billing','t_po_billing.purchase_order_id','=','t_purchase_order_gadget.purchase_order_id')
		->select('*','t_purchase_order.id as po_id','t_purchase_order_gadget.id as id','t_purchase_order.id as purchase_order_id')
		->get();
		
		$gadget_name=$vendor_gadget_details[0]->gadget_name;     

		return view('inventory.gadgetmanagement.getdetailsgadget',compact('vendor_gadget_details','gadget_name'));
	}

	public function dashboardPurchaseorder()
	{
		$po_id=DB::table('t_purchase_order')
		->orderBy('id','desc')
		->first();

		if($po_id == null)
		{
			$po_id = (object)array('id' => 0);
		}

		$po_id = $po_id->id + 1;

		$po_material=DB::table('t_purchase_order_vendor_material')
		->join('t_purchase_order_vendor','t_purchase_order_vendor.id','=','t_purchase_order_vendor_material.po_vendor_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_vendor.purchase_order_id')
		->join('m_vendor_material_details','m_vendor_material_details.id','=','t_purchase_order_vendor.vendor_name_id')
		->leftjoin('t_po_billing','t_po_billing.purchase_order_id','=','t_purchase_order_vendor.purchase_order_id')
		->leftjoin('t_po_delivery','t_po_delivery.purchase_order_id','=','t_purchase_order_vendor.purchase_order_id')
		->select('*','t_purchase_order_vendor.purchase_order_id as po_id','m_vendor_material_details.name as vendorname')
		->get();
		/*dd($po_material);*/
		return view('inventory.purchaseorder.material.dashboardpurchaseorder',compact('po_id','po_material'));
	}

	public function PurchaseOrderAdd($po_id)
	{


		$material_type=DB::table('s_material_type')
		->get();

		$order_status=DB::table('dnf_purchase_order_status')
		->get();

		$curr_po_details=DB::table('t_purchase_order')
		->where('t_purchase_order.id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_purchase_order.purchase_order_status_id')
		->select('*','t_purchase_order.id')
		->get();


		if($curr_po_details != null)
		{
			$purchase_ord_id=$curr_po_details['0']->id;
			$curr_mat_details=DB::table('t_purchase_order_material')
			->where('t_purchase_order_material.purchase_order_id',$purchase_ord_id)
			->join('m_material_details','m_material_details.id','=','t_purchase_order_material.material_name_id')
			->join('s_material_subtype','s_material_subtype.id','=','m_material_details.material_subtype_id')
			->join('s_material_type','s_material_type.id','=','s_material_subtype.material_type_id')
			->select('*','m_material_details.material_name as material_name','s_material_subtype.name as material_subtype','s_material_type.name as material_type','t_purchase_order_material.id as mat_id')
			->get();

			return view('inventory.purchaseorder.material.editPurchaseOrder',compact('curr_mat_details','material_type','order_status','po_id','curr_po_details','purchase_ord_id'));
		}
		else
		{
			return view('inventory.purchaseorder.material.AddPurchaseOrder',compact('material_type','order_status','po_id'));
		}

	}

	public function strorePurchaseorder(Request $request)
	{
		$po_id=$request->get('po_id');

		/*requested material */
		$material_type=$request->get('material_type');
		$material_subtype=$request->get('material_subtype');
		$mat_name_id=$request->get('mat_name');
		$quantity=$request->get('quantity');

		/*requested po details*/
		$dateoforder=$request->get('dateoforder');
		$expecteddateoforder=$request->get('expecteddateoforder');
		$order_status_id=$request->get('order_status_id');

		/*add data to purchase order*/
		$purchaseorder = new PurchaseOrder;
		$purchaseorder->date_of_order=$dateoforder;
		$purchaseorder->expected_date_of_delivery=$expecteddateoforder;
		$purchaseorder->purchase_order_status_id=$order_status_id;
		$purchaseorder->save();

		/*get purchase order id for material add */
		$purchase_id=DB::table('t_purchase_order')
		->orderBy('id','desc')
		->first();

		$purchase_order_id=$purchase_id->id;

		/*add material details*/		
		foreach($mat_name_id as $key=>$material)
		{
			$purchaseorderMat = new PurchaseOrderMaterial;
			$purchaseorderMat->material_name_id=$material;
			$purchaseorderMat->quantity=$quantity[$key];
			$purchaseorderMat->purchase_order_id=$purchase_order_id;

			$purchaseorderMat->save();
		}


		return redirect()->action('InventoryController@VendorDetails',[$po_id]);

	}


	public function updatePurchaseorder(Request $request)
	{

		$po_id=$request->get('po_id');

		$material_type=$request->get('material_type');
		$material_subtype=$request->get('material_subtype');
		$mat_name_id=$request->get('mat_name');
		$quantity=$request->get('quantity');


		$dateoforder=$request->get('dateoforder');
		$expecteddateoforder=$request->get('expecteddateoforder');
		$order_status_id=$request->get('order_status_id');

		DB::table('t_purchase_order')
		->where('id',$po_id)
		->update(['date_of_order'=>$dateoforder,
			'expected_date_of_delivery'=>$expecteddateoforder,
			'purchase_order_status_id'=>$order_status_id]);


		$podetail=DB::table('t_purchase_order')
		->where('id',$po_id)
		->select('id')
		->get();
		$purchase_order_id=$podetail['0']->id;/*id from t_purchase_order for foreign key to t_purchase+order_material*/

		foreach($mat_name_id as $key=>$material)
		{
			/*if user does not select any option for material*/
			if($material=='')
			{
				return redirect()->action('InventoryController@VendorDetails',[$po_id]);
			}
			else
			{
				$purchaseorderMat = new PurchaseOrderMaterial;
				$purchaseorderMat->material_name_id=$material;
				$purchaseorderMat->quantity=$quantity[$key];
				$purchaseorderMat->purchase_order_id=$purchase_order_id;

				$purchaseorderMat->save();

				return redirect()->action('InventoryController@VendorDetails',[$po_id]);
			}
		}
	}

	public function deleteMaterial($mat_id,$po_id)
	{
		DB::table('t_purchase_order_material')
		->where('id',$mat_id)
		->where('purchase_order_id',$po_id)
		->delete();

		return redirect()->action('InventoryController@PurchaseOrderAdd',[$po_id]);
	}

	public function VendorDetails($po_id)
	{

		$vendornames=DB::table('m_vendor_material_details')
		->get();

		$vendors=DB::table('t_purchase_order_material')
		->where('purchase_order_id',$po_id)
		->join('m_material_details','m_material_details.id','=','t_purchase_order_material.material_name_id')
		->join('m_vendor_mat_workspec_details','m_vendor_mat_workspec_details.mat_name_id','=','m_material_details.id')
		->join('m_vendor_material_details','m_vendor_material_details.id','=','m_vendor_mat_workspec_details.vendor_mat_id')
		->get();
		/*dd($vendors);*/

		$vendorlist=DB::table('t_purchase_order_material')
		->where('t_purchase_order_material.purchase_order_id',$po_id)
		->join('m_material_details','m_material_details.id','=','t_purchase_order_material.material_name_id')
		->join('m_vendor_mat_workspec_details','m_vendor_mat_workspec_details.mat_name_id','=','m_material_details.id')
		->join('m_vendor_material_details','m_vendor_material_details.id','=','m_vendor_mat_workspec_details.vendor_mat_id')
		->join('t_purchase_order_vendor','t_purchase_order_vendor.purchase_order_id','=','t_purchase_order_material.purchase_order_id')
		->join('t_purchase_order_vendor_material','t_purchase_order_vendor_material.po_vendor_id','=','t_purchase_order_vendor.id')
		->get();

		if($vendorlist != null)
		{

			$ven_material=DB::table('t_purchase_order_vendor_material')
			->where('t_purchase_order_vendor.purchase_order_id',$po_id)
			->where('t_purchase_order_material.purchase_order_id',$po_id)
			->join('t_purchase_order_vendor','t_purchase_order_vendor.id','=','t_purchase_order_vendor_material.po_vendor_id')
			->join('t_purchase_order_material','t_purchase_order_material.material_name_id','=','t_purchase_order_vendor_material.material_order_id')
			->join('m_material_details','m_material_details.id','=','t_purchase_order_material.material_name_id')
			->join('m_vendor_material_details','m_vendor_material_details.id','=','t_purchase_order_vendor.vendor_name_id')
			->get();

			return view('inventory.purchaseorder.material.editDetalisVendor',compact('vendorlist','po_id','vendornames','ven_material','vendors'));
		}
		else
		{
			return view('inventory.purchaseorder.material.DetalisVendor',compact('vendorlist','po_id','vendornames','vendors'));
		}

	}

	public function storevendorDetailsMaterial(Request $request)
	{
		$po_id=$request->get('po_id');

		$mat_name_id=$request->get('mat_name_id');
		$vendor_mat_id=$request->get('vendor_mat_id');
		$rate=$request->get('rate');
		$amount=$request->get('amount');
		$totalamt=$request->get('totalamt');
		$discount=$request->get('discount');
		$finalamount=$request->get('finalamount');

		$povendor=new purchaseOrderVendor;
		$povendor->total_amount=$totalamt;
		$povendor->discount=$discount;
		$povendor->final_amount=$finalamount;
		$povendor->vendor_name_id=$vendor_mat_id;
		$povendor->purchase_order_id=$po_id;

		$povendor->save();

		$po_vendor=DB::table('t_purchase_order_vendor')
		->where('purchase_order_id',$po_id)
		->select('id')
		->get();
		$po_vendor_id=$po_vendor['0']->id;

		foreach($mat_name_id as $key=>$material)
		{
			$povendormat=new purchaseOrderVendorMaterial;
			$povendormat->rate=$rate[$key];
			$povendormat->amount=$amount[$key];
			$povendormat->po_vendor_id=$po_vendor_id;
			$povendormat->material_order_id=$material;

			$povendormat->save();
		}

		return redirect()->action('InventoryController@GenerateDeliveryAdd',[$po_id]);
	}

	public function updateVendorDetailsMaterial(Request $request)
	{
		$po_id=$request->get('po_id');

		$mat_name_id=$request->get('mat_name_id');
		$vendor_mat_id=$request->get('vendor_mat_id');
		$rate=$request->get('rate');
		$amount=$request->get('amount');
		$totalamt=$request->get('curr_totalamt');
		$discount=$request->get('discount');
		$finalamount=$request->get('finalamount');


		/*dd($totalamt);*/
		if($vendor_mat_id == null)
		{
			DB::table('t_purchase_order_vendor')
			->where('purchase_order_id',$po_id)
			->update(['discount'=>$discount,
				'final_amount'=>$finalamount]);
		}
		else
		{
			DB::table('t_purchase_order_vendor')
			->where('purchase_order_id',$po_id)
			->update(['total_amount'=>$totalamt,
				'discount'=>$discount,
				'final_amount'=>$finalamount,
				'vendor_name_id'=>$vendor_mat_id]);

			$po_vendor=DB::table('t_purchase_order_vendor')
			->where('purchase_order_id',$po_id)
			->select('id')
			->get();

			$po_vendor_id=$po_vendor['0']->id;

			DB::table('t_purchase_order_vendor_material')
			->where('po_vendor_id',$po_vendor_id)
			->delete();

			foreach($mat_name_id as $key=>$material)
			{
				$povendormat=new purchaseOrderVendorMaterial;
				$povendormat->rate=$rate[$key];
				$povendormat->amount=$amount[$key];
				$povendormat->po_vendor_id=$po_vendor_id;
				$povendormat->material_order_id=$material;

				$povendormat->save();			
			}	
		}

		return redirect()->action('InventoryController@GenerateDeliveryAdd',[$po_id]);

	}

	public function GenerateDeliveryAdd($po_id)
	{
		$curr_delivery=DB::table('t_po_delivery')
		->where('purchase_order_id',$po_id)
		->get();


		return view('inventory.purchaseorder.material.AddGenerateDelivery',compact('po_id','curr_delivery'));
	}

	public function storeGenarateDelivery(Request $request)
	{
		$po_id=$request->get('po_id');
		$challan_no=$request->get('challan_no');
		$date_of_delivery=$request->get('date_of_delivery');
		$delivery_location=$request->get('delivery_location');

		foreach($challan_no as $key=>$challan)
		{
			$podelivery = new PurchaseOrderDelivery;
			$podelivery->chalan_number=$challan;
			$podelivery->delivery_date=$date_of_delivery[$key];
			$podelivery->delivery_location=$delivery_location[$key];
			$podelivery->purchase_order_id=$po_id;
			$podelivery->save();
		}

		return redirect()->action('InventoryController@GenerateBillingAdd',[$po_id]);
	}

	public function deleteDelivery($po_id,$id)
	{

		DB::table('t_po_delivery')
		->where('purchase_order_id',$po_id)
		->where('id',$id)
		->delete();

		return redirect()->action('InventoryController@GenerateDeliveryAdd',[$po_id]);
	}

	public function GenerateBillingAdd($po_id)
	{
		$bill_status=DB::table('dnf_purchase_order_status')
		->get();

		$curr_bill=Db::table('t_po_billing')
		->where('purchase_order_id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_po_billing.purchase_order_status_id')
		->get();

		if($curr_bill == null)
		{
			return view('inventory.purchaseorder.material.AddGenerateBilling',compact('po_id','bill_status'));
		}
		else
		{
			return view('inventory.purchaseorder.material.editGenerateBilling',compact('po_id','bill_status','curr_bill'));
		}

	}

	public function storeGenerateBilling(Request $request)
	{
		$po_id=$request->get('po_id');
		$bill_no=$request->get('bill_no');
		$bill_status=$request->get('bill_status');
		$dateofbilling=$request->get('dateofbilling');
		$amt=$request->get('amt');
		$dateofpayment=$request->get('dateofpayment');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$final_cost=$request->get('final_cost');

		$pobilling = new PurchaseOrderBilling;
		$pobilling->purchase_order_id=$po_id;
		$pobilling->bill_number=$bill_no;
		$pobilling->date_of_billing=$dateofbilling;
		$pobilling->advance	=$advance;
		$pobilling->discount=$discount;
		$pobilling->amount=$amt;
		$pobilling->purchase_order_status_id=$bill_status;
		$pobilling->purchase_order_id=$po_id;

		$pobilling->save();

		return redirect()->action('InventoryController@dashboardPurchaseorder',[$po_id]);
	}

	public function updateGeneralBilling(Request $request)
	{
		$po_id=$request->get('po_id');
		$bill_no=$request->get('bill_no');
		$bill_status=$request->get('bill_status');
		$dateofbilling=$request->get('dateofbilling');
		$amt=$request->get('amt');
		$dateofpayment=$request->get('dateofpayment');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$final_cost=$request->get('final_cost');

		DB::table('t_po_billing')
		->where('purchase_order_id',$po_id)
		->update(['bill_number'=>$bill_no,
			'date_of_billing'=>$dateofbilling,
			'advance'=>$advance,
			'discount'=>$discount,
			'amount'=>$amt,
			'final_cost'=>$final_cost,
			'purchase_order_status_id'=>$bill_status]);

		return redirect()->action('InventoryController@dashboardPurchaseorder',[$po_id]);
	}

	public function viewPurchaseOrderMaterial($po_id)
	{

		$material_type=DB::table('s_material_type')
		->get();

		$order_status=DB::table('dnf_purchase_order_status')
		->get();

		$curr_po_details=DB::table('t_purchase_order')
		->where('t_purchase_order.id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_purchase_order.purchase_order_status_id')
		->select('*','t_purchase_order.id')
		->get();	

		$purchase_ord_id=$curr_po_details['0']->id;

		$curr_mat_details=DB::table('t_purchase_order_material')
		->where('t_purchase_order_material.purchase_order_id',$purchase_ord_id)
		->join('m_material_details','m_material_details.id','=','t_purchase_order_material.material_name_id')
		->join('s_material_subtype','s_material_subtype.id','=','m_material_details.material_subtype_id')
		->join('s_material_type','s_material_type.id','=','s_material_subtype.material_type_id')
		->select('*','m_material_details.material_name as material_name','s_material_subtype.name as material_subtype','s_material_type.name as material_type','t_purchase_order_material.id as mat_id')
		->get();	

		return view('inventory.purchaseorder.material.viewpurchaseorder',compact('po_id','material_type','order_status','curr_po_details','curr_mat_details'));
	}

	public function viewvendorDetailsMaterial($po_id)
	{

		$vendordetails=DB::table('t_purchase_order_vendor_material')
		->where('t_purchase_order_vendor.purchase_order_id',$po_id)
		->join('t_purchase_order_vendor','t_purchase_order_vendor.id','=','t_purchase_order_vendor_material.po_vendor_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_vendor.purchase_order_id')
		->join('m_vendor_material_details','m_vendor_material_details.id','=','t_purchase_order_vendor.vendor_name_id')
		->join('m_material_details','m_material_details.id','=','t_purchase_order_vendor_material.material_order_id')
		->join('t_purchase_order_material','t_purchase_order_material.purchase_order_id','=','t_purchase_order_vendor.purchase_order_id')
		->get();


		return view('inventory.purchaseorder.material.viewvendordetails',compact('po_id','vendordetails'));
	}

	public function viewGenerateDeliveryMaterial($po_id)
	{

		$curr_delivery=DB::table('t_po_delivery')
		->where('purchase_order_id',$po_id)
		->get();
		return view('inventory.purchaseorder.material.viewgeneratedelivery',compact('po_id','curr_delivery'));
	}

	public function viewgenerateBillingMaterial($po_id)
	{
		$bill_status=DB::table('dnf_purchase_order_status')
		->get();

		$curr_bill=Db::table('t_po_billing')
		->where('purchase_order_id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_po_billing.purchase_order_status_id')
		->select('*')
		->get();

		return view('inventory.purchaseorder.material.viewgeneratebilling',compact('po_id','bill_status','curr_bill'));
	}

	public function listviewGenerateBillingMaterial($po_id)
	{
		$bill_status=DB::table('dnf_purchase_order_status')
		->get();
		
		return view('inventory.purchaseorder.material.AddGenerateBilling',compact('po_id','bill_status'));
	}

	public function listviewInstrument()
	{
		$po_id=DB::table('t_purchase_order')
		->orderBy('id','desc')
		->first();

		if($po_id == null)
		{
			$po_id = (object)array('id' => 0);
		}

		$po_id = $po_id->id + 1;

		$po_instrument=DB::table('t_purchase_order_vendor_instrument')
		->join('t_purchase_order_vendor','t_purchase_order_vendor.id','=','t_purchase_order_vendor_instrument.po_vendor_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_vendor.purchase_order_id')
		->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','t_purchase_order_vendor.vendor_name_id')
		->leftjoin('t_po_billing','t_po_billing.purchase_order_id','=','t_purchase_order_vendor.purchase_order_id')
		->leftjoin('t_po_delivery','t_po_delivery.purchase_order_id','=','t_purchase_order_vendor.purchase_order_id')
		->select('*','t_purchase_order_vendor.purchase_order_id as po_id','m_vendor_instrument_details.name as vendorname')
		->get();


		return view('inventory.purchaseorder.instrument.listviewinstrument',compact('po_id','po_instrument'));
	}

	public function addPurchaseorderInstrument($po_id)
	{
		$instrument_type=DB::table('s_instrument_type')
		->get();


		$order_status=DB::table('dnf_purchase_order_status')
		->get();



		$curr_po=DB::table('t_purchase_order')
		->where('t_purchase_order.id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_purchase_order.purchase_order_status_id')
		->get();

		if($curr_po != null)
		{
			$curr_instrument=DB::table('t_purchase_order_instrument')
			->where('purchase_order_id',$po_id)
			->join('m_instrument_details','m_instrument_details.id','=','t_purchase_order_instrument.instrument_name_id')
			->join('s_instrument_subtype','s_instrument_subtype.id','=','m_instrument_details.instrument_subtype_id')
			->join('s_instrument_type','s_instrument_type.id','=','s_instrument_subtype.instrument_type_id')
			->select('*','s_instrument_type.name as instrument_type','s_instrument_subtype.name as instrument_subtype')
			->get();

			return view('inventory.purchaseorder.instrument.editPurchaseOrderDetails',compact('po_id','instrument_type','order_status','curr_instrument','curr_po'));
		}
		else
		{
			return view('inventory.purchaseorder.instrument.addPurchaseOrderDetails',compact('po_id','instrument_type','order_status'));
		}

	}

	public function storePurchaseOrderInstrument(Request $request)
	{
		$po_id=$request->get('po_id');

		$dateoforder=$request->get('dateoforder');
		$expecteddateoforder=$request->get('expecteddateoforder');
		$order_status_id=$request->get('order_status_id');

		$instrument_type=$request->get('instrument_type');
		$instrument_subtype=$request->get('instrument_subtype');
		$instrument_name_id=$request->get('instrument_name');
		$quantity=$request->get('quantity');

		$poinstrument=new PurchaseOrder;
		$poinstrument->date_of_order=$dateoforder;
		$poinstrument->expected_date_of_delivery=$expecteddateoforder;
		$poinstrument->purchase_order_status_id=$order_status_id;

		$poinstrument->save();

		foreach($instrument_name_id as $key=>$instrument)
		{
			$poinstrDet=new PurchaseOrderInstrument;
			$poinstrDet->instrument_name_id=$instrument;
			$poinstrDet->quantity=$quantity[$key];
			$poinstrDet->purchase_order_id=$po_id;

			$poinstrDet->save();
		}

		return redirect()->action('InventoryController@addVendorDetailsInstrument',[$po_id]);

	}

	public function deleteInstrument($instrument_name_id,$po_id)
	{
		DB::table('t_purchase_order_instrument')
		->where('instrument_name_id',$instrument_name_id)
		->where('purchase_order_id',$po_id)
		->delete();

		return redirect()->action('InventoryController@addPurchaseorderInstrument',[$po_id]);
	}

	public function updatePurchaseOrderInstrument(Request $request)
	{
		$po_id=$request->get('po_id');

		$dateoforder=$request->get('dateoforder');
		$expecteddateoforder=$request->get('expecteddateoforder');
		$order_status_id=$request->get('order_status_id');

		$instrument_type=$request->get('instrument_type');
		$instrument_subtype=$request->get('instrument_subtype');
		$instrument_name_id=$request->get('instrument_name');
		$quantity=$request->get('quantity');

		DB::table('t_purchase_order')
		->where('id',$po_id)
		->update(['date_of_order'=>$dateoforder,
			'expected_date_of_delivery'=>$expecteddateoforder,
			'purchase_order_status_id'=>$order_status_id]);

		foreach($instrument_name_id as $key=>$instrument)
		{
			/*if user does not select any option for instrument*/
			if($instrument=='')
			{
				return redirect()->action('InventoryController@addVendorDetailsInstrument',[$po_id]);
			}
			else
			{
				$purchaseorder = new PurchaseOrderInstrument;
				$purchaseorder->instrument_name_id=$instrument;
				$purchaseorder->quantity=$quantity[$key];
				$purchaseorder->purchase_order_id=$po_id;

				$purchaseorder->save();

				return redirect()->action('InventoryController@addVendorDetailsInstrument',[$po_id]);
			}
		}


	}

	public function addVendorDetailsInstrument($po_id)
	{
		$vendornames=DB::table('m_vendor_instrument_details')
		->get();



		$vendorlist=DB::table('t_purchase_order_instrument')
		->where('t_purchase_order_instrument.purchase_order_id',$po_id)
		->join('m_instrument_details','m_instrument_details.id','=','t_purchase_order_instrument.instrument_name_id')
		->join('m_vendor_inst_workspec_details','m_vendor_inst_workspec_details.inst_name_id','=','m_instrument_details.id')
		->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','m_vendor_inst_workspec_details.vendor_inst_id')
		->join('t_purchase_order_vendor','t_purchase_order_vendor.purchase_order_id','=','t_purchase_order_instrument.purchase_order_id')
		->join('t_purchase_order_vendor_instrument','t_purchase_order_vendor_instrument.po_vendor_id','=','t_purchase_order_vendor.id')
		->get();

		if($vendorlist != null)
		{
			$vendors=DB::table('t_purchase_order_instrument')
			->where('purchase_order_id',$po_id)
			->join('m_instrument_details','m_instrument_details.id','=','t_purchase_order_instrument.instrument_name_id')
			->join('m_vendor_inst_workspec_details','m_vendor_inst_workspec_details.inst_name_id','=','m_instrument_details.id')
			->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','m_vendor_inst_workspec_details.vendor_inst_id')
			->get();

			$ven_instrument=DB::table('t_purchase_order_vendor_instrument')
			->where('t_purchase_order_vendor.purchase_order_id',$po_id)
			->where('t_purchase_order_instrument.purchase_order_id',$po_id)
			->join('t_purchase_order_vendor','t_purchase_order_vendor.id','=','t_purchase_order_vendor_instrument.po_vendor_id')
			->join('t_purchase_order_instrument','t_purchase_order_instrument.instrument_name_id','=','t_purchase_order_vendor_instrument.instrument_order_id')
			->join('m_instrument_details','m_instrument_details.id','=','t_purchase_order_instrument.instrument_name_id')
			->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','t_purchase_order_vendor.vendor_name_id')
			->get();

			return view('inventory.purchaseorder.instrument.editDetailsVendor',compact('po_id','vendorlist','vendornames','vendors','ven_instrument'));
		}
		else
		{
			$vendors=DB::table('t_purchase_order_instrument')
			->where('purchase_order_id',$po_id)
			->join('m_instrument_details','m_instrument_details.id','=','t_purchase_order_instrument.instrument_name_id')
			->join('m_vendor_inst_workspec_details','m_vendor_inst_workspec_details.inst_name_id','=','m_instrument_details.id')
			->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','m_vendor_inst_workspec_details.vendor_inst_id')
			->get();

			return view('inventory.purchaseorder.instrument.addDetailsVendor',compact('po_id','vendorlist','vendornames','vendors'));
		}

	}

	public function storeVendorDetailsInstrument(Request $request)
	{
		$po_id=$request->get('po_id');
		$inst_name_id=$request->get('inst_name_id');
		$vendor_inst_id=$request->get('vendor_inst_id');
		$rate=$request->get('rate');
		$amount=$request->get('amount');
		$totalamt=$request->get('totalamt');
		$discount=$request->get('discount');
		$finalamount=$request->get('finalamount');

		$povendor=new purchaseOrderVendor;
		$povendor->total_amount=$totalamt;
		$povendor->discount=$discount;
		$povendor->final_amount=$finalamount;
		$povendor->vendor_name_id=$vendor_inst_id;
		$povendor->purchase_order_id=$po_id;

		$povendor->save();

		$po_vendor=DB::table('t_purchase_order_vendor')
		->where('purchase_order_id',$po_id)
		->select('id')
		->get();
		$po_vendor_id=$po_vendor['0']->id;

		foreach($inst_name_id as $key=>$instrument)
		{
			$povendorinstr=new purchaseOrderVendorInstrument;
			$povendorinstr->rate=$rate[$key];
			$povendorinstr->amount=$amount[$key];
			$povendorinstr->po_vendor_id=$po_vendor_id;
			$povendorinstr->instrument_order_id=$instrument;

			$povendorinstr->save();
		}

		return redirect()->action('InventoryController@addGenerateDeliveryInstrument',[$po_id]);

	}

	public function updateVendorDetailsInstrument(Request $request)
	{
		$po_id=$request->get('po_id');

		$inst_name_id=$request->get('inst_name_id');
		$vendor_inst_id=$request->get('vendor_inst_id');
		$rate=$request->get('rate');
		$amount=$request->get('amount');
		$totalamt=$request->get('curr_totalamt');
		$discount=$request->get('discount');
		$finalamount=$request->get('finalamount');



		if($vendor_inst_id == null)
		{
			DB::table('t_purchase_order_vendor')
			->where('purchase_order_id',$po_id)
			->update(['discount'=>$discount,
				'final_amount'=>$finalamount]);
		}
		else
		{
			DB::table('t_purchase_order_vendor')
			->where('purchase_order_id',$po_id)
			->update(['total_amount'=>$totalamt,
				'discount'=>$discount,
				'final_amount'=>$finalamount,
				'vendor_name_id'=>$vendor_inst_id]);

			$po_vendor=DB::table('t_purchase_order_vendor')
			->where('purchase_order_id',$po_id)
			->select('id')
			->get();

			$po_vendor_id=$po_vendor['0']->id;

			DB::table('t_purchase_order_vendor_instrument')
			->where('po_vendor_id',$po_vendor_id)
			->delete();

			foreach($inst_name_id as $key=>$instrument)
			{
				$povendorinstr=new purchaseOrderVendorInstrument;
				$povendorinstr->rate=$rate[$key];
				$povendorinstr->amount=$amount[$key];
				$povendorinstr->po_vendor_id=$po_vendor_id;
				$povendorinstr->instrument_order_id=$instrument;

				$povendorinstr->save();			
			}	
		}

		return redirect()->action('InventoryController@addGenerateDeliveryInstrument',[$po_id]);
	}

	public function addGenerateDeliveryInstrument($po_id)
	{
		$curr_delivery=DB::table('t_po_delivery')
		->where('purchase_order_id',$po_id)
		->get();
		return view('inventory.purchaseorder.instrument.addGenerateDelivery',compact('po_id','curr_delivery'));
	}

	public function deleteInstrumentChallan($po_id,$id)
	{
		DB::table('t_po_delivery')
		->where('purchase_order_id',$po_id)
		->where('id',$id)
		->delete();

		return redirect()->action('InventoryController@addGenerateDeliveryInstrument',[$po_id]);
	}

	public function storeDeliveryInstrument(Request $request)
	{
		$po_id=$request->get('po_id');
		$challan_no=$request->get('challan_no');
		$date_of_delivery=$request->get('date_of_delivery');
		$delivery_location=$request->get('delivery_location');

		foreach($challan_no as $key=>$challan)
		{
			$podelivery=new PurchaseOrderDelivery;
			$podelivery->chalan_number=$challan;
			$podelivery->delivery_date=$date_of_delivery[$key];
			$podelivery->delivery_location=$delivery_location[$key];
			$podelivery->purchase_order_id=$po_id;

			$podelivery->save();
		}

		return redirect()->action('InventoryController@addGenerateBillingInstrument',[$po_id]);
	}

	public function addGenerateBillingInstrument($po_id)
	{

		$bill_status=DB::table('dnf_purchase_order_status')
		->get();

		$curr_bill=Db::table('t_po_billing')
		->where('purchase_order_id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_po_billing.purchase_order_status_id')
		->get();

		if($curr_bill == null)
		{
			return view('inventory.purchaseorder.instrument.addGenerateBilling',compact('po_id','bill_status'));
		}
		else
		{
			return view('inventory.purchaseorder.instrument.editGenerateBilling',compact('po_id','bill_status','curr_bill'));
		}


	}

	public function storegenareteBillingInstrument(Request $request)
	{
		$po_id=$request->get('po_id');

		$bill_no=$request->get('bill_no');
		$bill_status=$request->get('bill_status');
		$dateofbilling=$request->get('dateofbilling');
		$amt=$request->get('amt');
		$dateofpayment=$request->get('dateofpayment');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$final_cost=$request->get('final_cost');

		$pobilling = new PurchaseOrderBilling;
		$pobilling->purchase_order_id=$po_id;
		$pobilling->bill_number=$bill_no;

		$pobilling->date_of_billing=$dateofbilling;
		$pobilling->advance	=$advance;
		$pobilling->discount=$discount;
		$pobilling->amount=$amt;
		$pobilling->purchase_order_status_id=$bill_status;
		$pobilling->purchase_order_id=$po_id;

		$pobilling->save();

		return redirect()->action('InventoryController@listviewInstrument',[$po_id]);

	}

	public function updateGenerateBillingInstrument(request $request)
	{
		$po_id=$request->get('po_id');
		$bill_no=$request->get('bill_no');
		$bill_status=$request->get('bill_status');
		$dateofbilling=$request->get('dateofbilling');
		$amt=$request->get('amt');
		$dateofpayment=$request->get('dateofpayment');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$final_cost=$request->get('final_cost');

		DB::table('t_po_billing')
		->where('purchase_order_id',$po_id)
		->update(['bill_number'=>$bill_no,
			'date_of_billing'=>$dateofbilling,
			'advance'=>$advance,
			'discount'=>$discount,
			'amount'=>$amt,
			'final_cost'=>$final_cost,
			'purchase_order_status_id'=>$bill_status]);

		return redirect()->action('InventoryController@listviewInstrument',[$po_id]);
	}

	public function viewPurchaseOrderInstrument($po_id)
	{
		$instrument_type=DB::table('s_instrument_type')
		->get();


		$order_status=DB::table('dnf_purchase_order_status')
		->get();

		$curr_po=DB::table('t_purchase_order')
		->where('t_purchase_order.id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_purchase_order.purchase_order_status_id')
		->get();

		$curr_instrument=DB::table('t_purchase_order_instrument')
		->where('purchase_order_id',$po_id)
		->join('m_instrument_details','m_instrument_details.id','=','t_purchase_order_instrument.instrument_name_id')
		->join('s_instrument_subtype','s_instrument_subtype.id','=','m_instrument_details.instrument_subtype_id')
		->join('s_instrument_type','s_instrument_type.id','=','s_instrument_subtype.instrument_type_id')
		->select('*','s_instrument_type.name as instrument_type','s_instrument_subtype.name as instrument_subtype')
		->get();

		return view('inventory.purchaseorder.instrument.viewpurchaseorder',compact('po_id','curr_instrument','order_status','instrument_type','curr_po'));
	}

	public function viewVendorDetailsInstrument($po_id)
	{

		$vendordetails=DB::table('t_purchase_order_vendor_instrument')
		->where('t_purchase_order_vendor.purchase_order_id',$po_id)
		->join('t_purchase_order_vendor','t_purchase_order_vendor.id','=','t_purchase_order_vendor_instrument.po_vendor_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_vendor.purchase_order_id')
		->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','t_purchase_order_vendor.vendor_name_id')
		->join('m_instrument_details','m_instrument_details.id','=','t_purchase_order_vendor_instrument.instrument_order_id')
		->join('t_purchase_order_instrument','t_purchase_order_instrument.purchase_order_id','=','t_purchase_order_vendor.purchase_order_id')
		->get();



		return view('inventory.purchaseorder.instrument.viewvendordetails',compact('po_id','vendordetails'));
	}

	public function viewGenerateDeliveryInstrument($po_id)
	{
		$curr_delivery=DB::table('t_po_delivery')
		->where('purchase_order_id',$po_id)
		->get();
		return view('inventory.purchaseorder.instrument.viewgeneratedelivery',compact('po_id','curr_delivery'));
	}

	public function viewGenerateBillingInstrument($po_id)
	{
		$bill_status=DB::table('dnf_purchase_order_status')
		->get();

		$curr_bill=Db::table('t_po_billing')
		->where('purchase_order_id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_po_billing.purchase_order_status_id')
		->select('*')
		->get();
		/*dd($curr_bill);	*/
		return view('inventory.purchaseorder.instrument.viewgeneratebilling',compact('po_id','bill_status','curr_bill'));
	}

	public function listviewGenerateBillingInstrument()
	{
		return view('inventory.purchaseorder.instrument.addGenerateBilling');
	}

	public function listviewGadget()
	{
		$po_id=DB::table('t_purchase_order')
		->orderBy('id','desc')
		->first();

		if($po_id == null)
		{
			$po_id = (object)array('id' => 0);
		}

		$po_id = $po_id->id + 1;

		$po_gadget=DB::table('t_purchase_order_vendor_gadget')
		->join('t_purchase_order_vendor','t_purchase_order_vendor.id','=','t_purchase_order_vendor_gadget.po_vendor_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_vendor.purchase_order_id')
		->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','t_purchase_order_vendor.vendor_name_id')
		->leftjoin('t_po_billing','t_po_billing.purchase_order_id','=','t_purchase_order_vendor.purchase_order_id')
		->leftjoin('t_po_delivery','t_po_delivery.purchase_order_id','=','t_purchase_order_vendor.purchase_order_id')
		->select('*','t_purchase_order_vendor.purchase_order_id as po_id','m_vendor_gadget_details.name as vendorname')
		->get();


		return view('inventory.purchaseorder.gadget.listviewgadget',compact('po_id','po_gadget'));
	}

	public function addPurcahseorderGadget($po_id)
	{
		$gadget_type=DB::table('s_gadget_type')
		->get();
		$order_status=DB::table('dnf_purchase_order_status')
		->get();

		$curr_po=DB::table('t_purchase_order')
		->where('t_purchase_order.id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_purchase_order.purchase_order_status_id')
		->get();

		if($curr_po != null)
		{
			$curr_gadget=DB::table('t_purchase_order_gadget')
			->where('purchase_order_id',$po_id)
			->join('m_gadget_details','m_gadget_details.id','=','t_purchase_order_gadget.gadget_name_id')
			->join('s_gadget_subtype','s_gadget_subtype.id','=','m_gadget_details.gadget_subtype_id')
			->join('s_gadget_type','s_gadget_type.id','=','s_gadget_subtype.gadget_type_id')
			->select('*','m_gadget_details.gadget_name as gadgetname','s_gadget_subtype.name as gadget_subtype','s_gadget_type.name as gadget_type')
			->get();

			return view('inventory.purchaseorder.gadget.editPurchaseOrderDetails',compact('po_id','order_status','gadget_type','curr_po','curr_gadget'));
		}
		else
		{
			return view('inventory.purchaseorder.gadget.addPurchaseOrderDetails',compact('po_id','order_status','gadget_type'));
		}

	}

	public function storePurchaseOrderGadget(Request $request)
	{
		$po_id=$request->get('po_id');

		$dateoforder=$request->get('dateoforder');
		$expecteddateoforder=$request->get('expecteddateoforder');
		$order_status_id=$request->get('order_status_id');

		$gadget_type=$request->get('gadget_type');
		$gadget_subtype=$request->get('gadget_subtype');
		$gadget_name=$request->get('gadget_name');
		$quantity=$request->get('quantity');

		$pogadget=new PurchaseOrder;
		$pogadget->date_of_order=$dateoforder;
		$pogadget->expected_date_of_delivery=$expecteddateoforder;
		$pogadget->purchase_order_status_id=$order_status_id;

		$pogadget->save();

		foreach($gadget_name as $key=>$gadget)
		{
			$pogadget=new PurchseOrderGadget;
			$pogadget->gadget_name_id=$gadget;
			$pogadget->quantity=$quantity[$key];
			$pogadget->purchase_order_id=$po_id;

			$pogadget->save();
		}

		return redirect()->action('InventoryController@addVendorDetailsGadget',[$po_id]);

	}

	public function deleteGadget($gadget_name_id,$po_id)
	{
		DB::table('t_purchase_order_gadget')
		->where('gadget_name_id',$gadget_name_id)
		->where('purchase_order_id',$po_id)
		->delete();
		return redirect()->action('InventoryController@addPurcahseorderGadget',[$po_id]);
	}

	public function updatePurchaseOrderGadget(Request $request)
	{
		$po_id=$request->get('po_id');

		$dateoforder=$request->get('dateoforder');
		$expecteddateoforder=$request->get('expecteddateoforder');
		$order_status_id=$request->get('order_status_id');

		$gadget_type=$request->get('gadget_type');
		$gadget_subtype=$request->get('gadget_subtype');
		$gadget_name=$request->get('gadget_name');
		$quantity=$request->get('quantity');

		DB::table('t_purchase_order')
		->where('id',$po_id)
		->update(['date_of_order'=>$dateoforder,
			'expected_date_of_delivery'=>$expecteddateoforder,
			'purchase_order_status_id'=>$order_status_id]);

		foreach($gadget_name as $key=>$gadget)
		{
			/*if user does not select any option for instrument*/
			if($gadget=='')
			{
				return redirect()->action('InventoryController@addVendorDetailsGadget',[$po_id]);
			}
			else
			{
				$purchaseorder = new PurchseOrderGadget;
				$purchaseorder->gadget_name_id=$gadget;
				$purchaseorder->quantity=$quantity[$key];
				$purchaseorder->purchase_order_id=$po_id;

				$purchaseorder->save();

				return redirect()->action('InventoryController@addVendorDetailsGadget',[$po_id]);
			}
		}
	}

	public function addVendorDetailsGadget($po_id)
	{
		$vendornames=DB::table('m_vendor_gadget_details')
		->get();

		$vendorlist=DB::table('t_purchase_order_gadget')
		->where('t_purchase_order_gadget.purchase_order_id',$po_id)
		->join('m_gadget_details','m_gadget_details.id','=','t_purchase_order_gadget.gadget_name_id')
		->join('m_vendor_gadget_workspec_details','m_vendor_gadget_workspec_details.gadget_name_id','=','m_gadget_details.id')
		->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','m_vendor_gadget_workspec_details.vendor_gadget_id')
		->join('t_purchase_order_vendor','t_purchase_order_vendor.purchase_order_id','=','t_purchase_order_gadget.purchase_order_id')
		->join('t_purchase_order_vendor_gadget','t_purchase_order_vendor_gadget.po_vendor_id','=','t_purchase_order_vendor.id')
		->get();

		if($vendorlist != null)
		{
			$vendors=DB::table('t_purchase_order_gadget')
			->where('purchase_order_id',$po_id)
			->join('m_gadget_details','m_gadget_details.id','=','t_purchase_order_gadget.gadget_name_id')
			->join('m_vendor_gadget_workspec_details','m_vendor_gadget_workspec_details.gadget_name_id','=','m_gadget_details.id')
			->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','m_vendor_gadget_workspec_details.vendor_gadget_id')
			->get();

			$ven_gadget=DB::table('t_purchase_order_vendor_gadget')
			->where('t_purchase_order_vendor.purchase_order_id',$po_id)
			->where('t_purchase_order_gadget.purchase_order_id',$po_id)
			->join('t_purchase_order_vendor','t_purchase_order_vendor.id','=','t_purchase_order_vendor_gadget.po_vendor_id')
			->join('t_purchase_order_gadget','t_purchase_order_gadget.gadget_name_id','=','t_purchase_order_vendor_gadget.gadget_order_id')
			->join('m_gadget_details','m_gadget_details.id','=','t_purchase_order_gadget.gadget_name_id')
			->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','t_purchase_order_vendor.vendor_name_id')
			->get();

			return view('inventory.purchaseorder.gadget.editvendordetalis',compact('vendorlist','po_id','vendornames','ven_gadget','vendors'));
		}
		else
		{
			$vendors=DB::table('t_purchase_order_gadget')
			->where('purchase_order_id',$po_id)
			->join('m_gadget_details','m_gadget_details.id','=','t_purchase_order_gadget.gadget_name_id')
			->join('m_vendor_gadget_workspec_details','m_vendor_gadget_workspec_details.gadget_name_id','=','m_gadget_details.id')
			->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','m_vendor_gadget_workspec_details.vendor_gadget_id')
			->get();

			return view('inventory.purchaseorder.gadget.addvendordetails',compact('vendorlist','po_id','vendornames','vendors'));
		}
	}

	public function storeVendorDetailsGadget(Request $request)
	{
		$po_id=$request->get('po_id');
		$gadget_name_id=$request->get('gadget_name_id');
		$vendor_gadget_id=$request->get('vendor_gadget_id');
		$rate=$request->get('rate');
		$amount=$request->get('amount');
		$totalamt=$request->get('totalamt');
		$discount=$request->get('discount');
		$finalamount=$request->get('finalamount');

		$povendor=new purchaseOrderVendor;
		$povendor->total_amount=$totalamt;
		$povendor->discount=$discount;
		$povendor->final_amount=$finalamount;
		$povendor->vendor_name_id=$vendor_gadget_id;
		$povendor->purchase_order_id=$po_id;

		$povendor->save();

		$po_vendor=DB::table('t_purchase_order_vendor')
		->where('purchase_order_id',$po_id)
		->select('id')
		->get();
		$po_vendor_id=$po_vendor['0']->id;

		foreach($gadget_name_id as $key=>$gadget)
		{
			$povendorgadget=new purchaseOrderVendorGadget;
			$povendorgadget->rate=$rate[$key];
			$povendorgadget->amount=$amount[$key];
			$povendorgadget->po_vendor_id=$po_vendor_id;
			$povendorgadget->gadget_order_id=$gadget;

			$povendorgadget->save();
		}

		return redirect()->action('InventoryController@addGenerateDeliveryGadget',[$po_id]);				
	}

	public function updateVendorDetailsGadget(Request $request)
	{

		$po_id=$request->get('po_id');
		$gadget_name_id=$request->get('gadget_name_id');
		$vendor_gadget_id=$request->get('vendor_gadget_id');
		$rate=$request->get('rate');
		$amount=$request->get('amount');
		$totalamt=$request->get('curr_totalamt');
		$discount=$request->get('discount');
		$finalamount=$request->get('finalamount');

		if($vendor_gadget_id == null)
		{
			DB::table('t_purchase_order_vendor')
			->where('purchase_order_id',$po_id)
			->update(['discount'=>$discount,
				'final_amount'=>$finalamount]);
		}
		else
		{
			DB::table('t_purchase_order_vendor')
			->where('purchase_order_id',$po_id)
			->update(['total_amount'=>$totalamt,
				'discount'=>$discount,
				'final_amount'=>$finalamount,
				'vendor_name_id'=>$vendor_gadget_id]);

			$po_vendor=DB::table('t_purchase_order_vendor')
			->where('purchase_order_id',$po_id)
			->select('id')
			->get();

			$po_vendor_id=$po_vendor['0']->id;

			DB::table('t_purchase_order_vendor_gadget')
			->where('po_vendor_id',$po_vendor_id)
			->delete();

			foreach($gadget_name_id as $key=>$gadget)
			{
				$povendorgadget=new purchaseOrderVendorGadget;
				$povendorgadget->rate=$rate[$key];
				$povendorgadget->amount=$amount[$key];
				$povendorgadget->po_vendor_id=$po_vendor_id;
				$povendorgadget->gadget_order_id=$gadget;

				$povendorgadget->save();			
			}	
		}

		return redirect()->action('InventoryController@addGenerateDeliveryGadget',[$po_id]);
	}

	public function addGenerateDeliveryGadget($po_id)
	{
		$curr_delivery=DB::table('t_po_delivery')
		->where('purchase_order_id',$po_id)
		->get();

		return view('inventory.purchaseorder.gadget.addGenerateDelivery',compact('po_id','curr_delivery'));
	}

	public function deleteGadgetChallan($po_id,$id)
	{
		DB::table('t_po_delivery')
		->where('purchase_order_id',$po_id)
		->where('id',$id)
		->delete();

		return redirect()->action('InventoryController@addGenerateDeliveryGadget',[$po_id]);
	}

	public function storeGenerateDeliveryGadget(Request $request)
	{
		$po_id=$request->get('po_id');
		$challan_no=$request->get('challan_no');
		$date_of_delivery=$request->get('date_of_delivery');
		$delivery_location=$request->get('delivery_location');

		foreach($challan_no as $key=>$challan)
		{
			$podelivery=new PurchaseOrderDelivery;
			$podelivery->chalan_number=$challan;
			$podelivery->delivery_date=$date_of_delivery[$key];
			$podelivery->delivery_location=$delivery_location[$key];
			$podelivery->purchase_order_id=$po_id;

			$podelivery->save();
		}

		return redirect()->action('InventoryController@addGenerateBillingGadget',[$po_id]);
	}

	public function addGenerateBillingGadget($po_id)
	{
		$bill_status=DB::table('dnf_purchase_order_status')
		->get();

		$curr_bill=Db::table('t_po_billing')
		->where('purchase_order_id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_po_billing.purchase_order_status_id')
		->get();

		if($curr_bill != null)
		{
			return view('inventory.purchaseorder.gadget.editGenerateBilling',compact('po_id','bill_status','curr_bill'));
		}
		else
		{
			return view('inventory.purchaseorder.gadget.addGenerateBilling',compact('po_id','bill_status'));
		}	
	}

	public function storeganerateBillingGadget(Request $request)
	{
		$po_id=$request->get('po_id');

		$bill_no=$request->get('bill_no');
		$bill_status=$request->get('bill_status');
		$dateofbilling=$request->get('dateofbilling');
		$amt=$request->get('amt');
		$dateofpayment=$request->get('dateofpayment');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$final_cost=$request->get('final_cost');

		$pobilling = new PurchaseOrderBilling;
		$pobilling->purchase_order_id=$po_id;
		$pobilling->bill_number=$bill_no;

		$pobilling->date_of_billing=$dateofbilling;
		$pobilling->advance	=$advance;
		$pobilling->discount=$discount;
		$pobilling->amount=$amt;
		$pobilling->purchase_order_status_id=$bill_status;
		$pobilling->purchase_order_id=$po_id;

		$pobilling->save();

		return redirect()->action('InventoryController@listviewGadget',[$po_id]);	
	}

	public function updateGenerateBillingGadget(Request $request)
	{
		$po_id=$request->get('po_id');

		$bill_no=$request->get('bill_no');
		$bill_status=$request->get('bill_status');
		$dateofbilling=$request->get('dateofbilling');
		$amt=$request->get('amt');
		$dateofpayment=$request->get('dateofpayment');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$final_cost=$request->get('final_cost');

		/*dd($final_cost);*/

		DB::table('t_po_billing')
		->where('purchase_order_id',$po_id)
		->update(['bill_number'=>$bill_no,
			'date_of_billing'=>$dateofbilling,
			'advance'=>$advance,
			'discount'=>$discount,
			'amount'=>$amt,
			'final_cost'=>$final_cost,
			'purchase_order_status_id'=>$bill_status]);

		return redirect()->action('InventoryController@listviewGadget',[$po_id]);
	}

	public function generateBillingGadgetListview($po_id)
	{
		$bill_status=DB::table('dnf_purchase_order_status')
		->get();

		return view('inventory.purchaseorder.gadget.addGenerateBilling',compact('po_id','bill_status'));

	}

	public function viewPurchaseOrderGadget($po_id)
	{	
		$gadget_type=DB::table('s_gadget_type')
		->get();
		$order_status=DB::table('dnf_purchase_order_status')
		->get();

		$curr_po=DB::table('t_purchase_order')
		->where('t_purchase_order.id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_purchase_order.purchase_order_status_id')
		->get();

		$curr_gadget=DB::table('t_purchase_order_gadget')
		->where('purchase_order_id',$po_id)
		->join('m_gadget_details','m_gadget_details.id','=','t_purchase_order_gadget.gadget_name_id')
		->join('s_gadget_subtype','s_gadget_subtype.id','=','m_gadget_details.gadget_subtype_id')
		->join('s_gadget_type','s_gadget_type.id','=','s_gadget_subtype.gadget_type_id')
		->select('*','m_gadget_details.gadget_name as gadgetname','s_gadget_subtype.name as gadget_subtype','s_gadget_type.name as gadget_type')
		->get();

		return view('inventory.purchaseorder.gadget.viewpurchaseorderdetails',compact('po_id','order_status','gadget_type','curr_po','curr_gadget'));

	}

	public function viewVendorDetailsGadget($po_id)
	{

		$vendordetails=DB::table('t_purchase_order_vendor_gadget')
		->where('t_purchase_order_vendor.purchase_order_id',$po_id)
		->join('t_purchase_order_vendor','t_purchase_order_vendor.id','=','t_purchase_order_vendor_gadget.po_vendor_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_vendor.purchase_order_id')
		->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','t_purchase_order_vendor.vendor_name_id')
		->join('m_gadget_details','m_gadget_details.id','=','t_purchase_order_vendor_gadget.gadget_order_id')
		->join('t_purchase_order_gadget','t_purchase_order_gadget.purchase_order_id','=','t_purchase_order_vendor.purchase_order_id')
		->get();


		return view('inventory.purchaseorder.gadget.viewvendordetails',compact('po_id','vendordetails'));
	}

	public function viewGenerateDeliveryGadget($po_id)
	{
		$curr_delivery=DB::table('t_po_delivery')
		->where('purchase_order_id',$po_id)
		->get();

		return view('inventory.purchaseorder.gadget.viewgeneratedelivery',compact('po_id','curr_delivery'));
	}

	public function viewGenerateBillingGadget($po_id)
	{
		$bill_status=DB::table('dnf_purchase_order_status')
		->get();

		$curr_bill=Db::table('t_po_billing')
		->where('purchase_order_id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_po_billing.purchase_order_status_id')
		->select('*')
		->get();

		return view('inventory.purchaseorder.gadget.viewgeneratebilling',compact('po_id','bill_status','curr_bill'));
	}

	public function listviewMachine()
	{
		$po_id=DB::table('t_purchase_order')
		->orderBy('id','desc')
		->first();

		if($po_id == null)
		{
			$po_id = (object)array('id' => 0);
		}

		$po_id = $po_id->id + 1;

		$po_machine=DB::table('t_purchase_order_vendor_machine')
		->join('t_purchase_order_vendor','t_purchase_order_vendor.id','=','t_purchase_order_vendor_machine.po_vendor_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_vendor.purchase_order_id')
		->join('m_vendor_machine_details','m_vendor_machine_details.id','=','t_purchase_order_vendor.vendor_name_id')
		->leftjoin('t_po_billing','t_po_billing.purchase_order_id','=','t_purchase_order_vendor.purchase_order_id')
		->leftjoin('t_po_delivery','t_po_delivery.purchase_order_id','=','t_purchase_order_vendor.purchase_order_id')
		->select('*','t_purchase_order_vendor.purchase_order_id as po_id','m_vendor_machine_details.name as vendorname')
		->get();

		return view('inventory.purchaseorder.machine.listviewmachine',compact('po_id','po_machine'));
	}

	public function addPurchaseOrderMachine($po_id)
	{
		$order_status=DB::table('dnf_purchase_order_status')
		->get();
		$machine_type=DB::table('s_machine_type')
		->get();

		$curr_po=DB::table('t_purchase_order')
		->where('t_purchase_order.id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_purchase_order.purchase_order_status_id')
		->get();

		if($curr_po != null)
		{
			$curr_machine=DB::table('t_purchase_order_machine')
			->where('purchase_order_id',$po_id)
			->join('m_machine_details','m_machine_details.id','=','t_purchase_order_machine.machine_name_id')
			->join('s_machine_subtype','s_machine_subtype.id','=','m_machine_details.machine_subtype_id')
			->join('s_machine_type','s_machine_type.id','=','s_machine_subtype.machine_type_id')
			->select('*','m_machine_details.machine_name as machinename','s_machine_subtype.name as machine_subtype','s_machine_type.name as machine_type')
			->get();


			return view('inventory.purchaseorder.machine.editPurchaseOrderDetails',compact('po_id','order_status','machine_type','curr_po','curr_machine'));
		}	
		else
		{
			return view('inventory.purchaseorder.machine.addPurchaseOrderDetails',compact('po_id','order_status','machine_type'));
		}				

	}

	public function deleteMachine($machine_name_id,$po_id)
	{
		DB::table('t_purchase_order_machine')
		->where('machine_name_id',$machine_name_id)
		->where('purchase_order_id',$po_id)
		->delete();
		return redirect()->action('InventoryController@addPurchaseOrderMachine',[$po_id]);	
	}

	public function storePurchaseOrderMachine(Request $request)
	{
		$po_id=$request->get('po_id');

		$dateoforder=$request->get('dateoforder');
		$expecteddateoforder=$request->get('expecteddateoforder');
		$order_status_id=$request->get('order_status_id');

		$machine_type=$request->get('machine_type');
		$machine_subtype=$request->get('machine_subtype');
		$machine_name=$request->get('machine_name');
		$quantity=$request->get('quantity');

		$pomachine=new PurchaseOrder;
		$pomachine->date_of_order=$dateoforder;
		$pomachine->expected_date_of_delivery=$expecteddateoforder;
		$pomachine->purchase_order_status_id=$order_status_id;

		$pomachine->save();

		foreach($machine_name as $key=>$machine)
		{
			$machineDet=new PurchaseOrderMachine;
			$machineDet->machine_name_id=$machine;
			$machineDet->quantity=$quantity[$key];
			$machineDet->purchase_order_id=$po_id;

			$machineDet->save();
		}

		return redirect()->action('InventoryController@addVendorDetailsMachine',[$po_id]);
	}

	public function updatePurchaceOrderMachine(Request $request)
	{
		$po_id=$request->get('po_id');

		$dateoforder=$request->get('dateoforder');
		$expecteddateoforder=$request->get('expecteddateoforder');
		$order_status_id=$request->get('order_status_id');

		$machine_type=$request->get('machine_type');
		$machine_subtype=$request->get('machine_subtype');
		$machine_name=$request->get('machine_name');
		$quantity=$request->get('quantity');

		DB::table('t_purchase_order')
		->where('id',$po_id)
		->update(['date_of_order'=>$dateoforder,
			'expected_date_of_delivery'=>$expecteddateoforder,
			'purchase_order_status_id'=>$order_status_id]);

		foreach($machine_name as $key=>$machine)
		{

			/*if user does not select any option for instrument*/
			if($machine=='')
			{
				return redirect()->action('InventoryController@addVendorDetailsMachine',[$po_id]);
			}
			else
			{
				$machineDet=new PurchaseOrderMachine;
				$machineDet->machine_name_id=$machine;
				$machineDet->quantity=$quantity[$key];
				$machineDet->purchase_order_id=$po_id;

				$machineDet->save();

				return redirect()->action('InventoryController@addVendorDetailsMachine',[$po_id]);
			}			
		}

	}

	public function addVendorDetailsMachine($po_id)
	{
		$vendornames=DB::table('m_vendor_machine_details')
		->get();

		$vendorlist=DB::table('t_purchase_order_machine')
		->where('t_purchase_order_machine.purchase_order_id',$po_id)
		->join('m_machine_details','m_machine_details.id','=','t_purchase_order_machine.machine_name_id')
		->join('m_vendor_machine_workspec_details','m_vendor_machine_workspec_details.machine_name_id','=','m_machine_details.id')
		->join('m_vendor_machine_details','m_vendor_machine_details.id','=','m_vendor_machine_workspec_details.vendor_machine_id')
		->join('t_purchase_order_vendor','t_purchase_order_vendor.purchase_order_id','=','t_purchase_order_machine.purchase_order_id')
		->join('t_purchase_order_vendor_machine','t_purchase_order_vendor_machine.po_vendor_id','=','t_purchase_order_vendor.id')
		->get();

		if($vendorlist != null)
		{
			$vendors=DB::table('t_purchase_order_machine')
			->where('purchase_order_id',$po_id)
			->join('m_machine_details','m_machine_details.id','=','t_purchase_order_machine.machine_name_id')
			->join('m_vendor_machine_workspec_details','m_vendor_machine_workspec_details.machine_name_id','=','m_machine_details.id')
			->join('m_vendor_machine_details','m_vendor_machine_details.id','=','m_vendor_machine_workspec_details.vendor_machine_id')
			->get();

			$ven_machine=DB::table('t_purchase_order_vendor_machine')
			->where('t_purchase_order_vendor.purchase_order_id',$po_id)
			->where('t_purchase_order_machine.purchase_order_id',$po_id)
			->join('t_purchase_order_vendor','t_purchase_order_vendor.id','=','t_purchase_order_vendor_machine.po_vendor_id')
			->join('t_purchase_order_machine','t_purchase_order_machine.machine_name_id','=','t_purchase_order_vendor_machine.machine_order_id')
			->join('m_machine_details','m_machine_details.id','=','t_purchase_order_machine.machine_name_id')
			->join('m_vendor_machine_details','m_vendor_machine_details.id','=','t_purchase_order_vendor.vendor_name_id')
			->get();

			return view('inventory.purchaseorder.machine.editvendordetails',compact('vendorlist','po_id','vendornames','ven_machine','vendors'));
		}
		else
		{
			$vendors=DB::table('t_purchase_order_machine')
			->where('purchase_order_id',$po_id)
			->join('m_machine_details','m_machine_details.id','=','t_purchase_order_machine.machine_name_id')
			->join('m_vendor_machine_workspec_details','m_vendor_machine_workspec_details.machine_name_id','=','m_machine_details.id')
			->join('m_vendor_machine_details','m_vendor_machine_details.id','=','m_vendor_machine_workspec_details.vendor_machine_id')
			->get();

			return view('inventory.purchaseorder.machine.addvendordetails',compact('po_id','vendorlist','vendornames','vendors'));
		}

	}

	public function storeVendorDetailsMachine(Request $request)
	{

		$po_id=$request->get('po_id');
		$machine_name_id=$request->get('machine_name_id');
		$vendor_machine_id=$request->get('vendor_machine_id');
		$rate=$request->get('rate');
		$amount=$request->get('amount');
		$totalamt=$request->get('totalamt');
		$discount=$request->get('discount');
		$finalamount=$request->get('finalamount');

		$povendor=new purchaseOrderVendor;
		$povendor->total_amount=$totalamt;
		$povendor->discount=$discount;
		$povendor->final_amount=$finalamount;
		$povendor->vendor_name_id=$vendor_machine_id;
		$povendor->purchase_order_id=$po_id;

		$povendor->save();

		$po_vendor=DB::table('t_purchase_order_vendor')
		->where('purchase_order_id',$po_id)
		->select('id')
		->get();
		$po_vendor_id=$po_vendor['0']->id;

		foreach($machine_name_id as $key=>$machine)
		{
			$povendorgadget=new purchaseOrderVendorMachine;
			$povendorgadget->rate=$rate[$key];
			$povendorgadget->amount=$amount[$key];
			$povendorgadget->po_vendor_id=$po_vendor_id;
			$povendorgadget->machine_order_id=$machine;

			$povendorgadget->save();
		}

		return redirect()->action('InventoryController@addGenerateDeliveryMachine',[$po_id]);
	}

	public function updateVendordetailsMachine(Request $request)
	{
		$po_id=$request->get('po_id');
		$machine_name_id=$request->get('machine_name_id');
		$vendor_machine_id=$request->get('vendor_machine_id');
		$rate=$request->get('rate');
		$amount=$request->get('amount');
		$totalamt=$request->get('curr_totalamt');
		$discount=$request->get('discount');
		$finalamount=$request->get('finalamount');

		if($vendor_machine_id == null)
		{
			DB::table('t_purchase_order_vendor')
			->where('purchase_order_id',$po_id)
			->update(['discount'=>$discount,
				'final_amount'=>$finalamount]);
		}
		else
		{
			DB::table('t_purchase_order_vendor')
			->where('purchase_order_id',$po_id)
			->update(['total_amount'=>$totalamt,
				'discount'=>$discount,
				'final_amount'=>$finalamount,
				'vendor_name_id'=>$vendor_machine_id]);

			$po_vendor=DB::table('t_purchase_order_vendor')
			->where('purchase_order_id',$po_id)
			->select('id')
			->get();

			$po_vendor_id=$po_vendor['0']->id;

			DB::table('t_purchase_order_vendor_machine')
			->where('po_vendor_id',$po_vendor_id)
			->delete();

			foreach($machine_name_id as $key=>$machine)
			{
				$povendormachine=new purchaseOrderVendorMachine;
				$povendormachine->rate=$rate[$key];
				$povendormachine->amount=$amount[$key];
				$povendormachine->po_vendor_id=$po_vendor_id;
				$povendormachine->machine_order_id=$machine;

				$povendormachine->save();			
			}	
		}

		return redirect()->action('InventoryController@addGenerateDeliveryMachine',[$po_id]);
	}

	public function addGenerateDeliveryMachine($po_id)
	{
		$curr_delivery=DB::table('t_po_delivery')
		->where('purchase_order_id',$po_id)
		->get();

		return view('inventory.purchaseorder.machine.addgeneratedelivery',compact('po_id','curr_delivery'));
	}

	public function deleteMachineChallan($po_id,$id)
	{
		DB::table('t_po_delivery')
		->where('purchase_order_id',$po_id)
		->where('id',$id)
		->delete();

		return redirect()->action('InventoryController@addGenerateDeliveryMachine',[$po_id]);
	}

	public function storeGenerateDeliveryMachine(Request $request)
	{
		$po_id=$request->get('po_id');
		$challan_no=$request->get('challan_no');
		$date_of_delivery=$request->get('date_of_delivery');
		$delivery_location=$request->get('delivery_location');

		foreach($challan_no as $key=>$challan)
		{
			$podelivery=new PurchaseOrderDelivery;
			$podelivery->chalan_number=$challan;
			$podelivery->delivery_date=$date_of_delivery[$key];
			$podelivery->delivery_location=$delivery_location[$key];
			$podelivery->purchase_order_id=$po_id;

			$podelivery->save();
		}

		return redirect()->action('InventoryController@addGenerateBillingMachine',[$po_id]);
	}

	public function addGenerateBillingMachine($po_id)
	{
		/*$bill_status=DB::table('dnf_purchase_order_status')
		->get();
		return view('inventory.purchaseorder.machine.addgeneratebilling',compact('po_id','bill_status'));*/

		$bill_status=DB::table('dnf_purchase_order_status')
		->get();

		$curr_bill=Db::table('t_po_billing')
		->where('purchase_order_id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_po_billing.purchase_order_status_id')
		->get();

		if($curr_bill != null)
		{
			return view('inventory.purchaseorder.machine.editgeneratebilling',compact('po_id','bill_status','curr_bill'));
		}
		else
		{
			return view('inventory.purchaseorder.machine.addgeneratebilling',compact('po_id','bill_status'));
		}
	}

	public function storeGenerateBillingMachine(Request $request)
	{
		$po_id=$request->get('po_id');

		$bill_no=$request->get('bill_no');
		$bill_status=$request->get('bill_status');
		$dateofbilling=$request->get('dateofbilling');
		$amt=$request->get('amt');
		$dateofpayment=$request->get('dateofpayment');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$final_cost=$request->get('final_cost');

		$pobilling = new PurchaseOrderBilling;
		$pobilling->purchase_order_id=$po_id;
		$pobilling->bill_number=$bill_no;

		$pobilling->date_of_billing=$dateofbilling;
		$pobilling->advance	=$advance;
		$pobilling->discount=$discount;
		$pobilling->amount=$amt;
		$pobilling->purchase_order_status_id=$bill_status;
		$pobilling->purchase_order_id=$po_id;

		$pobilling->save();

		return redirect()->action('InventoryController@listviewMachine',[$po_id]);
	}

	public function updateGenerateBillingMachine(Request $request)
	{
		$po_id=$request->get('po_id');

		$bill_no=$request->get('bill_no');
		$bill_status=$request->get('bill_status');
		$dateofbilling=$request->get('dateofbilling');
		$amt=$request->get('amt');
		$dateofpayment=$request->get('dateofpayment');
		$advance=$request->get('advance');
		$discount=$request->get('discount');
		$final_cost=$request->get('final_cost');

		DB::table('t_po_billing')
		->where('purchase_order_id',$po_id)
		->update(['bill_number'=>$bill_no,
			'date_of_billing'=>$dateofbilling,
			'advance'=>$advance,
			'discount'=>$discount,
			'amount'=>$amt,
			'final_cost'=>$final_cost,
			'purchase_order_status_id'=>$bill_status]);

		return redirect()->action('InventoryController@listviewMachine',[$po_id]);
	}

	public function listviewMachineGenerateBilling($po_id)
	{
		$bill_status=DB::table('dnf_purchase_order_status')
		->get();
		return view('inventory.purchaseorder.machine.addgeneratebilling',compact('po_id','bill_status'));
	}

	public function viewDetailsaddMachine($po_id)
	{
		$order_status=DB::table('dnf_purchase_order_status')
		->get();
		$machine_type=DB::table('s_machine_type')
		->get();

		$curr_po=DB::table('t_purchase_order')
		->where('t_purchase_order.id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_purchase_order.purchase_order_status_id')
		->get();
		
		$curr_machine=DB::table('t_purchase_order_machine')
		->where('purchase_order_id',$po_id)
		->join('m_machine_details','m_machine_details.id','=','t_purchase_order_machine.machine_name_id')
		->join('s_machine_subtype','s_machine_subtype.id','=','m_machine_details.machine_subtype_id')
		->join('s_machine_type','s_machine_type.id','=','s_machine_subtype.machine_type_id')
		->select('*','m_machine_details.machine_name as machinename','s_machine_subtype.name as machine_subtype','s_machine_type.name as machine_type')
		->get();

		return view('inventory.purchaseorder.machine.viewpurchaseorderdetails',compact('po_id','order_status','machine_type','curr_po','curr_machine'));
		
	}

	public function viewVendorDetailsMachine($po_id)
	{
		$vendordetails=DB::table('t_purchase_order_vendor_machine')
		->where('t_purchase_order_vendor.purchase_order_id',$po_id)
		->join('t_purchase_order_vendor','t_purchase_order_vendor.id','=','t_purchase_order_vendor_machine.po_vendor_id')
		->join('t_purchase_order','t_purchase_order.id','=','t_purchase_order_vendor.purchase_order_id')
		->join('m_vendor_machine_details','m_vendor_machine_details.id','=','t_purchase_order_vendor.vendor_name_id')
		->join('m_machine_details','m_machine_details.id','=','t_purchase_order_vendor_machine.machine_order_id')
		->join('t_purchase_order_machine','t_purchase_order_machine.purchase_order_id','=','t_purchase_order_vendor.purchase_order_id')
		->get();


		return view('inventory.purchaseorder.machine.viewvendordetails',compact('po_id','vendordetails'));
	}

	public function viewGenerateDeliveryMachine($po_id)
	{
		$curr_delivery=DB::table('t_po_delivery')
		->where('purchase_order_id',$po_id)
		->get();

		return view('inventory.purchaseorder.machine.viewgeneratedelivery',compact('po_id','curr_delivery'));
	}

	public function viewGenerateBillingMachine($po_id)
	{
		$bill_status=DB::table('dnf_purchase_order_status')
		->get();

		$curr_bill=Db::table('t_po_billing')
		->where('purchase_order_id',$po_id)
		->join('dnf_purchase_order_status','dnf_purchase_order_status.id','=','t_po_billing.purchase_order_status_id')
		->select('*')
		->get();
		return view('inventory.purchaseorder.machine.viewgeneratebilling',compact('po_id','curr_bill','bill_status'));
	}

	public function dashboardAlert()
	{

		$po_id=DB::table('t_purchase_order')
		->orderBy('id','desc')
		->first();
		
		if($po_id == null)
		{
			$po_id = (object)array('id' => 0);
		}

		$po_id = $po_id->id + 1;

		$material_details = DB::table('m_material_details')
		->join('t_purchase_order_material','m_material_details.id', '=','t_purchase_order_material.material_name_id')
		->select('*','m_material_details.id as material_id','m_material_details.id as material_name_id')
		->get();

		$threshold_status = DB::table('dnf_threshold_status')
		->orderBy('id')
		->first();

		$material_po_id=DB::table('t_purchase_order_material')
		->orderBy('id','desc')
		->first();

		return view('inventory.alert.dashboardAlert',compact('material_details','po_id','curr_mat_details','threshold_status'));
	}

	public function DashboardInstrumentSafetyStock()
	{
		$threshold_status = DB::table('dnf_threshold_status')
		->where('id','2')
		->first();

		$po_id=DB::table('t_purchase_order')
		->orderBy('id','desc')
		->first();
		
		if($po_id == null)
		{
			$po_id = (object)array('id' => 0);
		}

		$po_id = $po_id->id + 1;

		$instrument_details = DB::table('m_instrument_details')
		->join('t_purchase_order_instrument', 'm_instrument_details.id', '=', 't_purchase_order_instrument.instrument_name_id')
		->select('*','m_instrument_details.id as instrument_id','m_instrument_details.id as instrument_name_id')
		->get();

		return view('inventory.alert.dashboardAlertSafetyStock',compact('instrument_details','po_id','instrument_details','threshold_status'));
	}

	// ajax functions

	public function getmaterialsubtype($material_id)
	{
		$materialsubtype=DB::table('s_material_subtype')
		->where('material_type_id',$material_id)
		->get();

		return $materialsubtype;
	}

	public function getmaterilaname($materialsubtype_id)
	{
		$materialname=DB::table('m_material_details')
		->where('material_subtype_id',$materialsubtype_id)
		->get();

		return $materialname;
	}

	public function getinstrumentsubtype($instrument_id)
	{
		$instrumentsubtype=DB::table('s_instrument_subtype')
		->where('instrument_type_id',$instrument_id)
		->get();

		return $instrumentsubtype;
	}

	public function getinstrumentname($instrument_subtypeid)
	{
		$instrumentnames=DB::table('m_instrument_details')
		->where('instrument_subtype_id',$instrument_subtypeid)
		->get();

		return $instrumentnames;
	}

	public function getgadgetsubtype($gadget_id)
	{
		$gadgetsubtype=DB::table('s_gadget_subtype')
		->where('gadget_type_id',$gadget_id)
		->get();

		return $gadgetsubtype;
	}

	public function getgadgetname($gadget_subtypeid)
	{
		$gadgetname=DB::table('m_gadget_details')
		->where('gadget_subtype_id',$gadget_subtypeid)
		->get();

		return $gadgetname;
	}

	public function getmachinesubtype($machine_id)
	{
		$machinesubtype=DB::table('s_machine_subtype')
		->where('machine_type_id',$machine_id)
		->get();

		return $machinesubtype;
	}

	public function getmachinename($machine_subtypeid)
	{
		$machinename=DB::table('m_machine_details')
		->where('machine_subtype_id',$machine_subtypeid)
		->get();

		return $machinename;
	}

	public function vendorMaterialNames($van_id,$po_id)
	{
		$materials=DB::table('m_vendor_mat_workspec_details')
		->where('m_vendor_material_details.id',$van_id)
		->where('t_purchase_order_material.purchase_order_id',$po_id)
		->join('m_vendor_material_details','m_vendor_material_details.id','=','m_vendor_mat_workspec_details.vendor_mat_id')
		->join('m_material_details','m_material_details.id','=','m_vendor_mat_workspec_details.mat_name_id')
		->join('t_purchase_order_material','t_purchase_order_material.material_name_id','=','m_material_details.id')
		->select('*',DB::raw('t_purchase_order_material.quantity*m_vendor_mat_workspec_details.rate AS amount'))
		->get();

		return $materials;
	}

	public function vendorInstrumentNames($van_id,$po_id)
	{
		$instruments=DB::table('m_vendor_inst_workspec_details')
		->where('m_vendor_instrument_details.id',$van_id)
		->where('t_purchase_order_instrument.purchase_order_id',$po_id)
		->join('m_vendor_instrument_details','m_vendor_instrument_details.id','=','m_vendor_inst_workspec_details.vendor_inst_id')
		->join('m_instrument_details','m_instrument_details.id','=','m_vendor_inst_workspec_details.inst_name_id')
		->join('t_purchase_order_instrument','t_purchase_order_instrument.instrument_name_id','=','m_instrument_details.id')
		->select('*',DB::raw('t_purchase_order_instrument.quantity*m_vendor_inst_workspec_details.rate AS amount'))
		->get();

		return $instruments;
	}

	public function vendorGadgetNames($van_id,$po_id)
	{
		$gadgets=DB::table('m_vendor_gadget_workspec_details')
		->where('m_vendor_gadget_details.id',$van_id)
		->where('t_purchase_order_gadget.purchase_order_id',$po_id)
		->join('m_vendor_gadget_details','m_vendor_gadget_details.id','=','m_vendor_gadget_workspec_details.vendor_gadget_id')
		->join('m_gadget_details','m_gadget_details.id','=','m_vendor_gadget_workspec_details.gadget_name_id')
		->join('t_purchase_order_gadget','t_purchase_order_gadget.gadget_name_id','=','m_gadget_details.id')
		->select('*',DB::raw('t_purchase_order_gadget.quantity*m_vendor_gadget_workspec_details.rate AS amount'))
		->get();

		return $gadgets;
	}

	public function vendorMachineNames($van_id,$po_id)
	{

		$machines=DB::table('m_vendor_machine_workspec_details')
		->where('m_vendor_machine_details.id',$van_id)
		->where('t_purchase_order_machine.purchase_order_id',$po_id)
		->join('m_vendor_machine_details','m_vendor_machine_details.id','=','m_vendor_machine_workspec_details.vendor_machine_id')
		->join('m_machine_details','m_machine_details.id','=','m_vendor_machine_workspec_details.machine_name_id')
		->join('t_purchase_order_machine','t_purchase_order_machine.machine_name_id','=','m_machine_details.id')
		->select('*',DB::raw('t_purchase_order_machine.quantity*m_vendor_machine_workspec_details.rate AS amount'))
		->get();

		return $machines;
	}

}
