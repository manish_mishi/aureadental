<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Http\Request;
use App\ChairDetails;
use App\DentistDetails;
use App\Materialtype;
use App\Materialsubtype;
use App\Unit;
use App\Instrumenttype;
use App\Instrumentsubtype;
use App\Instrumentunit;
use App\Machinetype;
use App\Machinesubtype;
use App\Machineunit;
use App\Gadgettype;
use App\Gadgetsubtype;
use App\TreatmentType;
use App\TreatmentStatus;
use App\WorkType;
use App\WorkSubtype;
use App\WorkName;
use App\PaymentMode;
use App\BillingStatus;
use App\StaffType;
use DB;

class SettingsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		return view('layouts.settingsNav');
	}

	public function createChairDetails()
	{
		$chairdetails=DB::table('s_chair_details')
		->get();

		return view('settings.patient.chairdetails.addChairDetails',compact('chairdetails'));
	}
	
	public function storeChairDetails(Request $request)
	{
		$chairname=new ChairDetails;
		$chairname->chair_name=$request->get('chair_name');
		$chairname->save();
		$request->session()->flash('alert-success', 'Chair Name was successfully added!');

		return redirect()->action('SettingsController@createChairDetails');

	}

	public function destroyChairDetails(Request $request)
	{
		$chairid=$request->get('chair_id');

		DB::table('s_chair_details')->where('id', $chairid)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Chair Name was successfully deleted!');

		return redirect()->action('SettingsController@createChairDetails');
	}

	public function createDentistDetails()
	{
		$dentistdetails=DB::table('s_dentist_details')
		->get();
		
		return view('settings.patient.dentistdetails.addDentistDetails',compact('dentistdetails'));
	}
	public function storeDentistDetails(Request $request)
	{
		$dentistname= new DentistDetails;
		$dentistname->dentist_name=$request->get('dentist_name');

		$dentistname->save();

//flash-message
		$request->session()->flash('alert-success', 'Dentist Name was successfully added!');

		return redirect()->action('SettingsController@createDentistDetails');

	}
	public function destroyDentistDetails(Request $request)
	{
		$dentistid=$request->get('dentist_id');

		DB::table('s_dentist_details')->where('id', $dentistid)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Dentist Name was successfully deleted!');

		return redirect()->action('SettingsController@createDentistDetails');
	}

	/******************Planner starts *********************/

	public function viewFollowUpStatus()
	{
		$followUpStatusDetails = DB::table('dnf_follow_up_status')
		->get();

		return view('settings.planner.viewFollowUpStatus',compact('followUpStatusDetails'));
	}

	public function viewAppointmentStatus()
	{
		$appointmentStatusDetails = DB::table('dnf_appointment_status')
		->get();

		return view('settings.planner.viewAppointmentStatus',compact('appointmentStatusDetails'));
	}

	/******************treatment starts *********************/

	public function createTreatmentType()
	{
		$treattyp_details = DB::table('s_treatment_type')
		->get();

		return view('settings.treatment.addTreatmentType',compact('treattyp_details'));
	}

	public function storeTreatmentType(Request $request)
	{
		$treatment_type= new TreatmentType;
		$treatment_type->name=$request->get('treat_type');

		$treatment_type->save();

//flash-message
		$request->session()->flash('alert-success', 'Treatment Type was successfully added!');

		return redirect()->action('SettingsController@createTreatmentType');

	}

	public function destroyTreatmentType(Request $request)
	{
		$treattyp_id=$request->get('treat_type_id');

		DB::table('s_treatment_type')->where('id', $treattyp_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Treatment Type was successfully deleted!');

		return redirect()->action('SettingsController@createTreatmentType');
	}

	public function viewTreatmentStatus()
	{
		$treatstat_details = DB::table('dnf_treatment_status')
		->get();

		return view('settings.treatment.viewTreatmentStatus',compact('treatstat_details'));
	}

	public function storeTreatmentStatus(Request $request)
	{
		$treatment_status= new TreatmentStatus;
		$treatment_status->name=$request->get('treat_status');

		$treatment_status->save();

//flash-message
		$request->session()->flash('alert-success', 'Treatment Status was successfully added!');

		return redirect()->action('SettingsController@createTreatmentStatus');

	}

	public function destroyTreatmentStatus(Request $request)
	{
		$treattyp_id=$request->get('treat_status_id');

		DB::table('s_treatment_status')->where('id', $treattyp_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Treatment Status was successfully deleted!');

		return redirect()->action('SettingsController@createTreatmentStatus');
	}


	/******************Inventory starts *********************/

	public function createMaterialType()
	{
		$mattyp_details = DB::table('s_material_type')
		->get();

		return view('settings.inventory.materialmanagement.addMaterialType',compact('mattyp_details'));
	}

	public function storeMaterialType(Request $request)
	{
		$materialtype= new Materialtype;
		$materialtype->name=$request->get('mat_type');

		$materialtype->save();

//flash-message
		$request->session()->flash('alert-success', 'Material Type was successfully added!');

		return redirect()->action('SettingsController@createMaterialType');

	}

	public function destroyMaterialType(Request $request)
	{
		$mattyp_id=$request->get('mat_type_id');

		DB::table('s_material_type')->where('id', $mattyp_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Material Type was successfully deleted!');

		return redirect()->action('SettingsController@createMaterialType');
	}

	public function createMaterialSubtype()
	{
		$matsubtyp_details = DB::table('s_material_type')
		->join('s_material_subtype','s_material_subtype.material_type_id','=','s_material_type.id')
		->select('*','s_material_type.name as material_type_name')
		->get();

		$materialTypes = DB::table('s_material_type')
		->get();

		return view('settings.inventory.materialmanagement.addMaterialSubtype',compact('materialTypes','matsubtyp_details'));
	}

	public function storeMaterialSubtype(Request $request)
	{
		$materialsubtype =new Materialsubtype;
		$materialsubtype->name=$request->get('mat_subtype');
		$materialsubtype->material_type_id=$request->get('material_type_id');

		$materialsubtype->save();

		//flash-message
		$request->session()->flash('alert-success', 'Material SubType was successfully added!');

		return redirect()->action('SettingsController@createMaterialSubtype');
	}

	public function destroyMaterialSubtype(Request $request)
	{
		$matsubtyp_id=$request->get('mat_subtype_id');

		DB::table('s_material_subtype')->where('id', $matsubtyp_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Material SubType was successfully deleted!');

		return redirect()->action('SettingsController@createMaterialSubtype');
	}

	public function createMaterialUnit()
	{
		$matunit_details = DB::table('s_material_unit')
		->get();

		return view('settings.inventory.materialmanagement.addMaterialUnit', compact('matunit_details'));
	}

	public function storeMateriaUnit(Request $request)
	{
		$unit=new Unit;
		$unit->name=$request->get('unit');

		$unit->save();

		//flash-message
		$request->session()->flash('alert-success', 'Material Unit was successfully added!');

		return redirect()->action('SettingsController@createMaterialUnit');
	}

	public function destroyMaterialUnit(Request $request)
	{
		$matunit_id=$request->get('mat_unit_id');

		DB::table('s_material_unit')->where('id', $matunit_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Material Unit was successfully deleted!');

		return redirect()->action('SettingsController@createMaterialUnit');
	}

	public function createInstrumentType()
	{
		$insttyp_details = DB::table('s_instrument_type')
		->get();

		return view('settings.inventory.instrumentmanagement.addInstrumentType',compact('insttyp_details'));
	}

	public function storeInstrumentType(Request $request)
	{
		$instrumenttype = new Instrumenttype;
		$instrumenttype->name=$request->get('instrument_type');

		$instrumenttype->save();

		//flash-message
		$request->session()->flash('alert-success', 'Instrument Type was successfully added!');

		return redirect()->action('SettingsController@createInstrumentType');
	}

	public function destroyInstrumentType(Request $request)
	{
		$insttyp_id=$request->get('inst_typ_id');
		DB::table('s_instrument_type')->where('id', $insttyp_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Instrument Type was successfully deleted!');

		return redirect()->action('SettingsController@createInstrumentType');
	}

	public function createInstrumentSubtype()
	{
		$instsubtyp_details = DB::table('s_instrument_type')
		->join('s_instrument_subtype','s_instrument_subtype.instrument_type_id','=','s_instrument_type.id')
		->select('*','s_instrument_type.name as instrument_type_name')
		->get();

		$instrumentTypes = DB::table('s_instrument_type')
		->get();

		return view('settings.inventory.instrumentmanagement.addInstrumentSubtype',compact('instrumentTypes','instsubtyp_details'));
	}

	public function storeInstrumentSubtype(Request $request)
	{
		$instrumentsubtype = new Instrumentsubtype;
		$instrumentsubtype->name=$request->get('instrument_subtype');
		$instrumentsubtype->instrument_type_id = $request->get('instrument_type_id');

		$instrumentsubtype->save();

		//flash-message
		$request->session()->flash('alert-success', 'Instrument SubType was successfully added!');

		return redirect()->action('SettingsController@createInstrumentSubtype');
	}

	public function destroyInstrumentSubtype(Request $request)
	{
		$instsubtyp_id=$request->get('inst_subtype_id');
		DB::table('s_instrument_subtype')->where('id', $instsubtyp_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Instrument SubType was successfully deleted!');

		return redirect()->action('SettingsController@createInstrumentSubtype');
	}

	public function createInstrumentUnit()
	{
		$instunit_details = DB::table('s_instrument_unit')
		->get();

		return view('settings.inventory.instrumentmanagement.addInstrumentUnit',compact('instunit_details'));
	}

	public function storeInstrumentUnit(Request $request)
	{

		$instrumentunit = new Instrumentunit;
		$instrumentunit->name=$request->get('instrument_unit');

		$instrumentunit->save();

		//flash-message
		$request->session()->flash('alert-success', 'Instrument Unit was successfully added!');

		return redirect()->action('SettingsController@createInstrumentUnit');
	}

	public function destroyInstrumentUnit(Request $request)
	{
		$instunit_id=$request->get('inst_unit_id');
		DB::table('s_instrument_unit')->where('id', $instunit_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Instrument Unit was successfully deleted!');

		return redirect()->action('SettingsController@createInstrumentUnit');
	}
/*
	public function Dashboardmachinemanagement()
	{
		return view('settings.inventory.machinemanagement.dashboardmachinemanagement');
	}*/

	public function createMachineType()
	{
		$mchtyp_details = DB::table('s_machine_type')
		->get();

		return view('settings.inventory.machinemanagement.addMachineType',compact('mchtyp_details'));
	}

	public function storeMachineType(Request $request)
	{
		$machinetype = new Machinetype;
		$machinetype->name = $request->get('machine_type');

		$machinetype->save();

		//flash-message
		$request->session()->flash('alert-success', 'Machine Type was successfully added!');

		return redirect()->action('SettingsController@createMachineType');
	}

	public function destroyMachineType(Request $request)
	{
		$mchtyp_id=$request->get('mch_typ_id');

		DB::table('s_machine_type')->where('id', $mchtyp_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Machine Type was successfully deleted!');

		return redirect()->action('SettingsController@createMachineType');
	}

	public function createMachineSubtype()
	{
		$mchsubtyp_details = DB::table('s_machine_type')
		->join('s_machine_subtype','s_machine_subtype.machine_type_id','=','s_machine_type.id')
		->select('*','s_machine_type.name as machine_type_name')
		->get();

		$machineTypes = DB::table('s_machine_type')
		->get();

		return view('settings.inventory.machinemanagement.addMachineSubtype',compact('machineTypes','mchsubtyp_details'));
	}

	public function storeMachineSubtype(Request $request)
	{
		$machinesubtype= new Machinesubtype;
		$machinesubtype->name=$request->get('machine_subtype');
		$machinesubtype->machine_type_id=$request->get('machine_type_id');

		$machinesubtype->save();

		//flash-message
		$request->session()->flash('alert-success', 'Machine SubType was successfully added!');

		return redirect()->action('SettingsController@createMachineSubtype');

	}

	public function destroyMachineSubtype(Request $request)
	{
		$mchsubtyp_id=$request->get('mch_subtype_id');

		DB::table('s_machine_subtype')->where('id', $mchsubtyp_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Machine SubType was successfully deleted!');

		return redirect()->action('SettingsController@createMachineSubtype');
	}

	public function createMachineUnit()
	{
		$mchunit_details = DB::table('s_machine_unit')
		->get();

		return view('settings.inventory.machinemanagement.addMachineUnit',compact('mchunit_details'));
	}

	public function storeMachineUnit(Request $request)
	{
		$machineunit= new Machineunit;
		$machineunit->name=$request->get('machine_unit');

		//flash-message
		$request->session()->flash('alert-success', 'Machine Unit was successfully added!');

		$machineunit->save();

		return redirect()->action('SettingsController@createMachineUnit');
	}

	public function destroyMachineUnit(Request $request)
	{
		$mchunit_id=$request->get('mch_unit_id');

		DB::table('s_machine_unit')->where('id', $mchunit_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Machine Unit was successfully deleted!');

		return redirect()->action('SettingsController@createMachineUnit');
	}
/*
	public function Dashboardgadgetmanagement()
	{
		return view('settings.inventory.gadgetmanagement.dashboardgadgetmanagement');
	}
*/
	public function createGadgetType()
	{
		$gdgttyp_details = DB::table('s_gadget_type')
		->get();

		return view('settings.inventory.gadgetmanagement.addGadgetType',compact('gdgttyp_details'));
	}

	public function storeGadgetType(Request $request)
	{
		$gadgettype = new Gadgettype;
		$gadgettype->name=$request->get('gadget_type');

		$gadgettype->save();

		//flash-message
		$request->session()->flash('alert-success', 'Gadget Type was successfully added!');

		return redirect()->action('SettingsController@createGadgetType');
	}

	public function destroyGadgetType(Request $request)
	{
		$gdgttyp_id=$request->get('gdgt_typ_id');

		DB::table('s_gadget_type')->where('id', $gdgttyp_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Gadget Type was successfully deleted!');

		return redirect()->action('SettingsController@createGadgetType');
	}

	public function createGadgetSubtype()
	{
		$gdgtsubtyp_details = DB::table('s_gadget_type')
		->join('s_gadget_subtype','s_gadget_subtype.gadget_type_id','=','s_gadget_type.id')
		->select('*','s_gadget_type.name as gadget_type_name')
		->get();

		$gadgetTypes = DB::table('s_gadget_type')
		->get();

		return view('settings.inventory.gadgetmanagement.addGadgetSubtype',compact('gadgetTypes','gdgtsubtyp_details'));
	}

	public function storeGadgetSubtype(Request $request)
	{
		$gadgetsubtype = new Gadgetsubtype;
		$gadgetsubtype->name = $request->get('gadget_subtype');
		$gadgetsubtype->gadget_type_id= $request->get('gadget_type_id');

		$gadgetsubtype->save();

		//flash-message
		$request->session()->flash('alert-success', 'Gadget SubType was successfully added!');

		return redirect()->action('SettingsController@createGadgetSubtype');
	}

	public function destroyGadgetSubtype(Request $request)
	{
		$gdgtsubtyp_id=$request->get('gdgt_subtype_id');

		DB::table('s_gadget_subtype')->where('id', $gdgtsubtyp_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Gadget SubType was successfully deleted!');

		return redirect()->action('SettingsController@createGadgetSubtype');
	}

	public function createStaff()
	{
		return view('settings.clinic.staff.materialdashboard');
	}

	/***************Lab starts *********************/   

	public function createLabWorkType()
	{
		$worktyp_details=DB::table('s_lab_work_type')
		->get();
		return view('settings.lab.addWorkType',compact('worktyp_details'));                 
	}

	public function storeLabWorkType(Request $request)
	{
		$worktype=new WorkType;
		$worktype->name=$request->get('work_type');

		$worktype->save();

		$request->session()->flash('alert-success','WorkType was successfully added !');

		return redirect()->action('SettingsController@createLabWorkType');

	}

	public function destroyLabWorkType(Request $request)
	{
		$worktype_id=$request->get('work_type_id');

		DB::table('s_lab_work_type')->where('id', $worktype_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Material Type was successfully deleted!');

		return redirect()->action('SettingsController@createLabWorkType');

	}

	public function createLabWorkSubtype()
	{
		$worksubtyp_details = DB::table('s_lab_work_type')
		->join('s_lab_work_subtype', 's_lab_work_subtype.lab_work_type_id', '=', 's_lab_work_type.id')
		->select('*','s_lab_work_type.name as lab_work_type')
		->get();

		$workType = DB::table('s_lab_work_type')
		->get();

		return view('settings.lab.addWorkSubtype',compact('workType','worksubtyp_details'));
	}

	public function storeLabWorkSubtype(Request $request)
	{
		$worksubtype =new WorkSubtype;
		$worksubtype->name=$request->get('work_subtype');
		$worksubtype->lab_work_type_id=$request->get('work_type_id');

		$worksubtype->save();

		//flash-message
		$request->session()->flash('alert-success', 'Work SubType was successfully added!');

		return redirect()->action('SettingsController@createLabWorkSubtype');
	}

	public function destroyLabWorkSubType(Request $request)
	{
		$worksubtyp_id=$request->get('work_subtype_id');

		DB::table('s_lab_work_subtype')->where('id', $worksubtyp_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Work SubType was successfully deleted!');

		return redirect()->action('SettingsController@createLabWorkSubtype');
	}

	public function createLabWorkName()
	{
		$workname_details = DB::table('s_lab_work_subtype')
		->join('s_lab_work_type', 's_lab_work_subtype.lab_work_type_id', '=', 's_lab_work_type.id')
		->join('s_lab_work_name', 's_lab_work_name.lab_work_type_id', '=', 's_lab_work_type.id')
		->select('*','s_lab_work_type.name as lab_work_type','s_lab_work_subtype.name as lab_work_subtype')
		->get();

		$workType = DB::table('s_lab_work_type')
		->get();

		return view('settings.lab.addWorkName',compact('workType','workname_details'));
	}

	public function storeLabWorkName(Request $request)
	{
		$worksubtype =new WorkName;
		$worksubtype->name=$request->get('work_name');
		$worksubtype->lab_work_type_id=$request->get('work_type');
		$worksubtype->lab_work_subtype_id=$request->get('work_subtype');

		$worksubtype->save();

		//flash-message
		$request->session()->flash('alert-success', 'Work Name was successfully added!');

		return redirect()->action('SettingsController@createLabWorkName');
	}

	public function destroyLabWorkName(Request $request)
	{
		$workname_id=$request->get('work_name_id');

		DB::table('s_lab_work_name')->where('id', $workname_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Work Name was successfully deleted!');

		return redirect()->action('SettingsController@createLabWorkName');
	}

	public function viewLabWorkStatus()
	{
		$labWorkStatusDetails = DB::table('dnf_lab_work_status')
		->get();

		return view('settings.lab.viewLabWorkStatus',compact('labWorkStatusDetails'));
	}


	public function createStaffType()
	{
		$stafftype_details = DB::table('s_staff_type')
		->get();

		return view('settings.clinic.staff.addStaffType',compact('stafftype_details'));
	}


	public function storeStaffType(Request $request)
	{
		$stafftype= new StaffType;
		$stafftype->name=$request->get('staff_type');

		$stafftype->save();

//flash-message
		$request->session()->flash('alert-success', 'Staff Type was successfully added!');

		return redirect()->action('SettingsController@createStaffType');

	}

	public function destroyStaffType(Request $request)
	{
		$stafftype_id=$request->get('staff_type_id');

		DB::table('s_staff_type')->where('id', $stafftype_id)->delete();

		//flash-message
		$request->session()->flash('alert-danger', 'Material Type was successfully deleted!');

		return redirect()->action('SettingsController@createStaffType');
	}

	/***************	settings billing part start *********************/   

	public function viewPaymentMode()
	{
		$paymentdetails=DB::table('dnf_payment_mode')
		->get();
		
		return view('settings.billing.paymentmode.viewPaymentMode',compact('paymentdetails'));
	}

	public function viewBillingStatus()
	{
		$billing_status=DB::table('dnf_billing_status')
		->get();
		
		return view('settings.billing.billingstatus.viewBillingStatus',compact('billing_status'));
	}


//***********Ajax function	


	public function getWorkSubtype($work_id)
	{
		$work_subtype = DB::table('s_lab_work_subtype')
		->where("lab_work_type_id", $work_id)->lists('name','id');

		foreach($work_subtype as $key=>$data)
		{

			$items[$key]=$data;
		}

		return $items;

	}

}
