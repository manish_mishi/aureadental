<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorWorkSpecification extends Model 
{

	protected $table='m_material_name';

	protected $fillable=['name','material_type_id','material_subtype_id','unit_id'];

}
