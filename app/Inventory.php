<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model 
{

	protected $table='m_material_name'; 

	protected $fillable=['name','material_type_id','material_subtype_id','unit_id','material-treatment_id'];

}
