<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Materialsubtype extends Model 
{

	protected $table ='s_material_subtype';

	protected $fillable = ['name'];

}
