<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class InstrumentDetails extends Model {

	protected $table='m_instrument_details';
	protected $fillable=['instrument_name','instrument_type','instrument_subtype','instrument_unit'];

}
