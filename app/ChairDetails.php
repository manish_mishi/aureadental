<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ChairDetails extends Model 
{

	protected $table='s_chair_details';

	protected $fillable=['chair_name'];

}
