<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorDetails extends Model {

	protected $table='m_vendor_detail';

	protected $fillable=['name','address','contact_person','cell_no','email'];
}
