<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DentistDetails extends Model 
{

	protected $table='s_dentist_details';

	protected $fillable=['dentist_name'];

}
