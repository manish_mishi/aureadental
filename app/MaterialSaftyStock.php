<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialSaftyStock extends Model {

	protected $table='m_material_alerts';
	protected $fillable=['safety_stock_value'];

}
