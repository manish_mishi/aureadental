<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMode extends Model 
{

	protected $table='s_payment_mode';

	protected $fillable=['name'];

}
