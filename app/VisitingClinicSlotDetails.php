<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitingClinicSlotDetails extends Model {

	protected $table='m_visiting_clinic_slot_details';

}
