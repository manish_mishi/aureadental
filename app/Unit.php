<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model 
{

protected $table='s_material_unit';
protected $fillable=['name'];

}
