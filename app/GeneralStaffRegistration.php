<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralStaffRegistration extends Model{

protected $table='m_staff_registration';
protected $fillable=['name','gender','cell_no','date_of_birth','email','primary_clinic','visiting_clinic'];

}