<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetails extends Model {

	protected $table='m_bank';

	protected $fillable=['account_holder','bank_name','account_number','branch'];
}
