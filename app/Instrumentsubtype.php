<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Instrumentsubtype extends Model 
{
	protected $table='s_instrument_subtype';

	protected $fillable=['name'];
}
