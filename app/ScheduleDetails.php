<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleDetails extends Model 
{

	protected $table='schedule_details';

	protected $fillable=['patient_name','cell_no','assign_date','dentist_name','assign_slot','chair','assistant','consultant','status'];

}
